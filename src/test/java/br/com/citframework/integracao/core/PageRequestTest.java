/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao.core;

import static br.com.centralit.UnitTestUtils.assertEqualsAndHashcode;
import static br.com.centralit.UnitTestUtils.assertNotEqualsAndHashcode;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * Classe de testes para valida��o do comportamento {@link PageRequest}
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 01/10/2014
 *
 */
public final class PageRequestTest {

    private PageRequest newPageRequest(final int page, final int size) {
        return new PageRequest(page, size);
    }

    @Test(expected = IllegalArgumentException.class)
    public void preventsNegativePage() {
        this.newPageRequest(-1, 10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void preventsNegativeSize() {
        this.newPageRequest(0, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void preventsPageSizeLessThanOne() {
        this.newPageRequest(0, 0);
    }

    @Test
    public void navigatesPageablesCorrectly() {
        final Pageable request = this.newPageRequest(1, 10);
        assertThat(request.hasPrevious(), is(true));
        assertThat(request.next(), is((Pageable) this.newPageRequest(2, 10)));

        final Pageable first = request.previousOrFirst();
        assertThat(first.hasPrevious(), is(false));
        assertThat(first, is((Pageable) this.newPageRequest(0, 10)));
        assertThat(first, is(request.first()));
        assertThat(first.previousOrFirst(), is(first));
    }

    @Test
    public void equalsHonoursPageAndSize() {
        final PageRequest request = this.newPageRequest(0, 10);
        assertEqualsAndHashcode(request, request);
        assertEqualsAndHashcode(request, this.newPageRequest(0, 10));
        assertNotEqualsAndHashcode(request, this.newPageRequest(1, 10));
        assertNotEqualsAndHashcode(request, this.newPageRequest(0, 11));
    }

}
