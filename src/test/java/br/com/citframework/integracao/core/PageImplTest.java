/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao.core;

import static br.com.centralit.UnitTestUtils.assertEqualsAndHashcode;
import static br.com.centralit.UnitTestUtils.assertNotEqualsAndHashcode;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

/**
 * Classe de testes para valida��o do comportamento {@link PageImpl}
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 01/10/2014
 *
 */
public final class PageImplTest {

    @Test
    public void assertEqualsForSimpleSetup() throws Exception {
        final PageImpl<String> page = new PageImpl<>(Arrays.asList("Foo"));
        assertEqualsAndHashcode(page, page);
        assertEqualsAndHashcode(page, new PageImpl<>(Arrays.asList("Foo")));
    }

    @Test
    public void assertEqualsForComplexSetup() throws Exception {
        final Pageable pageable = new PageRequest(0, 10);
        final List<String> content = Arrays.asList("Foo");
        final PageImpl<String> page = new PageImpl<>(content, pageable, 100);
        assertEqualsAndHashcode(page, page);
        assertEqualsAndHashcode(page, new PageImpl<>(content, pageable, 100));
        assertNotEqualsAndHashcode(page, new PageImpl<>(content, pageable, 90));
        assertNotEqualsAndHashcode(page, new PageImpl<>(content, new PageRequest(1, 10), 100));
        assertNotEqualsAndHashcode(page, new PageImpl<>(content, new PageRequest(0, 15), 100));
    }

    @Test(expected = IllegalArgumentException.class)
    public void preventsNullContentForSimpleSetup() throws Exception {
        new PageImpl<>(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void preventsNullContentForAdvancedSetup() throws Exception {
        new PageImpl<>(null, null, 0);
    }

    @Test
    public void returnsNextPageable() {
        final Page<Object> page = new PageImpl<>(Arrays.asList(new Object()), new PageRequest(0, 1), 10);
        assertThat(page.isFirst(), is(true));
        assertThat(page.hasPrevious(), is(false));
        assertThat(page.previousPageable(), is(nullValue()));
        assertThat(page.isLast(), is(false));
        assertThat(page.hasNext(), is(true));
        assertThat(page.nextPageable(), is((Pageable) new PageRequest(1, 1)));
    }

    @Test
    public void returnsPreviousPageable() {
        final Page<Object> page = new PageImpl<>(Arrays.asList(new Object()), new PageRequest(1, 1), 2);
        assertThat(page.isFirst(), is(false));
        assertThat(page.hasPrevious(), is(true));
        assertThat(page.previousPageable(), is((Pageable) new PageRequest(0, 1)));
        assertThat(page.isLast(), is(true));
        assertThat(page.hasNext(), is(false));
        assertThat(page.nextPageable(), is(nullValue()));
    }

    @Test
    public void createsPageForEmptyContentCorrectly() {
        final List<String> list = Collections.emptyList();
        final Page<String> page = new PageImpl<>(list);
        assertThat(page.getContent(), is(list));
        assertThat(page.getNumber(), is(0));
        assertThat(page.getNumberOfElements(), is(0));
        assertThat(page.getSize(), is(0));
        assertThat(page.getTotalElements(), is(0L));
        assertThat(page.getTotalPages(), is(1));
        assertThat(page.hasNext(), is(false));
        assertThat(page.hasPrevious(), is(false));
        assertThat(page.isFirst(), is(true));
        assertThat(page.isLast(), is(true));
        assertThat(page.hasContent(), is(false));
    }

    @Test
    public void returnsCorrectTotalPages() {
        final Page<String> page = new PageImpl<>(Arrays.asList("a"));
        assertThat(page.getTotalPages(), is(1));
        assertThat(page.hasNext(), is(false));
        assertThat(page.hasPrevious(), is(false));
    }

}
