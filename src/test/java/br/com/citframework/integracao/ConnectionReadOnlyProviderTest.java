/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao;

import java.sql.Connection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.citframework.util.ReflectionUtils;

/**
 * Classe de testes para valida��o do comportamento de {@link ConnectionReadOnlyProvider}
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 20/08/2014
 *
 */
public final class ConnectionReadOnlyProviderTest extends ConnectionProviderAbstractTest {

    private final Map<String, Connection> connections = new ConcurrentHashMap<>();

    @Before
    public void setUp() throws Exception {
        ReflectionUtils.setFieldOnStatic(ConnectionReadOnlyProvider.class, "connections", connections);
    }

    @Test
    public void testGetConnectionWithClassURLUserPassword() throws Exception {
        final Connection conn = ConnectionReadOnlyProvider.getConnection(EMBEDDED_JAVADB_JDBC_CLASS, EMBEDDED_JAVADB_JDBC_URL, EMBEDDED_JAVADB_JDBC_USER,
                EMBEDDED_JAVADB_JDBC_PASSWORD);
        Assert.assertNotNull(conn);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetConnectionWithoutClass() throws Exception {
        final Connection conn = ConnectionReadOnlyProvider.getConnection(EMPTY_STRING, EMBEDDED_JAVADB_JDBC_URL, EMBEDDED_JAVADB_JDBC_USER, EMBEDDED_JAVADB_JDBC_PASSWORD);
        Assert.assertNull(conn);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetConnectionWithoutURL() throws Exception {
        final Connection conn = ConnectionReadOnlyProvider.getConnection(EMBEDDED_JAVADB_JDBC_CLASS, EMPTY_STRING, EMBEDDED_JAVADB_JDBC_USER, EMBEDDED_JAVADB_JDBC_PASSWORD);
        Assert.assertNull(conn);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetConnectionWithoutUser() throws Exception {
        final Connection conn = ConnectionReadOnlyProvider.getConnection(EMBEDDED_JAVADB_JDBC_CLASS, EMBEDDED_JAVADB_JDBC_URL, EMPTY_STRING, EMBEDDED_JAVADB_JDBC_PASSWORD);
        Assert.assertNull(conn);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetConnectionWithoutPassword() throws Exception {
        final Connection conn = ConnectionReadOnlyProvider.getConnection(EMBEDDED_JAVADB_JDBC_CLASS, EMBEDDED_JAVADB_JDBC_URL, EMBEDDED_JAVADB_JDBC_USER, EMPTY_STRING);
        Assert.assertNull(conn);
    }

    @Test
    public void testGetConnectionFromJNDI() throws Exception {
        final Connection conn = ConnectionReadOnlyProvider.getConnection(JNDI_DATASOURCE);
        Assert.assertNotNull(conn);
        final Connection connection = ConnectionReadOnlyProvider.getConnection(JNDI_DATASOURCE);
        Assert.assertSame(conn, connection);
    }

}
