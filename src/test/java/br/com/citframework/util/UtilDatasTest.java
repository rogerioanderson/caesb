/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

/**
 * Classe de testes para validar comportamento de utilit�rios de
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 28/10/2014
 *
 */
public final class UtilDatasTest {

    private static final Integer DAY_28 = 28;
    private static final Integer DAY_27 = 27;
    private static final Integer YEAR_2014 = 2014;
    private static final Integer DAYS_TO_ADD_SUBTRACT = 30;

    @Test
    public void testAddDaysOnDate() {
        final Date datePlus30 = UtilDatas.addDaysOnDate(this.getDate(), DAYS_TO_ADD_SUBTRACT);

        final Calendar cal = Calendar.getInstance();
        cal.setTime(datePlus30);

        Assert.assertEquals(cal.get(Calendar.DAY_OF_MONTH), DAY_27.intValue());
        Assert.assertEquals(cal.get(Calendar.MONTH), Calendar.NOVEMBER);
        Assert.assertEquals(cal.get(Calendar.YEAR), YEAR_2014.intValue());
    }

    @Test
    public void testSubtractDaysFromDate() {
        final Date dateMinus30 = UtilDatas.subtractDaysFromDate(this.getDate(), DAYS_TO_ADD_SUBTRACT);

        final Calendar cal = Calendar.getInstance();
        cal.setTime(dateMinus30);

        Assert.assertEquals(cal.get(Calendar.DAY_OF_MONTH), DAY_28.intValue());
        Assert.assertEquals(cal.get(Calendar.MONTH), Calendar.SEPTEMBER);
        Assert.assertEquals(cal.get(Calendar.YEAR), YEAR_2014.intValue());
    }

    private Date date;

    private Date getDate() {
        if (date == null) {
            final Calendar cal = Calendar.getInstance();
            cal.set(Calendar.DAY_OF_MONTH, DAY_28);
            cal.set(Calendar.MONTH, Calendar.OCTOBER);
            cal.set(Calendar.YEAR, YEAR_2014);
            date = cal.getTime();
        }
        return date;
    }

}
