/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Classe de testes para valida��o do comportamento de {@link PropertyFile}
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 03/02/2015
 *
 */
public class PropertyFileTest {

    private static final String PROPERTY_FILE_NAME = "property-file";
    private static final String NOT_FOUND_PROPERTY_FILE_NAME = "property";

    private static final String PROPERTY_EMPTY = "property.empty";
    private static final String PROPERTY_DEFAULT_VALUE = "DefaultValue";
    private static final String PROPERTY_WITH_CONTENT = "property.with.content";
    private static final String PROPERTY_WITH_CONTENT_VALUE = "HasContent";

    private static final String UNDEFINED_KEY = "???";

    @Test
    public void testGetPropertyFound() {
        final PropertyFile pFile = PropertyFile.getPropertyFile(PROPERTY_FILE_NAME);
        Assert.assertEquals(pFile.getProperty(PROPERTY_WITH_CONTENT), PROPERTY_WITH_CONTENT_VALUE);
    }

    @Test
    public void testGetPropertyNotFound() {
        final PropertyFile pFile = PropertyFile.getPropertyFile(PROPERTY_FILE_NAME);
        Assert.assertEquals(pFile.getProperty(PROPERTY_EMPTY), UNDEFINED_KEY + PROPERTY_EMPTY + UNDEFINED_KEY);
    }

    @Test
    public void testGetPropertyNotFoundDefault() {
        final PropertyFile pFile = PropertyFile.getPropertyFile(PROPERTY_FILE_NAME);
        Assert.assertEquals(pFile.getProperty(PROPERTY_EMPTY, PROPERTY_DEFAULT_VALUE), PROPERTY_DEFAULT_VALUE);
    }

    @Test(expected = RuntimeException.class)
    public void testNotFoundPropertyFile() {
        PropertyFile.getPropertyFile(NOT_FOUND_PROPERTY_FILE_NAME);
    }

}
