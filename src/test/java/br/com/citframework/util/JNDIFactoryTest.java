/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import static org.mockito.Mockito.mock;

import javax.naming.Context;
import javax.resource.ResourceException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.centralit.MockInitialContextRule;

/**
 * Classe de testes, mockada, para valida��o do comportamento de {@link JNDIFactory}
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 19/08/2014
 *
 */
public final class JNDIFactoryTest {

    private static Context context = mock(Context.class);

    private static JNDIFactory factory = new JNDIFactory();

    private static final String JNDI_JAVA = "java:";
    private static final String JNDI_JAVA_TEST = JNDI_JAVA.concat("/test");
    private static final String JNDI_JAVA_TEST_NOTFOUND = JNDI_JAVA.concat("/notFound");

    private final String BINDED_STRING = "This is STRIINNNGGGG!!";

    @Rule
    public MockInitialContextRule mockInitialContextRule = new MockInitialContextRule(context);

    @BeforeClass
    public static void setUpClass() throws Exception {
        context.createSubcontext(JNDI_JAVA);
        context.createSubcontext(JNDI_JAVA_TEST);

        ReflectionUtils.setField(factory, "context", context);
    }

    @Test
    public void testPutResource() throws Exception {
        final Boolean result = factory.putResource(context, JNDI_JAVA_TEST, BINDED_STRING);
        Assert.assertTrue(result);
        Mockito.verify(context, Mockito.times(1)).bind(JNDI_JAVA_TEST, BINDED_STRING);
    }

    @Test
    public void testGetResource() throws Exception {
        factory.getResource(JNDI_JAVA_TEST);
        Mockito.when(context.lookup(JNDI_JAVA_TEST)).thenReturn(JNDI_JAVA_TEST);
        Mockito.verify(context, Mockito.times(1)).lookup(JNDI_JAVA_TEST);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testGetResourceNotFound() throws Exception {
        Mockito.when(context.lookup(JNDI_JAVA_TEST_NOTFOUND)).thenThrow(ResourceException.class);
    }

}
