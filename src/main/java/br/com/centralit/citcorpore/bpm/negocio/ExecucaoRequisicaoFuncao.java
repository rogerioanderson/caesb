/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bpm.negocio;

import java.util.Map;

import br.com.centralit.bpm.dto.EventoFluxoDTO;
import br.com.centralit.bpm.dto.FluxoDTO;
import br.com.centralit.bpm.negocio.InstanciaFluxo;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.rh.bean.RequisicaoFuncaoDTO;
import br.com.centralit.citcorpore.rh.integracao.RequisicaoFuncaoDao;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;

public class ExecucaoRequisicaoFuncao extends ExecucaoSolicitacao {

    @Override
    public InstanciaFluxo inicia() throws Exception {
        return super.inicia();
    }

    @Override
    public InstanciaFluxo inicia(FluxoDTO fluxoDto, Integer idFase) throws Exception {
        String idGrupo = ParametroUtil.getValor(ParametroSistema.ID_GRUPO_PADRAO_REQ_RH, getTransacao(), null);
        if (idGrupo == null || idGrupo.trim().equals(""))
            throw new Exception("Grupo padr�o para atendimento de solicita��es de recursos humanos n�o parametrizado");
        getSolicitacaoServicoDto().setIdGrupoAtual(new Integer(idGrupo));
        return super.inicia(fluxoDto, idFase);
    }

    @Override
    public void mapObjetoNegocio(Map<String, Object> map) throws Exception {
        super.mapObjetoNegocio(map);
    }

    @Override
    public void executaEvento(EventoFluxoDTO eventoFluxoDto) throws Exception {
        super.executaEvento(eventoFluxoDto);
    }

    public boolean requisicaoAprovada() throws Exception {

        RequisicaoFuncaoDao requisicaoFuncaoDao = new RequisicaoFuncaoDao();
        setTransacaoDao(requisicaoFuncaoDao);
        SolicitacaoServicoDTO solicitacaoDto = getSolicitacaoServicoDto();
        RequisicaoFuncaoDTO requisicaoFuncaoDto = new RequisicaoFuncaoDTO();
        requisicaoFuncaoDto.setIdSolicitacaoServico(solicitacaoDto.getIdSolicitacaoServico());
        requisicaoFuncaoDto = (RequisicaoFuncaoDTO) requisicaoFuncaoDao.restore(requisicaoFuncaoDto);

        Boolean status = null;
        if (requisicaoFuncaoDto.getRequisicaoValida().equals("N")) {
            status = false;
        } else {
            if (requisicaoFuncaoDto.getRequisicaoValida().equals("S")) {
                status = true;
            }
        }
        return status;
    }

    public boolean descricaoAprovada() throws Exception {

        RequisicaoFuncaoDao requisicaoFuncaoDao = new RequisicaoFuncaoDao();
        setTransacaoDao(requisicaoFuncaoDao);
        SolicitacaoServicoDTO solicitacaoDto = getSolicitacaoServicoDto();
        RequisicaoFuncaoDTO requisicaoFuncaoDto = new RequisicaoFuncaoDTO();
        requisicaoFuncaoDto.setIdSolicitacaoServico(solicitacaoDto.getIdSolicitacaoServico());
        requisicaoFuncaoDto = (RequisicaoFuncaoDTO) requisicaoFuncaoDao.restore(requisicaoFuncaoDto);

        Boolean status = null;
        if (requisicaoFuncaoDto.getDescricaoValida().equals("N")) {
            status = false;
        } else {
            if (requisicaoFuncaoDto.getDescricaoValida().equals("S")) {
                status = true;
            }
        }
        return status;
    }

}
