/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.InformacoesContratoConfigDTO;
import br.com.centralit.citcorpore.bean.InformacoesContratoPerfSegDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

public class InformacoesContratoPerfSegDao extends CrudDaoDefaultImpl {


	public InformacoesContratoPerfSegDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}
	
	public Collection find(IDto obj) throws PersistenceException
	{
	    List order =  new ArrayList();
	    order.add(new Order("idPerfilSeguranca", "ASC"));
	    return super.find(obj, order);
	}

	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();
		
		listFields.add(new Field("idInformacoesContratoConfig" ,"idInformacoesContratoConfig", true, false, false, true));
		listFields.add(new Field("idPerfilSeguranca" ,"idPerfilSeguranca", true, false, false, true));
		
		return listFields;
	}

	public String getTableName() {
		return "InformacoesContratoPerfSeg";
	}

	public Collection list() throws PersistenceException {
		return null;
	}
	
	public Class getBean() {
		return InformacoesContratoPerfSegDTO.class;
	}

	public Collection findByIdInformacoesContratoConfig(Integer idInformacoesContratoConfig) throws Exception
	{
		List lstCond = new ArrayList();
		List lstOrdem = new ArrayList();
		
		lstCond.add(new Condition("idInformacoesContratoConfig", "=", idInformacoesContratoConfig));
		
		lstOrdem.add(new Order("idPerfilSeguranca"));
	    return super.findByCondition(lstCond, lstOrdem);
	}
	
    public void deleteAllByIdInformacoesContratoConfig(
            InformacoesContratoConfigDTO contrato) throws PersistenceException {
    	List condicao = new ArrayList();
    	condicao.add(new Condition("idInformacoesContratoConfig", "=", contrato.getIdInformacoesContratoConfig()));
    	super.deleteByCondition(condicao);
    }
	
}
