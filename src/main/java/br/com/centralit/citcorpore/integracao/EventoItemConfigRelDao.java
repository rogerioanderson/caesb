/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.EventoItemConfigRelDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.util.Constantes;

/**
 * DAO da tabela de relacionamento entre item de configura��o e evento
 * 
 * @author diego.rezende
 *
 */
public class EventoItemConfigRelDao extends CrudDaoDefaultImpl {

	public EventoItemConfigRelDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}
	
	@Override
	public Collection<?> find(IDto obj) throws PersistenceException {
		return null;
	}

	@Override
	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<Field>();
		listFields.add(new Field("IDEVENTO", "idEvento", true, false, false, false));
		listFields.add(new Field("IDITEMCONFIGURACAO", "idItemConfiguracao", true, false, false, false));
		return listFields;
	}

	@Override
	public String getTableName() {
		return "EVENTOITEMCONFIGURACAO";
	}

	@Override
	public Collection<?> list() throws PersistenceException {
		return null;
	}

	@Override
	public Class<?> getBean() {
		return EventoItemConfigRelDTO.class;
	}
	
	public void deleteByIdEvento(Integer idEvento) throws PersistenceException {
		List<Condition> lstCondicao = new ArrayList<Condition>();
		lstCondicao.add(new Condition(Condition.AND, "idEvento", "=", idEvento));
		super.deleteByCondition(lstCondicao);
	}

	@SuppressWarnings("unchecked")
	public Collection<EventoItemConfigRelDTO> listByEvento(Integer idEvento) throws PersistenceException {
		String sql = "SELECT idevento, iditemconfiguracao FROM "
				+ getTableName() +" WHERE idevento = ?";
		List<?> dados = this.execSQL(sql, new Object[] { idEvento });
		List<String> fields = new ArrayList<String>();
		fields.add("idEvento");
		fields.add("idItemConfiguracao");
		return this.listConvertion(getBean(), dados, fields);
	}

}
