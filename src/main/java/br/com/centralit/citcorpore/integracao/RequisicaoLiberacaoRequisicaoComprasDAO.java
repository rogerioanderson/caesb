/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.RequisicaoLiberacaoRequisicaoComprasDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.util.Constantes;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class RequisicaoLiberacaoRequisicaoComprasDAO extends CrudDaoDefaultImpl {
	public RequisicaoLiberacaoRequisicaoComprasDAO() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}
	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();
		listFields.add(new Field("idrequisicaoliberacaocompras" ,"idRequisicaoLiberacaoCompras", true, true, false, false));
		listFields.add(new Field("idRequisicaoLiberacao" ,"idRequisicaoLiberacao", false, false, false, false));
		listFields.add(new Field("idSolicitacaoServico" ,"idSolicitacaoServico", false, false, false, false));
		listFields.add(new Field("idItemRequisicaoProduto" ,"idItemRequisicaoProduto", false, false, false, false));
		listFields.add(new Field("dataFim" ,"dataFim", false, false, false, false));
		return listFields;

	}
	public String getTableName() {
		return this.getOwner() + "requisicaoliberacaoCompras";
	}
	public Collection list() throws PersistenceException {
		return null;
	}

	public Class getBean() {
		return RequisicaoLiberacaoRequisicaoComprasDTO.class;
	}
	public Collection find(IDto arg0) throws PersistenceException {
		return null;
	}
	
	
	public Collection findByIdLiberacao(Integer idRequisicaoLiberacao) throws Exception {
		List fields = new ArrayList(); 
		
		String sql = " select ReqCompras.idRequisicaoLiberacaocompras, ReqCompras.idRequisicaoLiberacao, ReqCompras.idSolicitacaoServico, serv.nomeServico, ss.situacao"+
				" from requisicaoliberacaocompras ReqCompras "+ 
				" inner join liberacao lib on ReqCompras.idRequisicaoLiberacao = lib.idLiberacao "+
				" inner join solicitacaoservico ss on ReqCompras.idSolicitacaoServico = ss.idSolicitacaoServico "+
				" inner join servicocontrato sc on ss.idservicocontrato = sc.idservicocontrato "+
				" inner join servico serv on serv.idservico = sc.idservico "+
				" where ReqCompras.idRequisicaoLiberacao = ? ";
				
		
	  List resultado = 	execSQL(sql, new Object[]{idRequisicaoLiberacao	});
	 
	  fields.add("idRequisicaoLiberacaocompras");
	  fields.add("idRequisicaoLiberacao");
	  fields.add("idSolicitacaoServico");
	  fields.add("nomeServico");
	  fields.add("situacaoServicos");
	  
	  return listConvertion(getBean(), resultado,fields) ;
	}
	public Collection findByIdLiberacaoAndIdHistorico(Integer idRequisicaoLiberacao) throws Exception {
		List fields = new ArrayList(); 
		
		String sql = " select ReqCompras.idRequisicaoLiberacaocompras, ReqCompras.idRequisicaoLiberacao, "+
                 "ReqCompras.idSolicitacaoServico, serv.nomeServico, ss.situacao "+
				 "from requisicaoliberacaocompras ReqCompras "+
				 "inner join ligacaolibhistcompras lig on ReqCompras.idrequisicaoliberacaocompras = lig.idrequisicaoliberacaocompras "+
				 "inner join solicitacaoservico ss on ReqCompras.idSolicitacaoServico = ss.idSolicitacaoServico "+
				 "inner join servicocontrato sc on ss.idservicocontrato = sc.idservicocontrato "+
				 "inner join servico serv on serv.idservico = sc.idservico "+
				 "where lig.idhistoricoliberacao = ? ";
		
		
		List resultado = 	execSQL(sql, new Object[]{idRequisicaoLiberacao	});
		
		fields.add("idRequisicaoLiberacaocompras");
		fields.add("idRequisicaoLiberacao");
		fields.add("idSolicitacaoServico");
		fields.add("nomeServico");
		fields.add("situacaoServicos");
		
		return listConvertion(getBean(), resultado,fields) ;
	}
	
	public Collection findByIdLiberacaoAndDataFim(Integer idRequisicaoLiberacao) throws Exception {
		StringBuilder montarSql = new StringBuilder();
		List fields = new ArrayList(); 
		
		montarSql.append("select reqCompras.idrequisicaoliberacaocompras, reqCompras.idRequisicaoLiberacao , itemReqCom.iditemrequisicaoproduto ,solserv.idsolicitacaoservico, itemReqCom.descricaoitem,  pro.nomeprojeto, centReult.codigocentroresultado ,centReult.nomecentroresultado ,itemReqCom.situacao from solicitacaoservico solserv ");
		montarSql.append(" inner join itemrequisicaoproduto itemReqCom on solserv.idsolicitacaoservico = itemReqCom.idsolicitacaoservico  ");
		montarSql.append(" inner join requisicaoproduto reqProd on solserv.idsolicitacaoservico  = reqProd.idsolicitacaoservico ");
		montarSql.append(" inner join centroresultado centReult on reqProd.idcentrocusto  = centReult.idcentroresultado  ");
		montarSql.append(" inner join projetos pro on reqProd.idprojeto = pro.idprojeto ");
		montarSql.append(" inner join requisicaoliberacaocompras reqCompras on itemReqCom.iditemrequisicaoproduto = reqCompras.iditemrequisicaoproduto ");
		montarSql.append("where reqCompras.idRequisicaoLiberacao = ? and reqCompras.dataFim is null ");
	
		List dados  = 	execSQL(montarSql.toString(), new Object[]{idRequisicaoLiberacao});
		
		fields.add("idRequisicaoLiberacaoCompras");
		fields.add("idRequisicaoLiberacao");
		fields.add("idItemRequisicaoProduto");
		fields.add("idSolicitacaoServico");
		fields.add("descricaoItem");
		fields.add("nomeProjeto");
		fields.add("codigoCentroResultado");
		fields.add("nomeCentroResultado");
		fields.add("situacaoItemRequisicao");
		return  listConvertion(getBean(), dados, fields);
	}
	
	public void deleteByIdRequisicaoLiberacao(Integer idRequisicaoLiberacao) throws ServiceException, Exception{
		ArrayList<Condition> condicoes = new ArrayList<Condition>();
		condicoes.add(new Condition("idRequisicaoLiberacao", "=", idRequisicaoLiberacao));
		
		super.deleteByCondition(condicoes);	
	}
	
	public RequisicaoLiberacaoRequisicaoComprasDTO carregaItemRequisicaoComprasByidItemRequisicaProduto(Integer idItemrRequisicaoProduto) throws Exception{
		StringBuilder montarSql = new StringBuilder();
		List fields = new ArrayList(); 
		
		montarSql.append("select itemReqCom.iditemrequisicaoproduto ,solserv.idsolicitacaoservico, itemReqCom.descricaoitem,  pro.nomeprojeto, centReult.codigocentroresultado ,centReult.nomecentroresultado ,itemReqCom.situacao from solicitacaoservico solserv ");
		montarSql.append(" inner join itemrequisicaoproduto itemReqCom on solserv.idsolicitacaoservico = itemReqCom.idsolicitacaoservico  ");
		montarSql.append(" inner join requisicaoproduto reqProd on solserv.idsolicitacaoservico  = reqProd.idsolicitacaoservico ");
		montarSql.append(" inner join centroresultado centReult on reqProd.idcentrocusto  = centReult.idcentroresultado  ");
		montarSql.append(" inner join projetos pro on reqProd.idprojeto = pro.idprojeto ");
		montarSql.append("where itemReqCom.iditemrequisicaoproduto = ? ");
	
		List dados  = 	execSQL(montarSql.toString(), new Object[]{idItemrRequisicaoProduto});
		
		fields.add("idItemRequisicaoProduto");
		fields.add("idSolicitacaoServico");
		fields.add("descricaoItem");
		fields.add("nomeProjeto");
		fields.add("codigoCentroResultado");
		fields.add("nomeCentroResultado");
		fields.add("situacaoItemRequisicao");
		
		List resultado  =  listConvertion(getBean(), dados, fields);
		
		return (RequisicaoLiberacaoRequisicaoComprasDTO) resultado.get(0);
	}
}
