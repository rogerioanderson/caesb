/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.CatalogoServicoDTO;
import br.com.centralit.citcorpore.bean.ServContratoCatalogoServDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.util.Constantes;

/**
 * 
 * @author pedro
 *
 */
public class ServContratoCatalogoServDao extends CrudDaoDefaultImpl {

	/**
     * 
     */
	private static final String SQL_DELETE = 
	          "DELETE FROM servcontratocatalogoserv WHERE idcatalogoservico = ? ";
	
	private static final String SQL_FIND = 
		      "SELECT * FROM servcontratocatalogoserv WHERE idcatalogoservico = ? ";
	
	public ServContratoCatalogoServDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();
		
		listFields.add(new Field("idServicoContrato", "idServicoContrato", false, false, false, false));
		listFields.add(new Field("idCatalogoServico", "idCatalogoServico", false, false, false, false));
		
		return listFields;
	}
	
	public String getTableName() {
		return "SERVCONTRATOCATALOGOSERV";
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection find(IDto obj) throws PersistenceException {
		List ordem = new ArrayList();

		return super.find(obj, ordem);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection list() throws PersistenceException {
		List list = new ArrayList();

		return super.list(list);
	}
	
	@SuppressWarnings("rawtypes")
	public Class getBean() {
		return ServContratoCatalogoServDTO.class;
	}
	
	public void deleteByIdServContratoCatalogo(CatalogoServicoDTO catalogo)
		    throws PersistenceException {
		        super.execUpdate(SQL_DELETE, new Object[]{catalogo.getIdCatalogoServico()});
		    }
	public Collection findByIdCatalogo(ServContratoCatalogoServDTO servContratoCatalogoServDTO) throws PersistenceException {
        return super.listConvertion(getBean(), 
                super.execSQL(SQL_FIND, new Object[]{servContratoCatalogoServDTO.getIdCatalogoServico()}), 
                new ArrayList(getFields()));
}

}
