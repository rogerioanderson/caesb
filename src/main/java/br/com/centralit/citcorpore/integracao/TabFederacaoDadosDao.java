/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.TabFederacaoDadosDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilStrings;

public class TabFederacaoDadosDao extends CrudDaoDefaultImpl {
	public TabFederacaoDadosDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}
	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();
		listFields.add(new Field("nomeTabela" ,"nomeTabela", true, false, false, false));
		listFields.add(new Field("chaveFinal" ,"chaveFinal", true, false, false, false));
		listFields.add(new Field("chaveOriginal" ,"chaveOriginal", true, false, false, false));
		listFields.add(new Field("origem" ,"origem", true, false, false, false));
		listFields.add(new Field("criacao" ,"criacao", false, false, false, false));
		listFields.add(new Field("ultAtualiz" ,"ultAtualiz", false, false, false, false));
		listFields.add(new Field("uuid" ,"uuid", false, false, false, false));		
		return listFields;
	}
	public Collection findByOrigemTabelaChaveOriginal(String origem, String nomeTab, String chaveOrig) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList(); 
		condicao.add(new Condition("origem", "=", origem)); 
		condicao.add(new Condition("nomeTabela", "=", nomeTab));
		condicao.add(new Condition("chaveOriginal", "=", chaveOrig));
		ordenacao.add(new Order("chaveFinal"));
		return super.findByCondition(condicao, ordenacao);
	}	
	public String getTableName() {
		return this.getOwner() + "TabFederacaoDados";
	}
	public Collection list() throws PersistenceException {
		return null;
	}

	public Class getBean() {
		return TabFederacaoDadosDTO.class;
	}
	public Collection find(IDto arg0) throws PersistenceException {
		return null;
	}
	public Collection findByNomeTabela(String parm) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList(); 
		condicao.add(new Condition("nomeTabela", "=", parm)); 
		ordenacao.add(new Order("chaveFinal"));
		return super.findByCondition(condicao, ordenacao);
	}
	public void deleteByNomeTabela(String parm) throws PersistenceException {
		List condicao = new ArrayList();
		condicao.add(new Condition("nomeTabela", "=", parm));
		super.deleteByCondition(condicao);
	}
	public Collection findByChaveFinal(String parm) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList(); 
		condicao.add(new Condition("chaveFinal", "=", parm)); 
		ordenacao.add(new Order("chaveOriginal"));
		return super.findByCondition(condicao, ordenacao);
	}
	public void deleteByChaveFinal(String parm) throws PersistenceException {
		List condicao = new ArrayList();
		condicao.add(new Condition("chaveFinal", "=", parm));
		super.deleteByCondition(condicao);
	}
	public Collection findByChaveOriginal(String parm) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList(); 
		condicao.add(new Condition("chaveOriginal", "=", parm)); 
		ordenacao.add(new Order("chaveFinal"));
		return super.findByCondition(condicao, ordenacao);
	}
	public void deleteByChaveOriginal(String parm) throws PersistenceException {
		List condicao = new ArrayList();
		condicao.add(new Condition("chaveOriginal", "=", parm));
		super.deleteByCondition(condicao);
	}
	public Collection findByOrigem(String parm) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList(); 
		condicao.add(new Condition("origem", "=", parm)); 
		ordenacao.add(new Order("chaveFinal"));
		return super.findByCondition(condicao, ordenacao);
	}
	public void deleteByOrigem(String parm) throws PersistenceException {
		List condicao = new ArrayList();
		condicao.add(new Condition("origem", "=", parm));
		super.deleteByCondition(condicao);
	}
	@Override
	public void updateNotNull(IDto obj) throws PersistenceException {
		super.updateNotNull(obj);
	}
	
	public Collection findByChaveOriginal(final String origem, final String tabela, String chaveOriginal) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList(); 
		condicao.add(new Condition("origem", "=", origem)); 
		condicao.add(new Condition("nomeTabela", "=", tabela)); 
		condicao.add(new Condition("chaveOriginal", "=", chaveOriginal)); 
		
		return (List<TabFederacaoDadosDTO>) super.findByCondition(condicao, ordenacao);
	}

	public TabFederacaoDadosDTO findByChaveFinal(final String origem, final String tabela, String chaveFinal) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList(); 
		condicao.add(new Condition("origem", "=", origem)); 
		condicao.add(new Condition("nomeTabela", "=", tabela)); 
		condicao.add(new Condition("chaveFinal", "=", chaveFinal)); 
		
		List<TabFederacaoDadosDTO> result = (List<TabFederacaoDadosDTO>) super.findByCondition(condicao, ordenacao);
		if (result != null && !result.isEmpty()) {
			return result.get(0);
		}else{
			return null;
		}
	}

	public Collection find(String origem, String nomeTab, String chaveOrig, String chaveFinal) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList(); 
		if (!UtilStrings.isNullOrEmpty(origem))
			condicao.add(new Condition("origem", "=", origem)); 
		if (!UtilStrings.isNullOrEmpty(nomeTab))
			condicao.add(new Condition("nomeTabela", "=", nomeTab));
		if (!UtilStrings.isNullOrEmpty(chaveOrig))
			condicao.add(new Condition("chaveOriginal", "=", chaveOrig));
		if (!UtilStrings.isNullOrEmpty(chaveFinal))
			condicao.add(new Condition("chaveFinal", "=", chaveFinal));
		ordenacao.add(new Order("chaveFinal"));
		return super.findByCondition(condicao, ordenacao);
	}
}
