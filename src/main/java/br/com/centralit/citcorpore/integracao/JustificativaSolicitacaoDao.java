/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.JustificativaSolicitacaoDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

/**
 * @author breno.guimaraes
 *
 */
public class JustificativaSolicitacaoDao extends CrudDaoDefaultImpl {

    public JustificativaSolicitacaoDao() {
        super(Constantes.getValue("DATABASE_ALIAS"), null);
    }

    @Override
    public Collection<JustificativaSolicitacaoDTO> find(final IDto justificativa) throws PersistenceException {
        return null;
    }

    @Override
    public Collection<Field> getFields() {
        final Collection<Field> listFields = new ArrayList<>();
        listFields.add(new Field("idjustificativa", "idJustificativa", true, true, false, false));
        listFields.add(new Field("descricaojustificativa", "descricaoJustificativa", false, false, false, false));
        listFields.add(new Field("suspensao", "suspensao", false, false, false, false));
        listFields.add(new Field("aprovacao", "aprovacao", false, false, false, false));
        listFields.add(new Field("situacao", "situacao", false, false, false, false));
        listFields.add(new Field("viagem", "viagem", false, false, false, false));
        listFields.add(new Field("deleted", "deleted", false, false, false, false));
        return listFields;
    }

    public Collection<JustificativaSolicitacaoDTO> listAtivasParaSuspensao() throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordenacao = new ArrayList<>();
        condicao.add(new Condition("suspensao", "=", "S"));
        condicao.add(new Condition("situacao", "=", "A"));
        condicao.add(new Condition("deleted", "<>", "Y"));
        condicao.add(new Condition(Condition.OR, "deleted", "is", null));
        ordenacao.add(new Order("descricaoJustificativa"));
        return super.findByCondition(condicao, ordenacao);
    }

    public Collection<JustificativaSolicitacaoDTO> listAtivasParaAprovacao() throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordenacao = new ArrayList<>();
        condicao.add(new Condition("aprovacao", "=", "S"));
        condicao.add(new Condition("situacao", "=", "A"));
        condicao.add(new Condition("deleted", "is", null));
        ordenacao.add(new Order("descricaoJustificativa"));
        return super.findByCondition(condicao, ordenacao);
    }

    @Override
    public String getTableName() {
        return "justificativasolicitacao";
    }

    @Override
    public Collection<JustificativaSolicitacaoDTO> list() throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordenacao = new ArrayList<>();
        ordenacao.add(new Order("descricaoJustificativa"));
        return super.findByCondition(condicao, ordenacao);
    }

    /**
     * Retorna uma lista de justificativas ativas para requisicao viagem
     *
     * @return
     * @throws Exception
     * @author thays.araujo
     */
    public Collection<JustificativaSolicitacaoDTO> listAtivasParaViagem() throws PersistenceException {
        final List<String> condicao = new ArrayList<>();
        final List<String> listaRetorno = new ArrayList<>();

        final String sql = "select idjustificativa, descricaoJustificativa from justificativasolicitacao where suspensao = ? and viagem = ? and situacao = ? and (deleted <> ? or deleted is null) ";
        condicao.add("N");
        condicao.add("S");
        condicao.add("A");
        condicao.add("Y");

        final List<?> lista = this.execSQL(sql, condicao.toArray());

        listaRetorno.add("idJustificativa");
        listaRetorno.add("descricaoJustificativa");

        if (lista != null && !lista.isEmpty()) {
            return this.listConvertion(this.getBean(), lista, listaRetorno);
        }
        return null;
    }

    @Override
    public Class<JustificativaSolicitacaoDTO> getBean() {
        return JustificativaSolicitacaoDTO.class;
    }

}
