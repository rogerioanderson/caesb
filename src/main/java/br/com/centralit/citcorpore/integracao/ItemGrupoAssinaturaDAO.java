/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.ItemGrupoAssinaturaDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class ItemGrupoAssinaturaDAO extends CrudDaoDefaultImpl {

    public ItemGrupoAssinaturaDAO() {
	super(Constantes.getValue("DATABASE_ALIAS"), null);
    }

    @Override
    public Collection find(IDto obj) throws PersistenceException {
	List order =  new ArrayList();
	order.add(new Order("idItemGrupoAssinatura", "ASC"));
	return super.find(obj, order);
    }

    @Override
    public Collection<Field> getFields() {
	Collection<Field> listFields = new ArrayList<>();
	listFields.add(new Field("iditemgrupoassinatura", "idItemGrupoAssinatura", true, true, false, false));
	listFields.add(new Field("idgrupoassinatura", "idGrupoAssinatura", false, false, false, false));
	listFields.add(new Field("idassinatura", "idAssinatura", false, false, false, false));
	listFields.add(new Field("ordem", "ordem", false, false, false, false));
	listFields.add(new Field("datainicio", "dataInicio", false, false, false, false));
	listFields.add(new Field("datafim", "dataFim", false, false, false, false));
	return listFields;
    }

    @Override
    public String getTableName() {
	return "itemgrupoassinatura";
    }

    @Override
    public Collection list() throws PersistenceException {
	return super.list("ordem");
    }

    @Override
    public Class getBean() {
	return ItemGrupoAssinaturaDTO.class;
    }

    public Collection findByIdGrupoAssinatura(Integer idGrupoAssinatura) {
	List result;
	try {
	    List resp = new ArrayList();
	    List parametro = new ArrayList();
	    List listRetorno = new ArrayList();

	    listRetorno.add("idItemGrupoAssinatura");
	    listRetorno.add("idGrupoAssinatura");
	    listRetorno.add("idAssinatura");
	    listRetorno.add("ordem");
	    listRetorno.add("dataInicio");
	    listRetorno.add("dataFim");
	    listRetorno.add("nomeResponsavel");
	    listRetorno.add("papel");
	    listRetorno.add("fase");

	    String sql = "SELECT itemgrupoassinatura.iditemgrupoassinatura,itemgrupoassinatura.idgrupoassinatura,itemgrupoassinatura.idassinatura,itemgrupoassinatura.ordem,itemgrupoassinatura.datainicio,itemgrupoassinatura.datafim, empregados.nome as nomeresponsavel, assinatura.papel, assinatura.fase " + "FROM itemgrupoassinatura JOIN assinatura on  (itemgrupoassinatura.datafim is null) and itemgrupoassinatura.idgrupoassinatura = ? and itemgrupoassinatura.idassinatura=assinatura.idassinatura "
		    + " LEFT JOIN empregados on assinatura.idempregado = empregados.idempregado " + "ORDER BY itemgrupoassinatura.idgrupoassinatura,itemgrupoassinatura.ordem,itemgrupoassinatura.idassinatura";
	    parametro.add(idGrupoAssinatura);

	    resp = this.execSQL(sql, parametro.toArray());

	    result = this.engine.listConvertion(this.getBean(), resp,
		    listRetorno);
	} catch (PersistenceException e) {
	    e.printStackTrace();
	    result = null;
	} catch (Exception e) {
	    e.printStackTrace();
	    result = null;
	}
	return (((result == null) || (result.size() <= 0)) ? new ArrayList<ItemGrupoAssinaturaDTO>() : result);
    }

    public Collection findByIdAssinatura(Integer idAssinatura) {
	List result;
	try {
	    List resp = new ArrayList();
	    List parametro = new ArrayList();
	    List listRetorno = new ArrayList();

	    listRetorno.add("idItemGrupoAssinatura");
	    listRetorno.add("idGrupoAssinatura");
	    listRetorno.add("idAssinatura");
	    listRetorno.add("ordem");
	    listRetorno.add("dataInicio");
	    listRetorno.add("dataFim");
	    listRetorno.add("nomeResponsavel");
	    listRetorno.add("papel");
	    listRetorno.add("fase");

	    String sql = "SELECT itemgrupoassinatura.iditemgrupoassinatura,itemgrupoassinatura.idgrupoassinatura,itemgrupoassinatura.idassinatura,itemgrupoassinatura.ordem,itemgrupoassinatura.datainicio,itemgrupoassinatura.datafim, empregados.nome as nomeresponsavel, assinatura.papel, assinatura.fase " + "FROM itemgrupoassinatura JOIN assinatura on  (itemgrupoassinatura.datafim is null) and itemgrupoassinatura.idassinatura = ? and itemgrupoassinatura.idassinatura=assinatura.idassinatura "
		    + " LEFT JOIN empregados on assinatura.idempregado = empregados.idempregado " + "ORDER BY itemgrupoassinatura.idgrupoassinatura,itemgrupoassinatura.ordem,itemgrupoassinatura.idassinatura";
	    parametro.add(idAssinatura);

	    resp = this.execSQL(sql, parametro.toArray());

	    result = this.engine.listConvertion(this.getBean(), resp, listRetorno);
	} catch (PersistenceException e) {
	    e.printStackTrace();
	    result = null;
	} catch (Exception e) {
	    e.printStackTrace();
	    result = null;
	}
	return (((result == null) || (result.size() <= 0)) ? new ArrayList<ItemGrupoAssinaturaDTO>() : result);
    }

}
