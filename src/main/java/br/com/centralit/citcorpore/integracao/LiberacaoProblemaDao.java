/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.LiberacaoProblemaDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class LiberacaoProblemaDao extends CrudDaoDefaultImpl {

	public LiberacaoProblemaDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}
	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();
		listFields.add(new Field("idLiberacao" ,"idLiberacao", false, false, false, false));
		listFields.add(new Field("idProblema" ,"idProblema", true, false, false, false));
		listFields.add(new Field("idhistoricoliberacao" ,"idHistoricoLiberacao", false, false, false, false));
		return listFields;
	}
	public String getTableName() {
		return this.getOwner() + "LiberacaoProblema";
	}
	public Collection list() throws PersistenceException {
		return null;
	}

	public Class getBean() {
		return LiberacaoProblemaDTO.class;
	}
	public Collection find(IDto arg0) throws PersistenceException {
		return null;
	}

	public Collection findByIdLiberacao(Integer idLiberacao) throws Exception {
		List list;
		List parametro = new ArrayList();
		List fields = new ArrayList();
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT distinct lp.idproblema as idproblema, titulo, status FROM problema pro JOIN liberacaoproblema lp ON pro.idproblema = lp.idproblema WHERE lp.idliberacao = ? ORDER BY lp.idproblema");
		parametro.add(idLiberacao);
		list = this.execSQL(sql.toString(), parametro.toArray());
		fields.add("idProblema");
		fields.add("titulo");
		fields.add("status");
		if (list != null && !list.isEmpty()) {
			return (List<LiberacaoProblemaDTO>) this.listConvertion(getBean(), list, fields);
		} else {
			return null;
	}
	}
	
	public void deleteByIdLiberacao(Integer parm) throws Exception {
		List condicao = new ArrayList();
		condicao.add(new Condition("idLiberacao", "=", parm));
		super.deleteByCondition(condicao);
	}

	public ArrayList<LiberacaoProblemaDTO> listByIdRequisicaoLiberacao(Integer idrequisicaoliberacao) throws ServiceException, Exception {
		ArrayList<Condition> condicoes = new ArrayList<Condition>();

		condicoes.add(new Condition("idLiberacao", "=", idrequisicaoliberacao));

		return (ArrayList<LiberacaoProblemaDTO>) super.findByCondition(condicoes, null);
	}
	public ArrayList<LiberacaoProblemaDTO> listByIdHistorico(Integer idHistoricoLiberacao) throws ServiceException, Exception {
		ArrayList<Condition> condicoes = new ArrayList<Condition>();

		condicoes.add(new Condition("idHistoricoLiberacao", "=", idHistoricoLiberacao));

		return (ArrayList<LiberacaoProblemaDTO>) super.findByCondition(condicoes, null);
	}
}
