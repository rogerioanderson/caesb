/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.DadosBancariosIntegranteDTO;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

@SuppressWarnings({"rawtypes","unchecked"})
public class DadosBancariosIntegranteDAO extends CrudDaoDefaultImpl{

private static final long serialVersionUID = 1L;
	
	public DadosBancariosIntegranteDAO(){
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}
	
	@Override
	public String getTableName() {
		return this.getOwner() + "dadosbancariosintegrante";
	}

	@Override
	public Class getBean() {
		return DadosBancariosIntegranteDTO.class;
	}

	@Override
	public Collection getFields() {
		Collection listFields = new ArrayList();
		
		listFields.add(new Field("iddadosbancarios" ,"idDadosBancarios", true, true, false, false));
		listFields.add(new Field("idintegrante" ,"idIntegrante", false, false, false, false));
		listFields.add(new Field("banco" ,"banco", false, false, false, false));
		listFields.add(new Field("agencia" ,"agencia", false, false, false, false));
        listFields.add(new Field("conta" ,"conta", false, false, false, false));
		listFields.add(new Field("operacao" ,"operacao", false, false, false, false));
		listFields.add(new Field("cpf" ,"cpf", false, false, false, false));
		
		return listFields;
	}
	
	public DadosBancariosIntegranteDTO findByIdIntegrante(Integer idIntegrante) throws Exception{
	
		List result = new ArrayList<DadosBancariosIntegranteDTO>();
	
		List condicao = new ArrayList();
		List ordenacao = new ArrayList(); 
		condicao.add(new Condition("idIntegrante", "=", idIntegrante)); 
		ordenacao.add(new Order("idDadosBancarios"));
		
		result = (List) super.findByCondition(condicao, ordenacao);
		
		if(result != null && !result.isEmpty())
			return (DadosBancariosIntegranteDTO) result.get(0);
		else
			return null;
	}

}
