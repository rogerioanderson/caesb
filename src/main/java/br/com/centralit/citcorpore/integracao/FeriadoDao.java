/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.FeriadoDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;


public class FeriadoDao extends CrudDaoDefaultImpl {

	

	public FeriadoDao()
	{
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}
	
	public Collection find(IDto arg0) throws PersistenceException
	{
		return null;
	}
	
	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();
		listFields.add(new Field("idFeriado", "idFeriado", true, true, false, false));
		listFields.add(new Field("data", "data", false, false, false, false));
        listFields.add(new Field("descricao", "descricao", false, false, false, false));
        listFields.add(new Field("abrangencia", "abrangencia", false, false, false, false));
        listFields.add(new Field("idUf", "idUf", false, false, false, false));
        listFields.add(new Field("idCidade", "idCidade", false, false, false, false));
        listFields.add(new Field("recorrente", "recorrente", false, false, false, false));

		return listFields;
	}

	public String getTableName() {
		
		return "Feriado";
	}
	
	public Class getBean()
	{
		return FeriadoDTO.class;
	}
	
    public Collection list() throws PersistenceException
    {
        return super.list("data");
    }

    public boolean isFeriado(Date data, Integer idCidade, Integer idUf) throws Exception {
        String SQL = "SELECT data FROM Feriado "
            +" WHERE (data = ? AND abrangencia IN ('N','I')) "
            +"    OR (data = ? AND abrangencia IN ('N','I')) "
            +"    OR (data = ? AND ((? <> 0 AND idUf = ?) OR (? <> 0 AND idCidade = ?)))"
            +"    OR (data = ? AND ((? <> 0 AND idUf = ?) OR (? <> 0 AND idCidade = ?)))";

		String dataStr = UtilDatas.dateToSTR(data);
		dataStr = dataStr.substring(0, 6) + "1900";
		Date dataAux = UtilDatas.strToSQLDate(dataStr);
		
		Integer idCidadeParm = 0;
		if (idCidade != null)
		    idCidadeParm = idCidade;
		Integer idUfParm = 0;
		if (idUf != null)
		    idUfParm = idUf;
		
		Object[] objs = new Object[] {dataAux, data, dataAux, idUfParm, idUfParm, idCidadeParm, idCidadeParm, data, idUfParm, idUfParm, idCidadeParm, idCidadeParm};
		
		List lista = this.execSQL(SQL, objs);
		
		List listRetorno = new ArrayList();
		listRetorno.add("data");
		List result = this.engine.listConvertion(getBean(), lista, listRetorno);
		return result != null && result.size() > 0;
    }
        
}
