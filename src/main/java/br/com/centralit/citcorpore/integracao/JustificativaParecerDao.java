/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.JustificativaParecerDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

/**
 * @author breno.guimaraes
 *
 */
public class JustificativaParecerDao extends CrudDaoDefaultImpl {

    public JustificativaParecerDao() {
        super(Constantes.getValue("DATABASE_ALIAS"), null);
    }

    @Override
    public Collection<JustificativaParecerDTO> find(final IDto justificativa) throws PersistenceException {
        return null;
    }

    @Override
    public Collection<Field> getFields() {
        final Collection<Field> listFields = new ArrayList<>();
        listFields.add(new Field("idjustificativa", "idJustificativa", true, true, false, false));
        listFields.add(new Field("descricaojustificativa", "descricaoJustificativa", false, false, false, false));
        listFields.add(new Field("aplicavelRequisicao", "aplicavelRequisicao", false, false, false, false));
        listFields.add(new Field("aplicavelCotacao", "aplicavelCotacao", false, false, false, false));
        listFields.add(new Field("aplicavelInspecao", "aplicavelInspecao", false, false, false, false));
        listFields.add(new Field("situacao", "situacao", false, false, false, false));
        listFields.add(new Field("DATAINICIO", "dataInicio", false, false, false, false));
        listFields.add(new Field("DATAFIM", "dataFim", false, false, false, false));
        return listFields;
    }

    @Override
    public String getTableName() {
        return "justificativaparecer";
    }

    @Override
    public Collection<JustificativaParecerDTO> list() throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordenacao = new ArrayList<>();
        ordenacao.add(new Order("descricaoJustificativa"));
        return super.findByCondition(condicao, ordenacao);
    }

    @Override
    public Class<JustificativaParecerDTO> getBean() {
        return JustificativaParecerDTO.class;
    }

    public Collection<JustificativaParecerDTO> listAplicaveisCotacao() throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordenacao = new ArrayList<>();
        condicao.add(new Condition("aplicavelCotacao", "=", "S"));
        ordenacao.add(new Order("descricaoJustificativa"));
        return super.findByCondition(condicao, ordenacao);
    }

    public Collection<JustificativaParecerDTO> listAplicaveisRequisicao() throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordenacao = new ArrayList<>();
        condicao.add(new Condition("aplicavelRequisicao", "=", "S"));
        ordenacao.add(new Order("descricaoJustificativa"));
        return super.findByCondition(condicao, ordenacao);
    }

    public Collection<JustificativaParecerDTO> listAplicaveisInspecao() throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordenacao = new ArrayList<>();
        condicao.add(new Condition("aplicavelInspecao", "=", "S"));
        ordenacao.add(new Order("descricaoJustificativa"));
        return super.findByCondition(condicao, ordenacao);
    }

    public boolean consultarJustificativaAtiva(final JustificativaParecerDTO justificativaParecerDto) throws PersistenceException {
        final List<Object> parametro = new ArrayList<>();
        List<?> list = new ArrayList<>();
        final StringBuilder sql = new StringBuilder();
        sql.append("select idjustificativa from ");
        sql.append(this.getTableName());
        sql.append(" where descricaojustificativa = ? and situacao = 'A'");

        parametro.add(justificativaParecerDto.getDescricaoJustificativa());

        if (justificativaParecerDto.getIdJustificativa() != null) {
            sql.append("and idjustificativa <> ?");
            parametro.add(justificativaParecerDto.getIdJustificativa());
        }

        list = this.execSQL(sql.toString(), parametro.toArray());
        if (list != null && !list.isEmpty()) {
            return true;
        }
        return false;
    }

    public Collection<JustificativaParecerDTO> listAplicaveisRequisicaoViagem() throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordenacao = new ArrayList<>();
        condicao.add(new Condition("aplicavelRequisicao", "=", "S"));
        condicao.add(new Condition("viagem", "=", "S"));
        ordenacao.add(new Order("descricaoJustificativa"));
        return super.findByCondition(condicao, ordenacao);
    }

}
