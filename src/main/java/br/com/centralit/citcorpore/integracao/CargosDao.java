/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.CargosDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class CargosDao extends CrudDaoDefaultImpl {

	public CargosDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Collection find(IDto obj) throws PersistenceException {

		return null;
	}
	
	private static final String SQL_NOME = " select idcargo, nomecargo from cargos " +
			" where upper(nomecargo) like upper(?)";

	@Override
	public Collection<Field> getFields() {

		Collection<Field> listFields = new ArrayList<>();

		listFields.add(new Field("IDCARGO", "idCargo", true, true, false, false));
		listFields.add(new Field("NOMECARGO", "nomeCargo", false, false, false, false));
		listFields.add(new Field("DATAINICIO", "dataInicio", false, false, false, false));
		listFields.add(new Field("DATAFIM", "dataFim", false, false, false, false));
		listFields.add(new Field("IDDESCRICAOCARGO", "idDescricaoCargo", false, false, false, false));

		return listFields;
	}

	@Override
	public String getTableName() {
		return "CARGOS";
	}

	@Override
	public Collection list() throws PersistenceException {
		List list = new ArrayList();
		list.add(new Order("nomeCargo"));
		return super.list(list);
	}

	@Override
	public Class getBean() {
		return CargosDTO.class;
	}

	/**
	 * Retorna lista de status de usu�rio.
	 * 
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public boolean consultarCargosAtivos(CargosDTO obj) throws PersistenceException {
		List parametro = new ArrayList();
		List list = new ArrayList();
		String sql = "select idcargo From " + getTableName() + "  where  nomecargo = ?   and dataFim is null ";
		
		if(obj.getIdCargo() != null){
			sql+=" and idcargo <> "+ obj.getIdCargo();
		}
		
		parametro.add(obj.getNomeCargo());
		list = this.execSQL(sql, parametro.toArray());
		if (list != null && !list.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validaInsert(CargosDTO obj){
		
		
		return false;
		
	}

	public Collection findByNomeCargos(CargosDTO cargosDTO) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList();

		condicao.add(new Condition("nomeCargo", "=", cargosDTO.getNomeCargo())); 
		ordenacao.add(new Order("nomeCargo"));
		condicao.add(new Condition(Condition.AND, "dataFim", "is", null));
		return super.findByCondition(condicao, ordenacao);
	}
	
	public Collection<CargosDTO>  seCargoJaCadastrado(CargosDTO cargosDTO) throws PersistenceException {
		List parametro = new ArrayList();
		List list = new ArrayList();
		String sql = "";
		sql = " select lower(nomecargo) from cargos where nomecargo = lower(?) ";

		parametro.add(cargosDTO.getNomeCargo().trim().toLowerCase());
		list = this.execSQL(sql, parametro.toArray());
		return list;
	}
	
	public Collection<CargosDTO> listarAtivos() throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList();

		ordenacao.add(new Order("nomeCargo"));
		condicao.add(new Condition("dataFim", "is", null));
		return super.findByCondition(condicao, ordenacao);
	}
	
	public Collection findByNome(String nome) throws PersistenceException {
		if(nome == null)
			nome = "";
		String text = nome.trim().replaceAll(" ", "");
		text = Normalizer.normalize(text, Normalizer.Form.NFD);
		text = text.replaceAll("[^\\p{ASCII}]", "");
		text = text.replaceAll("�������������������������Ǵ`^''-+=", "aaaaeeiooouucAAAAEEIOOOUUC ");
		nome = text;		
		nome = "%"+nome.toUpperCase()+"%";
		Object[] objs = new Object[] {nome}; 
	    List list = this.execSQL(SQL_NOME, objs);
	  
	    List listRetorno = new ArrayList();
	    listRetorno.add("idCargo");
		listRetorno.add("nomeCargo");

		
		List result = this.engine.listConvertion(getBean(), list, listRetorno);
		
		return result;
	}
	
}
