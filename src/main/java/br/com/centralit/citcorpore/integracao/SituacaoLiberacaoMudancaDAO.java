/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.SituacaoLiberacaoMudancaDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;


/**
 * 
 * @author geber.costa
 *
 */

@SuppressWarnings({ "rawtypes", "unchecked" })
public class SituacaoLiberacaoMudancaDAO extends  CrudDaoDefaultImpl {

	public SituacaoLiberacaoMudancaDAO() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}

	@Override
	public Collection find(IDto obj) throws PersistenceException {

		return null;
	}

	@Override
	public Collection<Field> getFields() {

		Collection<Field> listFields = new ArrayList<>();
		listFields.add(new Field("idSituacaoLiberacaoMudanca", "idSituacaoLiberacaoMudanca", true, true, false, false));
		listFields.add(new Field("situacao", "situacao", false, false, false, false));

		return listFields;
	}

	@Override
	public String getTableName() {
		return "situacaoliberacaomudanca";
	}

	@Override
	public Collection list() throws PersistenceException {
		List list = new ArrayList();
		list.add(new Order("situacao"));
		return super.list(list);
	}

	@Override
	public Class getBean() {
		return SituacaoLiberacaoMudancaDTO.class;
	}

	/**
	 * Retorna lista de status de usu�rio.
	 * 
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public boolean consultarSituacoes(SituacaoLiberacaoMudancaDTO situacaoDto) throws Exception {
		List parametro = new ArrayList();
		List list = new ArrayList();
		String sql = "select idsituacaoliberacaomudanca from " + getTableName() + "  where  situacao = ? ";
		
		if(situacaoDto.getIdSituacaoLiberacaoMudanca()!= null){
			sql+=" and idsituacaoliberacaomudanca <> "+ situacaoDto.getIdSituacaoLiberacaoMudanca();
		}
		
		parametro.add(situacaoDto.getSituacao());
		list = this.execSQL(sql, parametro.toArray());
		if (list != null && !list.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validaInsert(SituacaoLiberacaoMudancaDTO obj){
		
		return false;
		
	}

	public Collection findByNomeSituacao(SituacaoLiberacaoMudancaDTO situacaoDto) throws Exception {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList();

		condicao.add(new Condition("situacao", "=", situacaoDto.getSituacao())); 
		ordenacao.add(new Order("situacao"));
		return super.findByCondition(condicao, ordenacao);
	}
	
	//geber.costa 
		public Collection<SituacaoLiberacaoMudancaDTO> listAll() throws ServiceException, Exception {
			
			StringBuilder sb = new StringBuilder();
			List listRetorno = new ArrayList();

			sb.append("SELECT situacao ");
			sb.append("FROM situacaoliberacaomudanca ");
			sb.append("WHERE situacao is not null ");
			
			List lista = this.execSQL(sb.toString(), null);
			listRetorno.add("situacao");
			
			List listaSolicitacoes = this.engine.listConvertion(getBean(), lista, listRetorno);

			return listaSolicitacoes;
			
		}
}
