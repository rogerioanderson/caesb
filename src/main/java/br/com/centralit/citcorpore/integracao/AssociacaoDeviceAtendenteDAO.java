/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.AssociacaoDeviceAtendenteDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

/**
 * DAO para persist�ncia de {@link AssociacaoDeviceAtendenteDTO}
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 15/11/2014
 */
public class AssociacaoDeviceAtendenteDAO extends CrudDaoDefaultImpl {

    public AssociacaoDeviceAtendenteDAO() {
        super(Constantes.getValue("DATABASE_ALIAS"), null);
    }

    @Override
    public Collection<AssociacaoDeviceAtendenteDTO> find(final IDto obj) throws PersistenceException {
        return null;
    }

    @Override
    public Collection<AssociacaoDeviceAtendenteDTO> list() throws PersistenceException {
        final List<Order> ordenacao = new ArrayList<>();
        ordenacao.add(new Order("id"));
        ordenacao.add(new Order("idusuario"));
        return super.list(ordenacao);
    }

    @Override
    public Collection<Field> getFields() {
        final List<Field> fields = new ArrayList<>();
        fields.add(new Field("id", "id", true, true, false, true));
        fields.add(new Field("idusuario", "idUsuario", false, false, false, false));
        fields.add(new Field("token", "token", false, false, false, false));
        fields.add(new Field("connection", "connection", false, false, false, false));
        fields.add(new Field("deviceplatform", "devicePlatform", false, false, false, false));
        fields.add(new Field("active", "active", false, false, false, false));
        return fields;
    }

    /**
     * Lista {@link AssociacaoDeviceAtendenteDTO} ativos de mesmo token, conex�o, device e usu�rio
     *
     * @param associacao
     *            {@link AssociacaoDeviceAtendenteDTO} contendo os dados
     * @return {@link AssociacaoDeviceAtendenteDTO} dever� ser apenas um
     * @throws Exception
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @date 17/11/2014
     */
    public List<AssociacaoDeviceAtendenteDTO> listActiveWithSameProperties(final AssociacaoDeviceAtendenteDTO associacao) throws PersistenceException {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT asso.id, ");
        sql.append("       asso.idusuario, ");
        sql.append("       asso.connection, ");
        sql.append("       asso.token, ");
        sql.append("       asso.devicePlatform, ");
        sql.append("       emp.nome AS nomeAtendente ");
        sql.append("FROM   associacaodeviceatendente AS asso ");
        sql.append("       LEFT JOIN usuario usu ");
        sql.append("              ON usu.idusuario = asso.idusuario ");
        sql.append("       LEFT JOIN empregados emp ");
        sql.append("              ON emp.idempregado = usu.idempregado ");
        sql.append("WHERE  asso.idusuario = ? ");
        sql.append("       AND asso.token = ? ");
        sql.append("       AND UPPER(asso.connection) = UPPER(?) ");
        sql.append("       AND asso.active = ?");

        final Object[] params = new Object[] {associacao.getIdUsuario(), associacao.getToken(), associacao.getConnection(), 1};
        final List<?> result = this.execSQL(sql.toString(), params);
        List<AssociacaoDeviceAtendenteDTO> associacoes = new ArrayList<>();
        if (result.size() > 0) {
            final List<String> fields = new ArrayList<>();
            fields.add("id");
            fields.add("idUsuario");
            fields.add("connection");
            fields.add("token");
            fields.add("devicePlatform");
            fields.add("nomeAtendente");
            associacoes = this.listConvertion(this.getBean(), result, fields);
        }

        return associacoes;
    }

    /**
     * Lista {@link AssociacaoDeviceAtendenteDTO} ativas de acordo com o usu�rio informado
     *
     * @param usuario
     *            usu�rio para o qual ser�o listadas as associa��es
     * @param connection
     *            "conex�o" no mobile, que � a URI acessada
     * @return lista de {@link AssociacaoDeviceAtendenteDTO} ativas do usu�rio
     * @throws Exception
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @date 17/11/2014
     */
    public List<AssociacaoDeviceAtendenteDTO> listActiveAssociationForUser(final UsuarioDTO usuario, final String connection) throws PersistenceException {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT asso.id, ");
        sql.append("       asso.idusuario, ");
        sql.append("       asso.token, ");
        sql.append("       asso.connection, ");
        sql.append("       asso.devicePlatform, ");
        sql.append("       asso.active ");
        sql.append("FROM   associacaodeviceatendente AS asso ");
        sql.append("WHERE  asso.idusuario = ? ");
        sql.append("       AND UPPER(asso.connection) = UPPER(?) ");
        sql.append("       AND asso.active = ?");

        final Object[] params = new Object[] {usuario.getIdUsuario(), connection, 1};
        final List<?> result = this.execSQL(sql.toString(), params);
        List<AssociacaoDeviceAtendenteDTO> associacoes = new ArrayList<>();
        if (result.size() > 0) {
            associacoes = this.listConvertion(this.getBean(), result, (List<Field>) this.getFields());
        }

        return associacoes;
    }

    @Override
    public String getTableName() {
        return "associacaodeviceatendente";
    }

    @Override
    public Class<AssociacaoDeviceAtendenteDTO> getBean() {
        return AssociacaoDeviceAtendenteDTO.class;
    }

}
