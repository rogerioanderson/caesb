/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.RequisicaoMudancaServicoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class RequisicaoMudancaServicoDao extends CrudDaoDefaultImpl {

	public RequisicaoMudancaServicoDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}

	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();
		listFields.add(new Field("idrequisicaomudancaservico", "idRequisicaoMudancaServico", true, true, false, false));
		listFields.add(new Field("idrequisicaomudanca", "idRequisicaoMudanca", false, false, false, false));
		listFields.add(new Field("idservico", "idServico", false, false, false, false));
		listFields.add(new Field("dataFim", "dataFim", false, false, false, false));

		return listFields;
	}

	public String getTableName() {
		return this.getOwner() + "requisicaomudancaservico";
	}

	public Class getBean() {
		return RequisicaoMudancaServicoDTO.class;
	}

	public Collection find(IDto arg0) throws PersistenceException {
		return super.find(arg0, null);
	}

	@Override
	public void updateNotNull(IDto obj) throws PersistenceException {
		super.updateNotNull(obj);
	}

	@Override
	public Collection list() throws PersistenceException {
		return super.list("idrequisicaomudancaservico");
	}

	@Override
	public int deleteByCondition(List condicao) throws PersistenceException {
		return super.deleteByCondition(condicao);
	}

	public ArrayList<RequisicaoMudancaServicoDTO> listByIdRequisicaoMudanca(Integer idRequisicaoMudanca) throws ServiceException, Exception {
		ArrayList<Condition> condicoes = new ArrayList<Condition>();

		condicoes.add(new Condition("idRequisicaoMudanca", "=", idRequisicaoMudanca));

		return (ArrayList<RequisicaoMudancaServicoDTO>) super.findByCondition(condicoes, null);
	}

	/**
	 * Retorna o item de relacionamento espec�fico sem a chave prim�ria da tabela.
	 * Uma esp�cie de consulta por chave composta.
	 *
	 * @param dto
	 * @return
	 * @throws Exception
	 * @throws ServiceException
	 */
	public RequisicaoMudancaServicoDTO restoreByChaveComposta(RequisicaoMudancaServicoDTO dto) throws ServiceException, Exception{
		ArrayList<Condition> condicoes = new ArrayList<Condition>();

		condicoes.add(new Condition("idRequisicaoMudanca", "=", dto.getIdRequisicaoMudanca()));
		condicoes.add(new Condition("idServico", "=", dto.getIdServico()));

		ArrayList<RequisicaoMudancaServicoDTO> retorno = (ArrayList<RequisicaoMudancaServicoDTO>) super.findByCondition(condicoes, null);
		if(retorno != null){
			return retorno.get(0);
		}
		return null;
	}

	public void deleteByIdRequisicaoMudanca(Integer idRequisicaoMudanca) throws ServiceException, Exception{
		ArrayList<Condition> condicoes = new ArrayList<Condition>();

		condicoes.add(new Condition("idRequisicaoMudanca", "=", idRequisicaoMudanca));

		super.deleteByCondition(condicoes);
	}


	public Collection findByIdMudancaEDataFim(Integer idRequisicaoMudanca) throws Exception {
		List fields = new ArrayList();


		String sql = " select idrequisicaomudancaservico,idrequisicaomudanca,idservico,datafim from requisicaomudancaservico where  idrequisicaomudanca = ? and datafim is null";

	  List resultado = 	execSQL(sql, new Object[]{idRequisicaoMudanca});



	  fields.add("idRequisicaoMudancaServico");
	  fields.add("idRequisicaoMudanca");
	  fields.add("idServico");
	  fields.add("dataFim");

	  return listConvertion(getBean(), resultado,fields) ;
	}
	
	public Collection findByIdMudanca(Integer idRequisicaoMudanca) {

		List result;
		try {
			List resp = new ArrayList();
			List parametro = new ArrayList();
			List listRetorno = new ArrayList();

			listRetorno.add("idRequisicaoMudancaServico");
			listRetorno.add("idRequisicaoMudanca");
			listRetorno.add("idServico");
			listRetorno.add("dataFim");
			listRetorno.add("nome");
			listRetorno.add("descricao");

			StringBuilder sql = new StringBuilder();
			sql.append("select req.idrequisicaomudancaservico,req.idrequisicaomudanca,req.idservico,req.datafim,ser.nomeservico nome, ser.detalheservico descricao ");
			sql.append("from requisicaomudancaservico req left join servico ser on req.idservico=ser.idservico ");
			sql.append("where req.idrequisicaomudanca = ? and req.datafim is null ");
			sql.append("order by nome,idservico");

			if ((idRequisicaoMudanca == null) || (idRequisicaoMudanca.intValue() <= 0)) {
				parametro.add(0);
			} else {
				parametro.add(idRequisicaoMudanca);
			}

			resp = this.execSQL(sql.toString(), parametro.toArray());
			result = this.engine.listConvertion(RequisicaoMudancaServicoDTO.class, resp, listRetorno);
		} catch (PersistenceException e) {
			e.printStackTrace();
			result = null;
		} catch (Exception e) {
			e.printStackTrace();
			result = null;
		}
		return (ArrayList<RequisicaoMudancaServicoDTO>) (((result == null) || (result.size() <= 0)) ? new ArrayList<RequisicaoMudancaServicoDTO>() : result);

	}
	
	public Collection listByIdHistoricoMudanca(Integer idRequisicaoMudanca) throws Exception {
		List fields = new ArrayList();


		String sql = " select distinct rs.idrequisicaomudancaservico, rs.idrequisicaomudanca, rs.idservico,datafim "+
				"from requisicaomudancaservico rs "+
				"inner join ligacao_mud_hist_se ligser on ligser.idrequisicaomudancaservico = rs.idrequisicaomudancaservico "+
				"where  ligser.idhistoricomudanca = ?";

		List resultado = 	execSQL(sql, new Object[]{idRequisicaoMudanca});



		fields.add("idRequisicaoMudancaServico");
		fields.add("idRequisicaoMudanca");
		fields.add("idServico");
		fields.add("dataFim");

		return listConvertion(getBean(), resultado,fields) ;
	}

}
