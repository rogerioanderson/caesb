/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.CategoriaSolucaoDTO;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.SQLConfig;

public class CategoriaSolucaoDao extends CrudDaoDefaultImpl {

    public CategoriaSolucaoDao() {
        super(Constantes.getValue("DATABASE_ALIAS"), null);
    }

    @Override
    public Collection<Field> getFields() {
        final Collection<Field> listFields = new ArrayList<>();
        listFields.add(new Field("idCategoriaSolucao", "idCategoriaSolucao", true, true, false, false));
        listFields.add(new Field("idCategoriaSolucaoPai", "idCategoriaSolucaoPai", false, false, false, false));
        listFields.add(new Field("descricaoCategoriaSolucao", "descricaoCategoriaSolucao", false, false, false, false));
        listFields.add(new Field("dataInicio", "dataInicio", false, false, false, false));
        listFields.add(new Field("dataFim", "dataFim", false, false, false, false));
        return listFields;
    }

    @Override
    public String getTableName() {
        return this.getOwner() + "CategoriaSolucao";
    }

    @Override
    public Collection<CategoriaSolucaoDTO> list() throws PersistenceException {
        return null;
    }

    @Override
    public Class<CategoriaSolucaoDTO> getBean() {
        return CategoriaSolucaoDTO.class;
    }

    @Override
    public Collection<CategoriaSolucaoDTO> find(final IDto arg0) throws PersistenceException {
        return null;
    }

    public Collection<CategoriaSolucaoDTO> findByIdCategoriaSolucaoPai(final Integer parm) throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordenacao = new ArrayList<>();
        condicao.add(new Condition("idCategoriaSolucaoPai", "=", parm));
        ordenacao.add(new Order("descricaoCategoriaSolucao"));
        return super.findByCondition(condicao, ordenacao);
    }

    public void deleteByIdCategoriaSolucaoPai(final Integer parm) throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        condicao.add(new Condition("idCategoriaSolucaoPai", "=", parm));
        super.deleteByCondition(condicao);
    }

    public Collection<CategoriaSolucaoDTO> findSemPai() throws PersistenceException {
        String sql = "SELECT idCategoriaSolucao, idCategoriaSolucaoPai, descricaoCategoriaSolucao, dataInicio, dataFim FROM categoriasolucao WHERE idCategoriaSolucaoPai IS NULL AND dataFim IS NULL AND ";
        if (CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.POSTGRESQL)) {
            sql += "(UPPER(deleted) IS NULL OR UPPER(deleted) = 'N') ";
        } else if (CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.SQLSERVER)) {
            sql += "(deleted IS NULL OR deleted = 'N') ";
        } else {
            sql += "(deleted IS NULL OR deleted = 'N') ";
        }
        sql += " ORDER BY descricaoCategoriaSolucao ";

        final List<?> colDados = this.execSQL(sql, null);
        if (colDados != null) {
            return this.listConvertion(CategoriaSolucaoDTO.class, colDados, this.getResultFields());
        }
        return null;
    }

    public Collection<CategoriaSolucaoDTO> findByIdPai(final Integer idPaiParm) throws PersistenceException {
        String sql = "SELECT idCategoriaSolucao, idCategoriaSolucaoPai, descricaoCategoriaSolucao, dataInicio, dataFim FROM categoriasolucao "
                + "WHERE idCategoriaSolucaoPai = ? AND dataFim IS NULL AND ";
        if (CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.POSTGRESQL)) {
            sql += "(UPPER(deleted) IS NULL OR UPPER(deleted) = 'N') ";
        } else if (CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.SQLSERVER)) {
            sql += "(deleted IS NULL OR deleted = 'N') ";
        } else {
            sql += "(deleted IS NULL OR deleted = 'N') ";
        }
        sql += "ORDER BY descricaoCategoriaSolucao ";

        final List<?> colDados = this.execSQL(sql, new Object[] {idPaiParm});
        if (colDados != null) {
            return this.listConvertion(CategoriaSolucaoDTO.class, colDados, this.getResultFields());
        }
        return null;
    }
    
    /**
     * Quando um "Problema" foi cadastrado, sendo associado a uma "Categoria Solu��o" que foi posteriormente exclu�da l�gicamente, surgia o problema de que n�o era mais trazido a "Categoria Solu��o"
     * associada ao "Problema". Assim, como solu��o tem-se este outro m�todo, que traz todas as "Categorias Solu��o" cadastradas e v�lidas, seguindo as mesma regras do m�todo
     * "categoriaSolucaoDao.findSemPai()", al�m de trazer tamb�m apenas uma "Categoria Solu��o" exclu�da, se houver associa��o.
     * 
     *
     * @author rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
     * @param idCategoriaSolucao
     * @return
     * @throws PersistenceException
     * @since 08/05/2015
     */
    public Collection<CategoriaSolucaoDTO> findSemPaiMesmoQuandoDeleted(final Integer idCategoriaSolucao) throws PersistenceException {
        Collection<CategoriaSolucaoDTO> colCategSolucaoSemPai = null;

        try {
            colCategSolucaoSemPai = this.findSemPai();
        } catch (PersistenceException pE) {
            pE.printStackTrace();
        }

        String sqlBuscaCategoriaSolDeletedMasAssociada = "SELECT idCategoriaSolucao, idCategoriaSolucaoPai, descricaoCategoriaSolucao, dataInicio, dataFim FROM categoriasolucao WHERE dataFim IS NULL AND idCategoriaSolucao = ?";

        final List<?> categoriaSolDeleted = this.execSQL(sqlBuscaCategoriaSolDeletedMasAssociada, new Object[] { idCategoriaSolucao });

        if (categoriaSolDeleted != null && !categoriaSolDeleted.isEmpty()) {
            colCategSolucaoSemPai.addAll(this.listConvertion(CategoriaSolucaoDTO.class, categoriaSolDeleted, this.getResultFields()));
        }

        return colCategSolucaoSemPai;
    }

    /**
     * Quando um "Problema" foi cadastrado, sendo associado a uma "Categoria Solu��o" que foi posteriormente exclu�da l�gicamente, surgia o problema de que n�o era mais trazido a "Categoria Solu��o"
     * associada ao "Problema". Assim, como solu��o tem-se este outro m�todo, que traz todas as "Categorias Solu��o" cadastradas e v�lidas, seguindo as mesma regras do m�todo
     * "categoriaSolucaoDao.findByIdPai()", al�m de trazer tamb�m apenas uma "Categoria Solu��o" exclu�da, se houver associa��o.
     * 
     * @author rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
     * @param idPaiParm
     * @param idCategoriaSolucao
     * @return
     * @throws PersistenceException
     * @since 08/05/2015
     */
    public Collection<CategoriaSolucaoDTO> findByIdPaiMesmoQuandoDeleted(final Integer idPaiParm) throws PersistenceException {
        Collection<CategoriaSolucaoDTO> colCategSolucaoIdPai = null;

        try {
            colCategSolucaoIdPai = this.findByIdPai(idPaiParm);
        } catch (PersistenceException pE) {
            pE.printStackTrace();
        }

        String sql = "SELECT idCategoriaSolucao, idCategoriaSolucaoPai, descricaoCategoriaSolucao, dataInicio, dataFim FROM categoriasolucao "
                + "WHERE idCategoriaSolucaoPai = ? AND dataFim IS NULL  AND idCategoriaSolucao = ?";
        sql += " ORDER BY descricaoCategoriaSolucao ";

        final List<?> categoriaSolDeleted = this.execSQL(sql, new Object[] { idPaiParm, idPaiParm });
        if (categoriaSolDeleted != null) {
            colCategSolucaoIdPai.addAll(this.listConvertion(CategoriaSolucaoDTO.class, categoriaSolDeleted, this.getResultFields()));
        }

        return colCategSolucaoIdPai;
    }

    public Collection<CategoriaSolucaoDTO> verificaDescricaoDuplicadaCategoriaAoCriar(final String DescricaoCategoriaSolucao) throws PersistenceException {
        String sql = "select * from categoriasolucao where descricaocategoriasolucao = ? AND ";
        if (CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.POSTGRESQL)) {
            sql += "(UPPER(deleted) IS NULL OR UPPER(deleted) = 'N') ";
        } else if (CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.SQLSERVER)) {
            sql += "(deleted IS NULL OR deleted = 'N') ";
        } else {
            sql += "(deleted IS NULL OR deleted = 'N') ";
        }
        sql += "ORDER BY descricaoCategoriaSolucao ";

        final List colDados = this.execSQL(sql, new Object[] {DescricaoCategoriaSolucao});
        if (colDados != null) {
            return this.listConvertion(CategoriaSolucaoDTO.class, colDados, this.getResultFields());
        }
        return null;
    }

    public Collection<CategoriaSolucaoDTO> verificaDescricaoDuplicadaCategoriaAoAtualizar(final Integer idCategoriaSolucao, final String DescricaoCategoriaSolucao)
            throws PersistenceException {
        String sql = "select * from categoriasolucao where descricaocategoriasolucao = ? AND idcategoriasolucao <> ? AND ";
        if (CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.POSTGRESQL)) {
            sql += "(UPPER(deleted) IS NULL OR UPPER(deleted) = 'N') ";
        } else if (CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.SQLSERVER)) {
            sql += "(deleted IS NULL OR deleted = 'N') ";
        } else {
            sql += "(deleted IS NULL OR deleted = 'N') ";
        }
        sql += "ORDER BY descricaoCategoriaSolucao ";

        final List<?> colDados = this.execSQL(sql, new Object[] {DescricaoCategoriaSolucao, idCategoriaSolucao});
        if (colDados != null) {
            return this.listConvertion(CategoriaSolucaoDTO.class, colDados, this.getResultFields());
        }
        return null;
    }

    public Collection<CategoriaSolucaoDTO> listaCategoriasSolucaoAtivas() throws PersistenceException {
        final List<String> fields = new ArrayList<>();
        final String sql = "SELECT idCategoriaSolucao, descricaoCategoriaSolucao FROM categoriasolucao WHERE datafim is NULL AND (deleted is NULL OR deleted = 'n') ";

        final List<?> listaR = this.execSQL(sql, null);

        fields.add("idCategoriaSolucao");
        fields.add("descricaoCategoriaSolucao");

        return this.listConvertion(CategoriaSolucaoDTO.class, listaR, fields);
    }

    private List<String> resultFields;

    private List<String> getResultFields() {
        if (resultFields == null) {
            resultFields = new ArrayList<>();
            resultFields.add("idCategoriaSolucao");
            resultFields.add("idCategoriaSolucaoPai");
            resultFields.add("descricaoCategoriaSolucao");
            resultFields.add("dataInicio");
            resultFields.add("dataFim");
        }
        return resultFields;
    }

}
