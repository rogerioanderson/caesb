/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.InstalacaoDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.util.Constantes;

/**
 * @author flavio.santana
 * 
 */
public class InstalacaoDao extends CrudDaoDefaultImpl {

	public InstalacaoDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}

	@SuppressWarnings("rawtypes")
	public Class getBean() {
		return InstalacaoDTO.class;
	}

	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<Field>();

		listFields.add(new Field("idinstalacao", "idinstalacao", true, true, false, true));
		listFields.add(new Field("sucesso", "sucesso", false, false, false, true));
		listFields.add(new Field("passo", "passo", false, false, false, true));

		return listFields;
	}

	public String getTableName() {
		return "instalacao";
	}

	public boolean isSucessoInstalacao() throws PersistenceException {
		List<Object> parametro = new ArrayList<Object>();
		List fields = new ArrayList();
		List list = new ArrayList();
		String sql = "select * from " + getTableName() + " where sucesso = 'S' ";
		list = this.execSQL(sql, parametro.toArray());
		fields.add("idinstalacao");
		fields.add("sucesso");
		fields.add("passo");
		if (list.isEmpty())
			return false;
		else 
			return true;
	}

	@Override
	public Collection find(IDto obj) throws PersistenceException {
		return null;
	}

	@Override
	public Collection list() throws PersistenceException {
		return null;
	}

}
