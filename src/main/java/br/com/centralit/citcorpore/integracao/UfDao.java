/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.UfDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class UfDao extends CrudDaoDefaultImpl {

	public UfDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}

	public Class getBean() {
		return UfDTO.class;
	}

	
	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();
		
		listFields.add(new Field("idUF", "idUf", true, false, false, true));
		listFields.add(new Field("idregioes", "idRegioes", true, false, false, true));
		listFields.add(new Field("NomeUF", "nomeUf", false, false, false, false));
		listFields.add(new Field("SiglaUF", "siglaUf", false, false, false, false));
		
		return listFields;
	}

	public String getTableName() {
		return "UFS";
	}

	public Collection find(IDto obj) throws PersistenceException {
		return null;
	}
	
	@Override
	public Collection list() throws PersistenceException {
		List list = new ArrayList();
		list.add(new Order("nomeUf"));
		return super.list(list);
	}

	public Collection<UfDTO> listByIdRegioes(UfDTO obj) throws PersistenceException {
		List list = new ArrayList();
		List fields = new ArrayList();
		String sql = "select iduf,nomeuf from " + getTableName()
				+ " where idregioes = ? ";
		fields.add("idUf");
		fields.add("nomeUf");
		list = this.execSQL(sql, new Object[] {obj.getIdRegioes()});
		Collection<UfDTO> result = this.engine.listConvertion(getBean(), list,fields);
		return result;
	}
	
	public Collection<UfDTO> listByIdPais(UfDTO obj) throws PersistenceException {
		List list = new ArrayList();
		List fields = new ArrayList();
		String sql = "select iduf,nomeuf from " + getTableName()
				+ " where idpais = ? ";
		
		
		fields.add("idUf");
		fields.add("nomeUf");
		list = this.execSQL(sql, new Object[] {obj.getIdPais()});
		if(list!=null && !list.isEmpty()){
			Collection<UfDTO> result = this.engine.listConvertion(getBean(), list,fields);
			return result;
		}
		return null;
		
	}
	
	public UfDTO findByIdUf(Integer idUf) throws PersistenceException {
		List list = new ArrayList();
		List fields = new ArrayList();
		String sql = "select iduf,nomeuf,idPais from " + getTableName()
				+ " where idUf = ? ";
		fields.add("idUf");
		fields.add("nomeUf");
		fields.add("idPais");
		
		list = this.execSQL(sql, new Object[] {idUf});
		List<UfDTO> result = this.engine.listConvertion(getBean(), list,fields);
		if (result != null && !result.isEmpty())
			return result.get(0);
		else
			return null;
	}


}
