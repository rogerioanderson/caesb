/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.MotivoSuspensaoAtividadeDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

public class MotivoSuspensaoAtividadeDao extends CrudDaoDefaultImpl {

    public MotivoSuspensaoAtividadeDao() {
        super(Constantes.getValue("DATABASE_ALIAS"), null);
    }

    @Override
    public Collection<Field> getFields() {
        final Collection<Field> listFields = new ArrayList<>();
        listFields.add(new Field("idMotivo", "idMotivo", true, true, false, true));
        listFields.add(new Field("descricao", "descricao", false, false, false, false));
        listFields.add(new Field("dataFim", "dataFim", false, false, false, false));
        return listFields;
    }

    @Override
    public String getTableName() {
        return this.getOwner() + "MotivoSuspensaoAtivid";
    }

    @Override
    public Collection<MotivoSuspensaoAtividadeDTO> list() throws PersistenceException {
        return super.list("descricao");
    }

    @Override
    public Class<MotivoSuspensaoAtividadeDTO> getBean() {
        return MotivoSuspensaoAtividadeDTO.class;
    }

    @Override
    public Collection<MotivoSuspensaoAtividadeDTO> find(final IDto arg0) throws PersistenceException {
        return null;
    }

    /**
     * Verifica se o registro informado j� consta gravado no BD. Considera apenas registros ativos.
     *
     * @param motivoSuspensaoAtividadeDTO
     * @return boolean
     * @throws Exception
     */
    public boolean jaExisteRegistroComMesmoNome(final MotivoSuspensaoAtividadeDTO motivoSuspensaoAtividadeDTO) throws PersistenceException {
        final List<Condition> condicoes = new ArrayList<>();
        condicoes.add(new Condition("descricao", "=", motivoSuspensaoAtividadeDTO.getDescricao()));
        condicoes.add(new Condition("dataFim", "is", null));
        final Collection retorno = super.findByCondition(condicoes, null);
        if (retorno != null) {
            if (retorno.size() > 0) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public Collection<MotivoSuspensaoAtividadeDTO> listarMotivosSuspensaoAtividadeAtivos() throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordenacao = new ArrayList<>();
        condicao.add(new Condition("dataFim", "is", null));
        ordenacao.add(new Order("descricao"));
        return super.findByCondition(condicao, ordenacao);
    }

}
