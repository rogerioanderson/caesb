/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.centralit.citcorpore.bean.AnexoBaseConhecimentoDTO;
import br.com.centralit.citcorpore.bean.TipoDemandaServicoDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class TipoDemandaServicoDao extends CrudDaoDefaultImpl {

	public TipoDemandaServicoDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}

	public Class getBean() {
		return TipoDemandaServicoDTO.class;
	}

	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();

		listFields.add(new Field("idTipoDemandaServico", "idTipoDemandaServico", true, true, false, false));
		listFields.add(new Field("nomeTipoDemandaServico", "nomeTipoDemandaServico", false, false, false, false));
		listFields.add(new Field("classificacao", "classificacao", false, false, false, false));
		listFields.add(new Field("deleted", "deleted", false, false, false, false));

		return listFields;
	}

	public String getTableName() {
		return "TIPODEMANDASERVICO";
	}

	public Collection find(IDto obj) throws PersistenceException {
		return null;
	}

	public Collection list() throws PersistenceException {
		List list = new ArrayList();
		list.add(new Order("nomeTipoDemandaServico"));
		return super.list(list);
	}

	public Collection<TipoDemandaServicoDTO> listSolicitacoes() throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList();
		condicao.add(new Condition("classificacao", "<>", "O"));
		condicao.add(new Condition("deleted", "is", null));
		condicao.add(new Condition(Condition.OR, "deleted", "<>", "Y"));
		ordenacao.add(new Order("nomeTipoDemandaServico"));
		return super.findByCondition(condicao, ordenacao);
	}

	/**
	 * Retorna lista de Tipo Demanda por nome.
	 *
	 * @return Collection
	 * @throws Exception
	 */
	public Collection findByNome(TipoDemandaServicoDTO tipoDemandaServicoDTO) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList();

		condicao.add(new Condition("nomeTipoDemandaServico", "=", tipoDemandaServicoDTO.getNomeTipoDemandaServico()));
		ordenacao.add(new Order("nomeTipoDemandaServico"));
		return super.findByCondition(condicao, ordenacao);
	}


	/**
	 * @author euler.ramos
	 * @param tipoDemandaServicoDTO
	 * @return
	 * @throws Exception
	 */
	public Collection findByClassificacao(String classificacao) throws PersistenceException {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from tipodemandaservico");

		if ((classificacao!=null)&&(classificacao.length()>0)){
			sql.append(" where (classificacao in ("+classificacao+"))");
		}
		sql.append(" order by idtipodemandaservico ");
		List lista = new ArrayList();
		lista = this.execSQL(sql.toString(), null);

		List listRetorno = new ArrayList();
		listRetorno.add("idTipoDemandaServico");
		listRetorno.add("nomeTipoDemandaServico");
		listRetorno.add("classificacao");
		listRetorno.add("deleted");

		List result = this.engine.listConvertion(getBean(), lista, listRetorno);
		return (result == null ? new ArrayList<AnexoBaseConhecimentoDTO>() : result);
	}

	/**
	 * @see br.com.centralit.citcorpore.negocio.TipoDemandaService#validarExclusaoVinculada(HashMap)
	 *
	 * @author Ezequiel
	 */
	public List validarExclusaoVinculada(Map mapFields) throws PersistenceException {

		StringBuilder query = new StringBuilder();

		final Integer idTipoDemanda = Integer.valueOf(mapFields.get("IDTIPODEMANDASERVICO").toString());

		query.append(" SELECT COUNT(IDSERVICO) as quantidade");

		query.append(" FROM SERVICO s ");

		query.append(" INNER JOIN TIPODEMANDASERVICO tp ON tp.idtipodemandaservico = s.idtipodemandaservico");

		query.append(" WHERE s.idtipodemandaservico = " +  idTipoDemanda);

		final List parametro = new ArrayList();

        List list = this.execSQL(query.toString(), parametro.toArray());

        final List listRetorno = new ArrayList();

        listRetorno.add("quantidade");

        return engine.listConvertion(getBean(), list, listRetorno);

	}
}
