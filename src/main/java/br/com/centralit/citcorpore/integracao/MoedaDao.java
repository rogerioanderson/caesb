/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.MoedaDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.dto.Usuario;
import br.com.citframework.excecao.InvalidTransactionControler;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.util.Constantes;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class MoedaDao extends CrudDaoDefaultImpl {

	public MoedaDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}

	public MoedaDao(TransactionControler tc, Usuario usuario)
			throws InvalidTransactionControler {
		super(tc, usuario);
		
	}

	@Override
	public Collection find(IDto arg0) throws PersistenceException {
		return null;
	}

	@Override
	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();
		listFields.add(new Field("idMoeda", "idMoeda", true, true, false, false));
		listFields.add(new Field("nomeMoeda", "nomeMoeda", false, false, false, false));
		listFields.add(new Field("usarCotacao", "usarCotacao", false, false, false, false));
		listFields.add(new Field("dataInicio", "dataInicio", false, false, false, false));
		listFields.add(new Field("dataFim", "dataFim", false, false, false, false));
		return listFields;
	}

	@Override
	public String getTableName() {
		return "MOEDAS";
	}

	@Override
	public Collection list() throws PersistenceException {
		List list = new ArrayList();
		list.add(new Order("nomeMoeda"));
        return super.list(list);	}

	@Override
	public Class getBean() {
		return MoedaDTO.class;
	}
	
	/**
	 * Verifica se existe uma moeda com o mesmo nome j� cadastrada
	 * Se existir retorna 'true', se nao existir retorna 'false';
	 * 
	 * @param moedaDTO
	 * @return estaCadastrato
	 * @throws PersistenceException
	 */
	public boolean verificaSeCadastrado(MoedaDTO moedaDTO) throws PersistenceException {
		boolean estaCadastrato;
		List parametro = new ArrayList();
		List list = new ArrayList();
		StringBuilder sql = new StringBuilder();
		sql.append("select idMoeda from " + getTableName() + "  where  nomeMoeda = ? and datafim is null  ");
		parametro.add(moedaDTO.getNomeMoeda());
		list = this.execSQL(sql.toString(), parametro.toArray());
		if (list != null && !list.isEmpty()) {
			estaCadastrato = true;
		} else {
			estaCadastrato = false;
		}
		return estaCadastrato;
	}


	/**
	 * Verifica se existe relacionamento de moeda com outras tabelas.
	 * Se existir retorna 'true', se nao existir retorna 'false';
	 * 
	 * @param moedaDTO
	 * @throws PersistenceException
	 */
	public boolean verificaRelacionamento(MoedaDTO moedaDTO) throws PersistenceException {
		boolean estaRelacionado;
		List parametro = new ArrayList();
		List list = new ArrayList();
		StringBuilder sql = new StringBuilder();
		sql.append("select idcontrato from contratos where  idMoeda = ? and deleted is null ");
		parametro.add(moedaDTO.getIdMoeda());
		list = this.execSQL(sql.toString(), parametro.toArray());
		if (list != null && !list.isEmpty()) {
			estaRelacionado = true;
		} else {
			estaRelacionado = false;
		}
		return estaRelacionado;
	}

	@Override
	public void updateNotNull(IDto obj) throws PersistenceException {
				super.updateNotNull(obj);
	}
	
	
	public Collection findAtivos() throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList();
		condicao.add(new Condition("dataFim","IS", null));
		ordenacao.add(new Order("nomeMoeda"));
		return super.findByCondition(condicao, ordenacao);
	}

}
