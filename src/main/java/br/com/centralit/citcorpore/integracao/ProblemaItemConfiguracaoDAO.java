/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.ItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.ProblemaItemConfiguracaoDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class ProblemaItemConfiguracaoDAO extends CrudDaoDefaultImpl {


	public ProblemaItemConfiguracaoDAO() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}

	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();
		listFields.add(new Field("idProblemaItemConfiguracao", "idProblemaItemConfiguracao", true, true, false, false));
		listFields.add(new Field("idProblema", "idProblema", false, false, false, false));
		listFields.add(new Field("idItemConfiguracao", "idItemConfiguracao", false, false, false, false));
		listFields.add(new Field("descricaoProblema", "descricaoProblema", false, false, false, false));
		return listFields;
	}

	public ProblemaItemConfiguracaoDTO findByIdItemConfiguracaoEIdProblema(Integer idItemConfiguracao, Integer idProblema) throws PersistenceException {
		ArrayList<Condition> condicoes = new ArrayList<Condition>();
		condicoes.add(new Condition("idItemConfiguracao", "=", idItemConfiguracao));
		condicoes.add(new Condition("idProblema", "=", idProblema));
		
		ArrayList<ProblemaItemConfiguracaoDTO> lista = (ArrayList<ProblemaItemConfiguracaoDTO>) findByCondition(condicoes, null);
		
		if(lista != null){
			return lista.get(0);
		}
		
		return null;
	}
	
	/**
	 * Verifica se existe outro item igual criado.
	 * Se existir retorna 'true', senao retorna 'false';
	 */
	public boolean verificaSeCadastrado(ProblemaItemConfiguracaoDTO itemDTO) throws PersistenceException {
		boolean estaCadastrado;		
		List parametro = new ArrayList();
		List list = new ArrayList();
		StringBuilder sql = new StringBuilder();
		sql.append("select * from " + getTableName() + " where idItemConfiguracao = ? and idProblema = ?  ");
		parametro.add(itemDTO.getIdItemConfiguracao());
		parametro.add(itemDTO.getIdProblema());
		list = this.execSQL(sql.toString(), parametro.toArray());
		if (list != null && !list.isEmpty()) {
			estaCadastrado = true;
		} else {
			estaCadastrado = false;
		}
		return estaCadastrado;
	}
	
	/**
	 * Deleta os que est�o no banco mas n�o est�o na lista passada para update.
	 * 
	 * @param idProblema
	 * @param listaICs
	 * @throws Exception
	 */
	public void deletaOsQueNaoEstaoNaListaByIdProblema(Integer idProblema, ArrayList<ProblemaItemConfiguracaoDTO> listaICs) throws PersistenceException {
		List condicao = new ArrayList();
		condicao.add(new Condition("idProblema", "=", idProblema));

		if(listaICs != null && listaICs.size() > 0){
			for (ProblemaItemConfiguracaoDTO p : listaICs) {
				condicao.add(new Condition("idItemConfiguracao", "<>", p.getIdItemConfiguracao()));
			}
		}
		this.deleteByCondition(condicao);
	}

	public String getTableName() {
		return this.getOwner() + "problemaitemconfiguracao";
	}

	public Collection list() throws PersistenceException {
		return null;
	}

	public Class getBean() {
		return ProblemaItemConfiguracaoDTO.class;
	}

	public Collection find(IDto arg0) throws PersistenceException {
		return null;
	}

	public Collection findByIdProblemaItemConfiguracao(Integer parm) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList();
		condicao.add(new Condition("idProblemaItemConfiguracao", "=", parm));
		ordenacao.add(new Order("idProblemaItemConfiguracao"));
		return super.findByCondition(condicao, ordenacao);
	}

	public void deleteByIdProblemaItemConfiguracao(Integer parm) throws PersistenceException {
		List condicao = new ArrayList();
		condicao.add(new Condition("idProblemaItemConfiguracao", "=", parm));
		super.deleteByCondition(condicao);
	}

	public Collection findByIdProblema(Integer idProblema) throws Exception {

		List list;
		List parametro = new ArrayList();
		List fields = new ArrayList();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT p.idproblemaitemconfiguracao, p.idproblema, p.iditemconfiguracao, p.descricaoproblema, i.identificacao nomeItemConfiguracao ");
		sql.append("FROM problemaitemconfiguracao p left join itemconfiguracao i on p.iditemconfiguracao = i.iditemconfiguracao ");
		sql.append("WHERE p.idproblema = ? ");
		sql.append("ORDER BY iditemconfiguracao");

		parametro.add(idProblema);

		list = this.execSQL(sql.toString(), parametro.toArray());

		fields.add("idProblemaItemConfiguracao");
		fields.add("idProblema");
		fields.add("idItemConfiguracao");
		fields.add("descricaoProblema");
		fields.add("nomeItemConfiguracao");

		if (list != null && !list.isEmpty()) {
			List listaIc = this.listConvertion(this.getBean(), list, fields);
			return listaIc;
	}
		return null;
	}

	public void deleteByIdProblema(Integer parm) throws PersistenceException {
		List condicao = new ArrayList();
		condicao.add(new Condition("idProblema", "=", parm));
		super.deleteByCondition(condicao);
	}

	public Collection findByIdItemConfiguracao(Integer parm) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList();
		condicao.add(new Condition("idItemConfiguracao", "=", parm));
		ordenacao.add(new Order("idItemConfiguracao"));
		return super.findByCondition(condicao, ordenacao);
	}

	public void deleteByIdItemConfiguracao(Integer parm) throws PersistenceException {
		List condicao = new ArrayList();
		condicao.add(new Condition("idItemConfiguracao", "=", parm));
		super.deleteByCondition(condicao);
	}
	
	public List<ItemConfiguracaoDTO> listItemConfiguracaoByIdProblema (Integer idProblema) throws PersistenceException {
		List parametro = new ArrayList();
		List fields = new ArrayList();
		StringBuilder sql = new StringBuilder();
		
		sql.append("select  ic.iditemconfiguracao  ");
		sql.append(" from itemconfiguracao ic    ");
		sql.append(" inner join problemaitemconfiguracao pi ");
		sql.append(" on ic.iditemconfiguracao = pi.iditemconfiguracao ");
		sql.append(" where pi.idproblema = ? ");
	
		parametro.add(idProblema);
		
		fields.add("idItemConfiguracao");
   
		List dados =  this.execSQL(sql.toString(), parametro.toArray());
		 
		return (List<ItemConfiguracaoDTO>) this.listConvertion(ItemConfiguracaoDTO.class, dados, fields);
	}
	
	public ProblemaItemConfiguracaoDTO restoreByIdProblema(Integer idSolicitacao) throws PersistenceException {
		List parametro = new ArrayList();
		parametro.add(idSolicitacao);
		
		List<String> listRetorno = new ArrayList<String>();
		listRetorno.add("idProblemaItemConfiguracao");
		listRetorno.add("idProblema");
		listRetorno.add("idItemConfiguracao");
		listRetorno.add("descricaoProblema");

		String sql = " select * from problemaitemconfiguracao where idproblema = ? ";

		List lista = this.execSQL(sql.toString(), parametro.toArray());

		if (lista != null && !lista.isEmpty()) {
			List listaResult = this.engine.listConvertion(ProblemaItemConfiguracaoDTO.class, lista, listRetorno);
			return (ProblemaItemConfiguracaoDTO) listaResult.get(0);
		} else {
			return null;
		}
	}
}
