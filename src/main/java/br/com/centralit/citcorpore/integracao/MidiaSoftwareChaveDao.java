/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.MidiaSoftwareChaveDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.util.Constantes;


public class MidiaSoftwareChaveDao extends CrudDaoDefaultImpl {

	/**
     * @author flavio.santana
     */
	
	public MidiaSoftwareChaveDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();
		
		listFields.add(new Field("idMidiaSoftwareChave", "idMidiaSoftwareChave", true, true, false, false));
		listFields.add(new Field("idMidiaSoftware", "idMidiaSoftware", false, false, false, false));
		listFields.add(new Field("chave", "chave", false, false, false, false));
		listFields.add(new Field("qtdPermissoes", "qtdPermissoes", false, false, false, false));
		
		return listFields;
	}
	
	public String getTableName() {
		return "MIDIASOFTWARECHAVE";
	}
	
	@SuppressWarnings("unchecked")
	public Collection<MidiaSoftwareChaveDTO> find(IDto obj) throws PersistenceException {
		List<MidiaSoftwareChaveDTO> ordem = new ArrayList<MidiaSoftwareChaveDTO>();
		return super.find(obj, ordem);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<MidiaSoftwareChaveDTO> list() throws PersistenceException {
		List<MidiaSoftwareChaveDTO> list = new ArrayList<MidiaSoftwareChaveDTO>();

		return super.list(list);
	}
	
	@SuppressWarnings("rawtypes")
	public Class getBean() {
		return MidiaSoftwareChaveDTO.class;
	}
	
	public void deleteByIdMidiaSoftware(Integer idMidiaSoftware) throws PersistenceException {
		String sql = "DELETE FROM " + getTableName() +" WHERE idMidiaSoftware = ? ";
        super.execUpdate(sql, new Object[]{idMidiaSoftware});
    }
	
	@SuppressWarnings("unchecked")
	public Collection<MidiaSoftwareChaveDTO> findByMidiaSoftware (Integer idMidiaSoftware) throws PersistenceException {
		String sql = "SELECT idMidiaSoftwareChave, chave, qtdPermissoes  FROM " + getTableName() + "  WHERE idMidiaSoftware = ?";
		List<MidiaSoftwareChaveDTO> dados = this.execSQL(sql, new Object[] { idMidiaSoftware });
		List<String> fields = new ArrayList<String>();
		fields.add("idMidiaSoftwareChave");
		fields.add("chave");
		fields.add("qtdPermissoes");
		return this.listConvertion(getBean(), dados, fields);
	}
}
