/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.MeuCatalogoDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

@SuppressWarnings({"unchecked", "rawtypes"})
public class MeuCatalogoDao extends CrudDaoDefaultImpl {

    public MeuCatalogoDao() {
        super(Constantes.getValue("DATABASE_ALIAS"), null);
    }

    @Override
    public Collection<Field> getFields() {
        final Collection<Field> listFields = new ArrayList<>();

        listFields.add(new Field("idServico", "idServico", true, false, false, false));
        listFields.add(new Field("idUsuario", "idUsuario", true, false, false, false));

        return listFields;
    }

    @Override
    public String getTableName() {
        return "MEUCATALOGO";
    }

    @Override
    public Collection list() throws PersistenceException {
        final List list = new ArrayList();
        list.add(new Order("idservico"));
        return super.list(list);
    }

    public Collection findByCondition(final Integer id) throws PersistenceException {
        final List list1 = new ArrayList();
        final List list2 = new ArrayList();
        list1.add(new Condition("idUsuario", "=", id));
        list2.add(new Order("idServico"));
        return super.findByCondition(list1, list2);
    }

    public Collection findByIdServicoAndIdUsuario(final Integer idServico, final Integer idUsuario) throws PersistenceException {
        final List list1 = new ArrayList();
        final List list2 = new ArrayList();
        list1.add(new Condition("idUsuario", "=", idUsuario));
        list1.add(new Condition("idServico", "=", idServico));
        list2.add(new Order("idServico"));
        return super.findByCondition(list1, list2);
    }

    @Override
    public void delete(final IDto obj) throws PersistenceException {
        final MeuCatalogoDTO dto = (MeuCatalogoDTO) obj;
        final List param = new ArrayList();
        param.add(dto.getIdUsuario());
        param.add(dto.getIdServico());
        final String str = "DELETE FROM " + this.getTableName() + " WHERE idUsuario = ? AND idServico = ?";
        super.execUpdate(str, param.toArray());
    }

    @Override
    public Collection find(final IDto obj) throws PersistenceException {
        final List ordem = new ArrayList();
        ordem.add(new Order("idServico"));
        return super.find(obj, ordem);
    }

    @Override
    public Class<MeuCatalogoDTO> getBean() {
        return MeuCatalogoDTO.class;
    }

}
