/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.bean.PerfilAcessoGrupoDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class PerfilAcessoGrupoDao extends CrudDaoDefaultImpl {

	public PerfilAcessoGrupoDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);

	}

	@Override
	public Collection find(IDto arg0) throws PersistenceException {
		return null;
	}

	@Override
	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();

		listFields.add(new Field("IDPERFIL", "idPerfilAcessoGrupo", true, false, false, false));
		listFields.add(new Field("IDGRUPO", "idGrupo", true, false, false, false));
		listFields.add(new Field("DATAINICIO", "dataInicio", true, false, false, false));
		listFields.add(new Field("DATAFIM", "dataFim", false, false, false, false));

		return listFields;
	}

	@Override
	public String getTableName() {
		return "PERFILACESSOGRUPO";
	}

	public PerfilAcessoGrupoDTO listByIdGrupo(PerfilAcessoGrupoDTO obj) throws PersistenceException {
		List list = new ArrayList();
		List fields = new ArrayList();
		String sql = "select idperfil from " + getTableName() + " where idgrupo = " + obj.getIdGrupo() + " ";
		fields.add("idPerfilAcessoGrupo");
		list = this.execSQL(sql, null);

		if (list != null && !list.isEmpty()) {
			return (PerfilAcessoGrupoDTO) this.listConvertion(getBean(), list, fields).get(0);
		} else {
			return null;
		}
	}

	/**
	 * Retorna perfil de acesso ativo do grupo.
	 * 
	 * @param grupo
	 * @return
	 * @throws Exception
	 */
	public PerfilAcessoGrupoDTO obterPerfilAcessoGrupo(GrupoDTO grupo) throws PersistenceException {
		List list = new ArrayList();
		List fields = new ArrayList();
		StringBuilder sql = new StringBuilder();
		List parametros = new ArrayList();

		sql.append("SELECT idperfil FROM perfilacessogrupo WHERE idgrupo = ? AND datafim IS NULL");
		parametros.add(grupo.getIdGrupo());

		fields.add("idPerfilAcessoGrupo");

		list = this.execSQL(sql.toString(), parametros.toArray());

		if (list != null && !list.isEmpty()) {
			return (PerfilAcessoGrupoDTO) this.listConvertion(getBean(), list, fields).get(0);
		} else {
			return null;
		}
	}

	@Override
	public Class getBean() {
		return PerfilAcessoGrupoDTO.class;
	}

	@Override
	public Collection list() throws PersistenceException {
		return null;
	}

	public void updateDataFim(PerfilAcessoGrupoDTO obj) throws PersistenceException {
		List parametros = new ArrayList();
		parametros.add(UtilDatas.getDataAtual());
		parametros.add(obj.getIdGrupo());

		String sql = "UPDATE " + getTableName() + " SET DATAFIM = ? WHERE IDGRUPO = ? ";

		this.execUpdate(sql, parametros.toArray());
	}

	@Override
	public void delete(IDto obj) throws PersistenceException {
		PerfilAcessoGrupoDTO dto = (PerfilAcessoGrupoDTO) obj;
		String sql = "DELETE FROM " + getTableName() + " WHERE IDGRUPO = " + dto.getIdGrupo() + " ";
		this.execUpdate(sql, null);
	}

	/**
	 * Retorna PerfilAcessoGrupo Ativos por idPerfilAcesso.
	 * 
	 * @param idPerfilAcesso
	 * @return
	 * @throws Exception
	 */
	public Collection findByIdPerfil(Integer idPerfilAcesso) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList();
		condicao.add(new Condition("idPerfilAcessoGrupo", "=", idPerfilAcesso));
		condicao.add(new Condition("dataFim", "is", null));
		ordenacao.add(new Order("idPerfilAcessoGrupo"));
		return super.findByCondition(condicao, ordenacao);
	}

	/**
	 * @param idPerfilAcesso
	 * @return
	 * @throws Exception
	 */
	public boolean existeGrupoVinculadoPerfil(Integer idPerfilAcesso) throws PersistenceException {
		
		List list = new ArrayList();

		StringBuilder sql = new StringBuilder();
		
		List parametros = new ArrayList();

		sql.append("SELECT * FROM perfilacessogrupo WHERE idPerfil = ?");
		
		parametros.add(idPerfilAcesso);

		list = this.execSQL(sql.toString(), parametros.toArray());

		return list != null && list.size() > 0;
	}

}
