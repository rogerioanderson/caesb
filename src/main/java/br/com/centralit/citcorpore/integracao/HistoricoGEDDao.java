/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.HistoricoGEDDTO;
import br.com.centralit.citcorpore.bean.UploadDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.util.Constantes;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class HistoricoGEDDao extends CrudDaoDefaultImpl {

	public HistoricoGEDDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
		// TODO Auto-generated constructor stub
	}

	/**
     *
     */

	@Override
	public Collection find(IDto obj) throws PersistenceException {

		return null;
	}

	@Override
	public Collection<Field> getFields() {

		Collection<Field> listFields = new ArrayList<>();

		listFields.add(new Field("idligacao_historico_ged", "idLigacaoHistoricoGed", true, true, false, false));
		listFields.add(new Field("idcontroleged", "idControleGed", false, false, false, false));
		listFields.add(new Field("idrequisicaoliberacao", "idRequisicaoMudanca", false, false, false, false));
		listFields.add(new Field("idhistoricoliberacao", "idHistoricoMudanca", false, false, false, false));
		listFields.add(new Field("idtabela", "idTabela", false, false, false, false));
		listFields.add(new Field("datafim", "dataFim", false, false, false, false));

		return listFields;
	}

	@Override
	public String getTableName() {
		return "LIGACAO_HISTORICO_GED";
	}

	@Override
	public Collection list() throws PersistenceException {
		List list = new ArrayList();
		return super.list(list);
	}

	@Override
	public Class getBean() {
		return HistoricoGEDDTO.class;
	}

	/*public Collection listByIdTabelaAndIdLiberacao(Integer idTabela, Integer idRequisicaoLiberacao) throws PersistenceException {
		List parametros = new ArrayList();

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT idligacao_historico_ged, idcontroleged, idrequisicaoliberacao, idhistoricoliberacao, idtabela ");
		sql.append("FROM ligacao_historico_ged ");
		sql.append("WHERE idtabela = ? AND idrequisicaoliberacao = ? and datafim is null");

		parametros.add(idTabela);
		parametros.add(idRequisicaoLiberacao);

		List list = this.execSQL(sql.toString(), parametros.toArray());

		return this.engine.listConvertion(this.getBean(), list, (List) getFields());
	}*/


	public Collection listByIdTabelaAndIdLiberacao(Integer idTabela, Integer idRequisicaoLiberacao) throws PersistenceException {
		List lstRetorno = new ArrayList();
		List list = new ArrayList();
		StringBuilder sql = new StringBuilder();
		List parametro = new ArrayList();

		parametro.add(idTabela);
		parametro.add(idRequisicaoLiberacao);

		sql.append("SELECT idligacao_historico_ged, idcontroleged, idrequisicaoliberacao, idhistoricoliberacao, idtabela ");
		sql.append("FROM ligacao_historico_ged ");
		sql.append("WHERE idtabela = ? AND idrequisicaoliberacao = ? and datafim is null");




		list = this.execSQL(sql.toString(),  parametro.toArray());

		lstRetorno.add("idLigacaoHistoricoGed");
		lstRetorno.add("idControleGed");
		lstRetorno.add("idRequisicaoLiberacao");
		lstRetorno.add("idHistoricoLiberacao");
		lstRetorno.add("idTabela");


		if (list != null && !list.isEmpty()) {

			return (List<UploadDTO>) this.listConvertion(this.getBean(), list, lstRetorno);

		} else {

			return null;
		}
	}
	public Collection listByIdTabelaAndIdLiberacaoEDataFim(Integer idTabela, Integer idRequisicaoLiberacao) throws PersistenceException {
		List lstRetorno = new ArrayList();
		List list = new ArrayList();
		StringBuilder sql = new StringBuilder();
		List parametro = new ArrayList();

		parametro.add(idTabela);
		parametro.add(idRequisicaoLiberacao);

		sql.append("SELECT idligacao_historico_ged, idcontroleged, idrequisicaoliberacao, idhistoricoliberacao, idtabela ");
		sql.append("FROM ligacao_historico_ged ");
		sql.append("WHERE idtabela = ? AND idrequisicaoliberacao = ? and datafim is not null");




		list = this.execSQL(sql.toString(),  parametro.toArray());

		lstRetorno.add("idLigacaoHistoricoGed");
		lstRetorno.add("idControleGed");
		lstRetorno.add("idRequisicaoLiberacao");
		lstRetorno.add("idHistoricoLiberacao");
		lstRetorno.add("idTabela");


		if (list != null && !list.isEmpty()) {

			return (List<UploadDTO>) this.listConvertion(this.getBean(), list, lstRetorno);

		} else {

			return null;
		}
	}

}
