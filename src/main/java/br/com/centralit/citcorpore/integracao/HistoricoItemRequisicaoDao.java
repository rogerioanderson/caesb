/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.HistoricoItemRequisicaoDTO;
import br.com.centralit.citcorpore.util.Enumerados.AcaoItemRequisicaoProduto;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

public class HistoricoItemRequisicaoDao extends CrudDaoDefaultImpl {
	
	public HistoricoItemRequisicaoDao() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}
	
    public HistoricoItemRequisicaoDao(String databaseAlias) {
    	super(databaseAlias, null);
    }
	
	public Collection<Field> getFields() {
		Collection<Field> listFields = new ArrayList<>();
		listFields.add(new Field("idHistorico" ,"idHistorico", true, true, false, false));
		listFields.add(new Field("idItemRequisicao" ,"idItemRequisicao", false, false, false, false));
		listFields.add(new Field("acao" ,"acao", false, false, false, false));
		listFields.add(new Field("idResponsavel" ,"idResponsavel", false, false, false, false));
		listFields.add(new Field("dataHora" ,"dataHora", false, false, false, false));
		listFields.add(new Field("situacao" ,"situacao", false, false, false, false));
		listFields.add(new Field("complemento" ,"complemento", false, false, false, false));
		listFields.add(new Field("atributosAnteriores" ,"atributosAnteriores", false, false, false, false));
		listFields.add(new Field("atributosAtuais" ,"atributosAtuais", false, false, false, false));
		
		return listFields;
	}
	
	public String getTableName() {
		return this.getOwner() + "historicoitemrequisicao";
	}
	
	public Collection list() throws PersistenceException {
		return null;
	}

	public Class getBean() {
		return HistoricoItemRequisicaoDTO.class;
	}
	
	public Collection find(IDto arg0) throws PersistenceException {
		return null;
	}
	
	public Collection findByIdItemRequisicao(Integer parm) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList(); 
		condicao.add(new Condition("idItemRequisicao", "=", parm)); 
		ordenacao.add(new Order("dataHora",Order.DESC));
		return super.findByCondition(condicao, ordenacao);
	}
	
	public void deleteByIdItemRequisicao(Integer parm) throws PersistenceException {
		List condicao = new ArrayList();
		condicao.add(new Condition("idItemRequisicao", "=", parm));
		super.deleteByCondition(condicao);
	}
	public HistoricoItemRequisicaoDTO findLastByIdItemRequisicao(Integer idItemRequisicao, AcaoItemRequisicaoProduto acao) throws PersistenceException {
		List condicao = new ArrayList();
		List ordenacao = new ArrayList(); 
		condicao.add(new Condition("idItemRequisicao", "=", idItemRequisicao)); 
		condicao.add(new Condition("acao", "=", acao.name())); 
		ordenacao.add(new Order("dataHora",Order.DESC));
		
		Collection<HistoricoItemRequisicaoDTO> result = super.findByCondition(condicao, ordenacao);
		if (result != null && result.size() > 0) {
			return ((List<HistoricoItemRequisicaoDTO>) result).get(0);
		}else{
			return null;
		}
	}
	
}
