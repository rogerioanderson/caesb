/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.integracao;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import br.com.centralit.citcorpore.bean.AnaliseTendenciasDTO;
import br.com.centralit.citcorpore.bean.TendenciaDTO;
import br.com.centralit.citcorpore.bean.TendenciaGanttDTO;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.SQLConfig;
import br.com.citframework.util.UtilDatas;

/**
 * @author euler.ramos
 *
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class AnaliseTendenciasDAO extends CrudDaoDefaultImpl {

	public AnaliseTendenciasDAO() {
		super(Constantes.getValue("DATABASE_ALIAS"), null);
	}

	@Override
	public Collection find(IDto obj) throws PersistenceException {
		return null;
	}

	@Override
	public Collection<Field> getFields() {
		return null;
	}

	@Override
	public String getTableName() {
		return "solicitacaoservico";
	}

	@Override
	public Collection list() throws PersistenceException {
		return null;
	}

	@Override
	public Class getBean() {
		return TendenciaDTO.class;
	}

        // M�todo criado para completar as consultas do gr�fico com as datas que n�o tiveram lan�amento de solicita��es de servi�o
        // � utilizado apenas nos bancos que n�o s�o o PostgreSQL
        public ArrayList<TendenciaGanttDTO> montarPeriodo(final AnaliseTendenciasDTO analiseTendenciasDTO, final List<TendenciaGanttDTO> listTendenciaGanttDTO) {
            ArrayList<TendenciaGanttDTO> listaPeriodo = null;
    
            if (!Objects.equals(analiseTendenciasDTO, null) && CollectionUtils.isNotEmpty(listTendenciaGanttDTO)) {
                listaPeriodo = new ArrayList<TendenciaGanttDTO>();
    
                try {
                    Date DataInicio                   = null;
                    Timestamp DataFim                 = null;
                    TendenciaGanttDTO registroGrafico = null;
                    java.util.Date utilDate           = null;
    
                    if (!Objects.equals(analiseTendenciasDTO.getDataInicio(), null) && !Objects.equals(analiseTendenciasDTO.getDataFim(), null)) {
                        DataInicio = UtilDatas.getSqlDate(analiseTendenciasDTO.getDataInicio());
                        DataFim = UtilDatas.getTimeStampComUltimaHoraDoDia(analiseTendenciasDTO.getDataFim());
    
                        while (DataFim.compareTo(DataInicio) >= 0) {
                            registroGrafico = new TendenciaGanttDTO();
                            registroGrafico.setData(DataInicio);
                            registroGrafico.setQtde(0);
    
                            for (TendenciaGanttDTO tendenciaGanttDTO : listTendenciaGanttDTO) {
                                if (DataInicio.compareTo(tendenciaGanttDTO.getData()) == 0) {
                                    registroGrafico.setQtde(tendenciaGanttDTO.getQtde());
                                }
                            }
    
                            listaPeriodo.add(registroGrafico);
                            utilDate = UtilDatas.incrementaDiasEmData(DataInicio, 1);
                            DataInicio = new java.sql.Date(utilDate.getTime());
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
    
            return listaPeriodo;
        }

	public List<TendenciaDTO> buscarTendenciasServico(AnaliseTendenciasDTO analiseTendenciasDTO){
		List result;
		try {
			List resp = new ArrayList();
			List parametro = new ArrayList();
			List listRetorno = new ArrayList();

			java.sql.Date DataInicio = null;
			Timestamp DataFim = null;

			if (analiseTendenciasDTO.getDataInicio()!=null){
				DataInicio = UtilDatas.getSqlDate(analiseTendenciasDTO.getDataInicio());
			}

			if (analiseTendenciasDTO.getDataFim()!=null){
				DataFim = UtilDatas.getTimeStampComUltimaHoraDoDia(analiseTendenciasDTO.getDataFim());
			}

			listRetorno.add("id");
			listRetorno.add("descricao");
			listRetorno.add("qtdeCritica");

			StringBuilder sql = new StringBuilder();
			sql.append("select ");

			sql.append("servico.idservico id, servico.nomeservico descricao, a.qtde qtdecritica ");
			sql.append("from servico join ");
			                  sql.append("(select servicocontrato.idservico,count(solicitacaoservico.idsolicitacaoservico) as qtde ");
			                   sql.append("from solicitacaoservico join servicocontrato on ");
			if ((DataInicio!=null) && (DataFim!=null)){
                									sql.append("(solicitacaoservico.datahorasolicitacao between ? and ?) and ");
                									parametro.add(DataInicio);
                									parametro.add(DataFim);
			}

			sql.append("solicitacaoservico.situacao <> ? and ");
			parametro.add(br.com.centralit.citcorpore.util.Enumerados.SituacaoSolicitacaoServico.Cancelada.getDescricao());

			sql.append("solicitacaoservico.idservicocontrato = servicocontrato.idservicocontrato ");
			if ((analiseTendenciasDTO.getIdContrato()!=null)&&(analiseTendenciasDTO.getIdContrato().intValue()>0)){
													sql.append("and servicocontrato.idcontrato = ? ");
													parametro.add(analiseTendenciasDTO.getIdContrato());
			}
			if ((analiseTendenciasDTO.getIdServico()!=null)&&(analiseTendenciasDTO.getIdServico().intValue()>0)){
													sql.append("and servicocontrato.idservico = ? ");
													parametro.add(analiseTendenciasDTO.getIdServico());
			}
			if ((analiseTendenciasDTO.getIdGrupoExecutor()!=null)&&(analiseTendenciasDTO.getIdGrupoExecutor().intValue()>0)){
													sql.append("and solicitacaoservico.idgrupoatual = ? ");
													parametro.add(analiseTendenciasDTO.getIdGrupoExecutor());
			}

			if ((analiseTendenciasDTO.getIdEmpregado()!=null)&&(analiseTendenciasDTO.getIdEmpregado().intValue()>0)){
													sql.append("and solicitacaoservico.idsolicitante = ? ");
													parametro.add(analiseTendenciasDTO.getIdEmpregado());
			}

			if ((analiseTendenciasDTO.getIdTipoDemandaServico()!=null)&&(analiseTendenciasDTO.getIdTipoDemandaServico().intValue()>0)){
													sql.append("and solicitacaoservico.idtipodemandaservico = ? ");
													parametro.add(analiseTendenciasDTO.getIdTipoDemandaServico());
			}

			if ((analiseTendenciasDTO.getUrgencia()!=null)&&(analiseTendenciasDTO.getUrgencia().length()>0)){
													sql.append("and solicitacaoservico.urgencia = ? ");
													parametro.add(analiseTendenciasDTO.getUrgencia());
			}

			if ((analiseTendenciasDTO.getImpacto()!=null)&&(analiseTendenciasDTO.getImpacto().length()>0)){
													sql.append("and solicitacaoservico.impacto = ? ");
													parametro.add(analiseTendenciasDTO.getImpacto());
			}

			if ((analiseTendenciasDTO.getIdCausaIncidente()!=null)&&(analiseTendenciasDTO.getIdCausaIncidente().intValue()>0)){
													sql.append("and solicitacaoservico.idcausaincidente = ? ");
													parametro.add(analiseTendenciasDTO.getIdCausaIncidente());
			}

			if ((analiseTendenciasDTO.getIdItemConfiguracao()!=null)&&(analiseTendenciasDTO.getIdItemConfiguracao().intValue()>0)){
									sql.append("join itemcfgsolicitacaoserv on solicitacaoservico.idsolicitacaoservico = itemcfgsolicitacaoserv.idsolicitacaoservico and ");
																		sql.append("itemcfgsolicitacaoserv.iditemconfiguracao = ? and ");
																		sql.append("(itemcfgsolicitacaoserv.datafim is null) ");
									parametro.add(analiseTendenciasDTO.getIdItemConfiguracao());
			}

			sql.append("group by servicocontrato.idservico) a on servico.idservico = a.idservico ");

			if ((analiseTendenciasDTO.getQtdeCritica() !=null)&&(analiseTendenciasDTO.getQtdeCritica().intValue()>0)){
				sql.append("and qtde >= ? ");
				parametro.add(analiseTendenciasDTO.getQtdeCritica());
			}
			sql.append("order by qtde desc, descricao");

			resp = this.execSQL(sql.toString(), parametro.toArray());
			result = this.engine.listConvertion(getBean(), resp, listRetorno);
		} catch (PersistenceException e) {
			e.printStackTrace();
			result = null;
		} catch (Exception e) {
			e.printStackTrace();
			result = null;
		}
		return (ArrayList<TendenciaDTO>) (((result == null)||(result.size()<=0)) ? new ArrayList<TendenciaDTO>() : result);
	}

	public List<TendenciaDTO> buscarTendenciasCausa(AnaliseTendenciasDTO analiseTendenciasDTO){
		List result;
		try {
			List resp = new ArrayList();
			List parametro = new ArrayList();
			List listRetorno = new ArrayList();

			java.sql.Date DataInicio = null;
			Timestamp DataFim = null;

			if (analiseTendenciasDTO.getDataInicio()!=null){
				DataInicio = UtilDatas.getSqlDate(analiseTendenciasDTO.getDataInicio());
			}

			if (analiseTendenciasDTO.getDataFim()!=null){
				DataFim = UtilDatas.getTimeStampComUltimaHoraDoDia(analiseTendenciasDTO.getDataFim());
			}

			listRetorno.add("id");
			listRetorno.add("descricao");
			listRetorno.add("qtdeCritica");

			StringBuilder sql = new StringBuilder();
			sql.append("select ");

			sql.append("causaincidente.idcausaincidente id, causaincidente.descricaocausa descricao, a.qtde qtdecritica ");
			sql.append("from causaincidente join ");
			                  sql.append("(select solicitacaoservico.idcausaincidente,count(solicitacaoservico.idsolicitacaoservico) as qtde ");
			                   sql.append("from solicitacaoservico join servicocontrato on ");
			if ((DataInicio!=null) && (DataFim!=null)){
                									sql.append("(solicitacaoservico.datahorasolicitacao between ? and ?) and ");
                									parametro.add(DataInicio);
                									parametro.add(DataFim);
			}

			sql.append("solicitacaoservico.situacao <> ? and ");
			parametro.add(br.com.centralit.citcorpore.util.Enumerados.SituacaoSolicitacaoServico.Cancelada.getDescricao());

			sql.append("solicitacaoservico.idservicocontrato = servicocontrato.idservicocontrato ");
			if ((analiseTendenciasDTO.getIdContrato()!=null)&&(analiseTendenciasDTO.getIdContrato().intValue()>0)){
													sql.append("and servicocontrato.idcontrato = ? ");
													parametro.add(analiseTendenciasDTO.getIdContrato());
			}
			if ((analiseTendenciasDTO.getIdServico()!=null)&&(analiseTendenciasDTO.getIdServico().intValue()>0)){
													sql.append("and servicocontrato.idservico = ? ");
													parametro.add(analiseTendenciasDTO.getIdServico());
			}
			if ((analiseTendenciasDTO.getIdGrupoExecutor()!=null)&&(analiseTendenciasDTO.getIdGrupoExecutor().intValue()>0)){
													sql.append("and solicitacaoservico.idgrupoatual = ? ");
													parametro.add(analiseTendenciasDTO.getIdGrupoExecutor());
			}

			if ((analiseTendenciasDTO.getIdEmpregado()!=null)&&(analiseTendenciasDTO.getIdEmpregado().intValue()>0)){
													sql.append("and solicitacaoservico.idsolicitante = ? ");
													parametro.add(analiseTendenciasDTO.getIdEmpregado());
			}

			if ((analiseTendenciasDTO.getIdTipoDemandaServico()!=null)&&(analiseTendenciasDTO.getIdTipoDemandaServico().intValue()>0)){
													sql.append("and solicitacaoservico.idtipodemandaservico = ? ");
													parametro.add(analiseTendenciasDTO.getIdTipoDemandaServico());
			}

			if ((analiseTendenciasDTO.getUrgencia()!=null)&&(analiseTendenciasDTO.getUrgencia().length()>0)){
													sql.append("and solicitacaoservico.urgencia = ? ");
													parametro.add(analiseTendenciasDTO.getUrgencia());
			}

			if ((analiseTendenciasDTO.getImpacto()!=null)&&(analiseTendenciasDTO.getImpacto().length()>0)){
													sql.append("and solicitacaoservico.impacto = ? ");
													parametro.add(analiseTendenciasDTO.getImpacto());
			}

			if ((analiseTendenciasDTO.getIdCausaIncidente()!=null)&&(analiseTendenciasDTO.getIdCausaIncidente().intValue()>0)){
													sql.append("and solicitacaoservico.idcausaincidente = ? ");
													parametro.add(analiseTendenciasDTO.getIdCausaIncidente());
			}

			if ((analiseTendenciasDTO.getIdItemConfiguracao()!=null)&&(analiseTendenciasDTO.getIdItemConfiguracao().intValue()>0)){
									sql.append("join itemcfgsolicitacaoserv on solicitacaoservico.idsolicitacaoservico = itemcfgsolicitacaoserv.idsolicitacaoservico and ");
																		sql.append("itemcfgsolicitacaoserv.iditemconfiguracao = ? and ");
																		sql.append("(itemcfgsolicitacaoserv.datafim is null) ");
									parametro.add(analiseTendenciasDTO.getIdItemConfiguracao());
			}

			sql.append("group by solicitacaoservico.idcausaincidente) a on causaincidente.idcausaincidente = a.idcausaincidente ");

			if ((analiseTendenciasDTO.getQtdeCritica() !=null)&&(analiseTendenciasDTO.getQtdeCritica().intValue()>0)){
				sql.append("and qtde >= ? ");
				parametro.add(analiseTendenciasDTO.getQtdeCritica());
			}
			sql.append("order by qtde desc, descricao");

			resp = this.execSQL(sql.toString(), parametro.toArray());
			result = this.engine.listConvertion(getBean(), resp, listRetorno);
		} catch (PersistenceException e) {
			e.printStackTrace();
			result = null;
		} catch (Exception e) {
			e.printStackTrace();
			result = null;
		}
		return (ArrayList<TendenciaDTO>) (((result == null)||(result.size()<=0)) ? new ArrayList<TendenciaDTO>() : result);
	}

	public List<TendenciaDTO> buscarTendenciasItemConfiguracao(AnaliseTendenciasDTO analiseTendenciasDTO){
		List result;
		try {
			List resp = new ArrayList();
			List parametro = new ArrayList();
			List listRetorno = new ArrayList();

			java.sql.Date DataInicio = null;
			Timestamp DataFim = null;

			if (analiseTendenciasDTO.getDataInicio()!=null){
				DataInicio = UtilDatas.getSqlDate(analiseTendenciasDTO.getDataInicio());
			}

			if (analiseTendenciasDTO.getDataFim()!=null){
				DataFim = UtilDatas.getTimeStampComUltimaHoraDoDia(analiseTendenciasDTO.getDataFim());
			}

			listRetorno.add("id");
			listRetorno.add("descricao");
			listRetorno.add("qtdeCritica");

			StringBuilder sql = new StringBuilder();
			sql.append("select ");

			sql.append("itemconfiguracao.iditemconfiguracao id,itemconfiguracao.identificacao descricao, a.qtde qtdeCritica ");
			sql.append("from itemconfiguracao join ");
			                  sql.append("(select itemcfgsolicitacaoserv.iditemconfiguracao,count(solicitacaoservico.idsolicitacaoservico) as qtde ");
			                   sql.append("from solicitacaoservico join servicocontrato on ");
			if ((DataInicio!=null) && (DataFim!=null)){
                									sql.append("(solicitacaoservico.datahorasolicitacao between ? and ?) and ");
                									parametro.add(DataInicio);
                									parametro.add(DataFim);
			}

			sql.append("solicitacaoservico.situacao <> ? and ");
			parametro.add(br.com.centralit.citcorpore.util.Enumerados.SituacaoSolicitacaoServico.Cancelada.getDescricao());

			sql.append("solicitacaoservico.idservicocontrato = servicocontrato.idservicocontrato ");
			if ((analiseTendenciasDTO.getIdContrato()!=null)&&(analiseTendenciasDTO.getIdContrato().intValue()>0)){
													sql.append("and servicocontrato.idcontrato = ? ");
													parametro.add(analiseTendenciasDTO.getIdContrato());
			}
			if ((analiseTendenciasDTO.getIdServico()!=null)&&(analiseTendenciasDTO.getIdServico().intValue()>0)){
													sql.append("and servicocontrato.idservico = ? ");
													parametro.add(analiseTendenciasDTO.getIdServico());
			}
			if ((analiseTendenciasDTO.getIdGrupoExecutor()!=null)&&(analiseTendenciasDTO.getIdGrupoExecutor().intValue()>0)){
													sql.append("and solicitacaoservico.idgrupoatual = ? ");
													parametro.add(analiseTendenciasDTO.getIdGrupoExecutor());
			}

			if ((analiseTendenciasDTO.getIdEmpregado()!=null)&&(analiseTendenciasDTO.getIdEmpregado().intValue()>0)){
													sql.append("and solicitacaoservico.idsolicitante = ? ");
													parametro.add(analiseTendenciasDTO.getIdEmpregado());
			}

			if ((analiseTendenciasDTO.getIdTipoDemandaServico()!=null)&&(analiseTendenciasDTO.getIdTipoDemandaServico().intValue()>0)){
													sql.append("and solicitacaoservico.idtipodemandaservico = ? ");
													parametro.add(analiseTendenciasDTO.getIdTipoDemandaServico());
			}

			if ((analiseTendenciasDTO.getUrgencia()!=null)&&(analiseTendenciasDTO.getUrgencia().length()>0)){
													sql.append("and solicitacaoservico.urgencia = ? ");
													parametro.add(analiseTendenciasDTO.getUrgencia());
			}

			if ((analiseTendenciasDTO.getImpacto()!=null)&&(analiseTendenciasDTO.getImpacto().length()>0)){
													sql.append("and solicitacaoservico.impacto = ? ");
													parametro.add(analiseTendenciasDTO.getImpacto());
			}

			if ((analiseTendenciasDTO.getIdCausaIncidente()!=null)&&(analiseTendenciasDTO.getIdCausaIncidente().intValue()>0)){
													sql.append("and solicitacaoservico.idcausaincidente = ? ");
													parametro.add(analiseTendenciasDTO.getIdCausaIncidente());
			}

			sql.append("join itemcfgsolicitacaoserv on solicitacaoservico.idsolicitacaoservico = itemcfgsolicitacaoserv.idsolicitacaoservico and ");
			sql.append("(itemcfgsolicitacaoserv.datafim is null) ");

			if ((analiseTendenciasDTO.getIdItemConfiguracao()!=null)&&(analiseTendenciasDTO.getIdItemConfiguracao().intValue()>0)){
																		sql.append("and itemcfgsolicitacaoserv.iditemconfiguracao = ? ");
									parametro.add(analiseTendenciasDTO.getIdItemConfiguracao());
			}

			sql.append("group by itemcfgsolicitacaoserv.iditemconfiguracao) a on itemconfiguracao.iditemconfiguracao = a.iditemconfiguracao ");

			if ((analiseTendenciasDTO.getQtdeCritica() !=null)&&(analiseTendenciasDTO.getQtdeCritica().intValue()>0)){
				sql.append("and qtde >= ? ");
				parametro.add(analiseTendenciasDTO.getQtdeCritica());
			}
			sql.append("order by qtde desc, descricao");

			resp = this.execSQL(sql.toString(), parametro.toArray());
			result = this.engine.listConvertion(getBean(), resp, listRetorno);
		} catch (PersistenceException e) {
			e.printStackTrace();
			result = null;
		} catch (Exception e) {
			e.printStackTrace();
			result = null;
		}
		return (ArrayList<TendenciaDTO>) (((result == null)||(result.size()<=0)) ? new ArrayList<TendenciaDTO>() : result);
	}

	/**
	 * alterado por rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a> 
         * data altera��o rcs: 24/09/2015
         * coment�rio altera��o rcs: adequa��o de sql ao SGBD Oracle.
         * 
	 * @param AnaliseTendenciasDTO analiseTendenciasDTO
	 * @return List<TendenciaGanttDTO>
	 */
        public List<TendenciaGanttDTO> listarGraficoGanttServico(final AnaliseTendenciasDTO analiseTendenciasDTO) {
            List<TendenciaGanttDTO> listTendenciaGanttDTO = null;
    
            if (!Objects.equals(analiseTendenciasDTO, null) && !Objects.equals(analiseTendenciasDTO.getDataInicio(), null) && !Objects.equals(analiseTendenciasDTO.getDataFim(), null)) {
                try {
                    List resp                = null;
                    List parametro           = new ArrayList();
                    List listRetorno         = new ArrayList();
                    StringBuilder sql        = new StringBuilder();
                    String fimSqlPostgres    = "";
                    boolean temItemConfig    = (analiseTendenciasDTO.getIdItemConfiguracao() != null) && (analiseTendenciasDTO.getIdItemConfiguracao() > 0);
                    java.sql.Date DataInicio = UtilDatas.getSqlDate(analiseTendenciasDTO.getDataInicio());
                    Timestamp DataFim        = UtilDatas.getTimeStampComUltimaHoraDoDia(analiseTendenciasDTO.getDataFim());
                    String parteSql_castData = null;
                    
                    listRetorno.add("data");
                    listRetorno.add("qtde");
                    
                    if (CITCorporeUtil.SGBD_PRINCIPAL.equalsIgnoreCase(SQLConfig.ORACLE)) {
                        parteSql_castData = " to_date(solicitacaoservico.datahorasolicitacao) ";
                    } else {
                        parteSql_castData = " cast(solicitacaoservico.datahorasolicitacao as DATE) ";
                    }
    
                    // Para o PostgreSQL j� podemos trazer a cole��o completa com as datas que n�o tem ocorr�ncia de solicita��es j� com a qtde zerada!
                    if (CITCorporeUtil.SGBD_PRINCIPAL.equalsIgnoreCase(SQLConfig.POSTGRESQL)) {
                        sql.append("select gdias.data, case when qtde is null then 0 else qtde end as qtde ");
                        sql.append("from ");
                        sql.append("(select generate_series::date as data ");
                        sql.append("from generate_series(?::timestamp, ?, '1 day')) gdias left join (");
                        parametro.add(DataInicio);
                        parametro.add(DataFim);
                        fimSqlPostgres = ") s on gdias.data=s.data";
                    }
    
                    sql.append("select ");
                    sql.append(parteSql_castData);
                    sql.append(" as data, ");
                    sql.append("count(solicitacaoservico.idsolicitacaoservico) as qtde ");
                    sql.append(" from solicitacaoservico ");
                    sql.append(" inner join servicocontrato on solicitacaoservico.idservicocontrato = servicocontrato.idservicocontrato ");
    
                    if (temItemConfig) {
                        sql.append(" inner join itemcfgsolicitacaoserv on solicitacaoservico.idsolicitacaoservico = itemcfgsolicitacaoserv.idsolicitacaoservico ");
                    }
    
                    sql.append(" where solicitacaoservico.datahorasolicitacao between ? and ? ");
                    parametro.add(DataInicio);
                    parametro.add(DataFim);
    
                    if (temItemConfig) {
                        sql.append(" and itemcfgsolicitacaoserv.iditemconfiguracao = ? and itemcfgsolicitacaoserv.datafim is null ");
                        parametro.add(analiseTendenciasDTO.getIdItemConfiguracao());
                    }
    
                    sql.append(" and solicitacaoservico.situacao <> ? ");
                    parametro.add(br.com.centralit.citcorpore.util.Enumerados.SituacaoSolicitacaoServico.Cancelada.getDescricao());
    
                    if ((analiseTendenciasDTO.getIdContrato() != null) && (analiseTendenciasDTO.getIdContrato() > 0)) {
                        sql.append(" and servicocontrato.idcontrato = ? ");
                        parametro.add(analiseTendenciasDTO.getIdContrato());
                    }
                    
                    if ((analiseTendenciasDTO.getIdServico() != null) && (analiseTendenciasDTO.getIdServico() > 0)) {
                        sql.append(" and servicocontrato.idservico = ? ");
                        parametro.add(analiseTendenciasDTO.getIdServico());
                    }
                    
                    if ((analiseTendenciasDTO.getIdGrupoExecutor() != null) && (analiseTendenciasDTO.getIdGrupoExecutor() > 0)) {
                        sql.append(" and solicitacaoservico.idgrupoatual = ? ");
                        parametro.add(analiseTendenciasDTO.getIdGrupoExecutor());
                    }
    
                    if ((analiseTendenciasDTO.getIdEmpregado() != null) && (analiseTendenciasDTO.getIdEmpregado() > 0)) {
                        sql.append(" and solicitacaoservico.idsolicitante = ? ");
                        parametro.add(analiseTendenciasDTO.getIdEmpregado());
                    }
    
                    if ((analiseTendenciasDTO.getIdTipoDemandaServico() != null) && (analiseTendenciasDTO.getIdTipoDemandaServico() > 0)) {
                        sql.append(" and solicitacaoservico.idtipodemandaservico = ? ");
                        parametro.add(analiseTendenciasDTO.getIdTipoDemandaServico());
                    }
    
                    if (StringUtils.isNotBlank(analiseTendenciasDTO.getUrgencia())) {
                        sql.append(" and solicitacaoservico.urgencia = ? ");
                        parametro.add(analiseTendenciasDTO.getUrgencia().trim());
                    }
    
                    if (StringUtils.isNotBlank(analiseTendenciasDTO.getImpacto())) {
                        sql.append(" and solicitacaoservico.impacto = ? ");
                        parametro.add(analiseTendenciasDTO.getImpacto().trim());
                    }
    
                    if ((analiseTendenciasDTO.getIdCausaIncidente() != null) && (analiseTendenciasDTO.getIdCausaIncidente() > 0)) {
                        sql.append(" and solicitacaoservico.idcausaincidente = ? ");
                        parametro.add(analiseTendenciasDTO.getIdCausaIncidente());
                    }
    
                    sql.append(" group by ");
                    sql.append(parteSql_castData);
                    sql.append(" order by data");
                    sql.append(fimSqlPostgres);
    
                    resp = this.execSQL(sql.toString(), parametro.toArray());
                    listTendenciaGanttDTO = this.engine.listConvertion(TendenciaGanttDTO.class, resp, listRetorno);
                    listTendenciaGanttDTO = this.montarPeriodo(analiseTendenciasDTO, listTendenciaGanttDTO);
                } catch (PersistenceException e) {
                    e.printStackTrace();
                    listTendenciaGanttDTO = null;
                } catch (Exception e) {
                    e.printStackTrace();
                    listTendenciaGanttDTO = null;
                }
            }
    
            return listTendenciaGanttDTO;
        }

	public List<TendenciaGanttDTO> listarGraficoGanttCausa(AnaliseTendenciasDTO analiseTendenciasDTO, Integer idCausa){
		List result;
		try {
			List resp = new ArrayList();
			List parametro = new ArrayList();
			List listRetorno = new ArrayList();

			java.sql.Date DataInicio = null;
			Timestamp DataFim = null;

			if (analiseTendenciasDTO.getDataInicio()!=null){
				DataInicio = UtilDatas.getSqlDate(analiseTendenciasDTO.getDataInicio());
			}

			if (analiseTendenciasDTO.getDataFim()!=null){
				DataFim = UtilDatas.getTimeStampComUltimaHoraDoDia(analiseTendenciasDTO.getDataFim());
			}

			//Deve-se ter um per�odo informado
			if ((DataInicio==null) || (DataFim==null)){
				return new ArrayList<TendenciaGanttDTO>();
			}

			listRetorno.add("data");
			listRetorno.add("qtde");

			StringBuilder sql = new StringBuilder();

			//Para o PostgreSQL j� podemos trazer a cole��o completa com as datas que n�o tem ocorr�ncia de solicita��es j� com a qtde zerada!
			if (CITCorporeUtil.SGBD_PRINCIPAL.trim().toUpperCase().equalsIgnoreCase(SQLConfig.POSTGRESQL)){
				sql.append("select gdias.data, case when qtde is null then 0 else qtde end as qtde ");
				sql.append("from ");
				    sql.append("(SELECT generate_series::date as data ");
				     sql.append("FROM generate_series(?::timestamp, ?, '1 day')) gdias left join (");
				parametro.add(DataInicio);
				parametro.add(DataFim);
			}

			sql.append("select CAST(datahorasolicitacao AS DATE) as data, count(solicitacaoservico.idsolicitacaoservico) as qtde ");
			sql.append("from solicitacaoservico join servicocontrato on ");
                									sql.append("(solicitacaoservico.datahorasolicitacao between ? and ?) and ");
                									parametro.add(DataInicio);
                									parametro.add(DataFim);
			sql.append("solicitacaoservico.situacao <> ? and ");
			parametro.add(br.com.centralit.citcorpore.util.Enumerados.SituacaoSolicitacaoServico.Cancelada.getDescricao());

			sql.append("solicitacaoservico.idservicocontrato = servicocontrato.idservicocontrato ");

			if ((analiseTendenciasDTO.getIdContrato()!=null)&&(analiseTendenciasDTO.getIdContrato().intValue()>0)){
													sql.append("and servicocontrato.idcontrato = ? ");
													parametro.add(analiseTendenciasDTO.getIdContrato());
			}

			if ((analiseTendenciasDTO.getIdServico()!=null)&&(analiseTendenciasDTO.getIdServico().intValue()>0)){
				sql.append("and servicocontrato.idservico = ? ");
				parametro.add(analiseTendenciasDTO.getIdServico());
			}

			if ((analiseTendenciasDTO.getIdGrupoExecutor()!=null)&&(analiseTendenciasDTO.getIdGrupoExecutor().intValue()>0)){
													sql.append("and solicitacaoservico.idgrupoatual = ? ");
													parametro.add(analiseTendenciasDTO.getIdGrupoExecutor());
			}

			if ((analiseTendenciasDTO.getIdEmpregado()!=null)&&(analiseTendenciasDTO.getIdEmpregado().intValue()>0)){
													sql.append("and solicitacaoservico.idsolicitante = ? ");
													parametro.add(analiseTendenciasDTO.getIdEmpregado());
			}

			if ((analiseTendenciasDTO.getIdTipoDemandaServico()!=null)&&(analiseTendenciasDTO.getIdTipoDemandaServico().intValue()>0)){
													sql.append("and solicitacaoservico.idtipodemandaservico = ? ");
													parametro.add(analiseTendenciasDTO.getIdTipoDemandaServico());
			}

			if ((analiseTendenciasDTO.getUrgencia()!=null)&&(analiseTendenciasDTO.getUrgencia().length()>0)){
													sql.append("and solicitacaoservico.urgencia = ? ");
													parametro.add(analiseTendenciasDTO.getUrgencia());
			}

			if ((analiseTendenciasDTO.getImpacto()!=null)&&(analiseTendenciasDTO.getImpacto().length()>0)){
													sql.append("and solicitacaoservico.impacto = ? ");
													parametro.add(analiseTendenciasDTO.getImpacto());
			}

			if ((idCausa!=null)&&(idCausa.intValue()>0)){
													sql.append("and solicitacaoservico.idcausaincidente = ? ");
													parametro.add(idCausa);
			}

			if ((analiseTendenciasDTO.getIdItemConfiguracao()!=null)&&(analiseTendenciasDTO.getIdItemConfiguracao().intValue()>0)){
									sql.append("join itemcfgsolicitacaoserv on solicitacaoservico.idsolicitacaoservico = itemcfgsolicitacaoserv.idsolicitacaoservico and ");
																		sql.append("itemcfgsolicitacaoserv.iditemconfiguracao = ? and ");
																		sql.append("(itemcfgsolicitacaoserv.datafim is null) ");
									parametro.add(analiseTendenciasDTO.getIdItemConfiguracao());
			}

			sql.append("group by CAST(datahorasolicitacao AS DATE) ");
			sql.append("order by data");

			//Para o PostgreSQL j� podemos trazer a cole��o completa com as datas que n�o tem ocorr�ncia de solicita��es j� com a qtde zerada!
			if (CITCorporeUtil.SGBD_PRINCIPAL.trim().toUpperCase().equalsIgnoreCase(SQLConfig.POSTGRESQL)){
				sql.append(") s on gdias.data=s.data");
			}

			resp = this.execSQL(sql.toString(), parametro.toArray());
			result = this.engine.listConvertion(TendenciaGanttDTO.class, resp, listRetorno);
		} catch (PersistenceException e) {
			e.printStackTrace();
			result = null;
		} catch (Exception e) {
			e.printStackTrace();
			result = null;
		}
		result = this.montarPeriodo(analiseTendenciasDTO, result);
		return (ArrayList<TendenciaGanttDTO>) (((result == null)||(result.size()<=0)) ? new ArrayList<TendenciaGanttDTO>() : result);
	}

	
        public List<TendenciaGanttDTO> listarGraficoGanttItemConfiguracao(AnaliseTendenciasDTO analiseTendenciasDTO, Integer idItemConfiguracao) {
            List result;
            try {
                List resp = new ArrayList();
                List parametro = new ArrayList();
                List listRetorno = new ArrayList();
    
                java.sql.Date DataInicio = null;
                Timestamp DataFim = null;
    
                if (analiseTendenciasDTO.getDataInicio() != null) {
                    DataInicio = UtilDatas.getSqlDate(analiseTendenciasDTO.getDataInicio());
                }
    
                if (analiseTendenciasDTO.getDataFim() != null) {
                    DataFim = UtilDatas.getTimeStampComUltimaHoraDoDia(analiseTendenciasDTO.getDataFim());
                }
    
                // Deve-se ter um per�odo informado
                if ((DataInicio == null) || (DataFim == null)) {
                    return new ArrayList<TendenciaGanttDTO>();
                }
    
                listRetorno.add("data");
                listRetorno.add("qtde");
    
                StringBuilder sql = new StringBuilder();
    
                // Para o PostgreSQL j� podemos trazer a cole��o completa com as datas que n�o tem ocorr�ncia de solicita��es j� com a qtde zerada!
                if (CITCorporeUtil.SGBD_PRINCIPAL.trim().toUpperCase().equalsIgnoreCase(SQLConfig.POSTGRESQL)) {
                    sql.append("select gdias.data, case when qtde is null then 0 else qtde end as qtde ");
                    sql.append("from ");
                    sql.append("(SELECT generate_series::date as data ");
                    sql.append("FROM generate_series(?::timestamp, ?, '1 day')) gdias left join (");
                    parametro.add(DataInicio);
                    parametro.add(DataFim);
                }
    
                sql.append("select CAST(datahorasolicitacao AS DATE) as data, count(solicitacaoservico.idsolicitacaoservico) as qtde ");
                sql.append("from solicitacaoservico join servicocontrato on ");
                sql.append("(solicitacaoservico.datahorasolicitacao between ? and ?) and ");
                parametro.add(DataInicio);
                parametro.add(DataFim);
                sql.append("solicitacaoservico.situacao <> ? and ");
                parametro.add(br.com.centralit.citcorpore.util.Enumerados.SituacaoSolicitacaoServico.Cancelada.getDescricao());
    
                sql.append("solicitacaoservico.idservicocontrato = servicocontrato.idservicocontrato ");
    
                if ((analiseTendenciasDTO.getIdContrato() != null) && (analiseTendenciasDTO.getIdContrato().intValue() > 0)) {
                    sql.append("and servicocontrato.idcontrato = ? ");
                    parametro.add(analiseTendenciasDTO.getIdContrato());
                }
    
                if ((analiseTendenciasDTO.getIdServico() != null) && (analiseTendenciasDTO.getIdServico().intValue() > 0)) {
                    sql.append("and servicocontrato.idservico = ? ");
                    parametro.add(analiseTendenciasDTO.getIdServico());
                }
    
                if ((analiseTendenciasDTO.getIdGrupoExecutor() != null) && (analiseTendenciasDTO.getIdGrupoExecutor().intValue() > 0)) {
                    sql.append("and solicitacaoservico.idgrupoatual = ? ");
                    parametro.add(analiseTendenciasDTO.getIdGrupoExecutor());
                }
    
                if ((analiseTendenciasDTO.getIdEmpregado() != null) && (analiseTendenciasDTO.getIdEmpregado().intValue() > 0)) {
                    sql.append("and solicitacaoservico.idsolicitante = ? ");
                    parametro.add(analiseTendenciasDTO.getIdEmpregado());
                }
    
                if ((analiseTendenciasDTO.getIdTipoDemandaServico() != null) && (analiseTendenciasDTO.getIdTipoDemandaServico().intValue() > 0)) {
                    sql.append("and solicitacaoservico.idtipodemandaservico = ? ");
                    parametro.add(analiseTendenciasDTO.getIdTipoDemandaServico());
                }
    
                if ((analiseTendenciasDTO.getUrgencia() != null) && (analiseTendenciasDTO.getUrgencia().length() > 0)) {
                    sql.append("and solicitacaoservico.urgencia = ? ");
                    parametro.add(analiseTendenciasDTO.getUrgencia());
                }
    
                if ((analiseTendenciasDTO.getImpacto() != null) && (analiseTendenciasDTO.getImpacto().length() > 0)) {
                    sql.append("and solicitacaoservico.impacto = ? ");
                    parametro.add(analiseTendenciasDTO.getImpacto());
                }
    
                if ((analiseTendenciasDTO.getIdCausaIncidente() != null) && (analiseTendenciasDTO.getIdCausaIncidente().intValue() > 0)) {
                    sql.append("and solicitacaoservico.idcausaincidente = ? ");
                    parametro.add(analiseTendenciasDTO.getIdCausaIncidente());
                }
    
                if ((idItemConfiguracao != null) && (idItemConfiguracao.intValue() > 0)) {
                    sql.append("join itemcfgsolicitacaoserv on solicitacaoservico.idsolicitacaoservico = itemcfgsolicitacaoserv.idsolicitacaoservico and ");
                    sql.append("itemcfgsolicitacaoserv.iditemconfiguracao = ? and ");
                    sql.append("(itemcfgsolicitacaoserv.datafim is null) ");
                    parametro.add(idItemConfiguracao);
                }
    
                sql.append("group by CAST(datahorasolicitacao AS DATE) ");
                sql.append("order by data");
    
                // Para o PostgreSQL j� podemos trazer a cole��o completa com as datas que n�o tem ocorr�ncia de solicita��es j� com a qtde zerada!
                if (CITCorporeUtil.SGBD_PRINCIPAL.trim().toUpperCase().equalsIgnoreCase(SQLConfig.POSTGRESQL)) {
                    sql.append(") s on gdias.data=s.data");
                }
    
                resp = this.execSQL(sql.toString(), parametro.toArray());
                result = this.engine.listConvertion(TendenciaGanttDTO.class, resp, listRetorno);
            } catch (PersistenceException e) {
                e.printStackTrace();
                result = null;
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            }
            result = this.montarPeriodo(analiseTendenciasDTO, (ArrayList<TendenciaGanttDTO>) result);
            return (ArrayList<TendenciaGanttDTO>) (((result == null) || (result.size() <= 0)) ? new ArrayList<TendenciaGanttDTO>() : result);
        }

}
