/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bi.utils;

import java.util.Map;

import br.com.centralit.citcorpore.mail.MensagemEmail;
import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;

/**
 * @author euler.ramos
 *
 */
public class BICitsmartEmailNotificacao {
	String emailGeral;
	String emailConexao;
	String emailFrom;
	Integer idModeloEmail;
	Map<String, String> map;
	boolean notificar;

	public BICitsmartEmailNotificacao(){
		this.emailGeral = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.BICITSMART_EMAIL_NOTIFICACAO_GERAL, null);
		this.emailFrom = "";
		this.notificar = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.BICITSMART_NOTIFICAR_ERRO_IMPORTACAO_POR_EMAIL, "N").equalsIgnoreCase("S");
	}
	
	private void enviaEmailGeral(){
		try {
			MensagemEmail mensagemEmail = new MensagemEmail(this.idModeloEmail, this.map);
			if ((this.emailGeral!=null)&&(this.emailGeral.length()>0)){
				mensagemEmail.envia(this.emailGeral, null, this.emailFrom);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("CITSMART - Problema no envio de notifica��o de erro da Importa��o Autom�tica BICitsmart; e-mail geral: " + this.emailGeral);
		}
	}
	
	private void enviaEmailConexao(){
		try {
			MensagemEmail mensagemEmail = new MensagemEmail(this.idModeloEmail, this.map);
			if ((this.emailConexao!=null)&&(this.emailConexao.length()>0)){
				mensagemEmail.envia(this.emailConexao, null, this.emailFrom);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("CITSMART - Problema no envio de notifica��o de erro da Importa��o Autom�tica BICitsmart; e-mail conex�o: " + this.emailConexao);
		}
	}
	
	public void envia(){
		//Se o par�metro permite a notifica��o
		if (this.notificar){
			if ((this.idModeloEmail!=null)&&(this.idModeloEmail>0)){
				try {
					this.enviaEmailGeral();
					this.enviaEmailConexao();
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("CITSMART - Problema no envio de notifica��o de erro da Importa��o Autom�tica BICitsmart;");
				}
			}
		}
	}

	public void setEmailConexao(String emailConexao) {
		this.emailConexao = emailConexao;
	}

	public void setModeloEmail(String modelo) {
		switch (modelo) {
		case "Exce��o":
			this.idModeloEmail = Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.BICITSMART_ID_MODELO_EMAIL_ERRO_AGEND_EXCECAO, "0").trim());
			break;
		case "Espec�fico":
			this.idModeloEmail = Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.BICITSMART_ID_MODELO_EMAIL_ERRO_AGEND_ESPECIFICO, "0").trim());
			break;
		case "Padr�o":
			this.idModeloEmail = Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.BICITSMART_ID_MODELO_EMAIL_ERRO_AGEND_PADRAO, "0").trim());
			break;
		case "Par�metro":
			this.idModeloEmail = Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.BICITSMART_ID_MODELO_EMAIL_ERRO_PARAMETRO, "0").trim());
			break;
		case "Problema":
			this.idModeloEmail = Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.BICITSMART_ID_MODELO_EMAIL_ERRO_EXCECUCAO, "0").trim());
			break;			
		default:
			this.idModeloEmail = 0;
		}
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}
	
}
