/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.regras;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;

import br.com.centralit.citcorpore.bean.ProgramacaoAtividadeDTO;
import br.com.centralit.citcorpore.integracao.FeriadoDao;
import br.com.centralit.citcorpore.util.Enumerados.TipoAgendamento;
import br.com.centralit.citcorpore.util.Util;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.util.UtilDatas;

public abstract class RegraProgramacaoAtividade implements Serializable {
    
    public static RegraProgramacaoAtividade getRegraFromDto(ProgramacaoAtividadeDTO programacaoAtividadeDto) throws Exception {
        return (RegraProgramacaoAtividade) Class.forName(RegraProgramacaoAtividade.class.getName()+"_"+programacaoAtividadeDto.getTipoAgendamento()).newInstance();
    }
    
    public static java.util.Date getDataProximaExecucao(ProgramacaoAtividadeDTO programacaoAtividadeDto, java.util.Date dataRef) throws Exception {
        return getRegraFromDto(programacaoAtividadeDto).calculaProximaExecucao(programacaoAtividadeDto, dataRef);
    }
    

    public static Integer[] getDiasUteis(int mes, int ano) throws Exception {
        FeriadoDao feriadoDao = new FeriadoDao();
        Integer[] diasUteis = new Integer[100];
        for (int i = 0; i < diasUteis.length; i++) 
            diasUteis[i] = new Integer(0);
        int m = 1;
        java.util.Date dataRef = Util.getData(1, mes, ano);
        while (UtilDatas.getMonth(dataRef) == mes) {
            int diaSemana = Util.getDiaSemana(dataRef); 
            if (diaSemana != 1 && diaSemana != 7 && !feriadoDao.isFeriado(new java.sql.Date(dataRef.getTime()), null, null)) {
                diasUteis[m] = Util.getDay(dataRef);
                m++;
            }
            dataRef = UtilDatas.alteraData(dataRef, 1, Calendar.DAY_OF_MONTH);
        }
        diasUteis[99] = diasUteis[m-1];
        return diasUteis;
    }    
    
    public static HashMap getSemanas(int mes, int ano) throws Exception {
        Integer[] contSemanas = new Integer[] {0,0,0,0,0,0,0,0};
        HashMap<Integer,Integer[]> mapSemanas = new HashMap();
        mapSemanas.put(new Integer(1), new Integer[]{0,0,0,0,0,0,0});
        mapSemanas.put(new Integer(2), new Integer[]{0,0,0,0,0,0,0});
        mapSemanas.put(new Integer(3), new Integer[]{0,0,0,0,0,0,0});
        mapSemanas.put(new Integer(4), new Integer[]{0,0,0,0,0,0,0});
        mapSemanas.put(new Integer(5), new Integer[]{0,0,0,0,0,0,0});
        mapSemanas.put(new Integer(6), new Integer[]{0,0,0,0,0,0,0});
        mapSemanas.put(new Integer(7), new Integer[]{0,0,0,0,0,0,0});
        
        java.util.Date dataRef = Util.getData(1, mes, ano);
        while (UtilDatas.getMonth(dataRef) == mes) {
            int diaSemana = Util.getDiaSemana(dataRef);
            contSemanas[diaSemana] = contSemanas[diaSemana].intValue() + 1;
            int semana = contSemanas[diaSemana].intValue();
            Integer[] dias = mapSemanas.get(diaSemana);
            dias[semana] = Util.getDay(dataRef);
            dataRef = UtilDatas.alteraData(dataRef, 1, Calendar.DAY_OF_MONTH);
        }
        return mapSemanas;
    } 
    
    public static void validaProgramacao(ProgramacaoAtividadeDTO programacaoAtividadeDto) throws Exception {
        if (programacaoAtividadeDto.getTipoAgendamento() == null) 
            throw new LogicException("Tipo de agendamento n�o foi informado");
        if (programacaoAtividadeDto.getDataInicio() == null) 
            throw new LogicException("Data de in�cio n�o foi informada");
        if (programacaoAtividadeDto.getHoraInicio() == null || programacaoAtividadeDto.getHoraInicio().length() < 4) 
            throw new LogicException("Hora de in�cio n�o foi informada");
        if (!Util.isHoraValida(programacaoAtividadeDto.getHoraInicio()))
            throw new LogicException("Hora de in�cio inv�lida");
        if (programacaoAtividadeDto.getDuracaoEstimada() == null) 
            throw new LogicException("Dura��o estimada n�o foi informada");
        if (programacaoAtividadeDto.getDataFim() != null && programacaoAtividadeDto.getDataFim().compareTo(programacaoAtividadeDto.getDataInicio()) < 0)
            throw new LogicException("A data de t�rmino � maior que a data de in�cio");
        if (programacaoAtividadeDto.getRepeticao() == null)
            programacaoAtividadeDto.setRepeticao("N");
        if (programacaoAtividadeDto.getRepeticao().equals("S")) {
            if (programacaoAtividadeDto.getRepeticaoIntervalo() == null)
                throw new LogicException("Intervalo de repeti��o n�o foi informado");
            if (programacaoAtividadeDto.getRepeticaoTipoIntervalo() == null)
                throw new LogicException("Tipo de intervalo da repeti��o n�o foi informado");
            if (programacaoAtividadeDto.getHoraFim() == null || programacaoAtividadeDto.getHoraFim().length() < 4)
                throw new LogicException("Hora de t�rmino da repeti��o n�o foi informada");
            if (!Util.isHoraValida(programacaoAtividadeDto.getHoraFim()))
                throw new LogicException("Hora de t�rmino da repeti��o inv�lida");
        }
        RegraProgramacaoAtividade regraImpl = getRegraFromDto(programacaoAtividadeDto);
        regraImpl.valida(programacaoAtividadeDto);
        setDescricao(programacaoAtividadeDto);
    }
    
    public static void setDescricao(ProgramacaoAtividadeDTO programacaoAtividadeDto) throws Exception {
        RegraProgramacaoAtividade regraImpl = getRegraFromDto(programacaoAtividadeDto);
        programacaoAtividadeDto.setTipoAgendamentoDescr(TipoAgendamento.valueOf(programacaoAtividadeDto.getTipoAgendamento()).getDescricao());
        programacaoAtividadeDto.setDetalhamento(regraImpl.getDetalhamento(programacaoAtividadeDto));
        programacaoAtividadeDto.setRepeticaoDescr(regraImpl.getRepeticaoDescr(programacaoAtividadeDto));
        programacaoAtividadeDto.setDuracaoEstimadaDescr(programacaoAtividadeDto.getDuracaoEstimada().intValue()+" minuto(s)");
    }
    
    public String getRepeticaoDescr(ProgramacaoAtividadeDTO programacaoAtividadeDto) throws Exception {
        String descricao = "";
        if (programacaoAtividadeDto.getRepeticao().equals("S")) {
            descricao = "A cada "+programacaoAtividadeDto.getRepeticaoIntervalo().intValue();
            if (programacaoAtividadeDto.getRepeticaoTipoIntervalo().equals("M"))
                descricao += " minuto(s)";
            else
                descricao += " hora(s)";
            descricao += " at� "+programacaoAtividadeDto.getHoraFim();
        }
        return descricao;
    }
    
    public abstract java.util.Date calculaProximaExecucao(ProgramacaoAtividadeDTO programacaoAtividadeDto, java.util.Date dataRef) throws Exception;
    public abstract String getDetalhamento(ProgramacaoAtividadeDTO programacaoAtividadeDto) throws Exception;
    public abstract void valida(ProgramacaoAtividadeDTO programacaoAtividadeDto) throws Exception;

}
