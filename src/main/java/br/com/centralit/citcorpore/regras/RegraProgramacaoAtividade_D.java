/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.regras;

import java.util.Calendar;

import br.com.centralit.citcorpore.bean.ProgramacaoAtividadeDTO;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.util.UtilDatas;

public class RegraProgramacaoAtividade_D extends RegraProgramacaoAtividade {
    
    public java.util.Date calculaProximaExecucao(ProgramacaoAtividadeDTO programacaoAtividadeDto, java.util.Date dataRef) throws Exception {
        java.util.Date proximaExecucao = null;        
        java.util.Date dataExecucao = programacaoAtividadeDto.getProximaExecucao();                
        if (dataExecucao == null)  
            dataExecucao = programacaoAtividadeDto.getDataInicio();
        if (dataExecucao.compareTo(dataRef) <= 0 && (programacaoAtividadeDto.getDataFim() == null || programacaoAtividadeDto.getDataFim().compareTo(dataRef) >= 0)) {
            while (dataExecucao.compareTo(dataRef) < 0) 
                dataExecucao = UtilDatas.alteraData(dataExecucao, programacaoAtividadeDto.getPeriodicidadeDiaria().intValue(), Calendar.DAY_OF_MONTH);
            proximaExecucao = dataExecucao;
        }
        return proximaExecucao;
    }
    
    public void valida(ProgramacaoAtividadeDTO programacaoAtividadeDto) throws Exception {
        if (programacaoAtividadeDto.getPeriodicidadeDiaria() == null)
            throw new LogicException("Periodicidade da programa��o di�ria n�o foi informada");
    } 
    
    public String getDetalhamento(ProgramacaoAtividadeDTO programacaoAtividadeDto) throws Exception {
        String descricao = "";      
        if (programacaoAtividadeDto.getPeriodicidadeDiaria().intValue() == 1)
            descricao = "Todo dia �s "+programacaoAtividadeDto.getHoraInicio();
        else
            descricao = "�s "+programacaoAtividadeDto.getHoraInicio()+" a cada "+programacaoAtividadeDto.getPeriodicidadeDiaria().intValue()+" dias";
        return descricao;
    }
    
}
