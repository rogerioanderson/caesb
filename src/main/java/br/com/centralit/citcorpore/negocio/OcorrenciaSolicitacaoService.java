/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import br.com.centralit.citcorpore.bean.OcorrenciaSolicitacaoDTO;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudService;

/**
 * @author breno.guimaraes
 *
 */
public interface OcorrenciaSolicitacaoService extends CrudService {
    public Collection findByIdSolicitacaoServico(Integer idSolicitacaoServicoParm) throws Exception;
    public OcorrenciaSolicitacaoDTO findUltimoByIdSolicitacaoServico(Integer idSolicitacaoServicoParm) throws Exception;
    public OcorrenciaSolicitacaoDTO findUltimoByIdSolicitacaoAndCategoria(Integer idSolicitacaoServicoParm, String categoria, TransactionControler tc) throws Exception;
    public Collection findOcorrenciasDoTesteIdSolicitacaoServico(Integer idSolicitacaoServicoParm) throws Exception;
    public Collection<OcorrenciaSolicitacaoDTO> findByIdPessoaEDataAtendidasGrupoTeste(Integer idPessoa, Date dataInicio, Date dataFim) throws Exception;
    public Collection<OcorrenciaSolicitacaoDTO> findByIdPessoaGrupoTeste(Integer idPessoa, Date dataInicio, Date dataFim) throws Exception;
    public OcorrenciaSolicitacaoDTO findByIdOcorrencia(Integer idOcorrencia) throws Exception;
    public long quantidadeDeOcorrenciasDeAlteracaoSlaPorNumeroDaSolicitacao(int idSolicitacaoServico) throws Exception;
    
    public void updateDataHora(OcorrenciaSolicitacaoDTO ocorrencia, Date data, String hora, TransactionControler tc) throws Exception;
    public List<OcorrenciaSolicitacaoDTO> findNaoSincronizadas(Integer idSolicitacaoServico, String origemSincronizacao, List<String> categorias) throws PersistenceException;
    
    public void updateNotNull(OcorrenciaSolicitacaoDTO ocorrencia, TransactionControler tc) throws PersistenceException;
}
