/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;

import br.com.centralit.citcorpore.bean.RelatorioSolicitacaoReabertaDTO;
import br.com.centralit.citcorpore.integracao.RelatorioSolicitacaoReabertaDAO;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilStrings;

@SuppressWarnings("unchecked")
public class RelatorioSolicitacaoReabertaServiceEjb extends CrudServiceImpl implements RelatorioSolicitacaoReabertaService {
    private RelatorioSolicitacaoReabertaDAO dao;
    private RelatorioSolicitacaoReabertaDAO daoReports;

    @Override
    protected RelatorioSolicitacaoReabertaDAO getDao() {
        if (dao == null) {
            dao = new RelatorioSolicitacaoReabertaDAO();
        }
        return dao;
    }
    
	/**
	 * Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 11:11 - ID Citsmart: 176362 -
	 * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
	 * @param databaseAlias
	 * @return dao com o databaseAlias especificado, caso esteja preenchido, sen�o dao com o datasource principal
	 */
    private RelatorioSolicitacaoReabertaDAO getDao(final String databaseAlias) {
		if (UtilStrings.isNotVazio(databaseAlias)) {
			if (daoReports == null) {
				daoReports = new RelatorioSolicitacaoReabertaDAO(databaseAlias);
			}
			return daoReports;
		}
		return this.getDao();
	}


    @Override
    public ArrayList<RelatorioSolicitacaoReabertaDTO> listSolicitacaoReaberta(final RelatorioSolicitacaoReabertaDTO relatorioSolicitacaoReabertaDTO) {
    	 // Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 11:13 - ID Citsmart: 176362 -
    	 // Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
    	 // @param databaseAlias
    	 // @return dao com o databaseAlias especificado, caso esteja preenchido, sen�o dao com o datasource principal
    	 
        return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).listSolicitacaoReaberta(relatorioSolicitacaoReabertaDTO);
    }

}
