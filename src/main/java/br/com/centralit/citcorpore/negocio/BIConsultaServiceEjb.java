/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.Iterator;

import br.com.centralit.citcorpore.bean.BIConsultaColunasDTO;
import br.com.centralit.citcorpore.bean.BIConsultaDTO;
import br.com.centralit.citcorpore.integracao.BIConsultaColunasDao;
import br.com.centralit.citcorpore.integracao.BIConsultaDao;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.CrudDAO;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;

public class BIConsultaServiceEjb extends CrudServiceImpl implements BIConsultaService {

    private BIConsultaDao dao;

    @Override
    protected BIConsultaDao getDao() {
        if (dao == null) {
            dao = new BIConsultaDao();
        }
        return dao;
    }

    @Override
    public IDto create(IDto model) throws ServiceException, LogicException {
        // Instancia Objeto controlador de transacao
        final CrudDAO crudDao = this.getDao();
        final BIConsultaColunasDao biConsultaColunasDao = new BIConsultaColunasDao();
        final TransactionControler tc = new TransactionControlerImpl(crudDao.getAliasDB());
        try {
            // Faz validacao, caso exista.
            this.validaCreate(model);

            // Instancia ou obtem os DAOs necessarios.

            // Seta o TransactionController para os DAOs
            crudDao.setTransactionControler(tc);
            biConsultaColunasDao.setTransactionControler(tc);

            // Inicia transacao
            tc.start();

            // Executa operacoes pertinentes ao negocio.
            model = crudDao.create(model);
            final BIConsultaDTO biConsultaDTO = (BIConsultaDTO) model;
            if (biConsultaDTO.getColColunas() != null) {
                for (final Iterator it = biConsultaDTO.getColColunas().iterator(); it.hasNext();) {
                    final BIConsultaColunasDTO biConsultaColunasDTO = (BIConsultaColunasDTO) it.next();
                    biConsultaColunasDTO.setIdConsulta(biConsultaDTO.getIdConsulta());
                    biConsultaColunasDao.create(biConsultaColunasDTO);
                }
            }

            // Faz commit e fecha a transacao.
            tc.commit();

            return model;
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        } finally {
            tc.closeQuietly();
        }
        return model;
    }

    @Override
    public void update(final IDto model) throws ServiceException, LogicException {
        // Instancia Objeto controlador de transacao
        final CrudDAO crudDao = this.getDao();
        final BIConsultaColunasDao biConsultaColunasDao = new BIConsultaColunasDao();
        final TransactionControler tc = new TransactionControlerImpl(crudDao.getAliasDB());
        try {
            // Faz validacao, caso exista.
            this.validaUpdate(model);

            // Seta o TransactionController para os DAOs
            crudDao.setTransactionControler(tc);
            biConsultaColunasDao.setTransactionControler(tc);

            // Inicia transacao
            tc.start();

            // Executa operacoes pertinentes ao negocio.
            crudDao.update(model);
            final BIConsultaDTO biConsultaDTO = (BIConsultaDTO) model;
            biConsultaColunasDao.deleteByIdConsulta(biConsultaDTO.getIdConsulta());
            if (biConsultaDTO.getColColunas() != null) {
                for (final Iterator it = biConsultaDTO.getColColunas().iterator(); it.hasNext();) {
                    final BIConsultaColunasDTO biConsultaColunasDTO = (BIConsultaColunasDTO) it.next();
                    biConsultaColunasDTO.setIdConsulta(biConsultaDTO.getIdConsulta());
                    biConsultaColunasDao.create(biConsultaColunasDTO);
                }
            }

            // Faz commit e fecha a transacao.
            tc.commit();

        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        } finally {
            tc.closeQuietly();
        }
    }

    @Override
    public Collection findByIdCategoria(final Integer parm) throws Exception {
        return this.getDao().findByIdCategoria(parm);
    }

    @Override
    public BIConsultaDTO getByIdentificacao(final String ident) throws Exception {
        return this.getDao().getByIdentificacao(ident);
    }

}
