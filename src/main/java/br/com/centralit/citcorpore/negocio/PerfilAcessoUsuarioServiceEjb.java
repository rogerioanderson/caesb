/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.PerfilAcessoUsuarioDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.PerfilAcessoUsuarioDAO;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

public class PerfilAcessoUsuarioServiceEjb extends CrudServiceImpl implements PerfilAcessoUsuarioService {

    private PerfilAcessoUsuarioDAO dao;

    @Override
    protected PerfilAcessoUsuarioDAO getDao() {
        if (dao == null) {
            dao = new PerfilAcessoUsuarioDAO();
        }
        return dao;
    }

    @Override
    public void reativaPerfisUsuario(final Integer idUsuario) {
        try {
            this.getDao().reativaPerfisUsuario(idUsuario);
        } catch (final PersistenceException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void validaCreate(final Object arg0) throws Exception {
        final PerfilAcessoUsuarioDTO dto = (PerfilAcessoUsuarioDTO) arg0;
        if (dto.getDataInicio() == null) {
            dto.setDataInicio(UtilDatas.getDataAtual());
        }
    }

    @Override
    public PerfilAcessoUsuarioDTO listByIdUsuario(final PerfilAcessoUsuarioDTO obj) throws Exception {
        try {
            return this.getDao().listByIdUsuario(obj);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public PerfilAcessoUsuarioDTO obterPerfilAcessoUsuario(final UsuarioDTO usuario) throws Exception {
        return this.getDao().obterPerfilAcessoUsuario(usuario);
    }

    @Override
    public Collection obterPerfisAcessoUsuario(final UsuarioDTO usuario) throws Exception {
        try {
            return this.getDao().listPerfilByIdUsuario(usuario);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }
    
    /**
     * @author rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
     * @param usuario
     * @return boolean
     * @since 25/06/2015
     */
    public boolean verificarSeUsuarioPossuiPerfilAcessoEspecifico(UsuarioDTO usuario) {
        boolean usuarioTemPerfilAcesso;
        try {
            usuarioTemPerfilAcesso = this.getDao().verificarSeUsuarioPossuiPerfilAcessoEspecifico(usuario);
        } catch (IllegalArgumentException illegArgExc) {
            usuarioTemPerfilAcesso = false;
            illegArgExc.printStackTrace();
        } catch (PersistenceException persExc) {
            usuarioTemPerfilAcesso = false;
            persExc.printStackTrace();
        } catch (Exception exc) {
            usuarioTemPerfilAcesso = false;
            exc.printStackTrace();
        }

        return usuarioTemPerfilAcesso;
    }

}
