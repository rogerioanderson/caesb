/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.List;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.ExternalConnectionDTO;
import br.com.centralit.citcorpore.bean.ImportManagerDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

/**
 * @author thiago.borges
 *
 */
public interface ExternalConnectionService extends CrudService {

    Collection getTables(final Integer idExternalConnection) throws Exception;

    Collection getLocalTables() throws Exception;

    Collection getFieldsTable(final Integer idExternalConnection, final String tableName) throws Exception;

    Collection getFieldsLocalTable(final String tableName) throws Exception;

    void processImport(final ImportManagerDTO importManagerDTO, final List colMatrizTratada) throws Exception;

    /**
     * Exclui Conexao
     *
     * @param model
     * @param document
     * @throws ServiceException
     * @throws Exception
     */
    public void deletarConexao(final IDto model, final DocumentHTML document) throws ServiceException, Exception;

    /**
     * Consultar Conexoes Ativas
     *
     * @param obj
     * @return
     * @throws Exception
     * @author Thiago.Borges
     */
    boolean consultarConexoesAtivas(final ExternalConnectionDTO obj) throws Exception;

    Collection<ExternalConnectionDTO> seConexaoJaCadastrada(final ExternalConnectionDTO conexoesDTO) throws Exception;

    Collection<ExternalConnectionDTO> listarAtivas() throws Exception;

}
