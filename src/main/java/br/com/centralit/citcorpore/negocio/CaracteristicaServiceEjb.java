/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITCorpore.
 *
 * @author CentralIT
 */
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citcorpore.bean.CaracteristicaDTO;
import br.com.centralit.citcorpore.bean.ValorDTO;
import br.com.centralit.citcorpore.integracao.CaracteristicaDao;
import br.com.centralit.citcorpore.integracao.CaracteristicaTipoItemConfiguracaoDAO;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;

/**
 * EJB de Caracter�stica Service.
 *
 * @author valdoilo.damasceno
 *
 */
public class CaracteristicaServiceEjb extends CrudServiceImpl implements CaracteristicaService {

    private CaracteristicaDao dao;

    @Override
    protected CaracteristicaDao getDao() {
        if (dao == null) {
            dao = new CaracteristicaDao();
        }
        return dao;
    }

    @Override
    public void create(final CaracteristicaDTO caracteristica, final HttpServletRequest request) throws ServiceException, LogicException {
        /*
		 * Retirado da rotina para valida��o da pink elephant caracteristica.setSistema("N"); Acrescentado novamente por apresentar erro ao gravar caracter�stica. No Oracle, campo n�o pode ser Null.
		 */
        caracteristica.setSistema("N");
		caracteristica.setNome(caracteristica.getNome().trim().replaceAll("[<>]", ""));
		caracteristica.setTag(caracteristica.getTag().trim().replaceAll("[<>]", ""));
		caracteristica.setDescricao(caracteristica.getDescricao() != null ? caracteristica.getDescricao().trim() : null);
        caracteristica.setDataInicio(UtilDatas.getDataAtual());
        caracteristica.setTipo("");
        caracteristica.setIdEmpresa(WebUtil.getIdEmpresa(request));
        super.create(caracteristica);
    }

	/*
	 * Desenvolvedor: Fabio Amorim - Data: 29/05/2015 - Hor�rio: 15:56 - ID Citsmart: 155312 - Motivo/Coment�rio: Remover espa�o no in�cio e fim dos campos texto.
	 */
	@Override
	public void update(IDto model) throws ServiceException, LogicException {
		CaracteristicaDTO caracteristicaDTO = (CaracteristicaDTO) model;
		caracteristicaDTO.setNome(caracteristicaDTO.getNome().trim());
		caracteristicaDTO.setDescricao(caracteristicaDTO.getDescricao().trim());
		caracteristicaDTO.setDescricao(caracteristicaDTO.getDescricao() != null ? caracteristicaDTO.getDescricao().trim() : null);
		super.update(caracteristicaDTO);
	}

    @Override
    public void excluirCaracteristica(final CaracteristicaDTO caracteristica) throws ServiceException, Exception {
        if (this.getCaracteristicaTipoItemConfiguracaoDao().existeAssociacaoComCaracteristica(caracteristica.getIdCaracteristica(), null)) {
			throw new LogicException("Caracter�stica n�o pode ser exclu�da!");
        } else {
            caracteristica.setDataFim(UtilDatas.getDataAtual());
            super.update(caracteristica);
        }
    }

    @SuppressWarnings({ "unchecked"})
    @Override
    public Collection<CaracteristicaDTO> consultarCaracteristicasAtivas(final Integer idTipoItemConfiguracao) throws ServiceException {
        try {
            return this.getDao().consultarCaracteristicasAtivas(idTipoItemConfiguracao);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @SuppressWarnings({ "unchecked"})
    @Override
    public Collection<CaracteristicaDTO> consultarCaracteristicasAtivas(final Integer idTipoItemConfiguracao, final String[] arrCaracteristicas) throws ServiceException {
        try {
            return this.getDao().consultarCaracteristicasAtivas(idTipoItemConfiguracao, arrCaracteristicas);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    /*
     * (non-Javadoc)
     * @see br.com.centralit.citcorpore.negocio.CaracteristicaService# consultarCaracteristicasComValores(java.lang.Integer, java.lang.Integer)
     */
    @Override
    public Collection<CaracteristicaDTO> consultarCaracteristicasComValores(final Integer idTipoItemConfiguracao, final Integer idBaseItemConfiguracao) throws LogicException,
            ServiceException, Exception {
        final Collection<CaracteristicaDTO> caracteristicas = this.consultarCaracteristicasAtivas(idTipoItemConfiguracao);

        for (final CaracteristicaDTO caracteristica : caracteristicas) {
            final ValorDTO valor = this.getValorService().restore(idBaseItemConfiguracao, caracteristica.getIdCaracteristica());

            if (valor != null && valor.getValorStr() != null) {
                caracteristica.setValorString(valor.getValorStr());
            }
        }
        return caracteristicas;
    }

    /*
     * (non-Javadoc)
     * @see br.com.centralit.citcorpore.negocio.CaracteristicaService# consultarCaracteristicasComValores(java.lang.Integer, java.lang.Integer)
     */
    @Override
    public Collection<CaracteristicaDTO> consultarCaracteristicasComValoresItemConfiguracao(final Integer idTipoItemConfiguracao, final Integer idItemConfiguracao)
            throws LogicException, ServiceException, Exception {
        final Collection<CaracteristicaDTO> caracteristicas = this.consultarCaracteristicasAtivas(idTipoItemConfiguracao);
        if (caracteristicas != null) {
            for (final CaracteristicaDTO caracteristica : caracteristicas) {
                final ValorDTO valor = this.getValorService().restoreItemConfiguracao(idItemConfiguracao, caracteristica.getIdCaracteristica());

                if (valor != null && valor.getValorStr() != null) {
                    caracteristica.setValorString(valor.getValorStr());
                }
            }
        }
        return caracteristicas;
    }

    @Override
    public Collection<CaracteristicaDTO> consultarCaracteristicasComValoresItemConfiguracao(final Integer idTipoItemConfiguracao, final Integer idItemConfiguracao,
            final String[] arr) throws LogicException, ServiceException, Exception {
        final Collection<CaracteristicaDTO> caracteristicas = this.consultarCaracteristicasAtivas(idTipoItemConfiguracao, arr);
        if (caracteristicas != null) {
            for (final CaracteristicaDTO caracteristica : caracteristicas) {
                final ValorDTO valor = this.getValorService().restoreItemConfiguracao(idItemConfiguracao, caracteristica.getIdCaracteristica());

                if (valor != null && valor.getValorStr() != null) {
                    caracteristica.setValorString(valor.getValorStr());
                }
            }
        }
        return caracteristicas;
    }

    /**
     * Retorna Service de Valor.
     *
     * @return ValorService
     * @throws ServiceException
     * @throws Exception
     * @author valdoilo.damasceno
     */
    public ValorService getValorService() throws ServiceException, Exception {
        return (ValorService) ServiceLocator.getInstance().getService(ValorService.class, null);
    }

    /**
     * Retorna DAO de CaracteristicaTipoItemConfiguracao.
     *
     * @return CaracteristicaTipoItemConfiguracaoDAO
     * @author VMD
     */
    public CaracteristicaTipoItemConfiguracaoDAO getCaracteristicaTipoItemConfiguracaoDao() {
        return new CaracteristicaTipoItemConfiguracaoDAO();
    }

    @Override
    public boolean verificarSeCaracteristicaExiste(final CaracteristicaDTO caracteristica) throws PersistenceException {
        return this.getDao().verificarSeCaracteristicaExiste(caracteristica);
    }

}
