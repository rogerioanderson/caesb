/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.Iterator;

import br.com.centralit.citcorpore.bean.MarcoPagamentoPrjDTO;
import br.com.centralit.citcorpore.integracao.MarcoPagamentoPrjDao;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

public class MarcoPagamentoPrjServiceEjb extends CrudServiceImpl implements MarcoPagamentoPrjService {

    private MarcoPagamentoPrjDao dao;

    @Override
    protected MarcoPagamentoPrjDao getDao() {
        if (dao == null) {
            dao = new MarcoPagamentoPrjDao();
        }
        return dao;
    }

    @Override
    public Collection findByIdProjeto(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdProjeto(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdProjeto(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdProjeto(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void saveFromCollection(final Collection colItens, final Integer idProjetoParm) throws ServiceException, LogicException {
        // Instancia Objeto controlador de transacao
        final MarcoPagamentoPrjDao crudDao = new MarcoPagamentoPrjDao();
        final TransactionControler tc = new TransactionControlerImpl(crudDao.getAliasDB());
        try {
            // Seta o TransactionController para os DAOs
            crudDao.setTransactionControler(tc);

            // Inicia transacao
            tc.start();

            for (final Iterator it = colItens.iterator(); it.hasNext();) {
                MarcoPagamentoPrjDTO model = (MarcoPagamentoPrjDTO) it.next();

                model.setDataUltAlteracao(UtilDatas.getDataAtual());
                final String hora = UtilDatas.getHoraHHMM(UtilDatas.getDataHoraAtual()).replaceAll(":", "");
                model.setHoraUltAlteracao(hora);
                model.setUsuarioUltAlteracao(usuario.getNomeUsuario());
                if (model.getIdMarcoPagamentoPrj() != null && model.getIdMarcoPagamentoPrj().intValue() > 0) {
                    // Faz validacao, caso exista.
                    this.validaUpdate(model);

                    // Executa operacoes pertinentes ao negocio.
                    crudDao.updateNotNull(model);
                } else {
                    model.setSituacao("E");

                    // Faz validacao, caso exista.
                    this.validaCreate(model);

                    // Executa operacoes pertinentes ao negocio.
                    model = (MarcoPagamentoPrjDTO) crudDao.create(model);
                }
            }
            crudDao.deleteByIdProjetoNotIn(idProjetoParm, colItens);

            // Faz commit e fecha a transacao.
            tc.commit();
            tc.close();
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        }
    }

}
