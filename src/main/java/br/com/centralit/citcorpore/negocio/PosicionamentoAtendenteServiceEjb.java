/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.com.centralit.citcorpore.bean.PosicionamentoAtendenteDTO;
import br.com.centralit.citcorpore.bean.result.PosicionamentoAtendenteResultDTO;
import br.com.centralit.citcorpore.integracao.PosicionamentoAtendenteDAO;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.Assert;
import br.com.citframework.util.UtilDatas;

import com.google.common.base.Function;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

public class PosicionamentoAtendenteServiceEjb extends CrudServiceImpl implements PosicionamentoAtendenteService {

    private PosicionamentoAtendenteDAO dao;

    @Override
    protected PosicionamentoAtendenteDAO getDao() {
        if (dao == null) {
            dao = new PosicionamentoAtendenteDAO();
        }
        return dao;
    }

    private static final Function<PosicionamentoAtendenteResultDTO, Integer> groupUsuarioFunction = new Function<PosicionamentoAtendenteResultDTO, Integer>() {

        @Override
        public Integer apply(final PosicionamentoAtendenteResultDTO item) {
            return item.getIdUsuario();
        }

    };

    @Override
    public List<PosicionamentoAtendenteResultDTO> listLastLocationWithSolicitationInfo(final PosicionamentoAtendenteDTO posicionamentoAtendente) throws Exception {
        Assert.notNull(posicionamentoAtendente, "'PosicionamentoAtendente' must not be null.");
        final List<PosicionamentoAtendenteResultDTO> onDB = this.getDao().listLastLocationWithSolicitationInfo(posicionamentoAtendente);
        for (final PosicionamentoAtendenteResultDTO posicionamentoAtendenteResult : onDB) {
            final Timestamp ultimaVisualizacao = posicionamentoAtendenteResult.getUltimaVisualizacao();
            posicionamentoAtendenteResult.setUltimaVisualizacao(null);
            posicionamentoAtendenteResult.setLastSeem(UtilDatas.convertDateToString(TipoDate.TIMESTAMP_WITH_SECONDS, ultimaVisualizacao));
        }

        final List<PosicionamentoAtendenteResultDTO> posicionamentos = new ArrayList<>();
        if (onDB != null) {
            final Multimap<Integer, PosicionamentoAtendenteResultDTO> maped = Multimaps.index(onDB.iterator(), groupUsuarioFunction);
            final Set<Integer> ids = maped.keySet();
            for (final Integer integer : ids) {
                posicionamentos.add(((List<PosicionamentoAtendenteResultDTO>) maped.get(integer)).get(0));
            }
        }

        return posicionamentos;
    }

}
