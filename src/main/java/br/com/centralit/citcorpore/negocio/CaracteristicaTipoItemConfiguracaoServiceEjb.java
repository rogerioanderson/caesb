/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart.
 *
 * @author valdoilo.damasceno
 */
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.CaracteristicaDTO;
import br.com.centralit.citcorpore.integracao.CaracteristicaTipoItemConfiguracaoDAO;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.service.ServiceLocator;

/**
 * EJB de CaracteristicaTipoItemConfiguracao.
 *
 * @author valdoilo.damasceno
 */
public class CaracteristicaTipoItemConfiguracaoServiceEjb extends CrudServiceImpl implements CaracteristicaTipoItemConfiguracaoService {

    private CaracteristicaTipoItemConfiguracaoDAO dao;

    @Override
    protected CaracteristicaTipoItemConfiguracaoDAO getDao() {
        if (dao == null) {
            dao = new CaracteristicaTipoItemConfiguracaoDAO();
        }
        return dao;
    }

    /*
     * (non-Javadoc)
     * @see
     * br.com.centralit.citcorpore.negocio.CaracteristicaTipoItemConfiguracaoService
     * #excluirAssociacaoCaracteristicaTipoItemConfiguracao(java.lang.Integer,
     * java.lang.Integer)
     */
    @Override
    public void excluirAssociacaoCaracteristicaTipoItemConfiguracao(final Integer idTipoItemConfiguracao, final Integer idCaracteristica) throws Exception {
        if (idCaracteristica != null && idCaracteristica.intValue() != 0) {
            this.getDao().excluirAssociacaoCaracteristicaTipoItemConfiguracao(idTipoItemConfiguracao, idCaracteristica);
        } else {
            final Collection<CaracteristicaDTO> caracteristicas = this.getCaracteristicaService().consultarCaracteristicasAtivas(idTipoItemConfiguracao);

            if (caracteristicas != null && !caracteristicas.isEmpty()) {
                for (final CaracteristicaDTO caracteristica : caracteristicas) {
                    this.getDao().excluirAssociacaoCaracteristicaTipoItemConfiguracao(idTipoItemConfiguracao, caracteristica.getIdCaracteristica());
                }
            }
        }
    }

    /**
     * Retorna Service de Caracteristica.
     *
     * @return CaracteristicaService
     * @throws ServiceException
     * @throws Exception
     */
    public CaracteristicaService getCaracteristicaService() throws ServiceException, Exception {
        return (CaracteristicaService) ServiceLocator.getInstance().getService(CaracteristicaService.class, null);
    }

}
