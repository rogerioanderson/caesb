/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.ADUserDTO;
import br.com.centralit.citcorpore.bean.LoginDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

public interface UsuarioService extends CrudService {

	/**
	 * Restaura Usu�rio por Login.
	 * 
	 * @param login
	 * @return
	 * @throws ServiceException
	 * @throws LogicException
	 */
	public UsuarioDTO restoreByLogin(String login) throws ServiceException, LogicException;

	public UsuarioDTO restoreByID(Integer id) throws ServiceException, LogicException;

	public UsuarioDTO restoreByLogin(String login, String senha) throws ServiceException, LogicException;

	public void deleteByIdEmpregado(Integer idEmpregado) throws ServiceException, Exception;

	public UsuarioDTO restoreByIdEmpregado(Integer idEmpregado) throws ServiceException, Exception;

	public IDto createFirs(IDto model) throws ServiceException, LogicException;

	public UsuarioDTO listStatus(UsuarioDTO obj) throws Exception;

	public UsuarioDTO listLogin(UsuarioDTO obj) throws Exception;

	public UsuarioDTO listUsuarioExistente(UsuarioDTO obj) throws Exception;

	public boolean listSeVazio() throws Exception;

	void updateNotNull(IDto dto);

	/**
	 * Sincroniza Usu�rio que logou no sistema.
	 * 
	 * @param usuarioAd
	 * @param login
	 * @throws ServiceException
	 * @throws Exception
	 */
	public void sincronizaUsuarioAD(ADUserDTO usuarioAd, LoginDTO login, Boolean isImport) throws ServiceException, Exception;

	/**
	 * Sincroniza Usu�rio pela rotina de Schedule.
	 * 
	 * @param usuarioAd
	 * @throws ServiceException
	 * @throws Exception
	 */
	public void sincronizaUsuarioAD(ADUserDTO usuarioAd, Integer idGrupoSolicitante) throws ServiceException, Exception;

	public UsuarioDTO restoreByIdEmpregadosDeUsuarios(Integer idEmpregado) throws Exception;

	/**
	 * Verifica se usu�rio informado � um usu�rio do AD.
	 * 
	 * @param usuarioDto
	 * @return true - Usu�rio do AD; false - Usu�rio cadastrado pelo sistema.
	 * @throws Exception
	 */
	public boolean usuarioIsAD(UsuarioDTO usuarioDto) throws Exception;

	/**
	 * Gera carga de Usu�rios do AD atrav�s de Arquivo .csv
	 * 
	 * @param arquivo
	 * @throws IOException
	 * @author Vadoilo Damasceno
	 */
	public void gerarCarga(File arquivo) throws IOException;

	@SuppressWarnings("rawtypes")
	public Collection listAtivos() throws Exception;

	public Collection<UsuarioDTO> consultarUsuarioPorNomeAutoComplete(String nome) throws Exception;
	
	public Collection<UsuarioDTO> getUsuariosDosGruposDoUsuarioLogado(Integer idUsuario, String txtFiltro) throws Exception;
	
	public Collection<UsuarioDTO> getUsuariosDosGruposDoUsuarioLogado(Integer idUsuario, String txtFiltro, int limite) throws Exception;

	/**
	 * Retorna a quantidade de usu�rios ativos no sistema
	 * 
	 * @return Long
	 * @throws Exception
	 * @author renato.jesus
	 */
	public Long retornaQuantidadeUsuariosAtivos() throws Exception;

	/**
	 * Consulta utilizada para Autocomplete de Respons�vel na tela de Gerenciamento de Servi�os op��o Mais Filtros.
	 * 
	 * @param nome
	 *            - Nome do usu�rio.
	 * @param idContrato
	 *            - Identificador �nico do contrato selecionado.
	 * @param idUnidade
	 *            - Identificador �nico da unidade selecionada.
	 * @return Collection<UsuarioDTO> com os atributos idEmpregado, nome e idUsuario preenchidos.
	 * @author valdoilo.damasceno
	 * @since 27.02.2015
	 */
	public Collection<UsuarioDTO> findUsuarioByNomeAndIdContratoAndIdUnidade(String nome, Integer idContrato, Integer idUnidade) throws PersistenceException;
	
}
