/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart.
 */
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citcorpore.bean.CaracteristicaDTO;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

/**
 * Service de Caracter�stica.
 * 
 * @author valdoilo.damasceno
 * 
 */
public interface CaracteristicaService extends CrudService {

	/**
	 * Cria Nova Caracter�stica.
	 * 
	 * @param caracteristica
	 * @param request
	 * @throws ServiceException
	 * @throws LogicException
	 * @author valdoilo.damasceno
	 */
	public void create(CaracteristicaDTO caracteristica, HttpServletRequest request) throws ServiceException, LogicException;

	/**
	 * Consulta Caracter�sticas Ativas.
	 * 
	 * @param id
	 * @return Collection - Cole��o de Caracter�sticas.
	 * @throws ServiceException
	 * @author valdoilo.damasceno
	 */
	public Collection<CaracteristicaDTO> consultarCaracteristicasAtivas(Integer idTipoItemConfiguracao) throws ServiceException;
	
	/**
	 * Exclui caracter�stica.
	 * 
	 * @param caracteristica
	 *            - Bean de Caracter�stica.
	 * @throws ServiceException
	 * @throws LogicException
	 * @throws Exception
	 * @author valdoilo.damasceno
	 */
	public void excluirCaracteristica(CaracteristicaDTO caracteristica) throws ServiceException, LogicException, Exception;

	/**
	 * Consulta Caracter�sticas com seus respectivos valores.
	 * 
	 * @param idTipoItemConfiguracao
	 * @param idBaseItemConfiguracao
	 * @return Collection - Cole��o de Caracter�sticas.
	 * @throws LogicException
	 * @throws ServiceException
	 * @throws Exception
	 * @author valdoilo.damasceno
	 */
	public Collection<CaracteristicaDTO> consultarCaracteristicasComValores(Integer idTipoItemConfiguracao, Integer idBaseItemConfiguracao) throws LogicException,
			ServiceException, Exception;

	/**
	 * Consulta Caracter�sticas com seus respectivos valores.
	 * 
	 * @param idTipoItemConfiguracao
	 * @param idBaseItemConfiguracao
	 * @return Collection - Cole��o de Caracter�sticas.
	 * @throws LogicException
	 * @throws ServiceException
	 * @throws Exception
	 * @author valdoilo.damasceno
	 */
	public Collection<CaracteristicaDTO> consultarCaracteristicasComValoresItemConfiguracao(Integer idTipoItemConfiguracao, Integer idItemConfiguracao) throws LogicException,
			ServiceException, Exception;
	
	/**
	 * Consulta Caracter�sticas com seus respectivos valores.
	 * 
	 * @param idTipoItemConfiguracao
	 * @param idBaseItemConfiguracao
	 * @param arrCaracteristicas
	 * @return Collection - Cole��o de Caracter�sticas.
	 * @throws LogicException
	 * @throws ServiceException
	 * @throws Exception
	 * @author valdoilo.damasceno
	 */
	public Collection<CaracteristicaDTO> consultarCaracteristicasComValoresItemConfiguracao(Integer idTipoItemConfiguracao, Integer idItemConfiguracao, String [] arrCaracteristicas) throws LogicException,
	ServiceException, Exception;

	/**
	 * Verifica se Caracteristica informada existe.
	 * 
	 * @param grupo
	 * @return true - existe; false - n�o existe;
	 * @throws PersistenceException
	 * @author Thays.araujo
	 */
	public boolean verificarSeCaracteristicaExiste(CaracteristicaDTO caracteristica) throws PersistenceException;

	
	/**
	 * Consulta Caracter�sticas Ativas com array.
	 * 
	 * @param id
	 * @return Collection - Cole��o de Caracter�sticas.
	 * @throws ServiceException
	 * @author flavio.santana
	 */
	public Collection<CaracteristicaDTO> consultarCaracteristicasAtivas(Integer idTipoItemConfiguracao, String[] arrCaracteristicas) throws ServiceException;

}
