/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.com.centralit.citcorpore.bean.CategoriaSolucaoDTO;
import br.com.centralit.citcorpore.integracao.CategoriaSolucaoDao;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class CategoriaSolucaoServiceEjb extends CrudServiceImpl implements CategoriaSolucaoService {

    private CategoriaSolucaoDao dao;

    @Override
    protected CategoriaSolucaoDao getDao() {
        if (dao == null) {
            dao = new CategoriaSolucaoDao();
        }
        return dao;
    }

    @Override
    public Collection<CategoriaSolucaoDTO> findByIdCategoriaSolucaoPai(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdCategoriaSolucaoPai(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdCategoriaSolucaoPai(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdCategoriaSolucaoPai(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<CategoriaSolucaoDTO> listHierarquia() throws Exception {
        final Collection<CategoriaSolucaoDTO> colFinal = new ArrayList<>();
        try {
            final Collection<CategoriaSolucaoDTO> col = this.getDao().findSemPai();
            if (col != null) {
                for (final CategoriaSolucaoDTO categoriaSolucaoDTO : col) {
                    categoriaSolucaoDTO.setNivel(0);
                    colFinal.add(categoriaSolucaoDTO);
                    final Collection<CategoriaSolucaoDTO> colAux = this.getCollectionHierarquia(categoriaSolucaoDTO.getIdCategoriaSolucao(), 0);
                    if (colAux != null && colAux.size() > 0) {
                        colFinal.addAll(colAux);
                    }
                }
            }
            return colFinal;
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<CategoriaSolucaoDTO> getCollectionHierarquia(final Integer idCateg, final Integer nivel) throws Exception {
        final Collection<CategoriaSolucaoDTO> col = this.getDao().findByIdPai(idCateg);
        final Collection<CategoriaSolucaoDTO> colFinal = new ArrayList<>();
        if (col != null) {
            for (final CategoriaSolucaoDTO categoriaSolucaoDTO : col) {
                categoriaSolucaoDTO.setNivel(nivel + 1);
                colFinal.add(categoriaSolucaoDTO);
                final Collection<CategoriaSolucaoDTO> colAux = this.getCollectionHierarquia(categoriaSolucaoDTO.getIdCategoriaSolucao(), categoriaSolucaoDTO.getNivel());
                if (colAux != null && colAux.size() > 0) {
                    colFinal.addAll(colAux);
                }
            }
        }
        return colFinal;
    }
    
    /**
     * Quando um "Problema" foi cadastrado, sendo associado a uma "Categoria Solu��o" que foi posteriormente exclu�da l�gicamente, surgia o problema de que n�o era mais trazido a "Categoria Solu��o"
     * associada ao "Problema". Assim, como solu��o tem-se este outro m�todo, que traz todas as "Categorias Solu��o" cadastradas e v�lidas, seguindo as mesma regras do m�todo
     * "categoriaSolucaoServiceEjb.listHierarquia()", al�m de trazer tamb�m apenas uma "Categoria Solu��o" exclu�da, se houver associa��o.
     * 
     * @author rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
     * @param idCategoriaSolucao
     * @return
     * @throws PersistenceException
     * @since 08/05/2015
     */
    public Collection<CategoriaSolucaoDTO> buscaCategoriasSolucoesMesmoQuandoDeleted(final Integer idCategoriaSolucao) throws Exception {
        final Collection<CategoriaSolucaoDTO> colFinal = new ArrayList<>();
        try {
            final Collection<CategoriaSolucaoDTO> col = this.getDao().findSemPaiMesmoQuandoDeleted(idCategoriaSolucao);
            if (col != null) {
                for (final CategoriaSolucaoDTO categoriaSolucaoDTO : col) {
                    categoriaSolucaoDTO.setNivel(0);
                    colFinal.add(categoriaSolucaoDTO);
                    final Collection<CategoriaSolucaoDTO> colAux = this.getCollectionHierarquiaMesmoQuandoDeleted(categoriaSolucaoDTO.getIdCategoriaSolucao(), 0);
                    if (colAux != null && colAux.size() > 0) {
                        colFinal.addAll(colAux);
                    }
                }
            }
            return colFinal;
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Quando um "Problema" foi cadastrado, sendo associado a uma "Categoria Solu��o" que foi posteriormente exclu�da l�gicamente, surgia o problema de que n�o era mais trazido a "Categoria Solu��o"
     * associada ao "Problema". Assim, como solu��o tem-se este outro m�todo, que traz todas as "Categorias Solu��o" cadastradas e v�lidas, seguindo as mesma regras do m�todo
     * "categoriaSolucaoServiceEjb.getCollectionHierarquia()", al�m de trazer tamb�m apenas uma "Categoria Solu��o" exclu�da, se houver associa��o.
     * 
     * @author rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
     * @param idCategoriaSolucao
     * @param nivel
     * @return
     * @throws PersistenceException
     * @since 08/05/2015
     */
    public Collection<CategoriaSolucaoDTO> getCollectionHierarquiaMesmoQuandoDeleted(final Integer idCateg, final Integer nivel) throws Exception {
        final Collection<CategoriaSolucaoDTO> col = this.getDao().findByIdPaiMesmoQuandoDeleted(idCateg);
        final Collection<CategoriaSolucaoDTO> colFinal = new ArrayList<>();
        if (col != null) {
            for (final CategoriaSolucaoDTO categoriaSolucaoDTO : col) {
                categoriaSolucaoDTO.setNivel(nivel + 1);
                colFinal.add(categoriaSolucaoDTO);
                final Collection<CategoriaSolucaoDTO> colAux = this.getCollectionHierarquiaMesmoQuandoDeleted(categoriaSolucaoDTO.getIdCategoriaSolucao(), categoriaSolucaoDTO.getNivel());
                if (colAux != null && colAux.size() > 0) {
                    colFinal.addAll(colAux);
                }
            }
        }
        return colFinal;
    }

    @Override
    public String verificaDescricaoDuplicadaCategoriaAoCriar(final Map mapFields) throws Exception {
        List<CategoriaSolucaoDTO> listaCategoriaSolucao = null;
        final String descricaoCategoria = mapFields.get("DESCRICAOCATEGORIASOLUCAO").toString().trim();
        listaCategoriaSolucao = (List<CategoriaSolucaoDTO>) this.getDao().verificaDescricaoDuplicadaCategoriaAoCriar(descricaoCategoria);
        if (listaCategoriaSolucao == null || listaCategoriaSolucao.isEmpty()) {
            return "1";
        }
        return "0";
    }

    @Override
    public String verificaDescricaoDuplicadaCategoriaAoAtualizar(final Map mapFields) throws Exception {
        List<CategoriaSolucaoDTO> listaCategoriaSolucao = null;
        final String descricaoCategoria = mapFields.get("DESCRICAOCATEGORIASOLUCAO").toString().trim();
        final String idCategoria = mapFields.get("IDCATEGORIASOLUCAO").toString().trim();
        listaCategoriaSolucao = (List<CategoriaSolucaoDTO>) this.getDao().verificaDescricaoDuplicadaCategoriaAoAtualizar(Integer.valueOf(idCategoria), descricaoCategoria);
        if (listaCategoriaSolucao == null || listaCategoriaSolucao.isEmpty()) {
            return "1";
        }
        return "0";
    }

    @Override
    public Collection<CategoriaSolucaoDTO> listaCategoriasSolucaoAtivas() throws Exception {
        return this.getDao().listaCategoriasSolucaoAtivas();
    }

}
