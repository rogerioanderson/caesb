/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.sql.Date;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.ContratoQuestionariosDTO;
import br.com.citframework.service.CrudService;

public interface SolicitacaoServicoQuestionarioService extends CrudService {
	public Collection listByIdContratoAndAba(Integer idContrato, String aba) throws Exception;
	public Collection listByIdContratoAndAbaOrdemCrescente(Integer idContrato, String aba) throws Exception;
	public ContratoQuestionariosDTO getUltimoByIdContratoAndAba(Integer idContrato, String aba) throws Exception;
	public Collection listByIdContrato(Integer idContrato) throws Exception;
	public Collection listByIdContratoOrderDecrescente(Integer idContrato) throws Exception;
	public Collection listByIdContratoAndQuestionario(Integer idQuestionario, Integer idContrato) throws Exception;
	public ContratoQuestionariosDTO getQuantidadeByIdDepEstabAndAba(Integer idDepartamento, Integer idEstabelecimento, Integer idCargo, String aba) throws Exception;
	public void updateConteudoImpresso(Integer idPessQuest, String conteudoImpresso) throws Exception;
	public ContratoQuestionariosDTO getUltimoByIdContratoAndAbaAndPeriodo(Integer idContrato, String aba, Date dataInicio, Date dataFim) throws Exception;
	public ContratoQuestionariosDTO getQuantidadeByIdDepEstabAndAbaAndPeriodo(Integer idDepartamento, Integer idEstabelecimento, 
			Integer idCargo, String aba, Date dataInicio, Date dataFim) throws Exception;
	public Collection listByIdContratoOrderIdDecrescente(Integer idContrato) throws Exception;
	public ContratoQuestionariosDTO getQuantidadeByIdDepEstabAndAbaAndPeriodoFinalizados(Integer idDepartamento, Integer idEstabelecimento, 
			Integer idCargo, String aba, Date dataInicio, Date dataFim) throws Exception;
	public ContratoQuestionariosDTO getQuantidadeByIdDepEstabFuncaoAndAbaAndPeriodo(Integer idDepartamento, Integer idEstabelecimento, 
            Integer idCargo, Integer idFuncao, String aba, Date dataInicio, Date dataFim) throws Exception;
}
