/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.EscalonamentoDTO;
import br.com.centralit.citcorpore.bean.RegraEscalonamentoDTO;
import br.com.centralit.citcorpore.integracao.EscalonamentoDAO;
import br.com.centralit.citcorpore.integracao.RegraEscalonamentoDAO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

public class RegraEscalonamentoServiceEjb extends CrudServiceImpl implements RegraEscalonamentoService {

    private RegraEscalonamentoDAO dao;

    @Override
    protected RegraEscalonamentoDAO getDao() {
        if (dao == null) {
            dao = new RegraEscalonamentoDAO();
        }
        return dao;
    }

    @Override
    public IDto create(final IDto model) throws ServiceException, LogicException {
        RegraEscalonamentoDTO regraEscalonamentoDTO = (RegraEscalonamentoDTO) model;
        final RegraEscalonamentoDAO regraEscalonamentoDAO = this.getDao();
        final EscalonamentoDAO escalonamentoDAO = new EscalonamentoDAO();

        final TransactionControler tc = new TransactionControlerImpl(regraEscalonamentoDAO.getAliasDB());
        try {
            regraEscalonamentoDAO.setTransactionControler(tc);
            escalonamentoDAO.setTransactionControler(tc);

            tc.start();

            regraEscalonamentoDTO.setDataInicio(UtilDatas.getDataAtual());
            regraEscalonamentoDTO = (RegraEscalonamentoDTO) regraEscalonamentoDAO.create(regraEscalonamentoDTO);

            this.mantemEscalonamentos(regraEscalonamentoDTO, escalonamentoDAO);

            tc.commit();
            tc.close();
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        }
        return regraEscalonamentoDTO;
    }

    @Override
    public void update(final IDto model) throws ServiceException, LogicException {
        final RegraEscalonamentoDTO regraEscalonamentoDTO = (RegraEscalonamentoDTO) model;
        final RegraEscalonamentoDAO regraEscalonamentoDAO = this.getDao();
        final EscalonamentoDAO escalonamentoDAO = new EscalonamentoDAO();

        final TransactionControler tc = new TransactionControlerImpl(regraEscalonamentoDAO.getAliasDB());
        try {
            regraEscalonamentoDAO.setTransactionControler(tc);
            escalonamentoDAO.setTransactionControler(tc);

            tc.start();

            regraEscalonamentoDAO.update(regraEscalonamentoDTO);

            this.mantemEscalonamentos(regraEscalonamentoDTO, escalonamentoDAO);

            tc.commit();
            tc.close();
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        }
    }

    @Override
    public void delete(final IDto model) throws ServiceException, LogicException {
        final RegraEscalonamentoDTO regraEscalonamentoDTO = (RegraEscalonamentoDTO) model;
        final RegraEscalonamentoDAO regraEscalonamentoDAO = this.getDao();
        final EscalonamentoDAO escalonamentoDAO = new EscalonamentoDAO();
        final TransactionControler tc = new TransactionControlerImpl(regraEscalonamentoDAO.getAliasDB());
        try {
            regraEscalonamentoDAO.setTransactionControler(tc);
            escalonamentoDAO.setTransactionControler(tc);

            tc.start();

            regraEscalonamentoDTO.setDataFim(UtilDatas.getDataAtual());

            if (regraEscalonamentoDTO.getColEscalonamentoDTOs() != null) {
                regraEscalonamentoDTO.getColEscalonamentoDTOs().clear();
            }
            regraEscalonamentoDAO.update(regraEscalonamentoDTO);

            this.mantemEscalonamentos(regraEscalonamentoDTO, escalonamentoDAO);

            tc.commit();
            tc.close();
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        }
    }

    private void mantemEscalonamentos(final RegraEscalonamentoDTO regraEscalonamentoDTO, final EscalonamentoDAO escalonamentoDAO) throws Exception {
        escalonamentoDAO.gravarDataFim(regraEscalonamentoDTO.getIdRegraEscalonamento());
        final Collection<EscalonamentoDTO> colEscalonamentoDTOs = regraEscalonamentoDTO.getColEscalonamentoDTOs();
        if (colEscalonamentoDTOs != null && colEscalonamentoDTOs.size() > 0) {
            for (final EscalonamentoDTO escalonamentoDTO : colEscalonamentoDTOs) {
                escalonamentoDTO.setIdRegraEscalonamento(regraEscalonamentoDTO.getIdRegraEscalonamento());
                escalonamentoDTO.setDataInicio(UtilDatas.getDataAtual());
                escalonamentoDAO.create(escalonamentoDTO);
            }
        }
    }

}
