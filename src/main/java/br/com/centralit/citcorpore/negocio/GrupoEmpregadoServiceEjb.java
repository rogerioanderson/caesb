/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.List;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.GrupoEmpregadoDTO;
import br.com.centralit.citcorpore.bean.RelatorioGruposUsuarioDTO;
import br.com.centralit.citcorpore.integracao.GrupoEmpregadoDao;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

@SuppressWarnings("rawtypes")
public class GrupoEmpregadoServiceEjb extends CrudServiceImpl implements GrupoEmpregadoService {

    private GrupoEmpregadoDao dao;

    @Override
    protected GrupoEmpregadoDao getDao() {
        if (dao == null) {
            dao = new GrupoEmpregadoDao();
        }
        return dao;
    }

    public Collection list(final List ordenacao) throws LogicException, ServiceException {
        return null;
    }

    public Collection list(final String ordenacao) throws LogicException, ServiceException {
        return null;
    }

    @Override
    public Collection<GrupoEmpregadoDTO> findByIdGrupo(final Integer idGrupo) throws Exception {
        return this.getDao().findByIdGrupo(idGrupo);
    }

    @Override
    public Collection<GrupoEmpregadoDTO> findUsariosGrupo() throws Exception {
        return this.getDao().findUsariosGrupo();
    }

    @Override
    public void gerarGridEmpregados(final DocumentHTML document, final Collection<GrupoEmpregadoDTO> grupoEmpregados) throws Exception {

    }

    @Override
    public Collection findByIdEmpregado(final Integer idEmpregado) throws Exception {
        return this.getDao().findByIdEmpregado(idEmpregado);
    }

    @Override
    public void deleteByIdGrupoAndEmpregado(final Integer idGrupo, final Integer idEmpregado) throws Exception {
        this.getDao().deleteByIdGrupoAndEmpregado(idGrupo, idEmpregado);

    }

    @Override
    public Collection<GrupoEmpregadoDTO> findGrupoEmpregadoHelpDeskByIdContrato(final Integer idContrato) {
        try {
            return this.getDao().findGrupoEmpregadoHelpDeskByIdContrato(idContrato);
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Collection<GrupoEmpregadoDTO> findGrupoAndEmpregadoByIdGrupo(final Integer idGrupo) throws Exception {
        return this.getDao().findGrupoAndEmpregadoByIdGrupo(idGrupo);
    }

    @Override
    public Collection<RelatorioGruposUsuarioDTO> listaRelatorioGruposUsuario(final Integer idColaborador) throws Exception {
        try {
            return this.getDao().listaRelatorioGruposUsuario(idColaborador);
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Collection findByIdEmpregadoNome(final Integer idEmpregado) throws Exception {
        try {
            return this.getDao().findByIdEmpregadoNome(idEmpregado);
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer calculaTotalPaginas(final Integer itensPorPagina, final Integer idGrupo) throws Exception {
        try {
            return this.getDao().calculaTotalPaginas(itensPorPagina, idGrupo);
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Collection<GrupoEmpregadoDTO> paginacaoGrupoEmpregado(final Integer idGrupo, final Integer pgAtual, final Integer qtdPaginacao) throws Exception {
        try {
            return this.getDao().paginacaoGrupoEmpregado(idGrupo, pgAtual, qtdPaginacao);
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean grupoempregado(final Integer idEmpregado, final Integer idGrupo) throws Exception {
        return this.getDao().grupoempregado(idEmpregado, idGrupo);
    }

    @Override
    public Collection<GrupoEmpregadoDTO> findEmpregado(final Integer idGrupo, final Integer idEmpregado) throws Exception {
        return this.getDao().findEmpregado(idGrupo, idEmpregado);
    }

    @Override
    public void deleteTodosEmpregados(final Integer idGrupo) throws Exception {
        this.getDao().deleteByIdGrupo(idGrupo);
    }

    @Override
    public Collection<GrupoEmpregadoDTO> verificacaoResponsavelPorSolicitacao(final Integer idGrupo, final Integer idEmpregado) {
        try {
            return this.getDao().verificacaoResponsavelPorSolicitacao(idGrupo, idEmpregado);
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
