/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;

import br.com.centralit.citcorpore.bean.FormulaOsDTO;
import br.com.centralit.citcorpore.integracao.FormulaOsDao;
import br.com.citframework.service.CrudServiceImpl;

/**
 * @author CentralIT
 *
 */
/**
 * @author Centralit
 *
 */
public class FormulaOsServiceEjb extends CrudServiceImpl implements FormulaOsService {

    private FormulaOsDao dao;

    @Override
    protected FormulaOsDao getDao() {
        if (dao == null) {
            dao = new FormulaOsDao();
        }
        return dao;
    }

    @Override
    public ArrayList<FormulaOsDTO> listar(final int idContrato) {
        try {
            final ArrayList<FormulaOsDTO> listar = (ArrayList<FormulaOsDTO>) this.getDao().listar(idContrato);
            return listar;
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FormulaOsDTO buscarPorFormula(final String formula) throws Exception {
        try {
            final FormulaOsDTO formulaOsDTO = this.getDao().buscarPorFormula(formula);
            return formulaOsDTO;
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean verificaSerExisteFormulaIgual(final String formula, final int idFormula) throws Exception {
        return this.getDao().verificaSerExisteFormulaIgual(formula, idFormula);
    }

}
