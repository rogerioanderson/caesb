/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.TipoMovimFinanceiraViagemDTO;
import br.com.centralit.citcorpore.integracao.TipoMovimFinanceiraViagemDao;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

/**
 * @author ronnie.lopes
 *
 */
@SuppressWarnings("rawtypes")
public class TipoMovimFinanceiraViagemServiceEjb extends CrudServiceImpl implements TipoMovimFinanceiraViagemService {

    private TipoMovimFinanceiraViagemDao dao;

    @Override
    protected TipoMovimFinanceiraViagemDao getDao() {
        if (dao == null) {
            dao = new TipoMovimFinanceiraViagemDao();
        }
        return dao;
    }

    public Collection list(final List ordenacao) throws LogicException, ServiceException {
        return null;
    }

    public Collection list(final String ordenacao) throws LogicException, ServiceException {
        return null;
    }

    @Override
    public List<TipoMovimFinanceiraViagemDTO> listByClassificacao(final String classificacao) throws Exception {
        return this.getDao().listByClassificacao(classificacao);
    }

    @Override
    public TipoMovimFinanceiraViagemDTO findByMovimentacao(final Long idtipoMovimFinanceiraViagem) throws Exception {
        return this.getDao().findByMovimentacao(idtipoMovimFinanceiraViagem);
    }

    @Override
    public List<TipoMovimFinanceiraViagemDTO> recuperaTipoAtivos() throws Exception {
        return this.getDao().recuperaTiposAtivos();
    }

    @Override
    public TipoMovimFinanceiraViagemDTO findByMovimentacaoEstadoAdiantamento(final Long idtipoMovimFinanceiraViagem, final String adiantamento) throws Exception {
        return this.getDao().findByMovimentacaoEstadoAdiantamento(idtipoMovimFinanceiraViagem, adiantamento);
    }
    
    @Override
    public TipoMovimFinanceiraViagemDTO buscaDiaria() throws Exception {
        return this.getDao().buscaDiaria();
    }

}
