/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.LocalidadeUnidadeDTO;
import br.com.citframework.service.CrudService;

public interface LocalidadeUnidadeService extends CrudService {
	/**
	 * Retonar lista de idlocalidade
	 * 
	 * @param idUnidade
	 * @return list
	 * @throws Exception
	 * @author Thays.araujo
	 */
	public Collection<LocalidadeUnidadeDTO> listaIdLocalidades(Integer idUnidade) throws Exception;

	/**
	 * Retonar verdadeiro caso a localidade esteja associado a uma unidade ou falso caso a localidade n�o esteja associado a uma unidade
	 * 
	 * @param idLocalidade
	 * @return true- false
	 * @throws Exception
	 */
	public boolean verificarExistenciaDeLocalidadeEmUnidade(Integer idLocalidade) throws Exception;
	
	public void deleteByIdUnidade(Integer idUnidade) throws Exception;
	
}
