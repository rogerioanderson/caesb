/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.List;

import br.com.centralit.citcorpore.bean.AssociacaoDeviceAtendenteDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

/**
 * Servi�os para {@link AssociacaoDeviceAtendenteDTO}
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 15/11/2014
 */
public interface AssociacaoDeviceAtendenteService extends CrudService {

    /**
     * Lista as associa��es ativas de um usu�rio, independentemente de conex�o e plataforma
     *
     * @param usuario
     *            usu�rio para o qual se deseja listar as associa��es ativas
     * @param connection
     *            "conex�o" no mobile, que � a URI acessada
     * @return {@link List} de {@link AssociacaoDeviceAtendenteDTO}
     * @throws ServiceException
     */
    List<AssociacaoDeviceAtendenteDTO> listActiveAssociationsForUserAndConnection(final UsuarioDTO usuario, final String connection) throws ServiceException;

    /**
     * Associa um device a um usu�rio
     *
     * @param associacao
     *            informa��es da associa��o a ser efetuada
     * @param usuario
     *            usu�rio para o qual ser� realizada a associa��o
     * @return {@link AssociacaoDeviceAtendenteDTO} realizada
     * @throws ServiceException
     */
    AssociacaoDeviceAtendenteDTO associateDeviceToAttendant(final AssociacaoDeviceAtendenteDTO associacao, final UsuarioDTO usuario) throws ServiceException;

    /**
     * Desassocia um device de um usu�rio
     *
     * @param associacao
     *            informa��es da associa��o a ser desfeita
     * @param usuario
     *            usu�rio para o qual ser� realizada a desassocia��o
     * @return {@link AssociacaoDeviceAtendenteDTO} desassociado
     * @throws ServiceException
     */
    AssociacaoDeviceAtendenteDTO disassociateDeviceFromAttendant(final AssociacaoDeviceAtendenteDTO associacao, final UsuarioDTO usuario) throws ServiceException;

}
