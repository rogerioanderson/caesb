/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.CategoriaProblemaDTO;
import br.com.citframework.service.CrudService;

@SuppressWarnings("rawtypes")
public interface CategoriaProblemaService extends CrudService {

	public Collection findByIdCategoriaProblema(Integer parm) throws Exception;

	public void deleteByIdCategoriaProblema(Integer parm) throws Exception;

	public Collection findByNomeCategoria(String parm) throws Exception;

	/**
	 * Deleta por nome categoria.
	 * 
	 * @param parm
	 * @throws Exception
	 */
	public void deleteByNomeCategoria(String parm) throws Exception;

	public Collection listHierarquia() throws Exception;
	
	public Collection findByNomeCategoriaProblema(CategoriaProblemaDTO categoriaProblemaDto) throws Exception;
	
	/**
	 * Retorna uma lista de categoria problema ativas
	 * @return
	 * @throws Exception
	 * @author thays.araujo
	 */
	public Collection getAtivos()throws Exception;
	
	/**
	 * Retorna verdadeiro ou falso caso a categoriaProblema esteje cadastrada
	 * @param obj
	 * @return
	 * @throws Exception
	 * @author thays.araujo
	 */
	public boolean consultarCategoriasAtivas(CategoriaProblemaDTO obj) throws Exception;

	/**
	 * Retorna todas as CategoriaProblema relacionadas a um determinado template
	 * @throws Exception
	 * @author murilo.rodrigues
	 */
	public Collection<CategoriaProblemaDTO> findByIdTemplate(Integer idTemplate) throws Exception;

	/**
	 * Desvincula CategoriaProblemas relacionadas a um determinado Template
	 * @throws Exception
	 * @author murilo.rodrigues
	 */
	public void desvincularCategoriaProblemasRelacionadasTemplate(Integer idTemplate) throws Exception;
	
	
}
