/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.integracao.DemandaDao;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class DemandaServiceEjb extends CrudServiceImpl implements DemandaService {

    private DemandaDao dao;

    @Override
    protected DemandaDao getDao() {
        if (dao == null) {
            dao = new DemandaDao();
        }
        return dao;
    }

    public Collection list(final List ordenacao) throws LogicException, ServiceException {
        return null;
    }

    public Collection list(final String ordenacao) throws LogicException, ServiceException {
        return null;
    }

    @Override
    public IDto create(final IDto model) throws ServiceException, LogicException {
        /*
         * //Instancia Objeto controlador de transacao
         * CrudDAO crudDao = getDao();
         * ExecucaoDemandaDao execucaoDemandaDao = new ExecucaoDemandaDao();
         * TransactionControler tc = new TransactionControlerImpl(crudDao.getAliasDB());
         * FluxoDao fluxoDao = new FluxoDao();
         * DemandaDTO demanda = (DemandaDTO)model;
         * try{
         * //Faz validacao, caso exista.
         * validaCreate(model);
         * //Seta o TransactionController para os DAOs
         * crudDao.setTransactionControler(tc);
         * execucaoDemandaDao.setTransactionControler(tc);
         * //Inicia transacao
         * tc.start();
         * FluxoDTO fluxo = fluxoDao.getNextAtividadeByFluxo(demanda.getIdFluxo(), null);
         * //Executa operacoes pertinentes ao negocio.
         * model = crudDao.create(model);
         * ExecucaoDemandaDTO execucaoDemanda = new ExecucaoDemandaDTO();
         * execucaoDemanda.setIdAtividade(fluxo.getIdAtividade());
         * execucaoDemanda.setIdDemanda(demanda.getIdDemanda());
         * execucaoDemanda.setSituacao("N");
         * execucaoDemanda.setGrupoExecutor(fluxo.getGrupoExecutor());
         * execucaoDemanda = (ExecucaoDemandaDTO) execucaoDemandaDao.create(execucaoDemanda);
         * //Faz commit e fecha a transacao.
         * tc.commit();
         * tc.close();
         * return model;
         * }catch(Exception e){
         * this.rollbackTransaction(tc, e);
         * }
         */

        return model;
    }

    @Override
    public Collection findByIdOS(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdOS(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
