/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 *
 */
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.BaseConhecimentoRelacionadoDTO;
import br.com.centralit.citcorpore.integracao.BaseConhecimentoRelacionadoDAO;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudServiceImpl;

/**
 * @author Vadoilo Damasceno
 *
 */
public class BaseConhecimentoRelacionadoServiceEjb extends CrudServiceImpl implements BaseConhecimentoRelacionadoService {

    private BaseConhecimentoRelacionadoDAO dao;

    @Override
    protected BaseConhecimentoRelacionadoDAO getDao() {
        if (dao == null) {
            dao = new BaseConhecimentoRelacionadoDAO();
        }
        return dao;
    }

    @Override
    public void deleteByIdConhecimento(final Integer idBaseConhecimento, final TransactionControler transactionControler) throws Exception {
        this.getDao().setTransactionControler(transactionControler);
        this.getDao().deleteByIdConhecimento(idBaseConhecimento);
    }

    @Override
    public void create(final BaseConhecimentoRelacionadoDTO baseConhecimentoRelacionado, final TransactionControler transactionControler) throws Exception {
        this.getDao().setTransactionControler(transactionControler);
        this.getDao().create(baseConhecimentoRelacionado);
    }

    @Override
    public Collection<BaseConhecimentoRelacionadoDTO> listByIdBaseConhecimento(final Integer idBaseConhecimento) throws Exception {
        return this.getDao().listByIdBaseConhecimento(idBaseConhecimento);
    }

    @Override
    public Collection<BaseConhecimentoRelacionadoDTO> listByIdBaseConhecimentoRelacionado(final Integer idBaseConhecimento) throws Exception {
        return this.getDao().listByIdBaseConhecimentoRelacionado(idBaseConhecimento);
    }

}
