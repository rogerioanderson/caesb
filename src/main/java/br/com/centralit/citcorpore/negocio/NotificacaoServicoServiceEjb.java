/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.NotificacaoServicoDTO;
import br.com.centralit.citcorpore.bean.ServicoDTO;
import br.com.centralit.citcorpore.integracao.EmpregadoDao;
import br.com.centralit.citcorpore.integracao.NotificacaoServicoDao;
import br.com.centralit.citcorpore.mail.MensagemEmail;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.citframework.dto.IDto;
import br.com.citframework.service.CrudServiceImpl;

public class NotificacaoServicoServiceEjb extends CrudServiceImpl implements NotificacaoServicoService {

    private NotificacaoServicoDao dao;

    @Override
    protected NotificacaoServicoDao getDao() {
        if (dao == null) {
            dao = new NotificacaoServicoDao();
        }
        return dao;
    }

    @Override
    public Collection<NotificacaoServicoDTO> listaIdServico(final Integer idServico) throws Exception {
        return this.getDao().listaIdServico(idServico);
    }

    @Override
    public Collection<NotificacaoServicoDTO> listaIdNotificacao(final Integer idNotificacao) throws Exception {
        return this.getDao().listaIdNotificacao(idNotificacao);
    }

    @Override
    public boolean existeServico(final Integer idNotificacao, final Integer idservico) throws Exception {
        return this.getDao().existeServico(idNotificacao, idservico);
    }

    public void enviarEmailNotificacao(final ServicoDTO servicoDto) throws Exception {
        try {
            final EmpregadoDao empregadoDao = new EmpregadoDao();
            Collection<EmpregadoDTO> colEmpregados = new ArrayList<>();
			final String remetente = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_RemetenteNotificacoesSolicitacao, null);

            final String ID_MODELO_EMAIL_AVISAR_ALTERACAO_SERVICO = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.ID_MODELO_EMAIL_AVISAR_ALTERACAO_SERVICO, "");
            if (ID_MODELO_EMAIL_AVISAR_ALTERACAO_SERVICO != null && !ID_MODELO_EMAIL_AVISAR_ALTERACAO_SERVICO.isEmpty()) {

                colEmpregados = empregadoDao.listarEmailsNotificacoesServico(servicoDto.getIdServico());

                if (colEmpregados != null) {

                    for (final EmpregadoDTO empregados : colEmpregados) {

                        final MensagemEmail mensagem = new MensagemEmail(Integer.parseInt(ID_MODELO_EMAIL_AVISAR_ALTERACAO_SERVICO.trim()), new IDto[] {servicoDto});

                        if (empregados.getEmail() != null) {
                            mensagem.envia(empregados.getEmail(), "", remetente);
                        }

                    }
                }

            }
        } catch (final Exception e) {}
    }

}
