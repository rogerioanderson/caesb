/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.CidadesDTO;
import br.com.centralit.citcorpore.integracao.CidadesDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

@SuppressWarnings("rawtypes")
public class CidadesServiceEjb extends CrudServiceImpl implements CidadesService {

    private CidadesDao dao;

    @Override
    protected CidadesDao getDao() {
        if (dao == null) {
            dao = new CidadesDao();
        }
        return dao;
    }

    @Override
    public Collection<CidadesDTO> listByIdCidades(final CidadesDTO obj) throws Exception {
        try {
            return this.getDao().listByIdCidades(obj);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<CidadesDTO> findByNome(final String nome) throws Exception {
        return this.getDao().findByNome(nome);
    }

    @Override
    public CidadesDTO findCidadeUF(final Integer idCidade) throws Exception {
        final List<CidadesDTO> lista = this.getDao().findCidadeUF(idCidade);
        if (lista != null) {
            return lista.get(0);
        }
        return null;
    }

    @Override
    public Collection<CidadesDTO> listByIdUf(final Integer idUf) throws Exception {
        return this.getDao().listByIdUf(idUf);
    }

    @Override
    public Collection<CidadesDTO> findByIdEstadoAndNomeCidade(final Integer idEstado, final String nomeCidade) throws Exception {
        return this.getDao().findByIdEstadoAndNomeCidade(idEstado, nomeCidade);
    }

    @Override
    public Collection findNomeByIdCidade(final Integer idCidade) throws Exception {
        return this.getDao().findCidadeUF(idCidade);
    }

    @Override
    public CidadesDTO findByIdCidade(final Integer idCidade) throws Exception {
        return this.getDao().findByIdCidade(idCidade);
    }

}
