/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart.
 */
package br.com.centralit.citcorpore.negocio;

import java.util.List;

import br.com.centralit.citcorpore.bean.BaseItemConfiguracaoDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

/**
 * Service de BaseItemConfiguracao.
 * 
 * @author valdoilo.damasceno
 * 
 */
public interface BaseItemConfiguracaoService extends CrudService {

    /**
     * Inclui Novo Item de Configura��o.
     * 
     * @param baseItemConfiguracaoBean
     * @param request
     * @return IDto
     * @throws ServiceException
     * @throws LogicException
     * @author valdoilo.damasceno
     * @throws FileUploadException
     */
    /*public IDto create(BaseItemConfiguracaoDTO baseItemConfiguracaoBean, HttpServletRequest request) throws ServiceException, LogicException,
	    FileUploadException;*/

    /**
     * Verifica se existe cadastro pra BaseItemConfigurac�o.
     * 
     * @param obj
     * @param nomePai
     * @return
     * @throws Exception
     */
    public boolean existBaseItemConfiguracao(BaseItemConfiguracaoDTO dto) throws Exception;

    /**
     * M�todo que persiste nova BaseItemConfigura��o
     * 
     * @param baseItemConfiguracao
     * @return
     * @throws ServiceException
     * @throws LogicException
     */
	public IDto[] create(BaseItemConfiguracaoDTO[] baseItemConfiguracao)
			throws ServiceException, LogicException;

	List<IDto> restoreChildren(BaseItemConfiguracaoDTO baseItemConfiguracaoBean)
			throws Exception;

	public void update(BaseItemConfiguracaoDTO[] vetorBaseItemConfiguracao) throws ServiceException, LogicException;

}
