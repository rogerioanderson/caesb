/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.List;

import br.com.centralit.bpm.dto.TarefaFluxoDTO;
import br.com.centralit.citcorpore.bean.ConexaoBIDTO;
import br.com.centralit.citcorpore.bpm.negocio.ExecucaoSolicitacao;
import br.com.centralit.citcorpore.integracao.ConexaoBIDAO;
import br.com.citframework.integracao.CrudDAO;
import br.com.citframework.service.CrudServiceImpl;

public class ExecucaoConexaoBIServiceEjb extends CrudServiceImpl implements ExecucaoConexaoBIService {

    @Override
    public Integer totalPaginas(final Integer itensPorPagina, final String loginUsuario) throws Exception {
        return new ExecucaoSolicitacao().totalPaginas(itensPorPagina, loginUsuario);
    }

    @Override
    public Integer obterTotalDePaginas(final Integer itensPorPagina, final String loginUsuario, final ConexaoBIDTO conexaoBIBean) throws Exception {
        Integer total = 0;

        final ConexaoBIDAO conexaoBIDao = new ConexaoBIDAO();

        // ESSA LISTA DE TAREFAS J� EST� VINDO COM O DTO E N�O DEVERIA VIR. CRIAR M�TODO PARA TRAZER APENAS AS TAREFAS COM O IDINSTANCIA, QUE � A �NICA INFORMA��O UTILIZADA NA
        // CONSULTA ABAIXO.
        // List<TarefaFluxoDTO> listTarefasComSolicitacaoServico = recuperaTarefas(loginUsuario);
        //
        // listTarefas = listTarefasComSolicitacaoServico;
        // Comentado para centalizar o m�todo abaixo

        total = conexaoBIDao.totalDePaginas(itensPorPagina, null, conexaoBIBean);

        return total;
    }

    @Override
    public List<TarefaFluxoDTO> recuperaTarefas(final String loginUsuario) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TarefaFluxoDTO recuperaTarefa(final String loginUsuario, final Integer idTarefa) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void delegaTarefa(final String loginUsuario, final Integer idTarefa, final String usuarioDestino, final String grupoDestino) throws Exception {
        // TODO Auto-generated method stub
    }

    @Override
    protected CrudDAO getDao() {
        return null;
    }

}
