/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import br.com.centralit.citcorpore.bean.ParecerDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.ParecerDao;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

public class ParecerServiceEjb extends CrudServiceImpl implements ParecerService {

    private ParecerDao dao;

    @Override
    protected ParecerDao getDao() {
        if (dao == null) {
            dao = new ParecerDao();
        }
        return dao;
    }

    public ParecerDTO create(final TransactionControler tc, final UsuarioDTO usuario, final Integer idJustificativa, final String complementoJustificativa, String aprovado)
            throws Exception {
        if (aprovado == null) {
            aprovado = "N";
        }
        if (aprovado.equalsIgnoreCase("N") && idJustificativa == null) {
            throw new LogicException("Justificativa n�o informada");
        }

        final ParecerDao parecerDao = new ParecerDao();
        parecerDao.setTransactionControler(tc);
        final ParecerDTO parecerDto = new ParecerDTO();
        parecerDto.setIdResponsavel(usuario.getIdEmpregado());
        parecerDto.setIdJustificativa(idJustificativa);
        parecerDto.setComplementoJustificativa(complementoJustificativa);
        parecerDto.setAprovado(aprovado);
        parecerDto.setDataHoraParecer(UtilDatas.getDataHoraAtual());
        return (ParecerDTO) parecerDao.create(parecerDto);
    }

    public ParecerDTO createOrUpdate(final TransactionControler tc, final Integer idParecer, final UsuarioDTO usuario, final Integer idJustificativa,
            final String complementoJustificativa, String aprovado) throws Exception {
        final ParecerDao parecerDao = new ParecerDao();
        parecerDao.setTransactionControler(tc);
        ParecerDTO parecerDto = new ParecerDTO();
        if (idParecer != null && idParecer.intValue() > 0) {
            parecerDto.setIdParecer(idParecer);
            parecerDto = (ParecerDTO) parecerDao.restore(parecerDto);
            if (parecerDto.getIdResponsavel().intValue() != usuario.getIdEmpregado().intValue()) {
                parecerDto = new ParecerDTO();
            }
        }
        if (aprovado == null) {
            aprovado = "N";
        }
        if (aprovado.equalsIgnoreCase("N") && idJustificativa == null) {
            throw new LogicException("Justificativa n�o informada");
        }

        parecerDto.setIdResponsavel(usuario.getIdEmpregado());
        parecerDto.setIdJustificativa(idJustificativa);
        parecerDto.setComplementoJustificativa(complementoJustificativa);
        parecerDto.setAprovado(aprovado);
        parecerDto.setDataHoraParecer(UtilDatas.getDataHoraAtual());
        if (parecerDto.getIdParecer() != null) {
            parecerDao.update(parecerDto);
        } else {
            parecerDto = (ParecerDTO) parecerDao.create(parecerDto);
        }
        return parecerDto;
    }

    @Override
    public boolean verificarSeExisteJustificativaParecer(final ParecerDTO obj) throws Exception {
        return this.getDao().verificarSeExisteJustificativaParecer(obj);
    }

}
