/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.com.centralit.citcorpore.bean.MenuDTO;
import br.com.centralit.citcorpore.bean.PerfilAcessoMenuDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.MenuDao;
import br.com.centralit.citcorpore.integracao.PerfilAcessoMenuDao;
import br.com.citframework.dto.IDto;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

public class PerfilAcessoMenuServiceEjb extends CrudServiceImpl implements PerfilAcessoMenuService {

    private PerfilAcessoMenuDao dao;

    @Override
    protected PerfilAcessoMenuDao getDao() {
        if (dao == null) {
            dao = new PerfilAcessoMenuDao();
        }
        return dao;
    }

    @Override
    public Collection<PerfilAcessoMenuDTO> restoreMenusAcesso(final IDto obj) throws Exception {
        return this.getDao().restoreMenusAcesso(obj);
    }

    @Override
    public void atualizaPerfis() throws Exception {
        final PerfilAcessoMenuDao perfilAcessoMenuDao = new PerfilAcessoMenuDao();
        final MenuDao menuDao = new MenuDao();

        final TransactionControler tc = new TransactionControlerImpl(perfilAcessoMenuDao.getAliasDB());

        tc.start();

        perfilAcessoMenuDao.setTransactionControler(tc);
        menuDao.setTransactionControler(tc);

        final List<PerfilAcessoMenuDTO> colecaoPerfisAcessoMenu = (List) this.getDao().list();
        for (final PerfilAcessoMenuDTO perfilAcessoMenu : colecaoPerfisAcessoMenu) {
            final List<MenuDTO> menuPai = (List<MenuDTO>) menuDao.listarMenuPai(perfilAcessoMenu.getIdMenu());

            if (menuPai != null && !menuPai.isEmpty() && menuPai.get(0).getIdMenuPai() != null && menuPai.get(0).getIdMenuPai() != 0) {
                final List<PerfilAcessoMenuDTO> perfilAcessoComMenuPai = (List) this.getDao().pesquisaSeJaExisteAcessoMenuPai(perfilAcessoMenu.getIdPerfilAcesso(),
                        menuPai.get(0).getIdMenuPai());
                if (perfilAcessoComMenuPai == null || perfilAcessoComMenuPai.isEmpty()) {
                    // criar acesso para o menu pai
                    final PerfilAcessoMenuDTO perfilAcessoMenuPaiDto = new PerfilAcessoMenuDTO();
                    perfilAcessoMenuPaiDto.setDataInicio(UtilDatas.getDataAtual());
                    perfilAcessoMenuPaiDto.setDeleta("S");
                    perfilAcessoMenuPaiDto.setGrava("S");
                    perfilAcessoMenuPaiDto.setPesquisa("S");
                    perfilAcessoMenuPaiDto.setIdMenu(menuPai.get(0).getIdMenuPai());
                    perfilAcessoMenuPaiDto.setIdPerfilAcesso(perfilAcessoMenu.getIdPerfilAcesso());
                    perfilAcessoMenuDao.create(perfilAcessoMenuPaiDto);
                }
            }
        }

        tc.commit();
        tc.close();
    }

    /**
     * Obt�m um Mapa<idMenu, List<PerfilAcessoMenu> > de todos os menus deste usu�rio
     * 
     * @author thyen.chang
     * @since 28/01/2015 - OPERA��O USAIN BOLT
     */
	@Override
	public Map<Integer, List<PerfilAcessoMenuDTO> > getPerfilAcessoBotoesMenu(UsuarioDTO usuario) throws Exception {
		return this.getDao().getPerfilAcessoBotoesMenu(usuario);
	}

}
