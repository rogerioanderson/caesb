/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import br.com.centralit.citcorpore.bean.SolucaoContornoDTO;
import br.com.centralit.citcorpore.integracao.SolucaoContornoDao;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

public class SolucaoContornoServiceEjb extends CrudServiceImpl implements SolucaoContornoService {

    private SolucaoContornoDao dao;

    @Override
    protected SolucaoContornoDao getDao() {
        if (dao == null) {
            dao = new SolucaoContornoDao();
        }
        return dao;
    }

    @Override
    public SolucaoContornoDTO findSolucaoContorno(final SolucaoContornoDTO solucaoContorno) throws Exception {
        return (SolucaoContornoDTO) this.getDao().restore(solucaoContorno);
    }

    @Override
    public SolucaoContornoDTO findByIdProblema(final SolucaoContornoDTO solucaoContorno) throws Exception {
        return (SolucaoContornoDTO) this.getDao().findByIdProblema(solucaoContorno);
    }

    @Override
    public SolucaoContornoDTO create(final SolucaoContornoDTO solucaoContornoDto, TransactionControler tc) throws Exception {
        solucaoContornoDto.setDataHoraCriacao(UtilDatas.getDataHoraAtual());
        final SolucaoContornoDao solucaoContornoDao = new SolucaoContornoDao();

        if (tc == null) {
            tc = new TransactionControlerImpl(this.getDao().getAliasDB());
            tc.start();
        }

        solucaoContornoDao.setTransactionControler(tc);

        return (SolucaoContornoDTO) solucaoContornoDao.create(solucaoContornoDto);
    }

}
