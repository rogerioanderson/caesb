/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.ExcecaoEmpregadoDTO;
import br.com.centralit.citcorpore.integracao.ExcecaoEmpregadoDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class ExcecaoEmpregadoServiceEjb extends CrudServiceImpl implements ExcecaoEmpregadoService {

    private ExcecaoEmpregadoDao dao;

    @Override
    protected ExcecaoEmpregadoDao getDao() {
        if (dao == null) {
            dao = new ExcecaoEmpregadoDao();
        }
        return dao;
    }

    @Override
    public Collection<ExcecaoEmpregadoDTO> listByIdEventoIdGrupo(final Integer idEvento, final Integer idGrupo) throws ServiceException {
        try {
            return this.getDao().listByIdEventoIdGrupo(idEvento, idGrupo);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<ExcecaoEmpregadoDTO> listByIdEventoIdUnidade(final Integer idEvento, final Integer idUnidade) throws ServiceException {
        try {
            return this.getDao().listByIdEventoIdUnidade(idEvento, idUnidade);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
