/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.LinguaDTO;
import br.com.centralit.citcorpore.integracao.LinguaDao;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class LinguaServiceEjb extends CrudServiceImpl implements LinguaService {

    private LinguaDao dao;

    @Override
    protected LinguaDao getDao() {
        if (dao == null) {
            dao = new LinguaDao();
        }
        return dao;
    }

    @Override
    public boolean consultarLinguaAtivas(final LinguaDTO obj) throws ServiceException {
        try {
            return this.getDao().consultarLinguaAtivas(obj);
        } catch (final PersistenceException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public LinguaDTO getIdLingua(final LinguaDTO obj) throws ServiceException {
        try {
            return this.getDao().getIdLingua(obj);
        } catch (final PersistenceException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<LinguaDTO> listarAtivos() throws ServiceException {
        try {
            return this.getDao().listarAtivos();
        } catch (final PersistenceException e) {
            throw new ServiceException(e);
        }
    }

}
