/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 *
 */
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.GlosaServicoContratoDTO;
import br.com.centralit.citcorpore.integracao.GlosaServicoContratoDAO;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

/**
 * @author rodrigo.oliveira
 *
 */
public class GlosaServicoContratoServiceEjb extends CrudServiceImpl implements GlosaServicoContratoService {

    private GlosaServicoContratoDAO dao;

    @Override
    protected GlosaServicoContratoDAO getDao() {
        if (dao == null) {
            dao = new GlosaServicoContratoDAO();
        }
        return dao;
    }

    @Override
    public Collection findByIdServicoContrato(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdServicoContrato(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Integer quantidadeGlosaServico(final Integer idServicoContrato) throws Exception {
        Integer quantidade = 0;
        try {
            final List<GlosaServicoContratoDTO> listResult = (List<GlosaServicoContratoDTO>) this.getDao().quantidadeGlosaServico(idServicoContrato);
            if (listResult != null && listResult.size() > 0) {
                quantidade = listResult.get(0).getQuantidadeGlosa();
            }
            return quantidade;
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void atualizaQuantidadeGlosa(final Integer novaQuantidade, final Integer idServicoContrato) throws Exception {
        try {
            this.getDao().atualizaQuantidadeGlosa(novaQuantidade, idServicoContrato);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
