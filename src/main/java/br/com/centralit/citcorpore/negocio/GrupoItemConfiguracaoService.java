/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.GrupoItemConfiguracaoDTO;
import br.com.centralit.citcorpore.integracao.GrupoItemConfiguracaoDAO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

public interface GrupoItemConfiguracaoService extends CrudService {
	
	public boolean VerificaSeCadastrado(GrupoItemConfiguracaoDTO grupoItemConfiguracaoDTO) throws PersistenceException;
	
	/**
	 * M�todo que traz a lista de Grupo de item de configura��o relacionados ao evento passado como par�metro
	 * 
	 * @param idEvento
	 * @return Collection<GrupoItemConfiguracaoDTO> relacionado ao evento
	 * @throws ServiceException
	 */
	public Collection<GrupoItemConfiguracaoDTO> listByEvento(Integer idEvento) throws ServiceException;
	
	public void updateNotNull(IDto dto) throws Exception;
	
	public boolean verificaICRelacionados(GrupoItemConfiguracaoDTO grupoItemConfiguracao) throws PersistenceException, Exception;
	
	public Collection<GrupoItemConfiguracaoDTO> listByIdGrupoItemConfiguracaoPai(Integer idGrupoItemConfiguracaoPai) throws Exception;
	
	public Collection<GrupoItemConfiguracaoDTO> listByIdGrupoItemConfiguracaoDesenvolvimento(Integer idGrupoItemConfiguracaoPai) throws Exception;
	
	public Collection<GrupoItemConfiguracaoDTO> listByIdGrupoItemConfiguracao(Integer idGrupoItemConfiguracaoPai) throws Exception;
	
	public Collection listHierarquiaGruposByIdGrupo(Integer idGrupo, GrupoItemConfiguracaoDAO dao) throws Exception;
	
	public Collection listHierarquiaGrupoPaiNull() throws Exception;
	
	public void autenticaGrupoPadrao(int id, int idPai, String nome) throws Exception;
	
}
