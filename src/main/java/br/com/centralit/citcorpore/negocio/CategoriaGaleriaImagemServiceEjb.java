/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.CategoriaGaleriaImagemDTO;
import br.com.centralit.citcorpore.integracao.CategoriaGaleriaImagemDao;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

public class CategoriaGaleriaImagemServiceEjb extends CrudServiceImpl implements CategoriaGaleriaImagemService {

    private CategoriaGaleriaImagemDao dao;

    @Override
    protected CategoriaGaleriaImagemDao getDao() {
        if (dao == null) {
            dao = new CategoriaGaleriaImagemDao();
        }
        return dao;
    }

    /**
     * Deleta Categoria Imagem Ativos
     *
     * @param model
     * @param document
     * @return
     * @throws Exception
     * @author cledson.junior
     */
    @Override
    public void deletarCategoriaImagem(final IDto model, final DocumentHTML document) throws ServiceException, Exception {
        final CategoriaGaleriaImagemDTO categoriaGaleriaImagemDto = (CategoriaGaleriaImagemDTO) model;
        final TransactionControler tc = new TransactionControlerImpl(this.getDao().getAliasDB());

        try {
            this.validaUpdate(model);
            this.getDao().setTransactionControler(tc);
            tc.start();
            categoriaGaleriaImagemDto.setDataFim(UtilDatas.getDataAtual());
            this.getDao().update(model);
            document.alert(this.i18nMessage("MSG07"));
            tc.commit();
            tc.close();
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        }

    }

    /**
     * Consultar Categoria Imagem Ativos
     *
     * @param obj
     * @return
     * @throws Exception
     * @author cledson.junior
     */
    @Override
    public boolean consultarCategoriaImagemAtivos(final CategoriaGaleriaImagemDTO obj) throws Exception {
        return this.getDao().consultarCategoriaImagemAtivos(obj);
    }

}
