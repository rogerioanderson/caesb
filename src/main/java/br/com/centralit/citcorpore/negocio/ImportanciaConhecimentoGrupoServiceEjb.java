/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 *
 */
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;
import br.com.centralit.citcorpore.bean.GrupoEmpregadoDTO;
import br.com.centralit.citcorpore.bean.ImportanciaConhecimentoGrupoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.ImportanciaConhecimentoGrupoDAO;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudServiceImpl;

/**
 * @author Vadoilo Damasceno
 *
 */
public class ImportanciaConhecimentoGrupoServiceEjb extends CrudServiceImpl implements ImportanciaConhecimentoGrupoService {

    private ImportanciaConhecimentoGrupoDAO dao;

    @Override
    protected ImportanciaConhecimentoGrupoDAO getDao() {
        if (dao == null) {
            dao = new ImportanciaConhecimentoGrupoDAO();
        }
        return dao;
    }

    @Override
    public void deleteByIdConhecimento(final Integer idBaseConhecimento, final TransactionControler transactionControler) throws Exception {
        final ImportanciaConhecimentoGrupoDAO importanciaConhecimentoGrupoDao = new ImportanciaConhecimentoGrupoDAO();
        importanciaConhecimentoGrupoDao.setTransactionControler(transactionControler);
        importanciaConhecimentoGrupoDao.deleteByIdConhecimento(idBaseConhecimento);
    }

    @Override
    public void create(final ImportanciaConhecimentoGrupoDTO importanciaConhecimentoGrupo, final TransactionControler transactionControler) throws Exception {
        final ImportanciaConhecimentoGrupoDAO importanciaConhecimentoGrupoDao = new ImportanciaConhecimentoGrupoDAO();
        importanciaConhecimentoGrupoDao.setTransactionControler(transactionControler);
        importanciaConhecimentoGrupoDao.create(importanciaConhecimentoGrupo);
    }

    @Override
    public Collection<ImportanciaConhecimentoGrupoDTO> listByIdBaseConhecimento(final Integer idBaseConhecimento) throws Exception {
        return this.getDao().listByIdBaseConhecimento(idBaseConhecimento);
    }

    @Override
    public ImportanciaConhecimentoGrupoDTO obterGrauDeImportancia(final BaseConhecimentoDTO baseConhecimentoDto, final Collection<GrupoEmpregadoDTO> listGrupoEmpregado,
            final UsuarioDTO usuarioDto) throws Exception {
        return this.getDao().obterGrauDeImportancia(baseConhecimentoDto, listGrupoEmpregado, usuarioDto);
    }

}
