/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.ScriptsDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

public interface ScriptsService extends CrudService {

    ScriptsDTO buscaScriptPorId(final Integer id) throws Exception;

    void deletarScript(final IDto model, final DocumentHTML document) throws Exception;

    String executaRotinaScripts() throws Exception;

    List<String[]> executarScriptConsulta(final ScriptsDTO script) throws Exception;

    String executarScriptUpdate(final ScriptsDTO script) throws Exception;

    boolean haScriptDeVersaoComErro() throws Exception;

    /**
     * Verifica se j� existe algum ScriptDTO com o mesmo NOME
     *
     * @return Retorna se j� existe algum ScriptDTO com o mesmo NOME
     * @author Murilo Gabriel
     */
    boolean temScriptsAtivos(final ScriptsDTO script) throws Exception;

    void marcaErrosScriptsComoCorrigidos() throws Exception;

    String verificaPermissoesUsuarioBanco(final HttpServletRequest request) throws ServiceException;

    List<ScriptsDTO> pesquisaScriptsComFaltaPermissao() throws Exception;

}
