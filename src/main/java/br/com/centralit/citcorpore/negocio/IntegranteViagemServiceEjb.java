/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.IntegranteViagemDTO;
import br.com.centralit.citcorpore.bean.PrestacaoContasViagemDTO;
import br.com.centralit.citcorpore.integracao.IntegranteViagemDao;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class IntegranteViagemServiceEjb extends CrudServiceImpl implements IntegranteViagemService{

	/**
	 *
	 */
	private static final long serialVersionUID = 3414511296205878674L;

	@Override
	protected IntegranteViagemDao getDao() {
		return new IntegranteViagemDao();
	}

	@Override
	protected void validaCreate(Object obj) throws Exception {

	}

	@Override
	protected void validaUpdate(Object obj) throws Exception {

	}

	@Override
	protected void validaDelete(Object obj) throws Exception {

	}

	@Override
	protected void validaFind(Object obj) throws Exception {

	}

	public IntegranteViagemDTO findById(Integer idIntegranteViagem) throws Exception{
		return this.getDao().findById(idIntegranteViagem);
	}

	public Collection<IntegranteViagemDTO> findAllRemarcacaoByIdSolicitacao(Integer idSolicitacaoServico){

		try {
			return this.getDao().findAllRemarcacaoByIdSolicitacao(idSolicitacaoServico);
		} catch (Exception e) {
			return null;
		}

	}


	@Override
	public Collection<IntegranteViagemDTO> recuperaIntegrantesRemarcacao(IntegranteViagemDTO integranteViagemDTO, String eOu) throws Exception{
		IntegranteViagemDao integranteViagemDao = this.getDao();
		Collection<IntegranteViagemDTO> colIntegrantes = new ArrayList<IntegranteViagemDTO>();

		colIntegrantes = integranteViagemDao.findAllIntegrantesParaRemarcacao(integranteViagemDTO, eOu);

		return colIntegrantes;
	}

	@Override
	public Collection<IntegranteViagemDTO> recuperaIntegrantesAdiantamento(IntegranteViagemDTO integranteViagemDTO, String eOu) throws Exception{
		IntegranteViagemDao integranteViagemDao = this.getDao();
		Collection<IntegranteViagemDTO> colIntegrantes = new ArrayList<IntegranteViagemDTO>();

		colIntegrantes = integranteViagemDao.recuperaIntegrantesAdiantamento(integranteViagemDTO, eOu);

		return colIntegrantes;
	}

	@Override
	public Collection<IntegranteViagemDTO> recuperaIntegrantesPrestacaoContas(IntegranteViagemDTO integranteViagemDTO, String eOu) throws Exception{
		IntegranteViagemDao integranteViagemDao = this.getDao();
		Collection<IntegranteViagemDTO> colIntegrantes = new ArrayList<IntegranteViagemDTO>();

		colIntegrantes = integranteViagemDao.recuperaIntegrantesPrestacaoContas(integranteViagemDTO, eOu);

		return colIntegrantes;
	}

	@Override
	public Collection<IntegranteViagemDTO> recuperaIntegrantesViagemByIdSolicitacao(Integer idSolicitacaoServico) throws Exception {
		IntegranteViagemDao integranteViagemDao = this.getDao();
		Collection<IntegranteViagemDTO> ColIntegrantes =  integranteViagemDao.findAllByIdSolicitacao(idSolicitacaoServico);
		return ColIntegrantes;
	}

	@Override
	public Collection<IntegranteViagemDTO> recuperaIntegrantesViagemByIdSolicitacaoEstado(Integer idSolicitacao, String estado) throws Exception {
		IntegranteViagemDao dao = (IntegranteViagemDao) this.getDao();
		return dao.recuperaIntegrantesViagemByIdSolicitacaoEstado(idSolicitacao, estado);
	}

	@Override
	public IntegranteViagemDTO getIntegranteByIdSolicitacaoAndTarefa(Integer idsolicitacaoServico, Integer idTarefa) throws Exception {
		IntegranteViagemDao dao = (IntegranteViagemDao) this.getDao();
		return dao.getIntegranteByIdSolicitacaoAndTarefa(idsolicitacaoServico, idTarefa);
	}

	@Override
	public Collection<IntegranteViagemDTO> recuperaIntegrantesByIdResponsavel(IntegranteViagemDTO integranteViagemDTO, String eOu) throws Exception {
		IntegranteViagemDao dao = (IntegranteViagemDao) this.getDao();
		return dao.recuperaIntegrantesByIdResponsavel(integranteViagemDTO, eOu);
	}

	/**
     * {@inheritDoc}
     */
    @Override
    public void expiraPrestacaoContasPendente(IntegranteViagemDTO integranteViagemDTO) throws ServiceException, LogicException, PersistenceException {
    	integranteViagemDTO = (IntegranteViagemDTO) this.restore(integranteViagemDTO);

    	integranteViagemDTO.setEstado(PrestacaoContasViagemDTO.AGUARDANDO_EXPIRAR_PRAZO);
    	integranteViagemDTO.setEmPrestacaoContas("N");

    	IntegranteViagemDao integranteViagemDao = new IntegranteViagemDao();
    	integranteViagemDao.update(integranteViagemDTO);
    }
    
    /**
     * {@inheritDoc}
     */
    public Collection<IntegranteViagemDTO> findAllIntegrantesViagemPrestouContas(IntegranteViagemDTO integranteViagemDTO) throws PersistenceException {
    	return this.getDao().findAllIntegrantesViagemPrestouContas(integranteViagemDTO);
    }
}
