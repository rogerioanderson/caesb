/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.ImportarDadosDTO;
import br.com.centralit.citcorpore.integracao.ImportarDadosDao;
import br.com.centralit.citged.bean.ControleGEDDTO;
import br.com.centralit.citged.negocio.ControleGEDServiceBean;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;

public class ImportarDadosServiceEjb extends CrudServiceImpl implements ImportarDadosService {

    private ImportarDadosDao dao;

    @Override
    protected ImportarDadosDao getDao() {
        if (dao == null) {
            dao = new ImportarDadosDao();
        }
        return dao;
    }

    @Override
    public IDto create(final IDto model) throws ServiceException, LogicException {
        final ImportarDadosDao importarDadosDao = new ImportarDadosDao();
        final TransactionControler tc = new TransactionControlerImpl(importarDadosDao.getAliasDB());

        try {
            this.validaCreate(model);

            importarDadosDao.setTransactionControler(tc);

            tc.start();

            ImportarDadosDTO importarDadosDto = (ImportarDadosDTO) model;
            importarDadosDto = (ImportarDadosDTO) importarDadosDao.create(importarDadosDto);

            this.atualizaAnexos(importarDadosDto, tc);

            tc.commit();
            tc.close();
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        }
        return model;
    }

    @Override
    public void update(final IDto model) throws ServiceException, LogicException {
        final ImportarDadosDao importarDadosDao = new ImportarDadosDao();
        final TransactionControler tc = new TransactionControlerImpl(importarDadosDao.getAliasDB());

        try {
            this.validaUpdate(model);

            importarDadosDao.setTransactionControler(tc);

            tc.start();

            final ImportarDadosDTO importarDadosDto = (ImportarDadosDTO) model;
            importarDadosDao.update(importarDadosDto);

            this.atualizaAnexos(importarDadosDto, tc);

            tc.commit();
            tc.close();
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        }
    }

    private void atualizaAnexos(final ImportarDadosDTO importarDadosDto, final TransactionControler tc) throws Exception {
        new ControleGEDServiceBean().atualizaAnexos(importarDadosDto.getAnexos(), ControleGEDDTO.TABELA_IMPORTARDADOS, importarDadosDto.getIdImportarDados(), tc);
    }

    /**
     * Retorna lista com os registros da tabela ImportarDados relacionados ao idExternalConnection
     */
    @Override
    public Collection<ImportarDadosDTO> consultarImportarDadosRelacionados(final Integer idExternalConnection) throws Exception {
        return this.getDao().consultarImportarDadosPeloExternalConnection(idExternalConnection);
    }

}
