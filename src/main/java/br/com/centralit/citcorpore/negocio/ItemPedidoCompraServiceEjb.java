/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.ItemPedidoCompraDTO;
import br.com.centralit.citcorpore.bean.ProdutoDTO;
import br.com.centralit.citcorpore.bean.UnidadeMedidaDTO;
import br.com.centralit.citcorpore.integracao.ItemPedidoCompraDao;
import br.com.centralit.citcorpore.integracao.ProdutoDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class ItemPedidoCompraServiceEjb extends CrudServiceImpl implements ItemPedidoCompraService {

    private ItemPedidoCompraDao dao;

    @Override
    protected ItemPedidoCompraDao getDao() {
        if (dao == null) {
            dao = new ItemPedidoCompraDao();
        }
        return dao;
    }

    public void recuperaRelacionamentos(final Collection<ItemPedidoCompraDTO> colItens) throws Exception {
        if (colItens != null) {
            final ProdutoDao produtoDao = new ProdutoDao();
            for (final ItemPedidoCompraDTO itemDto : colItens) {
                new UnidadeMedidaDTO();
                if (itemDto.getIdProduto() != null) {
                    ProdutoDTO produtoDto = new ProdutoDTO();
                    produtoDto.setIdProduto(itemDto.getIdProduto());
                    produtoDto = (ProdutoDTO) produtoDao.restore(produtoDto);
                    if (produtoDto != null) {
                        itemDto.setCodigoProduto(produtoDto.getCodigoProduto());
                        itemDto.setDescricaoItem(produtoDto.getNomeProduto());
                    }
                }
            }
        }
    }

    @Override
    public Collection findByIdPedido(final Integer parm) throws Exception {
        try {
            final Collection<ItemPedidoCompraDTO> colItens = this.getDao().findByIdPedido(parm);
            this.recuperaRelacionamentos(colItens);
            return colItens;
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdPedidoOrderByIdSolicitacao(final Integer idPedido) throws Exception {
        try {
            final Collection<ItemPedidoCompraDTO> colItens = this.getDao().findByIdPedidoOrderByIdSolicitacao(idPedido);
            this.recuperaRelacionamentos(colItens);
            return colItens;
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdPedido(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdPedido(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public double obtemQtdePedidoColetaPreco(final Integer idColetaPreco) throws Exception {
        double qtde = 0;
        try {
            final Collection<ItemPedidoCompraDTO> colItens = this.getDao().findByIdColetaPreco(idColetaPreco);
            if (colItens != null) {
                for (final ItemPedidoCompraDTO itemPedidoDto : colItens) {
                    qtde += itemPedidoDto.getQuantidade().doubleValue();
                }
            }
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
        return qtde;
    }

}
