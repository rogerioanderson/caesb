/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import br.com.centralit.citcorpore.bean.SolucaoDefinitivaDTO;
import br.com.centralit.citcorpore.integracao.SolucaoDefinitivaDAO;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

public class SolucaoDefinitivaServiceEjb extends CrudServiceImpl implements SolucaoDefinitivaService {

    private SolucaoDefinitivaDAO dao;

    @Override
    protected SolucaoDefinitivaDAO getDao() {
        if (dao == null) {
            dao = new SolucaoDefinitivaDAO();
        }
        return dao;
    }

    @Override
    public SolucaoDefinitivaDTO findSolucaoDefinitiva(final SolucaoDefinitivaDTO solucaoDefinitiva) throws Exception {
        return (SolucaoDefinitivaDTO) this.getDao().restore(solucaoDefinitiva);
    }

    @Override
    public SolucaoDefinitivaDTO create(final SolucaoDefinitivaDTO solucaoDefinitivaDto, TransactionControler tc) throws Exception {
        solucaoDefinitivaDto.setDataHoraCriacao(UtilDatas.getDataHoraAtual());
        final SolucaoDefinitivaDAO solucaoDefinitivaDao = new SolucaoDefinitivaDAO();

        if (tc == null) {
            tc = new TransactionControlerImpl(this.getDao().getAliasDB());
            tc.start();
        }
        solucaoDefinitivaDao.setTransactionControler(tc);
        return (SolucaoDefinitivaDTO) solucaoDefinitivaDao.create(solucaoDefinitivaDto);

    }

    @Override
    public SolucaoDefinitivaDTO findByIdProblema(final SolucaoDefinitivaDTO solucaoDefinitiva) throws Exception {
        return (SolucaoDefinitivaDTO) this.getDao().findByIdProblema(solucaoDefinitiva);
    }

}
