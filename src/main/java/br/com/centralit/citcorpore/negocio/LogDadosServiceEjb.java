/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.citframework.dto.IDto;
import br.com.citframework.dto.LogDados;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.LogDadosDao;
import br.com.citframework.service.CrudServiceImpl;

/**
 * @author karem.ricarte
 *
 */
@SuppressWarnings("unchecked")
public class LogDadosServiceEjb extends CrudServiceImpl implements LogDadosService {

    private LogDadosDao dao;

    @Override
    protected LogDadosDao getDao() {
        if (dao == null) {
            dao = new LogDadosDao(usuario);
        }
        return dao;
    }

    @Override
    public Collection<LogDados> listAllLogs() throws Exception {
        return this.getDao().listAllLogs();
    }

    @Override
    public Collection<LogDados> listLogs(final LogDados log) throws Exception {
        return this.getDao().listLogs(log);
    }

    @Override
    public Collection<LogDados> listNomeTabela() throws Exception {
        return this.getDao().listNomeTabela();
    }

	@Override
	public IDto create(IDto model) throws ServiceException, LogicException {
		try {
			return this.getDao().create(model);
		} catch (PersistenceException e) {
			e.printStackTrace();
		}

		return null;
	}
}
