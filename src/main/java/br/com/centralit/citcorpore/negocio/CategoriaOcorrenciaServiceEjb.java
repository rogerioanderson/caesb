/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.CategoriaOcorrenciaDTO;
import br.com.centralit.citcorpore.integracao.CategoriaOcorrenciaDAO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

/**
 * @author thiago.monteiro
 */
public class CategoriaOcorrenciaServiceEjb extends CrudServiceImpl implements CategoriaOcorrenciaService {

    private CategoriaOcorrenciaDAO dao;

    @Override
    protected CategoriaOcorrenciaDAO getDao() {
        if (dao == null) {
            dao = new CategoriaOcorrenciaDAO();
        }
        return dao;
    }

	@Override
	public IDto create(IDto model) throws ServiceException, LogicException {
		return super.create(removerEspacosNoInicioEouFimDoNome(model));
	}

    @Override
	public void update(IDto model) throws ServiceException, LogicException {
		super.update(removerEspacosNoInicioEouFimDoNome(model));
	}

	private CategoriaOcorrenciaDTO removerEspacosNoInicioEouFimDoNome(IDto model) {
		CategoriaOcorrenciaDTO categoriaOcorrenciaDTO = (CategoriaOcorrenciaDTO) model;
		categoriaOcorrenciaDTO.setNome(categoriaOcorrenciaDTO.getNome().trim());

		return categoriaOcorrenciaDTO;
	}

	@Override
    public void deletarCategoriaOcorrencia(final IDto model, final DocumentHTML document) throws ServiceException, Exception {
        CategoriaOcorrenciaDTO categoriaOcorrenciaDTO = (CategoriaOcorrenciaDTO) model;
        final TransactionControler tc = new TransactionControlerImpl(this.getDao().getAliasDB());

        try {
            this.validaUpdate(model);
			// Configura transa��es para cada entidade a ser registrada no banco
			// de dados
            this.getDao().setTransactionControler(tc);
			// inicia transa��o
            tc.start();
            categoriaOcorrenciaDTO = (CategoriaOcorrenciaDTO) this.getDao().restore(categoriaOcorrenciaDTO);
            categoriaOcorrenciaDTO.setDataFim(UtilDatas.getDataAtual());
            this.getDao().update(categoriaOcorrenciaDTO);
            document.alert(this.i18nMessage("MSG07"));
			// confirma transa��o
            tc.commit();
			// encerra transa��o
            tc.close();
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        }
    }

    @Override
    public boolean consultarCategoriaOcorrenciaAtiva(final CategoriaOcorrenciaDTO obj) throws Exception {
        return this.getDao().consultarCategoriaOcorrenciaAtiva(obj);
    }

    @Override
    public CategoriaOcorrenciaDTO restoreAll(final Integer idCategoriaOcorrencia) throws Exception {
        return this.getDao().restoreAll(idCategoriaOcorrencia);
    }

}
