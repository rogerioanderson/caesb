/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import br.com.centralit.bpm.dto.ItemTrabalhoFluxoDTO;
import br.com.centralit.citcorpore.bean.JustificativaSolicitacaoDTO;
import br.com.centralit.citcorpore.bean.OcorrenciaSolicitacaoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.OcorrenciaSolicitacaoDao;
import br.com.centralit.citcorpore.util.Enumerados.CategoriaOcorrencia;
import br.com.centralit.citcorpore.util.Enumerados.OrigemOcorrencia;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilStrings;

import com.google.gson.Gson;

/**
 * @author breno.guimaraes
 *
 */
public class OcorrenciaSolicitacaoServiceEjb extends CrudServiceImpl implements OcorrenciaSolicitacaoService {

    private OcorrenciaSolicitacaoDao ocorrenciaSolicitacaoDao;

    @Override
    protected OcorrenciaSolicitacaoDao getDao() {
        if (ocorrenciaSolicitacaoDao == null) {
            ocorrenciaSolicitacaoDao = new OcorrenciaSolicitacaoDao();
        }
        return ocorrenciaSolicitacaoDao;
    }

	@Override
	public IDto create(IDto model) throws ServiceException, LogicException {
		return super.create(removerEspacosNoInicioEouFimDosAtributosString(model));
	}

	@Override
	public void update(IDto model) throws ServiceException, LogicException {
		super.update(removerEspacosNoInicioEouFimDosAtributosString(model));
	}

	private OcorrenciaSolicitacaoDTO removerEspacosNoInicioEouFimDosAtributosString(IDto model) {
		OcorrenciaSolicitacaoDTO ocorrenciaSolicitacaoDTO = (OcorrenciaSolicitacaoDTO) model;
		ocorrenciaSolicitacaoDTO.setDescricao(ocorrenciaSolicitacaoDTO.getDescricao().trim());
		ocorrenciaSolicitacaoDTO.setOcorrencia(ocorrenciaSolicitacaoDTO.getOcorrencia().trim());
		ocorrenciaSolicitacaoDTO.setInformacoesContato(ocorrenciaSolicitacaoDTO.getInformacoesContato().trim());

		return ocorrenciaSolicitacaoDTO;
	}

    @Override
    public Collection findByIdSolicitacaoServico(final Integer idSolicitacaoServicoParm) throws Exception {
        return this.getDao().findByIdSolicitacaoServico(idSolicitacaoServicoParm);
    }

    @Override
    public OcorrenciaSolicitacaoDTO findUltimoByIdSolicitacaoServico(final Integer idSolicitacaoServicoParm) throws Exception {
        return this.getDao().findUltimoByIdSolicitacaoServico(idSolicitacaoServicoParm);
    }

    @Override
    public Collection findOcorrenciasDoTesteIdSolicitacaoServico(final Integer idSolicitacaoServicoParm) throws Exception {
        return this.getDao().findOcorrenciaDoTesteByIdSolicitacaoServico(idSolicitacaoServicoParm);
    }

    public static OcorrenciaSolicitacaoDTO create(final SolicitacaoServicoDTO solicitacaoServicoDto, final ItemTrabalhoFluxoDTO itemTrabalhoFluxoDto, final String ocorrencia,
            final OrigemOcorrencia origem, final CategoriaOcorrencia categoria, final String informacoesContato, final String descricao, final UsuarioDTO usuarioDTO,
            final int tempo, final JustificativaSolicitacaoDTO justificativaDto, final TransactionControler tc) throws Exception {
        final OcorrenciaSolicitacaoDTO ocorrenciaSolicitacaoDTO = new OcorrenciaSolicitacaoDTO();
        ocorrenciaSolicitacaoDTO.setIdSolicitacaoServico(solicitacaoServicoDto.getIdSolicitacaoServico());
        ocorrenciaSolicitacaoDTO.setDataregistro(UtilDatas.getDataAtual());
        ocorrenciaSolicitacaoDTO.setHoraregistro(UtilDatas.formatHoraFormatadaStr(UtilDatas.getHoraAtual()));
        ocorrenciaSolicitacaoDTO.setTempoGasto(tempo);
        ocorrenciaSolicitacaoDTO.setDescricao(descricao);
        ocorrenciaSolicitacaoDTO.setDataInicio(UtilDatas.getDataAtual());
        ocorrenciaSolicitacaoDTO.setDataFim(UtilDatas.getDataAtual());
        ocorrenciaSolicitacaoDTO.setInformacoesContato(informacoesContato);
        ocorrenciaSolicitacaoDTO.setRegistradopor(usuarioDTO.getLogin());
        try {
            ocorrenciaSolicitacaoDTO.setDadosSolicitacao(new Gson().toJson(solicitacaoServicoDto));
        } catch (final Exception e) {
			System.out.println("Problema na grava��o dos dados da solicita��o de servi�o de id " + solicitacaoServicoDto.getIdSolicitacaoServico());
            e.printStackTrace();
        }
        ocorrenciaSolicitacaoDTO.setOcorrencia(ocorrencia);
        ocorrenciaSolicitacaoDTO.setOrigem(origem.getSigla().toString());
        ocorrenciaSolicitacaoDTO.setCategoria(categoria.getSigla());
        if (itemTrabalhoFluxoDto != null) {
            ocorrenciaSolicitacaoDTO.setIdItemTrabalho(itemTrabalhoFluxoDto.getIdItemTrabalho());
        }
        if (justificativaDto != null) {
            ocorrenciaSolicitacaoDTO.setIdJustificativa(justificativaDto.getIdJustificativa());
            ocorrenciaSolicitacaoDTO.setComplementoJustificativa(justificativaDto.getComplementoJustificativa());
        }

        final OcorrenciaSolicitacaoDao ocorrenciaSolicitacaoDao = new OcorrenciaSolicitacaoDao();
        if (tc != null) {
            ocorrenciaSolicitacaoDao.setTransactionControler(tc);
        }
        return (OcorrenciaSolicitacaoDTO) ocorrenciaSolicitacaoDao.create(ocorrenciaSolicitacaoDTO);
    }

    @Override
    public Collection<OcorrenciaSolicitacaoDTO> findByIdPessoaEDataAtendidasGrupoTeste(final Integer idPessoa, final Date dataInicio, final Date dataFim) throws Exception {
        try {
            return this.getDao().findByIdPessoaEDataAtendidasGrupoTeste(idPessoa, dataInicio, dataFim);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<OcorrenciaSolicitacaoDTO> findByIdPessoaGrupoTeste(final Integer idPessoa, final Date dataInicio, final Date dataFim) throws Exception {
        try {
            return this.getDao().findByIdPessoaGrupoTeste(idPessoa, dataInicio, dataFim);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public OcorrenciaSolicitacaoDTO findByIdOcorrencia(final Integer idOcorrencia) throws Exception {
        try {
            return this.getDao().findByIdOcorrencia(idOcorrencia);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public long quantidadeDeOcorrenciasDeAlteracaoSlaPorNumeroDaSolicitacao(final int idSolicitacaoServico) throws Exception {
        final OcorrenciaSolicitacaoDTO lista = this.getDao().quantidadeDeOcorrenciasDeAlteracaoSlaPorNumeroDaSolicitacao(idSolicitacaoServico);

        return lista.getTotalOcorrenciasAlterarcaoSlaPorSolicitacao();
    }

	@Override
	public void updateDataHora(OcorrenciaSolicitacaoDTO ocorrencia, Date data, String hora, TransactionControler tc) throws Exception {
		if (data != null) {
			ocorrencia.setDataregistro(new java.sql.Date(data.getTime()));
		}
		if (!UtilStrings.isNullOrEmpty(hora)) {
			String horaAux = UtilDatas.formatHoraHHMM(hora);
			ocorrencia.setHoraregistro(UtilDatas.formatHoraStr(horaAux));
		}
		OcorrenciaSolicitacaoDao dao = new OcorrenciaSolicitacaoDao();
		if (tc != null) {
			dao.setTransactionControler(tc);
		}
		dao.update(ocorrencia);
	}
	
	@Override
	public List<OcorrenciaSolicitacaoDTO> findNaoSincronizadas(Integer idSolicitacaoServico, String origemSincronizacao, List<String> categorias) throws PersistenceException {
		return this.getDao().findNaoSincronizadas(idSolicitacaoServico, origemSincronizacao, categorias);
	}

	@Override
	public void updateNotNull(OcorrenciaSolicitacaoDTO ocorrencia, TransactionControler tc) throws PersistenceException {
		OcorrenciaSolicitacaoDao dao = new OcorrenciaSolicitacaoDao();
		if (tc != null) {
			dao.setTransactionControler(tc);
		}
		dao.updateNotNull(ocorrencia);
	}
	
	@Override
	public OcorrenciaSolicitacaoDTO findUltimoByIdSolicitacaoAndCategoria(Integer idSolicitacaoServicoParm, String categoria, TransactionControler tc) throws Exception {
		OcorrenciaSolicitacaoDao dao = new OcorrenciaSolicitacaoDao();
		if (tc != null) {
			dao.setTransactionControler(tc);
		}
        return dao.findUltimoByIdSolicitacaoAndCategoria(idSolicitacaoServicoParm, categoria);
	}
}
