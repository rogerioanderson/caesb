/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart
 */
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.IncidentesRelacionadosDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.integracao.IncidentesRelacionadosDAO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.service.CrudServiceImpl;

/**
 * @author breno.guimaraes
 */
@Deprecated
public class IncidentesRelacionadosServiceEjb extends CrudServiceImpl implements IncidentesRelacionadosService {

    private IncidentesRelacionadosDAO dao;
    private SolicitacaoServicoServiceEjb solicitacaoServicoServiceEjb;

    @Override
    protected IncidentesRelacionadosDAO getDao() {
        if (dao == null) {
            dao = new IncidentesRelacionadosDAO();
        }
        return dao;
    }

    private SolicitacaoServicoServiceEjb getSolicitacaoServicoEjb() {
        if (solicitacaoServicoServiceEjb == null) {
            solicitacaoServicoServiceEjb = new SolicitacaoServicoServiceEjb();
        }

        return solicitacaoServicoServiceEjb;
    }

    @Override
    public ArrayList<SolicitacaoServicoDTO> listIncidentesRelacionados(final int idSolicitacao) {
        final ArrayList<Condition> condicoes = new ArrayList<Condition>();
        condicoes.add(new Condition("idIncidente", "=", idSolicitacao));
        final SolicitacaoServicoDTO solicitacao = new SolicitacaoServicoDTO();

        final ArrayList<SolicitacaoServicoDTO> retorno = new ArrayList<SolicitacaoServicoDTO>();

        try {
            // pega lista de ids dos incidentes relacionados ao passado como argumento.
            final Collection<IncidentesRelacionadosDTO> incidentesRelacionados = this.getDao().findByCondition(condicoes, null);
            // preenche uma lista com os incidentes relacionados buscando pelos ids obtidos.
            if (incidentesRelacionados != null) {
                for (final IncidentesRelacionadosDTO inc : incidentesRelacionados) {
                    // solicitacao.setIdSolicitacaoServico(inc.getIdIncidenteRelacionado());
                    retorno.add((SolicitacaoServicoDTO) this.getSolicitacaoServicoEjb().restore(solicitacao));
                }
            }

        } catch (final Exception e) {
            e.printStackTrace();
        }
        return retorno;
    }

    @Override
    public IDto create(final IDto dto) throws ServiceException, LogicException {
        final IncidentesRelacionadosDTO icRelacionadoDto = (IncidentesRelacionadosDTO) dto;
        return super.create(icRelacionadoDto);
    }

}
