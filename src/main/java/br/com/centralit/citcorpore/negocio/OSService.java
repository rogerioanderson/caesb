/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.sql.Date;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.OSDTO;
import br.com.centralit.citcorpore.bean.RelatorioAcompanhamentoDTO;
import br.com.centralit.citcorpore.bean.RelatorioOrdemServicoUstDTO;
import br.com.citframework.service.CrudService;

@SuppressWarnings("rawtypes")
public interface OSService extends CrudService {

	public void updateSituacao(Integer idOs, Integer situacao, Collection colGlosasOS, Collection colItens, String obsFinalizacao) throws Exception;

	public void updateSituacao(OSDTO os, Collection colGlosas, Collection colItens) throws Exception;

	public Collection findByIdContrato(Integer parm) throws Exception;

	public void deleteByIdContrato(Integer parm) throws Exception;

	public Collection findByIdClassificacaoOS(Integer parm) throws Exception;

	public void deleteByIdClassificacaoOS(Integer parm) throws Exception;

	public Collection findByAno(Integer parm) throws Exception;

	public void deleteByAno(Integer parm) throws Exception;

	public Collection findByIdContratoAndSituacao(Integer parm, Integer sit) throws Exception;
	
	public Collection findByIdContratoAndPeriodoAndSituacao(Integer idContrato, Date dataInicio, Date dataFim, Integer[] situacao) throws Exception;
	
	public Collection findByIdContratoAndPeriodoAndSituacao(Integer idContrato, Date dataInicio, Date dataFim, Integer[] situacao, Integer idospai) throws Exception;

	public Collection listOSHomologadasENaoAssociadasFatura(Integer idContrato) throws Exception;

	public Collection listOSByIds(Integer idContrato, Integer[] idOSs) throws Exception;

	public Collection listOSAssociadasFatura(Integer idFatura) throws Exception;

	public void duplicarOS(Integer idOS) throws Exception;

	public Collection listOSByIdAtividadePeriodica(Integer parm) throws Exception;

	public Collection listAtividadePeridodicaByIdos(Integer idos) throws Exception;

	public void retornaAtividadeCadastradaByPai(OSDTO osDTO) throws Exception;

	/**
	 * Retornar uma lista de custo atividade por periodo
	 * 
	 * @param relatorio
	 * @return
	 * @throws Exception
	 */
	public Collection<RelatorioOrdemServicoUstDTO> listaCustoAtividadeOrdemServicoPorPeriodo(RelatorioOrdemServicoUstDTO relatorio) throws Exception;

	/**
	 * lista anos das ordem de servico
	 * 
	 * @return
	 * @throws Exception
	 */
	public Collection<RelatorioOrdemServicoUstDTO> listaAnos() throws Exception;

	/**
	 * Retonar uma lista de cussto atividade por perido do contrato;
	 * 
	 * @param relatorio
	 * @return
	 * @throws Exception
	 */
	public Collection<RelatorioAcompanhamentoDTO> listaAcompanhamentoPorPeriodoDoContrato(RelatorioAcompanhamentoDTO relatorio) throws Exception;
	
	public boolean retornaRegistroOsPai(OSDTO osDTO) throws Exception;

	/**
	 * Retonar uma lista com as informa��es do contrato.
	 * @param relatorio
	 * @return
	 * @throws Exception
	 */
//	public Collection<RelatorioAcompanhamentoDTO> listaInformcoesDoContrato(RelatorioAcompanhamentoDTO contrato) throws Exception;
	
	/**
	 * Cancela as OS filhas (RAs) gerados se houver. 
	 * @param osDTO
	 * @throws Exception
	 */
	public void cancelaOsFilhas(OSDTO osDTO) throws Exception;
	
	public Collection retornaSeExisteOSFilha(OSDTO osDTO) throws Exception;
}
