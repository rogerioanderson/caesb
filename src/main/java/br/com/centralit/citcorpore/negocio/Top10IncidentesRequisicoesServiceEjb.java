/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.RelatorioTop10IncidentesRequisicoesDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.Top10IncidentesRequisicoesDTO;
import br.com.centralit.citcorpore.integracao.Top10IncidentesRequisicoesDAO;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilStrings;

@SuppressWarnings("unchecked")
public class Top10IncidentesRequisicoesServiceEjb extends CrudServiceImpl implements Top10IncidentesRequisicoesService {

    private Top10IncidentesRequisicoesDAO dao;
    private Top10IncidentesRequisicoesDAO daoReports;

    @Override
    protected Top10IncidentesRequisicoesDAO getDao() {
        if (dao == null) {
            dao = new Top10IncidentesRequisicoesDAO();
        }
        return dao;
    }
    
    /**
	 * Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 10:00 - ID Citsmart: 176362 -
	 * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
	 * @param databaseAlias
	 * @return dao com o databaseAlias especificado, caso esteja preenchido, sen�o dao com o datasource principal
	 */
	private Top10IncidentesRequisicoesDAO getDao(String databaseAlias) {
		if (UtilStrings.isNotVazio(databaseAlias)) {
			if (daoReports == null) {
				daoReports = new Top10IncidentesRequisicoesDAO(databaseAlias);
			}
			return daoReports;
		}
		return this.getDao();
	}

    @Override
    public ArrayList<Top10IncidentesRequisicoesDTO> listSolicitantesMaisAbriramIncSol(final RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO) {
		// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 10:00 - ID Citsmart: 176362 -
		// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO,
		// citsmart_reports, por exemplo.
		return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).listSolicitantesMaisAbriramIncSol(relatorioTop10IncidentesRequisicoesDTO);
    }

    @Override
    public Collection<SolicitacaoServicoDTO> listDetalheSolicitanteMaisAbriuIncSol(final Integer idSolicitante,
            final RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO) {
		// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 10:15 - ID Citsmart: 176362 -
		// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO,
		// citsmart_reports, por exemplo.
		return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).listDetalheSolicitanteMaisAbriuIncSol(idSolicitante,
				relatorioTop10IncidentesRequisicoesDTO);
    }

    @Override
    public ArrayList<Top10IncidentesRequisicoesDTO> listGruposMaisResolveramIncSol(final RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO) {
    	// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 10:24 - ID Citsmart: 176362 -
		// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO,
		// citsmart_reports, por exemplo.
        return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).listGruposMaisResolveramIncSol(relatorioTop10IncidentesRequisicoesDTO);
    }

    @Override
    public Collection<SolicitacaoServicoDTO> listDetalheGruposMaisResolveramIncSol(final Integer idGrupo,
            final RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO) {
    	// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 10:25 - ID Citsmart: 176362 -
		// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO,
		// citsmart_reports, por exemplo.
        return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).listDetalheGruposMaisResolveramIncSol(idGrupo, relatorioTop10IncidentesRequisicoesDTO);
    }

    @Override
    public ArrayList<Top10IncidentesRequisicoesDTO> listReqIncMaisSolicitados(final RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO) {
    	// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 10:25 - ID Citsmart: 176362 -
		// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO,
		// citsmart_reports, por exemplo.
        return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).listReqIncMaisSolicitados(relatorioTop10IncidentesRequisicoesDTO);
    }

    @Override
    public Collection<SolicitacaoServicoDTO> listDetalheReqIncMaisSolicitados(final Integer idTipoDemanda, final Integer idServico,
            final RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO) {
    	// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 10:25 - ID Citsmart: 176362 -
		// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO,
		// citsmart_reports, por exemplo.
        return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).listDetalheReqIncMaisSolicitados(idTipoDemanda, idServico, relatorioTop10IncidentesRequisicoesDTO);
    }

    @Override
    public ArrayList<Top10IncidentesRequisicoesDTO> listUnidadesMaisAbriramReqInc(final RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO) {
    	// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 10:27 - ID Citsmart: 176362 -
		// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO,
		// citsmart_reports, por exemplo.
        return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).listUnidadesMaisAbriramReqInc(relatorioTop10IncidentesRequisicoesDTO);
    }

    @Override
    public Collection<SolicitacaoServicoDTO> listDetalheUnidadesMaisAbriramReqInc(final Integer id,
            final RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO) {
    	// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 10:27 - ID Citsmart: 176362 -
		// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO,
		// citsmart_reports, por exemplo.
        return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).listDetalheUnidadesMaisAbriramReqInc(id, relatorioTop10IncidentesRequisicoesDTO);
    }

    @Override
    public ArrayList<Top10IncidentesRequisicoesDTO> listLocMaisAbriramReqInc(final RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO) {
    	// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 10:26 - ID Citsmart: 176362 -
		// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO,
		// citsmart_reports, por exemplo.
        return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).listLocMaisAbriramReqInc(relatorioTop10IncidentesRequisicoesDTO);
    }

    @Override
    public Collection<SolicitacaoServicoDTO> listDetalheLocMaisAbriramReqInc(final Integer id, final RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO) {
    	// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 10:26 - ID Citsmart: 176362 -
		// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO,
		// citsmart_reports, por exemplo.
        return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).listDetalheLocMaisAbriramReqInc(id, relatorioTop10IncidentesRequisicoesDTO);
    }

}
