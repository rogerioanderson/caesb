/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import br.com.centralit.citcorpore.bean.PerfilAcessoGrupoDTO;
import br.com.centralit.citcorpore.bean.PerfilAcessoSituacaoOSDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.MenuDao;
import br.com.centralit.citcorpore.integracao.PerfilAcessoSituacaoOSDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

public class PerfilAcessoSituacaoOSServiceEjb extends CrudServiceImpl implements PerfilAcessoSituacaoOSService {

    private PerfilAcessoSituacaoOSDao dao;

    @Override
    protected PerfilAcessoSituacaoOSDao getDao() {
        if (dao == null) {
            dao = new PerfilAcessoSituacaoOSDao();
        }
        return dao;
    }

    @Override
    public Collection getSituacoesOSPermitidasByUsuario(final UsuarioDTO usuario) throws Exception {
        final MenuDao menuDao = new MenuDao();
        final Integer idPerfilAcesso = menuDao.getPerfilAcesso(usuario);
        if (idPerfilAcesso == null) {
            return null;
        }
        final Collection colSituacoesPerfil = this.getDao().findByIdPerfil(idPerfilAcesso);
        if (colSituacoesPerfil == null) {
            return null;
        }
        final Collection<Integer> colFinal = new ArrayList<>();
        for (final Iterator it = colSituacoesPerfil.iterator(); it.hasNext();) {
            final PerfilAcessoSituacaoOSDTO perfilAcessoSituacaoOSDTO = (PerfilAcessoSituacaoOSDTO) it.next();
            if (perfilAcessoSituacaoOSDTO.getDataFim() == null || perfilAcessoSituacaoOSDTO.getDataFim().after(UtilDatas.getDataAtual())) {
                colFinal.add(perfilAcessoSituacaoOSDTO.getSituacaoOs());
            }
        }
        return colFinal;
    }

    @Override
    public Collection getSituacoesOSPermitidasByGrupo(final PerfilAcessoGrupoDTO perfilAcessoGrupoDTO) throws Exception {

        final Integer idPerfilAcesso = perfilAcessoGrupoDTO.getIdPerfilAcessoGrupo();
        if (idPerfilAcesso == null) {
            return null;
        }
        final Collection colSituacoesPerfil = this.getDao().findByIdPerfil(idPerfilAcesso);
        if (colSituacoesPerfil == null) {
            return null;
        }
        final Collection colFinal = new ArrayList();
        for (final Iterator it = colSituacoesPerfil.iterator(); it.hasNext();) {
            final PerfilAcessoSituacaoOSDTO perfilAcessoSituacaoOSDTO = (PerfilAcessoSituacaoOSDTO) it.next();
            if (perfilAcessoSituacaoOSDTO.getDataFim() == null || perfilAcessoSituacaoOSDTO.getDataFim().after(UtilDatas.getDataAtual())) {
                colFinal.add(perfilAcessoSituacaoOSDTO.getSituacaoOs());
            }
        }
        return colFinal;
    }

    @Override
    public Collection findByIdPerfil(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdPerfil(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdPerfil(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdPerfil(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findBySituacaoOs(final Integer parm) throws Exception {
        try {
            return this.getDao().findBySituacaoOs(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteBySituacaoOs(final Integer parm) throws Exception {
        try {
            this.getDao().deleteBySituacaoOs(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
