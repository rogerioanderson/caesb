/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.NotificacaoDTO;
import br.com.centralit.citcorpore.bean.NotificacaoGrupoDTO;
import br.com.centralit.citcorpore.bean.NotificacaoServicoDTO;
import br.com.centralit.citcorpore.bean.NotificacaoUsuarioDTO;
import br.com.centralit.citcorpore.integracao.NotificacaoDao;
import br.com.centralit.citcorpore.integracao.NotificacaoGrupoDao;
import br.com.centralit.citcorpore.integracao.NotificacaoServicoDao;
import br.com.centralit.citcorpore.integracao.NotificacaoUsuarioDao;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

public class NotificacaoServiceEjb extends CrudServiceImpl implements NotificacaoService {

    private NotificacaoDao dao;

    @Override
    protected NotificacaoDao getDao() {
        if (dao == null) {
            dao = new NotificacaoDao();
        }
        return dao;
    }

    public void deletarNotificacao(final IDto model) throws ServiceException, Exception {
        final NotificacaoDTO notificacaoDto = (NotificacaoDTO) model;
        final TransactionControler transactionControler = new TransactionControlerImpl(this.getDao().getAliasDB());

        try {
            this.validaUpdate(model);
            transactionControler.start();
            notificacaoDto.setDataFim(UtilDatas.getDataAtual());
            this.getDao().update(model);
            transactionControler.commit();
            transactionControler.close();
        } catch (final Exception e) {
            this.rollbackTransaction(transactionControler, e);
        }

    }

    @Override
    public boolean consultarNotificacaoAtivos(final NotificacaoDTO obj) throws Exception {
        return this.getDao().consultarNotificacaoAtivos(obj);
    }

    @Override
    public void update(final NotificacaoDTO notificacaoDto, TransactionControler transactionControler) throws Exception {
        final NotificacaoGrupoDao notificacaoGrupoDao = new NotificacaoGrupoDao();
        final NotificacaoUsuarioDao notificacaoUsuarioDao = new NotificacaoUsuarioDao();
        final NotificacaoServicoDao notificacaoServicoDao = new NotificacaoServicoDao();

        if (transactionControler == null) {
            transactionControler = new TransactionControlerImpl(this.getDao().getAliasDB());
            transactionControler.start();
        }
        final NotificacaoGrupoDTO notificacaoGrupoDto = new NotificacaoGrupoDTO();
        final NotificacaoUsuarioDTO notificacaoUsuarioDto = new NotificacaoUsuarioDTO();
        final NotificacaoServicoDTO notificacaoServicoDto = new NotificacaoServicoDTO();

        this.getDao().setTransactionControler(transactionControler);
        notificacaoGrupoDao.setTransactionControler(transactionControler);
        notificacaoUsuarioDao.setTransactionControler(transactionControler);

        this.getDao().update(notificacaoDto);
        /* deletando as notifica��es para usuario */
        notificacaoUsuarioDao.deleteByIdNotificacaoUsuario(notificacaoDto.getIdNotificacao());
        if (notificacaoDto.getListaDeUsuario() != null) {
            if (notificacaoDto.getIdNotificacao() != null && notificacaoDto.getIdNotificacao() != 0) {
                notificacaoUsuarioDao.deleteByIdNotificacaoUsuario(notificacaoDto.getIdNotificacao());
                for (final NotificacaoUsuarioDTO notificacaoUsuario : notificacaoDto.getListaDeUsuario()) {
                    notificacaoUsuarioDto.setIdNotificacao(notificacaoDto.getIdNotificacao());
                    notificacaoUsuarioDto.setIdUsuario(notificacaoUsuario.getIdUsuario());
                    notificacaoUsuarioDao.create(notificacaoUsuarioDto);
                }
            }

        }
        /* deletando as notifica��es para grupo */
        notificacaoGrupoDao.deleteByIdNotificacaoGrupo(notificacaoDto.getIdNotificacao());
        if (notificacaoDto.getListaDeGrupo() != null) {
            if (notificacaoDto.getIdNotificacao() != null && notificacaoDto.getIdNotificacao() != 0) {
                notificacaoGrupoDao.deleteByIdNotificacaoGrupo(notificacaoDto.getIdNotificacao());
                for (final NotificacaoGrupoDTO notificacaoGrupo : notificacaoDto.getListaDeGrupo()) {
                    notificacaoGrupoDto.setIdNotificacao(notificacaoDto.getIdNotificacao());
                    notificacaoGrupoDto.setIdGrupo(notificacaoGrupo.getIdGrupo());
                    notificacaoGrupoDao.create(notificacaoGrupoDto);
                }
            }

        }
        /* deletando as notifica��es para servi�o */
        notificacaoServicoDao.deleteByIdNotificacaoServico(notificacaoDto.getIdNotificacao());
        if (notificacaoDto.getListaDeServico() != null) {
            for (final NotificacaoServicoDTO notificacaoServico : notificacaoDto.getListaDeServico()) {
                if (notificacaoServico.getIdServico() != null && notificacaoServico.getIdServico() != 0) {
                    notificacaoServicoDto.setIdNotificacao(notificacaoDto.getIdNotificacao());
                    notificacaoServicoDto.setIdServico(notificacaoServico.getIdServico());
                    notificacaoServicoDao.create(notificacaoServicoDto);
                }

            }
        }

    }

    @Override
    public NotificacaoDTO create(NotificacaoDTO notificacaoDto, final TransactionControler transactionControler) throws Exception {
        final NotificacaoGrupoDao notificacaoGrupoDao = new NotificacaoGrupoDao();
        final NotificacaoUsuarioDao notificacaoUsuarioDao = new NotificacaoUsuarioDao();
        final NotificacaoServicoDao notificacaoServicoDao = new NotificacaoServicoDao();

        final NotificacaoGrupoDTO notificacaoGrupoDto = new NotificacaoGrupoDTO();
        final NotificacaoUsuarioDTO notificacaoUsuarioDto = new NotificacaoUsuarioDTO();
        final NotificacaoServicoDTO notificacaoServicoDto = new NotificacaoServicoDTO();

        this.getDao().setTransactionControler(transactionControler);
        notificacaoGrupoDao.setTransactionControler(transactionControler);
        notificacaoUsuarioDao.setTransactionControler(transactionControler);
        notificacaoServicoDao.setTransactionControler(transactionControler);

        notificacaoDto.setDataInicio(UtilDatas.getDataAtual());

        notificacaoDto = (NotificacaoDTO) this.getDao().create(notificacaoDto);

        if (notificacaoDto.getListaDeUsuario() != null) {
            for (final NotificacaoUsuarioDTO notificacaoUsuario : notificacaoDto.getListaDeUsuario()) {
                if (notificacaoUsuario.getIdUsuario() != null && notificacaoUsuario.getIdUsuario() != 0) {
                    notificacaoUsuarioDto.setIdNotificacao(notificacaoDto.getIdNotificacao());
                    notificacaoUsuarioDto.setIdUsuario(notificacaoUsuario.getIdUsuario());
                    notificacaoUsuarioDao.create(notificacaoUsuarioDto);
                }
            }
        }

        if (notificacaoDto.getListaDeGrupo() != null) {
            for (final NotificacaoGrupoDTO notificacaoGrupo : notificacaoDto.getListaDeGrupo()) {
                if (notificacaoGrupo.getIdGrupo() != null && notificacaoGrupo.getIdGrupo() != 0) {
                    notificacaoGrupoDto.setIdNotificacao(notificacaoDto.getIdNotificacao());
                    notificacaoGrupoDto.setIdGrupo(notificacaoGrupo.getIdGrupo());
                    notificacaoGrupoDao.create(notificacaoGrupoDto);
                }
            }
        }

        if (notificacaoDto.getListaDeServico() != null) {
            for (final NotificacaoServicoDTO notificacaoServico : notificacaoDto.getListaDeServico()) {
                if (notificacaoServico.getIdServico() != null && notificacaoServico.getIdServico() != 0) {
                    notificacaoServicoDto.setIdNotificacao(notificacaoDto.getIdNotificacao());
                    notificacaoServicoDto.setIdServico(notificacaoServico.getIdServico());
                    notificacaoServicoDao.create(notificacaoServicoDto);
                }
            }
        }

        return notificacaoDto;
    }

    @Override
    public Collection<NotificacaoDTO> consultarNotificacaoAtivosOrigemServico(final Integer idContrato) throws Exception {
        return this.getDao().consultarNotificacaoAtivosOrigemServico(idContrato);
    }

    @Override
    public IDto create(final IDto model) throws ServiceException, br.com.citframework.excecao.LogicException {

        final NotificacaoGrupoDao notificacaoGrupoDao = new NotificacaoGrupoDao();
        final NotificacaoUsuarioDao notificacaoUsuarioDao = new NotificacaoUsuarioDao();
        final NotificacaoServicoDao notificacaoServicoDao = new NotificacaoServicoDao();

        NotificacaoDTO notificacaoDto = (NotificacaoDTO) model;
        final NotificacaoGrupoDTO notificacaoGrupoDto = new NotificacaoGrupoDTO();
        final NotificacaoUsuarioDTO notificacaoUsuarioDto = new NotificacaoUsuarioDTO();
        final NotificacaoServicoDTO notificacaoServicoDTO = new NotificacaoServicoDTO();

        final TransactionControler transactionControler = new TransactionControlerImpl(this.getDao().getAliasDB());

        try {
            this.getDao().setTransactionControler(transactionControler);
            notificacaoGrupoDao.setTransactionControler(transactionControler);
            notificacaoUsuarioDao.setTransactionControler(transactionControler);
            notificacaoServicoDao.setTransactionControler(transactionControler);

            transactionControler.start();
            notificacaoDto.setDataInicio(UtilDatas.getDataAtual());
            notificacaoDto = (NotificacaoDTO) this.getDao().create(notificacaoDto);

            if (notificacaoDto.getListaDeUsuario() != null) {
                for (final NotificacaoUsuarioDTO notificacaoUsuario : notificacaoDto.getListaDeUsuario()) {
                    if (notificacaoUsuario.getIdUsuario() != null && notificacaoUsuario.getIdUsuario() != 0) {
                        notificacaoUsuarioDto.setIdNotificacao(notificacaoDto.getIdNotificacao());
                        notificacaoUsuarioDto.setIdUsuario(notificacaoUsuario.getIdUsuario());
                        notificacaoUsuarioDao.create(notificacaoUsuarioDto);
                    }
                }
            }

            if (notificacaoDto.getListaDeGrupo() != null) {
                for (final NotificacaoGrupoDTO notificacaoGrupo : notificacaoDto.getListaDeGrupo()) {
                    if (notificacaoGrupo.getIdGrupo() != null && notificacaoGrupo.getIdGrupo() != 0) {
                        notificacaoGrupoDto.setIdNotificacao(notificacaoDto.getIdNotificacao());
                        notificacaoGrupoDto.setIdGrupo(notificacaoGrupo.getIdGrupo());
                        notificacaoGrupoDao.create(notificacaoGrupoDto);
                    }
                }
            }

            if (notificacaoDto.getListaDeServico() != null) {
                for (final NotificacaoServicoDTO notificacaoServico : notificacaoDto.getListaDeServico()) {
                    if (notificacaoServico.getIdServico() != null && notificacaoServico.getIdServico() != 0) {
                        notificacaoServicoDTO.setIdNotificacao(notificacaoDto.getIdNotificacao());
                        notificacaoServicoDTO.setIdServico(notificacaoServico.getIdServico());
                        notificacaoServicoDao.create(notificacaoServicoDTO);
                    }
                }
            }

            transactionControler.commit();
            transactionControler.close();

        } catch (final Exception e) {
            e.printStackTrace();
            this.rollbackTransaction(transactionControler, e);
        }

        return notificacaoDto;
    }

    @Override
    public Collection<NotificacaoDTO> listaIdContrato(final Integer idContrato) throws Exception {
        return this.getDao().listaIdContrato(idContrato);
    }

    @Override
    public void updateNotNull(final IDto obj) throws Exception {
        this.getDao().updateNotNull(obj);
    }

}
