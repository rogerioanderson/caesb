/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.com.centralit.citcorpore.bean.MenuDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.citframework.service.CrudService;

@SuppressWarnings("rawtypes")
public interface MenuService extends CrudService {
	public Collection listarMenus() throws Exception;

	public Collection<MenuDTO> listarSubMenus(MenuDTO submenu) throws Exception;

	public Collection<MenuDTO> listarMenusPorPerfil(UsuarioDTO usuario, Integer idMenuPai) throws Exception;
	
	public Collection<MenuDTO> listarMenusPorPerfil(UsuarioDTO usuario, Integer idMenuPai, boolean acessoRapido) throws Exception;

	public void criaMenus(Integer idUsuario) throws Exception;

	public Collection<MenuDTO> listaMenuByUsr(UsuarioDTO usuario) throws Exception;
	
	public Collection<MenuDTO> listarMenusPais() throws Exception;
	
	public Collection<MenuDTO> listarMenusFilhos(Integer idMenuPai) throws Exception;

	public void updateNotNull(Collection<MenuDTO> menus);

	/**
	 * M�todo para verificar se caso exista um menu com o mesmo nome
	 * 
	 * @author rodrigo.oliveira
	 * @param menuDTO
	 * @return Se caso exista menu com o mesmo nome retorna true
	 * @throws Exception
	 */
	public boolean verificaSeExisteMenu(MenuDTO menuDTO) throws Exception;

	/**
	 * buscar idMenu pelo link
	 * 
	 * @param link
	 * @return
	 * @throws Exception
	 */
	public Integer buscarIdMenu(String link) throws Exception;

	public void gerarCarga(File file) throws Exception;
	
	public void deletaMenusSemReferencia() throws Exception;
	
	/**
	 * M�todo para obter um mapa com todos os menus que o usu�rio pode acessar
	 * @author thyen.chang
	 * @since 16/01/2015 - OPERA��O USAIN BOLT
	 * @param usuario
	 * @return
	 * @throws Exception
	 */
	public Map<Integer, List<MenuDTO> > listaMenuPorUsuario(UsuarioDTO usuario) throws Exception;
}
