/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.ProblemaMudancaDTO;
import br.com.centralit.citcorpore.integracao.ProblemaMudancaDAO;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

@SuppressWarnings("rawtypes")
public class ProblemaMudancaServiceEjb extends CrudServiceImpl implements ProblemaMudancaService {

    private ProblemaMudancaDAO dao;

    @Override
    protected ProblemaMudancaDAO getDao() {
        if (dao == null) {
            dao = new ProblemaMudancaDAO();
        }
        return dao;
    }

    @Override
    public Collection findByIdProblemaMudanca(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdProblemaMudanca(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdProblemaMudanca(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdProblemaMudanca(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

	public Collection findByIdProblema(Integer idProblema) throws Exception {
		ProblemaMudancaDAO dao = new ProblemaMudancaDAO();
        try {
			return dao.findByIdProblema(idProblema);
		} catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdProblema(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdProblema(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdRequisicaoMudanca(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdRequisicaoMudanca(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdRequisicaoMudanca(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdRequisicaoMudanca(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public ProblemaMudancaDTO restoreByIdProblema(final Integer idProblema) throws Exception {
        try {
            return this.getDao().restoreByIdProblema(idProblema);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
