/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.Iterator;

import br.com.centralit.citcorpore.bean.TarefaLinhaBaseProjetoDTO;
import br.com.centralit.citcorpore.integracao.TarefaLinhaBaseProjetoDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudServiceImpl;

public class TarefaLinhaBaseProjetoServiceEjb extends CrudServiceImpl implements TarefaLinhaBaseProjetoService {

    private TarefaLinhaBaseProjetoDao dao;

    @Override
    protected TarefaLinhaBaseProjetoDao getDao() {
        if (dao == null) {
            dao = new TarefaLinhaBaseProjetoDao();
        }
        return dao;
    }

    @Override
    public Collection findByIdLinhaBaseProjeto(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdLinhaBaseProjeto(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdLinhaBaseProjeto(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdLinhaBaseProjeto(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findCarteiraByIdEmpregado(final Integer idEmpregado) throws Exception {
        try {
            return this.getDao().findCarteiraByIdEmpregado(idEmpregado);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdTarefaLinhaBaseProjetoMigr(final Integer idTarefaLinhaBaseProjetoMigr) throws Exception {
        try {
            return this.getDao().findByIdTarefaLinhaBaseProjetoMigr(idTarefaLinhaBaseProjetoMigr);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdTarefaLinhaBaseProjetoPai(final Integer idTarefaLinhaBaseProjetoPai) throws Exception {
        try {
            return this.getDao().findByIdTarefaLinhaBaseProjetoPai(idTarefaLinhaBaseProjetoPai);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    public void atualizaInfoProporcionais(final TransactionControler tc, final Integer idTarefaBase) throws Exception {
        // --Faz o calculo do proporcional das tarefas acima.
        final TarefaLinhaBaseProjetoDao tarefaLinhaBaseProjetoDao = new TarefaLinhaBaseProjetoDao();
        tarefaLinhaBaseProjetoDao.setTransactionControler(tc);
        //
        TarefaLinhaBaseProjetoDTO tarefaLinhaBaseProjetoDTO = new TarefaLinhaBaseProjetoDTO();
        tarefaLinhaBaseProjetoDTO.setIdTarefaLinhaBaseProjeto(idTarefaBase);
        tarefaLinhaBaseProjetoDTO = (TarefaLinhaBaseProjetoDTO) tarefaLinhaBaseProjetoDao.restore(tarefaLinhaBaseProjetoDTO);
        if (tarefaLinhaBaseProjetoDTO != null && tarefaLinhaBaseProjetoDTO.getIdTarefaLinhaBaseProjetoPai() != null) {
            final Collection colTarefas = tarefaLinhaBaseProjetoDao.calculaValoresTarefasFilhas(tarefaLinhaBaseProjetoDTO.getIdTarefaLinhaBaseProjetoPai());
            if (colTarefas != null) {
                for (final Iterator it = colTarefas.iterator(); it.hasNext();) {
                    final TarefaLinhaBaseProjetoDTO tarefaLinhaBaseProjetoAux = (TarefaLinhaBaseProjetoDTO) it.next();
                    final TarefaLinhaBaseProjetoDTO tarefaLinhaBaseProjetoAtu = new TarefaLinhaBaseProjetoDTO();
                    tarefaLinhaBaseProjetoAtu.setIdTarefaLinhaBaseProjeto(tarefaLinhaBaseProjetoDTO.getIdTarefaLinhaBaseProjetoPai());
                    tarefaLinhaBaseProjetoAtu.setCusto(tarefaLinhaBaseProjetoAux.getCusto());
                    tarefaLinhaBaseProjetoAtu.setCustoPerfil(tarefaLinhaBaseProjetoAux.getCustoPerfil());
                    tarefaLinhaBaseProjetoAtu.setTempoTotAlocMinutos(tarefaLinhaBaseProjetoAux.getTempoTotAlocMinutos());
                    tarefaLinhaBaseProjetoAtu.setProgresso(tarefaLinhaBaseProjetoAux.getProgresso());
                    if (tarefaLinhaBaseProjetoAtu.getProgresso() != null) {
                        tarefaLinhaBaseProjetoAtu.setProgresso(tarefaLinhaBaseProjetoAtu.getProgresso().doubleValue() * 100);
                    } else {
                        tarefaLinhaBaseProjetoAtu.setProgresso(new Double(0));
                    }
                    if (tarefaLinhaBaseProjetoAtu.getProgresso().doubleValue() > 100) {
                        tarefaLinhaBaseProjetoAtu.setProgresso(new Double(100));
                    }
                    if (tarefaLinhaBaseProjetoAtu.getProgresso().doubleValue() >= 100) {
                        tarefaLinhaBaseProjetoAtu.setSituacao(TarefaLinhaBaseProjetoDTO.PRONTO);
                    }
                    tarefaLinhaBaseProjetoDao.updateNotNull(tarefaLinhaBaseProjetoAtu);
                }
            }
            if (tarefaLinhaBaseProjetoDTO.getIdTarefaLinhaBaseProjetoPai() != null) {
                this.atualizaInfoProporcionais(tc, tarefaLinhaBaseProjetoDTO.getIdTarefaLinhaBaseProjetoPai());
            }
        }
    }

}
