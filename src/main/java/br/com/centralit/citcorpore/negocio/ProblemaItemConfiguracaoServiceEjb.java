/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.ProblemaItemConfiguracaoDTO;
import br.com.centralit.citcorpore.integracao.ProblemaItemConfiguracaoDAO;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

@SuppressWarnings("rawtypes")
public class ProblemaItemConfiguracaoServiceEjb extends CrudServiceImpl implements ProblemaItemConfiguracaoService {

    private ProblemaItemConfiguracaoDAO dao;

    @Override
    protected ProblemaItemConfiguracaoDAO getDao() {
        if (dao == null) {
            dao = new ProblemaItemConfiguracaoDAO();
        }
        return dao;
    }

    @Override
    public Collection findByIdProblemaItemConfiguracao(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdProblemaItemConfiguracao(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdProblemaItemConfiguracao(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdProblemaItemConfiguracao(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

	public Collection findByIdProblema(Integer idProblema) throws Exception {
		ProblemaItemConfiguracaoDAO dao = new ProblemaItemConfiguracaoDAO();
        try {
			return dao.findByIdProblema(idProblema);
		} catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdProblema(final Integer parm) throws Exception {
        try {
            dao.deleteByIdProblema(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdItemConfiguracao(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdItemConfiguracao(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdItemConfiguracao(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdItemConfiguracao(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public ProblemaItemConfiguracaoDTO restoreByIdProblema(final Integer idProblema) throws Exception {
        try {
            return this.getDao().restoreByIdProblema(idProblema);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
