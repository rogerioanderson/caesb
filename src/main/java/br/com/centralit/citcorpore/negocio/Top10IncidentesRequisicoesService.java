/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.RelatorioTop10IncidentesRequisicoesDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.Top10IncidentesRequisicoesDTO;
import br.com.citframework.service.CrudService;

public interface Top10IncidentesRequisicoesService extends CrudService {

	ArrayList<Top10IncidentesRequisicoesDTO> listSolicitantesMaisAbriramIncSol(RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO);

	Collection<SolicitacaoServicoDTO> listDetalheSolicitanteMaisAbriuIncSol(Integer idSolicitante, RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO);

	ArrayList<Top10IncidentesRequisicoesDTO> listGruposMaisResolveramIncSol(RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO);
	
	Collection<SolicitacaoServicoDTO> listDetalheGruposMaisResolveramIncSol(Integer idGrupo, RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO);

	ArrayList<Top10IncidentesRequisicoesDTO> listReqIncMaisSolicitados(RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO);
	
	Collection<SolicitacaoServicoDTO> listDetalheReqIncMaisSolicitados(Integer idTipoDemanda, Integer idServico, RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO);

	ArrayList<Top10IncidentesRequisicoesDTO> listUnidadesMaisAbriramReqInc(RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO);

	Collection<SolicitacaoServicoDTO> listDetalheUnidadesMaisAbriramReqInc(Integer id, RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO);

	ArrayList<Top10IncidentesRequisicoesDTO> listLocMaisAbriramReqInc(RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO);

	Collection<SolicitacaoServicoDTO> listDetalheLocMaisAbriramReqInc(Integer id, RelatorioTop10IncidentesRequisicoesDTO relatorioTop10IncidentesRequisicoesDTO);

}
