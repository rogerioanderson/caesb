/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import br.com.centralit.citcorpore.bean.GerenciamentoRotasDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.bean.result.GerenciamentoRotasResultDTO;
import br.com.centralit.citcorpore.util.Enumerados.TipoSolicitacaoServico;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.core.Page;
import br.com.citframework.integracao.core.Pageable;
import br.com.citframework.service.CrudService;

/**
 * Servi�os para as consultas de {@link SolicitacaoServicoDTO} realizadas pelo mobile V2
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 09/10/2014
 *
 */
public interface SolicitacaoServicoServiceForMobileV2 extends CrudService {

    /**
     * Lista {@link SolicitacaoServicoDTO} de acordo com a mais recente presente no APP
     *
     * @param newestNumber
     *            n�mero de solicita��o mais recente presente no mobile
     * @param usuario
     *            usu�rio que est� solicitando a listagem
     * @param tiposSolicitacao
     *            tipos de solicita��o a serem consideradas na listagem
     * @param aprovacao
     *            identificador de se a solicita��o est� ou n�o em aprova��o
     * @return {@link Page} contendo os registros resultantes da consulta
     * @throws ServiceException
     */
    Page<SolicitacaoServicoDTO> listNewest(final Integer newestNumber, final UsuarioDTO usuario, final TipoSolicitacaoServico[] tiposSolicitacao, final String aprovacao, final int ordem)
            throws ServiceException;

    /**
     * Lista {@link SolicitacaoServicoDTO} de acordo com a mais antiga presente no APP
     *
     * @param oldestNumber
     *            n�mero de solicita��o mais antiga presente no mobile
     * @param usuario
     *            usu�rio que est� solicitando a listagem
     * @param tiposSolicitacao
     *            tipos de solicita��o a serem consideradas na listagem
     * @param aprovacao
     *            identificador de se a solicita��o est� ou n�o em aprova��o
     * @return {@link Page} contendo os registros resultantes da consulta
     * @throws ServiceException
     */
    Page<SolicitacaoServicoDTO> listOldest(final Integer oldestNumber, final UsuarioDTO usuario, final TipoSolicitacaoServico[] tiposSolicitacao, final String aprovacao, final int ordem)
            throws ServiceException;

    /**
     * Lista {@link SolicitacaoServicoDTO} de acordo com o posicionamento geogr�fico do usu�rio
     *
     * @param latitude
     *            latitude do posicionamento do usu�rio
     * @param longitude
     *            longitude do posicionamento do usu�rio
     * @param usuario
     *            usu�rio que est� solicitando a listagem
     * @param tiposSolicitacao
     *            tipos de solicita��o a serem consideradas na listagem
     * @param aprovacao
     *            identificador de se a solicita��o est� ou n�o em aprova��o
     * @param pageable
     *            informa��o sobre pagina��o
     * @return {@link Page} contendo os registros resultantes da consulta
     * @throws ServiceException
     */
    Page<SolicitacaoServicoDTO> listByCoordinates(final Double latitude, final Double longitude, final UsuarioDTO usuario, final TipoSolicitacaoServico[] tiposSolicitacao,
            final String aprovacao, final Pageable pageable) throws ServiceException;

    /**
     * Lista solicita��es de servi�os para roteiriza��o
     *
     * @param filter
     *            informa��es para filtro
     * @param pageable
     *            informa��es para pagina��o
     * @return {@link Page} contendo os registros resultantes da consulta
     * @throws ServiceException
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @date 17/11/2014
     */
    Page<GerenciamentoRotasResultDTO> listSolicitacoesParaRoteirizacao(final GerenciamentoRotasDTO filter, final Pageable pageable) throws ServiceException;

    /**
     * Busca lista de {@link SolicitacaoServicoDTO} de acordo com o texto passado como par�metro. Campos da busca:
     * 
     * - Nome do Contrato;
     * - N�mero da solicita��o;
     * - Nome da Unidade;
     * - Grupo executor.
     *
     * @param newestNumber
     *            n�mero de solicita��o mais recente presente no mobile
     * @param usuario
     *            usu�rio que est� solicitando a listagem
     * @param tiposSolicitacao
     *            tipos de solicita��o a serem consideradas na listagem
     * @param aprovacao
     *            identificador de se a solicita��o est� ou n�o em aprova��o
     * @param textoBusca
     * 			  texto enviado utilizado para a busca dos campos
     * @return {@link Page} contendo os registros resultantes da consulta
     * @throws ServiceException
     * @author thyen.chang
     * @date 07/01/2016
     */
    Page<SolicitacaoServicoDTO> buscaSolicitacao(final Integer newestNumber, final UsuarioDTO usuario, final TipoSolicitacaoServico[] tiposSolicitacao, final String aprovacao, final String textoBusca, final String listaSolicitacoes)
            throws ServiceException;
    
    Page<SolicitacaoServicoDTO> listNotificationByNumberAndUser(final Integer number, final UsuarioDTO user) throws ServiceException;

}
