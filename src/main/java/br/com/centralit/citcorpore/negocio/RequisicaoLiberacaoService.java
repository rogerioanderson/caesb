/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import br.com.centralit.bpm.dto.FluxoDTO;
import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;
import br.com.centralit.citcorpore.bean.PesquisaRequisicaoLiberacaoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoLiberacaoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoLiberacaoItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoQuestionarioDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudService;

public interface RequisicaoLiberacaoService extends CrudService {

    Collection findByIdSolicitante(Integer parm) throws Exception;

    List<RequisicaoLiberacaoDTO> listLiberacoes() throws Exception;

    RequisicaoLiberacaoDTO restoreAll(Integer idRequisicaoLiberacao) throws Exception;

    void reativa(UsuarioDTO usuarioDto, RequisicaoLiberacaoDTO requisicaoLiberacaoDTO) throws Exception;

    void gravaInformacoesGED(RequisicaoLiberacaoDTO requisicaoLiberacaoDTO, TransactionControler tc) throws Exception;

    List<RequisicaoLiberacaoDTO> findByConhecimento(BaseConhecimentoDTO baseConhecimentoDto) throws Exception;

    void updateSimples(IDto model);

    /**
     * suspende a requisi��o mudan�a
     *
     * @param usuarioDto
     * @param solicitacaoServicoDto
     * @throws Exception
     * @author maycon.fernandes
     */
    void suspende(UsuarioDTO usuarioDto, RequisicaoLiberacaoDTO requisicaoLiberacaoDTO) throws Exception;

    List<RequisicaoLiberacaoItemConfiguracaoDTO> listItensRelacionadosRequisicaoLiberacao(
            RequisicaoLiberacaoDTO requisicaoLiberacaoDTO) throws ServiceException, Exception;

    /**
     * @param problemaDto
     * @return Template Liberacao
     * @throws Exception
     */
    String getUrlInformacoesComplementares(RequisicaoLiberacaoDTO requisicaoLiberacaoDTO) throws Exception;

    /**
     * @param problemaDto
     * @return Template Liberacao
     * @throws Exception
     */
    String getUrlInformacoesQuestionario(RequisicaoLiberacaoDTO requisicaoLiberacaoDTO) throws Exception;

    /**
     * @param RequisicaoQuestionarioDTO
     * @throws Exception
     */
    void atualizaInformacoesQuestionario(RequisicaoQuestionarioDTO requisicaoQuestionarioDTO) throws Exception;

    /**
     * O metodo atualiza somente os campos setados os campos anteriores continuar� intacto.
     *
     * @param RequisicaoLiberacaoDTO
     * @throws Exception
     */
    void updateLiberacaoAprovada(IDto obj) throws Exception;

    Collection<RequisicaoLiberacaoDTO> listaRequisicaoLiberacaoPorCriterios(
            PesquisaRequisicaoLiberacaoDTO pesquisaRequisicaoLiberacaoDto) throws Exception;

    FluxoDTO recuperaFluxo(RequisicaoLiberacaoDTO requisicaoLiberacaoDto) throws Exception;

    void reabre(UsuarioDTO usuarioDto, RequisicaoLiberacaoDTO requisicaoLiberacaoDto) throws Exception;

    /**
     * Retorna uma lista de Libera�oes que estejam relacionada a um determinado item de cofigura��o.
     *
     * @param Integer
     * @return List<RequisicaoLiberacaoDTO>
     * @throws Exception
     */
    List<RequisicaoLiberacaoDTO> listLiberacaoByItemConfiugracao(Integer idItemConfiguracao) throws Exception;

    Timestamp MontardataHoraAgendamentoInicial(RequisicaoLiberacaoDTO requisicaoLiberacaoDto);

    Timestamp MontardataHoraAgendamentoFinal(RequisicaoLiberacaoDTO requisicaoLiberacaoDto);

    void calculaTempoAtraso(RequisicaoLiberacaoDTO requisicaoLiberacaoDto) throws Exception;

    Boolean seHoraInicialMenorQAtual(RequisicaoLiberacaoDTO requisicaoLiberacaoDto);

    Boolean seHoraFinalMenorQAtual(RequisicaoLiberacaoDTO requisicaoLiberacaoDto);

    Boolean seHoraFinalMenorQHoraInicial(RequisicaoLiberacaoDTO requisicaoLiberacaoDto);

    Boolean verificaPermissaoGrupoCancelar(Integer idTipoLiberacao, Integer idGrupo) throws ServiceException, Exception;

}
