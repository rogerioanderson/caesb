/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.TabFederacaoDadosDTO;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudService;
public interface TabFederacaoDadosService extends CrudService {
	public Collection findByNomeTabela(String parm) throws Exception;
	public void deleteByNomeTabela(String parm) throws Exception;
	public Collection findByChaveFinal(String parm) throws Exception;
	public void deleteByChaveFinal(String parm) throws Exception;
	public Collection findByChaveOriginal(String parm) throws Exception;
	public void deleteByChaveOriginal(String parm) throws Exception;
	public Collection findByOrigem(String parm) throws Exception;
	public void deleteByOrigem(String parm) throws Exception;
	
	public TabFederacaoDadosDTO create(TabFederacaoDadosDTO federacaoDados, TransactionControler tc) throws Exception;
	public void update(TabFederacaoDadosDTO federacaoDados, TransactionControler tc) throws Exception;
	public TabFederacaoDadosDTO createOrUpdate(TabFederacaoDadosDTO federacaoDados, TransactionControler tc) throws Exception;
	public Collection find(String origem, String nomeTab, String chaveOrig, String chaveFinal) throws Exception;
	public Collection findByChaveOriginal(String origem, String tabela, String chaveOriginal) throws Exception;
}
