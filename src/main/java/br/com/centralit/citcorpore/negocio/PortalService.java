/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.PedidoPortalDTO;
import br.com.centralit.citcorpore.bean.PortalDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoQuestionarioDTO;
import br.com.centralit.citcorpore.bean.UploadDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

public interface PortalService extends CrudService {

	public Collection<PortalDTO> findByCondition(Integer i) throws ServiceException, Exception;
	public Collection<PortalDTO> findByCondition(Integer idUsuario, Integer idItem) throws ServiceException, Exception;
	public Collection<PortalDTO> listByUsuario(Integer idUsuario) throws Exception;
	public PedidoPortalDTO criarPedidoSolicitacao(PedidoPortalDTO pedidoPortalDTO, UsuarioDTO usuarioDTO,  Collection<SolicitacaoServicoQuestionarioDTO> colecaoRespQuestionario, Collection<UploadDTO> arquivosUpados) throws ServiceException, Exception;
	public void relacionaImpactoUrgencia(SolicitacaoServicoDTO solicitacaoServicoDto) throws Exception;
	
	/**
	 * @return
	 */
	public boolean existeQuestionario(final Integer idServico) throws ServiceException, Exception;
	
	/**
	 * @param idServico
	 * @return
	 */
	public boolean existeQuestionarioServico(final Integer idServico) throws ServiceException, Exception;
	
	/**
	 * @param idServicoCatalogo
	 * @return
	 * @throws ServiceException 
	 */
	public Integer obterIdQuestionarioServico(final Integer idServicoCatalogo) throws ServiceException;
	
}
