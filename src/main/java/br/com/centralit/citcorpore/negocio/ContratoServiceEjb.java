/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.ClienteDTO;
import br.com.centralit.citcorpore.bean.ComplexidadeDTO;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.ContratosGruposDTO;
import br.com.centralit.citcorpore.bean.FornecedorDTO;
import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.ClienteDao;
import br.com.centralit.citcorpore.integracao.ContratoDao;
import br.com.centralit.citcorpore.integracao.FornecedorDao;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;
import br.com.citframework.util.UtilStrings;

@SuppressWarnings({"rawtypes", "unchecked"})
public class ContratoServiceEjb extends CrudServiceImpl implements ContratoService {

    private ContratoDao dao;

    @Override
    protected ContratoDao getDao() {
        if (dao == null) {
            dao = new ContratoDao();
        }
        return dao;
    }

    @Override
    public Collection findByIdCliente(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdCliente(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdFornecedor(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdFornecedor(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdCliente(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdCliente(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdContrato(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdContrato(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<ComplexidadeDTO> listaComplexidadePorContrato(final Integer idServicoContrato) throws Exception {
        return this.getDao().listaComplexidadePorContrato(idServicoContrato);
    }

    @Override
    public Collection listByIdAcordoNivelServicoAndTipo(final Integer idAcordoNivelServicoParm, final String tipoParm) throws Exception {
        return this.getDao().listByIdAcordoNivelServicoAndTipo(idAcordoNivelServicoParm, tipoParm);
    }

    @Override
    public Collection<ContratoDTO> listAtivos() throws Exception {
        return this.getDao().listAtivos();
    }

    @Override
    public Collection findByIdGrupo(final Integer idGrupo) throws Exception {
        return this.getDao().findByIdGrupo(idGrupo);
    }

    @Override
    public Collection<ContratoDTO> findAtivosByGrupos(final Collection<GrupoDTO> listGrupoDto) throws Exception {
        return this.getDao().findAtivosByGrupos(listGrupoDto);
    }

    @Override
    public Collection<ContratoDTO> findAtivosByIdEmpregado(final Integer idEmpregado) throws ServiceException, Exception {
		return this.getDao().findAtivosByIdEmpregado(idEmpregado);
	}

    @Override
    public String verificaIdCliente(final HashMap mapFields) throws Exception {
        final ClienteDao clienteDao = new ClienteDao();
        List<ClienteDTO> listaCliente = null;
        String id = mapFields.get("IDCLIENTE").toString().trim();
        if (id == null || id.equals("")) {
            id = "0";
        }
        if (UtilStrings.soContemNumeros(id)) {
            Integer idCliente;
            idCliente = Integer.parseInt(id);
            listaCliente = clienteDao.findByIdCliente(idCliente);
        } else {
            listaCliente = clienteDao.findByRazaoSocial(id);
        }
        if (listaCliente != null && listaCliente.size() > 0) {
            return String.valueOf(listaCliente.get(0).getIdCliente());
        } else {
            return "0";
        }
    }

    @Override
    public String verificaIdFornecedor(final HashMap mapFields) throws Exception {
        final FornecedorDao fornecedorDao = new FornecedorDao();
        List<FornecedorDTO> listaFornecedor = null;
        String id = mapFields.get("IDFORNECEDOR").toString().trim();
        if (id == null || id.equals("")) {
            id = "0";
        }
        if (UtilStrings.soContemNumeros(id)) {
            final Integer idFornecedor = Integer.parseInt(id);
            listaFornecedor = fornecedorDao.findByIdFornecedor(idFornecedor);
        } else {
            listaFornecedor = fornecedorDao.findByRazaoSocial(id);
        }
        if (listaFornecedor != null && listaFornecedor.size() > 0) {
            return String.valueOf(listaFornecedor.get(0).getIdFornecedor());
        } else {
            return "0";
        }
    }

    @Override
    public Collection<ContratoDTO> listAtivosWithNomeRazaoSocialCliente() throws ServiceException, Exception {
        return this.getDao().listAtivosWithNomeRazaoSocialCliente();
    }

    /**
     * desenvolvedor: rcs(Rafael C�sar Soyer) data: 02/02/2015
     * 
     * @author rafael.soyer
     * @param idContrato
     * @param request
     * @return
     * @throws Exception
     */
    public String montaNomeDoContratoParaGridDePesquisa(Integer idContrato, HttpServletRequest request) throws Exception {

        ClienteService clienteService = (ClienteService) ServiceLocator
                .getInstance().getService(ClienteService.class, null);
        FornecedorService fornecedorService = (FornecedorService) ServiceLocator
                .getInstance().getService(FornecedorService.class, null);
        Collection contratoDaRequisicaoMudanca = this.findByIdContrato(idContrato);
        ContratoDTO contratoDaRequisicaoMudancaDTO = (ContratoDTO) contratoDaRequisicaoMudanca
                .iterator().next();
        String nomeDoContrato = "";

        String nomeCliente = "";
        String nomeForn = "";
        ClienteDTO clienteDto = new ClienteDTO();
        clienteDto.setIdCliente(contratoDaRequisicaoMudancaDTO.getIdCliente());
        clienteDto = (ClienteDTO) clienteService.restore(clienteDto);

        if (clienteDto != null) {
            nomeCliente = clienteDto.getNomeRazaoSocial();
}
        FornecedorDTO fornecedorDto = new FornecedorDTO();
        fornecedorDto.setIdFornecedor(contratoDaRequisicaoMudancaDTO
                .getIdFornecedor());
        fornecedorDto = (FornecedorDTO) fornecedorService
                .restore(fornecedorDto);

        if (fornecedorDto != null) {
            nomeForn = fornecedorDto.getRazaoSocial();
        }

        if (contratoDaRequisicaoMudancaDTO.getSituacao().equalsIgnoreCase("A")) {
            nomeDoContrato = ""
                    + contratoDaRequisicaoMudancaDTO.getNumero()
                    + " de "
                    + UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT,
                            contratoDaRequisicaoMudancaDTO.getDataContrato(),
                            WebUtil.getLanguage(request)) + " (" + nomeCliente
                    + " - " + nomeForn + ")";
        }

        return nomeDoContrato;
    }
    
	private boolean isContratoInList(Integer idContrato, Collection colContratosColab) {
		if (colContratosColab != null) {
			for (Iterator it = colContratosColab.iterator(); it.hasNext();) {
				ContratosGruposDTO contratosGruposDTO = (ContratosGruposDTO) it.next();
				if (contratosGruposDTO.getIdContrato().intValue() == idContrato.intValue()) {
					return true;
				}
			}
		}
		return false;
	}
	
	public void carregarContratos(String editar, UsuarioDTO usuario, DocumentHTML document, HttpServletRequest request) throws Exception {
	        HTMLSelect selectContrato = document.getSelectById("idContrato");
		//Carrega todos os contratos permitidos, somente quando a visualiza��o permite editar
		if ((editar == null) || (editar.equalsIgnoreCase("") || (editar.equalsIgnoreCase("S")))) {
			ContratosGruposService contratosGruposService = (ContratosGruposService) ServiceLocator.getInstance().getService(ContratosGruposService.class, null);
			
			ClienteService clienteService = (ClienteService) ServiceLocator.getInstance().getService(ClienteService.class, null);
			FornecedorService fornecedorService = (FornecedorService) ServiceLocator.getInstance().getService(FornecedorService.class, null);

			String COLABORADORES_VINC_CONTRATOS = ParametroUtil.getValorParametroCitSmartHashMap(br.com.centralit.citcorpore.util.Enumerados.ParametroSistema.COLABORADORES_VINC_CONTRATOS, "N");

			if (COLABORADORES_VINC_CONTRATOS == null) {
				COLABORADORES_VINC_CONTRATOS = "N";
			}

			Collection colContratosColab = null;

			if (COLABORADORES_VINC_CONTRATOS.equalsIgnoreCase("S")) {
				colContratosColab = contratosGruposService.findByIdEmpregado(usuario.getIdEmpregado());
			}

			ArrayList<ContratoDTO> colContratos = (ArrayList<ContratoDTO>) list();

			selectContrato.removeAllOptions();

			if (colContratos != null) {

				//Requisito: Quando existir apenas um contrato, ele deve vir selecionado.
				if (colContratos.size() > 1) {
					selectContrato.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
				}

				String nomeCliente = "";
				String nomeForn = "";
				ClienteDTO clienteDto = new ClienteDTO();
				FornecedorDTO fornecedorDto = new FornecedorDTO();
				StringBuilder nomeContrato = new StringBuilder();
				
				for (ContratoDTO contratoDto : colContratos) {
					if (contratoDto.getDeleted() == null || !contratoDto.getDeleted().equalsIgnoreCase("y")) {

						// Se parametro de colaboradores por contrato ativo, entao filtra.
						if (COLABORADORES_VINC_CONTRATOS.equalsIgnoreCase("S")) {

							if (colContratosColab == null) {
								continue;
							}
							if (!isContratoInList(contratoDto.getIdContrato(), colContratosColab)) {
								continue;
							}
						}

						clienteDto.setIdCliente(contratoDto.getIdCliente());

						clienteDto = (ClienteDTO) clienteService.restore(clienteDto);

						if (clienteDto != null) {
							nomeCliente = clienteDto.getNomeRazaoSocial();
						}

						fornecedorDto.setIdFornecedor(contratoDto.getIdFornecedor());

						fornecedorDto = (FornecedorDTO) fornecedorService.restore(fornecedorDto);

						if (fornecedorDto != null) {
							nomeForn = fornecedorDto.getRazaoSocial();
						}

						if (contratoDto.getSituacao().equalsIgnoreCase("A")) {
							nomeContrato.delete(0, nomeContrato.length());
							nomeContrato.append(contratoDto.getNumero());
							nomeContrato.append(" ");
							nomeContrato.append(UtilI18N.internacionaliza(request, "citcorpore.comum.deMin"));
							nomeContrato.append(" ");
							nomeContrato.append(UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT, contratoDto.getDataContrato(), WebUtil.getLanguage(request)));
							nomeContrato.append(" (");
							nomeContrato.append(nomeCliente);
							nomeContrato.append(" - ");
							nomeContrato.append(nomeForn);
							nomeContrato.append(")");
							selectContrato.addOption(contratoDto.getIdContrato().toString(), nomeContrato.toString());
						}
					}
				}
			}
		}
	}
	
    public void restauraContrato(Integer idContrato, UsuarioDTO usuario, DocumentHTML document, HttpServletRequest request) throws Exception {
        HTMLSelect selectContrato = document.getSelectById("idContrato");
        // Carrega apenas o contrato que ser� visualizado quando a tela est� em
        // modo somente leitura
        if ((idContrato != null) && (idContrato.intValue() > 0)) {
            ContratoService contratoService = (ContratoService) ServiceLocator
                    .getInstance().getService(ContratoService.class, null);
            ClienteService clienteService = (ClienteService) ServiceLocator
                    .getInstance().getService(ClienteService.class, null);
            FornecedorService fornecedorService = (FornecedorService) ServiceLocator
                    .getInstance().getService(FornecedorService.class, null);

            selectContrato.removeAllOptions();

            ContratoDTO contratoDto = new ContratoDTO();
            contratoDto.setIdContrato(idContrato);
            contratoDto = (ContratoDTO) contratoService.restore(contratoDto);

            if (contratoDto != null) {
                String nomeCliente = "";

                String nomeForn = "";

                ClienteDTO clienteDto = new ClienteDTO();

                clienteDto.setIdCliente(contratoDto.getIdCliente());

                clienteDto = (ClienteDTO) clienteService.restore(clienteDto);

                if (clienteDto != null) {
                    nomeCliente = clienteDto.getNomeRazaoSocial();
                }

                FornecedorDTO fornecedorDto = new FornecedorDTO();

                fornecedorDto.setIdFornecedor(contratoDto.getIdFornecedor());

                fornecedorDto = (FornecedorDTO) fornecedorService
                        .restore(fornecedorDto);

                if (fornecedorDto != null) {
                    nomeForn = fornecedorDto.getRazaoSocial();
                }
                StringBuilder nomeContrato = new StringBuilder();

                nomeContrato.delete(0, nomeContrato.length());
                nomeContrato.append(contratoDto.getNumero());
                nomeContrato.append(UtilI18N.internacionaliza(request,
                        "citcorpore.comum.deMin"));
                nomeContrato.append(UtilDatas.convertDateToString(
                        TipoDate.DATE_DEFAULT, contratoDto.getDataContrato(),
                        WebUtil.getLanguage(request)));
                nomeContrato.append(" (");
                nomeContrato.append(nomeCliente);
                nomeContrato.append(" - ");
                nomeContrato.append(nomeForn);
                nomeContrato.append(")");

                selectContrato.addOption(
                        contratoDto.getIdContrato().toString(), nomeContrato
                                .toString());

            } else {
                selectContrato.addOption("", UtilI18N.internacionaliza(request,
                        "citcorpore.comum.selecione"));
            }
        }
    }


    @Override
   	public boolean verificaDataFinalContrato(final HashMap mapFields) {
   		final String dtInicioContrato = (String) mapFields.get("DATACONTRATO");
   		final String dtTerminoContrato = (String) mapFields.get("DATAFIMCONTRATO");

   		Date dataInicio = null;
   		Date dataFim = null;
   		boolean retorno = Boolean.TRUE;

   		try {
   			dataInicio = new SimpleDateFormat("dd/MM/yyyy").parse(dtInicioContrato);
   			dataFim = new SimpleDateFormat("dd/MM/yyyy").parse(dtTerminoContrato);

   			if (dataInicio.compareTo(dataFim) > 0) {
   				retorno = Boolean.FALSE;
   			}
   		} catch (final ParseException e) {
   			e.printStackTrace();
   		}

   		return retorno;
   	}
}
