/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 *
 */
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.ContratosGruposDTO;
import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.bean.GrupoEmpregadoDTO;
import br.com.centralit.citcorpore.integracao.ContratosGruposDAO;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.service.ServiceLocator;

/**
 * @author Centralit
 *
 */
@SuppressWarnings({"unchecked"})
public class ContratosGruposServiceEjb extends CrudServiceImpl implements ContratosGruposService {

    private ContratosGruposDAO dao;

    @Override
    protected ContratosGruposDAO getDao() {
        if (dao == null) {
            dao = new ContratosGruposDAO();
        }
        return dao;
    }

    @Override
    public Collection<ContratosGruposDTO> findByIdGrupo(final Integer idGrupo) throws Exception {
        try {
            return this.getDao().findByIdGrupo(idGrupo);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<ContratosGruposDTO> findByIdEmpregado(final Integer idEmpregado) throws Exception {
        final GrupoService grupoService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);

        try {
            final Collection<GrupoDTO> gruposEmpregado = grupoService.getGruposByPessoa(idEmpregado);
            return this.getDao().findByGrupos(gruposEmpregado);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdGrupo(final Integer idGrupo) throws Exception {
        try {
            this.getDao().deleteByIdGrupo(idGrupo);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<ContratosGruposDTO> findByGrupos(final Collection<GrupoDTO> gruposEmpregado) throws Exception {
        try {
            return this.getDao().findByGrupos(gruposEmpregado);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<ContratosGruposDTO> findByIdContrato(final Integer idContrato) throws Exception {
        try {
            return this.getDao().findByIdContrato(idContrato);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdContrato(final Integer idContrato) throws Exception {
        try {
            this.getDao().deleteByIdContrato(idContrato);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean hasContrato(final Collection<GrupoEmpregadoDTO> gruposEmpregado, final ContratoDTO contrato) throws Exception {
        try {
            return this.getDao().hasContrato(gruposEmpregado, contrato);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    public ArrayList<ContratosGruposDTO> buscarListaContratos(final Integer idGrupo) throws Exception {
        try {
            return this.getDao().buscarListaContratos(idGrupo);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
