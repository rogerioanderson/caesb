/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.JornadaTrabalhoDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;
@SuppressWarnings("rawtypes")
public interface JornadaTrabalhoService extends CrudService {

	/**
	 * Exclui jornada caso a mesma n�o esteja sendo utilizada no calend�rio.
	 * 
	 * @param model
	 * @param document
	 * @throws ServiceException
	 * @throws Exception
	 */
	public String deletarJornada(IDto model) throws ServiceException, Exception;

	/**
	 * Consultar jornadas ativas
	 * 
	 * @return retorna a lista de jornadas que est�o ativas
	 * @throws Exception
	 * @author rodrigo.oliveira
	 */
	
	public Collection listarJornadasAtivas() throws Exception;
	
	/**
	 * Verifica jornada existente
	 * 
	 * @param JornadaTrabalhoDTO
	 * @return retorna true se caso exista jornada j� cadastrada
	 * @throws Exception
	 * @author rodrigo.oliveira
	 */
	public boolean verificaJornadaExistente(JornadaTrabalhoDTO jornadaTrabalho) throws Exception;
		
}
