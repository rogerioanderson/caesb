/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart
 */
package br.com.centralit.citcorpore.negocio;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;

import br.com.centralit.bpm.dto.PermissoesFluxoDTO;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.ajaxForms.RequisicaoMudanca;
import br.com.centralit.citcorpore.bean.AprovacaoMudancaDTO;
import br.com.centralit.citcorpore.bean.AprovacaoPropostaDTO;
import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;
import br.com.centralit.citcorpore.bean.CalculoJornadaDTO;
import br.com.centralit.citcorpore.bean.ContatoRequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.bean.GrupoEmpregadoDTO;
import br.com.centralit.citcorpore.bean.GrupoRequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.HistoricoGEDDTO;
import br.com.centralit.citcorpore.bean.HistoricoMudancaDTO;
import br.com.centralit.citcorpore.bean.ItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.JustificativaRequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.LiberacaoMudancaDTO;
import br.com.centralit.citcorpore.bean.LigacaoRequisicaoMudancaHistoricoGrupoDTO;
import br.com.centralit.citcorpore.bean.LigacaoRequisicaoMudancaHistoricoItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.LigacaoRequisicaoMudancaHistoricoProblemaDTO;
import br.com.centralit.citcorpore.bean.LigacaoRequisicaoMudancaHistoricoResponsavelDTO;
import br.com.centralit.citcorpore.bean.LigacaoRequisicaoMudancaHistoricoRiscosDTO;
import br.com.centralit.citcorpore.bean.LigacaoRequisicaoMudancaHistoricoServicoDTO;
import br.com.centralit.citcorpore.bean.OcorrenciaMudancaDTO;
import br.com.centralit.citcorpore.bean.PesquisaRequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.ProblemaDTO;
import br.com.centralit.citcorpore.bean.ProblemaMudancaDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaResponsavelDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaRiscoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaServicoDTO;
import br.com.centralit.citcorpore.bean.ReuniaoRequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.ServicoContratoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoMudancaDTO;
import br.com.centralit.citcorpore.bean.TemplateSolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.TipoMudancaDTO;
import br.com.centralit.citcorpore.bean.UploadDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.AprovacaoMudancaDao;
import br.com.centralit.citcorpore.integracao.AprovacaoPropostaDao;
import br.com.centralit.citcorpore.integracao.ContatoRequisicaoMudancaDao;
import br.com.centralit.citcorpore.integracao.EmpregadoDao;
import br.com.centralit.citcorpore.integracao.GrupoDao;
import br.com.centralit.citcorpore.integracao.GrupoRequisicaoMudancaDao;
import br.com.centralit.citcorpore.integracao.HistoricoGEDDao;
import br.com.centralit.citcorpore.integracao.HistoricoMudancaDao;
import br.com.centralit.citcorpore.integracao.LiberacaoMudancaDao;
import br.com.centralit.citcorpore.integracao.LigacaoRequisicaoLiberacaoRiscosDao;
import br.com.centralit.citcorpore.integracao.LigacaoRequisicaoMudancaGrupoDao;
import br.com.centralit.citcorpore.integracao.LigacaoRequisicaoMudancaItemConfiguracaoDao;
import br.com.centralit.citcorpore.integracao.LigacaoRequisicaoMudancaProblemaDao;
import br.com.centralit.citcorpore.integracao.LigacaoRequisicaoMudancaResponsavelDao;
import br.com.centralit.citcorpore.integracao.LigacaoRequisicaoMudancaServicoDao;
import br.com.centralit.citcorpore.integracao.OcorrenciaMudancaDao;
import br.com.centralit.citcorpore.integracao.PermissoesFluxoDao;
import br.com.centralit.citcorpore.integracao.ProblemaMudancaDAO;
import br.com.centralit.citcorpore.integracao.RequisicaoMudancaDao;
import br.com.centralit.citcorpore.integracao.RequisicaoMudancaItemConfiguracaoDao;
import br.com.centralit.citcorpore.integracao.RequisicaoMudancaResponsavelDao;
import br.com.centralit.citcorpore.integracao.RequisicaoMudancaRiscoDao;
import br.com.centralit.citcorpore.integracao.RequisicaoMudancaServicoDao;
import br.com.centralit.citcorpore.integracao.ReuniaoRequisicaoMudancaDAO;
import br.com.centralit.citcorpore.integracao.SolicitacaoServicoMudancaDao;
import br.com.centralit.citcorpore.integracao.TemplateSolicitacaoServicoDao;
import br.com.centralit.citcorpore.integracao.TipoMudancaDAO;
import br.com.centralit.citcorpore.integracao.UsuarioDao;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.CriptoUtils;
import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.Enumerados.CategoriaOcorrencia;
import br.com.centralit.citcorpore.util.Enumerados.OrigemOcorrencia;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.Enumerados.SituacaoRequisicaoMudanca;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citcorpore.util.Util;
import br.com.centralit.citged.bean.ControleGEDDTO;
import br.com.centralit.citged.integracao.ControleGEDDao;
import br.com.citframework.dto.IDto;
import br.com.citframework.dto.Usuario;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.CrudDAO;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.Reflexao;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;
import br.com.citframework.util.UtilStrings;
import net.htmlparser.jericho.Source;

@SuppressWarnings({"rawtypes", "unchecked"})
public class RequisicaoMudancaServiceEjb extends CrudServiceImpl implements RequisicaoMudancaService {

    private RequisicaoMudancaDao dao;

    private ItemConfiguracaoService itemConfiguracaoService;
    
    /**
     * Desenvolvedor: ibimon.morais - Data: 17/08/2015 - Hor�rio: 10:48 - ID Citsmart: 176362 - 
     * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
     */
	private RequisicaoMudancaDao daoReports;

    public RequisicaoMudancaDao getDao(final String aliasDataBase) {
    	if (UtilStrings.isNotVazio(aliasDataBase)) {
			if (daoReports == null) {
				daoReports = new RequisicaoMudancaDao(aliasDataBase);
			}
			return daoReports;
		}
		return this.getDao();
    }

	
    @Override
    public RequisicaoMudancaDao getDao() {
        if (dao == null) {
            dao = new RequisicaoMudancaDao();
        }
        return dao;
    }

    @Override
    public void updateNotNull(final IDto obj) throws Exception {
        this.getDao().updateNotNull(obj);
    }

    /*
     * (non-Javadoc)
     * @see br.com.citframework.service.CrudServicePojoImpl#create(br.com.citframework.dto.IDto)
     */
    @Override
	public IDto create(final IDto model, final DocumentHTML document, final HttpServletRequest request) throws ServiceException, LogicException {
        final ExecucaoMudancaServiceEjb execucaoMudancaService = new ExecucaoMudancaServiceEjb();
        final SolicitacaoServicoMudancaDao solicitacaoServicoMudancaDao = new SolicitacaoServicoMudancaDao();
        final TipoMudancaDAO tipoMudancaDAO = new TipoMudancaDAO();
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao();

        ContatoRequisicaoMudancaDTO contatoRequisicaoMudancaDto = new ContatoRequisicaoMudancaDTO();

        final AprovacaoMudancaDao aprovacaoMudancaDao = new AprovacaoMudancaDao();
        final AprovacaoPropostaDao aprovacaoPropostaDao = new AprovacaoPropostaDao();
        final RequisicaoMudancaItemConfiguracaoDao requisicaoMudancaItemConfiguracaoDao = new RequisicaoMudancaItemConfiguracaoDao();
        final ProblemaMudancaDAO problemaMudancaDao = new ProblemaMudancaDAO();
        final RequisicaoMudancaRiscoDao requisicaoMudancaRiscoDao = new RequisicaoMudancaRiscoDao();
        final LiberacaoMudancaDao liberacaoMudancaDao = new LiberacaoMudancaDao();
        final RequisicaoMudancaServicoDao requisicaoMudancaServicoDao = new RequisicaoMudancaServicoDao();
        final RequisicaoMudancaResponsavelDao requisicaoMudancaResponsavelDao = new RequisicaoMudancaResponsavelDao();
        final GrupoRequisicaoMudancaDao grupoRequisicaoMudancaDao = new GrupoRequisicaoMudancaDao();

        TipoMudancaDTO tipoMudancaDTO = new TipoMudancaDTO();
        RequisicaoMudancaDTO requisicaoMudancaDto = (RequisicaoMudancaDTO) model;

        final TransactionControler tc = new TransactionControlerImpl(requisicaoMudancaDao.getAliasDB());

        try {
            tc.start();

            this.validaCreate(model);

            tipoMudancaDAO.setTransactionControler(tc);
            requisicaoMudancaDao.setTransactionControler(tc);
            solicitacaoServicoMudancaDao.setTransactionControler(tc);
            aprovacaoMudancaDao.setTransactionControler(tc);
            aprovacaoPropostaDao.setTransactionControler(tc);
            requisicaoMudancaItemConfiguracaoDao.setTransactionControler(tc);
            problemaMudancaDao.setTransactionControler(tc);
            requisicaoMudancaRiscoDao.setTransactionControler(tc);
            liberacaoMudancaDao.setTransactionControler(tc);
            requisicaoMudancaServicoDao.setTransactionControler(tc);
            requisicaoMudancaResponsavelDao.setTransactionControler(tc);
            grupoRequisicaoMudancaDao.setTransactionControler(tc);

            if (usuario == null) {
                usuario = new Usuario();
            }

            if (usuario != null && requisicaoMudancaDto != null) {
                usuario.setLocale(requisicaoMudancaDto.getUsuarioDto().getLocale());
            }

            if (requisicaoMudancaDto != null && requisicaoMudancaDto.getDataHoraInicioAgendada() != null && requisicaoMudancaDto.getHoraAgendamentoInicial() != null
                    && requisicaoMudancaDto != null && requisicaoMudancaDto.getDataHoraInicioAgendada() != null && requisicaoMudancaDto.getHoraAgendamentoInicial() != null) {
                final boolean resultado = this.seHoraFinalMenorQHoraInicial(requisicaoMudancaDto);
                if (resultado == true) {
                    throw new LogicException(this.i18nMessage("requisicaoMudanca.horaFinalMenorQueInicial"));
                }
            }

            /*if (requisicaoMudancaDto != null && requisicaoMudancaDto.getDataHoraInicioAgendada() != null && requisicaoMudancaDto.getHoraAgendamentoInicial() != null) {
                final boolean resultado = this.seHoraInicialMenorQAtual(requisicaoMudancaDto);
                if (resultado == true) {
                    throw new LogicException(this.i18nMessage("requisicaoMudanca.horaInicialMenorQueAtual"));
                }
            }*/

            /*if (requisicaoMudancaDto != null && requisicaoMudancaDto.getDataHoraTerminoAgendada() != null && requisicaoMudancaDto.getHoraAgendamentoFinal() != null) {
                final boolean resultado = this.seHoraFinalMenorQAtual(requisicaoMudancaDto);
                if (resultado == true) {
                    throw new LogicException(this.i18nMessage("requisicaoMudanca.horaFinalMenorQueAtual"));
                }
            }*/

            if (requisicaoMudancaDto != null && requisicaoMudancaDto.getDataHoraInicioAgendada() != null && requisicaoMudancaDto.getHoraAgendamentoInicial() != null) {
                final Timestamp dataHoraInicial = this.MontardataHoraAgendamentoInicial(requisicaoMudancaDto);
                requisicaoMudancaDto.setDataHoraInicioAgendada(dataHoraInicial);
            }

            if (requisicaoMudancaDto != null && requisicaoMudancaDto.getDataHoraTerminoAgendada() != null && requisicaoMudancaDto.getHoraAgendamentoFinal() != null) {
                final Timestamp dataHoraFinal = this.MontardataHoraAgendamentoFinal(requisicaoMudancaDto);
                requisicaoMudancaDto.setDataHoraTerminoAgendada(dataHoraFinal);
                requisicaoMudancaDto.setDataHoraTermino(dataHoraFinal);
            }

            final boolean resultado = this.validacaoGrupoExecutor(requisicaoMudancaDto);
            if (resultado == false) {
                throw new LogicException(this.i18nMessage("requisicaoMudanca.grupoSemPermissao"));
            }

            if (requisicaoMudancaDto.getDataHoraTerminoAgendada() != null && requisicaoMudancaDto.getDataHoraInicioAgendada() != null) {

                this.calculaTempoAtraso(requisicaoMudancaDto);

            } else {
                requisicaoMudancaDto.setPrazoHH(00);
                requisicaoMudancaDto.setPrazoMM(00);
            }

            if (requisicaoMudancaDto.getUsuarioDto() == null) {
                throw new LogicException(this.i18nMessage("citcorpore.comum.usuarioNaoidentificado"));
            }

            if (requisicaoMudancaDto.getIdTipoMudanca() != null) {

                tipoMudancaDTO.setIdTipoMudanca(requisicaoMudancaDto.getIdTipoMudanca());
                tipoMudancaDTO = (TipoMudancaDTO) tipoMudancaDAO.restore(tipoMudancaDTO);
            }

            if (tipoMudancaDTO.getIdGrupoExecutor() == null) {

                throw new LogicException(this.i18nMessage("citcorpore.comum.grupoExecutorNaoDefinido"));
            }

            if (tipoMudancaDTO.getIdCalendario() == null) {

                throw new LogicException(this.i18nMessage("citcorpore.comum.calendarioNaoDefinido"));
            }

            if (requisicaoMudancaDto.getEhPropostaAux().equalsIgnoreCase("S")) {
                requisicaoMudancaDto.setStatus(SituacaoRequisicaoMudanca.Proposta.name());
            } else {
                requisicaoMudancaDto.setStatus(SituacaoRequisicaoMudanca.Registrada.name());
            }

            if (requisicaoMudancaDto.getIdGrupoAtual() == null) {
                requisicaoMudancaDto.setIdGrupoAtual(tipoMudancaDTO.getIdGrupoExecutor());
            }
            requisicaoMudancaDto.setIdGrupoNivel1(requisicaoMudancaDto.getIdGrupoAtual());
            requisicaoMudancaDto.setIdCalendario(tipoMudancaDTO.getIdCalendario());
            requisicaoMudancaDto.setTempoDecorridoHH(new Integer(0));
            requisicaoMudancaDto.setTempoDecorridoMM(new Integer(0));
            requisicaoMudancaDto.setDataHoraSuspensao(null);
            requisicaoMudancaDto.setDataHoraReativacao(null);
            requisicaoMudancaDto.setSeqReabertura(new Integer(0));
            requisicaoMudancaDto.setDataHoraSolicitacao(new Timestamp(new java.util.Date().getTime()));
            requisicaoMudancaDto.setIdProprietario(requisicaoMudancaDto.getUsuarioDto().getIdEmpregado());

            requisicaoMudancaDto.setDataHoraInicio(new Timestamp(new java.util.Date().getTime()));

            requisicaoMudancaDto.setDataHoraCaptura(requisicaoMudancaDto.getDataHoraInicio());

            contatoRequisicaoMudancaDto = this.criarContatoRequisicaoMudanca(requisicaoMudancaDto, tc);

            requisicaoMudancaDto.setIdContatoRequisicaoMudanca(contatoRequisicaoMudancaDto.getIdContatoRequisicaoMudanca());

            if (contatoRequisicaoMudancaDto.getIdLocalidade() != null) {
                requisicaoMudancaDto.setIdLocalidade(contatoRequisicaoMudancaDto.getIdLocalidade());
            }

			final String remetente = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_RemetenteNotificacoesSolicitacao, null);
            if (remetente == null) {
                throw new LogicException(this.i18nMessage("citcorpore.comum.notficacaoEmailParametrizado"));
            }

            requisicaoMudancaDto = (RequisicaoMudancaDTO) requisicaoMudancaDao.create(requisicaoMudancaDto);

            this.criarOcorrenciaMudanca(requisicaoMudancaDto, tc);

            Source source = null;
            if (requisicaoMudancaDto != null) {
                source = new Source(requisicaoMudancaDto.getRegistroexecucao());
                requisicaoMudancaDto.setRegistroexecucao(source.getTextExtractor().toString());
            }

            if (requisicaoMudancaDto != null && requisicaoMudancaDto.getRegistroexecucao() != null && !requisicaoMudancaDto.getRegistroexecucao().trim().equalsIgnoreCase("")) {
                final OcorrenciaMudancaDao ocorrenciaMudancaDao = new OcorrenciaMudancaDao();
                ocorrenciaMudancaDao.setTransactionControler(tc);
                final OcorrenciaMudancaDTO ocorrenciaMudancaDto = new OcorrenciaMudancaDTO();
                ocorrenciaMudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                ocorrenciaMudancaDto.setDataregistro(UtilDatas.getDataAtual());
                ocorrenciaMudancaDto.setHoraregistro(UtilDatas.formatHoraFormatadaStr(UtilDatas.getHoraAtual()));
                ocorrenciaMudancaDto.setTempoGasto(0);
                ocorrenciaMudancaDto.setDescricao(br.com.centralit.citcorpore.util.Enumerados.CategoriaOcorrencia.Execucao.getDescricao());
                ocorrenciaMudancaDto.setDataInicio(UtilDatas.getDataAtual());
                ocorrenciaMudancaDto.setDataFim(UtilDatas.getDataAtual());
				ocorrenciaMudancaDto.setInformacoesContato("n�o se aplica");
                ocorrenciaMudancaDto.setRegistradopor(requisicaoMudancaDto.getUsuarioDto().getLogin());
                try {
                    ocorrenciaMudancaDto.setDadosMudanca(new Gson().toJson(requisicaoMudancaDto));
                } catch (final Exception e) {
                    e.printStackTrace();
                }
                ocorrenciaMudancaDto.setOrigem(br.com.centralit.citcorpore.util.Enumerados.OrigemOcorrencia.OUTROS.getSigla().toString());
                ocorrenciaMudancaDto.setCategoria(br.com.centralit.citcorpore.util.Enumerados.CategoriaOcorrencia.Criacao.getSigla());
                ocorrenciaMudancaDto.setOcorrencia(requisicaoMudancaDto.getRegistroexecucao());
                ocorrenciaMudancaDao.create(ocorrenciaMudancaDto);
            }

            if (requisicaoMudancaDto != null) {
                if (requisicaoMudancaDto.getListIdSolicitacaoServico() != null && requisicaoMudancaDto.getListIdSolicitacaoServico().size() > 0) {
                    for (final SolicitacaoServicoDTO solicitacaoServicoDTO : requisicaoMudancaDto.getListIdSolicitacaoServico()) {
                        final SolicitacaoServicoMudancaDTO solicitacaoServicoMudancaDTO = new SolicitacaoServicoMudancaDTO();
                        solicitacaoServicoMudancaDTO.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                        solicitacaoServicoMudancaDTO.setIdSolicitacaoServico(solicitacaoServicoDTO.getIdSolicitacaoServico());
                        solicitacaoServicoMudancaDao.create(solicitacaoServicoMudancaDTO);
                    }
                }

                if (requisicaoMudancaDto.getListAprovacaoMudancaDTO() != null) {
                    for (final AprovacaoMudancaDTO aprovacaoMudancaDto : requisicaoMudancaDto.getListAprovacaoMudancaDTO()) {
                        aprovacaoMudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                        aprovacaoMudancaDto.setDataHoraInicio(UtilDatas.getDataHoraAtual());
                        aprovacaoMudancaDao.create(aprovacaoMudancaDto);
                    }
                }

                if (requisicaoMudancaDto.getListAprovacaoPropostaDTO() != null) {
                    for (final AprovacaoPropostaDTO aprovacaoPropostaDto : requisicaoMudancaDto.getListAprovacaoPropostaDTO()) {
                        aprovacaoPropostaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                        aprovacaoPropostaDto.setDataHoraInicio(UtilDatas.getDataHoraAtual());
                        aprovacaoPropostaDao.create(aprovacaoPropostaDto);
                    }
                }

                if (requisicaoMudancaDto.getListRequisicaoMudancaItemConfiguracaoDTO() != null) {
                    for (final RequisicaoMudancaItemConfiguracaoDTO requisicaoMudancaItemConfiguracaoDto : requisicaoMudancaDto.getListRequisicaoMudancaItemConfiguracaoDTO()) {
                        requisicaoMudancaItemConfiguracaoDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                        requisicaoMudancaItemConfiguracaoDao.create(requisicaoMudancaItemConfiguracaoDto);
                    }
                }

                if (requisicaoMudancaDto.getListProblemaMudancaDTO() != null) {
                    for (final ProblemaMudancaDTO problemaMudancaDto : requisicaoMudancaDto.getListProblemaMudancaDTO()) {
                        problemaMudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                        problemaMudancaDao.create(problemaMudancaDto);
                    }
                }

                if (requisicaoMudancaDto.getListRequisicaoMudancaRiscoDTO() != null) {
                    for (final RequisicaoMudancaRiscoDTO RequisicaoMudancaRiscoDto : requisicaoMudancaDto.getListRequisicaoMudancaRiscoDTO()) {
                        RequisicaoMudancaRiscoDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                        requisicaoMudancaRiscoDao.create(RequisicaoMudancaRiscoDto);
                    }
                }

                if (requisicaoMudancaDto.getListGrupoRequisicaoMudancaDTO() != null) {
                    for (final GrupoRequisicaoMudancaDTO grupoRequisicaoMudancaDto : requisicaoMudancaDto.getListGrupoRequisicaoMudancaDTO()) {
                        grupoRequisicaoMudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                        grupoRequisicaoMudancaDao.create(grupoRequisicaoMudancaDto);
                    }
                }

                // geber.costa
                if (requisicaoMudancaDto.getListLiberacaoMudancaDTO() != null) {
                    for (final LiberacaoMudancaDTO liberacaoMudancaDto : requisicaoMudancaDto.getListLiberacaoMudancaDTO()) {
                        liberacaoMudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                        liberacaoMudancaDto.setIdLiberacao(liberacaoMudancaDto.getIdLiberacao());
						// O Trim foi utilizado para tratamento, os campos n�o virem com espa�o
                        if (liberacaoMudancaDto.getSituacaoLiberacao() != null) {
                            liberacaoMudancaDto.setSituacaoLiberacao(liberacaoMudancaDto.getSituacaoLiberacao().trim());
                        }
                        if (liberacaoMudancaDto.getStatus() != null) {
                            liberacaoMudancaDto.setStatus(liberacaoMudancaDto.getStatus().trim());
                        }
                        liberacaoMudancaDao.create(liberacaoMudancaDto);
                    }
                }

                if (requisicaoMudancaDto.getDataHoraInicioAgendada() != null && requisicaoMudancaDto.getDataHoraTerminoAgendada() != null
                        && (requisicaoMudancaDto.getIdGrupoAtvPeriodica() == null || requisicaoMudancaDto.getIdGrupoAtvPeriodica() == 0)) {
                    throw new LogicException(this.i18nMessage("gerenciaservico.agendaratividade.informacoesGrupoAtividade"));
                }

                if (requisicaoMudancaDto.getListRequisicaoMudancaServicoDTO() != null) {
                    for (final RequisicaoMudancaServicoDTO requisicaoMudancaServicoDto : requisicaoMudancaDto.getListRequisicaoMudancaServicoDTO()) {
                        requisicaoMudancaServicoDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                        requisicaoMudancaServicoDao.create(requisicaoMudancaServicoDto);
                    }
                }
            }

            // create Responsavel
            Collection<RequisicaoMudancaResponsavelDTO> colRequisicaoMudancaResp = null;
            if (requisicaoMudancaDto != null) {
                colRequisicaoMudancaResp = requisicaoMudancaDto.getColResponsaveis();
            }
            if (colRequisicaoMudancaResp != null) {
                for (final RequisicaoMudancaResponsavelDTO mudancaResponsavelDTO : colRequisicaoMudancaResp) {
                    mudancaResponsavelDTO.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                    requisicaoMudancaResponsavelDao.create(mudancaResponsavelDTO);
                }
            }

            final HistoricoMudancaDTO historicoMudancaDTO = new HistoricoMudancaDTO();
            
			// gravar anexos de mudan�a
            if (requisicaoMudancaDto != null && requisicaoMudancaDto.getColArquivosUpload() != null) {
                this.gravaInformacoesGED(requisicaoMudancaDto, tc, historicoMudancaDTO);
            }

			// gravar anexos dos planos de reversao de mudan�a
            if (requisicaoMudancaDto != null && requisicaoMudancaDto.getColUploadPlanoDeReversaoGED() != null) {
                this.gravaPlanoDeReversaoGED(requisicaoMudancaDto, tc, historicoMudancaDTO);
            }

            if (requisicaoMudancaDto != null) {
                execucaoMudancaService.registraMudanca(requisicaoMudancaDto, tc, requisicaoMudancaDto.getUsuarioDto());
            }

            tc.commit();

            if (requisicaoMudancaDto != null && requisicaoMudancaDto.getDataHoraInicioAgendada() != null) {
                RequisicaoMudanca.salvaGrupoAtvPeriodicaEAgenda(requisicaoMudancaDto, document, request);
            }
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
            throw new ServiceException(e);
        } finally {
            try {
                tc.close();
            } catch (final PersistenceException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        return model;
    }

    public void criarOcorrenciaMudanca(final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) throws Exception {
        final OcorrenciaMudancaDao ocorrenciaMudancaDao = new OcorrenciaMudancaDao();
        ocorrenciaMudancaDao.setTransactionControler(tc);
        final OcorrenciaMudancaDTO ocorrenciaMudancaDto = new OcorrenciaMudancaDTO();
        ocorrenciaMudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
        ocorrenciaMudancaDto.setDataregistro(UtilDatas.getDataAtual());
        ocorrenciaMudancaDto.setHoraregistro(UtilDatas.formatHoraFormatadaStr(UtilDatas.getHoraAtual()));
        ocorrenciaMudancaDto.setTempoGasto(0);
        ocorrenciaMudancaDto.setDescricao(br.com.centralit.citcorpore.util.Enumerados.CategoriaOcorrencia.Execucao.getDescricao());
        ocorrenciaMudancaDto.setDataInicio(UtilDatas.getDataAtual());
        ocorrenciaMudancaDto.setDataFim(UtilDatas.getDataAtual());
		ocorrenciaMudancaDto.setInformacoesContato("n�o se aplica");
        ocorrenciaMudancaDto.setRegistradopor(requisicaoMudancaDto.getUsuarioDto().getLogin());
        try {
            ocorrenciaMudancaDto.setDadosMudanca(new Gson().toJson(requisicaoMudancaDto));
        } catch (final Exception e) {
			System.out.println("Falha na grava��o de dadosMudanca - Objeto Gson");
            e.printStackTrace();
        }
        ocorrenciaMudancaDto.setOrigem(br.com.centralit.citcorpore.util.Enumerados.OrigemOcorrencia.OUTROS.getSigla().toString());
        ocorrenciaMudancaDto.setCategoria(br.com.centralit.citcorpore.util.Enumerados.CategoriaOcorrencia.Criacao.getSigla());
        ocorrenciaMudancaDao.create(ocorrenciaMudancaDto);
    }

    /**
     * PODE SER UTILIZADO PARA SETAR OS CONTATOS DA REQUISICAO MUDANCA
     *
     */
    public ContatoRequisicaoMudancaDTO criarContatoRequisicaoMudanca(final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) throws ServiceException,
            LogicException {

        final ContatoRequisicaoMudancaDao contatoRequisicaoMudancaDao = new ContatoRequisicaoMudancaDao();
        ContatoRequisicaoMudancaDTO contatoRequisicaoMudancaDto = new ContatoRequisicaoMudancaDTO();

        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao();

        try {
            contatoRequisicaoMudancaDao.setTransactionControler(tc);
            requisicaoMudancaDao.setTransactionControler(tc);

            if (requisicaoMudancaDto.getIdContatoRequisicaoMudanca() != null) {

                contatoRequisicaoMudancaDto.setIdContatoRequisicaoMudanca(requisicaoMudancaDto.getIdContatoRequisicaoMudanca());
                contatoRequisicaoMudancaDto.setNomecontato(requisicaoMudancaDto.getNomeContato());
                contatoRequisicaoMudancaDto.setTelefonecontato(requisicaoMudancaDto.getTelefoneContato());
                contatoRequisicaoMudancaDto.setRamal(requisicaoMudancaDto.getRamal());
                if (requisicaoMudancaDto.getEmailSolicitante() != null) {
                    contatoRequisicaoMudancaDto.setEmailcontato(requisicaoMudancaDto.getEmailSolicitante().trim());
                }
                contatoRequisicaoMudancaDto.setObservacao(requisicaoMudancaDto.getObservacao());
                contatoRequisicaoMudancaDto.setIdLocalidade(requisicaoMudancaDto.getIdLocalidade());
                contatoRequisicaoMudancaDao.update(contatoRequisicaoMudancaDto);

            } else {
                contatoRequisicaoMudancaDto.setNomecontato(requisicaoMudancaDto.getNomeContato());
                contatoRequisicaoMudancaDto.setTelefonecontato(requisicaoMudancaDto.getTelefoneContato());
                contatoRequisicaoMudancaDto.setRamal(requisicaoMudancaDto.getRamal());
                if (requisicaoMudancaDto.getEmailSolicitante() != null) {
                    contatoRequisicaoMudancaDto.setEmailcontato(requisicaoMudancaDto.getEmailSolicitante().trim());
                }
                contatoRequisicaoMudancaDto.setObservacao(requisicaoMudancaDto.getObservacao());
                contatoRequisicaoMudancaDto.setIdLocalidade(requisicaoMudancaDto.getIdLocalidade());
                contatoRequisicaoMudancaDto = (ContatoRequisicaoMudancaDTO) contatoRequisicaoMudancaDao.create(contatoRequisicaoMudancaDto);
            }

        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
            throw new ServiceException(e);
        }
        return contatoRequisicaoMudancaDto;
    }

    @SuppressWarnings("unused")
    private void determinaPrazo(final RequisicaoMudancaDTO requisicaoDto, final Integer idCalendarioParm) throws Exception {
        if (requisicaoDto.getDataHoraTerminoAgendada() == null) {
            throw new LogicException(this.i18nMessage("citcorpore.comum.Data/horaTerminoNaoDefinida"));
        }

        CalculoJornadaDTO calculoDto = new CalculoJornadaDTO(idCalendarioParm, requisicaoDto.getDataHoraInicioAgendada());
        calculoDto = new CalendarioServiceEjb().calculaPrazoDecorrido(calculoDto, requisicaoDto.getDataHoraTerminoAgendada(), null);
        requisicaoDto.setPrazoHH(calculoDto.getTempoDecorridoHH());
        requisicaoDto.setPrazoMM(calculoDto.getTempoDecorridoMM());
    }

    @SuppressWarnings("unused")
    private void determinaPrazo(final RequisicaoMudancaDTO requisicaoDto) throws Exception {
        if (requisicaoDto.getDataHoraTerminoAgendada() == null) {
            throw new LogicException(this.i18nMessage("citcorpore.comum.Data/horaTerminoNaoDefinida"));
        }

        TipoMudancaDTO tipoMudancaDto = new TipoMudancaDTO();
        final TipoMudancaDAO tipoMudancaDAO = new TipoMudancaDAO();
        tipoMudancaDto.setIdTipoMudanca(requisicaoDto.getIdTipoMudanca());
        tipoMudancaDto = (TipoMudancaDTO) tipoMudancaDAO.restore(tipoMudancaDto);

        CalculoJornadaDTO calculoDto = new CalculoJornadaDTO(tipoMudancaDto.getIdCalendario(), requisicaoDto.getDataHoraInicioAgendada());
        calculoDto = new CalendarioServiceEjb().calculaPrazoDecorrido(calculoDto, requisicaoDto.getDataHoraTerminoAgendada(), null);
        requisicaoDto.setPrazoHH(calculoDto.getTempoDecorridoHH());
        requisicaoDto.setPrazoMM(calculoDto.getTempoDecorridoMM());
    }

    /**
	 * Lista os ics relacionados a requisi��o de mudan�a e atribui o nome do item de configura��o para correta restaura��o de suas informa��es na table
	 *
	 * @param requisicaoMudancaItemConfiguracaoDTO
	 * @throws ServiceException
	 * @throws Exception
	 */
    @Override
	public ArrayList<RequisicaoMudancaItemConfiguracaoDTO> listItensRelacionadosRequisicaoMudanca(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws ServiceException,
            Exception {
        ItemConfiguracaoDTO ic = null;
        final RequisicaoMudancaItemConfiguracaoDao requisicaoMudancaItemConfiguracaoDao = new RequisicaoMudancaItemConfiguracaoDao();

        final ArrayList<RequisicaoMudancaItemConfiguracaoDTO> listaReqMudancaIC = (ArrayList<RequisicaoMudancaItemConfiguracaoDTO>) requisicaoMudancaItemConfiguracaoDao
                .findByIdMudancaEDataFim(requisicaoMudancaDTO.getIdRequisicaoMudanca());
        // getRequisicaoMudancaItemConfiguracaoService().listByIdRequisicaoMudanca(requisicaoMudancaDTO.getIdRequisicaoMudanca());

        // atribui nome para os itens retornados
        if (listaReqMudancaIC != null) {
            for (final RequisicaoMudancaItemConfiguracaoDTO r : listaReqMudancaIC) {
                ic = this.getItemConfiguracaoService().restoreByIdItemConfiguracao(r.getIdItemConfiguracao());
                if (ic != null) {
                    r.setNomeItemConfiguracao(ic.getIdentificacao());
                }
            }
        }

        return listaReqMudancaIC;
    }

    private ItemConfiguracaoService getItemConfiguracaoService() throws ServiceException, Exception {
        if (itemConfiguracaoService == null) {
            itemConfiguracaoService = (ItemConfiguracaoService) ServiceLocator.getInstance().getService(ItemConfiguracaoService.class, null);
        }
        return itemConfiguracaoService;
    }

    /*
     * (non-Javadoc)
     * @see br.com.citframework.service.CrudServicePojoImpl#update(br.com.citframework.dto.IDto)
     */

    @Override
	public void update(final IDto model, final DocumentHTML document, final HttpServletRequest request) throws ServiceException, LogicException {
        final RequisicaoMudancaDTO requisicaoMudancaDto = (RequisicaoMudancaDTO) model;
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao();
        LigacaoRequisicaoMudancaHistoricoResponsavelDTO ligacaoResponsavelDTO = new LigacaoRequisicaoMudancaHistoricoResponsavelDTO();
        final LigacaoRequisicaoMudancaResponsavelDao ligacaoResponsavelDao = new LigacaoRequisicaoMudancaResponsavelDao();
        LigacaoRequisicaoMudancaHistoricoItemConfiguracaoDTO ligacaoRequisicaoMudancaHistoricoItemConfiguracaoDTO = new LigacaoRequisicaoMudancaHistoricoItemConfiguracaoDTO();
        final LigacaoRequisicaoMudancaItemConfiguracaoDao ligacaoRequisicaoMudancaItemConfiguracaoDao = new LigacaoRequisicaoMudancaItemConfiguracaoDao();
        LigacaoRequisicaoMudancaHistoricoServicoDTO ligacaoRequisicaoMudancaHistoricoServicoDTO = new LigacaoRequisicaoMudancaHistoricoServicoDTO();
        final LigacaoRequisicaoMudancaServicoDao ligacaoRequisicaoMudancaServicoDao = new LigacaoRequisicaoMudancaServicoDao();
        LigacaoRequisicaoMudancaHistoricoProblemaDTO ligacaoRequisicaoMudancaHistoricoProblemaDTO = new LigacaoRequisicaoMudancaHistoricoProblemaDTO();
        LigacaoRequisicaoMudancaHistoricoGrupoDTO ligacaoRequisicaoMudancaHistoricoGrupoDTO = new LigacaoRequisicaoMudancaHistoricoGrupoDTO();
        final LigacaoRequisicaoMudancaProblemaDao ligacaoRequisicaoMudancaProblemaDao = new LigacaoRequisicaoMudancaProblemaDao();
        final LigacaoRequisicaoMudancaGrupoDao ligacaoRequisicaoMudancaGrupoDao = new LigacaoRequisicaoMudancaGrupoDao();
        final LigacaoRequisicaoMudancaHistoricoRiscosDTO ligaHistoricoRiscosDTO = new LigacaoRequisicaoMudancaHistoricoRiscosDTO();
        final LigacaoRequisicaoLiberacaoRiscosDao ligacaoriscoDao = new LigacaoRequisicaoLiberacaoRiscosDao();

        if (usuario == null) {
            usuario = new Usuario();
        }

        if (usuario != null && requisicaoMudancaDto != null) {
            usuario.setLocale(requisicaoMudancaDto.getUsuarioDto().getLocale());
        }

        try {
            requisicaoMudancaDto.setDataHoraInicio(this.getRequisicaoAtual(requisicaoMudancaDto.getIdRequisicaoMudanca()).getDataHoraInicio());
        } catch (final Exception e1) {
            e1.printStackTrace();
        }

        if (requisicaoMudancaDto != null && requisicaoMudancaDto.getDataHoraInicioAgendada() != null && requisicaoMudancaDto.getHoraAgendamentoInicial() != null) {
            final Timestamp dataHoraInicial = this.MontardataHoraAgendamentoInicial(requisicaoMudancaDto);
            requisicaoMudancaDto.setDataHoraInicioAgendada(dataHoraInicial);
        }

        if (requisicaoMudancaDto != null && requisicaoMudancaDto.getDataHoraTerminoAgendada() != null && requisicaoMudancaDto.getHoraAgendamentoFinal() != null) {
            final Timestamp dataHoraFinal = this.MontardataHoraAgendamentoFinal(requisicaoMudancaDto);
            requisicaoMudancaDto.setDataHoraTerminoAgendada(dataHoraFinal);
            requisicaoMudancaDto.setDataHoraTermino(dataHoraFinal);
        }

        ContatoRequisicaoMudancaDTO contatoRequisicaoMudancaDto = new ContatoRequisicaoMudancaDTO();
        TipoMudancaDTO tipoMudancaDto = new TipoMudancaDTO();
        final SolicitacaoServicoMudancaDao solicitacaoServicoMudancaDao = new SolicitacaoServicoMudancaDao();

        final AprovacaoMudancaDao aprovacaoMudancaDao = new AprovacaoMudancaDao();
        final AprovacaoPropostaDao aprovacaoPropostaDao = new AprovacaoPropostaDao();

        final TipoMudancaDAO tipoMudancaDAO = new TipoMudancaDAO();
        final ProblemaMudancaDAO problemaMudancaDao = new ProblemaMudancaDAO();
        final GrupoRequisicaoMudancaDao gruporequisicaomudancaDao = new GrupoRequisicaoMudancaDao();

        final TransactionControler tc = new TransactionControlerImpl(requisicaoMudancaDao.getAliasDB());

        final RequisicaoMudancaServicoDao requisicaoMudancaServicoDao = new RequisicaoMudancaServicoDao();
        final RequisicaoMudancaItemConfiguracaoDao requisicaoMudancaItemConfiguracaoDao = new RequisicaoMudancaItemConfiguracaoDao();
        final RequisicaoMudancaRiscoDao requisicaoMudancaRiscoDao = new RequisicaoMudancaRiscoDao();
        final LiberacaoMudancaDao liberacaoMudancaDao = new LiberacaoMudancaDao();
        final RequisicaoMudancaResponsavelDao mudancaResponsavelDao = new RequisicaoMudancaResponsavelDao();
        try {
            tc.start();

            solicitacaoServicoMudancaDao.setTransactionControler(tc);
            requisicaoMudancaServicoDao.setTransactionControler(tc);
            requisicaoMudancaItemConfiguracaoDao.setTransactionControler(tc);
            tipoMudancaDAO.setTransactionControler(tc);
            requisicaoMudancaDao.setTransactionControler(tc);
            aprovacaoMudancaDao.setTransactionControler(tc);
            aprovacaoPropostaDao.setTransactionControler(tc);
            problemaMudancaDao.setTransactionControler(tc);
            gruporequisicaomudancaDao.setTransactionControler(tc);
            requisicaoMudancaRiscoDao.setTransactionControler(tc);
            liberacaoMudancaDao.setTransactionControler(tc);
            mudancaResponsavelDao.setTransactionControler(tc);
            ligacaoRequisicaoMudancaProblemaDao.setTransactionControler(tc);
            ligacaoRequisicaoMudancaGrupoDao.setTransactionControler(tc);
            ligacaoriscoDao.setTransactionControler(tc);

            if (requisicaoMudancaDto != null) {
                if (requisicaoMudancaDto.getIdTipoMudanca() != null) {
                    tipoMudancaDto.setIdTipoMudanca(requisicaoMudancaDto.getIdTipoMudanca());
                    tipoMudancaDto = (TipoMudancaDTO) tipoMudancaDAO.restore(tipoMudancaDto);
                }

                if (requisicaoMudancaDto.getListAprovacaoPropostaDTO() != null && requisicaoMudancaDto.getFase().equalsIgnoreCase("Proposta")) {
                    for (final AprovacaoPropostaDTO aprovacaoPropostaDto : requisicaoMudancaDto.getListAprovacaoPropostaDTO()) {
                        aprovacaoPropostaDao.deleteLinha(requisicaoMudancaDto.getIdRequisicaoMudanca(), aprovacaoPropostaDto.getIdEmpregado());
                        aprovacaoPropostaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                        aprovacaoPropostaDto.setDataHoraInicio(UtilDatas.getDataHoraAtual());
                        aprovacaoPropostaDao.create(aprovacaoPropostaDto);
                    }
                }

                if (requisicaoMudancaDto.getAcaoFluxo().equalsIgnoreCase("E") && requisicaoMudancaDto.getFase().equalsIgnoreCase("Proposta")) {
                    if (!requisicaoMudancaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoMudanca.Cancelada.name())) {
                        if (!this.validacaoAvancaFluxoProposta(requisicaoMudancaDto, tc)) {
                            throw new LogicException(this.i18nMessage("requisicaoMudanca.essaPropostaNaoFoiAprovada"));
                        }
                    }
                }

                if (requisicaoMudancaDto.getDataHoraTerminoAgendada() != null && requisicaoMudancaDto.getDataHoraInicioAgendada() != null) {

                    this.calculaTempoAtraso(requisicaoMudancaDto);

                } else {
                    requisicaoMudancaDto.setPrazoHH(00);
                    requisicaoMudancaDto.setPrazoMM(00);
                }

                if (requisicaoMudancaDto.getStatus() != null && !requisicaoMudancaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoMudanca.Cancelada.name())
                        && !requisicaoMudancaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoMudanca.Rejeitada.name())) {
                    if (requisicaoMudancaDto.getAlterarSituacao().equalsIgnoreCase("N")) {
                        requisicaoMudancaDto.setStatus(this.getStatusAtual(requisicaoMudancaDto.getIdRequisicaoMudanca()));
                    }
                }

            }

            // Gravando o historico
            HistoricoMudancaDTO historicoMudancaDTO = new HistoricoMudancaDTO();
            final HistoricoMudancaDao historicoMudancaDao = new HistoricoMudancaDao();
            historicoMudancaDao.setTransactionControler(tc);
            if (requisicaoMudancaDto != null && requisicaoMudancaDto.getIdRequisicaoMudanca() != null) {
                historicoMudancaDTO = (HistoricoMudancaDTO) historicoMudancaDao.create(this.createHistoricoMudanca(requisicaoMudancaDto));

                final ControleGEDDao controleGEDDao = new ControleGEDDao();
                controleGEDDao.setTransactionControler(tc);

                historicoMudancaDTO.setColResponsaveis(this.listarColResponsaveis(historicoMudancaDTO));
                if (historicoMudancaDTO.getColResponsaveis() != null) {
                    for (final RequisicaoMudancaResponsavelDTO requisicaoMudancaRespDTO : historicoMudancaDTO.getColResponsaveis()) {
                        ligacaoResponsavelDTO.setIdRequisicaoMudanca(requisicaoMudancaRespDTO.getIdRequisicaoMudanca());
                        ligacaoResponsavelDTO.setIdRequisicaoMudancaResp(requisicaoMudancaRespDTO.getIdRequisicaoMudancaResp());
                        ligacaoResponsavelDTO.setIdHistoricoMudanca(historicoMudancaDTO.getIdHistoricoMudanca());
                        ligacaoResponsavelDao.create(ligacaoResponsavelDTO);
                        ligacaoResponsavelDTO = new LigacaoRequisicaoMudancaHistoricoResponsavelDTO();
                    }
                }

                historicoMudancaDTO.setListRequisicaoMudancaItemConfiguracaoDTO(this.listarColItemConfiguracao(historicoMudancaDTO));
                if (historicoMudancaDTO.getListRequisicaoMudancaItemConfiguracaoDTO() != null) {
                    for (final RequisicaoMudancaItemConfiguracaoDTO requisicaoMudancaItemConfiguracaoDTO : historicoMudancaDTO.getListRequisicaoMudancaItemConfiguracaoDTO()) {
                        ligacaoRequisicaoMudancaHistoricoItemConfiguracaoDTO.setIdRequisicaoMudanca(requisicaoMudancaItemConfiguracaoDTO.getIdRequisicaoMudanca());
                        ligacaoRequisicaoMudancaHistoricoItemConfiguracaoDTO.setIdrequisicaomudancaitemconfiguracao(requisicaoMudancaItemConfiguracaoDTO
                                .getIdRequisicaoMudancaItemConfiguracao());
                        ligacaoRequisicaoMudancaHistoricoItemConfiguracaoDTO.setIdHistoricoMudanca(historicoMudancaDTO.getIdHistoricoMudanca());
                        ligacaoRequisicaoMudancaItemConfiguracaoDao.create(ligacaoRequisicaoMudancaHistoricoItemConfiguracaoDTO);
                        ligacaoRequisicaoMudancaHistoricoItemConfiguracaoDTO = new LigacaoRequisicaoMudancaHistoricoItemConfiguracaoDTO();
                    }
                }

                historicoMudancaDTO.setListRequisicaoMudancaServicoDTO(this.listarServico(historicoMudancaDTO));
                if (historicoMudancaDTO.getListRequisicaoMudancaServicoDTO() != null) {
                    for (final RequisicaoMudancaServicoDTO requisicaoMudancaServicoDTO : historicoMudancaDTO.getListRequisicaoMudancaServicoDTO()) {
                        ligacaoRequisicaoMudancaHistoricoServicoDTO.setIdRequisicaoMudanca(requisicaoMudancaServicoDTO.getIdRequisicaoMudanca());
                        ligacaoRequisicaoMudancaHistoricoServicoDTO.setIdrequisicaomudancaservico(requisicaoMudancaServicoDTO.getIdRequisicaoMudancaServico());
                        ligacaoRequisicaoMudancaHistoricoServicoDTO.setIdHistoricoMudanca(historicoMudancaDTO.getIdHistoricoMudanca());
                        ligacaoRequisicaoMudancaServicoDao.create(ligacaoRequisicaoMudancaHistoricoServicoDTO);
                        ligacaoRequisicaoMudancaHistoricoServicoDTO = new LigacaoRequisicaoMudancaHistoricoServicoDTO();
                    }
                }

                historicoMudancaDTO.setListProblemaMudancaDTO(this.listarProblema(historicoMudancaDTO));
                if (historicoMudancaDTO.getListProblemaMudancaDTO() != null) {
                    for (final ProblemaMudancaDTO problemaMudancaDTO : historicoMudancaDTO.getListProblemaMudancaDTO()) {
                        ligacaoRequisicaoMudancaHistoricoProblemaDTO.setIdRequisicaoMudanca(problemaMudancaDTO.getIdRequisicaoMudanca());
                        ligacaoRequisicaoMudancaHistoricoProblemaDTO.setIdProblemaMudanca(problemaMudancaDTO.getIdProblemaMudanca());
                        ligacaoRequisicaoMudancaHistoricoProblemaDTO.setIdHistoricoMudanca(historicoMudancaDTO.getIdHistoricoMudanca());
                        ligacaoRequisicaoMudancaProblemaDao.create(ligacaoRequisicaoMudancaHistoricoProblemaDTO);
                        ligacaoRequisicaoMudancaHistoricoProblemaDTO = new LigacaoRequisicaoMudancaHistoricoProblemaDTO();
                    }
                }

                historicoMudancaDTO.setListGrupoRequisicaoMudancaDTO(this.listarGrupo(historicoMudancaDTO));
                if (historicoMudancaDTO.getListGrupoRequisicaoMudancaDTO() != null) {
                    for (final GrupoRequisicaoMudancaDTO gruporequisicaomudancaDTO : historicoMudancaDTO.getListGrupoRequisicaoMudancaDTO()) {
                        ligacaoRequisicaoMudancaHistoricoGrupoDTO.setIdRequisicaoMudanca(gruporequisicaomudancaDTO.getIdRequisicaoMudanca());
                        ligacaoRequisicaoMudancaHistoricoGrupoDTO.setIdGrupoRequisicaoMudanca(gruporequisicaomudancaDTO.getIdGrupoRequisicaoMudanca());
                        ligacaoRequisicaoMudancaHistoricoGrupoDTO.setIdHistoricoMudanca(historicoMudancaDTO.getIdHistoricoMudanca());
                        ligacaoRequisicaoMudancaGrupoDao.create(ligacaoRequisicaoMudancaHistoricoGrupoDTO);
                        ligacaoRequisicaoMudancaHistoricoGrupoDTO = new LigacaoRequisicaoMudancaHistoricoGrupoDTO();
                    }
                }

                historicoMudancaDTO.setListRequisicaoMudancaRiscoDTO(this.listarRiscos(historicoMudancaDTO));
                if (historicoMudancaDTO.getListRequisicaoMudancaRiscoDTO() != null) {
                    for (final RequisicaoMudancaRiscoDTO riscoMudancaDTO : historicoMudancaDTO.getListRequisicaoMudancaRiscoDTO()) {
                        ligaHistoricoRiscosDTO.setIdRequisicaoMudanca(historicoMudancaDTO.getIdRequisicaoMudanca());
                        ligaHistoricoRiscosDTO.setIdRequisicaoMudancaRisco(riscoMudancaDTO.getIdRequisicaoMudancaRisco());
                        ligaHistoricoRiscosDTO.setIdHistoricoMudanca(historicoMudancaDTO.getIdHistoricoMudanca());
                        ligacaoriscoDao.create(ligaHistoricoRiscosDTO);
                        ligacaoRequisicaoMudancaHistoricoProblemaDTO = new LigacaoRequisicaoMudancaHistoricoProblemaDTO();
                    }
                }

                historicoMudancaDTO.setListLiberacaoMudancaDTO(this.listarLiberacoes(historicoMudancaDTO));
                if (historicoMudancaDTO.getListLiberacaoMudancaDTO() != null && historicoMudancaDTO.getListLiberacaoMudancaDTO().size() > 0) {
                    this.gravarLiberacaoHistorico(historicoMudancaDTO, tc);
                }

                // gravando historico mudancaSolicitacaoServico
                List<RequisicaoMudancaDTO> listSolicitacaoServicosMudanca = new ArrayList<RequisicaoMudancaDTO>();
                listSolicitacaoServicosMudanca = this.listarSolicitacaoServico(historicoMudancaDTO);
                if (listSolicitacaoServicosMudanca != null) {
                    this.gravarSolicitacaoServicoHistoricos(historicoMudancaDTO, listSolicitacaoServicosMudanca, tc);
                }

				// gravando o historico de aprova��o de mudan�a.
                historicoMudancaDTO.setListAprovacaoMudancaDTO(this.listarAprovacoes(historicoMudancaDTO));
                if (historicoMudancaDTO.getListAprovacaoMudancaDTO() != null) {
                    for (final AprovacaoMudancaDTO aprovacaoMudancaDTO : historicoMudancaDTO.getListAprovacaoMudancaDTO()) {
                        aprovacaoMudancaDTO.setIdHistoricoMudanca(historicoMudancaDTO.getIdHistoricoMudanca());
                        aprovacaoMudancaDao.create(aprovacaoMudancaDTO);
                    }
                }

            }

            contatoRequisicaoMudancaDto = this.criarContatoRequisicaoMudanca(requisicaoMudancaDto, tc);
            if (contatoRequisicaoMudancaDto != null) {
                requisicaoMudancaDto.setIdContatoRequisicaoMudanca(contatoRequisicaoMudancaDto.getIdContatoRequisicaoMudanca());
            }

            if (requisicaoMudancaDto != null) {
                if (requisicaoMudancaDto.getIdGrupoAtual() == null) {
                    requisicaoMudancaDto.setIdGrupoAtual(tipoMudancaDto.getIdGrupoExecutor());

                }
                if (requisicaoMudancaDto.getAcaoFluxo().equalsIgnoreCase("E")) {
                    if (requisicaoMudancaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoMudanca.Executada.name())) {
                        if (requisicaoMudancaDto.getFechamento() == null || requisicaoMudancaDto.getFechamento().equalsIgnoreCase("")) {
                            throw new LogicException(this.i18nMessage("citcorpore.comum.informeFechamento"));
                        }
                    }
                }

                if (requisicaoMudancaDto.getStatus() != null && requisicaoMudancaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoMudanca.Cancelada.name())) {
                    if (requisicaoMudancaDto.getFechamento() == null || requisicaoMudancaDto.getFechamento().equalsIgnoreCase("")) {
                        throw new LogicException(this.i18nMessage("citcorpore.comum.informeFechamento"));
                    }
                }

                solicitacaoServicoMudancaDao.deleteByIdMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            }

            if (requisicaoMudancaDto != null) {
                if (requisicaoMudancaDto.getListIdSolicitacaoServico() != null && requisicaoMudancaDto.getListIdSolicitacaoServico().size() > 0) {
                    for (final SolicitacaoServicoDTO solicitacaoServicoDTO : requisicaoMudancaDto.getListIdSolicitacaoServico()) {
                        final SolicitacaoServicoMudancaDTO solicitacaoServicoMudancaDTO = new SolicitacaoServicoMudancaDTO();

                        solicitacaoServicoMudancaDTO.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                        solicitacaoServicoMudancaDTO.setIdSolicitacaoServico(solicitacaoServicoDTO.getIdSolicitacaoServico());
                        solicitacaoServicoMudancaDao.create(solicitacaoServicoMudancaDTO);
                    }
                }

            }
            // adiciona e deleta logicamente os itens da grid de problemas
            if (requisicaoMudancaDto != null) {
                if (requisicaoMudancaDto.getListProblemaMudancaDTO() != null && requisicaoMudancaDto.getListProblemaMudancaDTO().size() > 0) {
                    this.deleteAdicionaTabelaProblema(requisicaoMudancaDto, tc);
                } else {
                    final ProblemaMudancaDAO problemadaoDao = new ProblemaMudancaDAO();
                    final Collection<ProblemaMudancaDTO> ListProblemaMudancaaDTO = problemadaoDao.findByIdMudancaEDataFim(requisicaoMudancaDto.getIdRequisicaoMudanca());
                    if (ListProblemaMudancaaDTO != null && ListProblemaMudancaaDTO.size() > 0) {
                        for (final ProblemaMudancaDTO problemaMudancaDTO : ListProblemaMudancaaDTO) {
                            problemaMudancaDTO.setDataFim(UtilDatas.getDataAtual());
                            problemaMudancaDao.update(problemaMudancaDTO);
                        }
                    }
                }

                if (requisicaoMudancaDto.getListGrupoRequisicaoMudancaDTO() != null && requisicaoMudancaDto.getListGrupoRequisicaoMudancaDTO().size() > 0) {
                    this.deleteAdicionaTabelaGrupo(requisicaoMudancaDto, tc);
                } else {
                    final GrupoRequisicaoMudancaDao gruporequisicaomudancaDao1 = new GrupoRequisicaoMudancaDao();
                    final Collection<GrupoRequisicaoMudancaDTO> ListGrupoRequisicaoMudanca = gruporequisicaomudancaDao1.findByIdMudancaEDataFim(requisicaoMudancaDto
                            .getIdRequisicaoMudanca());
                    if (ListGrupoRequisicaoMudanca != null && ListGrupoRequisicaoMudanca.size() > 0) {
                        for (final GrupoRequisicaoMudancaDTO gruporequisicaomudancaDTO : ListGrupoRequisicaoMudanca) {
                            gruporequisicaomudancaDTO.setDataFim(UtilDatas.getDataAtual());
                            gruporequisicaomudancaDao1.update(gruporequisicaomudancaDTO);
                        }
                    }
                }

                if (requisicaoMudancaDto.getListRequisicaoMudancaRiscoDTO() != null && requisicaoMudancaDto.getListRequisicaoMudancaRiscoDTO().size() > 0) {
                    this.deleteAdicionaTabelaRiscos(requisicaoMudancaDto, tc);
                } else {
                    final RequisicaoMudancaRiscoDao riscosDao = new RequisicaoMudancaRiscoDao();
                    final Collection<RequisicaoMudancaRiscoDTO> ListRiscosMudancaaDTO = riscosDao.findByIdRequisicaoMudancaEDataFim(requisicaoMudancaDto.getIdRequisicaoMudanca());
                    if (ListRiscosMudancaaDTO != null && ListRiscosMudancaaDTO.size() > 0) {
                        for (final RequisicaoMudancaRiscoDTO riscosMudancaDTO : ListRiscosMudancaaDTO) {
                            riscosMudancaDTO.setDataFim(UtilDatas.getDataAtual());
                            riscosMudancaDTO.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                            riscosDao.update(riscosMudancaDTO);
                        }
                    }
                }

                // geber.costa
                liberacaoMudancaDao.deleteByIdMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                if (requisicaoMudancaDto.getListLiberacaoMudancaDTO() != null) {
                    for (final LiberacaoMudancaDTO dto : requisicaoMudancaDto.getListLiberacaoMudancaDTO()) {
                        dto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                        liberacaoMudancaDao.create(dto);
                    }
                }
            }
            List<RequisicaoMudancaServicoDTO> servicosBanco = null;

            RequisicaoMudancaServicoDTO aux = null;
            if (requisicaoMudancaDto != null && requisicaoMudancaDto.getListRequisicaoMudancaServicoDTO() != null) {
				// se n�o existir no banco, cria, caso contr�rio, atualiza
                for (final RequisicaoMudancaServicoDTO requisicaoMudancaServicoDTO : requisicaoMudancaDto.getListRequisicaoMudancaServicoDTO()) {
                    requisicaoMudancaServicoDTO.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());

                    aux = requisicaoMudancaServicoDao.restoreByChaveComposta(requisicaoMudancaServicoDTO);

                    if (aux == null) {
                        requisicaoMudancaServicoDao.create(requisicaoMudancaServicoDTO);
                    } else {
                        requisicaoMudancaServicoDao.update(aux);
                    }
                }

            }
			// confere se existe algo no banco que n�o est� na lista salva, e deleta
            if (requisicaoMudancaDto != null) {
                servicosBanco = requisicaoMudancaServicoDao.listByIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            }
            if (servicosBanco != null) {
                for (final RequisicaoMudancaServicoDTO i : servicosBanco) {
                    if (!this.requisicaoMudancaServicoExisteNaLista(i, requisicaoMudancaDto.getListRequisicaoMudancaServicoDTO())) {
                        i.setDataFim(UtilDatas.getDataAtual());
                        requisicaoMudancaServicoDao.update(i);
                    }
                }
            }

            List<RequisicaoMudancaItemConfiguracaoDTO> icsBanco = null;
            RequisicaoMudancaItemConfiguracaoDTO requisicaoMudancaItemConfiguracaoDTO2 = new RequisicaoMudancaItemConfiguracaoDTO();

            if (requisicaoMudancaDto != null && requisicaoMudancaDto.getListRequisicaoMudancaItemConfiguracaoDTO() != null) {
				// se n�o existir no banco, cria, caso contr�rio, atualiza
                for (final RequisicaoMudancaItemConfiguracaoDTO requisicaoMudancaItemConfiguracaoDTO : requisicaoMudancaDto.getListRequisicaoMudancaItemConfiguracaoDTO()) {

                    requisicaoMudancaItemConfiguracaoDTO.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());

                    requisicaoMudancaItemConfiguracaoDTO2 = requisicaoMudancaItemConfiguracaoDao.restoreByChaveComposta(requisicaoMudancaItemConfiguracaoDTO);

                    if (requisicaoMudancaItemConfiguracaoDTO2 == null) {
                        requisicaoMudancaItemConfiguracaoDao.create(requisicaoMudancaItemConfiguracaoDTO);
                    } else {
                        requisicaoMudancaItemConfiguracaoDao.update(requisicaoMudancaItemConfiguracaoDTO2);
                    }
                }
            }
			// confere se existe algo no banco que n�o est� na lista salva, e deleta
            if (requisicaoMudancaDto != null) {
                icsBanco = requisicaoMudancaItemConfiguracaoDao.listByIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            }
            if (icsBanco != null) {
                for (final RequisicaoMudancaItemConfiguracaoDTO i : icsBanco) {
                    if (!this.requisicaoMudancaICExisteNaLista(i, requisicaoMudancaDto.getListRequisicaoMudancaItemConfiguracaoDTO())) {
                        i.setDataFim(UtilDatas.getDataAtual());
                        requisicaoMudancaItemConfiguracaoDao.update(i);
                    }
                }
            }

            // update Responsavel
            if (requisicaoMudancaDto != null && requisicaoMudancaDto.getColResponsaveis() != null && requisicaoMudancaDto.getColResponsaveis().size() > 0) {
                this.deleteAdicionaTabelaResponsavel(requisicaoMudancaDto, tc);
            } else {
                final RequisicaoMudancaResponsavelDao requisicaoMudancaResponsavelDao = new RequisicaoMudancaResponsavelDao();
                final Collection<RequisicaoMudancaResponsavelDTO> responsavel = requisicaoMudancaResponsavelDao.findByIdMudancaEDataFim(requisicaoMudancaDto
                        .getIdRequisicaoMudanca());
                if (responsavel != null && responsavel.size() > 0) {
                    for (final RequisicaoMudancaResponsavelDTO requisicaoLiberacaoResponsavelDTO : responsavel) {
                        requisicaoLiberacaoResponsavelDTO.setDataFim(UtilDatas.getDataAtual());
                        requisicaoMudancaResponsavelDao.update(requisicaoLiberacaoResponsavelDTO);
                    }
                }
            }

            // gravando historico de anexos
            final HistoricoGEDDao historicoGEDDao = new HistoricoGEDDao();
            Collection<HistoricoGEDDTO> colHistoricoGed = new ArrayList<HistoricoGEDDTO>();
            colHistoricoGed = historicoGEDDao.listByIdTabelaAndIdLiberacao(ControleGEDDTO.TABELA_REQUISICAOMUDANCA, requisicaoMudancaDto.getIdRequisicaoMudanca());
            if (colHistoricoGed != null && colHistoricoGed.size() > 0) {
                for (final HistoricoGEDDTO historicoGEDDTO : colHistoricoGed) {
                    historicoGEDDTO.setDataFim(UtilDatas.getDataAtual());
                    historicoGEDDTO.setIdHistoricoMudanca(historicoMudancaDTO.getIdHistoricoMudanca());
                    historicoGEDDao.update(historicoGEDDTO);
                }
            }
            // gravando historico de anexos
            Collection<HistoricoGEDDTO> colHistoricoPlanoReversaoGed = new ArrayList<HistoricoGEDDTO>();
            colHistoricoPlanoReversaoGed = historicoGEDDao
                    .listByIdTabelaAndIdLiberacao(ControleGEDDTO.TABELA_PLANO_REVERSAO_MUDANCA, requisicaoMudancaDto.getIdRequisicaoMudanca());
            if (colHistoricoPlanoReversaoGed != null && colHistoricoPlanoReversaoGed.size() > 0) {
                for (final HistoricoGEDDTO historicoGEDDTO : colHistoricoPlanoReversaoGed) {
                    historicoGEDDTO.setDataFim(UtilDatas.getDataAtual());
                    historicoGEDDTO.setIdHistoricoMudanca(historicoMudancaDTO.getIdHistoricoMudanca());
                    historicoGEDDao.update(historicoGEDDTO);
                }
            }
            
			// gravar anexos de mudan�a
            if (requisicaoMudancaDto != null && requisicaoMudancaDto.getColArquivosUpload() != null) {
                this.gravaInformacoesGED(requisicaoMudancaDto, tc, historicoMudancaDTO);
            }

			// gravar anexos dos planos de reversao de mudan�a
            if (requisicaoMudancaDto != null && requisicaoMudancaDto.getColUploadPlanoDeReversaoGED() != null) {
                this.gravaPlanoDeReversaoGED(requisicaoMudancaDto, tc, historicoMudancaDTO);
            }

            
            if (requisicaoMudancaDto.getStatus().equalsIgnoreCase(Enumerados.SituacaoRequisicaoMudanca.Resolvida.getDescricao())) {
                this.fechaRelacionamentoMudanca(tc, requisicaoMudancaDto);
            }

            if (requisicaoMudancaDto.getRegistroexecucao() != null) {
                final Source source = new Source(requisicaoMudancaDto.getRegistroexecucao());
                requisicaoMudancaDto.setRegistroexecucao(source.getTextExtractor().toString());
            }

            final ExecucaoMudancaServiceEjb execucaoMudancaService = new ExecucaoMudancaServiceEjb();
            if (requisicaoMudancaDto.getIdTarefa() == null) {
                requisicaoMudancaDao.update(requisicaoMudancaDto);
            } else {

                if (requisicaoMudancaDto.getFase() != null && !requisicaoMudancaDto.getFase().equalsIgnoreCase("")) {
                    requisicaoMudancaDao.updateFase(requisicaoMudancaDto.getIdRequisicaoMudanca(), requisicaoMudancaDto.getFase());
                    requisicaoMudancaDao.update(requisicaoMudancaDto);
                } else {
                    if (tipoMudancaDto != null) {
                        requisicaoMudancaDao.update(model);
                    } else {
                        throw new LogicException(this.i18nMessage("requisicaoMudanca.categoriaMudancaNaoLocalizada"));
                    }
                }
                if (requisicaoMudancaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoMudanca.Cancelada.name())
                        || requisicaoMudancaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoMudanca.Rejeitada.name())) {
                    execucaoMudancaService.encerra(requisicaoMudancaDto.getUsuarioDto(), requisicaoMudancaDto, tc);
                } else {
                    execucaoMudancaService.executa(requisicaoMudancaDto, requisicaoMudancaDto.getIdTarefa(), requisicaoMudancaDto.getAcaoFluxo(), tc);
                }

            }

            if (requisicaoMudancaDto.getRegistroexecucao() != null && !requisicaoMudancaDto.getRegistroexecucao().trim().equalsIgnoreCase("")) {
                final OcorrenciaMudancaDao ocorrenciaMudancaDao = new OcorrenciaMudancaDao();
                ocorrenciaMudancaDao.setTransactionControler(tc);
                final OcorrenciaMudancaDTO ocorrenciaMudancaDto = new OcorrenciaMudancaDTO();
                ocorrenciaMudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                ocorrenciaMudancaDto.setDataregistro(UtilDatas.getDataAtual());
                ocorrenciaMudancaDto.setHoraregistro(UtilDatas.formatHoraFormatadaStr(UtilDatas.getHoraAtual()));
                ocorrenciaMudancaDto.setTempoGasto(0);
                ocorrenciaMudancaDto.setDescricao(br.com.centralit.citcorpore.util.Enumerados.CategoriaOcorrencia.Execucao.getDescricao());
                ocorrenciaMudancaDto.setDataInicio(UtilDatas.getDataAtual());
                ocorrenciaMudancaDto.setDataFim(UtilDatas.getDataAtual());
				ocorrenciaMudancaDto.setInformacoesContato("n�o se aplica");
                ocorrenciaMudancaDto.setRegistradopor(requisicaoMudancaDto.getUsuarioDto().getLogin());
                ocorrenciaMudancaDto.setDadosMudanca(new Gson().toJson(requisicaoMudancaDto));
                ocorrenciaMudancaDto.setOrigem(br.com.centralit.citcorpore.util.Enumerados.OrigemOcorrencia.OUTROS.getSigla().toString());
                ocorrenciaMudancaDto.setCategoria(br.com.centralit.citcorpore.util.Enumerados.CategoriaOcorrencia.Criacao.getSigla());
                ocorrenciaMudancaDto.setOcorrencia(requisicaoMudancaDto.getRegistroexecucao());
                ocorrenciaMudancaDao.create(ocorrenciaMudancaDto);
            }

            if (requisicaoMudancaDto.getAcaoFluxo().equalsIgnoreCase("E")) {
                if (requisicaoMudancaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoMudanca.Executada.name())
                        || requisicaoMudancaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoMudanca.Concluida.name())) {
                    this.fechaRelacionamentoMudanca(tc, requisicaoMudancaDto);
                }
            }

            tc.commit();

            if (requisicaoMudancaDto.getDataHoraInicioAgendada() != null) {
                RequisicaoMudanca.salvaGrupoAtvPeriodicaEAgenda(requisicaoMudancaDto, document, request);
            }
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
            throw new ServiceException(e);
        } finally {
            try {
                tc.close();
            } catch (final PersistenceException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    @Override
    public RequisicaoMudancaDTO restoreAll(final Integer idRequisicaoMudanca) throws Exception {
        return this.restoreAll(idRequisicaoMudanca, null);
    }

    public RequisicaoMudancaDTO restoreAll(final Integer idRequisicaoMudanca, final TransactionControler tc) throws Exception {
        final RequisicaoMudancaDao requisicaoDao = this.getDao();
        if (tc != null) {
            requisicaoDao.setTransactionControler(tc);
        }
        RequisicaoMudancaDTO requisicaoDto = new RequisicaoMudancaDTO();
        requisicaoDto.setIdRequisicaoMudanca(idRequisicaoMudanca);
        requisicaoDto = (RequisicaoMudancaDTO) requisicaoDao.restore(requisicaoDto);
        if (requisicaoDto != null && requisicaoDto.getDataHoraInicioAgendada() != null) {
            final Timestamp dataHoraTerminoAgendada = requisicaoDto.getDataHoraInicioAgendada();
            final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            final String horaAgendamentoInicialSTR = format.format(dataHoraTerminoAgendada);
            final String horaInicial = horaAgendamentoInicialSTR.substring(11, 16);
            requisicaoDto.setHoraAgendamentoInicial(horaInicial.trim());
        }
        if (requisicaoDto != null && requisicaoDto.getDataHoraTermino() != null && requisicaoDto.getDataHoraTerminoStr() != null) {
            final Timestamp dataHoraTerminoAgendada = requisicaoDto.getDataHoraTerminoAgendada();
            final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            if (dataHoraTerminoAgendada != null) {
                final String horaAgendamentoFinallSTR = format.format(dataHoraTerminoAgendada);
                final String horaFinal = horaAgendamentoFinallSTR.substring(11, 16);
                requisicaoDto.setHoraAgendamentoFinal(horaFinal.trim());
            }

        }
        if (requisicaoDto != null && requisicaoDto.getDescricao() != null) {
            final Source source = new Source(requisicaoDto.getDescricao());
            requisicaoDto.setDescricao(source.getTextExtractor().toString());
        }

        if (requisicaoDto != null) {
            requisicaoDto.setDataHoraTerminoStr(requisicaoDto.getDataHoraTerminoStr());

            final EmpregadoDTO empregadoDto = new EmpregadoDao().restoreByIdEmpregado(requisicaoDto.getIdSolicitante());
            if (empregadoDto != null) {
                requisicaoDto.setNomeSolicitante(empregadoDto.getNome());
                requisicaoDto.setEmailSolicitante(empregadoDto.getEmail());
            }

            if (requisicaoDto.getIdProprietario() != null) {
                UsuarioDTO usuarioDto = new UsuarioDTO();
                final UsuarioDao usuarioDao = new UsuarioDao();
                // usuarioDto.setIdUsuario(requisicaoDto.getIdProprietario());
                // usuarioDto = (UsuarioDTO) usuarioDao.restore(usuarioDto);
                /**
				 * Motivo: Restaura o usu�rio a partir do idProprietario gravado no banco de dados Autor: flavio.santana Data/Hora: 28/11/2013 17:50
				 */
                usuarioDto = usuarioDao.restoreByIdEmpregado(requisicaoDto.getIdProprietario());
                if (usuarioDto != null) {
                    requisicaoDto.setResponsavelAtual(usuarioDto.getLogin());
                }
            }

            if (requisicaoDto.getIdGrupoAtual() != null) {
                final GrupoDao grupoDao = new GrupoDao();
                GrupoDTO grupoDto = new GrupoDTO();
                grupoDto.setIdGrupo(requisicaoDto.getIdGrupoAtual());
                grupoDto = (GrupoDTO) grupoDao.restore(grupoDto);
                if (grupoDto != null) {
                    requisicaoDto.setNomeGrupoAtual(grupoDto.getSigla());
                }
            }

            if (requisicaoDto.getIdGrupoNivel1() != null) {
                final GrupoDao grupoDao = new GrupoDao();
                GrupoDTO grupoDto = new GrupoDTO();
                grupoDto.setIdGrupo(requisicaoDto.getIdGrupoNivel1());
                grupoDto = (GrupoDTO) grupoDao.restore(grupoDto);
                if (grupoDto != null) {
                    requisicaoDto.setNomeGrupoNivel1(grupoDto.getSigla());
                }
            }

            if (requisicaoDto.getIdContatoRequisicaoMudanca() != null) {
                ContatoRequisicaoMudancaDTO contatoRequisicaoMudancaDto = new ContatoRequisicaoMudancaDTO();
                final ContatoRequisicaoMudancaDao contatoRequisicaoMudancaDao = new ContatoRequisicaoMudancaDao();

                if (CITCorporeUtil.SGBD_PRINCIPAL.equalsIgnoreCase("SQLSERVER")) {
                    if (tc != null) {
                        contatoRequisicaoMudancaDao.setTransactionControler(tc);
                    }
                }

                contatoRequisicaoMudancaDto.setIdContatoRequisicaoMudanca(requisicaoDto.getIdContatoRequisicaoMudanca());
                contatoRequisicaoMudancaDto = (ContatoRequisicaoMudancaDTO) contatoRequisicaoMudancaDao.restore(contatoRequisicaoMudancaDto);
                if (contatoRequisicaoMudancaDto != null) {
                    requisicaoDto.setNomeContato(contatoRequisicaoMudancaDto.getNomecontato());
                }
            }
            if (requisicaoDto.getIdTipoMudanca() != null) {
                TipoMudancaDTO tipoMudancaDto = new TipoMudancaDTO();
                final TipoMudancaDAO tipoMudancaDao = new TipoMudancaDAO();

                tipoMudancaDto.setIdTipoMudanca(requisicaoDto.getIdTipoMudanca());
                tipoMudancaDto = (TipoMudancaDTO) tipoMudancaDao.restore(tipoMudancaDto);
                if (tipoMudancaDto != null) {
                    requisicaoDto.setTipo(tipoMudancaDto.getNomeTipoMudanca());
                }
            }

        }
        return this.verificaAtraso(requisicaoDto);
    }

    public RequisicaoMudancaDTO restoreAllReuniao(final Integer idRequisicaoMudanca, final Integer idReuniaoRequisicaoMudanca, final TransactionControler tc) throws Exception {
        final RequisicaoMudancaDao requisicaoDao = this.getDao();
        if (tc != null) {
            requisicaoDao.setTransactionControler(tc);
        }
        RequisicaoMudancaDTO requisicaoDto = new RequisicaoMudancaDTO();
        requisicaoDto.setIdRequisicaoMudanca(idRequisicaoMudanca);
        requisicaoDto = (RequisicaoMudancaDTO) requisicaoDao.restore(requisicaoDto);
        if (requisicaoDto != null && requisicaoDto.getDescricao() != null) {
            final Source source = new Source(requisicaoDto.getDescricao());
            requisicaoDto.setDescricao(source.getTextExtractor().toString());
        }

        if (requisicaoDto != null) {
            requisicaoDto.setDataHoraTerminoStr(requisicaoDto.getDataHoraTerminoStr());

            final EmpregadoDTO empregadoDto = new EmpregadoDao().restoreByIdEmpregado(requisicaoDto.getIdSolicitante());
            if (empregadoDto != null) {
                requisicaoDto.setNomeSolicitante(empregadoDto.getNome());
                requisicaoDto.setEmailSolicitante(empregadoDto.getEmail());
            }

            if (requisicaoDto.getIdGrupoAtual() != null) {
                final GrupoDao grupoDao = new GrupoDao();
                GrupoDTO grupoDto = new GrupoDTO();
                grupoDto.setIdGrupo(requisicaoDto.getIdGrupoAtual());
                grupoDto = (GrupoDTO) grupoDao.restore(grupoDto);
                if (grupoDto != null) {
                    requisicaoDto.setNomeGrupoAtual(grupoDto.getSigla());
                }
            }

            if (requisicaoDto.getIdGrupoNivel1() != null) {
                final GrupoDao grupoDao = new GrupoDao();
                GrupoDTO grupoDto = new GrupoDTO();
                grupoDto.setIdGrupo(requisicaoDto.getIdGrupoNivel1());
                grupoDto = (GrupoDTO) grupoDao.restore(grupoDto);
                if (grupoDto != null) {
                    requisicaoDto.setNomeGrupoNivel1(grupoDto.getSigla());
                }
            }

            if (requisicaoDto.getIdContatoRequisicaoMudanca() != null) {
                ContatoRequisicaoMudancaDTO contatoRequisicaoMudancaDto = new ContatoRequisicaoMudancaDTO();
                final ContatoRequisicaoMudancaDao contatoRequisicaoMudancaDao = new ContatoRequisicaoMudancaDao();
                // contatoRequisicaoMudancaDao.setTransactionControler(tc);
                contatoRequisicaoMudancaDto.setIdContatoRequisicaoMudanca(requisicaoDto.getIdContatoRequisicaoMudanca());
                contatoRequisicaoMudancaDto = (ContatoRequisicaoMudancaDTO) contatoRequisicaoMudancaDao.restore(contatoRequisicaoMudancaDto);
                if (contatoRequisicaoMudancaDto != null) {
                    requisicaoDto.setNomeContato(contatoRequisicaoMudancaDto.getNomecontato());
                }
            }
            if (requisicaoDto.getIdTipoMudanca() != null) {
                TipoMudancaDTO tipoMudancaDto = new TipoMudancaDTO();
                final TipoMudancaDAO tipoMudancaDao = new TipoMudancaDAO();

                tipoMudancaDto.setIdTipoMudanca(requisicaoDto.getIdTipoMudanca());
                tipoMudancaDto = (TipoMudancaDTO) tipoMudancaDao.restore(tipoMudancaDto);
                if (tipoMudancaDto != null) {
                    requisicaoDto.setTipo(tipoMudancaDto.getNomeTipoMudanca());
                }
            }

            final ReuniaoRequisicaoMudancaDAO reuniaoDao = new ReuniaoRequisicaoMudancaDAO();
            ReuniaoRequisicaoMudancaDTO reuniaoDto = new ReuniaoRequisicaoMudancaDTO();
            reuniaoDto.setIdReuniaoRequisicaoMudanca(idReuniaoRequisicaoMudanca);
            reuniaoDto = (ReuniaoRequisicaoMudancaDTO) reuniaoDao.restore(reuniaoDto);
            requisicaoDto.setLocalReuniao(reuniaoDto.getLocalReuniao());
            requisicaoDto.setDataInicio(reuniaoDto.getDataInicio());
            requisicaoDto.setHoraInicio(reuniaoDto.getHoraInicio());
            requisicaoDto.setDuracaoEstimada(reuniaoDto.getDuracaoEstimada());
            requisicaoDto.setDescricao(reuniaoDto.getDescricao());

        }
        return this.verificaAtraso(requisicaoDto);
    }

    public RequisicaoMudancaDTO verificaAtraso(final RequisicaoMudancaDTO requisicaoDto) throws Exception {
        if (requisicaoDto == null) {
            return null;
        }

        long atrasoSLA = 0;

        if (requisicaoDto.getDataHoraTermino() != null) {
            final Timestamp dataHoraLimite = requisicaoDto.getDataHoraTermino();
            Timestamp dataHoraComparacao = UtilDatas.getDataHoraAtual();
            if (requisicaoDto.encerrada()) {
                if (requisicaoDto.getDataHoraConclusao() != null) {
                    dataHoraComparacao = requisicaoDto.getDataHoraConclusao();
                }
            }
            if (dataHoraComparacao.compareTo(dataHoraLimite) > 0) {
                atrasoSLA = UtilDatas.calculaDiferencaTempoEmMilisegundos(dataHoraComparacao, dataHoraLimite) / 1000;
                requisicaoDto.setPrazoHH(0);
                requisicaoDto.setPrazoMM(0);
            }
        }

        requisicaoDto.setAtraso(atrasoSLA);
        return requisicaoDto;
    }

    @Override
    public Collection findBySolictacaoServico(final RequisicaoMudancaDTO bean) throws ServiceException, LogicException {

        try {
            return this.getDao().listProblemaByIdSolicitacao(bean);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<RequisicaoMudancaDTO> obterMudancaCriticos(final Integer idItemConfiguracao) {
        try {
            final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao();
            return requisicaoMudancaDao.listMudancaByIdItemConfiguracao(idItemConfiguracao);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<RequisicaoMudancaDTO> listMudancaByIdItemConfiguracao(final Integer idItemConfiguracao) throws Exception {
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao();

        return requisicaoMudancaDao.listMudancaByIdItemConfiguracao(idItemConfiguracao);
    }

    @Override
    public Collection<RequisicaoMudancaDTO> listaMudancaPorBaseConhecimento(final RequisicaoMudancaDTO mudanca) throws Exception {
        Collection<RequisicaoMudancaDTO> listaMudancaPorBaseConhecimento = null;
        final RequisicaoMudancaDao mudancaDao = this.getDao();
        try {
            listaMudancaPorBaseConhecimento = mudancaDao.listaMudancasPorBaseConhecimento(mudanca);
            if (listaMudancaPorBaseConhecimento != null) {
                for (final RequisicaoMudancaDTO mudancaDTO : listaMudancaPorBaseConhecimento) {

                    final Source source = new Source(mudancaDTO.getDescricao());
                    mudancaDTO.setDescricao(source.getTextExtractor().toString());
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return listaMudancaPorBaseConhecimento;

    }

    @Override
    public Collection<RequisicaoMudancaDTO> quantidadeMudancaPorBaseConhecimento(final RequisicaoMudancaDTO mudanca) throws Exception {
        final RequisicaoMudancaDao mudancaDao = this.getDao();
        return mudancaDao.quantidadeMudancaPorBaseConhecimento(mudanca);
    }

    @Override
    public Collection findByConhecimento(final BaseConhecimentoDTO baseConhecimentoDto) throws ServiceException, LogicException {
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao();

        try {
            return requisicaoMudancaDao.findByConhecimento(baseConhecimentoDto);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public ServicoContratoDTO findByIdContratoAndIdServico(final Integer idContrato, final Integer idServico) throws Exception {
        return null;
    }

    public Collection<RequisicaoMudancaDTO> listRequisicaoMudancaByCriterios(final RequisicaoMudancaDTO requisicaoMudancaDto) throws Exception {
        return null;
    }

    @Override
    public Collection<PesquisaRequisicaoMudancaDTO> listRequisicaoMudancaByCriterios(final PesquisaRequisicaoMudancaDTO pesquisaRequisicaoMudancaDto) throws Exception {

        final Collection<PesquisaRequisicaoMudancaDTO> listRequisicaoMudancaByCriterios = this.getDao().listRequisicaoMudancaByCriterios(pesquisaRequisicaoMudancaDto);

        if (listRequisicaoMudancaByCriterios != null) {
            for (final PesquisaRequisicaoMudancaDTO requisicaoMudanca : listRequisicaoMudancaByCriterios) {

                final Source source = new Source(requisicaoMudanca.getDescricao());

                requisicaoMudanca.setDescricao(source.getTextExtractor().toString());
            }
        }

        return listRequisicaoMudancaByCriterios;
    }

    @Override
    public boolean verificarSeRequisicaoMudancaPossuiTipoMudanca(final Integer idTipoMudanca) throws Exception {
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao();
        return requisicaoMudancaDao.verificarSeRequisicaoMudancaPossuiTipoMudanca(idTipoMudanca);
    }

    /**
     * Verifica se o item existe na lista.
     *
     * @param item
     * @param lista
     * @return
     */
    private boolean requisicaoMudancaServicoExisteNaLista(final RequisicaoMudancaServicoDTO item, final List<RequisicaoMudancaServicoDTO> lista) {
        if (lista == null) {
            return false;
        }
        for (final RequisicaoMudancaServicoDTO l : lista) {
            if (l.equals(item)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Verifica se o item existe na lista.
     *
     * @param item
     * @param lista
     * @return
     */
    private boolean requisicaoMudancaICExisteNaLista(final RequisicaoMudancaItemConfiguracaoDTO item, final List<RequisicaoMudancaItemConfiguracaoDTO> lista) {
        if (lista == null) {
            return false;
        }
        for (final RequisicaoMudancaItemConfiguracaoDTO l : lista) {
            if (l.equals(item)) {
                return true;
            }
        }
        return false;
    }

    private String getStatusAtual(final Integer id) throws ServiceException, Exception {
        RequisicaoMudancaDTO reqMudanca = new RequisicaoMudancaDTO();
        reqMudanca.setIdRequisicaoMudanca(id);
        reqMudanca = (RequisicaoMudancaDTO) this.getDao().restore(reqMudanca);
        final String res = reqMudanca.getStatus();
        return res;

    }

    private RequisicaoMudancaDTO getRequisicaoAtual(final Integer id) throws ServiceException, Exception {
        RequisicaoMudancaDTO reqMudanca = new RequisicaoMudancaDTO();
        reqMudanca.setIdRequisicaoMudanca(id);
        reqMudanca = (RequisicaoMudancaDTO) this.getDao().restore(reqMudanca);
        return reqMudanca;

    }

    @Override
    public void suspende(final UsuarioDTO usuarioDto, final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception {
        final TransactionControler tc = new TransactionControlerImpl(this.getDao().getAliasDB());
        tc.start();
        this.suspende(usuarioDto, requisicaoMudancaDTO, tc);
        tc.commit();
        tc.close();
    }

    public void suspende(final UsuarioDTO usuarioDto, final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) throws Exception {
        final RequisicaoMudancaDTO requisicaoMudancaAuxiliarDto = this.restoreAll(requisicaoMudancaDto.getIdRequisicaoMudanca(), tc);
        requisicaoMudancaAuxiliarDto.setIdJustificativa(requisicaoMudancaDto.getIdJustificativa());
        requisicaoMudancaAuxiliarDto.setComplementoJustificativa(requisicaoMudancaDto.getComplementoJustificativa());
        new ExecucaoMudancaServiceEjb().suspende(usuarioDto, requisicaoMudancaDto, tc);
    }

    @Override
    public void reativa(final UsuarioDTO usuarioDto, final RequisicaoMudancaDTO requisicaoMudancaDto) throws Exception {
        final TransactionControler tc = new TransactionControlerImpl(this.getDao().getAliasDB());
        tc.start();
        this.reativa(usuarioDto, requisicaoMudancaDto, tc);
        tc.commit();
        tc.close();
    }

    public void reativa(final UsuarioDTO usuarioDto, final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) throws Exception {
        final RequisicaoMudancaDTO requisicaoMudancaAuxDto = this.restoreAll(requisicaoMudancaDto.getIdRequisicaoMudanca(), tc);
        new ExecucaoMudancaServiceEjb().reativa(usuarioDto, requisicaoMudancaAuxDto, tc);
    }

    @Override
    public List<RequisicaoMudancaDTO> listMudancaByIdSolicitacao(final RequisicaoMudancaDTO bean) throws Exception {
        return this.getDao().listMudancaByIdSolicitacao(bean);
    }

    @Override
    public boolean validacaoAvancaFluxo(final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) throws Exception {
        final AprovacaoMudancaDTO aprovacaoMudancaDto = new AprovacaoMudancaDTO();
        final AprovacaoMudancaDao dao = new AprovacaoMudancaDao();
        dao.setTransactionControler(tc);
        if (requisicaoMudancaDto.getIdRequisicaoMudanca() != null) {
            aprovacaoMudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            aprovacaoMudancaDto.setVoto("A");
            aprovacaoMudancaDto.setQuantidadeVotoAprovada(dao.quantidadeAprovacaoMudancaPorVotoAprovada(aprovacaoMudancaDto, requisicaoMudancaDto.getIdGrupoComite()));
            aprovacaoMudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            aprovacaoMudancaDto.setVoto("R");
            aprovacaoMudancaDto.setQuantidadeVotoRejeitada(dao.quantidadeAprovacaoMudancaPorVotoRejeitada(aprovacaoMudancaDto, requisicaoMudancaDto.getIdGrupoComite()));
            aprovacaoMudancaDto.setQuantidadeAprovacaoMudanca(dao.quantidadeDeEmpregdosPorGrupo(requisicaoMudancaDto.getIdGrupoComite()));

        }
        if (aprovacaoMudancaDto.getQuantidadeVotoAprovada() > 0) {

            if (aprovacaoMudancaDto.getQuantidadeAprovacaoMudanca().intValue() == aprovacaoMudancaDto.getQuantidadeVotoAprovada()) {
                return true;
            } else {
                if (aprovacaoMudancaDto.getQuantidadeVotoAprovada() >= aprovacaoMudancaDto.getQuantidadeAprovacaoMudanca() / 2 + 1) {
                    return true;
                }
            }
        } else if (aprovacaoMudancaDto.getQuantidadeVotoRejeitada() > 0
                && aprovacaoMudancaDto.getQuantidadeVotoRejeitada() > aprovacaoMudancaDto.getQuantidadeAprovacaoMudanca() / 2) {
            requisicaoMudancaDto.setStatus("Rejeitada");
            return true;
        }

        return false;

    }

    public boolean validacaoAvancaFluxoProposta(final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) throws Exception {
        final AprovacaoPropostaDTO aprovacaoPropostaDto = new AprovacaoPropostaDTO();
        final AprovacaoPropostaDao dao = new AprovacaoPropostaDao();
        dao.setTransactionControler(tc);
        if (requisicaoMudancaDto.getIdRequisicaoMudanca() != null) {
            aprovacaoPropostaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            aprovacaoPropostaDto.setVoto("A");
            aprovacaoPropostaDto.setQuantidadeVotoAprovada(dao.quantidadeAprovacaoPropostaPorVotoAprovada(aprovacaoPropostaDto, requisicaoMudancaDto.getIdGrupoComite()));
            aprovacaoPropostaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            aprovacaoPropostaDto.setVoto("R");
            aprovacaoPropostaDto.setQuantidadeVotoRejeitada(dao.quantidadeAprovacaoPropostaPorVotoAprovada(aprovacaoPropostaDto, requisicaoMudancaDto.getIdGrupoComite()));
            aprovacaoPropostaDto.setQuantidadeAprovacaoProposta(dao.quantidadeDeEmpregdosPorGrupo(requisicaoMudancaDto.getIdGrupoComite()));

        }
        if (aprovacaoPropostaDto.getQuantidadeVotoAprovada() > 0) {

            if (aprovacaoPropostaDto.getQuantidadeAprovacaoProposta().intValue() == aprovacaoPropostaDto.getQuantidadeVotoAprovada()) {
                requisicaoMudancaDto.setVotacaoPropostaAprovadaAux("S");
                requisicaoMudancaDto.setFase("Registrada");
                return true;
            } else if (aprovacaoPropostaDto.getQuantidadeVotoAprovada() >= aprovacaoPropostaDto.getQuantidadeAprovacaoProposta() / 2 + 1) {
                requisicaoMudancaDto.setVotacaoPropostaAprovadaAux("S");
                requisicaoMudancaDto.setFase("Registrada");
                return true;
            }

        } else if (aprovacaoPropostaDto.getQuantidadeVotoRejeitada() > 0
                && aprovacaoPropostaDto.getQuantidadeVotoRejeitada() >= aprovacaoPropostaDto.getQuantidadeAprovacaoProposta() / 2) {
            requisicaoMudancaDto.setVotacaoPropostaAprovadaAux("N");
            requisicaoMudancaDto.setStatus("Rejeitada");
            return true;
        }

        return false;
    }

    @Override
    public String verificaAprovacaoProposta(final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) throws Exception {
        final String aprovado = "aprovado";
        final String reprovado = "reprovado";
        final String aquardando = "aquardando";
        final AprovacaoPropostaDTO aprovacaoPropostaDto = new AprovacaoPropostaDTO();
        final AprovacaoPropostaDao dao = new AprovacaoPropostaDao();
        dao.setTransactionControler(tc);
        if (requisicaoMudancaDto.getIdRequisicaoMudanca() != null) {
            aprovacaoPropostaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            aprovacaoPropostaDto.setVoto("A");
            aprovacaoPropostaDto.setQuantidadeVotoAprovada(dao.quantidadeAprovacaoPropostaPorVotoAprovada(aprovacaoPropostaDto, requisicaoMudancaDto.getIdGrupoComite()));
            aprovacaoPropostaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            aprovacaoPropostaDto.setVoto("R");
            aprovacaoPropostaDto.setQuantidadeVotoRejeitada(dao.quantidadeAprovacaoPropostaPorVotoAprovada(aprovacaoPropostaDto, requisicaoMudancaDto.getIdGrupoComite()));
            aprovacaoPropostaDto.setQuantidadeAprovacaoProposta(dao.quantidadeDeEmpregdosPorGrupo(requisicaoMudancaDto.getIdGrupoComite()));

        }

        if (aprovacaoPropostaDto.getQuantidadeVotoAprovada() > 0) {
            if (aprovacaoPropostaDto.getQuantidadeAprovacaoProposta().intValue() == aprovacaoPropostaDto.getQuantidadeVotoAprovada()) {
                return aprovado;
            }

            if (aprovacaoPropostaDto.getQuantidadeVotoAprovada() >= aprovacaoPropostaDto.getQuantidadeAprovacaoProposta() / 2 + 1) {
                return aprovado;
            }

            if (aprovacaoPropostaDto.getQuantidadeVotoRejeitada() > aprovacaoPropostaDto.getQuantidadeAprovacaoProposta() / 2) {
                return reprovado;
            }
        }
        return aquardando;
    }

    @Override
    public String verificaAprovacaoMudanca(final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) throws Exception {
        final String aprovado = "aprovado";
        final String reprovado = "reprovado";
        final String aquardando = "aquardando";
        final AprovacaoMudancaDTO aprovacaoMudancaDto = new AprovacaoMudancaDTO();
        final AprovacaoMudancaDao dao = new AprovacaoMudancaDao();
        dao.setTransactionControler(tc);
        if (requisicaoMudancaDto.getIdRequisicaoMudanca() != null) {
            aprovacaoMudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            aprovacaoMudancaDto.setVoto("A");
            aprovacaoMudancaDto.setQuantidadeVotoAprovada(dao.quantidadeAprovacaoMudancaPorVotoAprovada(aprovacaoMudancaDto, requisicaoMudancaDto.getIdGrupoComite()));
            aprovacaoMudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            aprovacaoMudancaDto.setVoto("R");
            aprovacaoMudancaDto.setQuantidadeVotoRejeitada(dao.quantidadeAprovacaoMudancaPorVotoRejeitada(aprovacaoMudancaDto, requisicaoMudancaDto.getIdGrupoComite()));
            aprovacaoMudancaDto.setQuantidadeAprovacaoMudanca(dao.quantidadeDeEmpregdosPorGrupo(requisicaoMudancaDto.getIdGrupoComite()));
        }

        if (aprovacaoMudancaDto.getQuantidadeVotoAprovada() > 0) {
            if (aprovacaoMudancaDto.getQuantidadeAprovacaoMudanca().intValue() == aprovacaoMudancaDto.getQuantidadeVotoAprovada()) {
                return aprovado;
            }

            if (aprovacaoMudancaDto.getQuantidadeVotoAprovada() >= aprovacaoMudancaDto.getQuantidadeAprovacaoMudanca() / 2 + 1) {
                return aprovado;
            }

            if (aprovacaoMudancaDto.getQuantidadeVotoRejeitada() > aprovacaoMudancaDto.getQuantidadeAprovacaoMudanca() / 2) {
                return reprovado;

            }
        }
        return aquardando;
    }

    public void gravaInformacoesGED(final RequisicaoMudancaDTO requisicaomudacaDTO, final TransactionControler tc, final HistoricoMudancaDTO historicoMudancaDTO) throws Exception {
        final Collection<UploadDTO> colArquivosUpload = requisicaomudacaDTO.getColArquivosUpload();
        final HistoricoGEDDTO historicoGEDDTO = new HistoricoGEDDTO();
        final HistoricoGEDDao historicoGEDDao = new HistoricoGEDDao();

        // Setando a transaction no GED
        final ControleGEDDao controleGEDDao = new ControleGEDDao();
        if (tc != null) {
            controleGEDDao.setTransactionControler(tc);
            historicoGEDDao.setTransactionControler(tc);
        }

        String PRONTUARIO_GED_DIRETORIO = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.GedDiretorio, "");
        if (PRONTUARIO_GED_DIRETORIO == null || PRONTUARIO_GED_DIRETORIO.trim().equalsIgnoreCase("")) {
            PRONTUARIO_GED_DIRETORIO = "";
        }

        if (PRONTUARIO_GED_DIRETORIO.equalsIgnoreCase("")) {
            PRONTUARIO_GED_DIRETORIO = Constantes.getValue("DIRETORIO_GED");
        }

        if (PRONTUARIO_GED_DIRETORIO == null || PRONTUARIO_GED_DIRETORIO.equalsIgnoreCase("")) {
            PRONTUARIO_GED_DIRETORIO = "/ged";
        }
        String PRONTUARIO_GED_INTERNO = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.GedInterno, "S");
        if (PRONTUARIO_GED_INTERNO == null) {
            PRONTUARIO_GED_INTERNO = "S";
        }
        String prontuarioGedInternoBancoDados = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.GedInternoBD, "N");
        if (!UtilStrings.isNotVazio(prontuarioGedInternoBancoDados)) {
            prontuarioGedInternoBancoDados = "N";
        }
        String pasta = "";
        if (PRONTUARIO_GED_INTERNO.equalsIgnoreCase("S")) {
            pasta = controleGEDDao.getProximaPastaArmazenar();
            File fileDir = new File(PRONTUARIO_GED_DIRETORIO);
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }
            fileDir = new File(PRONTUARIO_GED_DIRETORIO + "/" + requisicaomudacaDTO.getIdEmpresa());
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }
            fileDir = new File(PRONTUARIO_GED_DIRETORIO + "/" + requisicaomudacaDTO.getIdEmpresa() + "/" + pasta);
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }
        }

		// Grava informa��es do upload principal.
        if (colArquivosUpload != null) {
            for (final UploadDTO upDto : colArquivosUpload) {
            	if(upDto.getTemporario().equals("S")){
	                final UploadDTO uploadDTO = upDto;
	                ControleGEDDTO controleGEDDTO = new ControleGEDDTO();
	
	                Integer idControleGed = uploadDTO.getIdControleGED();
	
	                historicoGEDDTO.setIdRequisicaoMudanca(requisicaomudacaDTO.getIdRequisicaoMudanca());
	                historicoGEDDTO.setIdTabela(ControleGEDDTO.TABELA_REQUISICAOMUDANCA);
	                if (historicoMudancaDTO.getIdHistoricoMudanca() != null) {
	                    historicoGEDDTO.setIdHistoricoMudanca(null);
	                } else {
	                    historicoGEDDTO.setIdHistoricoMudanca(-1);
	                }
	                historicoGEDDao.create(historicoGEDDTO);
	
	                controleGEDDTO.setIdTabela(ControleGEDDTO.TABELA_REQUISICAOMUDANCA);
	                controleGEDDTO.setId(requisicaomudacaDTO.getIdRequisicaoMudanca());
	                controleGEDDTO.setDataHora(UtilDatas.getDataAtual());
	                controleGEDDTO.setDescricaoArquivo(uploadDTO.getDescricao());
	                controleGEDDTO.setExtensaoArquivo(Util.getFileExtension(uploadDTO.getNameFile()));
	                controleGEDDTO.setPasta(pasta);
	                controleGEDDTO.setNomeArquivo(uploadDTO.getNameFile());
	                upDto.setTemporario("S");
	                uploadDTO.setTemporario("S");
	                if (upDto.getTemporario() != null) {
	                    if (!uploadDTO.getTemporario().equalsIgnoreCase("S")) { // Se nao //
	                        continue;
	                    }
	                } else {
	                    continue;
	                }
	
	                // Se utiliza GEDinterno e eh BD
	                if (PRONTUARIO_GED_INTERNO != null && PRONTUARIO_GED_INTERNO.trim().equalsIgnoreCase("S") && "S".equalsIgnoreCase(prontuarioGedInternoBancoDados.trim())) {
	                    controleGEDDTO.setPathArquivo(uploadDTO.getPath()); // Isso vai fazer a gravacao no BD. dento do create abaixo.
	                } else {
	                    controleGEDDTO.setPathArquivo(null);
	                }
	                // esse bloco grava a tabela de historicos de anexos:
	
	                boolean existe = false;
	                if (idControleGed != null) {
	                    final Collection<HistoricoGEDDTO> colAux = historicoGEDDao.listByIdTabelaAndIdLiberacaoEDataFim(ControleGEDDTO.TABELA_REQUISICAOMUDANCA,
	                            requisicaomudacaDTO.getIdRequisicaoMudanca());
	                    if (colAux != null && colAux.size() > 0) {
	                        for (final HistoricoGEDDTO historicoGedDTOAux : colAux) {
	                            if (idControleGed.intValue() == historicoGedDTOAux.getIdControleGed().intValue()) {
	                                idControleGed = historicoGedDTOAux.getIdControleGed();
	                                existe = true;
	                                break;
	                            }
	                        }
	                    }
	                }
	
	                if (!existe) {
	                    controleGEDDTO = (ControleGEDDTO) controleGEDDao.create(controleGEDDTO);
	                    controleGEDDTO.setId(historicoGEDDTO.getIdLigacaoHistoricoGed());
	                    idControleGed = controleGEDDTO.getIdControleGED();
	                }
	                historicoGEDDTO.setIdControleGed(idControleGed);
	                historicoGEDDao.update(historicoGEDDTO);
	
	                // Se utiliza GED interno e nao eh BD
	                if (PRONTUARIO_GED_INTERNO.equalsIgnoreCase("S") && !"S".equalsIgnoreCase(prontuarioGedInternoBancoDados)) {
	                    if (controleGEDDTO != null) {
	                        try {
	                            final File arquivo = new File(PRONTUARIO_GED_DIRETORIO + "/" + requisicaomudacaDTO.getIdEmpresa() + "/" + pasta + "/"
	                                    + controleGEDDTO.getIdControleGED() + "." + Util.getFileExtension(uploadDTO.getNameFile()));
	                            CriptoUtils.encryptFile(uploadDTO.getPath(),
	                                    PRONTUARIO_GED_DIRETORIO + "/" + requisicaomudacaDTO.getIdEmpresa() + "/" + pasta + "/" + controleGEDDTO.getIdControleGED() + ".ged", System
	                                            .getProperties().get("user.dir") + Constantes.getValue("CAMINHO_CHAVE_PUBLICA"));
	                            arquivo.delete();
	                        } catch (final Exception e) {
	
	                        }
	
	                    }
	                }
	            }
            }
        }
    }

    @Override
    public void gravaPlanoDeReversaoGED(final RequisicaoMudancaDTO requisicaomudacaDTO, final TransactionControler tc, final HistoricoMudancaDTO historicoMudancaDTO)
            throws Exception {
        final Collection<UploadDTO> colArquivosUpload = requisicaomudacaDTO.getColUploadPlanoDeReversaoGED();

        // Setando a transaction no GED
        final ControleGEDDao controleGEDDao = new ControleGEDDao();
        if (tc != null) {
            controleGEDDao.setTransactionControler(tc);
        }

        String PRONTUARIO_GED_DIRETORIO = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.GedDiretorio, "");
        if (PRONTUARIO_GED_DIRETORIO == null || PRONTUARIO_GED_DIRETORIO.trim().equalsIgnoreCase("")) {
            PRONTUARIO_GED_DIRETORIO = "";
        }

        if (PRONTUARIO_GED_DIRETORIO.equalsIgnoreCase("")) {
            PRONTUARIO_GED_DIRETORIO = Constantes.getValue("DIRETORIO_GED");
        }

        if (PRONTUARIO_GED_DIRETORIO == null || PRONTUARIO_GED_DIRETORIO.equalsIgnoreCase("")) {
            PRONTUARIO_GED_DIRETORIO = "/ged";
        }
        String PRONTUARIO_GED_INTERNO = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.GedInterno, "S");
        if (PRONTUARIO_GED_INTERNO == null) {
            PRONTUARIO_GED_INTERNO = "S";
        }
        String prontuarioGedInternoBancoDados = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.GedInternoBD, "N");
        if (!UtilStrings.isNotVazio(prontuarioGedInternoBancoDados)) {
            prontuarioGedInternoBancoDados = "N";
        }
        String pasta = "";
        if (PRONTUARIO_GED_INTERNO.equalsIgnoreCase("S")) {
            pasta = controleGEDDao.getProximaPastaArmazenar();
            File fileDir = new File(PRONTUARIO_GED_DIRETORIO);
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }
            fileDir = new File(PRONTUARIO_GED_DIRETORIO + "/" + requisicaomudacaDTO.getIdEmpresa());
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }
            fileDir = new File(PRONTUARIO_GED_DIRETORIO + "/" + requisicaomudacaDTO.getIdEmpresa() + "/" + pasta);
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }
        }

		// Grava informa��es do upload principal.
        if (colArquivosUpload != null) {
            for (final UploadDTO upDto : colArquivosUpload) {
                ControleGEDDTO controleGEDDTO = new ControleGEDDTO();

                Integer idControleGed = upDto.getIdControleGED();

                controleGEDDTO.setIdTabela(ControleGEDDTO.TABELA_PLANO_REVERSAO_MUDANCA);
                controleGEDDTO.setId(historicoMudancaDTO.getIdRequisicaoMudanca());
                controleGEDDTO.setDataHora(UtilDatas.getDataAtual());
                controleGEDDTO.setDescricaoArquivo(upDto.getDescricao());
                controleGEDDTO.setExtensaoArquivo(Util.getFileExtension(upDto.getNameFile()));
                controleGEDDTO.setPasta(pasta);
                controleGEDDTO.setVersao(upDto.getVersao());
                controleGEDDTO.setNomeArquivo(upDto.getNameFile());
                upDto.setTemporario("S");
                upDto.setTemporario("S");
                if (upDto.getTemporario() != null) {
                    if (!upDto.getTemporario().equalsIgnoreCase("S")) { // Se
                                                                            // nao
                                                                            // //
                        continue;
                    }
                } else {
                    continue;
                }

                // Se utiliza GED interno e eh BD
                if (PRONTUARIO_GED_INTERNO.trim().equalsIgnoreCase("S") && "S".equalsIgnoreCase(prontuarioGedInternoBancoDados.trim())) {
                    controleGEDDTO.setPathArquivo(upDto.getPath()); // Isso vai fazer a gravacao no BD. dento do create abaixo.
                } else {
                    controleGEDDTO.setPathArquivo(null);
                }
                // esse bloco grava a tabela de historicos de anexos:

                boolean existe = false;
                if (idControleGed != null) {
                    final Collection<ControleGEDDTO> colAux = controleGEDDao.listByIdTabelaAndID(ControleGEDDTO.TABELA_PLANO_REVERSAO_MUDANCA,
                            requisicaomudacaDTO.getIdRequisicaoMudanca());
                    if (colAux != null && colAux.size() > 0) {
                        for (final ControleGEDDTO controleGedDTOAux : colAux) {
                            if (idControleGed.intValue() == controleGedDTOAux.getIdControleGED().intValue()) {
                                idControleGed = controleGedDTOAux.getIdControleGED();
                                existe = true;
                                break;
                            }
                        }
                    }
                }

                if (!existe) {
                    controleGEDDTO.setId(requisicaomudacaDTO.getIdRequisicaoMudanca());
                    controleGEDDTO = (ControleGEDDTO) controleGEDDao.create(controleGEDDTO);
                    idControleGed = controleGEDDTO.getIdControleGED();
                    upDto.setIdControleGED(idControleGed);
                }

                // Se utiliza GED interno e nao eh BD
                if (PRONTUARIO_GED_INTERNO.equalsIgnoreCase("S") && !"S".equalsIgnoreCase(prontuarioGedInternoBancoDados)) {
                    if (controleGEDDTO != null) {
                        try {
                            final File arquivo = new File(PRONTUARIO_GED_DIRETORIO + "/" + requisicaomudacaDTO.getIdEmpresa() + "/" + pasta + "/"
                                    + controleGEDDTO.getIdControleGED() + "." + Util.getFileExtension(upDto.getNameFile()));
                            CriptoUtils.encryptFile(upDto.getPath(),
                                    PRONTUARIO_GED_DIRETORIO + "/" + requisicaomudacaDTO.getIdEmpresa() + "/" + pasta + "/" + controleGEDDTO.getIdControleGED() + ".ged", System
                                            .getProperties().get("user.dir") + Constantes.getValue("CAMINHO_CHAVE_PUBLICA"));
                            arquivo.delete();
                        } catch (final Exception e) {

                        }

                    }
                }
            }
        }

		final Collection<ControleGEDDTO> colAnexo = controleGEDDao.listByIdTabelaAndIdBaseConhecimento(ControleGEDDTO.TABELA_PLANO_REVERSAO_MUDANCA, requisicaomudacaDTO.getIdRequisicaoMudanca());
		if (colAnexo != null) {
			UploadDTO uploadDTO;
			boolean excluirAnexoPlanoReversao;
			for (final ControleGEDDTO dtoGed : colAnexo) {
				excluirAnexoPlanoReversao = true;
				for (final Object element : colArquivosUpload) {
					uploadDTO = (UploadDTO) element;
					if ((uploadDTO.getIdControleGED() != null)&&(uploadDTO.getIdControleGED().intValue() == dtoGed.getIdControleGED().intValue())) {
						excluirAnexoPlanoReversao = false;
						break;
					}
				}
				if(excluirAnexoPlanoReversao){
					controleGEDDao.delete(dtoGed);
				}
			}
		}

    }

    @Override
    public Collection listaQuantidadeMudancaPorImpacto(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception {
    	/**
    	 * Desenvolvedor: ibimon.morais - Data: 17/08/2015 - Hor�rio: 10:48 - ID Citsmart: 176362 - 
    	 * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
    	 */
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS);
        return requisicaoMudancaDao.listaQuantidadeMudancaPorImpacto(requisicaoMudancaDTO);
    }

    @Override
    public Collection listaQuantidadeMudancaPorPeriodo(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception {
    	/**
    	 * Desenvolvedor: ibimon.morais - Data: 17/08/2015 - Hor�rio: 10:48 - ID Citsmart: 176362 - 
    	 * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
    	 */
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS);
        return requisicaoMudancaDao.listaQuantidadeMudancaPorPeriodo(requisicaoMudancaDTO);
    }

    public String negritoHtml(final String string) {
        return "<style isBold=\"true\" pdfFontName=\"Helvetica-Bold\">" + string + "</style>";
    }

    @Override
    public Collection listaIdETituloMudancasPorPeriodo(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception {
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao();
        return requisicaoMudancaDao.listaIdETituloMudancasPorPeriodo(requisicaoMudancaDTO);
    }

    @Override
    public Collection listaQuantidadeMudancaPorProprietario(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception {
    	/**
    	 * Desenvolvedor: ibimon.morais - Data: 17/08/2015 - Hor�rio: 10:48 - ID Citsmart: 176362 - 
    	 * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
    	 */
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS);
        return requisicaoMudancaDao.listaQuantidadeMudancaPorProprietario(requisicaoMudancaDTO);
    }

    @Override
    public Collection listaQuantidadeMudancaPorSolicitante(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception {
    	/**
    	 * Desenvolvedor: ibimon.morais - Data: 17/08/2015 - Hor�rio: 10:48 - ID Citsmart: 176362 - 
    	 * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
    	 */
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS);
        return requisicaoMudancaDao.listaQuantidadeMudancaPorSolicitante(requisicaoMudancaDTO);
    }

    @Override
    public Collection listaQuantidadeMudancaPorStatus(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception {
    	/**
    	 * Desenvolvedor: ibimon.morais - Data: 17/08/2015 - Hor�rio: 10:48 - ID Citsmart: 176362 - 
    	 * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
    	 */
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS);
        return requisicaoMudancaDao.listaQuantidadeMudancaPorStatus(requisicaoMudancaDTO);
    }

    @Override
    public Collection listaQuantidadeMudancaPorUrgencia(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception {
    	/**
    	 * Desenvolvedor: ibimon.morais - Data: 17/08/2015 - Hor�rio: 10:48 - ID Citsmart: 176362 - 
    	 * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
    	 */
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS);
        return requisicaoMudancaDao.listaQuantidadeMudancaPorUrgencia(requisicaoMudancaDTO);
    }

    @Override
    public Collection listaQuantidadeSemAprovacaoPorPeriodo(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception {
    	/**
    	 * Desenvolvedor: ibimon.morais - Data: 17/08/2015 - Hor�rio: 10:48 - ID Citsmart: 176362 - 
    	 * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
    	 */
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS);
        return requisicaoMudancaDao.listaQuantidadeSemAprovacaoPorPeriodo(requisicaoMudancaDTO);
    }

    @Override
    public Collection listaQuantidadeERelacionamentos(final HttpServletRequest request, final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception {
    	/**
    	 * Desenvolvedor: ibimon.morais - Data: 17/08/2015 - Hor�rio: 10:48 - ID Citsmart: 176362 - 
    	 * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
    	 */
        final RequisicaoMudancaDao requisicaoMudancaDao = this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS);
        final List<RequisicaoMudancaDTO> listaMudancas = requisicaoMudancaDao.listaQuantidadeERelacionamentos(requisicaoMudancaDTO);
        return listaMudancas;
    }

    public Timestamp MontardataHoraAgendamentoInicial(final RequisicaoMudancaDTO requisicaoMudancaDto) {
        final Timestamp dataHoraInicio = requisicaoMudancaDto.getDataHoraInicioAgendada();
        final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String horaAgendamentoInicialSTR = format.format(dataHoraInicio);
        horaAgendamentoInicialSTR = horaAgendamentoInicialSTR.substring(0, 11);
        final String horaAgendamentoInicial = requisicaoMudancaDto.getHoraAgendamentoInicial();
        final String dia = horaAgendamentoInicialSTR.substring(0, 2);
        final String mes = horaAgendamentoInicialSTR.substring(3, 5);
        final String ano = horaAgendamentoInicialSTR.substring(6, 10);
        final String dataHoraMontada = ano + "-" + mes + "-" + dia + " " + horaAgendamentoInicial + ":00.0";
        final Timestamp dataHoraInicial = Timestamp.valueOf(dataHoraMontada);

        return dataHoraInicial;
    }

    public Timestamp MontardataHoraAgendamentoFinal(final RequisicaoMudancaDTO requisicaoMudancaDto) {
        final Timestamp dataHoraFim = requisicaoMudancaDto.getDataHoraTerminoAgendada();
        final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        final String horaAgendamentoFinalSTR = format.format(dataHoraFim);
        final String horaAgendamentoFinal = requisicaoMudancaDto.getHoraAgendamentoFinal();
        final String dia = horaAgendamentoFinalSTR.substring(0, 2);
        final String mes = horaAgendamentoFinalSTR.substring(3, 5);
        final String ano = horaAgendamentoFinalSTR.substring(6, 10);
        final String dataHoraMontada = ano + "-" + mes + "-" + dia + " " + horaAgendamentoFinal + ":00.0";
        final Timestamp dataHoraFinal = Timestamp.valueOf(dataHoraMontada);

        return dataHoraFinal;
    }

    public void calculaTempoAtraso(final RequisicaoMudancaDTO requisicaoMudancaDto) throws Exception {
        requisicaoMudancaDto.setPrazoHH(0);
        requisicaoMudancaDto.setPrazoMM(0);
        if (requisicaoMudancaDto.getDataHoraInicioAgendada() != null && requisicaoMudancaDto.getDataHoraTerminoAgendada() != null) {
            final Timestamp dataHoraInicioComparacao = requisicaoMudancaDto.getDataHoraInicioAgendada();
            final Timestamp dataHoraFinalComparacao = requisicaoMudancaDto.getDataHoraTerminoAgendada();
            if (dataHoraFinalComparacao.compareTo(dataHoraInicioComparacao) > 0) {
                final long atrasoSLA = UtilDatas.calculaDiferencaTempoEmMilisegundos(dataHoraFinalComparacao, dataHoraInicioComparacao) / 1000;

                final String hora = Util.getHoraStr(new Double(atrasoSLA) / 3600);
                final int tam = hora.length();
                requisicaoMudancaDto.setPrazoHH(new Integer(hora.substring(0, tam - 2)));
                requisicaoMudancaDto.setPrazoMM(new Integer(hora.substring(tam - 2, tam)));
            }
        }
    }

    @Override
	public boolean seHoraInicialMenorQAtual(final RequisicaoMudancaDTO requisicaoMudancaDto) {
        boolean resultado = false;
        final Time horaAtual = UtilDatas.getHoraAtual();
        final Date dataAtual = UtilDatas.getDataAtual();
        String horaAtualStr = horaAtual.toString();
        final String dataAtualStr = dataAtual.toString();
        horaAtualStr = horaAtualStr.substring(0, 5);
        final Timestamp dataHoraInicio = requisicaoMudancaDto.getDataHoraInicioAgendada();
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        final String dataAgendamentoFinalSTR = format.format(dataHoraInicio);
        if (dataAtualStr.equals(dataAgendamentoFinalSTR)) {
            String horaAgendamentoInicial = requisicaoMudancaDto.getHoraAgendamentoInicial();
            horaAgendamentoInicial = horaAgendamentoInicial.replaceAll(":", "");
            horaAtualStr = horaAtualStr.replaceAll(":", "");
            final int horaAtualInt = Integer.parseInt(horaAtualStr);
            final int horaAgendamentoInicialInt = Integer.parseInt(horaAgendamentoInicial);
            if (horaAgendamentoInicialInt < horaAtualInt) {
                resultado = true;
            }
        }
        return resultado;
    }

    @Override
	public boolean seHoraFinalMenorQAtual(final RequisicaoMudancaDTO requisicaoMudancaDto) {
        boolean resultado = false;
        final Time horaAtual = UtilDatas.getHoraAtual();
        final Date dataAtual = UtilDatas.getDataAtual();
        String horaAtualStr = horaAtual.toString();
        final String dataAtualStr = dataAtual.toString();
        horaAtualStr = horaAtualStr.substring(0, 5);
        final Timestamp dataHoraFim = requisicaoMudancaDto.getDataHoraTerminoAgendada();
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        final String dataAgendamentoFinalSTR = format.format(dataHoraFim);
        if (dataAtualStr.equals(dataAgendamentoFinalSTR)) {
            String horaAgendamentoFim = requisicaoMudancaDto.getHoraAgendamentoFinal();
            horaAgendamentoFim = horaAgendamentoFim.replaceAll(":", "");
            horaAtualStr = horaAtualStr.replaceAll(":", "");
            final int horaAtualInt = Integer.parseInt(horaAtualStr);
            final int horaAgendamentoFimInt = Integer.parseInt(horaAgendamentoFim);
            if (horaAgendamentoFimInt < horaAtualInt) {
                resultado = true;
            }
        }
        return resultado;
    }

    @Override
	public boolean seHoraFinalMenorQHoraInicial(final RequisicaoMudancaDTO requisicaoMudancaDto) {
        boolean resultado = false;
        final Time horaAtual = UtilDatas.getHoraAtual();
        final Date dataAtual = UtilDatas.getDataAtual();
        String horaAtualStr = horaAtual.toString();
        final String dataAtualStr = dataAtual.toString();
        horaAtualStr = horaAtualStr.substring(0, 5);
        final Timestamp dataHoraInicio = requisicaoMudancaDto.getDataHoraInicioAgendada();
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        final String dataAgendamentoInicialSTR = format.format(dataHoraInicio);
        final Timestamp dataHoraFim = requisicaoMudancaDto.getDataHoraTerminoAgendada();
        final String dataAgendamentoFinalSTR = format.format(dataHoraFim);
        String horaAgendamentoFim = requisicaoMudancaDto.getHoraAgendamentoFinal();
        String horaAgendamentoInicial = requisicaoMudancaDto.getHoraAgendamentoInicial();
        horaAgendamentoInicial = horaAgendamentoInicial.replaceAll(":", "");
        horaAgendamentoFim = horaAgendamentoFim.replaceAll(":", "");
        final int horaInicioInt = Integer.parseInt(horaAgendamentoInicial);
        final int horaFimInt = Integer.parseInt(horaAgendamentoFim);
        if (dataAtualStr.equals(dataAgendamentoFinalSTR) && dataAtualStr.equals(dataAgendamentoInicialSTR)) {
            if (horaInicioInt > horaFimInt) {
                resultado = true;
            }
        }
        if (dataAgendamentoInicialSTR.equals(dataAgendamentoFinalSTR)) {
            if (horaInicioInt > horaFimInt) {
                resultado = true;
            }
        }
        return resultado;
    }

    public boolean validacaoGrupoExecutor(final RequisicaoMudancaDTO requisicaoMudancaDto) throws Exception {
        boolean resultado = false;

        if (requisicaoMudancaDto != null && requisicaoMudancaDto.getIdGrupoAtual() != null && requisicaoMudancaDto.getIdTipoMudanca() != null) {
            final Integer idGrupoExecutor = requisicaoMudancaDto.getIdGrupoAtual();
            final Integer idTipoMudanca = requisicaoMudancaDto.getIdTipoMudanca();

            final PermissoesFluxoService permissoesFluxoService = (PermissoesFluxoService) ServiceLocator.getInstance().getService(PermissoesFluxoService.class, null);

            resultado = permissoesFluxoService.permissaoGrupoExecutor(idTipoMudanca, idGrupoExecutor);
        }
        return resultado;
    }

    @Override
    public String getUrlInformacoesComplementares(final RequisicaoMudancaDTO requisicaoMudancaDto) throws Exception {
        String url = "";
        final TemplateSolicitacaoServicoDao templateDao = new TemplateSolicitacaoServicoDao();
        TemplateSolicitacaoServicoDTO templateDto = null;
        if (templateDto == null) {
            final String idTemplate = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.TEMPLATE_QUESTIONARIO, "13");
            if (idTemplate != null && !idTemplate.equals("")) {
                templateDto = new TemplateSolicitacaoServicoDTO();
                templateDto.setIdTemplate(new Integer(idTemplate));
                templateDto = (TemplateSolicitacaoServicoDTO) templateDao.restore(templateDto);
            }
        }
        if (templateDto != null) {
            url += templateDto.getUrlRecuperacao();
            url += "?";

            url = url.replaceAll("\n", "");
            url = url.replaceAll("\r", "");

            String editar = "S";
            if (requisicaoMudancaDto.getIdRequisicaoMudanca() != null && requisicaoMudancaDto.getIdRequisicaoMudanca().intValue() > 0) {
                url += "idRequisicao=" + requisicaoMudancaDto.getIdRequisicaoMudanca() + "&";
                url += "IdTipoRequisicao=" + Enumerados.TipoRequisicao.LIBERCAO.getId() + "&";
                if (requisicaoMudancaDto.getIdTipoAba() != null) {
                    url += "idTipoAba=" + requisicaoMudancaDto.getIdTipoAba() + "&";
                }
                if (requisicaoMudancaDto.getIdTarefa() == null) {
                    editar = "N";
                } else {
                    url += "idTarefa=" + requisicaoMudancaDto.getIdTarefa() + "&";
                }
            }
            url += "&editar=" + editar;
            
            if ("RO".equalsIgnoreCase(requisicaoMudancaDto.getEditar())) {
            	url += "&isVisualizarMudanca=S";
            } else {
            	url += "&isVisualizarMudanca=N";
            }
            		
        }
        return url;
    }

    @Override
	public boolean verificaPermissaoGrupoCancelar(final Integer idTipoMudan�a, final Integer idGrupo) throws ServiceException, Exception {
        boolean isOk = false;
        final TipoMudancaService tipoMudancaService = (TipoMudancaService) ServiceLocator.getInstance().getService(TipoMudancaService.class, null);
        TipoMudancaDTO tipoMudancaDto = new TipoMudancaDTO();
        final PermissoesFluxoDao permissoesDao = new PermissoesFluxoDao();

		tipoMudancaDto.setIdTipoMudanca(idTipoMudan�a);
        tipoMudancaDto = (TipoMudancaDTO) tipoMudancaService.restore(tipoMudancaDto);
        if (tipoMudancaDto != null) {
            final PermissoesFluxoDTO permissoesDto = permissoesDao.permissaoGrupoCancelar(idGrupo, tipoMudancaDto.getIdTipoFluxo());
            if (permissoesDto != null && permissoesDto.getCancelar() != null && permissoesDto.getCancelar().equalsIgnoreCase("S")) {
                isOk = true;
            }
        }

        return isOk;
    }

    /* ################################################# HISTORICO MUDANCA ################################################# */

    public HistoricoMudancaDTO createHistoricoMudanca(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception {
        final HistoricoMudancaDTO historico = new HistoricoMudancaDTO();
        RequisicaoMudancaDTO requisicaoMudancaDTOAux = requisicaoMudancaDTO;
        final Integer idExecutormodificacao = requisicaoMudancaDTO.getUsuarioDto().getIdEmpregado();
        final RequisicaoMudancaService requisicaoMudancaService = (RequisicaoMudancaService) ServiceLocator.getInstance().getService(RequisicaoMudancaService.class, null);
        requisicaoMudancaDTOAux = (RequisicaoMudancaDTO) requisicaoMudancaService.restore(requisicaoMudancaDTO);
        requisicaoMudancaDTOAux.setAlterarSituacao(requisicaoMudancaDTO.getAlterarSituacao());
        requisicaoMudancaDTOAux.setAcaoFluxo(requisicaoMudancaDTO.getAcaoFluxo());
        Reflexao.copyPropertyValues(requisicaoMudancaDTOAux, historico);
        historico.setIdExecutorModificacao(idExecutormodificacao);
        historico.setRegistroexecucao(requisicaoMudancaDTO.getRegistroexecucao());

		// esse bloco seta as informa��es de contato.
        ContatoRequisicaoMudancaDTO contatoRequisicaoMudancaDTO = new ContatoRequisicaoMudancaDTO();
        final ContatoRequisicaoMudancaService contatoRequisicaoMudancaService = (ContatoRequisicaoMudancaService) ServiceLocator.getInstance().getService(
                ContatoRequisicaoMudancaService.class, null);
        requisicaoMudancaDTO.setIdContatoRequisicaoMudanca(historico.getIdContatoRequisicaoMudanca());
        contatoRequisicaoMudancaDTO = contatoRequisicaoMudancaService.restoreContatosById(requisicaoMudancaDTO.getIdContatoRequisicaoMudanca());
        if (contatoRequisicaoMudancaDTO != null) {
            historico.setNomeContato(contatoRequisicaoMudancaDTO.getNomecontato());
            historico.setEmailSolicitante(contatoRequisicaoMudancaDTO.getEmailcontato());
            historico.setIdContatoRequisicaoMudanca(contatoRequisicaoMudancaDTO.getIdContatoRequisicaoMudanca());
            historico.setIdLocalidade(contatoRequisicaoMudancaDTO.getIdLocalidade());
            historico.setRamal(contatoRequisicaoMudancaDTO.getRamal());
            historico.setTelefoneContato(contatoRequisicaoMudancaDTO.getTelefonecontato());
            historico.setObservacao(contatoRequisicaoMudancaDTO.getObservacao());
        }

        HistoricoMudancaDTO ultVersao = new HistoricoMudancaDTO();
        ultVersao = this.getHistoricoMudancaDao().maxIdHistorico(requisicaoMudancaDTO);
        if (ultVersao.getIdHistoricoMudanca() != null) {
            ultVersao = (HistoricoMudancaDTO) this.getHistoricoMudancaDao().restore(ultVersao);
            historico.setHistoricoVersao(ultVersao.getHistoricoVersao() == null ? 1d : +new BigDecimal(ultVersao.getHistoricoVersao() + 0.1f).setScale(1, BigDecimal.ROUND_DOWN)
                    .floatValue());
        } else {
            historico.setHistoricoVersao(1d);
        }

        historico.setDataHoraModificacao(UtilDatas.getDataHoraAtual());
        if (historico.getIdExecutorModificacao() == null) {
            historico.setIdExecutorModificacao(1);
        } else {
            historico.setIdExecutorModificacao(idExecutormodificacao);
        }

        return historico;
    }

    public HistoricoMudancaDao getHistoricoMudancaDao() throws ServiceException {
        return (HistoricoMudancaDao) this.getHistoricoMudDao();
    }

    protected CrudDAO getHistoricoMudDao() throws ServiceException {
        return new HistoricoMudancaDao();
    }

    public Collection<RequisicaoMudancaResponsavelDTO> listarColResponsaveis(final HistoricoMudancaDTO historicoMudancaDTO) throws ServiceException, Exception {
        final RequisicaoMudancaResponsavelDao requisicaoMudancaResponsavelDao = new RequisicaoMudancaResponsavelDao();
        final Collection<RequisicaoMudancaResponsavelDTO> requisicaoMudancaResponsavelDTOs = requisicaoMudancaResponsavelDao.findByIdMudancaEDataFim(historicoMudancaDTO
                .getIdRequisicaoMudanca());

        return requisicaoMudancaResponsavelDTOs;
    }

    public List<RequisicaoMudancaItemConfiguracaoDTO> listarColItemConfiguracao(final HistoricoMudancaDTO historicoMudancaDTO) throws ServiceException, Exception {
        final RequisicaoMudancaItemConfiguracaoDao requisicaoMudancaItemConfiguracaoDao = new RequisicaoMudancaItemConfiguracaoDao();
        final List<RequisicaoMudancaItemConfiguracaoDTO> requisicaoMudancaItemConfiguracaoDTOs = (List<RequisicaoMudancaItemConfiguracaoDTO>) requisicaoMudancaItemConfiguracaoDao
                .findByIdMudancaEDataFim(historicoMudancaDTO.getIdRequisicaoMudanca());
        return requisicaoMudancaItemConfiguracaoDTOs;
    }

    public List<RequisicaoMudancaServicoDTO> listarServico(final HistoricoMudancaDTO historicoMudancaDTO) throws ServiceException, Exception {
        final RequisicaoMudancaServicoDao requisicaoMudancaServicoDao = new RequisicaoMudancaServicoDao();
        final List<RequisicaoMudancaServicoDTO> requisicaoMudancaServicoDTOs = (List<RequisicaoMudancaServicoDTO>) requisicaoMudancaServicoDao
                .findByIdMudancaEDataFim(historicoMudancaDTO.getIdRequisicaoMudanca());
        return requisicaoMudancaServicoDTOs;
    }

    public List<ProblemaMudancaDTO> listarProblema(final HistoricoMudancaDTO historicoMudancaDTO) throws ServiceException, Exception {

        final ProblemaMudancaDAO problemaMudancaDAO = new ProblemaMudancaDAO();
        final List<ProblemaMudancaDTO> problemaMudancaDTOs = (List<ProblemaMudancaDTO>) problemaMudancaDAO.findByIdMudancaEDataFim(historicoMudancaDTO.getIdRequisicaoMudanca());
        return problemaMudancaDTOs;
    }

    public List<GrupoRequisicaoMudancaDTO> listarGrupo(final HistoricoMudancaDTO historicoMudancaDTO) throws ServiceException, Exception {

        final GrupoRequisicaoMudancaDao grupoRequisicaoMudancaDAO = new GrupoRequisicaoMudancaDao();
        final List<GrupoRequisicaoMudancaDTO> grupoRequisicaoMudancaDTOs = (List<GrupoRequisicaoMudancaDTO>) grupoRequisicaoMudancaDAO.findByIdMudancaEDataFim(historicoMudancaDTO
                .getIdRequisicaoMudanca());
        return grupoRequisicaoMudancaDTOs;
    }

    public List<RequisicaoMudancaRiscoDTO> listarRiscos(final HistoricoMudancaDTO historicoMudancaDTO) throws ServiceException, Exception {

        final RequisicaoMudancaRiscoDao riscoDao = new RequisicaoMudancaRiscoDao();
        final List<RequisicaoMudancaRiscoDTO> problemaMudancaDTOs = (List<RequisicaoMudancaRiscoDTO>) riscoDao.findByIdRequisicaoMudancaEDataFim(historicoMudancaDTO
                .getIdRequisicaoMudanca());
        return problemaMudancaDTOs;
    }

    public List<AprovacaoMudancaDTO> listarAprovacoes(final HistoricoMudancaDTO historicoMudancaDTO) throws ServiceException, Exception {

        final AprovacaoMudancaDao aprovacaoDao = new AprovacaoMudancaDao();
        final List<AprovacaoMudancaDTO> aprovacaoMudancaDTOs = (List<AprovacaoMudancaDTO>) aprovacaoDao.listaAprovacaoMudancaPorIdRequisicaoMudancaEHistorico(
                historicoMudancaDTO.getIdRequisicaoMudanca(), null, null);
        return aprovacaoMudancaDTOs;
    }

    public List<LiberacaoMudancaDTO> listarLiberacoes(final HistoricoMudancaDTO historicoMudancaDTO) throws ServiceException, Exception {

        final LiberacaoMudancaDao liberacaoMudancaDao = new LiberacaoMudancaDao();
        final List<LiberacaoMudancaDTO> liberacaoMudancaDTOs = (List<LiberacaoMudancaDTO>) liberacaoMudancaDao.findByIdRequisicaoMudanca(historicoMudancaDTO.getIdLiberacao(),
                historicoMudancaDTO.getIdRequisicaoMudanca());
        return liberacaoMudancaDTOs;
    }

    public List<RequisicaoMudancaDTO> listarSolicitacaoServico(final HistoricoMudancaDTO historicoMudancaDTO) throws ServiceException, Exception {

        final RequisicaoMudancaDao mudancaDao = this.getDao();
        final List<RequisicaoMudancaDTO> solicitacaoMudancaDTOs = (List<RequisicaoMudancaDTO>) mudancaDao.findByIdRequisicaoMudancaEDataFim(historicoMudancaDTO
                .getIdRequisicaoMudanca());
        return solicitacaoMudancaDTOs;
    }

    public void deleteAdicionaTabelaResponsavel(final RequisicaoMudancaDTO requisicaoMudancaDTO, final TransactionControler tc) throws Exception {
        Collection<RequisicaoMudancaResponsavelDTO> colResponsavelBanco = new ArrayList<RequisicaoMudancaResponsavelDTO>();
        final RequisicaoMudancaResponsavelDao responsavelDao = new RequisicaoMudancaResponsavelDao();
        colResponsavelBanco = responsavelDao.findByIdMudancaEDataFim(requisicaoMudancaDTO.getIdRequisicaoMudanca());
        responsavelDao.setTransactionControler(tc);
        boolean grava = false;
        boolean exclui = false;
        int idResp1 = 0;
        int idResp2 = 0;
        if (colResponsavelBanco == null || colResponsavelBanco.size() == 0) {
            for (final RequisicaoMudancaResponsavelDTO requisicaoMudancaResponsavelDTO : requisicaoMudancaDTO.getColResponsaveis()) {
                requisicaoMudancaResponsavelDTO.setIdRequisicaoMudanca(requisicaoMudancaDTO.getIdRequisicaoMudanca());
                responsavelDao.create(requisicaoMudancaResponsavelDTO);
            }
        } else {
            if (requisicaoMudancaDTO.getColResponsaveis() != null && requisicaoMudancaDTO.getColResponsaveis().size() > 0) {
				// compara o que vem da tela com o que est� no banco se o que estiver na tela for diferente do banco
				// ent�o ele grava poruqe o item n�o existe no banco.
                for (final RequisicaoMudancaResponsavelDTO requisicaoMudancaResponsavelDTO : requisicaoMudancaDTO.getColResponsaveis()) {
                    for (final RequisicaoMudancaResponsavelDTO requisicaoMudancaResponsavelDTO2 : colResponsavelBanco) {
                        idResp1 = requisicaoMudancaResponsavelDTO.getIdResponsavel();
                        idResp2 = requisicaoMudancaResponsavelDTO2.getIdResponsavel();
                        if (idResp1 == idResp2) {
                            grava = false;
                            break;
                        } else {
                            grava = true;
                        }
                    }
                    if (grava) {
                        requisicaoMudancaResponsavelDTO.setIdRequisicaoMudanca(requisicaoMudancaDTO.getIdRequisicaoMudanca());
                        responsavelDao.create(requisicaoMudancaResponsavelDTO);
                    }
                }
				// Compara o que vem do banco com o que est� na tela se o que estiver no banco for diferente do que tem na tela
				// ent�o ele seta a data fim para desabilitar no banco.
                if (colResponsavelBanco != null && colResponsavelBanco.size() > 0) {
                    for (final RequisicaoMudancaResponsavelDTO requisicaoMudancaResponsavelDTO : colResponsavelBanco) {
                        for (final RequisicaoMudancaResponsavelDTO requisicaoMudancaResponsavelDTO2 : requisicaoMudancaDTO.getColResponsaveis()) {
                            idResp1 = requisicaoMudancaResponsavelDTO.getIdResponsavel();
                            idResp2 = requisicaoMudancaResponsavelDTO2.getIdResponsavel();
                            if (idResp1 == idResp2) {
                                exclui = false;
                                break;
                            } else {
                                exclui = true;
                                requisicaoMudancaResponsavelDTO.setDataFim(UtilDatas.getDataAtual());
                            }
                        }
                        if (exclui) {
                            responsavelDao.update(requisicaoMudancaResponsavelDTO);
                        }
                    }
                }
            }
        }

    }

    public void deleteAdicionaTabelaProblema(final RequisicaoMudancaDTO requisicaoMudancaDTO, final TransactionControler tc) throws Exception {
        Collection<ProblemaMudancaDTO> colProblemaBanco = new ArrayList<ProblemaMudancaDTO>();
        final ProblemaMudancaDAO problemaDao = new ProblemaMudancaDAO();
        colProblemaBanco = problemaDao.findByIdMudancaEDataFim(requisicaoMudancaDTO.getIdRequisicaoMudanca());
        problemaDao.setTransactionControler(tc);
        boolean grava = false;
        boolean exclui = false;
        int idProblema1 = 0;
        int idProblema2 = 0;
        if (colProblemaBanco == null || colProblemaBanco.size() == 0) {
            for (final ProblemaMudancaDTO problemamudancaDto : requisicaoMudancaDTO.getListProblemaMudancaDTO()) {
                problemamudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDTO.getIdRequisicaoMudanca());
                problemaDao.create(problemamudancaDto);
            }
        } else {
            if (requisicaoMudancaDTO.getListProblemaMudancaDTO() != null && requisicaoMudancaDTO.getListProblemaMudancaDTO().size() > 0) {
				// compara o que vem da tela com o que est� no banco se o que estiver na tela for diferente do banco
				// ent�o ele grava poruqe o item n�o existe no banco.
                for (final ProblemaMudancaDTO problemaMudancaDTO : requisicaoMudancaDTO.getListProblemaMudancaDTO()) {
                    for (final ProblemaMudancaDTO problemaMudancaDTO2 : colProblemaBanco) {
                        idProblema1 = problemaMudancaDTO.getIdProblema();
                        idProblema2 = problemaMudancaDTO2.getIdProblema();
                        if (idProblema1 == idProblema2) {
                            grava = false;
                            break;
                        } else {
                            grava = true;
                        }
                    }
                    if (grava) {
                        problemaMudancaDTO.setIdRequisicaoMudanca(requisicaoMudancaDTO.getIdRequisicaoMudanca());
                        problemaDao.create(problemaMudancaDTO);
                    }
                }
				// Compara o que vem do banco com o que est� na tela se o que estiver no banco for diferente do que tem na tela
				// ent�o ele seta a data fim para desabilitar no banco.
                if (colProblemaBanco != null && colProblemaBanco.size() > 0) {
                    for (final ProblemaMudancaDTO problemaMudancaDTO : colProblemaBanco) {
                        for (final ProblemaMudancaDTO requisicaoMudancaResponsavelDTO2 : requisicaoMudancaDTO.getListProblemaMudancaDTO()) {
                            idProblema1 = problemaMudancaDTO.getIdProblema();
                            idProblema2 = requisicaoMudancaResponsavelDTO2.getIdProblema();
                            if (idProblema1 == idProblema2) {
                                exclui = false;
                                break;
                            } else {
                                exclui = true;
                                problemaMudancaDTO.setDataFim(UtilDatas.getDataAtual());
                            }
                        }
                        if (exclui) {
                            problemaDao.update(problemaMudancaDTO);
                        }
                    }
                }
            }
        }

    }

    public void deleteAdicionaTabelaRiscos(final RequisicaoMudancaDTO requisicaoMudancaDTO, final TransactionControler tc) throws Exception {
        Collection<RequisicaoMudancaRiscoDTO> colRiscosBanco = new ArrayList<RequisicaoMudancaRiscoDTO>();
        final RequisicaoMudancaRiscoDao riscosDao = new RequisicaoMudancaRiscoDao();
        colRiscosBanco = riscosDao.findByIdRequisicaoMudancaEDataFim(requisicaoMudancaDTO.getIdRequisicaoMudanca());
        riscosDao.setTransactionControler(tc);
        boolean grava = false;
        boolean exclui = false;
        int idRisco1 = 0;
        int idRisco2 = 0;
        if (colRiscosBanco == null || colRiscosBanco.size() == 0) {
            for (final RequisicaoMudancaRiscoDTO riscoMudancaDto : requisicaoMudancaDTO.getListRequisicaoMudancaRiscoDTO()) {
                riscoMudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDTO.getIdRequisicaoMudanca());
                riscosDao.create(riscoMudancaDto);
            }
        } else {
            if (requisicaoMudancaDTO.getListRequisicaoMudancaRiscoDTO() != null && requisicaoMudancaDTO.getListRequisicaoMudancaRiscoDTO().size() > 0) {
				// compara o que vem da tela com o que est� no banco se o que estiver na tela for diferente do banco
				// ent�o ele grava poruqe o item n�o existe no banco.
                for (final RequisicaoMudancaRiscoDTO riscoMudancaDTO : requisicaoMudancaDTO.getListRequisicaoMudancaRiscoDTO()) {
                    for (final RequisicaoMudancaRiscoDTO riscoMudancaDTO2 : colRiscosBanco) {
                        idRisco1 = riscoMudancaDTO.getIdRisco();
                        idRisco2 = riscoMudancaDTO2.getIdRisco();
                        if (idRisco1 == idRisco2) {
                            grava = false;
                            break;
                        } else {
                            grava = true;
                        }
                    }
                    if (grava) {
                        riscoMudancaDTO.setIdRequisicaoMudanca(requisicaoMudancaDTO.getIdRequisicaoMudanca());
                        riscosDao.create(riscoMudancaDTO);
                    }
                }
				// Compara o que vem do banco com o que est� na tela se o que estiver no banco for diferente do que tem na tela
				// ent�o ele seta a data fim para desabilitar no banco.
                if (colRiscosBanco != null && colRiscosBanco.size() > 0) {
                    for (final RequisicaoMudancaRiscoDTO riscoMudancaDTO : colRiscosBanco) {
                        for (final RequisicaoMudancaRiscoDTO riscoMudancaDTO2 : requisicaoMudancaDTO.getListRequisicaoMudancaRiscoDTO()) {
                            idRisco1 = riscoMudancaDTO.getIdRisco();
                            idRisco2 = riscoMudancaDTO2.getIdRisco();
                            if (idRisco1 == idRisco2) {
                                exclui = false;
                                break;
                            } else {
                                exclui = true;
                                riscoMudancaDTO.setDataFim(UtilDatas.getDataAtual());
                                riscoMudancaDTO.setIdRequisicaoMudanca(requisicaoMudancaDTO.getIdRequisicaoMudanca());
                            }
                        }
                        if (exclui) {
                            riscosDao.update(riscoMudancaDTO);
                        }
                    }
                }
            }
        }

    }

    public void deleteAdicionaTabelaGrupo(final RequisicaoMudancaDTO requisicaoMudancaDTO, final TransactionControler tc) throws Exception {
        Collection<GrupoRequisicaoMudancaDTO> colGrupoBanco = new ArrayList<GrupoRequisicaoMudancaDTO>();
        final GrupoRequisicaoMudancaDao grupoDao = new GrupoRequisicaoMudancaDao();
        colGrupoBanco = grupoDao.findByIdMudancaEDataFim(requisicaoMudancaDTO.getIdRequisicaoMudanca());
        grupoDao.setTransactionControler(tc);
        boolean grava = false;
        boolean exclui = false;
        int idGrupo1 = 0;
        int idGrupo2 = 0;
        if (colGrupoBanco == null || colGrupoBanco.size() == 0) {
            for (final GrupoRequisicaoMudancaDTO gruporequisicaomudancaDto : requisicaoMudancaDTO.getListGrupoRequisicaoMudancaDTO()) {
                gruporequisicaomudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDTO.getIdRequisicaoMudanca());
                grupoDao.create(gruporequisicaomudancaDto);
            }
        } else {
            if (requisicaoMudancaDTO.getListGrupoRequisicaoMudancaDTO() != null && requisicaoMudancaDTO.getListGrupoRequisicaoMudancaDTO().size() > 0) {
                for (final GrupoRequisicaoMudancaDTO gruporequisicaomudancaDto : requisicaoMudancaDTO.getListGrupoRequisicaoMudancaDTO()) {
                    for (final GrupoRequisicaoMudancaDTO gruporequisicaomudancaDto2 : colGrupoBanco) {
                        idGrupo1 = gruporequisicaomudancaDto.getIdGrupo();
                        idGrupo2 = gruporequisicaomudancaDto2.getIdGrupo();
                        if (idGrupo1 == idGrupo2) {
                            grava = false;
                            break;
                        } else {
                            grava = true;
                        }
                    }
                    if (grava) {
                        gruporequisicaomudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDTO.getIdRequisicaoMudanca());
                        grupoDao.create(gruporequisicaomudancaDto);
                    }
                }

                if (colGrupoBanco != null && colGrupoBanco.size() > 0) {
                    for (final GrupoRequisicaoMudancaDTO gruporequisicaomudancaDto : colGrupoBanco) {
                        for (final GrupoRequisicaoMudancaDTO gruporequisicaomudancaDto2 : requisicaoMudancaDTO.getListGrupoRequisicaoMudancaDTO()) {
                            idGrupo1 = gruporequisicaomudancaDto.getIdGrupo();
                            idGrupo2 = gruporequisicaomudancaDto2.getIdGrupo();
                            if (idGrupo1 == idGrupo2) {
                                exclui = false;
                                break;
                            } else {
                                exclui = true;
                                gruporequisicaomudancaDto.setDataFim(UtilDatas.getDataAtual());
                            }
                        }
                        if (exclui) {
                            grupoDao.update(gruporequisicaomudancaDto);
                        }
                    }
                }
            }
        }

    }

    private void gravarLiberacaoHistorico(final HistoricoMudancaDTO historicoMudancaDTO, final TransactionControler tc) throws ServiceException, Exception {
        final LiberacaoMudancaDao liberacaoMudancaDao = new LiberacaoMudancaDao();

        if (tc != null) {
            liberacaoMudancaDao.setTransactionControler(tc);
        }

        if (historicoMudancaDTO.getListLiberacaoMudancaDTO() != null) {
            // grava no banco os historicos de liberacao.
            for (final LiberacaoMudancaDTO liberacaoMudancaDTO : historicoMudancaDTO.getListLiberacaoMudancaDTO()) {

                liberacaoMudancaDTO.setIdHistoricoMudanca(historicoMudancaDTO.getIdHistoricoMudanca());
                liberacaoMudancaDTO.setIdRequisicaoMudanca(historicoMudancaDTO.getIdRequisicaoMudanca());
                liberacaoMudancaDao.create(liberacaoMudancaDTO);
            }
        }

    }

    // metodo que grava o historico da grid de incidentes/requisicoes
    private void gravarSolicitacaoServicoHistoricos(final HistoricoMudancaDTO historicoMudancaDTO, final List<RequisicaoMudancaDTO> listSolicitacaoServicosMudanca,
            final TransactionControler tc) throws ServiceException, Exception {
        final SolicitacaoServicoMudancaDao solicitacaoServicoMudancaDao = new SolicitacaoServicoMudancaDao();

        if (tc != null) {
            solicitacaoServicoMudancaDao.setTransactionControler(tc);
        }

        if (listSolicitacaoServicosMudanca != null) {
            // grava no banco os historicos de liberacao.
            for (final RequisicaoMudancaDTO solicitacaoMudancaDTO : listSolicitacaoServicosMudanca) {
                final SolicitacaoServicoMudancaDTO solicitacaoServicoMudancaDTO = new SolicitacaoServicoMudancaDTO();

                solicitacaoServicoMudancaDTO.setIdHistoricoMudanca(historicoMudancaDTO.getIdHistoricoMudanca());
                solicitacaoServicoMudancaDTO.setIdRequisicaoMudanca(historicoMudancaDTO.getIdRequisicaoMudanca());
                solicitacaoServicoMudancaDTO.setIdSolicitacaoServico(solicitacaoMudancaDTO.getIdSolicitacaoServico());
                solicitacaoServicoMudancaDao.create(solicitacaoServicoMudancaDTO);
            }
        }
    }

	public void fechaRelacionamentoMudanca(final TransactionControler tc, final RequisicaoMudancaDTO requisicaoMudancaDto) {
		if (tc != null && requisicaoMudancaDto != null && requisicaoMudancaDto.getFecharItensRelacionados() != null && requisicaoMudancaDto.getFecharItensRelacionados().equals("S")) {
			this.fecharSolicitacaoServicoVinculadaMudanca(tc, requisicaoMudancaDto);
			this.fecharProblemaVinculadoMudanca(requisicaoMudancaDto, tc);
			
	        /*
	         * Desenvolvedor: Euler Ramos - Data: 30/11/2015 - Hor�rio: 11:15 - ID Citsmart: 179863- Motivo/Coment�rio: Segundo o usu�rio, n�o se deve excluir o IC no fechamento da mudan�a.
	         */
			//this.fecharItemConfiguracaoVinculadoMudanca(requisicaoMudancaDto, tc);
		}
	}

    public void fecharSolicitacaoServicoVinculadaMudanca(final TransactionControler tc, final RequisicaoMudancaDTO requisicaoMudancaDto) {
        final SolicitacaoServicoMudancaDao solicitacaoServicoMudancaDao = new SolicitacaoServicoMudancaDao();
        try {
            solicitacaoServicoMudancaDao.setTransactionControler(tc);
            final List<SolicitacaoServicoDTO> listSolicitacaoServicoDTO = solicitacaoServicoMudancaDao.listSolicitacaoByIdMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            if (listSolicitacaoServicoDTO != null && listSolicitacaoServicoDTO.size() > 0) {
                for (final SolicitacaoServicoDTO solicitacaoServicoDTO : listSolicitacaoServicoDTO) {
                	
                	/**
					 * Incidente 181016
					 * 
					 * Regra de Negocio definida: Ao fechar um problema com solicitacao de servico relacionado, fechar tambem as solicitacoes 
					 * solucao resposta da solicitacao de servico = fechamento do problema 
					 * usuario que fechou o problema = usuario responsavel pela solicitacao
					 * 
					 * Caminho: \\10.2.1.11\Desenvolvimento\Equipes CDI\Equipe ITSM\RGN
					 * E-mail: RES Solicita��o 181016.msg
					 * 
					 * @author gilberto.nery
					 * @date 13/11/2015
					 * 
					 */
                	
                	solicitacaoServicoDTO.setResposta(requisicaoMudancaDto.getFechamento());
					solicitacaoServicoDTO.setIdUsuarioResponsavelAtual(requisicaoMudancaDto.getUsuarioDto().getIdUsuario());
                	
                    new SolicitacaoServicoServiceEjb().fechaSolicitacaoServicoVinculadaByProblemaOrMudanca(solicitacaoServicoDTO, tc);
                }
            }
        } catch (final ServiceException e) {
            e.printStackTrace();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public void fecharProblemaVinculadoMudanca(final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) {
        final ProblemaMudancaDAO problemaMudancaDAO = new ProblemaMudancaDAO();
        try {
            problemaMudancaDAO.setTransactionControler(tc);
            final List<ProblemaDTO> listProblemaDto = problemaMudancaDAO.listProblemaByIdMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            if (listProblemaDto != null && listProblemaDto.size() > 0) {
                for (final ProblemaDTO problemaDTO : listProblemaDto) {
                    new ProblemaServiceEjb().fechaProblemaERelacionamento(problemaDTO, tc);
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public void fecharItemConfiguracaoVinculadoMudanca(final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) {
        final RequisicaoMudancaItemConfiguracaoDao requisicaoMudancaIcDao = new RequisicaoMudancaItemConfiguracaoDao();
        try {
            requisicaoMudancaIcDao.setTransactionControler(tc);
            final List<ItemConfiguracaoDTO> listItemCofiguracao = requisicaoMudancaIcDao.listItemConfiguracaoByIdMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
            if (listItemCofiguracao != null && listItemCofiguracao.size() > 0) {
                for (final ItemConfiguracaoDTO itemConfiguracaoDTO : listItemCofiguracao) {
                    new ItemConfiguracaoServiceEjb().finalizarItemConfiguracao(itemConfiguracaoDTO, tc);
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean verificarItensRelacionados(final RequisicaoMudancaDTO requisicaoMudancaDto) throws ServiceException, Exception {
        final RequisicaoMudancaItemConfiguracaoDao requisicaoMudancaIcDao = new RequisicaoMudancaItemConfiguracaoDao();
        final ProblemaMudancaDAO problemaMudancaDAO = new ProblemaMudancaDAO();
        final SolicitacaoServicoMudancaDao solicitacaoServicoMudancaDao = new SolicitacaoServicoMudancaDao();

        final List<ProblemaDTO> listProblemaDto = problemaMudancaDAO.listProblemaByIdMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
        final List<SolicitacaoServicoDTO> listSolicitacaoServicoDTO = solicitacaoServicoMudancaDao.listSolicitacaoByIdMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
        final List<ItemConfiguracaoDTO> listItemCofiguracao = requisicaoMudancaIcDao.listItemConfiguracaoByIdMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());

        if (listProblemaDto != null && listProblemaDto.size() > 0) {
            return true;
        } else if (listSolicitacaoServicoDTO != null && listSolicitacaoServicoDTO.size() > 0) {
            return true;
        } else if (listItemCofiguracao != null && listItemCofiguracao.size() > 0) {
            return true;
        }

        return false;
    }

    public void updateTimeAction(final Integer idGrupoRedirect, final Integer idPrioridadeRedirect, final Integer idRequisicaoMudanca) throws ServiceException, LogicException {
        final ExecucaoMudancaServiceEjb execucaoMudancaService = new ExecucaoMudancaServiceEjb();
        final RequisicaoMudancaDao requiscaoMudancaDao = this.getDao();
        final OcorrenciaMudancaDao ocorrenciaMudancaDao = new OcorrenciaMudancaDao();

        final TransactionControler tc = new TransactionControlerImpl(requiscaoMudancaDao.getAliasDB());

        try {
            tc.start();

            // Faz validacao, caso exista.

            requiscaoMudancaDao.setTransactionControler(tc);
            ocorrenciaMudancaDao.setTransactionControler(tc);

            List<RequisicaoMudancaDTO> listaRequisicaoMudanca = new ArrayList<RequisicaoMudancaDTO>();

            RequisicaoMudancaDTO mudancaAuxDto = new RequisicaoMudancaDTO();
            mudancaAuxDto.setIdRequisicaoMudanca(idRequisicaoMudanca);

            listaRequisicaoMudanca = (List<RequisicaoMudancaDTO>) requiscaoMudancaDao.find(mudancaAuxDto);
            if (listaRequisicaoMudanca != null) {
                mudancaAuxDto = listaRequisicaoMudanca.get(0);
            }

            final RequisicaoMudancaDTO requisicaoMudancaDto = new RequisicaoMudancaDTO();

            requisicaoMudancaDto.setIdGrupoAtual(idGrupoRedirect);
            requisicaoMudancaDto.setPrioridade(idPrioridadeRedirect);
            requisicaoMudancaDto.setIdRequisicaoMudanca(idRequisicaoMudanca);

            requiscaoMudancaDao.updateNotNull(requisicaoMudancaDto);
            execucaoMudancaService.direcionaAtendimentoAutomatico(requisicaoMudancaDto, tc);

			final String strOcorr = "\nEscala��o autom�tica.";

            final JustificativaRequisicaoMudancaDTO justificativaDto = new JustificativaRequisicaoMudancaDTO();
            justificativaDto.setIdJustificativaMudanca(requisicaoMudancaDto.getIdJustificativa());
            justificativaDto.setDescricaoJustificativa(requisicaoMudancaDto.getComplementoJustificativa());

            final UsuarioDTO usuarioDTO = new UsuarioDTO();
			usuarioDTO.setLogin("Autom�tico");

            OcorrenciaMudancaServiceEjb.create(requisicaoMudancaDto, null, strOcorr, OrigemOcorrencia.OUTROS, CategoriaOcorrencia.Atualizacao, null,
                    CategoriaOcorrencia.Atualizacao.getDescricao(), usuarioDTO, 0, justificativaDto, tc);

            tc.commit();

        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
            throw new ServiceException(e);
        } finally {
            try {
                tc.close();
            } catch (final PersistenceException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    @Override
	public Collection gravaInformacoesGED(final Collection colArquivosUpload, final int idEmpresa, final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) throws Exception {
		// Setando a transaction no GED
		ControleGEDDao controleGEDDao = new ControleGEDDao();
		if (tc != null) {
			controleGEDDao.setTransactionControler(tc);
		}

		String PRONTUARIO_GED_DIRETORIO = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.GedDiretorio, "");
		if (PRONTUARIO_GED_DIRETORIO == null || PRONTUARIO_GED_DIRETORIO.trim().equalsIgnoreCase("")) {
			PRONTUARIO_GED_DIRETORIO = "";
    }

		if (PRONTUARIO_GED_DIRETORIO.equalsIgnoreCase("")) {
			PRONTUARIO_GED_DIRETORIO = Constantes.getValue("DIRETORIO_GED");
		}

		if (PRONTUARIO_GED_DIRETORIO == null || PRONTUARIO_GED_DIRETORIO.equalsIgnoreCase("")) {
			PRONTUARIO_GED_DIRETORIO = "/ged";
		}
		String PRONTUARIO_GED_INTERNO = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.GedInterno, "S");
		if (PRONTUARIO_GED_INTERNO == null) {
			PRONTUARIO_GED_INTERNO = "S";
		}
		String prontuarioGedInternoBancoDados = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.GedInternoBD, "N");
		if (!UtilStrings.isNotVazio(prontuarioGedInternoBancoDados))
			prontuarioGedInternoBancoDados = "N";
		String pasta = "";
		if (PRONTUARIO_GED_INTERNO.equalsIgnoreCase("S")) {
			pasta = controleGEDDao.getProximaPastaArmazenar();
			File fileDir = new File(PRONTUARIO_GED_DIRETORIO);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			fileDir = new File(PRONTUARIO_GED_DIRETORIO + "/" + idEmpresa);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			fileDir = new File(PRONTUARIO_GED_DIRETORIO + "/" + idEmpresa + "/" + pasta);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
		}

		UploadDTO uploadDTO;
		ControleGEDDTO controleGEDDTO;
		for (Iterator it = colArquivosUpload.iterator(); it.hasNext();) {
			uploadDTO = (UploadDTO) it.next();
			controleGEDDTO = new ControleGEDDTO();
			controleGEDDTO.setIdTabela(ControleGEDDTO.TABELA_REQUISICAOMUDANCA);
			controleGEDDTO.setId(requisicaoMudancaDto.getIdRequisicaoMudanca());
			controleGEDDTO.setDataHora(UtilDatas.getDataAtual());
			controleGEDDTO.setDescricaoArquivo(uploadDTO.getDescricao());
			controleGEDDTO.setExtensaoArquivo(Util.getFileExtension(uploadDTO.getNameFile()));
			controleGEDDTO.setPasta(pasta);
			controleGEDDTO.setNomeArquivo(uploadDTO.getNameFile());

			if (!uploadDTO.getTemporario().equalsIgnoreCase("S")) { // Se nao //
																	// for //
																	// temporario
				continue;
			}

			if (PRONTUARIO_GED_INTERNO.trim().equalsIgnoreCase("S") && "S".equalsIgnoreCase(prontuarioGedInternoBancoDados.trim())) { // Se
				// utiliza
				// GED
				// interno e eh BD
				controleGEDDTO.setPathArquivo(uploadDTO.getPath()); // Isso vai
																	// fazer a
																	// gravacao
																	// no BD.
																	// dento do
																	// create
																	// abaixo.
			} else {
				controleGEDDTO.setPathArquivo(null);
			}
			controleGEDDTO = (ControleGEDDTO) controleGEDDao.create(controleGEDDTO);
			if (controleGEDDTO != null) {
				uploadDTO.setIdControleGED(controleGEDDTO.getIdControleGED());
			}
			if (PRONTUARIO_GED_INTERNO.equalsIgnoreCase("S") && !"S".equalsIgnoreCase(prontuarioGedInternoBancoDados)) { // Se
																															// utiliza
																															// GED
				// interno e nao eh BD
				if (controleGEDDTO != null) {
					try {
						File arquivo = new File(PRONTUARIO_GED_DIRETORIO + "/" + idEmpresa + "/" + pasta + "/" + controleGEDDTO.getIdControleGED() + "."
								+ Util.getFileExtension(uploadDTO.getNameFile()));
						CriptoUtils.encryptFile(uploadDTO.getPath(), PRONTUARIO_GED_DIRETORIO + "/" + idEmpresa + "/" + pasta + "/" + controleGEDDTO.getIdControleGED() + ".ged", System
								.getProperties().get("user.dir") + Constantes.getValue("CAMINHO_CHAVE_PUBLICA"));
						arquivo.delete();
					} catch (Exception e) {

					}

				}
			} /*
			 * else if (!PRONTUARIO_GED_INTERNO.equalsIgnoreCase("S")) { // Se // utiliza // GED // externo // FALTA IMPLEMENTAR!!! }
			 */
		}
		Collection<ControleGEDDTO> colAnexo = controleGEDDao.listByIdTabelaAndIdBaseConhecimento(ControleGEDDTO.TABELA_REQUISICAOMUDANCA, requisicaoMudancaDto.getIdRequisicaoMudanca());
		if (colAnexo != null) {
			for (ControleGEDDTO dtoGed : colAnexo) {
				boolean b = false;
				for (Iterator it = colArquivosUpload.iterator(); it.hasNext();) {
					uploadDTO = (UploadDTO) it.next();
					if (uploadDTO.getIdControleGED() == null) {
						b = true;
						break;
					}
					if (uploadDTO.getIdControleGED().intValue() == dtoGed.getIdControleGED().intValue()) {
						b = true;
					}
					if (b) {
						break;
					}
				}
				if (!b) {
					controleGEDDao.delete(dtoGed);
				}
			}
		}
		return colAnexo;
	}

	// Thiago Fernandes - 01/11/2013 14:06 - Sol. 121468 - Cria��o de metodo para validar se foi ninformado o anxo plano de rever��o, caso n�o tenha deve ser aberta a aba de anxo
	// plano de rever��o.
    @Override
    public boolean planoDeReversaoInformado(final RequisicaoMudancaDTO requisicaoMudancaDto, final HttpServletRequest request) throws Exception {
        boolean planoReversaoInformado = true;
        TipoMudancaDTO tipoMudancaDto = new TipoMudancaDTO();
        final AprovacaoMudancaDao aprovacaoMudancaDao = new AprovacaoMudancaDao();
        final TipoMudancaDAO tipoMudancaDAO = new TipoMudancaDAO();
        final TransactionControler tc = new TransactionControlerImpl(this.getDao().getAliasDB());
        try {
            aprovacaoMudancaDao.setTransactionControler(tc);
            tipoMudancaDAO.setTransactionControler(tc);
            tc.start();
            if ((requisicaoMudancaDto!=null)&&(requisicaoMudancaDto.getIdTipoMudanca() != null)) {
                tipoMudancaDto.setIdTipoMudanca(requisicaoMudancaDto.getIdTipoMudanca());
                tipoMudancaDto = (TipoMudancaDTO) tipoMudancaDAO.restore(tipoMudancaDto);
            }

			// gravando a aprova��o de mudan�a
            if ((requisicaoMudancaDto!=null)&&(requisicaoMudancaDto.getListAprovacaoMudancaDTO() != null) && (requisicaoMudancaDto.getFase()!=null)&& (!requisicaoMudancaDto.getFase().equalsIgnoreCase("Proposta"))) {
                for (final AprovacaoMudancaDTO aprovacaoMudancaDto : requisicaoMudancaDto.getListAprovacaoMudancaDTO()) {
                    aprovacaoMudancaDao.deleteLinha(requisicaoMudancaDto.getIdRequisicaoMudanca(), aprovacaoMudancaDto.getIdEmpregado());
                    aprovacaoMudancaDto.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
                    aprovacaoMudancaDto.setDataHoraInicio(UtilDatas.getDataHoraAtual());
                    aprovacaoMudancaDao.create(aprovacaoMudancaDto);
                }
            }

            if (requisicaoMudancaDto.getAcaoFluxo().equalsIgnoreCase("E") && !requisicaoMudancaDto.getFase().equalsIgnoreCase("Proposta")) {
                if (!requisicaoMudancaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoMudanca.Cancelada.name())) {
                    if (tipoMudancaDto.getExigeAprovacao() != null) {
                        if (!this.validacaoAvancaFluxo(requisicaoMudancaDto, tc)) {
                            throw new LogicException(UtilI18N.internacionaliza(request, "requisicaoMudanca.essaSolicitacaoMudancaNaoFoiAprovada"));
                        }
                    }
                    if (!requisicaoMudancaDto.getStatus().equalsIgnoreCase("Rejeitada")) {
                        final Collection<UploadDTO> arquivosUpados = (Collection<UploadDTO>) request.getSession(true).getAttribute("colUploadPlanoDeReversaoGED");
                        if (arquivosUpados == null || arquivosUpados.size() == 0) {
                            planoReversaoInformado = false;
                        }
                    }
                }

            }
            tc.commit();

        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
            throw new ServiceException(e);
        } finally {
            try {
                tc.close();
            } catch (final PersistenceException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        return planoReversaoInformado;
    }

    @Override
	public Set<AprovacaoPropostaDTO> retornaAprovacoesProposta(RequisicaoMudancaDTO requisicaoMudancaDto,UsuarioDTO usuarioDto,HttpServletRequest request) throws Exception{

        EmpregadoDTO empregadoDto = new EmpregadoDTO();

        GrupoEmpregadoService grupoEmpregadoService = (GrupoEmpregadoService) ServiceLocator.getInstance().getService(GrupoEmpregadoService.class, null);

        EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);

        AprovacaoPropostaService aprovacaoPropostaService = (AprovacaoPropostaService) ServiceLocator.getInstance().getService(AprovacaoPropostaService.class, null);

        Collection<GrupoEmpregadoDTO> listaGrupoEmpregados = null;

        Collection<AprovacaoPropostaDTO> listaAprovacaoProposta = new ArrayList<AprovacaoPropostaDTO>();

        Set<AprovacaoPropostaDTO> setListaAprovacaoProposta= new HashSet<AprovacaoPropostaDTO>();

        if ((requisicaoMudancaDto != null)&&(requisicaoMudancaDto.getIdRequisicaoMudanca() != null)) {

        	listaAprovacaoProposta =  aprovacaoPropostaService.listaAprovacaoPropostaPorIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca(), requisicaoMudancaDto.getIdGrupoComite(),usuarioDto.getIdEmpregado());

}
        
		if (requisicaoMudancaDto != null) {
			if (requisicaoMudancaDto.getIdGrupoComite() != null) {
				listaGrupoEmpregados = grupoEmpregadoService.findByIdGrupo(requisicaoMudancaDto.getIdGrupoComite());
			} else {
				if (requisicaoMudancaDto.getIdGrupoAtual() != null) {
					listaGrupoEmpregados = grupoEmpregadoService.findByIdGrupo(requisicaoMudancaDto.getIdGrupoAtual());
				}
			}
		}

        if(listaAprovacaoProposta != null){
            for (AprovacaoPropostaDTO aprovacaoPropostaDto : listaAprovacaoProposta) {
                setListaAprovacaoProposta.add(aprovacaoPropostaDto);
            }
        }
        if (listaGrupoEmpregados != null) {
            for (GrupoEmpregadoDTO grupoEmpregadoDTO : listaGrupoEmpregados) {
                AprovacaoPropostaDTO aprovacao = new AprovacaoPropostaDTO();
                if(grupoEmpregadoDTO.getIdEmpregado()!=null){
                    empregadoDto.setIdEmpregado(grupoEmpregadoDTO.getIdEmpregado());
                    empregadoDto = (EmpregadoDTO) empregadoService.restore(empregadoDto);
                    aprovacao.setIdEmpregado(empregadoDto.getIdEmpregado());
                    aprovacao.setNomeEmpregado(empregadoDto.getNome());

                    setListaAprovacaoProposta.add(aprovacao);
                }
            }
        }

		if (requisicaoMudancaDto != null) {
			if (requisicaoMudancaDto.getIdProprietario() != null) {
				AprovacaoPropostaDTO aprovacaoDto = new AprovacaoPropostaDTO();
				if (usuarioDto.getIdUsuario().intValue() == requisicaoMudancaDto.getIdProprietario().intValue()) {
					empregadoDto.setIdEmpregado(usuarioDto.getIdEmpregado());
					empregadoDto = (EmpregadoDTO) empregadoService.restore(empregadoDto);
					aprovacaoDto.setIdEmpregado(empregadoDto.getIdEmpregado());
					aprovacaoDto.setNomeEmpregado(empregadoDto.getNome());
					setListaAprovacaoProposta.add(aprovacaoDto);

				}
			}
		}

            for (AprovacaoPropostaDTO aprovacaoPropostaDto : setListaAprovacaoProposta) {
                if (aprovacaoPropostaDto.getIdEmpregado() != null) {
                    if (aprovacaoPropostaDto.getComentario() == null) {
                        aprovacaoPropostaDto.setComentario("");
                    }
                    if (aprovacaoPropostaDto.getDataHoraInicio() == null) {
                        aprovacaoPropostaDto.setDataHoraInicio(UtilDatas.getDataHoraAtual());
                    }
                    if(aprovacaoPropostaDto.getDataHoraVotacao() == null){
                        aprovacaoPropostaDto.setDataHoraVotacao(UtilI18N.internacionaliza(request, "requisicaoMudanca.aindaNaoVotou"));
                    }
                }
            }

        return setListaAprovacaoProposta;
    }    
    
	@Override
	public Set<AprovacaoMudancaDTO> retornaAprovacoesMudanca(RequisicaoMudancaDTO requisicaoMudancaDto, UsuarioDTO usuarioDto, HttpServletRequest request) throws Exception {

		EmpregadoDTO empregadoDto = new EmpregadoDTO();

		GrupoEmpregadoService grupoEmpregadoService = (GrupoEmpregadoService) ServiceLocator.getInstance().getService(GrupoEmpregadoService.class, null);

		EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);

		AprovacaoMudancaService aprovacaoMudancaService = (AprovacaoMudancaService) ServiceLocator.getInstance().getService(AprovacaoMudancaService.class, null);

		Collection<GrupoEmpregadoDTO> listaGrupoEmpregados = null;

		Collection<AprovacaoMudancaDTO> listaAprovacaoMudanca = new ArrayList<AprovacaoMudancaDTO>();

		Set<AprovacaoMudancaDTO> setListaAprovacaoMudanca = new HashSet<AprovacaoMudancaDTO>();

		if ((requisicaoMudancaDto != null) && (requisicaoMudancaDto.getIdRequisicaoMudanca() != null)) {

			listaAprovacaoMudanca = aprovacaoMudancaService.listaAprovacaoMudancaPorIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca(), requisicaoMudancaDto.getIdGrupoComite(),
					usuarioDto.getIdEmpregado());

		}

		if (requisicaoMudancaDto != null) {
			if (requisicaoMudancaDto.getIdGrupoComite() != null) {
				listaGrupoEmpregados = grupoEmpregadoService.findByIdGrupo(requisicaoMudancaDto.getIdGrupoComite());
			} else {
				if (requisicaoMudancaDto.getIdGrupoAtual() != null) {
					listaGrupoEmpregados = grupoEmpregadoService.findByIdGrupo(requisicaoMudancaDto.getIdGrupoAtual());
				}
			}
		}

		if (listaAprovacaoMudanca != null) {
			for (AprovacaoMudancaDTO aprovacaoMudancaDto : listaAprovacaoMudanca) {
				setListaAprovacaoMudanca.add(aprovacaoMudancaDto);
			}
		}
		if (listaGrupoEmpregados != null) {
			for (GrupoEmpregadoDTO grupoEmpregadoDTO : listaGrupoEmpregados) {
				AprovacaoMudancaDTO aprovacao = new AprovacaoMudancaDTO();
				if (grupoEmpregadoDTO.getIdEmpregado() != null) {
					empregadoDto.setIdEmpregado(grupoEmpregadoDTO.getIdEmpregado());
					empregadoDto = (EmpregadoDTO) empregadoService.restore(empregadoDto);
					aprovacao.setIdEmpregado(empregadoDto.getIdEmpregado());
					aprovacao.setNomeEmpregado(empregadoDto.getNome());

					setListaAprovacaoMudanca.add(aprovacao);
				}

			}

		}

		if (requisicaoMudancaDto != null) {
			if (requisicaoMudancaDto.getIdProprietario() != null) {
				AprovacaoMudancaDTO aprovacaoDto = new AprovacaoMudancaDTO();
				if (usuarioDto.getIdUsuario().intValue() == requisicaoMudancaDto.getIdProprietario().intValue()) {
					empregadoDto.setIdEmpregado(usuarioDto.getIdEmpregado());
					empregadoDto = (EmpregadoDTO) empregadoService.restore(empregadoDto);
					aprovacaoDto.setIdEmpregado(empregadoDto.getIdEmpregado());
					aprovacaoDto.setNomeEmpregado(empregadoDto.getNome());
					setListaAprovacaoMudanca.add(aprovacaoDto);

				}
			}
		}

		for (AprovacaoMudancaDTO aprovacaoMudancaDto : setListaAprovacaoMudanca) {
			if (aprovacaoMudancaDto.getIdEmpregado() != null) {
				if (aprovacaoMudancaDto.getComentario() == null) {
					aprovacaoMudancaDto.setComentario("");
				}
				if (aprovacaoMudancaDto.getDataHoraInicio() == null) {
					aprovacaoMudancaDto.setDataHoraInicio(UtilDatas.getDataHoraAtual());
				}
				if (aprovacaoMudancaDto.getDataHoraVotacao() == null) {
					aprovacaoMudancaDto.setDataHoraVotacao(UtilI18N.internacionaliza(request, "requisicaoMudanca.aindaNaoVotou"));
				}
			}
		}

		return setListaAprovacaoMudanca;
	}

	@Override
	public Collection<RequisicaoMudancaServicoDTO> listServicosRequisicaoMudanca(RequisicaoMudancaDTO requisicaoMudancaDTO){
		RequisicaoMudancaServicoDao requisicaoMudancaServicoDao = new RequisicaoMudancaServicoDao();
		return requisicaoMudancaServicoDao.findByIdMudanca(requisicaoMudancaDTO.getIdRequisicaoMudanca());
	}
	
	@Override
	public Collection<SolicitacaoServicoDTO> listaSolicitacoesRequisicaoMudanca(Integer idRequisicaoMudanca){
		SolicitacaoServicoMudancaDao solicitacaoServicoMudancaDao = new SolicitacaoServicoMudancaDao();
		return solicitacaoServicoMudancaDao.listaSolicitacoesRequisicaoMudanca(idRequisicaoMudanca);
	}
}
