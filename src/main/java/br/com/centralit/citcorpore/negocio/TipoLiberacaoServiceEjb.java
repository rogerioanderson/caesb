/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.TipoLiberacaoDTO;
import br.com.centralit.citcorpore.integracao.TipoLiberacaoDAO;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.service.CrudServiceImpl;

@SuppressWarnings({"rawtypes", "unchecked"})
public class TipoLiberacaoServiceEjb extends CrudServiceImpl implements TipoLiberacaoService {

    private TipoLiberacaoDAO dao;

    @Override
    protected TipoLiberacaoDAO getDao() {
        if (dao == null) {
            dao = new TipoLiberacaoDAO();
        }
        return dao;
    }

    @Override
    public Collection findByIdTipoLiberacao(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdTipoLiberacao(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdTipoLiberacao(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdTipoLiberacao(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByNomeTipoLiberacao(final Integer parm) throws Exception {
        return this.getDao().findByNomeTipoLiberacao(parm);
    }

    @Override
    public void deleteByNomeTipoLiberacao(final Integer parm) throws Exception {
        this.getDao().findByNomeTipoLiberacao(parm);
    }

    @Override
    public Collection<TipoLiberacaoDTO> tiposAtivosPorNome(final String nome) {
        final List condicoes = new ArrayList<Condition>();
        condicoes.add(new Condition("nomeTipoLiberacao", "=", nome));
        condicoes.add(new Condition("datafim", "!=", "null"));
        try {
            return this.getDao().findByCondition(condicoes, null);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<TipoLiberacaoDTO>();

    }

    @Override
    public boolean verificarTipoLiberacaoAtivos(final TipoLiberacaoDTO obj) throws Exception {
        return this.getDao().verificarTipoLiberacaoAtivos(obj);
    }

    @Override
    public Collection encontrarPorNomeTipoLiberacao(final TipoLiberacaoDTO obj) throws Exception {
        return this.getDao().encontrarPorNomeTipoLiberacao(obj);
    }

    @Override
    public Collection getAtivos() throws Exception {
        return this.getDao().getAtivos();
    }

}
