/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;
import br.com.centralit.citcorpore.bean.ContadorAcessoDTO;
import br.com.centralit.citcorpore.integracao.ContadorAcessoDao;
import br.com.citframework.service.CrudServiceImpl;

public class ContadorAcessoServiceEjb extends CrudServiceImpl implements ContadorAcessoService {

    private ContadorAcessoDao dao;

    @Override
    protected ContadorAcessoDao getDao() {
        if (dao == null) {
            dao = new ContadorAcessoDao();
        }
        return dao;
    }

    @Override
    public boolean verificarDataHoraDoContadorDeAcesso(final ContadorAcessoDTO contadorDto) throws Exception {
        return this.getDao().verificarDataHoraDoContadorDeAcesso(contadorDto);
    }

    @Override
    public Integer quantidadesDeAcessoPorBaseConhecimnto(final BaseConhecimentoDTO baseConhecimentoDTO) throws Exception {
        return this.getDao().quantidadesDeAcessoPorBaseConhecimnto(baseConhecimentoDTO);
    }

    @Override
    public Integer quantidadesDeAcessoPorPeriodo(final BaseConhecimentoDTO baseConhecimentoDTO) throws Exception {
        Integer quantidadeAcessos = 0;
        try {
            quantidadeAcessos = this.getDao().quantidadesDeAcessoPorPeriodo(baseConhecimentoDTO);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return quantidadeAcessos;
    }

}
