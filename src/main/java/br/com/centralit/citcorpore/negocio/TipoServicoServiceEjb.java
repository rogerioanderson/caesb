/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import br.com.centralit.citcorpore.bean.TipoServicoDTO;
import br.com.centralit.citcorpore.integracao.TipoServicoDao;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilI18N;

@SuppressWarnings("rawtypes")
public class TipoServicoServiceEjb extends CrudServiceImpl implements TipoServicoService {

    private TipoServicoDao dao;

    @Override
    protected TipoServicoDao getDao() {
        if (dao == null) {
            dao = new TipoServicoDao();
        }
        return dao;
    }

    public Collection list(final List ordenacao) throws LogicException, ServiceException {
        return null;
    }

    public Collection list(final String ordenacao) throws LogicException, ServiceException {
        return null;
    }

    @Override
    public boolean verificarSeTipoServicoExiste(final TipoServicoDTO tipoServicoDto) throws PersistenceException, ServiceException {
        return this.getDao().verificarSeTipoServicoExiste(tipoServicoDto);
    }

    /**
     * Metodo responsavel por verificar se existe vinculo entre o tipo de servi�o e cadastro de servi�o
     *
     * @param idTipoServico
     * @author Ezequiel
     * @throws Exception
     * @throws PersistenceException
     * @throws Exception
     * @data 25/11/2014
     */
    @Override
    public boolean existeVinculadoCadastroServico(final Integer idTipoServico) throws PersistenceException, Exception {
        return this.getDao().existeVinculadoCadastroServico(idTipoServico);
    }
    
    /**
     * @author rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
     * @param TipoServicoDTO
     * @since 01/07/2015
     */
    public void substituiNomeTipoServicoQuandoBlank(final TipoServicoDTO tipoServicoDTO, final HttpServletRequest request) {
        try {
            if (StringUtils.isBlank(tipoServicoDTO.getNomeTipoServico())) {
                tipoServicoDTO.setNomeTipoServico(UtilI18N.internacionaliza(request, "comum.SemTitulo"));
            }
        } catch (IllegalArgumentException illegArgExc) {
            illegArgExc.printStackTrace();
        }
    }

}
