/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.TipoMudancaDTO;
import br.com.centralit.citcorpore.integracao.TipoMudancaDAO;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.service.CrudServiceImpl;

@SuppressWarnings({"rawtypes", "unchecked"})
public class TipoMudancaServiceEjb extends CrudServiceImpl implements TipoMudancaService {

    private TipoMudancaDAO dao;

    @Override
    protected TipoMudancaDAO getDao() {
        if (dao == null) {
            dao = new TipoMudancaDAO();
        }
        return dao;
    }

    @Override
    public Collection findByIdTipoMudanca(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdTipoMudanca(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdTipoMudanca(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdTipoMudanca(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByNomeTipoMudanca(final Integer parm) throws Exception {
        return this.getDao().findByNomeTipoMudanca(parm);
    }

    @Override
    public void deleteByNomeTipoMudanca(final Integer parm) throws Exception {
        this.getDao().findByNomeTipoMudanca(parm);
    }

    @Override
    public Collection<TipoMudancaDTO> tiposAtivosPorNome(final String nome) {
        final List condicoes = new ArrayList<Condition>();
        condicoes.add(new Condition("nomeTipoMudanca", "=", nome));
        condicoes.add(new Condition("datafim", "!=", "null"));
        try {
            return this.getDao().findByCondition(condicoes, null);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public boolean verificarTipoMudancaAtivos(final TipoMudancaDTO obj) throws Exception {
        return this.getDao().verificarTipoMudancaAtivos(obj);
    }

    @Override
    public Collection encontrarPorNomeTipoMudanca(final TipoMudancaDTO obj) throws Exception {
        return this.getDao().encontrarPorNomeTipoMudanca(obj);
    }

    @Override
    public Collection getAtivos() throws Exception {
        return this.getDao().getAtivos();
    }

}
