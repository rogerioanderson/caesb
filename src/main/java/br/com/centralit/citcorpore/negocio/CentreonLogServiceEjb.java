/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.sql.Date;
import java.util.Collection;

import br.com.centralit.citcorpore.integracao.CentreonLogDao;
import br.com.citframework.integracao.CrudDAO;
import br.com.citframework.service.CrudServiceImpl;

public class CentreonLogServiceEjb extends CrudServiceImpl implements CentreonLogService {

    private String jndiName;

    @Override
    protected CrudDAO getDao() {
        return new CentreonLogDao(jndiName);
    }

    @Override
    public void setJndiName(final String jndiNameParm) {
        jndiName = jndiNameParm;
    }

    @Override
    public Collection findByHostServiceStatus(final String jndiNameParm, final String hostName, final String serviceName, final String status, final Date dataInicial,
            final Date dataFinal) throws Exception {
        final CentreonLogDao centreonLogDao = new CentreonLogDao(jndiNameParm);
        return centreonLogDao.findByHostServiceStatus(hostName, serviceName, status, dataInicial, dataFinal);
    }

    @Override
    public Collection findByHostServiceStatusAndServiceNull(final String jndiNameParm, final String hostName, final String status, final Date dataInicial, final Date dataFinal)
            throws Exception {
        final CentreonLogDao centreonLogDao = new CentreonLogDao(jndiNameParm);
        return centreonLogDao.findByHostServiceStatusAndServiceNull(hostName, status, dataInicial, dataFinal);
    }

}
