/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart
 */
package br.com.centralit.citcorpore.negocio;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citcorpore.bean.ParametroCorporeDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

/**
 * @author valdoilo.damasceno
 * 
 */
public interface ParametroCorporeService extends CrudService {

	void create(ParametroCorporeDTO parametroBean, HttpServletRequest request) throws ServiceException, LogicException;

	public List<ParametroCorporeDTO> pesquisarParamentro(Integer id, String nomeParametro) throws ServiceException, LogicException, Exception;

	/**
	 * Cria Par�metros do CITSmart de acordo com o Enum ParametroSistema. Esses par�metros n�o podem ser exclu�dos.
	 * 
	 * @throws Exception
	 * @author valdoilo
	 */
//	public void criarParametros() throws Exception;

	/**
	 * Cria e atualiza o HashMap statico de Par�metros do CITSMart.
	 * 
	 * @throws Exception
	 * @author valdoilo.damasceno
	 */
	public void criarParametrosNovos() throws Exception;
	
	/**
     * Carrega o Map com informa��es existentes no banco de dados, visto que isso deve acontecer antes de criar os par�metros novos "criarParametrosNovos()" 
     * para gravar log, caso esteja parametrizado para tal.
     * @author fabio.amorim
     */
	public void carregarMapDeParametros() throws Exception;

	public ParametroCorporeDTO getParamentroAtivo(Integer id) throws Exception;

	/**
	 * Atualiza Par�metros utilizando UpdateNotNull. Informar apenas o ID do Par�metro e o Valor.
	 * 
	 * @param parametroDto
	 * @throws Exception
	 * @author valdoilo.damasceno
	 */
	public void atualizarParametros(ParametroCorporeDTO parametroDto) throws Exception;
	
	public void updateNotNull(IDto dto) throws Exception;

	/**
	 * Atualiza o valor do parametro pelo id 
	 * 
	 */
	public void atualizarParametro(Integer id, String valor)  throws Exception;
	
}
