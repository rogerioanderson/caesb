/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.CriterioAvaliacaoFornecedorDTO;
import br.com.centralit.citcorpore.integracao.CriterioAvaliacaoFornecedorDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

@SuppressWarnings("rawtypes")
public class CriterioAvaliacaoFornecedorServiceEjb extends CrudServiceImpl implements CriterioAvaliacaoFornecedorService {

    private CriterioAvaliacaoFornecedorDao dao;

    @Override
    protected CriterioAvaliacaoFornecedorDao getDao() {
        if (dao == null) {
            dao = new CriterioAvaliacaoFornecedorDao();
        }
        return dao;
    }

    @Override
    public Collection findByIdCriterio(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdCriterio(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdCriterio(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdCriterio(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<CriterioAvaliacaoFornecedorDTO> listByIdAvaliacaoFornecedor(final Integer idAvaliacaoFornecedor) throws Exception {
        final Collection<CriterioAvaliacaoFornecedorDTO> listCriterioAvaliacaoFornecedor = this.getDao().listByIdAvaliacaoFornecedor(idAvaliacaoFornecedor);
        try {
            if (listCriterioAvaliacaoFornecedor != null) {
                for (final CriterioAvaliacaoFornecedorDTO criterioAvaliacaoFornecedor : listCriterioAvaliacaoFornecedor) {
                    if (criterioAvaliacaoFornecedor.getValorInteger() == 1) {
                        criterioAvaliacaoFornecedor.setValor("Sim");
                    } else {
                        if (criterioAvaliacaoFornecedor.getValorInteger() == 0) {
                            criterioAvaliacaoFornecedor.setValor("N�o");
                        } else {
                            criterioAvaliacaoFornecedor.setValor("N/A");
                        }
                    }
                }
            }
            return listCriterioAvaliacaoFornecedor;
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdAvaliacaoFornecedor(final Integer idAvaliacaoFornecedor) throws Exception {
        try {
            this.getDao().deleteByIdAvaliacaoFornecedor(idAvaliacaoFornecedor);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }

    }
}
