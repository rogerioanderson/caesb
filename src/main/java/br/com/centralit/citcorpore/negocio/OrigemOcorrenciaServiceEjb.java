/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.OrigemOcorrenciaDTO;
import br.com.centralit.citcorpore.integracao.OrigemOcorrenciaDAO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

/**
 * @author thiago.monteiro
 */
public class OrigemOcorrenciaServiceEjb extends CrudServiceImpl implements OrigemOcorrenciaService {

    private OrigemOcorrenciaDAO dao;

    @Override
    protected OrigemOcorrenciaDAO getDao() {
        if (dao == null) {
            dao = new OrigemOcorrenciaDAO();
        }
        return dao;
    }

	@Override
	public IDto create(IDto model) throws ServiceException, LogicException {
		return super.create(removerEspacosNoInicioEouFimDoNome(model));
	}

	@Override
	public void update(IDto model) throws ServiceException, LogicException {
		super.update(removerEspacosNoInicioEouFimDoNome(model));
	}

	private OrigemOcorrenciaDTO removerEspacosNoInicioEouFimDoNome(IDto model) {
		OrigemOcorrenciaDTO origemOcorrenciaDTO = (OrigemOcorrenciaDTO) model;
		origemOcorrenciaDTO.setNome(origemOcorrenciaDTO.getNome().trim());

		return origemOcorrenciaDTO;
	}

    @Override
    public void deletarOrigemOcorrencia(final IDto model, final DocumentHTML document) throws ServiceException, Exception {
        OrigemOcorrenciaDTO origemOcorrenciaDTO = (OrigemOcorrenciaDTO) model;
        final TransactionControler tc = new TransactionControlerImpl(this.getDao().getAliasDB());
        try {
            this.validaUpdate(model);
            this.getDao().setTransactionControler(tc);
            tc.start();
            origemOcorrenciaDTO = (OrigemOcorrenciaDTO) this.getDao().restore(origemOcorrenciaDTO);
            origemOcorrenciaDTO.setDataFim(UtilDatas.getDataAtual());
            this.getDao().update(origemOcorrenciaDTO);
            document.alert(this.i18nMessage("MSG07"));
            tc.commit();
            tc.close();
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        }
    }

    @Override
    public boolean consultarOrigemOcorrenciaAtiva(final OrigemOcorrenciaDTO origemOcorrenciaDTO) throws Exception {
        return this.getDao().consultarOrigemOcorrenciaAtiva(origemOcorrenciaDTO);
    }

    /**
	 * Metodo responsavel por retornar todos os dados da Origem de uma
	 * ocorr�ncia
	 *
	 * @param idOrigem
	 * @return
	 * @author Ezequiel
	 * @throws Exception
	 * @throws ServiceException
	 */
    @Override
    public OrigemOcorrenciaDTO restoreAll(final Integer idOrigem) throws ServiceException, Exception {
        return this.getDao().restoreAll(idOrigem);
    }

}
