/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart
 */
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.PastaDTO;
import br.com.centralit.citcorpore.bean.PerfilAcessoPastaDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.PerfilAcessoPastaDAO;
import br.com.centralit.citcorpore.util.Enumerados.PermissaoAcessoPasta;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.service.ServiceLocator;

/**
 * EJB de PerfilAcessoPasta.
 *
 * @author valdoilo.damasceno
 */
public class PerfilAcessoPastaServiceEjb extends CrudServiceImpl implements PerfilAcessoPastaService {

    private PerfilAcessoPastaDAO dao;

    @Override
    protected PerfilAcessoPastaDAO getDao() {
        if (dao == null) {
            dao = new PerfilAcessoPastaDAO();
        }
        return dao;
    }

    private PastaService pastaService;

    private PastaService getPastaService() throws Exception {
        if (pastaService == null) {
            pastaService = (PastaService) ServiceLocator.getInstance().getService(PastaService.class, null);
        }
        return pastaService;
    }

    @Override
    public boolean verificarSeUsuarioAprovaBaseConhecimentoParaPastaSelecionada(final UsuarioDTO usuario, final Integer idPasta) throws Exception {
        return this.getDao().usuarioAprovaBaseConhecimentoParaPastaSelecionada(usuario, idPasta);
    }

    @Override
    public List<PerfilAcessoPastaDTO> validaPasta(final UsuarioDTO usuario) throws Exception {
        return this.getDao().validaPasta(usuario);
    }

    @Override
    public Collection<PerfilAcessoPastaDTO> findByIdPasta(final Integer idPasta) throws Exception {
        return this.getDao().findByIdPasta(idPasta);
    }

    @Override
    public Collection<PerfilAcessoPastaDTO> listByIdPasta(final Integer idPasta) throws Exception {
        return this.getDao().listByIdPasta(idPasta);
    }

    @Override
    public PermissaoAcessoPasta verificarPermissaoDeAcessoPasta(final UsuarioDTO usuario, PastaDTO pastaDto) throws Exception {
        pastaDto = this.getPastaService().obterHerancaDePermissao(pastaDto);
        PermissaoAcessoPasta permissao = null;
        if (pastaDto != null) {
            permissao = this.getDao().verificarPermissaoDeAcessoPasta(usuario, pastaDto.getId());
        }

        return permissao;
    }
    
    /**
     * Verifica se um usu�rio tem permiss�o de acesso a um pasta.
     * 
     * @author rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
     * @param UsuarioDTO
     *            usuarioDto
     * @param PastaDTO
     *            pastaDto
     * @return boolean
     * @since 25/06/2015
     */
    public boolean verificaPermissaoDeAcessoNaPasta(final UsuarioDTO usuarioDto, final PastaDTO pastaDto) {
        boolean temPermmissaoNaPasta = false;
        PermissaoAcessoPasta permissaoAcessoPasta;

        try {
            permissaoAcessoPasta = this.verificarPermissaoDeAcessoPasta(usuarioDto, pastaDto);
            if (permissaoAcessoPasta != null) {
                if (PermissaoAcessoPasta.LEITURA.equals(permissaoAcessoPasta) || PermissaoAcessoPasta.LEITURAGRAVACAO.equals(permissaoAcessoPasta)) {
                    temPermmissaoNaPasta = true;
                } else {
                    if (PermissaoAcessoPasta.SEMPERMISSAO.equals(permissaoAcessoPasta)) {
                        temPermmissaoNaPasta = false;
                    }
                }
            }
        } catch (IllegalArgumentException illegArgExc) {
            illegArgExc.printStackTrace();
            temPermmissaoNaPasta = false;
        } catch (PersistenceException perExc) {
            perExc.printStackTrace();
            temPermmissaoNaPasta = false;
        } catch (ServiceException servExc) {
            servExc.printStackTrace();
            temPermmissaoNaPasta = false;
        } catch (LogicException logicExc) {
            logicExc.printStackTrace();
            temPermmissaoNaPasta = false;
        } catch (Exception exc) {
            exc.printStackTrace();
            temPermmissaoNaPasta = false;
        }

        return temPermmissaoNaPasta;
    }
    
    /**
     * Verifica se um usu�rio tem permiss�o de administrador sobre uma pasta.
     * 
     * @author rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
     * @param Integer
     *            idPasta
     * @param Integer
     *            idUsuario
     * @return boolean
     * @since 25/06/2014
     */
    public boolean verificaPermissaoDeAcessoAdministradorDoUsuarioNaPasta(final Integer idPasta, final Integer idUsuario) {
        boolean temPermissaoAdministradorNaPasta = this.getDao().verificaPermissaoDeAcessoAdministradorNaPasta(idPasta, idUsuario);

        return temPermissaoAdministradorNaPasta;
    }

}
