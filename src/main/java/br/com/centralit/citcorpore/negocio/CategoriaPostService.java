/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart
 */
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.CategoriaPostDTO;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

@SuppressWarnings("rawtypes")
public interface CategoriaPostService extends CrudService {

	/**
	 * Retorna Lista de Categorias Ativas.
	 * 
	 * @throws Exception
	 * @return <code>Collection</code>
	 */
	public Collection listCategoriasAtivas() throws Exception;

	/**
	 * Verifica se Categoria Servi�o possui subcategoria ou servi�o associado.
	 * 
	 * @param categoriaServico
	 * @return - <b>True:</b> Possui. - <b>False: </b>N�o possui.
	 * @throws PersistenceException
	 * @throws ServiceException
	 */
	public boolean verificarSeCategoriaPossuiServicoOuSubCategoria(CategoriaPostDTO categoriaPostDTO) throws PersistenceException, br.com.citframework.excecao.ServiceException;

	/**
	 * Verifica se categoria informada j� existe.
	 * 
	 * @param categoriaServicoDTO
	 * @throws PersistenceException
	 * @throws ServiceException
	 * @return true - existe; false - n�o existe;
	 */
	public boolean verificarSeCategoriaExiste(CategoriaPostDTO categoriaPostDTO) throws PersistenceException, ServiceException;


}
