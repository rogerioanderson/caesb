/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import br.com.centralit.citcorpore.bean.AcordoNivelServicoDTO;
import br.com.centralit.citcorpore.bean.AcordoNivelServicoHistoricoDTO;
import br.com.centralit.citcorpore.bean.ServicoContratoDTO;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

public interface AcordoNivelServicoService extends CrudService {

    Collection findByIdServicoContrato(Integer parm) throws Exception;
    
    Collection consultaPorIdServicoContrato(Integer parm) throws Exception;

    void deleteByIdServicoContrato(Integer parm) throws Exception;

    Collection findByIdPrioridadePadrao(Integer parm) throws Exception;

    void deleteByIdPrioridadePadrao(Integer parm) throws Exception;

    void copiarSLA(Integer idAcordoNivelServico, Integer idServicoContratoOrigem, Integer[] idServicoCopiarPara)
            throws Exception;

    AcordoNivelServicoDTO findAtivoByIdServicoContrato(Integer idServicoContrato, String tipo) throws Exception;

    List<ServicoContratoDTO> buscaServicosComContrato(String tituloSla) throws Exception;

    boolean verificaSeNomeExiste(HashMap mapFields) throws Exception;

    List<AcordoNivelServicoDTO> findAcordosSemVinculacaoDireta() throws Exception;

    /**
     * Cria um novo acordo de n�vel de servi�o
     *
     * @param acordoNivelServicoDTO
     * @param acordoNivelServicoHistoricoDTO
     * @return AcordoNivelServicoDTO
     * @throws Exception
     * @author rodrigo.oliveira
     */
    AcordoNivelServicoDTO create(AcordoNivelServicoDTO acordoNivelServicoDTO,
            AcordoNivelServicoHistoricoDTO acordoNivelServicoHistoricoDTO) throws ServiceException, LogicException;

    /**
     * Atualiza um novo acordo de n�vel de servi�o
     *
     * @param acordoNivelServicoDTO
     * @param acordoNivelServicoHistoricoDTO
     * @return AcordoNivelServicoDTO
     * @throws Exception
     * @author rodrigo.oliveira
     */
    AcordoNivelServicoDTO update(AcordoNivelServicoDTO acordoNivelServicoDTO,
            AcordoNivelServicoHistoricoDTO acordoNivelServicoHistoricoDTO) throws ServiceException, LogicException;

    /**
     * Exclui Base de Conhecimento.
     *
     * @param baseConhecimentoBean
     * @param isAprovaBaseConhecimento
     * @throws Exception
     * @author rodrigo.oliveira
     */
    void excluir(AcordoNivelServicoDTO acordoNivelServicoDTO) throws Exception;

    List<AcordoNivelServicoDTO> findIdEmailByIdSolicitacaoServico(Integer idSolicitacaoServico) throws Exception;

    String verificaIdAcordoNivelServico(HashMap mapFields) throws Exception;

}
