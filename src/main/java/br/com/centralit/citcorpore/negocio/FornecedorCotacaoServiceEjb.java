/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.CotacaoDTO;
import br.com.centralit.citcorpore.bean.FornecedorCotacaoDTO;
import br.com.centralit.citcorpore.integracao.ColetaPrecoDao;
import br.com.centralit.citcorpore.integracao.CotacaoDao;
import br.com.centralit.citcorpore.integracao.FornecedorCotacaoDao;
import br.com.centralit.citcorpore.util.Enumerados.SituacaoCotacao;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;

public class FornecedorCotacaoServiceEjb extends CrudServiceImpl implements FornecedorCotacaoService {

    private FornecedorCotacaoDao dao;

    @Override
    protected FornecedorCotacaoDao getDao() {
        if (dao == null) {
            dao = new FornecedorCotacaoDao();
        }
        return dao;
    }

    @Override
    protected void validaDelete(final Object arg0) throws Exception {
        final FornecedorCotacaoDTO fornecedorCotacaoDto = (FornecedorCotacaoDTO) this.restore((FornecedorCotacaoDTO) arg0);
        CotacaoDTO cotacaoDto = new CotacaoDTO();
        cotacaoDto.setIdCotacao(fornecedorCotacaoDto.getIdCotacao());
        cotacaoDto = (CotacaoDTO) new CotacaoDao().restore(cotacaoDto);
        if (!cotacaoDto.getSituacao().equals(SituacaoCotacao.EmAndamento.name())) {
            throw new LogicException("A situa��o da cota��o n�o permite a exclus�o do fornecedor.");
        }

        final Collection colColetas = new ColetaPrecoDao().findByIdCotacaoAndIdFornecedor(fornecedorCotacaoDto.getIdCotacao(), fornecedorCotacaoDto.getIdFornecedor());
        if (colColetas != null && !colColetas.isEmpty()) {
            throw new LogicException("Exclus�o n�o permitida. Existe pelo menos uma coleta de pre�os associada ao fornecedor.");
        }
    }

    @Override
    public Collection findByIdCotacao(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdCotacao(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdCotacao(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdCotacao(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdFornecedor(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdFornecedor(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(final IDto model) throws ServiceException, LogicException {
        FornecedorCotacaoDTO fornecedorCotacaoDto = (FornecedorCotacaoDTO) model;
        fornecedorCotacaoDto = (FornecedorCotacaoDTO) this.restore(fornecedorCotacaoDto);
        final FornecedorCotacaoDao fornecedorCotacaoDao = new FornecedorCotacaoDao();
        final TransactionControler tc = new TransactionControlerImpl(fornecedorCotacaoDao.getAliasDB());

        try {
            this.validaDelete(model);

            fornecedorCotacaoDao.setTransactionControler(tc);

            tc.start();

            fornecedorCotacaoDao.delete(fornecedorCotacaoDto);

            tc.commit();
            tc.close();
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        }
    }

}
