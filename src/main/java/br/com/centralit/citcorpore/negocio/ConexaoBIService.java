/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.BICitsmartResultRotinaDTO;
import br.com.centralit.citcorpore.bean.ConexaoBIDTO;
import br.com.centralit.citcorpore.bean.ProcessamentoBatchDTO;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;


public interface ConexaoBIService extends CrudService {
	@SuppressWarnings("rawtypes")
	public Collection listAll() throws Exception;
	@SuppressWarnings("rawtypes")
	public Collection findByIdConexao(ConexaoBIDTO conexaoBIDTO) throws Exception;
	@SuppressWarnings("rawtypes")
	public Collection listarConexoesPaginadas(Collection<ConexaoBIDTO> conexaoBIDTO, Integer pgAtual, Integer qtdPaginacao) throws Exception;
	@SuppressWarnings("rawtypes")
	public Collection listarConexoesPaginadasFiltradas(ConexaoBIDTO conexaoBIDTO, Integer pgAtual, Integer qtdPaginacao) throws Exception;
	public boolean jaExisteRegistroComMesmoNome(ConexaoBIDTO conexaoBIDTO) throws Exception;
	public boolean jaExisteRegistroComMesmoLink(ConexaoBIDTO conexaoBIDTO) throws Exception;
	public Integer obterTotalDePaginas(Integer itensPorPagina, String loginUsuario, ConexaoBIDTO conexaoBIBean) throws Exception;
	public ConexaoBIDTO findByIdProcessBatch(Integer idProcessamentoBatch) throws Exception;
	public ArrayList<ConexaoBIDTO> listarConexoesAutomaticasSemAgendEspOuExcecao() throws ServiceException, Exception;
	public String getIdProcEspecificoOuExcecao() throws Exception;
	public java.util.Date getProxDtExecucao(ConexaoBIDTO conexaoBIDto) throws ServiceException, Exception;
	public java.util.Date getProxDtExecucaoPadraoOuEspecifica(ConexaoBIDTO conexaoBIDto) throws ServiceException, Exception;
	public BICitsmartResultRotinaDTO validaAgendamentoExcecao (ConexaoBIDTO conexaoBIDTO, ProcessamentoBatchDTO processamentoBatchDTO)  throws ServiceException, Exception;
}
