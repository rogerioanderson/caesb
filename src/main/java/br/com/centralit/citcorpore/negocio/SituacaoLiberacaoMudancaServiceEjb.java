/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.SituacaoLiberacaoMudancaDTO;
import br.com.centralit.citcorpore.integracao.SituacaoLiberacaoMudancaDAO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilI18N;

public class SituacaoLiberacaoMudancaServiceEjb extends CrudServiceImpl implements SituacaoLiberacaoMudancaService {

    private SituacaoLiberacaoMudancaDAO dao;

    @Override
    protected SituacaoLiberacaoMudancaDAO getDao() {
        if (dao == null) {
            dao = new SituacaoLiberacaoMudancaDAO();
        }
        return dao;
    }

    @Override
    public boolean consultaExistenciaSituacao(final SituacaoLiberacaoMudancaDTO obj) throws Exception {
        return this.getDao().consultarSituacoes(obj);
    }

    @Override
    public void deletarSituacao(final IDto model, final DocumentHTML document, final HttpServletRequest request) throws ServiceException, Exception {
        final SituacaoLiberacaoMudancaDTO situacaoDto = (SituacaoLiberacaoMudancaDTO) model;
        try {

            if (this.getDao().consultarSituacoes(situacaoDto)) {
                document.alert(this.i18nMessage("citcorpore.comum.registroNaoPodeSerExcluido"));
                return;
            } else {
                this.getDao().delete(situacaoDto);
                document.alert(UtilI18N.internacionaliza(request, "MSG07"));
            }
        } catch (final Exception e) {

        }
    }

    @Override
    public Collection<SituacaoLiberacaoMudancaDTO> listAll() throws ServiceException, Exception {
        try {
            return this.getDao().listAll();
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
