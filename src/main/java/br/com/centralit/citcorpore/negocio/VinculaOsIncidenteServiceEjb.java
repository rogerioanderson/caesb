/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.integracao.VinculaOsIncidenteDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

/**
 *
 * @author rodrigo.oliveira
 *
 */
public class VinculaOsIncidenteServiceEjb extends CrudServiceImpl implements VinculaOsIncidenteService {

    private VinculaOsIncidenteDao dao;

    @Override
    protected VinculaOsIncidenteDao getDao() {
        if (dao == null) {
            dao = new VinculaOsIncidenteDao();
        }
        return dao;
    }

    @Override
    public Collection findByIdOS(final Integer idOS) throws Exception {
        try {
            return this.getDao().findByIdOS(idOS);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdOs(final Integer idOS) throws Exception {
        try {
            this.getDao().deleteByIdOs(idOS);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdAtividadeOS(final Integer idAtividadeOS) throws Exception {
        try {
            this.getDao().deleteByIdAtividadeOS(idAtividadeOS);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean verificaServicoSelecionado(final Integer idServicoContratoContabil) throws Exception {
        try {
            return this.getDao().verificaServicoSelecionado(idServicoContratoContabil);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean verificaServicoJaVinculado(final Integer idOS, final Integer idServicoContratoContabil) throws Exception {
        try {
            return this.getDao().verificaServicoJaVinculado(idOS, idServicoContratoContabil);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdAtividadeOS(final Integer idAtividadeOS) throws Exception {
        try {
            return this.getDao().findByIdAtividadeOS(idAtividadeOS);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
