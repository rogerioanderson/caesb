/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.TabFederacaoDadosDTO;
import br.com.centralit.citcorpore.integracao.TabFederacaoDadosDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilNumbersAndDecimals;

public class TabFederacaoDadosServiceEjb extends CrudServiceImpl implements TabFederacaoDadosService {

    private TabFederacaoDadosDao dao;

    @Override
    protected TabFederacaoDadosDao getDao() {
        if (dao == null) {
            dao = new TabFederacaoDadosDao();
        }
        return dao;
    }

    @Override
    public Collection findByNomeTabela(final String parm) throws Exception {
        try {
            return this.getDao().findByNomeTabela(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByNomeTabela(final String parm) throws Exception {
        try {
            this.getDao().deleteByNomeTabela(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByChaveFinal(final String parm) throws Exception {
        try {
            return this.getDao().findByChaveFinal(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByChaveFinal(final String parm) throws Exception {
        try {
            this.getDao().deleteByChaveFinal(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByChaveOriginal(final String parm) throws Exception {
        try {
            return this.getDao().findByChaveOriginal(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByChaveOriginal(final String parm) throws Exception {
        try {
            this.getDao().deleteByChaveOriginal(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByOrigem(final String parm) throws Exception {
        try {
            return this.getDao().findByOrigem(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByOrigem(final String parm) throws Exception {
        try {
            this.getDao().deleteByOrigem(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }
 
	@Override
	public Collection findByChaveOriginal(final String origem, final String tabela, final String chaveOriginal) throws Exception {
        try {
            return this.getDao().findByChaveOriginal(origem, tabela, chaveOriginal);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
	}

	@Override
	public TabFederacaoDadosDTO create(TabFederacaoDadosDTO federacaoDados, TransactionControler tc) throws Exception {
		TabFederacaoDadosDao federacaoDadosDao = new TabFederacaoDadosDao();
		if (tc != null) {
			federacaoDadosDao.setTransactionControler(tc);
		}
		return (TabFederacaoDadosDTO) federacaoDadosDao.create(federacaoDados);
	}

	@Override
	public void update(TabFederacaoDadosDTO federacaoDados, TransactionControler tc) throws Exception {
		TabFederacaoDadosDao federacaoDadosDao = new TabFederacaoDadosDao();
		if (tc != null) {
			federacaoDadosDao.setTransactionControler(tc);
		}
		federacaoDadosDao.updateNotNull(federacaoDados);
	}

	@Override
	public TabFederacaoDadosDTO createOrUpdate(TabFederacaoDadosDTO federacaoDados, TransactionControler tc) throws Exception {
		TabFederacaoDadosDao federacaoDadosDao = new TabFederacaoDadosDao();
		if (tc != null) {
			federacaoDadosDao.setTransactionControler(tc);
		}
		
		Collection<TabFederacaoDadosDTO> col = federacaoDadosDao.find(federacaoDados.getOrigem(), federacaoDados.getNomeTabela(), federacaoDados.getChaveOriginal(), federacaoDados.getChaveFinal());
		if (col == null || col.isEmpty()) {
			col = federacaoDadosDao.findByChaveOriginal(federacaoDados.getOrigem(), federacaoDados.getNomeTabela(), federacaoDados.getChaveOriginal());
		}
		
		if (col == null || col.isEmpty()) {
			return (TabFederacaoDadosDTO) federacaoDadosDao.create(federacaoDados);
		}else{
			TabFederacaoDadosDTO bean = (TabFederacaoDadosDTO) ((List) col).get(0);
			if (UtilNumbersAndDecimals.isInteger(bean.getChaveFinal()) && Long.parseLong(bean.getChaveFinal()) > 0) {
				return bean;
			}else{
				federacaoDadosDao.delete(bean);
				return (TabFederacaoDadosDTO) federacaoDadosDao.create(federacaoDados);
			}
		}
	}

	@Override
	public Collection find(String origem, String nomeTab, String chaveOrig, String chaveFinal) throws Exception {
        try {
            return this.getDao().find(origem, nomeTab, chaveOrig, chaveFinal);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
	}
}
