/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.List;

import br.com.centralit.citcorpore.bean.HistoricoMudancaDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaDTO;
import br.com.centralit.citcorpore.integracao.HistoricoMudancaDao;
import br.com.citframework.dto.IDto;
import br.com.citframework.service.CrudServiceImpl;

public class HistoricoMudancaServiceEjb extends CrudServiceImpl implements HistoricoMudancaService {

    private HistoricoMudancaDao dao;

    @Override
    protected HistoricoMudancaDao getDao() {
        if (dao == null) {
            dao = new HistoricoMudancaDao();
        }
        return dao;
    }

    @Override
    public void updateNotNull(final IDto obj) throws Exception {
        this.getDao().updateNotNull(obj);
    }

    @Override
    public List<HistoricoMudancaDTO> listHistoricoMudancaByIdRequisicaoMudanca(final Integer idRequisicaoMudanca) throws Exception {
        return this.getDao().listHistoricoMudancaByIdRequisicaoMudanca(idRequisicaoMudanca);
    }

    @Override
    public HistoricoMudancaDTO maxIdHistorico(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception {
        return this.getDao().maxIdHistorico(requisicaoMudancaDTO);
    }

}
