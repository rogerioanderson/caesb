/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.Iterator;

import br.com.centralit.citcorpore.bean.BIDashBoardDTO;
import br.com.centralit.citcorpore.bean.BIItemDashBoardDTO;
import br.com.centralit.citcorpore.integracao.BIDashBoardDao;
import br.com.centralit.citcorpore.integracao.BIItemDashBoardDao;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.CrudDAO;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;

public class BIDashBoardServiceEjb extends CrudServiceImpl implements BIDashBoardService {

    private BIDashBoardDao dao;

    @Override
    protected BIDashBoardDao getDao() {
        if (dao == null) {
            dao = new BIDashBoardDao();
        }
        return dao;
    }

    @Override
    public IDto create(IDto model) throws ServiceException, LogicException {
        // Instancia Objeto controlador de transacao
        final CrudDAO crudDao = this.getDao();
        final BIItemDashBoardDao biItemDashBoardDao = new BIItemDashBoardDao();
        final TransactionControler tc = new TransactionControlerImpl(crudDao.getAliasDB());
        try {
            // Faz validacao, caso exista.
            this.validaCreate(model);

            // Instancia ou obtem os DAOs necessarios.

            // Seta o TransactionController para os DAOs
            crudDao.setTransactionControler(tc);
            biItemDashBoardDao.setTransactionControler(tc);

            // Inicia transacao
            tc.start();

            // Executa operacoes pertinentes ao negocio.
            model = crudDao.create(model);
            final BIDashBoardDTO biDashBoardDTO = (BIDashBoardDTO) model;
            if (biDashBoardDTO != null) {
                final Collection col = biDashBoardDTO.getColItens();
                if (col != null) {
                    for (final Iterator it = col.iterator(); it.hasNext();) {
                        final BIItemDashBoardDTO biItemDashBoardDTO = (BIItemDashBoardDTO) it.next();
                        biItemDashBoardDTO.setIdDashBoard(biDashBoardDTO.getIdDashBoard());
                        biItemDashBoardDao.create(biItemDashBoardDTO);
                    }
                }
            }

            // Faz commit e fecha a transacao.
            tc.commit();

            return model;
        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        } finally {
            tc.closeQuietly();
        }
        return model;
    }

    @Override
    public void update(final IDto model) throws ServiceException, LogicException {
        final CrudDAO crudDao = this.getDao();
        final BIItemDashBoardDao biItemDashBoardDao = new BIItemDashBoardDao();
        final TransactionControler tc = new TransactionControlerImpl(crudDao.getAliasDB());
        try {
            // Faz validacao, caso exista.
            this.validaCreate(model);

            // Instancia ou obtem os DAOs necessarios.

            // Seta o TransactionController para os DAOs
            crudDao.setTransactionControler(tc);
            biItemDashBoardDao.setTransactionControler(tc);

            // Inicia transacao
            tc.start();

            // Executa operacoes pertinentes ao negocio.
            crudDao.update(model);
            final BIDashBoardDTO biDashBoardDTO = (BIDashBoardDTO) model;
            biItemDashBoardDao.deleteByIdDashBoard(biDashBoardDTO.getIdDashBoard());
            if (biDashBoardDTO != null) {
                final Collection col = biDashBoardDTO.getColItens();
                if (col != null) {
                    for (final Iterator it = col.iterator(); it.hasNext();) {
                        final BIItemDashBoardDTO biItemDashBoardDTO = (BIItemDashBoardDTO) it.next();
                        biItemDashBoardDTO.setIdDashBoard(biDashBoardDTO.getIdDashBoard());
                        biItemDashBoardDao.create(biItemDashBoardDTO);
                    }
                }
            }

            // Faz commit e fecha a transacao.
            tc.commit();

        } catch (final Exception e) {
            this.rollbackTransaction(tc, e);
        } finally {
            tc.closeQuietly();
        }
    }

    @Override
    public IDto getByIdentificacao(final String ident) throws Exception {
        return this.getDao().getByIdentificacao(ident);
    }

}
