/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import br.com.centralit.citcorpore.bean.ContatoRequisicaoLiberacaoDTO;
import br.com.centralit.citcorpore.integracao.ContatoRequisicaoLiberacaoDao;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class ContatoRequisicaoLiberacaoServiceEjb extends CrudServiceImpl implements ContatoRequisicaoLiberacaoService {

    private ContatoRequisicaoLiberacaoDao dao;

    @Override
    protected ContatoRequisicaoLiberacaoDao getDao() {
        if (dao == null) {
            dao = new ContatoRequisicaoLiberacaoDao();
        }
        return dao;
    }

    @Override
    public ContatoRequisicaoLiberacaoDTO restoreContatosById(final Integer idContatoRequisicaoLiberacao) {
        ContatoRequisicaoLiberacaoDTO contatoRequisicaoLiberacaoDTO = new ContatoRequisicaoLiberacaoDTO();
        contatoRequisicaoLiberacaoDTO.setIdContatoRequisicaoLiberacao(idContatoRequisicaoLiberacao);
        try {
            contatoRequisicaoLiberacaoDTO = (ContatoRequisicaoLiberacaoDTO) this.getDao().restore(contatoRequisicaoLiberacaoDTO);
        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("Contato Requisicao Liberac�o n�o foi encontrado com esse ID");
        }
        return contatoRequisicaoLiberacaoDTO;
    }

    @Override
    public synchronized IDto create(final IDto model) throws ServiceException, LogicException {
        return super.create(model);
    }

}
