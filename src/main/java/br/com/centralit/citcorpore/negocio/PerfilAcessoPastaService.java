/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart
 */
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.PastaDTO;
import br.com.centralit.citcorpore.bean.PerfilAcessoPastaDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.util.Enumerados.PermissaoAcessoPasta;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

/**
 * Service de PerfilAcessoPasta.
 * 
 * @author valdoilo.damasceno
 */
public interface PerfilAcessoPastaService extends CrudService {

	/**
	 * Verifica se Usu�rio Pode Aprovar Base de Conhecimento da pasta Selecionada.
	 * 
	 * @param usuario
	 * @param idPasta
	 * @return Boolean
	 * @author valdoilo.damasceno
	 * @throws ServiceException
	 * @throws Exception
	 */
	public boolean verificarSeUsuarioAprovaBaseConhecimentoParaPastaSelecionada(UsuarioDTO usuario, Integer idPasta) throws ServiceException, Exception;

	public List<PerfilAcessoPastaDTO> validaPasta(UsuarioDTO usuario) throws Exception;

	public Collection<PerfilAcessoPastaDTO> findByIdPasta(Integer idPasta) throws Exception;

	/**
	 * Lista PERFILACESSOPASTA ATIVOS.
	 * 
	 * @param idPasta
	 * @return Collection<PerfilAcessoPastaDTO>
	 * @throws Exception
	 * @author Vadoilo Damasceno
	 */
	public Collection<PerfilAcessoPastaDTO> listByIdPasta(Integer idPasta) throws Exception;

	/**
	 * Verifica Permiss�o de Acesso a Pasta Informada de acordo com a Heran�a de Permiss�o, caso a Pasta possua..
	 * 
	 * @param usuario
	 * @param idPasta
	 * @return SemPermissao = Sem permiss�o de acesso; Leitura = Permiss�o de Leitura; LeituraGravacao = Permiss�o de Leitura/Grava��o.
	 * @throws Exception
	 * @author Vadoilo Damasceno
	 */
	public PermissaoAcessoPasta verificarPermissaoDeAcessoPasta(UsuarioDTO usuario, PastaDTO pastaDto) throws Exception;
	
	/**
	 * @author rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
	 * @param idPasta
	 * @param idUsuario
	 * @return boolean
	 * @since 25/06/2015
	 */
	public boolean verificaPermissaoDeAcessoAdministradorDoUsuarioNaPasta(final Integer idPasta, final Integer idUsuario);
	
	/**
	 * @author rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
	 * @param usuarioDto
	 * @param pastaDto
	 * @return boolean
	 * @since 25/06/2015
	 */
	public boolean verificaPermissaoDeAcessoNaPasta(final UsuarioDTO usuarioDto, final PastaDTO pastaDto);
}
