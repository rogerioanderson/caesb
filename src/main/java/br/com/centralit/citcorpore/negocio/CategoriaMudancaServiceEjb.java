/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import br.com.centralit.citcorpore.bean.CategoriaMudancaDTO;
import br.com.centralit.citcorpore.integracao.CategoriaMudancaDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class CategoriaMudancaServiceEjb extends CrudServiceImpl implements CategoriaMudancaService {

    private CategoriaMudancaDao dao;

    @Override
    protected CategoriaMudancaDao getDao() {
        if (dao == null) {
            dao = new CategoriaMudancaDao();
        }
        return dao;
    }

    @Override
    @SuppressWarnings("rawtypes")
    public Collection findByIdCategoriaMudanca(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdCategoriaMudanca(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdCategoriaMudanca(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdCategoriaMudanca(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    @SuppressWarnings("rawtypes")
    public Collection findByIdCategoriaMudancaPai(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdCategoriaMudancaPai(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdCategoriaMudancaPai(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdCategoriaMudancaPai(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    @SuppressWarnings("rawtypes")
    public Collection findByNomeCategoria(final Integer parm) throws Exception {
        try {
            return this.getDao().findByNomeCategoria(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByNomeCategoria(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByNomeCategoria(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    // ////
    @Override
    @SuppressWarnings("rawtypes")
    public Collection listHierarquia() throws Exception {
        final Collection colFinal = new ArrayList();
        try {
            final Collection col = this.getDao().findCategoriaMudancaSemPai();
            if (col != null) {
                for (final Iterator it = col.iterator(); it.hasNext();) {
                    final CategoriaMudancaDTO categoriaMudancaDto = (CategoriaMudancaDTO) it.next();
                    categoriaMudancaDto.setNivel(0);
                    colFinal.add(categoriaMudancaDto);
                    final Collection colAux = this.getCollectionHierarquia(categoriaMudancaDto, 0);
                    if (colAux != null && colAux.size() > 0) {
                        colFinal.addAll(colAux);
                    }
                }
            }
            return colFinal;
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    private Collection getCollectionHierarquia(final CategoriaMudancaDTO idCategoriaMudanca, final Integer nivel) throws Exception {
        final Collection col = this.getDao().findByIdCategoriaMudancaPai(idCategoriaMudanca.getIdCategoriaMudancaPai());
        final Collection colFinal = new ArrayList();
        if (col != null) {
            for (final Iterator it = col.iterator(); it.hasNext();) {
                final CategoriaMudancaDTO categoriaMudancaDto = (CategoriaMudancaDTO) it.next();
                categoriaMudancaDto.setNivel(nivel + 1);
                colFinal.add(categoriaMudancaDto);
                final Collection colAux = this.getCollectionHierarquia(categoriaMudancaDto, categoriaMudancaDto.getNivel());
                if (colAux != null && colAux.size() > 0) {
                    colFinal.addAll(colAux);
                }
            }
        }
        return colFinal;
    }

    @Override
    public Collection findCategoriaAtivos() {
        // TODO Auto-generated method stub
        return null;
    }

}
