/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.EmailSolicitacaoServicoDTO;
import br.com.centralit.citcorpore.integracao.EmailSolicitacaoServicoDao;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class EmailSolicitacaoServicoServiceEjb extends CrudServiceImpl implements EmailSolicitacaoServicoService {

    private EmailSolicitacaoServicoDao dao;

    @Override
    protected EmailSolicitacaoServicoDao getDao() {
        if (dao == null) {
            dao = new EmailSolicitacaoServicoDao();
        }
        return dao;
    }

    @Override
    public EmailSolicitacaoServicoDTO listSituacao(final String messageid) throws Exception {
        try {
            return this.getDao().listSituacao(messageid);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<EmailSolicitacaoServicoDTO> getEmailByOrigem(final String origem) throws Exception {
        try {
            return this.getDao().getEmailByOrigem(origem);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public EmailSolicitacaoServicoDTO getEmailByIdSolicitacaoAndOrigem(final Integer idSolicitacao, final String origem) throws Exception {
        try {
            return this.getDao().getEmailByIdSolicitacaoAndOrigem(idSolicitacao, origem);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public EmailSolicitacaoServicoDTO getEmailByIdMessageAndOrigem(final String idMessage, final String origem) throws Exception {
        try {
            return this.getDao().getEmailByIdMessageAndOrigem(idMessage, origem);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public EmailSolicitacaoServicoDTO getEmailByIdMessage(final String idMessage) throws Exception {
        try {
            return this.getDao().getEmailByIdMessage(idMessage);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public IDto createWithID(final IDto model) throws Exception {
        return this.getDao().createWithID(model);
    }

}
