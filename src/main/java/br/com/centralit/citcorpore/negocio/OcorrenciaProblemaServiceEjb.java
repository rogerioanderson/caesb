/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.bpm.dto.ItemTrabalhoFluxoDTO;
import br.com.centralit.citcorpore.bean.JustificativaProblemaDTO;
import br.com.centralit.citcorpore.bean.OcorrenciaProblemaDTO;
import br.com.centralit.citcorpore.bean.ProblemaDTO;
import br.com.centralit.citcorpore.integracao.OcorrenciaProblemaDAO;
import br.com.centralit.citcorpore.util.Enumerados.CategoriaOcorrencia;
import br.com.centralit.citcorpore.util.Enumerados.OrigemOcorrencia;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

import com.google.gson.Gson;

@SuppressWarnings("rawtypes")
public class OcorrenciaProblemaServiceEjb extends CrudServiceImpl implements OcorrenciaProblemaService {

    private OcorrenciaProblemaDAO dao;

    @Override
    protected OcorrenciaProblemaDAO getDao() {
        if (dao == null) {
            dao = new OcorrenciaProblemaDAO();
        }
        return dao;
    }

    @Override
    public Collection findByIdProblema(final Integer idProblema) throws Exception {
        return this.getDao().findByIdProblema(idProblema);
    }

    public static OcorrenciaProblemaDTO create(final ProblemaDTO problemaDto, final ItemTrabalhoFluxoDTO itemTrabalhoFluxoDto, final String ocorrencia,
            final OrigemOcorrencia origem, final CategoriaOcorrencia categoria, final String informacoesContato, final String descricao, final String loginUsuario,
            final int tempo, final JustificativaProblemaDTO justificativaDto, final TransactionControler tc) throws Exception {
        final OcorrenciaProblemaDTO ocorrenciaProblemaDTO = new OcorrenciaProblemaDTO();
        ocorrenciaProblemaDTO.setIdProblema(problemaDto.getIdProblema());
        ocorrenciaProblemaDTO.setDataregistro(UtilDatas.getDataAtual());
        ocorrenciaProblemaDTO.setHoraregistro(UtilDatas.formatHoraFormatadaStr(UtilDatas.getHoraAtual()));
        ocorrenciaProblemaDTO.setTempoGasto(tempo);
        ocorrenciaProblemaDTO.setDescricao(descricao);
        ocorrenciaProblemaDTO.setDataInicio(UtilDatas.getDataAtual());
        ocorrenciaProblemaDTO.setDataFim(UtilDatas.getDataAtual());
        ocorrenciaProblemaDTO.setInformacoesContato(informacoesContato);
        ocorrenciaProblemaDTO.setRegistradopor(loginUsuario);
        try {
            ocorrenciaProblemaDTO.setDadosProblema(new Gson().toJson(problemaDto));
        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ocorrenciaProblemaDTO.setOcorrencia(ocorrencia);
        ocorrenciaProblemaDTO.setOrigem(origem.getSigla().toString());
        ocorrenciaProblemaDTO.setCategoria(categoria.getSigla());
        if (itemTrabalhoFluxoDto != null) {
            ocorrenciaProblemaDTO.setIdItemTrabalho(itemTrabalhoFluxoDto.getIdItemTrabalho());
        }
        if (justificativaDto != null) {
            ocorrenciaProblemaDTO.setIdJustificativa(justificativaDto.getIdJustificativaProblema());
            ocorrenciaProblemaDTO.setComplementoJustificativa(justificativaDto.getDescricaoProblema());
        }

        final OcorrenciaProblemaDAO ocorrenciaProblemaDao = new OcorrenciaProblemaDAO();
        if (tc != null) {
            ocorrenciaProblemaDao.setTransactionControler(tc);
        }
        return (OcorrenciaProblemaDTO) ocorrenciaProblemaDao.create(ocorrenciaProblemaDTO);
    }

}
