/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.bean.DadosEmailRegOcorrenciaDTO;
import br.com.centralit.citcorpore.bean.OcorrenciaDTO;
import br.com.centralit.citcorpore.integracao.OcorrenciaDao;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class OcorrenciaServiceEjb extends CrudServiceImpl implements OcorrenciaService {

    private OcorrenciaDao dao;

    @Override
    protected OcorrenciaDao getDao() {
        if (dao == null) {
            dao = new OcorrenciaDao();
        }
        return dao;
    }

    public Collection list(final List ordenacao) throws LogicException, ServiceException {
        return null;
    }

    public Collection list(final String ordenacao) throws LogicException, ServiceException {
        return null;
    }

    @Override
    public Collection findByDemanda(final Integer idDemanda) throws LogicException, ServiceException {
        try {
            return this.getDao().findByDemanda(idDemanda);
        } catch (final Exception e) {
            e.printStackTrace();
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateResposta(final IDto bean) throws LogicException, ServiceException {
        try {
            this.getDao().updateResposta(bean);
        } catch (final Exception e) {
            e.printStackTrace();
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<OcorrenciaDTO> findByIdSolicitacao(final Integer idSolicitacao) throws Exception {
        try {
            return this.getDao().findByIdSolicitacao(idSolicitacao);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public OcorrenciaDTO findSiglaGrupoExecutorByIdSolicitacao(final Integer idSolicitacao) throws Exception {
        try {
            return this.getDao().findSiglaGrupoExecutorByIdSolicitacao(idSolicitacao);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    /*
     * (non-Javadoc)
     * @see br.com.centralit.citcorpore.negocio.OcorrenciaService#obterDadosResponsavelEmailRegOcorrencia(java.lang.Integer)
     */
    @Override
    public DadosEmailRegOcorrenciaDTO obterDadosResponsavelEmailRegOcorrencia(final Integer idSolicitacaoServico) throws Exception {
        return this.getDao().obterDadosResponsavelEmailRegOcorrencia(idSolicitacaoServico);
    }

}
