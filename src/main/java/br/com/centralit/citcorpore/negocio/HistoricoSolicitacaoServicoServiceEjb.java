/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.HistoricoSolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.RelatorioCargaHorariaDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.integracao.HistoricoSolicitacaoServicoDao;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.citframework.dto.IDto;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilStrings;

@SuppressWarnings("unchecked")
public class HistoricoSolicitacaoServicoServiceEjb extends CrudServiceImpl implements HistoricoSolicitacaoServicoService {

    private HistoricoSolicitacaoServicoDao dao;
    private HistoricoSolicitacaoServicoDao daoReports;

    @Override
    protected HistoricoSolicitacaoServicoDao getDao() {
        if (dao == null) {
            dao = new HistoricoSolicitacaoServicoDao();
        }
        return dao;
    }

	/**
	 * Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 14:40 - ID Citsmart: 176362 -
	 * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
	 * @param databaseAlias
	 * @return dao com o databaseAlias especificado, caso esteja preenchido, sen�o dao com o datasource principal
	 */
	private HistoricoSolicitacaoServicoDao getDao(String databaseAlias) {
		if (UtilStrings.isNotVazio(databaseAlias)) {
			if (daoReports == null) {
				daoReports = new HistoricoSolicitacaoServicoDao(databaseAlias);
			}
			return daoReports;
		}
		return this.getDao();
	}
    
    @Override
    public boolean findHistoricoSolicitacao(final Integer idSolicitacaoServico) throws Exception {
        return this.getDao().findHistoricoSolicitacao(idSolicitacaoServico);
    }

    @Override
    public Collection<HistoricoSolicitacaoServicoDTO> restoreHistoricoServico(final Integer idSolicitacaoServico) throws Exception {
        return this.getDao().restoreHistoricoServico(idSolicitacaoServico);
    }

    @Override
    public Collection<HistoricoSolicitacaoServicoDTO> findResponsavelAtual(final Integer idSolicitacaoServico) throws Exception {
        return this.getDao().findResponsavelAtual(idSolicitacaoServico);
    }

    @Override
    public Collection<RelatorioCargaHorariaDTO> imprimirCargaHorariaUsuario(final SolicitacaoServicoDTO solicitacaoServicoDTO) throws Exception {
    	// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 14:40 - ID Citsmart: 176362 -
   	 	// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
        return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).imprimirCargaHorariaUsuario(solicitacaoServicoDTO);
    }

    @Override
    public Collection<SolicitacaoServicoDTO> imprimirSolicitacaoEncaminhada(final SolicitacaoServicoDTO solicitacaoServicoDTO) throws Exception {
   	 	// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 14:40 - ID Citsmart: 176362 -
   	 	// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
    	return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).imprimirSolicitacaoEncaminhada(solicitacaoServicoDTO);
    }

    @Override
    public Collection<SolicitacaoServicoDTO> imprimirSolicitacaoEncaminhadaFilhas(final SolicitacaoServicoDTO solicitacaoServicoDTO) throws Exception {
    	// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 14:40 - ID Citsmart: 176362 -
   	 	// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
        return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).imprimirSolicitacaoEncaminhadaFilhas(solicitacaoServicoDTO);
    }

    @Override
    public Collection<RelatorioCargaHorariaDTO> imprimirCargaHorariaGrupo(final SolicitacaoServicoDTO solicitacaoServicoDTO) throws Exception {
    	// Desenvolvedor: ibimon.morais - Data: 18/08/2015 - Hor�rio: 14:40 - ID Citsmart: 176362 -
   	 	// Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
        return this.getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).imprimirCargaHorariaGrupo(solicitacaoServicoDTO);
    }

    public static IDto create(final HistoricoSolicitacaoServicoDTO historicoSolicitacaoServicoDTO, final TransactionControler tc) throws Exception {
        IDto historico = new HistoricoSolicitacaoServicoDTO();
        final HistoricoSolicitacaoServicoDao historicoSolicitacaoServicoDao = new HistoricoSolicitacaoServicoDao();
        if (tc != null) {
            historicoSolicitacaoServicoDao.setTransactionControler(tc);
            historico = historicoSolicitacaoServicoDao.create(historicoSolicitacaoServicoDTO);
        }
        return historico;
    }

    public static void update(final HistoricoSolicitacaoServicoDTO historicoSolicitacaoServicoDTO, final TransactionControler tc) throws Exception {
        final HistoricoSolicitacaoServicoDao historicoSolicitacaoServicoDao = new HistoricoSolicitacaoServicoDao();
        if (tc != null) {
            historicoSolicitacaoServicoDao.setTransactionControler(tc);
            historicoSolicitacaoServicoDao.update(historicoSolicitacaoServicoDTO);
        }
    }

}
