/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.sql.Date;
import java.util.Collection;

import br.com.centralit.citcorpore.integracao.NagiosNDOStateHistoryDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class NagiosNDOStateHistoryServiceEjb extends CrudServiceImpl implements NagiosNDOStateHistoryService {

    private String jndiName;

    @Override
    protected NagiosNDOStateHistoryDao getDao() {
        return new NagiosNDOStateHistoryDao(jndiName);
    }

    @Override
    public void setJndiName(final String jndiNameParm) {
        jndiName = jndiNameParm;
    }

    @Override
    public Collection findByObject_id(final Integer parm) throws Exception {
        final NagiosNDOStateHistoryDao dao = new NagiosNDOStateHistoryDao(jndiName);
        try {
            return dao.findByObject_id(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByObject_id(final Integer parm) throws Exception {
        final NagiosNDOStateHistoryDao dao = new NagiosNDOStateHistoryDao(jndiName);
        try {
            dao.deleteByObject_id(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByHostServiceStatus(final String jndiNameParm, final String hostName, final String serviceName, final String status, final Date dataInicial,
            final Date dataFinal) throws Exception {
        final NagiosNDOStateHistoryDao nagiosNDOStateHistoryDao = new NagiosNDOStateHistoryDao(jndiNameParm);
        return nagiosNDOStateHistoryDao.findByHostServiceStatus(hostName, serviceName, status, dataInicial, dataFinal);
    }

    @Override
    public Collection findByHostServiceStatusAndServiceNull(final String jndiNameParm, final String hostName, final String status, final Date dataInicial, final Date dataFinal)
            throws Exception {
        final NagiosNDOStateHistoryDao nagiosNDOStateHistoryDao = new NagiosNDOStateHistoryDao(jndiNameParm);
        return nagiosNDOStateHistoryDao.findByHostServiceStatusAndServiceNull(hostName, status, dataInicial, dataFinal);
    }

}
