/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.centralit.citcorpore.bean.MotivoNegacaoCheckinDTO;
import br.com.centralit.citcorpore.integracao.MotivoNegacaoCheckinDao;
import br.com.citframework.service.CrudServiceImpl;

public class MotivoNegacaoCheckinServiceEjb extends CrudServiceImpl implements MotivoNegacaoCheckinService {

    private static final Logger LOGGER = Logger.getLogger(MotivoNegacaoCheckinServiceEjb.class.getName());

    private MotivoNegacaoCheckinDao dao;

    @Override
    protected MotivoNegacaoCheckinDao getDao() {
        if (dao == null) {
            dao = new MotivoNegacaoCheckinDao();
        }
        return dao;
    }

    @Override
    public boolean hasWithSameName(final MotivoNegacaoCheckinDTO motivo) {
        try {
            return this.getDao().hasWithSameName(motivo);
        } catch (final Exception e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
        return false;
    }

    @Override
    public Collection<MotivoNegacaoCheckinDTO> listAllActiveDeniedReasons() throws Exception {
        return this.getDao().listAllActiveDeniedReasons();
    }

}
