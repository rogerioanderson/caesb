/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.RequisicaoLiberacaoItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaItemConfiguracaoDTO;
import br.com.centralit.citcorpore.integracao.RequisicaoLiberacaoItemConfiguracaoDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.service.CrudServiceImpl;

@SuppressWarnings("rawtypes")
public class RequisicaoLiberacaoItemConfiguracaoServiceEjb extends CrudServiceImpl implements RequisicaoLiberacaoItemConfiguracaoService {

    private RequisicaoLiberacaoItemConfiguracaoDao dao;

    @Override
    protected RequisicaoLiberacaoItemConfiguracaoDao getDao() {
        if (dao == null) {
            dao = new RequisicaoLiberacaoItemConfiguracaoDao();
        }
        return dao;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ArrayList<RequisicaoLiberacaoItemConfiguracaoDTO> listByIdRequisicaoLiberacao(final Integer idRequisicaoLiberacao) throws ServiceException, Exception {
        final ArrayList<Condition> condicoes = new ArrayList<Condition>();

        condicoes.add(new Condition("idRequisicaoLiberacao", "=", idRequisicaoLiberacao));

        return (ArrayList<RequisicaoLiberacaoItemConfiguracaoDTO>) this.getDao().findByCondition(condicoes, null);
    }

    /**
     * Retorna o item de relacionamento espec�fico sem a chave prim�ria da tabela.
     * Uma esp�cie de consulta por chave composta.
     *
     * @param dto
     * @return
     * @throws Exception
     * @throws ServiceException
     */
    @SuppressWarnings("unchecked")
    public RequisicaoMudancaItemConfiguracaoDTO restoreByChaveComposta(final RequisicaoMudancaItemConfiguracaoDTO dto) throws ServiceException, Exception {
        final ArrayList<Condition> condicoes = new ArrayList<Condition>();

        condicoes.add(new Condition("idRequisicaoMudanca", "=", dto.getIdRequisicaoMudanca()));
        condicoes.add(new Condition("idItemConfiguracao", "=", dto.getIdItemConfiguracao()));

        final ArrayList<RequisicaoMudancaItemConfiguracaoDTO> retorno = (ArrayList<RequisicaoMudancaItemConfiguracaoDTO>) this.getDao().findByCondition(condicoes, null);

        if (retorno != null) {
            return retorno.get(0);
        }

        return null;
    }

    @Override
    public Collection findByIdItemConfiguracao(final Integer parm) throws Exception {
        return this.getDao().findByIdItemConfiguracao(parm);
    }

    @Override
    public RequisicaoLiberacaoItemConfiguracaoDTO restoreByChaveComposta(final RequisicaoLiberacaoItemConfiguracaoDTO dto) throws ServiceException, Exception {
        return null;
    }

    @Override
    public Collection findByIdRequisicaoLiberacao(final Integer parm) throws Exception {
        return this.getDao().findByIdRequisicaoLiberacao(parm);
    }

    @Override
    public RequisicaoLiberacaoItemConfiguracaoDTO findByIdReqLiberacao(final Integer parm) throws Exception {
        return this.getDao().findByIdReqLiberacao(parm);
    }

}
