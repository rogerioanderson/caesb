/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.List;

import br.com.centralit.citcorpore.bean.AssociacaoDeviceAtendenteDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.AssociacaoDeviceAtendenteDAO;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.Assert;

public class AssociacaoDeviceAtendenteServiceEjb extends CrudServiceImpl implements AssociacaoDeviceAtendenteService {

    private static final String ASSOCIACAO_MUST_NOT_BE_NULL = "'Associa��o' must not be null.";
    private static final String USUARIO_MUST_NOT_BE_NULL = "'Usu�rio' must not be null.";

    private AssociacaoDeviceAtendenteDAO dao;

    @Override
    protected AssociacaoDeviceAtendenteDAO getDao() {
        if (dao == null) {
            dao = new AssociacaoDeviceAtendenteDAO();
        }
        return dao;
    }

    @Override
    public AssociacaoDeviceAtendenteDTO associateDeviceToAttendant(final AssociacaoDeviceAtendenteDTO associacao, final UsuarioDTO usuario) throws ServiceException {
        Assert.notNull(associacao, ASSOCIACAO_MUST_NOT_BE_NULL);
        List<AssociacaoDeviceAtendenteDTO> associacoes;
        try {
            associacoes = this.getDao().listActiveWithSameProperties(associacao);

            if (associacoes.isEmpty()) {
                return (AssociacaoDeviceAtendenteDTO) this.getDao().create(associacao);
            }
        } catch (final Exception e) {
            throw new ServiceException(e);
        }

        associacao.setId(associacoes.get(0).getId());

        return associacao;
    }

    @Override
    public AssociacaoDeviceAtendenteDTO disassociateDeviceFromAttendant(final AssociacaoDeviceAtendenteDTO associacao, final UsuarioDTO usuario) throws ServiceException {
        Assert.notNull(associacao, ASSOCIACAO_MUST_NOT_BE_NULL);
        Assert.notNull(usuario, USUARIO_MUST_NOT_BE_NULL);
        List<AssociacaoDeviceAtendenteDTO> associacoes;
        try {
            associacoes = this.getDao().listActiveWithSameProperties(associacao);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }

        if (associacoes.size() != 1) {
            throw new ServiceException(this.i18nMessage(usuario.getLocale(), "rest.service.mobile.v2.device.disassociate.associacao.inexistente"));
        }

        final AssociacaoDeviceAtendenteDTO toUpdate = associacoes.get(0);
        toUpdate.setActive(0);

        try {
            this.getDao().update(toUpdate);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
        return toUpdate;
    }

    @Override
    public List<AssociacaoDeviceAtendenteDTO> listActiveAssociationsForUserAndConnection(final UsuarioDTO usuario, final String connection) throws ServiceException {
        Assert.notNull(usuario, USUARIO_MUST_NOT_BE_NULL);
        Assert.notNullAndNotEmpty(connection, "'Connection' must not be null or empty.");
        try {
            return this.getDao().listActiveAssociationForUser(usuario, connection);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
