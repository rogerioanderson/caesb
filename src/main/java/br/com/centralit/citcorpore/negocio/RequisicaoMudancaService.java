/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart
 */
package br.com.centralit.citcorpore.negocio;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.AprovacaoMudancaDTO;
import br.com.centralit.citcorpore.bean.AprovacaoPropostaDTO;
import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;
import br.com.centralit.citcorpore.bean.HistoricoMudancaDTO;
import br.com.centralit.citcorpore.bean.PesquisaRequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaServicoDTO;
import br.com.centralit.citcorpore.bean.ServicoContratoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudService;

@SuppressWarnings("rawtypes")
public interface RequisicaoMudancaService extends CrudService {

    /**
     * Retorna Requisi��es de Mudan�a associados ao conhecimento informado.
     *
     * @param baseConhecimentoDto
     * @return Collection
     * @throws ServiceException
     * @throws LogicException
     * @author Vadoilo Damasceno
     */
    Collection findByConhecimento(final BaseConhecimentoDTO baseConhecimentoDto) throws ServiceException, LogicException;

    ServicoContratoDTO findByIdContratoAndIdServico(final Integer idContrato, final Integer idServico) throws Exception;

    Collection findBySolictacaoServico(final RequisicaoMudancaDTO bean) throws ServiceException, LogicException;

    Collection gravaInformacoesGED(final Collection colArquivosUpload, final int idEmpresa, final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc)
            throws Exception;

    void gravaPlanoDeReversaoGED(final RequisicaoMudancaDTO requisicaomudacaDTO, final TransactionControler tc, final HistoricoMudancaDTO historicoMudancaDTO) throws Exception;

    Collection<RequisicaoMudancaDTO> listaMudancaPorBaseConhecimento(final RequisicaoMudancaDTO mudanca) throws Exception;

    Collection listaQuantidadeMudancaPorImpacto(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception;

    Collection listaQuantidadeMudancaPorPeriodo(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception;

    Collection listaQuantidadeMudancaPorProprietario(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception;

    Collection listaQuantidadeMudancaPorSolicitante(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception;

    Collection listaQuantidadeMudancaPorStatus(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception;

    Collection listaQuantidadeMudancaPorUrgencia(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception;

    Collection listaQuantidadeSemAprovacaoPorPeriodo(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception;

    List<RequisicaoMudancaDTO> listMudancaByIdItemConfiguracao(final Integer idItemConfiguracao) throws Exception;

    String getUrlInformacoesComplementares(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception;

    void updateNotNull(final IDto obj) throws Exception;

    /**
     * Retorna uma lista de solicitacao servico associada a requisi��o mudanca
     *
     * @param bean
     * @return
     * @throws Exception
     * @author thays.araujo
     */
    List<RequisicaoMudancaDTO> listMudancaByIdSolicitacao(final RequisicaoMudancaDTO bean) throws Exception;

    /**
     * Retorna uma lista de requisicao mudanca de acordo com os criterios passados.
     *
     * @param requisicaoMudancaDto
     * @return Collection<RequisicaoMudancaDTO>
     * @throws Exception
     * @author thays.araujo
     */
    Collection<PesquisaRequisicaoMudancaDTO> listRequisicaoMudancaByCriterios(final PesquisaRequisicaoMudancaDTO requisicaoMudancaDto) throws Exception;

    Collection<RequisicaoMudancaDTO> quantidadeMudancaPorBaseConhecimento(final RequisicaoMudancaDTO mudanca) throws Exception;

    /**
     * reativa requisi��o servico
     *
     * @param usuarioDto
     * @param solicitacaoServicoDto
     * @throws Exception
     * @author thays.araujo
     */
    void reativa(final UsuarioDTO usuarioDto, final RequisicaoMudancaDTO RequisicaoMudancaDto) throws Exception;

    RequisicaoMudancaDTO restoreAll(final Integer idRequisicaoMudanca) throws Exception;

    /**
     * suspende a requisi��o mudan�a
     *
     * @param usuarioDto
     * @param solicitacaoServicoDto
     * @throws Exception
     * @author thays.araujo
     */
    void suspende(final UsuarioDTO usuarioDto, final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception;

    /**
     * Retorna verdadeiro ou falso caso a requisi��o esteja aprovada
     *
     * @param requisicaoMudancaDto
     * @param tc
     * @return
     * @throws Exception
     * @author thays.araujo
     */
    boolean validacaoAvancaFluxo(final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) throws Exception;

    /**
     * Retorna se a requisi��o mudan�a esta aprovada, reprovada ou aguardando vota��o
     * 
     * @param requisicaoMudancaDto
     * @param tc
     * @return
     * @throws Exception
     * @author thiago.oliveira
     */
    String verificaAprovacaoProposta(final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) throws Exception;

    String verificaAprovacaoMudanca(final RequisicaoMudancaDTO requisicaoMudancaDto, final TransactionControler tc) throws Exception;

    /**
     * Retorna verdadeiro ou falso caso tipo mudanca esteja associado a requisi��o mudan�a
     *
     * @param idTipoMudanca
     * @return
     * @throws Exception
     * @author geber.costa
     */
    boolean verificarSeRequisicaoMudancaPossuiTipoMudanca(final Integer idTipoMudanca) throws Exception;

    Collection listaQuantidadeERelacionamentos(final HttpServletRequest request, final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception;

    Collection listaIdETituloMudancasPorPeriodo(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws Exception;

    boolean verificaPermissaoGrupoCancelar(final Integer idTipoMudan�a, final Integer idGrupo) throws ServiceException, Exception;

    boolean verificarItensRelacionados(final RequisicaoMudancaDTO requisicaoMudancaDto) throws ServiceException, Exception;

    boolean planoDeReversaoInformado(final RequisicaoMudancaDTO requisicaoMudancaDto, final HttpServletRequest request) throws Exception;

    public Set<AprovacaoPropostaDTO> retornaAprovacoesProposta(RequisicaoMudancaDTO requisicaoMudancaDto,UsuarioDTO usuarioDto,HttpServletRequest request) throws Exception;
    public Set<AprovacaoMudancaDTO> retornaAprovacoesMudanca(RequisicaoMudancaDTO requisicaoMudancaDto,UsuarioDTO usuarioDto,HttpServletRequest request) throws Exception;
    public ArrayList<RequisicaoMudancaItemConfiguracaoDTO> listItensRelacionadosRequisicaoMudanca(final RequisicaoMudancaDTO requisicaoMudancaDTO) throws ServiceException, Exception;

	public Collection<RequisicaoMudancaServicoDTO> listServicosRequisicaoMudanca(RequisicaoMudancaDTO requisicaoMudancaDTO);

	public Collection<SolicitacaoServicoDTO> listaSolicitacoesRequisicaoMudanca(Integer idRequisicaoMudanca);

	public boolean seHoraInicialMenorQAtual(RequisicaoMudancaDTO requisicaoMudancaDto);

	public boolean seHoraFinalMenorQHoraInicial(RequisicaoMudancaDTO requisicaoMudancaDto);

	public boolean seHoraFinalMenorQAtual(RequisicaoMudancaDTO requisicaoMudancaDto);

	public IDto create(IDto model, DocumentHTML document, HttpServletRequest request) throws ServiceException, LogicException;

	public void update(IDto model, DocumentHTML document, HttpServletRequest request) throws ServiceException, LogicException;

}
