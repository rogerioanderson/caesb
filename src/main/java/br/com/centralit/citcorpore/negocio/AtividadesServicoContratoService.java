/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart.
 */
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.HashMap;

import br.com.centralit.citcorpore.bean.AtividadesServicoContratoDTO;
import br.com.citframework.service.CrudService;

@SuppressWarnings("rawtypes")
public interface AtividadesServicoContratoService extends CrudService {

	public Collection findByIdServicoContrato(Integer parm) throws Exception;

	public void deleteByIdServicoContrato(Integer parm) throws Exception;

	/**
	 * Retorna Atividades Ativas do Servi�o Contrato pelo Id do Servi�o
	 * Contrato.
	 * 
	 * @param idServicoContrato
	 * @return atividadesServicoContrato
	 * @throws Exception
	 */
	public Collection obterAtividadesAtivasPorIdServicoContrato(Integer idServicoContrato) throws Exception;
	
	/**
	 * M�todo para atualizar a observacao de os n�o homologadas
	 * 
	 * @param mapFields
	 * @throws Exception
	 */
	public boolean atualizaObservacao(HashMap mapFields) throws Exception;
	
	/**
	 * M�todo para calcular f�rmula
	 * 
	 * @param mapFields
	 * @throws Exception
	 */
	public String calculaFormula(HashMap mapFields) throws Exception;
	
	/**
	 * Verifica se complexidades est�o cadastradas
	 * 
	 * @param mapFields
	 * @throws Exception
	 */
	public boolean verificaComplexidade(HashMap mapFields) throws Exception;
	
	/**
	 * M�todo que retorna os servi�os vinculado ao contrato em quest�o
	 * 
	 * @param mapFields
	 * @throws Exception
	 * @author rodrigo.oliveira
	 */
	public Collection preencheComboServicoContrato(HashMap mapFields, String language) throws Exception;
	
	public Double calculaFormula(AtividadesServicoContratoDTO atividadesServicoContrato) throws Exception;
	
	public Collection listarPorFormula() throws Exception;
}
