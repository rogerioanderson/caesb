/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.HashMap;

import br.com.centralit.citcorpore.bean.ServicoDTO;
import br.com.citframework.service.CrudService;

@SuppressWarnings("rawtypes")
public interface ServicoService extends CrudService {
	public Collection findByIdCategoriaServico(Integer parm) throws Exception;

	public void deleteByIdCategoriaServico(Integer parm) throws Exception;

	public Collection findByIdSituacaoServico(Integer parm) throws Exception;
	
	public void deleteByIdSituacaoServico(Integer parm) throws Exception;

	public Collection findByIdTipoDemandaAndIdCategoria(Integer idTipoDemanda, Integer idCategoria) throws Exception;

	public Collection findByIdServicoAndIdTipoDemandaAndIdCategoria(Integer idServico, Integer idTipoDemanda, Integer idCategoria) throws Exception;

	/**
	 * Retorna Sigla do Servi�o por idOs.
	 * 
	 * @param idOs
	 * @return
	 * @throws Exception
	 */
	public String retornarSiglaPorIdOs(Integer idOs) throws Exception;

	public Collection findByIdTipoDemandaAndIdContrato(Integer idTipoDemanda, Integer idContrato, Integer idCategoria) throws Exception;

	public Collection<ServicoDTO> findByServico(Integer idServico) throws Exception;

	public Collection<ServicoDTO> findByServico(Integer idServico, String nome) throws Exception;

	public Collection<ServicoDTO> listaQuantidadeServicoAnalitico(ServicoDTO servicoDTO) throws Exception;

	public ServicoDTO findByIdServico(Integer idServico) throws Exception;

	public Collection<ServicoDTO> listAtivos() throws Exception;

	public void desvincularServicosRelacionadosTemplate(Integer idTemplate) throws Exception;
	
	public Collection findByNomeAndContratoAndTipoDemandaAndCategoria(Integer idTipoDemanda, Integer idContrato, Integer idCategoria, String nome) throws Exception;

	public ServicoDTO findById(Integer idServico) throws Exception;

	String verificaIdCategoriaServico(HashMap mapFields) throws Exception;
	/**
	 * Retorna uma lista de servicos ativos que ainda n�o foram adicionados a este contrato
	 * @param servicoDto
	 * @return
	 * @throws Exception
	 * @author thays.araujo
	 */
	public Collection<ServicoDTO> listAtivosDiferenteContrato(ServicoDTO servicoDto) throws Exception;
}
