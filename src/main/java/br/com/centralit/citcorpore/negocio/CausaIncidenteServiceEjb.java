/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import br.com.centralit.citcorpore.bean.CausaIncidenteDTO;
import br.com.centralit.citcorpore.integracao.CausaIncidenteDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

@SuppressWarnings({"rawtypes", "unchecked",})
public class CausaIncidenteServiceEjb extends CrudServiceImpl implements CausaIncidenteService {

    private CausaIncidenteDao dao;

    @Override
    protected CausaIncidenteDao getDao() {
        if (dao == null) {
            dao = new CausaIncidenteDao();
        }
        return dao;
    }

    @Override
    public Collection findByIdCausaIncidentePai(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdCausaIncidentePai(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdCausaIncidentePai(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdCausaIncidentePai(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection listHierarquia() throws Exception {
        final Collection colFinal = new ArrayList();
        try {
            final Collection col = this.getDao().findSemPai();
            if (col != null) {
                for (final Iterator it = col.iterator(); it.hasNext();) {
                    final CausaIncidenteDTO causaIncidenteDTO = (CausaIncidenteDTO) it.next();
                    causaIncidenteDTO.setNivel(0);
                    colFinal.add(causaIncidenteDTO);
                    final Collection colAux = this.getCollectionHierarquia(causaIncidenteDTO.getIdCausaIncidente(), 0);
                    if (colAux != null && colAux.size() > 0) {
                        colFinal.addAll(colAux);
                    }
                }
            }
            return colFinal;
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection getCollectionHierarquia(final Integer idCausa, final Integer nivel) throws Exception {
        final Collection col = this.getDao().findByIdPai(idCausa);
        final Collection colFinal = new ArrayList();
        if (col != null) {
            for (final Iterator it = col.iterator(); it.hasNext();) {
                final CausaIncidenteDTO causaIncidenteDTO = (CausaIncidenteDTO) it.next();
                causaIncidenteDTO.setNivel(nivel + 1);
                colFinal.add(causaIncidenteDTO);
                final Collection colAux = this.getCollectionHierarquia(causaIncidenteDTO.getIdCausaIncidente(), causaIncidenteDTO.getNivel());
                if (colAux != null && colAux.size() > 0) {
                    colFinal.addAll(colAux);
                }
            }
        }
        return colFinal;
    }

    @Override
    public Collection listaCausaByDescricaoCausa(final String descricaoCausa) throws Exception {
        return this.getDao().listaCausaByDescricaoCausa(descricaoCausa);
    }

    @Override
    public Collection listaCausasAtivas() throws Exception {
        return this.getDao().listaCausasAtivas();
    }

    /**
     * Foi observado pela equipe GQ, que o com o combo "Causa", trazia "Causas de Incidentes" que haviam sido registrados com data superior a data atual. Afim de corrigir isso, faz-se agora uma
     * filtragem das causas, s� ir�o aparecer as causas que foram registradas com data igual ou anterior a data atual.
     * 
     * @param colCausasIncidentes
     * @author rafael.soyer rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
     * @since 27/04/2015
     */
    public void filtraColCausasIncidentesPorDataAtiva(Collection<CausaIncidenteDTO> colCausasIncidentes) {
        java.util.Date obj_date = new java.util.Date();
        SimpleDateFormat obj_simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String str_dataAtual = obj_simpleDateFormat.format(obj_date);
        java.sql.Date dataAtual = java.sql.Date.valueOf(str_dataAtual);
        ArrayList causasIncidenteDTO_dataSuperiorAAtual_aSeremRemovidas = new ArrayList();
        int resultComparacaoDataIncioEAtual;

        for (CausaIncidenteDTO causaIncidenteDTO : colCausasIncidentes) {
            resultComparacaoDataIncioEAtual = causaIncidenteDTO.getDataInicio().compareTo(dataAtual);

            if (resultComparacaoDataIncioEAtual > 0) {
                causasIncidenteDTO_dataSuperiorAAtual_aSeremRemovidas.add(causaIncidenteDTO);
            }
        }

        colCausasIncidentes.removeAll(causasIncidenteDTO_dataSuperiorAAtual_aSeremRemovidas);
    }

}
