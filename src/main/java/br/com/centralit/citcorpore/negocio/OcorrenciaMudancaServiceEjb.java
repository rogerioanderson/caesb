/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.bpm.dto.ItemTrabalhoFluxoDTO;
import br.com.centralit.citcorpore.bean.JustificativaRequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.OcorrenciaMudancaDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.OcorrenciaMudancaDao;
import br.com.centralit.citcorpore.util.Enumerados.CategoriaOcorrencia;
import br.com.centralit.citcorpore.util.Enumerados.OrigemOcorrencia;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

import com.google.gson.Gson;

/**
 * @author breno.guimaraes
 *
 */
public class OcorrenciaMudancaServiceEjb extends CrudServiceImpl implements OcorrenciaMudancaService {

    private OcorrenciaMudancaDao dao;

    @Override
    protected OcorrenciaMudancaDao getDao() {
        if (dao == null) {
            dao = new OcorrenciaMudancaDao();
        }
        return dao;
    }

    @Override
    public Collection findByIdRequisicaoMudanca(final Integer idRequisicaoMudanca) throws Exception {
        return this.getDao().findByIdRequisicaoMudanca(idRequisicaoMudanca);
    }

    private static final OcorrenciaMudancaDao ocorrenciaMudancaDao = new OcorrenciaMudancaDao();

    public static OcorrenciaMudancaDTO create(final RequisicaoMudancaDTO requisicaoMudancaDto, final ItemTrabalhoFluxoDTO itemTrabalhoFluxoDto, final String ocorrencia,
            final OrigemOcorrencia origem, final CategoriaOcorrencia categoria, final String informacoesContato, final String descricao, final UsuarioDTO usuarioDTO,
            final int tempo, final JustificativaRequisicaoMudancaDTO justificativaDto, final TransactionControler tc) throws Exception {
        final OcorrenciaMudancaDTO ocorrenciaMudancaDTO = new OcorrenciaMudancaDTO();
        ocorrenciaMudancaDTO.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
        ocorrenciaMudancaDTO.setDataregistro(UtilDatas.getDataAtual());
        ocorrenciaMudancaDTO.setHoraregistro(UtilDatas.formatHoraFormatadaStr(UtilDatas.getHoraAtual()));
        ocorrenciaMudancaDTO.setTempoGasto(tempo);
        ocorrenciaMudancaDTO.setDescricao(descricao);
        ocorrenciaMudancaDTO.setDataInicio(UtilDatas.getDataAtual());
        ocorrenciaMudancaDTO.setDataFim(UtilDatas.getDataAtual());
        ocorrenciaMudancaDTO.setInformacoesContato(informacoesContato);
        ocorrenciaMudancaDTO.setRegistradopor(usuarioDTO.getLogin());
        try {
            ocorrenciaMudancaDTO.setDadosMudanca(new Gson().toJson(requisicaoMudancaDto));
        } catch (final Exception e) {
            System.out.println("Problema na grava��o dos dados da ocorr�ncia da mudan�a - Objeto GSON");
            // e.printStackTrace();
        }
        ocorrenciaMudancaDTO.setOcorrencia(ocorrencia);
        ocorrenciaMudancaDTO.setOrigem(origem.getSigla().toString());
        ocorrenciaMudancaDTO.setCategoria(categoria.getSigla());
        if (itemTrabalhoFluxoDto != null) {
            ocorrenciaMudancaDTO.setIdItemTrabalho(itemTrabalhoFluxoDto.getIdItemTrabalho());
        }
        if (justificativaDto != null) {
            ocorrenciaMudancaDTO.setIdJustificativa(justificativaDto.getIdJustificativaMudanca());
            ocorrenciaMudancaDTO.setComplementoJustificativa(justificativaDto.getDescricaoJustificativa());
        }

        if (tc != null) {
            ocorrenciaMudancaDao.setTransactionControler(tc);
        }
        return (OcorrenciaMudancaDTO) ocorrenciaMudancaDao.create(ocorrenciaMudancaDTO);
    }

}
