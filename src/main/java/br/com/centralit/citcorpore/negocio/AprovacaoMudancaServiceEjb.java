/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.AprovacaoMudancaDTO;
import br.com.centralit.citcorpore.integracao.AprovacaoMudancaDao;
import br.com.citframework.service.CrudServiceImpl;

public class AprovacaoMudancaServiceEjb extends CrudServiceImpl implements AprovacaoMudancaService {

    private AprovacaoMudancaDao dao;

    @Override
    protected AprovacaoMudancaDao getDao() {
        if (dao == null) {
            dao = new AprovacaoMudancaDao();
        }
        return dao;
    }

    @Override
    public Collection<AprovacaoMudancaDTO> listaAprovacaoMudancaPorIdRequisicaoMudanca(final Integer idRequisicaoMudanca, final Integer idGrupo, final Integer idEmpregado)
            throws Exception {
        return this.getDao().listaAprovacaoMudancaPorIdRequisicaoMudanca(idRequisicaoMudanca, idGrupo, idEmpregado);
    }

    @Override
    public Integer quantidadeAprovacaoMudancaPorVotoAprovada(final AprovacaoMudancaDTO aprovacao, final Integer idGrupo) throws Exception {
        return this.getDao().quantidadeAprovacaoMudancaPorVotoAprovada(aprovacao, idGrupo);
    }

    @Override
    public Integer quantidadeAprovacaoMudancaPorVotoRejeitada(final AprovacaoMudancaDTO aprovacao, final Integer idGrupo) throws Exception {
        return this.getDao().quantidadeAprovacaoMudancaPorVotoRejeitada(aprovacao, idGrupo);
    }

    @Override
    public Boolean validacaoAprovacaoMudanca(final Integer idRequisicaoMudanca) throws Exception {
        return this.getDao().validacaoAprovacaoMudanca(idRequisicaoMudanca);
    }

    @Override
    public Integer quantidadeAprovacaoMudanca(final AprovacaoMudancaDTO aprovacao, final Integer idGrupo) throws Exception {
        return this.getDao().quantidadeAprovacaoMudanca(aprovacao, idGrupo);
    }

}
