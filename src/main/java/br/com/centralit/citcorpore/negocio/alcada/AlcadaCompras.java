/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio.alcada;
import br.com.centralit.citcorpore.bean.RequisicaoProdutoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bpm.negocio.ExecucaoRequisicaoProduto;
import br.com.centralit.citcorpore.bpm.negocio.ExecucaoSolicitacao;
import br.com.centralit.citcorpore.util.Enumerados.TipoAlcada;
import br.com.citframework.dto.IDto;
import br.com.citframework.integracao.TransactionControler;

public class AlcadaCompras extends AlcadaImpl {

	@Override
	protected TipoAlcada getTipoAlcada() {
		return TipoAlcada.Compras;
	}

	@Override
	protected ExecucaoSolicitacao getExecucaoSolicitacao() {
		return new ExecucaoRequisicaoProduto();
	}

	@Override
	protected String getFinalidade(IDto objetoNegociotDto) {
		return ((RequisicaoProdutoDTO) objetoNegociotDto).getFinalidade();
	}

	@Override
	protected double getValorParaAprovacao(IDto objetoNegociotDto, TransactionControler tc) throws Exception {
		SolicitacaoServicoDTO solicitacaoServicoDto = (SolicitacaoServicoDTO) objetoNegociotDto;

    	return this.getExecucaoSolicitacao().calculaValorParaAprovacao(solicitacaoServicoDto, tc);
	}

}
