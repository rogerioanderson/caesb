/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.bpm.negocio.ItemTrabalho;
import br.com.centralit.citcorpore.bean.AprovacaoSolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.integracao.AprovacaoSolicitacaoServicoDao;
import br.com.centralit.citcorpore.integracao.SolicitacaoServicoDao;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.WebUtil;

@SuppressWarnings("rawtypes")
public class AprovacaoSolicitacaoServicoServiceEjb extends ComplemInfSolicitacaoServicoServiceEjb implements AprovacaoSolicitacaoServicoService {

    private AprovacaoSolicitacaoServicoDao dao;

    @Override
    protected AprovacaoSolicitacaoServicoDao getDao() {
        if (dao == null) {
            dao = new AprovacaoSolicitacaoServicoDao();
        }
        return dao;
    }

    @Override
    public Collection findByIdSolicitacaoServico(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdSolicitacaoServico(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdSolicitacaoServico(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdSolicitacaoServico(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public IDto create(final TransactionControler tc, final SolicitacaoServicoDTO solicitacaoServicoDto, final IDto model) throws Exception {
        return null;
    }

    @Override
    public void delete(final TransactionControler tc, final SolicitacaoServicoDTO solicitacaoServicoDto, final IDto model) throws Exception {

    }

    @Override
    public IDto deserializaObjeto(final String serialize) throws Exception {
        AprovacaoSolicitacaoServicoDTO aprovacaoDto = null;

        if (serialize != null) {
            aprovacaoDto = (AprovacaoSolicitacaoServicoDTO) WebUtil.deserializeObject(AprovacaoSolicitacaoServicoDTO.class, serialize);
        }

        return aprovacaoDto;
    }

    @Override
    public void update(final TransactionControler tc, final SolicitacaoServicoDTO solicitacaoServicoDto, final IDto model) throws Exception {
        this.validaUpdate(solicitacaoServicoDto, model);

        AprovacaoSolicitacaoServicoDTO aprovacaoDto = (AprovacaoSolicitacaoServicoDTO) model;
        this.getDao().setTransactionControler(tc);

        aprovacaoDto.setIdResponsavel(solicitacaoServicoDto.getUsuarioDto().getIdEmpregado());
        aprovacaoDto.setDataHora(UtilDatas.getDataHoraAtual());
        aprovacaoDto.setIdSolicitacaoServico(solicitacaoServicoDto.getIdSolicitacaoServico());
        aprovacaoDto.setIdTarefa(solicitacaoServicoDto.getIdTarefa());
        aprovacaoDto = (AprovacaoSolicitacaoServicoDTO) this.getDao().create(aprovacaoDto);

        solicitacaoServicoDto.setIdUltimaAprovacao(aprovacaoDto.getIdAprovacaoSolicitacaoServico());
        final SolicitacaoServicoDao solicitacaoDao = new SolicitacaoServicoDao();
        solicitacaoDao.setTransactionControler(tc);
        solicitacaoDao.atualizaIdUltimaAprovacao(solicitacaoServicoDto);

        solicitacaoServicoDto.setAprovacao(aprovacaoDto.getAprovacao());
    }

    public void validaAtualizacao(final SolicitacaoServicoDTO solicitacaoServicoDto, final IDto model) throws Exception {
        final AprovacaoSolicitacaoServicoDTO aprovacaoDto = (AprovacaoSolicitacaoServicoDTO) model;

        if (aprovacaoDto.getAprovacao() == null || aprovacaoDto.getAprovacao().trim().length() == 0) {
            throw new LogicException("Aprova��o n�o informada");
        }
        if (aprovacaoDto.getAprovacao().equalsIgnoreCase("N") && aprovacaoDto.getIdJustificativa() == null) {
            throw new LogicException("Justificativa n�o informada");
        }
    }

    @Override
    public void validaCreate(final SolicitacaoServicoDTO solicitacaoServicoDto, final IDto model) throws Exception {
        this.validaAtualizacao(solicitacaoServicoDto, model);
    }

    @Override
    public void validaDelete(final SolicitacaoServicoDTO solicitacaoServicoDto, final IDto model) throws Exception {}

    @Override
    public void validaUpdate(final SolicitacaoServicoDTO solicitacaoServicoDto, final IDto model) throws Exception {
        this.validaAtualizacao(solicitacaoServicoDto, model);
    }

    @Override
    public AprovacaoSolicitacaoServicoDTO findNaoAprovadaBySolicitacaoServico(final SolicitacaoServicoDTO solicitacaoServicoDto) {
        return this.getDao().findNaoAprovadaBySolicitacaoServico(solicitacaoServicoDto);
    }

    @Override
    public void preparaSolicitacaoParaAprovacao(final SolicitacaoServicoDTO solicitacaoDto, final ItemTrabalho itemTrabalho, final String aprovacao, final Integer idJustificativa,
            final String observacoes) throws Exception {
        final AprovacaoSolicitacaoServicoDTO aprovacaoDto = new AprovacaoSolicitacaoServicoDTO();
        aprovacaoDto.setAprovacao(aprovacao);
        if (aprovacao.equalsIgnoreCase("N")) {
            aprovacaoDto.setIdJustificativa(idJustificativa);
            aprovacaoDto.setComplementoJustificativa(observacoes);
        }
        solicitacaoDto.setInformacoesComplementares(aprovacaoDto);
        solicitacaoDto.setAcaoFluxo(br.com.centralit.bpm.util.Enumerados.ACAO_EXECUTAR);
        solicitacaoDto.setIdTarefa(itemTrabalho.getIdItemTrabalho());
    }

	@Override
	public AprovacaoSolicitacaoServicoDTO findByIdTarefa(Integer idTarefa) throws PersistenceException {
		return this.getDao().findByIdTarefa(idTarefa);
	}

}
