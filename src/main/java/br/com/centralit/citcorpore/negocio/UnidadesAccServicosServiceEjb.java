/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citcorpore.bean.UnidadesAccServicosDTO;
import br.com.centralit.citcorpore.integracao.UnidadesAccServicosDao;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.WebUtil;

@SuppressWarnings({"rawtypes", "unchecked"})
public class UnidadesAccServicosServiceEjb extends CrudServiceImpl implements UnidadesAccServicosService {

    private UnidadesAccServicosDao dao;

    @Override
    protected UnidadesAccServicosDao getDao() {
        if (dao == null) {
            dao = new UnidadesAccServicosDao();
        }
        return dao;
    }

    @Override
    public Collection findByIdUnidade(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdUnidade(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdUnidade(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdUnidade(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdServico(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdServico(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdServico(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdServico(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List deserealizaObjetosDoRequest(final HttpServletRequest request) throws Exception {
        return (List) WebUtil.deserializeCollectionFromRequest(UnidadesAccServicosDTO.class, "servicosSerializados", request);
    }

    @Override
    public Collection<UnidadesAccServicosDTO> consultarServicosAtivosPorUnidade(final Integer idUnidade) throws Exception {
        try {
            return this.getDao().consultaServicosPorUnidade(idUnidade);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void excluirAssociacaoServicosUnidade(final Integer idUnidade, final Integer idServico) throws PersistenceException, ServiceException, Exception {
        try {
            this.getDao().deleteByIdServicoUnidade(idUnidade, idServico);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
