/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.LiberacaoMudancaDTO;
import br.com.centralit.citcorpore.integracao.LiberacaoMudancaDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

@SuppressWarnings("rawtypes")
public class LiberacaoMudancaServiceEjb extends CrudServiceImpl implements LiberacaoMudancaService {

    private LiberacaoMudancaDao dao;

    @Override
    protected LiberacaoMudancaDao getDao() {
        if (dao == null) {
            dao = new LiberacaoMudancaDao();
        }
        return dao;
    }

    @Override
    public Collection findByIdLiberacao(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdLiberacao(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdLiberacao(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdLiberacao(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Lista por idrequisicaoliberacao, no caso ele listar� apenas os que o status n�o for nulo.
     * 
     * @author geber.costa
     * @param idrequisicaoliberacao
     * @return
     * @throws ServiceException
     * @throws Exception
     *
     */
    @Override
    public Collection<LiberacaoMudancaDTO> listAll() throws ServiceException, Exception {
        try {
            return this.getDao().listAll();
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdRequisicaoMudanca(final Integer idLiberacao, final Integer idRequisicaoMudanca) throws Exception {
        try {
            return this.getDao().findByIdRequisicaoMudanca(idLiberacao, idRequisicaoMudanca);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
