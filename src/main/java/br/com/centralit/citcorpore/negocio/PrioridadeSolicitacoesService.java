/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.ImpactoDTO;
import br.com.centralit.citcorpore.bean.MatrizPrioridadeDTO;
import br.com.centralit.citcorpore.bean.UrgenciaDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.service.CrudService;

/**
 *
 * @author rodrigo.oliveira
 *
 */
@SuppressWarnings("rawtypes")
public interface PrioridadeSolicitacoesService extends CrudService {

    Collection findById(final Integer idMatrizPrioridade) throws Exception;

    void createImpacto(final IDto impacto) throws Exception;

    void deleteImpacto() throws Exception;

    void createUrgencia(final IDto urgencia) throws Exception;

    void deleteUrgencia() throws Exception;

    void createMatrizPrioridade(final IDto matrizPrioridade) throws Exception;

    void deleteMatrizPrioridade() throws Exception;

    void restaurarGridMatrizPrioridade(final DocumentHTML document, final Collection<MatrizPrioridadeDTO> matrizPrioridade);

    Integer consultaValorPrioridade(final Integer idImpacto, final Integer idUrgencia);

    boolean consultaCadastros() throws Exception;

    Collection consultaImpacto() throws Exception;

    Collection consultaUrgencia() throws Exception;

    Collection consultaMatrizPrioridade() throws Exception;

    IDto restoreImpacto(final IDto impacto) throws Exception;

    IDto restoreImpactoBySigla(final ImpactoDTO impacto) throws Exception;

    IDto restoreUrgencia(final IDto urgencia) throws Exception;

    IDto restoreUrgenciaBySigla(final UrgenciaDTO impacto) throws Exception;

    boolean verificaImpactoJaExiste(final ImpactoDTO impacto) throws Exception;

    boolean verificaUrgenciaJaExiste(final UrgenciaDTO urgencia) throws Exception;

}
