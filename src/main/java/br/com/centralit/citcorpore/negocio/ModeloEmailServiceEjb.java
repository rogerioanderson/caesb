/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.ModeloEmailDTO;
import br.com.centralit.citcorpore.integracao.ModeloEmailDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

@SuppressWarnings("rawtypes")
public class ModeloEmailServiceEjb extends CrudServiceImpl implements ModeloEmailService {

    private ModeloEmailDao dao;

    @Override
    protected ModeloEmailDao getDao() {
        if (dao == null) {
            dao = new ModeloEmailDao();
        }
        return dao;
    }

    @Override
    protected void validaCreate(final Object arg0) throws Exception {
        final ModeloEmailDTO modeloDto = (ModeloEmailDTO) arg0;

        if (modeloDto.getIdentificador() == null) {
            throw new ServiceException(this.i18nMessage("modeloemail.IdentificadorNaoDefinido"));
        }

        if (modeloDto.getIdentificador().indexOf(" ") >= 0) {
            throw new ServiceException(this.i18nMessage("modeloemail.identificadorNaoPodeConterEspacos"));
        }

        final ModeloEmailDTO modeloAuxDto = this.getDao().findByIdentificador(modeloDto.getIdentificador());

        if (modeloAuxDto != null) {
            throw new ServiceException(this.i18nMessage("modeloemail.jaExisteModeloEmailComEsseIdentificador"));
        }
    }

    @Override
    protected void validaUpdate(final Object arg0) throws Exception {
        final ModeloEmailDTO modeloDto = (ModeloEmailDTO) arg0;

        if (modeloDto.getIdentificador().trim() != null) {
            modeloDto.setIdentificador(modeloDto.getIdentificador().trim());
        }

        if (modeloDto.getIdentificador() == null) {
            throw new ServiceException(this.i18nMessage("modeloemail.IdentificadorNaoDefinido"));
        }

        if (modeloDto.getIdentificador().indexOf(" ") >= 0) {
            throw new ServiceException(this.i18nMessage("modeloemail.identificadorNaoPodeConterEspacos"));
        }

        final ModeloEmailDTO modeloAuxDto = this.getDao().findByIdentificador(modeloDto.getIdentificador());

        if (modeloAuxDto != null && modeloAuxDto.getIdModeloEmail().intValue() != modeloDto.getIdModeloEmail().intValue()) {
            throw new ServiceException(this.i18nMessage("modeloemail.jaExisteModeloEmailComEsseIdentificador"));
        }
    }

    @Override
    public Collection getAtivos() throws Exception {
        return this.getDao().getAtivos();
    }

    @Override
    public ModeloEmailDTO findByIdentificador(final String identificador) throws Exception {
        ModeloEmailDTO modeloEmailDTO = null;

        if (identificador != null && !identificador.trim().equals("")) {
            modeloEmailDTO = this.getDao().findByIdentificador(identificador);
        }

        return modeloEmailDTO;
    }

}
