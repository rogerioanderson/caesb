/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.ComplexidadeDTO;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

@SuppressWarnings("rawtypes")
public interface ContratoService extends CrudService {

	public Collection findByIdCliente(Integer parm) throws Exception;

	public Collection findByIdFornecedor(Integer parm) throws Exception;

	public Collection findByIdContrato(Integer parm) throws Exception;

	public void deleteByIdCliente(Integer parm) throws Exception;

	public boolean verificaDataFinalContrato(final HashMap mapFields);
	
	/**
	 * Retorna uma lista de complexidade de acordo com o contrato passado.
	 * 
	 * @param idServicoContrato
	 * @return Collection
	 * @throws Exception
	 */
	public Collection<ComplexidadeDTO> listaComplexidadePorContrato(Integer idServicoContrato) throws Exception;

	public Collection listByIdAcordoNivelServicoAndTipo(Integer idAcordoNivelServicoParm, String tipoParm) throws Exception;

	/**
	 * Lista Contratos Ativos (Situa��o Ativa e DataFim maior que a data Atual).
	 * 
	 * @return Collection<ContratoDTO> - Lista de Contratos Ativos.
	 * @throws Exception
	 * @author valdoilo.damasceno
	 * @since 30.10.2013
	 */
	public Collection<ContratoDTO> listAtivos() throws Exception;

	public Collection findByIdGrupo(Integer idGrupo) throws Exception;

	/**
	 * Retorna a Lista de Contratos (Situa��o Ativa e DataFim maior que a data Atual) que est�o relacionados aos Grupos informados.
	 * 
	 * @param listGrupoDto
	 *            - Lista de GrupoDTO.
	 * @return Collection<ContratoDTO> - Lista de Contratos Ativos encontrados.
	 * @throws Exception
	 * @author valdoilo.damasceno
	 * @since 30.10.2013
	 */
	public Collection<ContratoDTO> findAtivosByGrupos(Collection<GrupoDTO> listGrupoDto) throws Exception;

	/**
	 * Retorna Lista de Contratos Ativos (Situa��o Ativa e DataFim maior que a data Atual) que est�o relacionados aos Grupos do Empregado informado.
	 * 
	 * @param idEmpregado
	 *            - Identificador do Empregado.
	 * @return Collection<ContratoDTO> - Lista de Contratos Ativos.
	 * @throws Exception
	 * @throws ServiceException
	 * @since 30.10.2013
	 */
	public Collection<ContratoDTO> findAtivosByIdEmpregado(Integer idEmpregado) throws ServiceException, Exception;

	String verificaIdCliente(HashMap mapFields) throws Exception;

	String verificaIdFornecedor(HashMap mapFields) throws Exception;

	/**
	 * Retorna a lista de Contratos com o nome da Raz�o Social do Cliente do Contrato.
	 * 
	 * @return Collection<ContratoDTO>
	 * @throws ServiceException
	 * @throws Exception
	 * @since 04.06.2014
	 * @author valdoilo.damasceno
	 */
	public Collection<ContratoDTO> listAtivosWithNomeRazaoSocialCliente() throws ServiceException, Exception;

	public void restauraContrato(Integer idContrato, UsuarioDTO usuarioDto, DocumentHTML document, HttpServletRequest request) throws Exception;

	public void carregarContratos(String editar, UsuarioDTO usuario, DocumentHTML document, HttpServletRequest request) throws Exception;

}
