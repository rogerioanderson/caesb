/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.ControleRendimentoUsuarioDTO;
import br.com.centralit.citcorpore.integracao.ControleRendimentoUsuarioDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class ControleRendimentoUsuarioServiceEjb extends CrudServiceImpl implements ControleRendimentoUsuarioService {

    private ControleRendimentoUsuarioDao dao;

    @Override
    protected ControleRendimentoUsuarioDao getDao() {
        if (dao == null) {
            dao = new ControleRendimentoUsuarioDao();
        }
        return dao;
    }

    @Override
    public Collection<ControleRendimentoUsuarioDTO> findByIdControleRendimentoUsuario(final Integer idGrupo, final String mes, final String ano) throws ServiceException {
        try {
            return this.getDao().findByIdControleRendimentoUsuario(idGrupo, mes, ano);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<ControleRendimentoUsuarioDTO> findByIdControleRendimentoMelhoresUsuario(final Integer idGrupo, final String mesInicio, final String mesFim,
            final String anoInicio, final String anoFim, final Boolean deUmAnoParaOOutro) throws ServiceException {
        try {
            return this.getDao().findByIdControleRendimentoMelhoresUsuario(idGrupo, mesInicio, mesFim, anoInicio, anoFim, deUmAnoParaOOutro);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<ControleRendimentoUsuarioDTO> findIdsControleRendimentoUsuarioPorPeriodo(final Integer idGrupo, final String mesInicio, final String mesFim,
            final String anoInicio, final String anoFim, final Boolean deUmAnoParaOOutro) throws ServiceException {
        try {
            return this.getDao().findIdsControleRendimentoUsuarioPorPeriodo(idGrupo, mesInicio, mesFim, anoInicio, anoFim, deUmAnoParaOOutro);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
