/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.FornecedorDTO;
import br.com.centralit.citcorpore.integracao.FornecedorDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class FornecedorServiceEjb extends CrudServiceImpl implements FornecedorService {

    private FornecedorDao dao;

    @Override
    protected FornecedorDao getDao() {
        if (dao == null) {
            dao = new FornecedorDao();
        }
        return dao;
    }

    @Override
    public Collection<FornecedorDTO> listEscopoFornecimento(final FornecedorDTO fornecedorDto) throws Exception {
        try {
            return this.getDao().listEscopoFornecimento(fornecedorDto);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean consultarCargosAtivos(final FornecedorDTO fornecedor) throws Exception {
        return this.getDao().consultarCargosAtivos(fornecedor);
    }

    @Override
    public boolean excluirFornecedor(final FornecedorDTO fornecedor) throws Exception {
        return this.getDao().excluirFornecedor(fornecedor);
    }

    /**
     * Consulta os fornecedores pelo campo razaoSocial
     * 
     * @param razaoSocial
     * @return
     * @throws Exception
     */
    @Override
    public Collection<FornecedorDTO> consultarFornecedorPorRazaoSocialAutoComplete(final String razaoSocial) throws Exception {
        try {
            return this.getDao().consultarFornecedorPorRazaoSocialAutoComplete(razaoSocial);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @param idFornecedor
     * @return
     * @throws Exception
     * @author mario.haysaki
     */
    @Override
    public boolean existeFornecedorAssociadoContrato(final Integer idFornecedor) throws Exception {
        try {
            return this.getDao().existeFornecedorAssociadoContrato(idFornecedor);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
