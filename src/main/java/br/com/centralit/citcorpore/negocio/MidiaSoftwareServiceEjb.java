/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.List;

import br.com.centralit.citcorpore.bean.MidiaSoftwareChaveDTO;
import br.com.centralit.citcorpore.bean.MidiaSoftwareDTO;
import br.com.centralit.citcorpore.integracao.MidiaSoftwareChaveDao;
import br.com.centralit.citcorpore.integracao.MidiaSoftwareDAO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.service.CrudServiceImpl;

public class MidiaSoftwareServiceEjb extends CrudServiceImpl implements MidiaSoftwareService {

    private MidiaSoftwareDAO dao;

    @Override
    protected MidiaSoftwareDAO getDao() {
        if (dao == null) {
            dao = new MidiaSoftwareDAO();
        }
        return dao;
    }

    @Override
    public IDto create(final IDto model) throws ServiceException, LogicException {
        MidiaSoftwareDTO midiaSoftwareDTO = (MidiaSoftwareDTO) model;
        final MidiaSoftwareDAO dao = this.getDao();

        final MidiaSoftwareChaveDao midiaSoftwareChaveDao = new MidiaSoftwareChaveDao();

        final TransactionControler tc = new TransactionControlerImpl(dao.getAliasDB());

        try {
            midiaSoftwareChaveDao.setTransactionControler(tc);
            tc.start();
            midiaSoftwareDTO = (MidiaSoftwareDTO) dao.create(model);

            if (midiaSoftwareDTO.getMidiaSoftwareChaves() != null && !midiaSoftwareDTO.getMidiaSoftwareChaves().isEmpty()) {
                for (final MidiaSoftwareChaveDTO midiaSoftwareChaveDTO : midiaSoftwareDTO.getMidiaSoftwareChaves()) {
                    midiaSoftwareChaveDTO.setIdMidiaSoftware(midiaSoftwareDTO.getIdMidiaSoftware());
                    midiaSoftwareChaveDao.create(midiaSoftwareChaveDTO);
                }
            }
            tc.commit();
            tc.close();
        } catch (final Exception e) {
            e.printStackTrace();
            this.rollbackTransaction(tc, e);
        }

        return midiaSoftwareDTO;
    }

    @Override
    public void update(final IDto model) throws ServiceException, LogicException {
        final MidiaSoftwareDTO midiaSoftwareDTO = (MidiaSoftwareDTO) model;
        final MidiaSoftwareDAO dao = this.getDao();

        final MidiaSoftwareChaveDao midiaSoftwareChaveDao = new MidiaSoftwareChaveDao();

        final TransactionControler tc = new TransactionControlerImpl(dao.getAliasDB());

        try {
            midiaSoftwareChaveDao.setTransactionControler(tc);
            tc.start();
            dao.update(midiaSoftwareDTO);
            midiaSoftwareChaveDao.deleteByIdMidiaSoftware(midiaSoftwareDTO.getIdMidiaSoftware());
            if (midiaSoftwareDTO.getMidiaSoftwareChaves() != null && !midiaSoftwareDTO.getMidiaSoftwareChaves().isEmpty()) {
                for (final MidiaSoftwareChaveDTO midiaSoftwareChaveDTO : midiaSoftwareDTO.getMidiaSoftwareChaves()) {
                    midiaSoftwareChaveDTO.setIdMidiaSoftware(midiaSoftwareDTO.getIdMidiaSoftware());
                    midiaSoftwareChaveDao.create(midiaSoftwareChaveDTO);
                }
            }
            tc.commit();
            tc.close();
        } catch (final Exception e) {
            e.printStackTrace();
            this.rollbackTransaction(tc, e);
        }
    }

    @Override
    public IDto restore(final IDto model) throws ServiceException, LogicException {
        MidiaSoftwareDTO midiaSoftwareDTO = null;
        try {
            midiaSoftwareDTO = (MidiaSoftwareDTO) this.getDao().restore(model);

            final MidiaSoftwareChaveDTO midiaSoftwareChaveDTO = new MidiaSoftwareChaveDTO();

            midiaSoftwareChaveDTO.setIdMidiaSoftware(midiaSoftwareDTO.getIdMidiaSoftware());
            midiaSoftwareDTO.setMidiaSoftwareChaves((List<MidiaSoftwareChaveDTO>) new MidiaSoftwareChaveDao().findByMidiaSoftware(midiaSoftwareDTO.getIdMidiaSoftware()));

        } catch (final Exception e) {
            e.printStackTrace();
            throw new ServiceException(e);
        }
        return midiaSoftwareDTO;
    }

    @Override
    public boolean consultarMidiasAtivas(final MidiaSoftwareDTO midiaSoftware) throws Exception {
        return this.getDao().consultarMidiasAtivas(midiaSoftware);
    }

}
