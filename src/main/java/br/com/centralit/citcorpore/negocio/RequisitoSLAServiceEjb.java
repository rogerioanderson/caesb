/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.integracao.EmpregadoDao;
import br.com.centralit.citcorpore.integracao.RequisitoSLADao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilStrings;

public class RequisitoSLAServiceEjb extends CrudServiceImpl implements RequisitoSLAService {

    private RequisitoSLADao dao;

    @Override
    protected RequisitoSLADao getDao() {
        if (dao == null) {
            dao = new RequisitoSLADao();
        }
        return dao;
    }

    @Override
    public Collection findByIdEmpregado(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdEmpregado(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdEmpregado(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdEmpregado(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findById(final Integer idRequisitoSla) throws Exception {
        try {
            return this.getDao().findById(idRequisitoSla);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public String verificaIdSolicitante(final HashMap mapFields) throws Exception {
        final EmpregadoDao empregadoDao = new EmpregadoDao();
        List<EmpregadoDTO> listaEmpregado = null;
        String id = mapFields.get("IDEMPREGADO").toString();

        if (id == null || id.equals("")) {
            id = "0";
        }

        if (UtilStrings.soContemNumeros(id.trim())) {
            Integer idEmpregado;
            idEmpregado = Integer.parseInt(id.trim());
            listaEmpregado = empregadoDao.findByIdEmpregado(idEmpregado);
        } else {
            listaEmpregado = empregadoDao.findByNomeEmpregado(id);
        }

        if (listaEmpregado != null && listaEmpregado.size() > 0) {
            return String.valueOf(listaEmpregado.get(0).getIdEmpregado());
        } else {
            return "0";
        }
    }

}
