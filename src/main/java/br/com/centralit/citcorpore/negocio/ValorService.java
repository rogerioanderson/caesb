/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart
 */
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.ItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.TipoItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.ValorDTO;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

/**
 * Service de Valor.
 * 
 * @author valdoilo.damasceno
 * 
 */
@SuppressWarnings("rawtypes")
public interface ValorService extends CrudService {

    /**
     * Consulta valor por idItemConfiguracao.
     * 
     * @param parm
     * @return Collection
     * @throws Exception
     * @author valdoilo.damasceno
     */
    public Collection findByIdItemConfiguracao(Integer parm) throws Exception;

    /**
     * Consulta Valor por idCaracteristica.
     * 
     * @param idCaracteristica
     * @return Collection
     * @throws Exception
     * @author valdoilo.damasceno
     */
    public Collection findByIdCaracteristica(Integer idCaracteristica) throws Exception;

    /**
     * Consulta valor por ItemConfiguracao e TipoItemConfiguracao.
     * 
     * @param itemConfiguracao
     * @param tipoItemConfiguracao
     * @return Collection<ValorDTO>
     * @author valdoilo.damasceno
     * @throws Exception
     * @throws ServiceException
     */
    public Collection<ValorDTO> findByItemAndTipoItemConfiguracao(ItemConfiguracaoDTO itemConfiguracao, TipoItemConfiguracaoDTO tipoItemConfiguracao) throws ServiceException, Exception;

    /**
     * Deleta valor por idItemConfiguracao.
     * 
     * @param idItemConfiguracao
     * @throws Exception
     * @author valdoilo.damasceno
     */
    public void deleteByIdItemConfiguracao(Integer idItemConfiguracao) throws Exception;

    /**
     * Deleta Valor por idCaracteristica.
     * 
     * @param idCaracteristica
     * @throws Exception
     * @author valdoilo.damasceno
     */
    public void deleteByIdCaracteristica(Integer idCaracteristica) throws Exception;

    /**
     * Recupera Valor da Caracter�stica do Item Configuracao.
     * 
     * @param idBaseItemConfiguracao
     * @param idCaracteristica
     * @return ValorDTO
     * @throws Exception
     * @author valdoilo.damasceno
     */
    public ValorDTO restore(Integer idBaseItemConfiguracao, Integer idCaracteristica) throws Exception;
    
    public Collection<ValorDTO> findByItemAndTipoItemConfiguracaoSofware(ItemConfiguracaoDTO itemConfiguracao, TipoItemConfiguracaoDTO tipoItemConfiguracao)  throws Exception;
    
    public ValorDTO restoreItemConfiguracao(Integer idItemConfiguracao, Integer idCaracteristica) throws Exception ;
    public Collection listByItemConfiguracaoAndTagCaracteristica(Integer idItemConfiguracao, String tag) throws Exception;
    public Collection listUniqueValuesByTagCaracteristica(String tag) throws Exception;
}
