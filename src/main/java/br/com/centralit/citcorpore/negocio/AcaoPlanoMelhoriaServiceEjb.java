/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.AcaoPlanoMelhoriaDTO;
import br.com.centralit.citcorpore.integracao.AcaoPlanoMelhoriaDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

@SuppressWarnings("rawtypes")
public class AcaoPlanoMelhoriaServiceEjb extends CrudServiceImpl implements AcaoPlanoMelhoriaService {

    private AcaoPlanoMelhoriaDao dao;

    @Override
    protected AcaoPlanoMelhoriaDao getDao() {
        if (dao == null) {
            dao = new AcaoPlanoMelhoriaDao();
        }
        return dao;
    }

    @Override
    public Collection findByIdPlanoMelhoria(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdPlanoMelhoria(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdPlanoMelhoria(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdPlanoMelhoria(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdObjetivoPlanoMelhoria(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdObjetivoPlanoMelhoria(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdObjetivoPlanoMelhoria(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdObjetivoPlanoMelhoria(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection<AcaoPlanoMelhoriaDTO> listAcaoPlanoMelhoria(final AcaoPlanoMelhoriaDTO acaoPlanoMelhoriaDto) throws Exception {
        try {
            return this.getDao().listAcaoPlanoMelhoria(acaoPlanoMelhoriaDto);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
