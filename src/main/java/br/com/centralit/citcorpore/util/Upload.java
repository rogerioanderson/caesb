/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Created on 15/07/2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package br.com.centralit.citcorpore.util;

import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.Enumerados.CampoUpload;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
 
/**
 * @author CentralIT
 */
public class Upload {
	@SuppressWarnings("deprecation")
	public void doUpload(HttpServletRequest request, Collection colFilesUpload) throws Exception {
//		DiskFileUpload fu = new DiskFileUpload(); 
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload fu = new ServletFileUpload(factory); 
		fu.setSizeMax(-1);
		fu.setFileSizeMax(-1);
//		fu.setSizeThreshold(4096);
//		fu.setRepositoryPath("");
		

		List fileItems = fu.parseRequest(request);
		Iterator i = fileItems.iterator();
		FileItem fi;
		UploadItem upIt;
		File arquivo;
		Iterator itAux = colFilesUpload.iterator();
		while(itAux.hasNext()){
			upIt = (UploadItem)itAux.next();
			while(i.hasNext()){
				fi = (FileItem)i.next();
				if (upIt.getNomeArquivo().toUpperCase().trim().equals(fi.getName().toUpperCase().trim())){
					arquivo = new File(upIt.getPathArquivo() + "\\" + upIt.getNomeArquivo());
					fi.write(arquivo);
				}
			}
		}
	}
	
	
	/**
	 * Modificando a forma de anexar, foi mudado para um m�todo n�o depreciado.
	 * @param request
	 * @return
	 * @throws Exception
	 * @author mario.haysaki
	 */
	public HashMap[] doUploadAll(HttpServletRequest request) throws Exception {
		HashMap[] hshRetorno =  new HashMap[2];
		DiskFileItemFactory  fact = new DiskFileItemFactory(); 
						
		String DISKFILEUPLOAD_REPOSITORYPATH = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.DISKFILEUPLOAD_REPOSITORYPATH,"");
		if(DISKFILEUPLOAD_REPOSITORYPATH == null){
			DISKFILEUPLOAD_REPOSITORYPATH = "";
		}		
		File repositoryPath = new File(DISKFILEUPLOAD_REPOSITORYPATH);
		fact.setRepository(repositoryPath);
				
		ServletFileUpload fu = new ServletFileUpload(fact);

		try {
			/**
			 * @author pedro.lino, Danilo.Lisboa
			 * Necess�rio especificar o encoding, pois quando existe dois ou mais uploads na mesma tela estava vindo com caracteres especiais;
			 * N�O RETIRAR O TRATAMENTO DE ENCODING.
			 * **/
			fu.setHeaderEncoding("iso-8859-1");
			fu.setSizeMax(-1);
			
			hshRetorno[0] = new HashMap();  //Retorna os campos de formul�rio
			hshRetorno[1] = new HashMap();  //Retorna os nomes de arquivos
			
                        if (ServletFileUpload.isMultipartContent(request)) {
            
                            List fileItems = fu.parseRequest(request);
                            Iterator i = fileItems.iterator();
                            FileItem fi;
            
                            String encoding;
            
                            while (i.hasNext()) {
                                fi = (FileItem) i.next();
                                if (!fi.isFormField()) {
                                    hshRetorno[1].put(CITCorporeUtil.getNameFile(fi.getName()), fi);
                                    hshRetorno[0].put(fi.getFieldName().toUpperCase(), CITCorporeUtil.getNameFile(fi.getName()));
                                    request.setAttribute(fi.getFieldName(), CITCorporeUtil.getNameFile(fi.getName()));
                                } else {
                                    // euler.ramos
                                    // Tratando direntes encodings para os campos das telas de upload.
                                    encoding = CampoUpload.buscaEncoding(fi.getFieldName());
                                    if (encoding != null) {
                                        hshRetorno[0].put(fi.getFieldName().toUpperCase(), fi.getString(encoding));
                                        request.setAttribute(fi.getFieldName(), fi.getString(encoding));
                                    } else {
                                        hshRetorno[0].put(fi.getFieldName().toUpperCase(), fi.getString());
                                        request.setAttribute(fi.getFieldName(), fi.getString());
                                    }
                                }
                            }
                        }
		} catch(FileUploadException fileUpExc){
		    fileUpExc.printStackTrace();
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		return hshRetorno;
	}

}
