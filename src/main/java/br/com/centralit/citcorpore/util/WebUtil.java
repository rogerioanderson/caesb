/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.InformacoesContratoItem;
import br.com.centralit.citcorpore.bean.PerfilAcessoMenuDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.PerfilAcessoMenuService;
import br.com.citframework.dto.Usuario;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.Reflexao;
import br.com.citframework.util.UtilI18N;
import br.com.citframework.util.UtilStrings;

public class WebUtil {

    public static void setUsuario(final UsuarioDTO usuario, final HttpServletRequest request) {
        request.getSession().setAttribute(Constantes.getValue("USUARIO_SESSAO") + "_CITCORPORE", usuario);
    }

    public void setLocale(final String locale, final HttpServletRequest request) {
        request.getSession().setAttribute("locale", locale);
    	//request.getSession().setAttribute("locale", getUsuario(request).getLocale());
    }

	public static UsuarioDTO getUsuario(final HttpServletRequest request) {
		final UsuarioDTO user = getUsuarioRequest(request);
		if (user != null) {
			if (request.getSession().getAttribute("locale") != null && !request.getSession().getAttribute("locale").equals("")) {
				user.setLocale((String) request.getSession().getAttribute("locale"));
			} else {
				/**
				 * Se n�o tiver uma l�ngua definida, seta como portugu�s
				 * 
				 * @author thyen.chang
				 * @since 04/02/2015
				 */
				user.setLocale(UtilI18N.PORTUGUESE_SIGLA);
			}
		}
		return user;
	}

	private static UsuarioDTO getUsuarioRequest(final HttpServletRequest request) {
		final UsuarioDTO user = (UsuarioDTO) request.getSession().getAttribute(Constantes.getValue("USUARIO_SESSAO") + "_CITCORPORE");
		return user;
	}

    public static br.com.citframework.dto.Usuario getUsuarioSistema(final HttpServletRequest request) throws Exception {
        final br.com.citframework.dto.Usuario usr = new Usuario();
        final UsuarioDTO usuario = getUsuarioRequest(request);
        if (usuario != null) {
            if (request.getSession().getAttribute("locale") != null && !request.getSession().getAttribute("locale").equals("")) {
                usuario.setLocale((String) request.getSession().getAttribute("locale"));
            } else {
            	/**
            	 * Se n�o tiver uma l�ngua definida, seta como portugu�s
            	 * 
            	 * @author thyen.chang
            	 * @since 04/02/2015
            	 */
                usuario.setLocale(UtilI18N.PORTUGUESE_SIGLA);
            }

            Reflexao.copyPropertyValues(usuario, usr);
        } else {
            return null;
        }

        return usr;
    }

    public static boolean isUserInGroup(final HttpServletRequest req, final String grupo) {
        final UsuarioDTO usuario = WebUtil.getUsuario(req);
        if (usuario == null) {
            return false;
        }

        final String[] grupos = usuario.getGrupos();
        final String grpAux = UtilStrings.nullToVazio(grupo);
        for (final String grupo2 : grupos) {
            if (grupo2 != null) {
                if (grupo2.trim().equalsIgnoreCase(grpAux.trim())) {
                    return true;
                }
            }
        }
        return false;
    }

    @SuppressWarnings("rawtypes")
    public static void renderizaFilhos(final InformacoesContratoItem itemProntuario, final JspWriter out) throws IOException {
        if (itemProntuario.getColSubItens() == null) {
            return;
        }

        out.print("<div id='divMenu_" + itemProntuario.getNome() + "' style='display:none'>");

        final Iterator it = itemProntuario.getColSubItens().iterator();
        InformacoesContratoItem itemProntuarioTemp;
        for (; it.hasNext();) {
            itemProntuarioTemp = (InformacoesContratoItem) it.next();

            final boolean subItens = itemProntuarioTemp.getColSubItens() != null && itemProntuarioTemp.getColSubItens().size() > 0;

            out.print("<table width='100%'>");
            out.print("<tr id='trITEMMENU_" + itemProntuarioTemp.getNome() + "'>");
            out.print("<td width='10%'>&nbsp;</td>");
            out.print("<td id='tdITEMMENU_" + itemProntuarioTemp.getNome() + "' style='cursor:pointer' class='bordaNaoSelecionaProntuario' onclick=\"setaAbaSelecionada('"
                    + itemProntuarioTemp.getNome() + "', " + subItens + ", '" + itemProntuarioTemp.getPath() + "', 'tdITEMMENU_" + itemProntuarioTemp.getNome() + "')\">");
            out.print(itemProntuarioTemp.getDescricao());

            WebUtil.renderizaFilhos(itemProntuarioTemp, out);
            out.print("</td>");
            out.print("</tr>");

            out.println("<script>arrayItensMenu[iItemMenu] = 'tdITEMMENU_" + itemProntuarioTemp.getNome() + "';</script>");
            out.println("<script>iItemMenu++;</script>");

            out.print("<tr><td style='height:5px'></td></tr>");
            out.print("</table>");
        }

        out.print("</div>");
    }

    @SuppressWarnings("rawtypes")
    public static void renderizaFilhosSomenteQuestionarios(final InformacoesContratoItem itemProntuario, final JspWriter out) throws IOException {
        if (itemProntuario.getColSubItens() == null) {
            return;
        }

        out.print("<div id='divMenu2_" + itemProntuario.getNome() + "' style='display:none'>");

        final Iterator it = itemProntuario.getColSubItens().iterator();
        InformacoesContratoItem itemProntuarioTemp;
        for (; it.hasNext();) {
            itemProntuarioTemp = (InformacoesContratoItem) it.next();
            if (!UtilStrings.nullToVazio(itemProntuarioTemp.getFuncItem()).equalsIgnoreCase("1")) {
                continue;
            }

            final boolean subItens = itemProntuarioTemp.getColSubItens() != null && itemProntuarioTemp.getColSubItens().size() > 0;

            out.print("<table width='100%'>");
            out.print("<tr id='trITEMMENU2_" + itemProntuarioTemp.getNome() + "'>");
            out.print("<td width='10%'>&nbsp;</td>");
            out.print("<td id='tdITEMMENU2_" + itemProntuarioTemp.getNome() + "' style='cursor:pointer' class='bordaNaoSelecionaProntuario' onclick=\"setaAbaSel2('"
                    + itemProntuarioTemp.getNome() + "', " + subItens + ", '" + itemProntuarioTemp.getPath() + "', 'tdITEMMENU2_" + itemProntuarioTemp.getNome() + "', false, '"
                    + itemProntuarioTemp.getIdQuestionario() + "')\">");
            out.print(itemProntuarioTemp.getDescricao());

            WebUtil.renderizaFilhosSomenteQuestionarios(itemProntuarioTemp, out);
            out.print("</td>");
            out.print("</tr>");

            out.println("<script>arrayItensMenu2[iItemMenu] = 'tdITEMMENU2_" + itemProntuarioTemp.getNome() + "';</script>");
            out.println("<script>iItemMenu2++;</script>");

            out.print("<tr><td style='height:5px'></td></tr>");
            out.print("</table>");
        }
        out.print("</div>");
    }

    /**
     * Retorna o Id da Empresa.
     *
     * @param request
     * @return
     */
    public static Integer getIdEmpresa(final HttpServletRequest req) {
        final UsuarioDTO usuario = WebUtil.getUsuario(req);
        return usuario.getIdEmpresa();
    }

    public static EmpregadoDTO getColaborador(final HttpServletRequest req) throws Exception {
        final UsuarioDTO user = getUsuarioRequest(req);
        final EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
        return empregadoService.restoreByIdEmpregado(user.getIdEmpregado());
    }

    /**
     * Valida se usuario esta ativo na sessao
     *
     * @param request
     * @return true:usuario esta na sess�o || false:usuario n�o esta na sess�o
     */
    public static Boolean usuarioEstaNaSessao(final HttpServletRequest request) {
        if (WebUtil.getUsuario(request) == null) {
            return false;
        }
        return true;
    }

    /**
     * Valida se o usuario esta na sess�o, podendo direcionar o formulario para tela de login
     *
     * @param request
     * @param document
     * @return true: usuario est� na sessao ||
     *         false: usuario n�o esta na sessao e a tela � redirecionada para a tela de login
     */
    public static Boolean validarSeUsuarioEstaNaSessao(final HttpServletRequest request, final DocumentHTML document) {
        if (!usuarioEstaNaSessao(request)) {
            document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
            document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
            return false;
        }
        return true;
    }

    /**
     * Retorna Linguagem do session do request.
     *
     * @param request
     * @return String - language
     * @author valdoilo.damasceno
     * @since 04.02.2014
     */
    public static String getLanguage(final HttpServletRequest request) {
        String language = UtilI18N.PORTUGUESE_SIGLA;

        if (request != null && request.getSession() != null && request.getSession().getAttribute("locale") != null) {
        	if(getUsuario(request) != null)
        		language = getUsuario(request).getLocale();
        	else
        		language = (String) request.getSession().getAttribute("locale");
        }
        return language.trim().isEmpty() ? UtilI18N.PORTUGUESE_SIGLA : language.trim();
    }

    /**
     * Retorna um n�mero inteiro aleat�rio. M�todo pode ser utilizado para a gera��o do nome de relat�rio.
     *
     * @return int - N�mero aleat�rio.
     * @author valdoilo.damasceno
     */
    public static int getRandomNumber() {
        final Random gerador = new Random();
        return gerador.nextInt();
    }

    /**
     * Obt�m lista de PerfilAcessoMenu do menu que o usu�rio est� tentando acessar
     *
     * @author thyen.chang
     * @since 28/01/2015 - OPERA��O USAIN BOLT
     * @param request
     * @param usuario
     * @param idMenu
     * @return
     * @throws ServiceException
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public static List<PerfilAcessoMenuDTO> getPerfilAcessoUsuarioByMenu(final HttpServletRequest request, final UsuarioDTO usuario, final Integer idMenu) throws ServiceException,
            Exception {
        Map<Integer, List<PerfilAcessoMenuDTO>> mapaPerfilAcessoUsuario = (Map<Integer, List<PerfilAcessoMenuDTO>>) request.getSession().getAttribute(
                Constantes.getValue("USUARIO_SESSAO") + "_PERFILACESSOMENU_CITCORPORE");
        if (mapaPerfilAcessoUsuario == null) {
            final PerfilAcessoMenuService perfilAcessoMenuService = (PerfilAcessoMenuService) ServiceLocator.getInstance().getService(PerfilAcessoMenuService.class, null);
            mapaPerfilAcessoUsuario = perfilAcessoMenuService.getPerfilAcessoBotoesMenu(usuario);
            request.getSession().setAttribute(Constantes.getValue("USUARIO_SESSAO") + "_PERFILACESSOMENU_CITCORPORE", mapaPerfilAcessoUsuario);
        }
        try{
        	return mapaPerfilAcessoUsuario.get(idMenu);
        } catch (NullPointerException e){
        	return new ArrayList<PerfilAcessoMenuDTO>();
        }
    }

}
