/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.util;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

public class Telefone {
	public static String numeroMascarado(String numero, boolean sql){
		StringBuilder resultado = new StringBuilder();
		if (numero.length()<8){
			resultado.append(numero);
		} else {
			switch (numero.length()) {
			case 8:
				resultado.append(format("####-####", numero));
				break;
			case 9:
				resultado.append(format("#####-####", numero));
				break;
			case 10:
				if (!sql){
					resultado.append(format("(0##) ####-####", numero));
				} else {
					resultado.append(format("##) ####-####", numero));
				}
				break;
			case 11:
				if (!sql){
					resultado.append(format("(###) ####-####", numero));
				} else {
					resultado.append(format("###) ####-####", numero));
				}
				break;
			case 12:
				if (!sql){
					resultado.append(format("(###) #####-####", numero));
				} else {
					resultado.append(format("###) #####-####", numero));
				}				
				break;
			case 13:
				if (!sql){
					resultado.append(format("+0# (###) #####-####", numero));
				} else {
					resultado.append(format("# (###) #####-####", numero));
				}
				break;				
			default:
				if (!sql){
					resultado.append(format("+## (###) #####-####", numero));
				} else {
					resultado.append(format("## (###) #####-####", numero));
				}
				break;
			}
		}
		return resultado.toString();
	}
	
	public static String mascaraProcuraSql(String numero){
		StringBuilder resultado = new StringBuilder();
		if (numero.length()<8){
			resultado.append("='"+numero+"'");
		} else {
			resultado.append("like '%");
			resultado.append(numeroMascarado(numero,true));
			resultado.append("'");
		}
		return resultado.toString();
	}
	
	private static String format(String pattern, Object value) {
        MaskFormatter mask;
        try {
			mask = new MaskFormatter(pattern);
	        mask.setValueContainsLiteralCharacters(false);
	        return mask.valueToString(value);
        } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
    }
	
}
