/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.util;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpSession;

public class UtilRelatorio implements Serializable{

	public static Map<String, Object> trataInternacionalizacaoLocale(HttpSession session, Map<String, Object> parametros){
		String localeSession = null;
		if(session.getAttribute("locale") != null){
			localeSession = session.getAttribute("locale").toString();
			if(localeSession.equalsIgnoreCase("pt") || localeSession.trim().equalsIgnoreCase("")){
				Locale locale = new Locale("pt","BR");
				parametros.put("REPORT_LOCALE", locale);
				parametros.put("REPORT_RESOURCE_BUNDLE", ResourceBundle.getBundle("br.com.centralit.citcorpore.Mensagens.Mensagens_relatorio",locale));
			}else if(localeSession.equalsIgnoreCase("en")){
				Locale locale = new Locale("en","US");
				parametros.put("REPORT_LOCALE", locale);
				parametros.put("REPORT_RESOURCE_BUNDLE", ResourceBundle.getBundle("br.com.centralit.citcorpore.Mensagens.Mensagens_relatorio",locale));
			} else{
				Locale locale = new Locale("es","ES");
				parametros.put("REPORT_LOCALE", locale);
				parametros.put("REPORT_RESOURCE_BUNDLE", ResourceBundle.getBundle("br.com.centralit.citcorpore.Mensagens.Mensagens_relatorio",locale));
			}
		}
		else{
			Locale locale = new Locale("pt","BR");
			parametros.put("REPORT_LOCALE", locale);
			parametros.put("REPORT_RESOURCE_BUNDLE", ResourceBundle.getBundle("br.com.centralit.citcorpore.Mensagens.Mensagens_relatorio",locale));
		}
		
		return parametros;
		
	}
	
}
