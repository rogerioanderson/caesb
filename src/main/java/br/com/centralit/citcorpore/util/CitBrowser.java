/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.util;

import javax.servlet.http.HttpServletRequest;

import nl.bitwalker.useragentutils.UserAgent;
import nl.bitwalker.useragentutils.Version;

public class CitBrowser {
	String nome;
	int versao;
	
	public CitBrowser(HttpServletRequest request) {
		String userAgent = request.getHeader("user-agent");
		UserAgent ua = UserAgent.parseUserAgentString(userAgent);
		Version browserVersion = ua.getBrowserVersion();
		this.nome = ua.getBrowser().toString();
		if(browserVersion != null)
			this.versao = Integer.parseInt(browserVersion.getMajorVersion());	
	}

	public String getNome() {
		return nome;
	}

	public int getVersao() {
		return versao;
	}
	
	public int valido(){
		if ((this.nome.contains("IE"))||(this.nome.contains("Explorer"))) {
			if (this.versao<10){
				return 0;
			}
		}
		/*else {
			if (this.nome.contains("Firefox")) {
				if (this.versao<25){
					return -1;
				}
			} else {
				if (this.nome.contains("Chrome")) {
					if (this.versao<30){
						return -2;
					}
				}
			}
		}*/
		return 1;
	}
	
}
