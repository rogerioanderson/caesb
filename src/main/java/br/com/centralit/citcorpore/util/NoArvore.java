/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.util;
import java.util.TreeSet;


/**
 * @author euler.ramos
 *
 */
public class NoArvore implements Comparable<NoArvore>{
	
	private Integer id;
	private String texto;
	private Integer idPai;
	private Integer nivel;
	private TreeSet<NoArvore> filhos;
	
	public NoArvore(Integer id, String texto, Integer idPai) {
		super();
		this.id = id;
		this.texto = texto;
		this.idPai = idPai;
		this.filhos = new TreeSet<NoArvore>();
	}

	public Integer getId() {
		return id;
	}

	public String getTexto() {
		return texto;
	}

	public Integer getIdPai() {
		return idPai;
	}

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}
	
	public TreeSet<NoArvore> getFilhos() {
		return filhos;
	}

	@Override
	public int compareTo(NoArvore outroNoArvore) {
		return this.getTexto().compareTo(outroNoArvore.getTexto());
	}

}
