/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.util;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.citframework.util.UtilFormatacao;

public class FormulasUtil {
    public static double getDoubleValueFromGrid(HttpServletRequest request, int seq, String name){
	String value = request.getParameter(name + UtilFormatacao.formatInt(seq, "00000"));
	if (value == null){
	    return 0;
	}
	value = value.replaceAll("\\.", "");
	value = value.replaceAll("\\,", ".");
	double v = 0;
	try{
	    v = Double.parseDouble(value);
	}catch (Exception e) {
	}
	return v;
    }
    public static void setDoubleValueFromGrid(DocumentHTML document, int seq, String name, double value){
	String valueStr = UtilFormatacao.formatDouble(value, 2);
	document.executeScript("document.getElementById('" + name + UtilFormatacao.formatInt(seq, "00000") + "').value = '" + valueStr + "'");
    }
    public static void lockObjectFromGrid(DocumentHTML document, int seq, String name){
	document.executeScript("document.getElementById('" + name + UtilFormatacao.formatInt(seq, "00000") + "').disabled = 'true'");
    }
}
