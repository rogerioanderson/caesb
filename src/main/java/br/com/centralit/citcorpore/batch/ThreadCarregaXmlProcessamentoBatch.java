/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.batch;

import java.io.File;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import br.com.centralit.citcorpore.bean.ProcessamentoBatchDTO;
import br.com.centralit.citcorpore.negocio.ProcessamentoBatchService;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.citframework.service.ServiceLocator;

public class ThreadCarregaXmlProcessamentoBatch extends Thread {
	public void run() {
		try {
			sleep(3000);
		} catch (InterruptedException e2) {
			e2.printStackTrace();
		}
		try {
			ProcessamentoBatchService processamentoBatchService = (ProcessamentoBatchService) ServiceLocator.getInstance().getService(ProcessamentoBatchService.class, null);
			String separator = System.getProperty("file.separator");
			String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "XMLs"  + separator;
			File file = new File(diretorioReceita + "processamentoBatch.xml");/*
			Collection<ProcessamentoBatchDTO> colProcessamentoBatchDTOs = (Collection<ProcessamentoBatchDTO>) processamentoBatchService.getAtivos();*/
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(file);
			Element elements = doc.getRootElement();
			List<Element> processametoSuperior = elements.getChild("processamentoBatch").getChildren();
			for (Element batchs : processametoSuperior) {
				ProcessamentoBatchDTO processamentoBatchDTO = new ProcessamentoBatchDTO();
				processamentoBatchDTO.setDescricao(batchs.getChildText("descricao").trim());
				processamentoBatchDTO.setConteudo(batchs.getChildText("conteudo").trim());
				if(!processamentoBatchService.existeDuplicidade(processamentoBatchDTO) && !processamentoBatchService.existeDuplicidadeClasse(processamentoBatchDTO)){
					processamentoBatchDTO.setTipo(batchs.getChildText("tipo").trim());
					processamentoBatchDTO.setSituacao(batchs.getChildText("situacao").trim());
					processamentoBatchDTO.setExpressaoCRON("");
					processamentoBatchService.create(processamentoBatchDTO);
				}
				processamentoBatchDTO = null;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
