/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.batch;

import java.io.File;

public class LimpaTemporarios {

    public void executar() throws Exception {
        System.out.println("CITSMart -> INICIANDO PROCESSO DE LIMPEZA DE ARQUIVOS TEMPORARIOS...");

        final String userDir = System.getProperty("user.dir");
        File dir = new File(userDir + "/tempReceitas");
        if (dir.exists() && dir.isDirectory()) {
            this.apagarDoDiretorio(dir.listFiles());
        }
        dir = new File(userDir + "/tempRelatorio");
        if (dir.exists() && dir.isDirectory()) {
            this.apagarDoDiretorio(dir.listFiles());
        }
        dir = new File(userDir + "/tempUpload");
        if (dir.exists() && dir.isDirectory()) {
            this.apagarDoDiretorio(dir.listFiles());
        }
        dir = new File(userDir + "/tempUploadAutoCadastro");
        if (dir.exists() && dir.isDirectory()) {
            this.apagarDoDiretorio(dir.listFiles());
        }
        dir = new File(userDir + "/temporario");
        if (dir.exists() && dir.isDirectory()) {
            this.apagarDoDiretorio(dir.listFiles());
        }
        dir = new File(userDir + "/tempInventario");
        if (dir.exists() && dir.isDirectory()) {
            this.apagarDoDiretorio(dir.listFiles());
        }

        System.out.println("CITSMart -> FINALIZANDO PROCESSO DE LIMPEZA DE ARQUIVOS TEMPORARIOS...");
    }

    private void apagarDoDiretorio(final File[] files) {
        for (final File file : files) {
            if (file.exists() && file.isDirectory()) {
                this.apagarDoDiretorio(file.listFiles());
            } else {
                final String name = file.getName();
                System.out.println("CITSMart -> Preparando para apagar arquivo temporario '" + name + "'...");
                try {
                    file.delete();
                    System.out.println("CITSMart -> arquivo temporario '" + name + "' apagado!");
                } catch (final Exception e) {
                    System.out.println("CITSMart -> Problemas ao apagar o arquivo temporario '" + name + "' !");
                }
            }
        }
    }

    public static void main(final String[] args) throws Exception {
        final LimpaTemporarios l = new LimpaTemporarios();
        l.executar();
    }

}
