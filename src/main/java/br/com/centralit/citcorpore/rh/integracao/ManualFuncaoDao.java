/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.integracao;

import java.util.ArrayList;
import java.util.Collection;

import br.com.centralit.citcorpore.rh.bean.ManualFuncaoDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.util.Constantes;

/**
 * Classe DAO da tabela rh_manualFuncao. Respons�vel por editar as informa��es de RequisicaoFuncao. V�nculo realizado pelo idSolicitacaoServico.
 *
 * @author CentralIT
 *
 */
public class ManualFuncaoDao extends CrudDaoDefaultImpl {

    public ManualFuncaoDao() {
        super(Constantes.getValue("DATABASE_ALIAS"), null);
    }

    @Override
    public String getAliasDB() {
        return super.getAliasDB();
    }

    @Override
    public Collection find(final IDto obj) throws PersistenceException {
        return null;
    }

    @Override
    public Collection<Field> getFields() {
        final Collection<Field> listFields = new ArrayList<>();

        listFields.add(new Field("idManualFuncao", "idManualFuncao", true, true, false, false));
        listFields.add(new Field("idRequisicaoFuncao", "idRequisicaoFuncao", false, false, false, false));
        listFields.add(new Field("idCargo", "idCargo", false, false, false, false));

        listFields.add(new Field("tituloCargo", "tituloCargo", false, false, false, false));
        listFields.add(new Field("tituloFuncao", "tituloFuncao", false, false, false, false));
        listFields.add(new Field("resumoFuncao", "resumoFuncao", false, false, false, false));
        listFields.add(new Field("CBO", "codCBO", false, false, false, false));
        listFields.add(new Field("codigo", "codigo", false, false, false, false));

        listFields.add(new Field("idFormacaoRA", "idFormacaoRA", false, false, false, false));
        listFields.add(new Field("idIdiomaRA", "idIdiomaRA", false, false, false, false));
        listFields.add(new Field("idNivelEscritaRA", "idNivelEscritaRA", false, false, false, false));
        listFields.add(new Field("idNivelLeituraRA", "idNivelLeituraRA", false, false, false, false));
        listFields.add(new Field("idNivelConversaRA", "idNivelConversaRA", false, false, false, false));
        listFields.add(new Field("expAnteriorRA", "expAnteriorRA", false, false, false, false));

        listFields.add(new Field("idFormacaoRF", "idFormacaoRF", false, false, false, false));
        listFields.add(new Field("idIdiomaRF", "idIdiomaRF", false, false, false, false));
        listFields.add(new Field("idNivelEscritaRF", "idNivelEscritaRF", false, false, false, false));
        listFields.add(new Field("idNivelLeituraRF", "idNivelLeituraRF", false, false, false, false));
        listFields.add(new Field("idNivelConversaRF", "idNivelConversaRF", false, false, false, false));
        listFields.add(new Field("expAnteriorRF", "expAnteriorRF", false, false, false, false));

        listFields.add(new Field("pesoComplexidade", "pesoComplexidade", false, false, false, false));
        listFields.add(new Field("pesoTecnica", "pesoTecnica", false, false, false, false));
        listFields.add(new Field("pesoComportamental", "pesoComportamental", false, false, false, false));
        listFields.add(new Field("pesoResultados", "pesoResultados", false, false, false, false));

        return listFields;
    }

    @Override
    public String getTableName() {
        return "rh_manualFuncao";
    }

    @Override
    public Collection list() throws PersistenceException {
        return null;
    }

    @Override
    public Class getBean() {
        return ManualFuncaoDTO.class;
    }

}
