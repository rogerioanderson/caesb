/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.bean;


public class TriagemRequisicaoPessoalDTO extends CurriculoDTO {

	private static final long serialVersionUID = 1L;
	
	private Integer idTriagem;
	private Integer idSolicitacaoServico;
	private Integer idItemTrabalhoEntrevistaRH;
	private Integer idItemTrabalhoEntrevistaGestor;
	private String tipoEntrevista;
	private String existeEntrevistaRH;
	private Integer idSolicitacaoServicoCurriculo;
	
	public Integer getIdSolicitacaoServicoCurriculo() {
		return idSolicitacaoServicoCurriculo;
	}
	public void setIdSolicitacaoServicoCurriculo(
			Integer idSolicitacaoServicoCurriculo) {
		this.idSolicitacaoServicoCurriculo = idSolicitacaoServicoCurriculo;
	}
	public Integer getIdTriagem(){
		return this.idTriagem;
	}
	public void setIdTriagem(Integer parm){
		this.idTriagem = parm;
	}

	public Integer getIdSolicitacaoServico(){
		return this.idSolicitacaoServico;
	}
	public void setIdSolicitacaoServico(Integer parm){
		this.idSolicitacaoServico = parm;
	}
	public Integer getIdItemTrabalhoEntrevistaRH() {
		return idItemTrabalhoEntrevistaRH;
	}
	public void setIdItemTrabalhoEntrevistaRH(Integer idItemTrabalhoEntrevistaRH) {
		this.idItemTrabalhoEntrevistaRH = idItemTrabalhoEntrevistaRH;
	}
	public Integer getIdItemTrabalhoEntrevistaGestor() {
		return idItemTrabalhoEntrevistaGestor;
	}
	public void setIdItemTrabalhoEntrevistaGestor(
			Integer idItemTrabalhoEntrevistaGestor) {
		this.idItemTrabalhoEntrevistaGestor = idItemTrabalhoEntrevistaGestor;
	}
	public String getTipoEntrevista() {
		return tipoEntrevista;
	}
	public void setTipoEntrevista(String tipoEntrevista) {
		this.tipoEntrevista = tipoEntrevista;
	}
	public String getExisteEntrevistaRH() {
		return existeEntrevistaRH;
	}
	public void setExisteEntrevistaRH(String existeEntrevistaRH) {
		this.existeEntrevistaRH = existeEntrevistaRH;
	}


}
