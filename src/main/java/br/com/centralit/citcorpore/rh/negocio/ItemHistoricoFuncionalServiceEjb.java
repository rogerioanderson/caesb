/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.UsuarioDao;
import br.com.centralit.citcorpore.rh.bean.CandidatoDTO;
import br.com.centralit.citcorpore.rh.bean.HistoricoFuncionalDTO;
import br.com.centralit.citcorpore.rh.integracao.CandidatoDao;
import br.com.centralit.citcorpore.rh.integracao.HistoricoFuncionalDao;
import br.com.centralit.citcorpore.rh.integracao.ItemHistoricoFuncionalDao;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.service.CrudServiceImpl;

/**
 * @author david.silva
 *
 */
@SuppressWarnings("rawtypes")
public class ItemHistoricoFuncionalServiceEjb extends CrudServiceImpl implements ItemHistoricoFuncionalService {

    private ItemHistoricoFuncionalDao dao;

    @Override
    protected ItemHistoricoFuncionalDao getDao() {
        if (dao == null) {
            dao = new ItemHistoricoFuncionalDao();
        }
        return dao;
    }

    @Override
    public Collection findByIdItemHistorico(final Integer idHistorico) throws Exception {
        return this.getDao().findByIdItemHistorico(idHistorico);
    }

    @Override
    public void inserirRegistroHistoricoAutomatico(final Integer idCurriculo, final Integer idResponsavel, final String titulo, String descricao, final TransactionControler tc)
            throws Exception {
        final HistoricoFuncionalDao historicoFuncionalDao = new HistoricoFuncionalDao();

        UsuarioDTO usuarioDto = new UsuarioDTO();
        final UsuarioDao usuarioDao = new UsuarioDao();

        usuarioDto = usuarioDao.restoreByIdEmpregado(idResponsavel);

        descricao += usuarioDto.getNomeUsuario();

        final HistoricoFuncionalDTO historicoFuncionalDto = historicoFuncionalDao.restoreByIdCurriculo(idCurriculo);

        this.getDao().inserirRegistroHistoricoAutomatico(historicoFuncionalDto.getIdCandidato(), idResponsavel, titulo, descricao, tc);
    }

    @Override
    public void inserirRegistroHistoricoAutomaticoClassificacao(final Integer idCurriculo, final Integer idResponsavel, final Integer idSolicitacao, final String classificacao)
            throws Exception {
        final HistoricoFuncionalDao historicoFuncionalDao = new HistoricoFuncionalDao();

        CandidatoDTO candidatoDto = new CandidatoDTO();
        final CandidatoDao candidatoDao = new CandidatoDao();

        UsuarioDTO usuarioDto = new UsuarioDTO();
        final UsuarioDao usuarioDao = new UsuarioDao();

        usuarioDto = usuarioDao.restoreByIdEmpregado(idResponsavel);
        final HistoricoFuncionalDTO historicoFuncionalDto = historicoFuncionalDao.restoreByIdCurriculo(idCurriculo);
        candidatoDto = candidatoDao.restoreByID(idCurriculo);

        if (candidatoDto != null && historicoFuncionalDto != null && usuarioDto != null) {
            final String titulo = "Classifica��o - Curr�culo em Processo de Sele��o";
            final StringBuilder descricao = new StringBuilder();

            descricao.append("Candidato ");
            descricao.append(candidatoDto.getNome());

            if (classificacao.equalsIgnoreCase("A")) {
                descricao.append(" foi Aprovado ");
            }
            if (classificacao.equalsIgnoreCase("R")) {
                descricao.append(" foi Reprovado ");
            }
            if (classificacao.equalsIgnoreCase("D")) {
                descricao.append(" Desistiu ");
            }

            descricao.append("no Processo de Sele��o ");

            if (idSolicitacao != null && idSolicitacao > 0) {
                descricao.append("referente a Requisi��o Pessoal N� " + idSolicitacao);
            }

            this.getDao().inserirRegistroHistoricoAutomatico(historicoFuncionalDto.getIdCandidato(), usuarioDto.getIdUsuario(), titulo, descricao.toString(), null);
        }
    }

}
