/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.bean;

import java.sql.Timestamp;

import br.com.citframework.dto.IDto;

public class CargaHorariaDTO implements IDto {
	
	private static final long serialVersionUID = 8926245340324495564L;
	
	private Integer idCargaHoraria;
	private Integer codCargaHor;
	private Integer diaSemana;
	private Integer entrada;
	private Integer saida;
	private String descansoEm;
	private Integer turno;
	private Timestamp dataAlter;
	
	public Integer getIdCargaHoraria() {
		return idCargaHoraria;
	}
	public void setIdCargaHoraria(Integer idCargaHoraria) {
		this.idCargaHoraria = idCargaHoraria;
	}
	public Integer getCodCargaHor() {
		return codCargaHor;
	}
	public void setCodCargaHor(Integer codCargaHor) {
		this.codCargaHor = codCargaHor;
	}
	public Integer getDiaSemana() {
		return diaSemana;
	}
	public void setDiaSemana(Integer diaSemana) {
		this.diaSemana = diaSemana;
	}
	public Integer getEntrada() {
		return entrada;
	}
	public void setEntrada(Integer entrada) {
		this.entrada = entrada;
	}
	public Integer getSaida() {
		return saida;
	}
	public void setSaida(Integer saida) {
		this.saida = saida;
	}
	public String getDescansoEm() {
		return descansoEm;
	}
	public void setDescansoEm(String descansoEm) {
		this.descansoEm = descansoEm;
	}
	public Integer getTurno() {
		return turno;
	}
	public void setTurno(Integer turno) {
		this.turno = turno;
	}
	public Timestamp getDataAlter() {
		return dataAlter;
	}
	public void setDataAlter(Timestamp dataAlter) {
		this.dataAlter = dataAlter;
	}
}
