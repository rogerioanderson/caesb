/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.bean;

import br.com.citframework.dto.IDto;

public class CompetenciaTecnicaDTO implements IDto{

	private static final long serialVersionUID = 1L;
	
	private Integer idCompetencia;
	private String descricao;
	private String situacao;
	
	//variaveis auxiliares, n�o salvas no banco
	private String idNivelCompetenciaAcesso;
	private String idNivelCompetenciaFuncao;
	private String descricaoNivelCompetenciaAcesso;
	private String descricaoNivelCompetenciaFuncao;
	
	public Integer getIdCompetencia() {
		return idCompetencia;
	}
	public void setIdCompetencia(Integer idCompetencia) {
		this.idCompetencia = idCompetencia;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getIdNivelCompetenciaAcesso() {
		return idNivelCompetenciaAcesso;
	}
	public void setIdNivelCompetenciaAcesso(String idNivelCompetenciaAcesso) {
		this.idNivelCompetenciaAcesso = idNivelCompetenciaAcesso;
	}
	public String getIdNivelCompetenciaFuncao() {
		return idNivelCompetenciaFuncao;
	}
	public void setIdNivelCompetenciaFuncao(String idNivelCompetenciaFuncao) {
		this.idNivelCompetenciaFuncao = idNivelCompetenciaFuncao;
	}
	public String getDescricaoNivelCompetenciaAcesso() {
		return descricaoNivelCompetenciaAcesso;
	}
	public void setDescricaoNivelCompetenciaAcesso(String descricaoNivelCompetenciaAcesso) {
		this.descricaoNivelCompetenciaAcesso = descricaoNivelCompetenciaAcesso;
	}
	public String getDescricaoNivelCompetenciaFuncao() {
		return descricaoNivelCompetenciaFuncao;
	}
	public void setDescricaoNivelCompetenciaFuncao(String descricaoNivelCompetenciaFuncao) {
		this.descricaoNivelCompetenciaFuncao = descricaoNivelCompetenciaFuncao;
	}

}
