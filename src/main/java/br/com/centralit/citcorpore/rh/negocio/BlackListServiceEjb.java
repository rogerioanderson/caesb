/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.negocio;

import br.com.centralit.citcorpore.rh.bean.BlackListDTO;
import br.com.centralit.citcorpore.rh.bean.CandidatoDTO;
import br.com.centralit.citcorpore.rh.integracao.BlackListDao;
import br.com.centralit.citcorpore.rh.integracao.CandidatoDao;
import br.com.centralit.citcorpore.rh.integracao.ItemHistoricoFuncionalDao;
import br.com.citframework.service.CrudServiceImpl;

/**
 * @author david.silva
 *
 */
public class BlackListServiceEjb extends CrudServiceImpl implements BlackListService {

    private BlackListDao dao;

    @Override
    protected BlackListDao getDao() {
        if (dao == null) {
            dao = new BlackListDao();
        }
        return dao;
    }

    @Override
    public boolean isCandidatoBlackList(final Integer idCandidato) throws Exception {
        return this.getDao().isCandidatoListaNegra(idCandidato);
    }

    @Override
    public BlackListDTO retornaBlackList(final Integer idCandidato) throws Exception {
        return this.getDao().retornaBlackList(idCandidato);
    }

    /**
     * Desenvolvedor: David Rodrigues - Data: 26/03/2014 - Hor�rio: 14:36 - ID Citsmart: 0
     *
     * Motivo/Coment�rio: Adapta��o no codido para funcionamento do Historico Funcional (Item Historico Funcional)
     */
    @Override
    public void inserirRegistroHistorico(final Integer idCandidato, final Integer idResponsavel, final boolean listaNegra) throws Exception {
        final ItemHistoricoFuncionalDao itemHistoricoFuncionalDao = new ItemHistoricoFuncionalDao();

        String titulo = "";
        final StringBuilder descricao = new StringBuilder();

        final CandidatoDao candidatoDao = new CandidatoDao();
        CandidatoDTO candidatoDto = new CandidatoDTO();

        candidatoDto = candidatoDao.restoreByID(idCandidato);

        descricao.append("Candidato ");

        if (listaNegra) {
            titulo = "Inclus�o na Lista Negra";
            descricao.append(candidatoDto.getNome());
            descricao.append(", foi inclu�do na Lista Negra");
        } else {
            titulo = "Remo��o da Lista Negra";
            descricao.append(candidatoDto.getNome());
            descricao.append(", foi removido da Lista Negra");
        }

        itemHistoricoFuncionalDao.inserirRegistroHistoricoAutomatico(idCandidato, idResponsavel, titulo, descricao.toString(), null);
    }

}
