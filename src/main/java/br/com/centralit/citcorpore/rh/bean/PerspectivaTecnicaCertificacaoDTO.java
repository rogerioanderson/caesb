/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.bean;

import br.com.citframework.dto.IDto;

public class PerspectivaTecnicaCertificacaoDTO implements IDto{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer idPerspectivaTecnicaCertificacao;
	private String descricaoCertificacao;
	private String versaoCertificacao;
	private String obrigatorioCertificacao;
	private Integer idSolicitacaoServico;
	private Integer idCertificacao;
	private String detalhe;
	
	
	public Integer getIdPerspectivaTecnicaCertificacao() {
		return idPerspectivaTecnicaCertificacao;
	}
	public void setIdPerspectivaTecnicaCertificacao(Integer idPerspectivaTecnicaCertificacao) {
		this.idPerspectivaTecnicaCertificacao = idPerspectivaTecnicaCertificacao;
	}
	public String getDescricaoCertificacao() {
		return descricaoCertificacao;
	}
	public void setDescricaoCertificacao(String descricaoCertificacao) {
		this.descricaoCertificacao = descricaoCertificacao;
	}
	
	public String getObrigatorioCertificacao() {
		return obrigatorioCertificacao;
	}
	public void setObrigatorioCertificacao(String obrigatorioCertificacao) {
		this.obrigatorioCertificacao = obrigatorioCertificacao;
	}
	public Integer getIdSolicitacaoServico() {
		return idSolicitacaoServico;
	}
	public void setIdSolicitacaoServico(Integer idSolicitacaoServico) {
		this.idSolicitacaoServico = idSolicitacaoServico;
	}
	public String getVersaoCertificacao() {
		return versaoCertificacao;
	}
	public void setVersaoCertificacao(String versaoCertificacao) {
		this.versaoCertificacao = versaoCertificacao;
	}
	public Integer getIdCertificacao() {
		return idCertificacao;
	}
	public void setIdCertificacao(Integer idCertificacao) {
		this.idCertificacao = idCertificacao;
	}
	/**
	 * @return the detalhe
	 */
	public String getDetalhe() {
		return detalhe;
	}
	/**
	 * @param detalhe the detalhe to set
	 */
	public void setDetalhe(String detalhe) {
		this.detalhe = detalhe;
	}
	
}
