/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/

package br.com.centralit.citcorpore.rh.bean;

import br.com.citframework.dto.IDto;

public class ManualCompetenciaTecnicaDTO implements IDto{

	private static final long serialVersionUID = 1L;
	
	private Integer idManualCompetenciaTecnica;
	private Integer idManualFuncao;
	private Integer idCompetenciaTecnica;
	private Integer idNivelCompetenciaAcesso;
	private Integer idNivelCompetenciaFuncao;
	private String descricao;
	private String situacao;
	private String nomeCompetenciaTecnica;
	private String nomeCompetenciaAcesso;
	private String nomeCompetenciaFuncao;
	
	
	public Integer getIdManualCompetenciaTecnica() {
		return idManualCompetenciaTecnica;
	}
	public void setIdManualCompetenciaTecnica(Integer idManualCompetenciaTecnica) {
		this.idManualCompetenciaTecnica = idManualCompetenciaTecnica;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public Integer getIdManualFuncao() {
		return idManualFuncao;
	}
	public void setIdManualFuncao(Integer idManualFuncao) {
		this.idManualFuncao = idManualFuncao;
	}
	public Integer getIdNivelCompetenciaAcesso() {
		return idNivelCompetenciaAcesso;
	}
	public void setIdNivelCompetenciaAcesso(Integer idNivelCompetenciaAcesso) {
		this.idNivelCompetenciaAcesso = idNivelCompetenciaAcesso;
	}
	public Integer getIdNivelCompetenciaFuncao() {
		return idNivelCompetenciaFuncao;
	}
	public void setIdNivelCompetenciaFuncao(Integer idNivelCompetenciaFuncao) {
		this.idNivelCompetenciaFuncao = idNivelCompetenciaFuncao;
	}
	public String getNomeCompetenciaTecnica() {
		return nomeCompetenciaTecnica;
	}
	public void setNomeCompetenciaTecnica(String nomeCompetenciaTecnica) {
		this.nomeCompetenciaTecnica = nomeCompetenciaTecnica;
	}
	public String getNomeCompetenciaAcesso() {
		return nomeCompetenciaAcesso;
	}
	public void setNomeCompetenciaAcesso(String nomeCompetenciaAcesso) {
		this.nomeCompetenciaAcesso = nomeCompetenciaAcesso;
	}
	public String getNomeCompetenciaFuncao() {
		return nomeCompetenciaFuncao;
	}
	public void setNomeCompetenciaFuncao(String nomeCompetenciaFuncao) {
		this.nomeCompetenciaFuncao = nomeCompetenciaFuncao;
	}
	public Integer getIdCompetenciaTecnica() {
		return idCompetenciaTecnica;
	}
	public void setIdCompetenciaTecnica(Integer idCompetenciaTecnica) {
		this.idCompetenciaTecnica = idCompetenciaTecnica;
	}	
}

