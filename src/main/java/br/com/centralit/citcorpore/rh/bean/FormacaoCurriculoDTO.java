/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.bean;

import br.com.citframework.dto.IDto;

public class FormacaoCurriculoDTO implements IDto,Comparable<FormacaoCurriculoDTO> {
	private Integer idFormacao;
	private Integer idTipoFormacao;
	private Integer idSituacao;
	private String instituicao;
	
	private Integer idPessoa;
	
	private String descricaoTipoFormacao;
	private String descricaoSituacao;
	private String descricao;
	private Integer idCurriculo;
	
	
	
	public Integer getIdCurriculo() {
		return idCurriculo;
	}
	public void setIdCurriculo(Integer idCurriculo) {
		this.idCurriculo = idCurriculo;
	}
	public Integer getIdFormacao() {
		return idFormacao;
	}
	public void setIdFormacao(Integer idFormacao) {
		this.idFormacao = idFormacao;
	}
	public Integer getIdTipoFormacao() {
		return idTipoFormacao;
	}
	public void setIdTipoFormacao(Integer idTipoFormacao) {
		this.idTipoFormacao = idTipoFormacao;
	}
	public Integer getIdSituacao() {
		return idSituacao;
	}
	public void setIdSituacao(Integer idSituacao) {
		this.idSituacao = idSituacao;
	}
	public String getInstituicao() {
		return instituicao;
	}
	public void setInstituicao(String instituicao) {
		this.instituicao = instituicao;
	}
	public Integer getIdPessoa() {
		return idPessoa;
	}
	public void setIdPessoa(Integer idPessoa) {
		this.idPessoa = idPessoa;
	}
	public String getDescricaoTipoFormacao() {
		switch (this.getIdTipoFormacao()) {
		case 1: descricaoTipoFormacao = "Ensino Fundamental";
		break;
		case 2: descricaoTipoFormacao = "Ensino M�dio";
		break;
		case 3: descricaoTipoFormacao = "Gradua��o";
		break;
		case 4: descricaoTipoFormacao = "P�s-Gradua��o";
		break;
		case 5: descricaoTipoFormacao = "Mestrado";
		break;
		case 6: descricaoTipoFormacao = "Doutorado";
		break;
		case 7: descricaoTipoFormacao = "Treinamento";
		}
		return descricaoTipoFormacao;
	}
	public void setDescricaoTipoFormacao(String descricaoTipoFormacao) {
		this.descricaoTipoFormacao = descricaoTipoFormacao;
	}
	public String getDescricaoSituacao() {
		switch (this.getIdSituacao()) {
		case 1: descricaoSituacao = "Cursou";
		break;
		case 2: descricaoSituacao = "Cursando";
		break;
		case 3: descricaoSituacao = "Trancado/Interrompido";
		}
		return descricaoSituacao;
	}
	public void setDescricaoSituacao(String descricaoSituacao) {
		this.descricaoSituacao = descricaoSituacao;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@Override
	public int compareTo(FormacaoCurriculoDTO o) {
		return idTipoFormacao.compareTo(o.getIdTipoFormacao());
		
	}
	
}
