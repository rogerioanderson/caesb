/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.bean;

import br.com.citframework.dto.IDto;

public class PerspectivaTecnicaFormacaoAcademicaDTO implements IDto{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer idPerspectivaTecnicaFormacaoAcademica;
	private String descricaoFormacaoAcademica;
	private String detalheFormacaoAcademica;
	private String obrigatorioFormacaoAcademica;
	private Integer idSolicitacaoServico;
	private Integer idFormacaoAcademica;
		
	
	public Integer getIdPerspectivaTecnicaFormacaoAcademica() {
		return idPerspectivaTecnicaFormacaoAcademica;
	}
	public void setIdPerspectivaTecnicaFormacaoAcademica(Integer idPerspectivaTecnicaFormacaoAcademica) {
		this.idPerspectivaTecnicaFormacaoAcademica = idPerspectivaTecnicaFormacaoAcademica;
	}
	public String getDescricaoFormacaoAcademica() {
		return descricaoFormacaoAcademica;
	}
	public void setDescricaoFormacaoAcademica(String descricaoFormacaoAcademica) {
		this.descricaoFormacaoAcademica = descricaoFormacaoAcademica;
	}
	public String getDetalheFormacaoAcademica() {
		return detalheFormacaoAcademica;
	}
	public void setDetalheFormacaoAcademica(String detalheFormacaoAcademica) {
		this.detalheFormacaoAcademica = detalheFormacaoAcademica;
	}
	public Integer getIdSolicitacaoServico() {
		return idSolicitacaoServico;
	}
	public void setIdSolicitacaoServico(Integer idSolicitacaoServico) {
		this.idSolicitacaoServico = idSolicitacaoServico;
	}
	public String getObrigatorioFormacaoAcademica() {
		return obrigatorioFormacaoAcademica;
	}
	public void setObrigatorioFormacaoAcademica(String obrigatorioFormacaoAcademica) {
		this.obrigatorioFormacaoAcademica = obrigatorioFormacaoAcademica;
	}
	public Integer getIdFormacaoAcademica() {
		return idFormacaoAcademica;
	}
	public void setIdFormacaoAcademica(Integer idFormacaoAcademica) {
		this.idFormacaoAcademica = idFormacaoAcademica;
	}
	
	
}
