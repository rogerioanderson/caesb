/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.ajaxForms;
 
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.rh.bean.CursoDTO;
import br.com.centralit.citcorpore.rh.negocio.CursoService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilI18N;
 
 
public class Curso extends AjaxFormAction {
 
      public Class getBeanClass() {
            return CursoDTO.class;
      }
      
      private boolean validaSessao(DocumentHTML document, HttpServletRequest request) {

    	  UsuarioDTO usuario = WebUtil.getUsuario(request);
    	  
          if (usuario == null) {

                document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
                document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
                
                return false;
          }

    	  return true;
      }
 
      public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	  
    	  if (!this.validaSessao(document, request)) {
    		  
    		  return;
    	  }

          CursoDTO cursosDto = new CursoDTO();
        
          HTMLForm form = document.getForm("form");
          form.setValues(cursosDto);
      }
              
      
      public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	  if (!this.validaSessao(document, request)) {
    		  
    		  return;
    	  }
          
          CursoDTO cursosDto  = (CursoDTO) document.getBean();
          CursoService cursosService = (CursoService) ServiceLocator.getInstance().getService(CursoService.class, null);
          
          if (cursosDto.getIdCurso() != null){
        	  
              cursosService.update(cursosDto);
              
              document.alert(UtilI18N.internacionaliza(request, "MSG06"));
          }else{
        	  
              cursosService.create(cursosDto);
              
              document.alert(UtilI18N.internacionaliza(request, "MSG05"));
          }
          
          document.executeScript("limpar()");
      }
      
      public void delete(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	  if (!this.validaSessao(document, request)) {
    		  
    		  return;
    	  }
          
          CursoDTO cursosDto = (CursoDTO) document.getBean();
          
          CursoService cursosService = (CursoService) ServiceLocator.getInstance().getService(CursoService.class, null);
          cursosDto = (CursoDTO) cursosService.restore(cursosDto);
          
          cursosService.delete(cursosDto);
          document.executeScript("limpar()");
          
          document.alert(UtilI18N.internacionaliza(request, "MSG07")); 
      }
      
      public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	  if (!this.validaSessao(document, request)) {
    		  
    		  return;
    	  }
          
          CursoDTO cursosDto = (CursoDTO) document.getBean();
          if (cursosDto.getIdCurso() == null)
                return;
          
          CursoService cursosService = (CursoService) ServiceLocator.getInstance().getService(CursoService.class, null);
          cursosDto = (CursoDTO) cursosService.restore(cursosDto);
 
          HTMLForm form = document.getForm("form");
          form.setValues(cursosDto);
      } 
      
      public void exibeSelecao(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	  CursoService cursosService = (CursoService) ServiceLocator.getInstance().getService(CursoService.class, null);
        
    	  String idFuncao =  (String) request.getParameter("idFuncao");
	  		Collection<CursoDTO> colCursos = null;
	  		if(idFuncao != null && !idFuncao.equalsIgnoreCase("")){
	  			colCursos = cursosService.findByNotIdFuncao(new Integer(idFuncao));
	  		}else{
	  			document.alert("Selecione uma fun��o!");
	  		}
    	  
          if (colCursos != null) {
          	for (CursoDTO beanDto : colCursos) {
          		document.executeScript("incluirOpcaoSelecao(\""+beanDto.getIdCurso()+"\",\""+beanDto.getDescricao().trim()+"\",\""+beanDto.getDetalhe().trim()+"\");");
  			}
          }
      }
}


