/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.bean;

import br.com.citframework.dto.IDto;

public class PesquisaCurriculoDTO implements IDto {

	private static final long serialVersionUID = 1L;

	private String pesquisa_chave;
	private String pesquisa_formacao;
	private String pesquisa_certificacao;
	private String pesquisa_idiomas;
	private String pesquisa_cidade;
	private String funcao;
	private Integer idCurriculo;
	private Integer idCandidato;
	private Integer idHistorico;
	private Integer idPais;
	
	public String getPesquisa_chave() {
		return pesquisa_chave;
	}
	public void setPesquisa_chave(String pesquisa_chave) {
		this.pesquisa_chave = pesquisa_chave;
	}
	public String getPesquisa_formacao() {
		return pesquisa_formacao;
	}
	public void setPesquisa_formacao(String pesquisa_formacao) {
		this.pesquisa_formacao = pesquisa_formacao;
	}
	public String getPesquisa_certificacao() {
		return pesquisa_certificacao;
	}
	public void setPesquisa_certificacao(String pesquisa_certificacao) {
		this.pesquisa_certificacao = pesquisa_certificacao;
	}
	public String getPesquisa_idiomas() {
		return pesquisa_idiomas;
	}
	public void setPesquisa_idiomas(String pesquisa_idiomas) {
		this.pesquisa_idiomas = pesquisa_idiomas;
	}
	public String getPesquisa_cidade() {
		return pesquisa_cidade;
	}
	public void setPesquisa_cidade(String pesquisa_cidade) {
		this.pesquisa_cidade = pesquisa_cidade;
	}
	public String getFuncao() {
		return funcao;
	}
	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}
	public Integer getIdCurriculo() {
		return idCurriculo;
	}
	public void setIdCurriculo(Integer idCurriculo) {
		this.idCurriculo = idCurriculo;
	}
	public Integer getIdCandidato() {
		return idCandidato;
	}
	public void setIdCandidato(Integer idCandidato) {
		this.idCandidato = idCandidato;
	}
	public Integer getIdHistorico() {
		return idHistorico;
	}
	public void setIdHistorico(Integer idHistorico) {
		this.idHistorico = idHistorico;
	}
	public Integer getIdPais() {
		return idPais;
	}
	public void setIdPais(Integer idPais) {
		this.idPais = idPais;
	}
	
}
