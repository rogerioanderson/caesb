/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.ajaxForms;
 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.rh.bean.AtitudeOrganizacionalDTO;
import br.com.centralit.citcorpore.rh.negocio.AtitudeOrganizacionalService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
 
 
public class AtitudeOrganizacional extends AjaxFormAction {
 
      public Class getBeanClass() {
            return AtitudeOrganizacionalDTO.class;
      }
 
      public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
          UsuarioDTO usuario = WebUtil.getUsuario(request);
          AtitudeOrganizacionalDTO atitudeOrganizacionalDto = new AtitudeOrganizacionalDTO();
        
          HTMLForm form = document.getForm("form");
          form.setValues(atitudeOrganizacionalDto);
 
      }
              
      
      public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	  UsuarioDTO usuario = WebUtil.getUsuario(request);
          if (usuario == null){
                document.alert("Sess�o expirada! Favor efetuar logon novamente!");
                return;
          }
          
          AtitudeOrganizacionalDTO atitudeOrganizacionalDto  = (AtitudeOrganizacionalDTO) document.getBean();
          AtitudeOrganizacionalService atitudeOrganizacionalService = (AtitudeOrganizacionalService) ServiceLocator.getInstance().getService(AtitudeOrganizacionalService.class, null);
          if (atitudeOrganizacionalDto.getIdAtitudeOrganizacional() != null){
        	  atitudeOrganizacionalService.update(atitudeOrganizacionalDto);
          }else{
        	  atitudeOrganizacionalService.create(atitudeOrganizacionalDto);
          }
          
          document.alert("Registro gravado com sucesso!");
          document.executeScript("limpar()");
      }
      
      public void delete(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
          UsuarioDTO usuario = WebUtil.getUsuario(request);
          if (usuario == null){
                document.alert("Sess�o expirada! Favor efetuar logon novamente!");
                return;
          }
          
          AtitudeOrganizacionalDTO atitudeOrganizacionalDto = (AtitudeOrganizacionalDTO) document.getBean();
          
          AtitudeOrganizacionalService atitudeOrganizacionalService = (AtitudeOrganizacionalService) ServiceLocator.getInstance().getService(AtitudeOrganizacionalService.class, null);
          atitudeOrganizacionalDto = (AtitudeOrganizacionalDTO) atitudeOrganizacionalService.restore(atitudeOrganizacionalDto);
          
          atitudeOrganizacionalService.delete(atitudeOrganizacionalDto);
          document.executeScript("limpar()");
          
          document.alert("Solicita��o exclu�da com sucesso!"); 
    }           
      
      public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
          UsuarioDTO usuario = WebUtil.getUsuario(request);
          if (usuario == null){
                document.alert("Sess�o expirada! Favor efetuar logon novamente!");
                return;
          }
          
          AtitudeOrganizacionalDTO atitudeOrganizacionalDto = (AtitudeOrganizacionalDTO) document.getBean();
          if (atitudeOrganizacionalDto.getIdAtitudeOrganizacional() == null)
                return;
          
          AtitudeOrganizacionalService atitudeOrganizacionalService = (AtitudeOrganizacionalService) ServiceLocator.getInstance().getService(AtitudeOrganizacionalService.class, null);
          atitudeOrganizacionalDto = (AtitudeOrganizacionalDTO) atitudeOrganizacionalService.restore(atitudeOrganizacionalDto);
 
          HTMLForm form = document.getForm("form");
          form.setValues(atitudeOrganizacionalDto);
          
    } 
      
}


