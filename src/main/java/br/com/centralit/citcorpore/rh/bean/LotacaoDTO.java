/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

public class LotacaoDTO implements IDto {
	 	private Integer idLotacao;
		private Integer idCargo;
		private Integer idEmpregado;
		private Integer idEmpresa;
		private Integer idFuncao;
		private Date dataInicio;
		private Date dataFim;
		
		private String nomeLotacao;
		
		public Integer getIdLotacao() {
			return idLotacao;
		}
		public void setIdLotacao(Integer idLotacao) {
			this.idLotacao = idLotacao;
		}
		public Integer getIdCargo() {
			return idCargo;
		}
		public void setIdCargo(Integer idCargo) {
			this.idCargo = idCargo;
		}
		public Integer getIdEmpregado() {
			return idEmpregado;
		}
		public void setIdEmpregado(Integer idEmpregado) {
			this.idEmpregado = idEmpregado;
		}
		public Integer getIdEmpresa() {
			return idEmpresa;
		}
		public void setIdEmpresa(Integer idEmpresa) {
			this.idEmpresa = idEmpresa;
		}
		public Integer getIdFuncao() {
			return idFuncao;
		}
		public void setIdFuncao(Integer idFuncao) {
			this.idFuncao = idFuncao;
		}
		public Date getDataInicio() {
			return dataInicio;
		}
		public void setDataInicio(Date dataInicio) {
			this.dataInicio = dataInicio;
		}
		public Date getDataFim() {
			return dataFim;
		}
		public void setDataFim(Date dataFim) {
			this.dataFim = dataFim;
		}
		public String getNomeLotacao() {
			return nomeLotacao;
		}
		public void setNomeLotacao(String nomeLotacao) {
	        this.nomeLotacao = nomeLotacao;
	    }
		
	}
