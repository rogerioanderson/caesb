/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.ajaxForms;
 
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.negocio.JustificativaParecerService;
import br.com.centralit.citcorpore.rh.bean.DescricaoCargoDTO;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
 
 
public class AnaliseSolicitacaoCargo extends SolicitacaoCargo {
	
	  public String getAcao() {
		 return DescricaoCargoDTO.ACAO_ANALISE; 
	  }
	  
	  public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
          JustificativaParecerService justificativaService = (JustificativaParecerService) ServiceLocator.getInstance().getService(JustificativaParecerService.class, WebUtil.getUsuarioSistema(request));
          HTMLSelect idJustificativa = (HTMLSelect) document.getSelectById("idJustificativaValidacao");
          idJustificativa.removeAllOptions();
          idJustificativa.addOption("", "---");
          Collection colJustificativas = justificativaService.listAplicaveisRequisicao();
          if(colJustificativas != null && !colJustificativas.isEmpty())
              idJustificativa.addOptions(colJustificativas, "idJustificativa", "descricaoJustificativa", null);

          super.load(document, request, response); 
      }
	  
      public boolean verificaSolicitacao(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	  DescricaoCargoDTO descricaoCargoDto = (DescricaoCargoDTO) document.getBean();
    	  if (descricaoCargoDto.getIdDescricaoCargo() == null)
      	   return false;
		  return true;
     }    
}
