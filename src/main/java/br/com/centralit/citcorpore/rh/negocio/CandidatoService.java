/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.negocio;

import java.util.Collection;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.rh.bean.CandidatoDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

/**
 * @author thiago.borges
 *
 */
@SuppressWarnings("rawtypes")
public interface CandidatoService extends CrudService {

    /**
     * Exclui candidato.
     *
     * @param model
     * @param document
     * @throws ServiceException
     * @throws Exception
     */
    void deletarCandidato(final IDto model, final DocumentHTML document) throws ServiceException, Exception;

    /**
     * @param obj
     * @return
     * @throws Exception
     * @author Thiago.Borges
     */
    boolean consultarCandidatosAtivos(final CandidatoDTO obj) throws Exception;

    Collection<CandidatoDTO> seCandidatoJaCadastrado(final CandidatoDTO candidatoDTO) throws Exception;

    Collection<CandidatoDTO> listarAtivos() throws Exception;

    Collection findByNome(final String nome) throws Exception;

    Collection findByCpf(final String nome) throws Exception;

    Collection findListTodosCandidatos() throws Exception;

    Collection findByIdCandidatoJoinIdHistorico(final Integer idCandidato) throws Exception;

    Collection recuperaColecaoCandidatos(final CandidatoDTO candidatoDto, final Integer pgAtual, final Integer qtdPaginacao) throws Exception;

    Integer calculaTotalPaginas(final Integer itensPorPagina, final CandidatoDTO candidatoDto) throws Exception;

    // CandidatoDTO create(CandidatoDTO candidatoDto) throws Exception;
    Integer restoreIdCandidato(final Integer idCurriculo) throws Exception;

    CandidatoDTO findByEmail(final String nome) throws Exception;

    CandidatoDTO restoreByCpf(final String nome) throws Exception;

    void updateNotNull(final IDto obj) throws Exception;

    CandidatoDTO findByHashID(final String nome) throws Exception;

    Integer findByCpfCurriculo(final String cpf) throws Exception;

    CandidatoDTO restoreByIdEmpregado(final Integer idEmpregado) throws Exception;

}
