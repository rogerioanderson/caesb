/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.integracao;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.citcorpore.rh.bean.ManualCertificacaoDto;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;

public class ManualCertificacaoDao extends CrudDaoDefaultImpl {

    public ManualCertificacaoDao() {
        super(Constantes.getValue("DATABASE_ALIAS"), null);
    }

    @Override
    public Collection find(final IDto arg0) throws PersistenceException {
        return null;
    }

    @Override
    public Collection<Field> getFields() {
        final Collection<Field> listFields = new ArrayList<>();

        listFields.add(new Field("idManualCertificacao", "idManualCertificacao", true, true, false, false));
        listFields.add(new Field("descricao", "descricao", false, false, false, false));
        listFields.add(new Field("detalhe", "detalhe", false, false, false, false));
        listFields.add(new Field("idManualFuncao", "idManualFuncao", false, false, false, false));
        listFields.add(new Field("RAouRF", "RAouRF", false, false, false, false));

        return listFields;
    }

    @Override
    public String getTableName() {
        return "rh_manualCertificacao";
    }

    @Override
    public Class getBean() {
        return ManualCertificacaoDto.class;
    }

    @Override
    public Collection list() throws PersistenceException {
        final List list = new ArrayList();
        list.add(new Order("descricao"));
        return super.list(list);
    }

    /**
     * Realiza consulta por nome atraves do AutoComplete
     *
     * @param nome
     * @return
     * @throws Exception
     */
    public Collection findByNome(String nome) throws PersistenceException {
        if (nome == null) {
            nome = "";
        }

        String texto = Normalizer.normalize(nome, Normalizer.Form.NFD);
        texto = texto.replaceAll("[^\\p{ASCII}]", "");
        texto = texto.replaceAll("�������������������������Ǵ`^''-+=", "aaaaeeiooouucAAAAEEIOOOUUC ");
        texto = "%" + texto.toUpperCase() + "%";

        final Object[] objs = new Object[] {texto};

        final StringBuilder sql = new StringBuilder();

        sql.append("select  c.idManualCertificacao, c.descricao, c.detalhe, c.idManualFuncao, c.RAouRF ");
        sql.append(" from rh_manualCertificacao c ");
        sql.append(" where upper(c.detalhe) like upper(?) ");
        sql.append(" order by c.detalhe limit 0,10");

        final List list = this.execSQL(sql.toString(), objs);

        final List listRetorno = new ArrayList();
        listRetorno.add("idManualCertificacao");
        listRetorno.add("descricao");
        listRetorno.add("detalhe");
        listRetorno.add("RAouRF");

        return engine.listConvertion(this.getBean(), list, listRetorno);
    }

    public Collection findByIdManualFuncao(final Integer idManualFuncao) throws PersistenceException {

        final StringBuilder sql = new StringBuilder();

        sql.append("select  c.idManualCertificacao, c.descricao, c.detalhe, c.idManualFuncao, c.RAouRF ");
        sql.append(" from rh_manualCertificacao c ");
        sql.append(" where c.idManualFuncao = ?");

        final Object[] objs = new Object[] {idManualFuncao};

        final List list = this.execSQL(sql.toString(), objs);

        final List listRetorno = new ArrayList();
        listRetorno.add("idManualCertificacao");
        listRetorno.add("descricao");
        listRetorno.add("detalhe");
        listRetorno.add("idManualFuncao");
        listRetorno.add("RAouRF");

        final List result = engine.listConvertion(this.getBean(), list, listRetorno);

        return result;

    }

    public void deleteByIdManualFuncaoRA(final Integer idManualFuncao) throws PersistenceException {
        final List condicao = new ArrayList();
        condicao.add(new Condition("idManualFuncao", "=", idManualFuncao));
        condicao.add(new Condition("RAouRF", "=", "RA"));
        super.deleteByCondition(condicao);
    }

    public void deleteByIdManualFuncaoRF(final Integer idManualFuncao) throws PersistenceException {
        final List condicao = new ArrayList();
        condicao.add(new Condition("idManualFuncao", "=", idManualFuncao));
        condicao.add(new Condition("RAouRF", "=", "RF"));
        super.deleteByCondition(condicao);
    }

}
