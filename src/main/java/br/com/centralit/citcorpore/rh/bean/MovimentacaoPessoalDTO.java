/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

public class MovimentacaoPessoalDTO implements IDto {
	
	private Integer numeroSolicitacaoPessoal;
	private String nomeCandidato;
	private Date data;
	private String indicacao;
	private String cargo;
	private String tipoMovimentacao;
	private String lotacao;
	private String horarioTrabalho;
	private Date dataInicio;
	private String excelencia;
	private String aperfeicoamento;
	private String eficacia;
	private String integridade;
	private String resultado;
	private String projeto;
	private String salario;
	private String requisitos;
	private String comprometimento;
	private String iniciativa;
	private String gestor;
	
	private String dataIncioStr;
	private String dataStr;
	
	private Integer idCurriculo;
	private Integer idSolicitacao;
	private Integer idEntrevista;
	
	public Integer getNumeroSolicitacaoPessoal() {
		return numeroSolicitacaoPessoal;
	}
	public void setNumeroSolicitacaoPessoal(Integer numeroSolicitacaoPessoal) {
		this.numeroSolicitacaoPessoal = numeroSolicitacaoPessoal;
	}
	public String getNomeCandidato() {
		return nomeCandidato;
	}
	public void setNomeCandidato(String nomeCandidato) {
		this.nomeCandidato = nomeCandidato;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getIndicacao() {
		return indicacao;
	}
	public void setIndicacao(String indicacao) {
		this.indicacao = indicacao;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public String getTipoMovimentacao() {
		return tipoMovimentacao;
	}
	public void setTipoMovimentacao(String tipoMovimentacao) {
		this.tipoMovimentacao = tipoMovimentacao;
	}
	public String getLotacao() {
		return lotacao;
	}
	public void setLotacao(String lotacao) {
		this.lotacao = lotacao;
	}
	public String getHorarioTrabalho() {
		return horarioTrabalho;
	}
	public void setHorarioTrabalho(String horarioTrabalho) {
		this.horarioTrabalho = horarioTrabalho;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public String getExcelencia() {
		return excelencia;
	}
	public void setExcelencia(String excelencia) {
		this.excelencia = excelencia;
	}
	public String getAperfeicoamento() {
		return aperfeicoamento;
	}
	public void setAperfeicoamento(String aperfeicoamento) {
		this.aperfeicoamento = aperfeicoamento;
	}
	public String getEficacia() {
		return eficacia;
	}
	public void setEficacia(String eficacia) {
		this.eficacia = eficacia;
	}
	public String getIntegridade() {
		return integridade;
	}
	public void setIntegridade(String integridade) {
		this.integridade = integridade;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	public String getProjeto() {
		return projeto;
	}
	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}
	public String getSalario() {
		return salario;
	}
	public void setSalario(String salario) {
		this.salario = salario;
	}
	public String getRequisitos() {
		return requisitos;
	}
	public void setRequisitos(String requisitos) {
		this.requisitos = requisitos;
	}
	public String getComprometimento() {
		return comprometimento;
	}
	public void setComprometimento(String comprometimento) {
		this.comprometimento = comprometimento;
	}
	public String getIniciativa() {
		return iniciativa;
	}
	public void setIniciativa(String iniciativa) {
		this.iniciativa = iniciativa;
	}
	public Integer getIdCurriculo() {
		return idCurriculo;
	}
	public void setIdCurriculo(Integer idCurriculo) {
		this.idCurriculo = idCurriculo;
	}
	public Integer getIdSolicitacao() {
		return idSolicitacao;
	}
	public void setIdSolicitacao(Integer idSolicitacao) {
		this.idSolicitacao = idSolicitacao;
	}
	public Integer getIdEntrevista() {
		return idEntrevista;
	}
	public void setIdEntrevista(Integer idEntrevista) {
		this.idEntrevista = idEntrevista;
	}
	public String getGestor() {
		return gestor;
	}
	public void setGestor(String gestor) {
		this.gestor = gestor;
	}
	public String getDataIncioStr() {
		return dataIncioStr;
	}
	public void setDataIncioStr(String dataIncioStr) {
		this.dataIncioStr = dataIncioStr;
	}
	public String getDataStr() {
		return dataStr;
	}
	public void setDataStr(String dataStr) {
		this.dataStr = dataStr;
	}
	
	
}
