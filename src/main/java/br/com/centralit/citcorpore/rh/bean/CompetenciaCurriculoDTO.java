/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.rh.bean;

import br.com.citframework.dto.IDto;
import br.com.citframework.util.UtilI18N;


public class CompetenciaCurriculoDTO implements IDto {
	private Integer idCompetencia;
	private String descricaoCompetencia;
	private Integer nivelCompetencia;
	private String nivelCompetenciaDesc;
	private Integer idCurriculo;
	
	private String imagemEmailprincipal;
	
	
	public String getImagemEmailprincipal() {
		return imagemEmailprincipal;
	}
	public void setImagemEmailprincipal(String imagemEmailprincipal) {
		this.imagemEmailprincipal = imagemEmailprincipal;
	}
	public Integer getIdCurriculo() {
		return idCurriculo;
	}
	public void setIdCurriculo(Integer idCurriculo) {
		this.idCurriculo = idCurriculo;
	}
	public Integer getIdCompetencia() {
		return idCompetencia;
	}
	public void setIdCompetencia(Integer idCompetencia) {
		this.idCompetencia = idCompetencia;
	}
	public String getDescricaoCompetencia() {
		return descricaoCompetencia;
	}
	public void setDescricaoCompetencia(String descricaoCompetencia) {
		this.descricaoCompetencia = descricaoCompetencia;
	}
	public Integer getNivelCompetencia() {
		return nivelCompetencia;
	}
	public void setNivelCompetencia(Integer nivelCompetencia) {
		this.nivelCompetencia = nivelCompetencia;
		if(this.nivelCompetencia != null) {
			switch(this.nivelCompetencia) {
				case 1:
					this.nivelCompetenciaDesc = UtilI18N.internacionaliza(UtilI18N.getLocale(), "curriculo.idiomaBasico");
				break;
				case 2:
					this.nivelCompetenciaDesc = UtilI18N.internacionaliza(UtilI18N.getLocale(), "curriculo.idiomaIntermediario");
				break;
				case 3:
					this.nivelCompetenciaDesc = UtilI18N.internacionaliza(UtilI18N.getLocale(), "curriculo.idiomaAvancado");
				break;
			}
		} else {
			this.nivelCompetenciaDesc = UtilI18N.internacionaliza(UtilI18N.getLocale(), "rh.naoTem");
		}
	}
	public String getNivelCompetenciaDesc() {
		return nivelCompetenciaDesc;
	}
	public void setNivelCompetenciaDesc(String nivelCompetenciaDesc) {
		this.nivelCompetenciaDesc = nivelCompetenciaDesc;
	}
	
}
