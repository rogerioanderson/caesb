/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.comm.client;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Cliente {
    public static void main(String[] args) {
	// Declaro o ServerSocket (ele que trata todos os pedidos de conexao)
	ServerSocket serv = null;
	try {
	    // Cria o ServerSocket na porta 7000 se estiver dispon�vel (veja na
	    // lista de portas em execucao da maquina servidora)
	    serv = new ServerSocket(7000);

	    while (true) {
		// Declaro o Socket de comunica��o
		Socket s = null;
		// Aguarda uma conex�o na porta especificada e cria retorna o
		// socket
		// que ir� comunicar com o cliente
		s = serv.accept();

		// Cria a tread que vai tratar a comunica��o e deixa a conversa
		// com ela!
		ThreadTrataComunicacao threadTratarComm = new ThreadTrataComunicacao(s);
		threadTratarComm.start();

		// Apos o start da thread, esta apto para receber novos pedidos
		// de conexao!
	    }
	} catch (IOException e) {
	    // Imprime uma notifica��o na sa�da padr�o caso haja algo errado.
	    System.out.println("Algum problema ocorreu para criar ou receber o socket.");

	} finally {
	    try {
		// Encerra o ServerSocket
	    	if(serv != null){
	    		serv.close();
	    	}
	    } catch (IOException e) {
	    }
	}
    }
}
