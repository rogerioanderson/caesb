/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.comm.server;

import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Date;

import br.com.centralit.citcorpore.ajaxForms.Inventario;
import br.com.centralit.citcorpore.negocio.InventarioXMLService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;

public class ServidorSocket extends Thread implements ClientServer {
	
	@Override
	public void run() {
		ServerSocket server = null;

		try {
			server = new ServerSocket(CLIENT_PORT);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			while (true) {
				Socket socket = null;
				socket = server.accept();
				
				String ipCliente = socket.getRemoteSocketAddress().toString();
				System.out.println(socket.getRemoteSocketAddress().toString());
				System.out.println(socket.getLocalAddress());
				System.out.println(socket.getLocalSocketAddress());
				String ipParaBusca = "";
				
				
				/* Trata o IP. */
				ipCliente = ipCliente.replaceAll("/", "");
				String[] array = ipCliente.split(":");
				ipCliente = array[0];
				ipParaBusca = ipCliente.replace(".", "-");
				
				InventarioXMLService inventarioXMLService = (InventarioXMLService) ServiceLocator.getInstance().getService(InventarioXMLService.class, null);
				/*ParametroCorporeService parametroService = (ParametroCorporeService) ServiceLocator.getInstance().getService(ParametroCorporeService.class, null);
				ParametroCorporeDTO paramentroDTO = null;
				
				List<ParametroCorporeDTO> listDiasInventario = parametroService.pesquisarParamentro(Enumerados.ParametroSistema.DiasInventario.id(), Enumerados.ParametroSistema.DiasInventario.campo());
		
				String diasInventario = "";
				
				if ((paramentroDTO = (ParametroCorporeDTO) listDiasInventario.get(0)).getValor() != null) {
					diasInventario = (paramentroDTO = (ParametroCorporeDTO) listDiasInventario.get(0)).getValor();
				}*/
				
				//Date dataInventario = UtilDatas.getSqlDate(UtilDatas.incrementaDiasEmData(Util.getDataAtual(), -(new Integer(diasInventario.trim()))));
				Date dataInventario = UtilDatas.getSqlDate(new java.util.Date());
						
				boolean inventarioAtualizado = inventarioXMLService.inventarioAtualizado(ipParaBusca,dataInventario);
				
				if(!inventarioAtualizado){
					Inventario inventario = new Inventario();
					inventario.InventarioAutomatico(ipCliente);
				}
			}
		} catch (ServiceException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
