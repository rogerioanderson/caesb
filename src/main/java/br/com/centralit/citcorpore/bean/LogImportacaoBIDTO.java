/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Timestamp;

import br.com.citframework.dto.IDto;

public class LogImportacaoBIDTO implements IDto {

	private static final long serialVersionUID = 1L;
	private Integer idLogImportacao;
	private Timestamp dataHoraInicio;
	private Timestamp dataHoraFim;
	private String status;
	private StringBuilder detalhamento;
	private String tipo;
	private Integer idConexaoBI;
	private Integer paginaSelecionada;
	
	public LogImportacaoBIDTO() {
		super();
		this.detalhamento = new StringBuilder();
	}
	
	public Integer getIdLogImportacao() {
		return idLogImportacao;
	}
	public void setIdLogImportacao(Integer idLogImportacao) {
		this.idLogImportacao = idLogImportacao;
	}
	public Timestamp getDataHoraInicio() {
		return dataHoraInicio;
	}
	public void setDataHoraInicio(Timestamp dataHoraInicio) {
		this.dataHoraInicio = dataHoraInicio;
	}
	public Timestamp getDataHoraFim() {
		return dataHoraFim;
	}
	public void setDataHoraFim(Timestamp dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDetalhamento() {
		return detalhamento.toString();
	}
	public void setDetalhamento(String detalhamento) {
		this.clear();
		this.concatDetalhamento(detalhamento);
	}
	public void concatDetalhamento(String detalhamento){
		if (this.detalhamento.length()>0){
			this.detalhamento.append("\r\n");
		}
		this.detalhamento.append(detalhamento);
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Integer getIdConexaoBI() {
		return idConexaoBI;
	}
	public void setIdConexaoBI(Integer idConexaoBI) {
		this.idConexaoBI = idConexaoBI;
	}
	public Integer getPaginaSelecionada() {
		return paginaSelecionada;
	}
	public void setPaginaSelecionada(Integer paginaSelecionada) {
		this.paginaSelecionada = paginaSelecionada;
	}
	public void clear(){
		this.detalhamento.delete(0, this.detalhamento.length());
	}
	
}
