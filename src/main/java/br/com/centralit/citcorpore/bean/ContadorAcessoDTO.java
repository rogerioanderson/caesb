/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Timestamp;

import br.com.citframework.dto.IDto;

@SuppressWarnings("serial")
public class ContadorAcessoDTO implements IDto {

	private Integer idContadorAcesso;
	private Integer idUsuario;
	private Integer idBaseConhecimento;
	private Integer contadorAcesso;
	private Timestamp dataHoraAcesso;

	/**
	 * @return the idContadorAcesso
	 */
	public Integer getIdContadorAcesso() {
		return idContadorAcesso;
	}

	/**
	 * @param idContadorAcesso
	 *            the idContadorAcesso to set
	 */
	public void setIdContadorAcesso(Integer idContadorAcesso) {
		this.idContadorAcesso = idContadorAcesso;
	}

	/**
	 * @return the idUsuario
	 */
	public Integer getIdUsuario() {
		return idUsuario;
	}

	/**
	 * @param idUsuario
	 *            the idUsuario to set
	 */
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	/**
	 * @return the idBaseConhecimento
	 */
	public Integer getIdBaseConhecimento() {
		return idBaseConhecimento;
	}

	/**
	 * @param idBaseConhecimento
	 *            the idBaseConhecimento to set
	 */
	public void setIdBaseConhecimento(Integer idBaseConhecimento) {
		this.idBaseConhecimento = idBaseConhecimento;
	}

	/**
	 * @return the contadorAcesso
	 */
	public Integer getContadorAcesso() {
		return contadorAcesso;
	}

	/**
	 * @param contadorAcesso
	 *            the contadorAcesso to set
	 */
	public void setContadorAcesso(Integer contadorAcesso) {
		this.contadorAcesso = contadorAcesso;
	}

	/**
	 * @return the dataHoraAcesso
	 */
	public Timestamp getDataHoraAcesso() {
		return dataHoraAcesso;
	}

	/**
	 * @param dataHoraAcesso
	 *            the dataHoraAcesso to set
	 */
	public void setDataHoraAcesso(Timestamp dataHoraAcesso) {
		this.dataHoraAcesso = dataHoraAcesso;
	}

}
