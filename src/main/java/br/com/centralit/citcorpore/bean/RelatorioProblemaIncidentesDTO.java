/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;
import java.util.Collection;

import br.com.citframework.dto.IDto;

public class RelatorioProblemaIncidentesDTO implements IDto{


	private static final long serialVersionUID = 1L;
	
	private Integer idProblema;
	private String tituloProblema;
	private String descricao;
	private Integer idContrato;
	private String contrato;
	private Collection<SolicitacaoServicoDTO> colSolicitacaoServico;
	
	private Date dataInicio;
	private Date dataFim;
	private String proprietario;
	private String formatoArquivoRelatorio;
	
	//Desenvolvedor: ibimon.morais Data: 13/08/2015 Hor�rio: 15h16min Solicita��o: 176362 Motivo/Coment�rio: Altera��o feita para melhorar
	// a consulta de dados para relatorio passando a consultar em um novo banco de dados espelhado, de acordo com citsmart.cfg
	private boolean baseConsultaReports;
	
	private Integer totalSolicitacaoServicoPorProblema;
	
	public Integer getIdProblema() {
		return idProblema;
	}
	public void setIdProblema(Integer idProblema) {
		this.idProblema = idProblema;
	}
	public String getTituloProblema() {
		return tituloProblema;
	}
	public void setTituloProblema(String tituloProblema) {
		this.tituloProblema = tituloProblema;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Integer getIdContrato() {
		return idContrato;
	}
	public void setIdContrato(Integer idContrato) {
		this.idContrato = idContrato;
	}
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public Collection<SolicitacaoServicoDTO> getColSolicitacaoServico() {
		return colSolicitacaoServico;
	}
	public void setColSolicitacaoServico(
			Collection<SolicitacaoServicoDTO> colSolicitacaoServico) {
		this.colSolicitacaoServico = colSolicitacaoServico;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public String getProprietario() {
		return proprietario;
	}
	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}
	public String getFormatoArquivoRelatorio() {
		return formatoArquivoRelatorio;
	}
	public void setFormatoArquivoRelatorio(String formatoArquivoRelatorio) {
		this.formatoArquivoRelatorio = formatoArquivoRelatorio;
	}
	
	public Integer getTotalSolicitacaoServicoPorProblema() {
		return totalSolicitacaoServicoPorProblema;
	}
	public void setTotalSolicitacaoServicoPorProblema(Integer totalSolicitacaoServicoPorProblema) {
		this.totalSolicitacaoServicoPorProblema = totalSolicitacaoServicoPorProblema;
	}
	public boolean isBaseConsultaReports() {
		return baseConsultaReports;
	}
	public void setBaseConsultaReports(boolean baseConsultaReports) {
		this.baseConsultaReports = baseConsultaReports;
	}

}
