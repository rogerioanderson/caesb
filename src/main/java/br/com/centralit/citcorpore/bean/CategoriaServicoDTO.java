/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

/**
 * @author rosana.godinho
 * 
 */
public class CategoriaServicoDTO implements IDto {

    private static final long serialVersionUID = 2984986456849222230L;
    private Integer idCategoriaServico;
    private Integer idCategoriaServicoPai;
    private Integer idEmpresa;
    private String nomeCategoriaServico;
    private Date dataInicio;
    private Date dataFim;
    private String nomeCategoriaServicoConcatenado;
    private String nomeCategoriaServicoPai;
    
    private int nivel;
   
  
    /**
     * @return valor do atributo idCategoriaServico.
     */
    public Integer getIdCategoriaServico() {
	return idCategoriaServico;
    }

    /**
     * Define valor do atributo idCategoriaServico.
     * 
     * @param idCategoriaServico
     */
    public void setIdCategoriaServico(Integer idCategoriaServico) {
	this.idCategoriaServico = idCategoriaServico;
    }

    /**
     * @return valor do atributo idCategoriaServicoPai.
     */
    public Integer getIdCategoriaServicoPai() {
	return idCategoriaServicoPai;
    }

    /**
     * Define valor do atributo idCategoriaServicoPai.
     * 
     * @param idCategoriaServicoPai
     */
    public void setIdCategoriaServicoPai(Integer idCategoriaServicoPai) {
	this.idCategoriaServicoPai = idCategoriaServicoPai;
    }

    /**
     * @return valor do atributo idEmpresa.
     */
    public Integer getIdEmpresa() {
	return idEmpresa;
    }

    /**
     * Define valor do atributo idEmpresa.
     * 
     * @param idEmpresa
     */
    public void setIdEmpresa(Integer idEmpresa) {
	this.idEmpresa = idEmpresa;
    }

    /**
     * @return valor do atributo nomeCategoriaServico.
     */
    public String getNomeCategoriaServico() {
	return nomeCategoriaServico;
    }
	public String getNomeCategoriaServicoNivel(){
	    if (this.getNomeCategoriaServico() == null){
		return this.nomeCategoriaServico;
	    }
	    String str = "";
	    for (int i = 0; i < this.getNivel(); i++){
		str += "....";
	    }
	    return str + this.nomeCategoriaServico;
	}
    /**
     * Define valor do atributo nomeCategoriaServico.
     * 
     * @param nomeCategoriaServico
     */
    public void setNomeCategoriaServico(String nomeCategoriaServico) {
	this.nomeCategoriaServico = nomeCategoriaServico;
    }

    /**
     * @return valor do atributo dataInicio.
     */
    public Date getDataInicio() {
	return dataInicio;
    }

    /**
     * Define valor do atributo dataInicio.
     * 
     * @param dataInicio
     */
    public void setDataInicio(Date dataInicio) {
	this.dataInicio = dataInicio;
    }
    
    /**
     * @return valor do atributo dataFim.
     */
    public Date getDataFim() {
	return dataFim;
    }

    /**
     * Define valor do atributo dataFim.
     * 
     * @param dataFim
     */
    public void setDataFim(Date dataFim) {
	this.dataFim = dataFim;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

	/**
	 * @return the nomeCatServicoConcatenado
	 */
	public String getNomeCategoriaServicoConcatenado() {
		return nomeCategoriaServicoConcatenado;
	}

	/**
	 * @param nomeCatServicoConcatenado the nomeCatServicoConcatenado to set
	 */
	public void setNomeCategoriaServicoConcatenado(String nomeCategoriaServicoConcatenado) {
		this.nomeCategoriaServicoConcatenado = nomeCategoriaServicoConcatenado;
	}

	/**
	 * @return the nomeCategoriaServicoPai
	 */
	public String getNomeCategoriaServicoPai() {
		return nomeCategoriaServicoPai;
	}

	/**
	 * @param nomeCategoriaServicoPai the nomeCategoriaServicoPai to set
	 */
	public void setNomeCategoriaServicoPai(String nomeCategoriaServicoPai) {
		this.nomeCategoriaServicoPai = nomeCategoriaServicoPai;
	}

}
