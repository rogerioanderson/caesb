/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Collection;

import br.com.citframework.dto.IDto;

/**
 * 
 * @author pedro
 *
 */
public class InfoCatalogoServicoDTO implements IDto {

		private Integer idInfoCatalogoServico;
		private Integer idCatalogoServico;
		private String descInfoCatalogoServico;
		private String nomeInfoCatalogoServico;
		private Integer idServicoCatalogo;
		private Integer idContrato;
		private String nomeServicoContrato;
		private String observacaoPortal;
		private String nomeServico;
		
		private Collection colArquivosUpload;
		
		public String getNomeServico() {
			return nomeServico;
		}
		public void setNomeServico(String nomeServico) {
			this.nomeServico = nomeServico;
		}
		public Integer getIdInfoCatalogoServico() {
			return idInfoCatalogoServico;
		}
		public void setIdInfoCatalogoServico(Integer idInfoCatalogoServico) {
			this.idInfoCatalogoServico = idInfoCatalogoServico;
		}
		public Integer getIdCatalogoServico() {
			return idCatalogoServico;
		}
		public void setIdCatalogoServico(Integer idCatalogoServico) {
			this.idCatalogoServico = idCatalogoServico;
		}
		public String getDescInfoCatalogoServico() {
			return descInfoCatalogoServico;
		}
		public void setDescInfoCatalogoServico(String descInfoCatalogoServico) {
			this.descInfoCatalogoServico = descInfoCatalogoServico;
		}
		public String getNomeInfoCatalogoServico() {
			return nomeInfoCatalogoServico;
		}
		public void setNomeInfoCatalogoServico(String nomeInfoCatalogoServico) {
			this.nomeInfoCatalogoServico = nomeInfoCatalogoServico;
		}
		public String getNomeServicoContrato() {
			return nomeServicoContrato;
		}
		public void setNomeServicoContrato(String nomeServicoContrato) {
			this.nomeServicoContrato = nomeServicoContrato;
		}
		public Integer getIdContrato() {
			return idContrato;
		}
		public void setIdContrato(Integer idContrato) {
			this.idContrato = idContrato;
		}
		public Integer getIdServicoCatalogo() {
			return idServicoCatalogo;
		}
		public void setIdServicoCatalogo(Integer idServicoCatalogo) {
			this.idServicoCatalogo = idServicoCatalogo;
		}
		public String getObservacaoPortal() {
			return observacaoPortal;
		}
		public void setObservacaoPortal(String observacaoPortal) {
			this.observacaoPortal = observacaoPortal;
		}
		public Collection getColArquivosUpload() {
			return colArquivosUpload;
		}
		public void setColArquivosUpload(Collection colArquivosUpload) {
			this.colArquivosUpload = colArquivosUpload;
		}
		
		
}
