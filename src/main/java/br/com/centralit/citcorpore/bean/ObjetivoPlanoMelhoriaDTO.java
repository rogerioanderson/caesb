/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Collection;

import br.com.citframework.dto.IDto;

@SuppressWarnings("serial")
public class ObjetivoPlanoMelhoriaDTO implements IDto {
	private Integer idObjetivoPlanoMelhoria;
	private Integer idPlanoMelhoria;
	private Integer idPlanoMelhoriaAux1;
	private String tituloObjetivo;
	private String detalhamento;
	private String resultadoEsperado;
	private String medicao;
	private String responsavel;
	private String criadoPor;
	private String modificadoPor;
	private java.sql.Date dataCriacao;
	private java.sql.Date ultModificacao;

	private Collection<AcaoPlanoMelhoriaDTO> listAcaoPlanoMelhoria;

	private Collection<ObjetivoPlanoMelhoriaDTO> listObjetivosPlanoMelhoria;

	private Collection<ObjetivoMonitoramentoDTO> listObjetivosMonitoramento;
	
	//private Object listObjetivosMonitoramento;

	private Integer sequencialObjetivo;

	public Integer getIdObjetivoPlanoMelhoria() {
		return this.idObjetivoPlanoMelhoria;
	}

	public void setIdObjetivoPlanoMelhoria(Integer parm) {
		this.idObjetivoPlanoMelhoria = parm;
	}

	public Integer getIdPlanoMelhoria() {
		return this.idPlanoMelhoria;
	}

	public void setIdPlanoMelhoria(Integer parm) {
		this.idPlanoMelhoria = parm;
	}

	public String getTituloObjetivo() {
		return this.tituloObjetivo;
	}

	public void setTituloObjetivo(String parm) {
		this.tituloObjetivo = parm;
	}

	public String getDetalhamento() {
		return this.detalhamento;
	}

	public void setDetalhamento(String parm) {
		this.detalhamento = parm;
	}

	public String getResultadoEsperado() {
		return this.resultadoEsperado;
	}

	public void setResultadoEsperado(String parm) {
		this.resultadoEsperado = parm;
	}

	public String getMedicao() {
		return this.medicao;
	}

	public void setMedicao(String parm) {
		this.medicao = parm;
	}

	public String getResponsavel() {
		return this.responsavel;
	}

	public void setResponsavel(String parm) {
		this.responsavel = parm;
	}

	public String getCriadoPor() {
		return this.criadoPor;
	}

	public void setCriadoPor(String parm) {
		this.criadoPor = parm;
	}

	public String getModificadoPor() {
		return this.modificadoPor;
	}

	public void setModificadoPor(String parm) {
		this.modificadoPor = parm;
	}

	public java.sql.Date getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(java.sql.Date parm) {
		this.dataCriacao = parm;
	}

	public java.sql.Date getUltModificacao() {
		return this.ultModificacao;
	}

	public void setUltModificacao(java.sql.Date parm) {
		this.ultModificacao = parm;
	}

	/**
	 * @return the listAcaoPlanoMelhoria
	 */
	public Collection<AcaoPlanoMelhoriaDTO> getListAcaoPlanoMelhoria() {
		return listAcaoPlanoMelhoria;
	}

	/**
	 * @param listAcaoPlanoMelhoria
	 *            the listAcaoPlanoMelhoria to set
	 */
	public void setListAcaoPlanoMelhoria(Collection<AcaoPlanoMelhoriaDTO> listAcaoPlanoMelhoria) {
		this.listAcaoPlanoMelhoria = listAcaoPlanoMelhoria;
	}

	/**
	 * @return the sequencialObjetivo
	 */
	public Integer getSequencialObjetivo() {
		return sequencialObjetivo;
	}

	/**
	 * @param sequencialObjetivo
	 *            the sequencialObjetivo to set
	 */
	public void setSequencialObjetivo(Integer sequencialObjetivo) {
		this.sequencialObjetivo = sequencialObjetivo;
	}

	/**
	 * @return the listObjetivosPlanoMelhoria
	 */
	public Collection<ObjetivoPlanoMelhoriaDTO> getListObjetivosPlanoMelhoria() {
		return listObjetivosPlanoMelhoria;
	}

	/**
	 * @param listObjetivosPlanoMelhoria
	 *            the listObjetivosPlanoMelhoria to set
	 */
	public void setListObjetivosPlanoMelhoria(Collection<ObjetivoPlanoMelhoriaDTO> listObjetivosPlanoMelhoria) {
		this.listObjetivosPlanoMelhoria = listObjetivosPlanoMelhoria;
	}

	public Integer getIdPlanoMelhoriaAux1() {
		return idPlanoMelhoriaAux1;
	}

	public void setIdPlanoMelhoriaAux1(Integer idPlanoMelhoriaAux1) {
		this.idPlanoMelhoriaAux1 = idPlanoMelhoriaAux1;
	}

	/**
	 * @return the listObjetivosMonitoramento
	 */
	public Collection<ObjetivoMonitoramentoDTO> getListObjetivosMonitoramento() {
		return listObjetivosMonitoramento;
	}

	/**
	 * @param listObjetivosMonitoramento the listObjetivosMonitoramento to set
	 */
	public void setListObjetivosMonitoramento(Collection<ObjetivoMonitoramentoDTO> listObjetivosMonitoramento) {
		this.listObjetivosMonitoramento = listObjetivosMonitoramento;
	}


}
