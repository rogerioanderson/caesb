/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

public class MonitoramentoAtivosDTO implements IDto {

	private static final long serialVersionUID = 1L;

	private Integer idMonitoramentoAtivos;

	private Integer idTipoItemConfiguracao;

	private String titulo;

	private String descricao;

	private String tipoRegra;

	private String enviarEmail;

	private String criarProblema;

	private String criarIncidente;

	private Date dataInicio;

	private Date dataFim;

	private Integer idCaracteristica;

	private String script;

	private Integer[] usuariosNotificacao;

	private Integer[] gruposNotificacao;
	
	private boolean scriptSuccess;

	public Integer getIdMonitoramentoAtivos() {
		return idMonitoramentoAtivos;
	}

	public void setIdMonitoramentoAtivos(Integer idMonitoramentoAtivos) {
		this.idMonitoramentoAtivos = idMonitoramentoAtivos;
	}

	public Integer getIdTipoItemConfiguracao() {
		return idTipoItemConfiguracao;
	}

	public void setIdTipoItemConfiguracao(Integer idTipoItemConfiguracao) {
		this.idTipoItemConfiguracao = idTipoItemConfiguracao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getTipoRegra() {
		return tipoRegra;
	}

	public void setTipoRegra(String tipoRegra) {
		this.tipoRegra = tipoRegra;
	}

	public String getEnviarEmail() {
		return enviarEmail;
	}

	public void setEnviarEmail(String enviarEmail) {
		this.enviarEmail = enviarEmail;
	}

	public String getCriarProblema() {
		return criarProblema;
	}

	public void setCriarProblema(String criarProblema) {
		this.criarProblema = criarProblema;
	}

	public String getCriarIncidente() {
		return criarIncidente;
	}

	public void setCriarIncidente(String criarIncidente) {
		this.criarIncidente = criarIncidente;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Integer getIdCaracteristica() {
		return idCaracteristica;
	}

	public void setIdCaracteristica(Integer idCaracteristica) {
		this.idCaracteristica = idCaracteristica;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public Integer[] getUsuariosNotificacao() {
		return usuariosNotificacao;
	}

	public void setUsuariosNotificacao(Integer[] usuariosNotificacao) {
		this.usuariosNotificacao = usuariosNotificacao;
	}

	public Integer[] getGruposNotificacao() {
		return gruposNotificacao;
	}

	public void setGruposNotificacao(Integer[] gruposNotificacao) {
		this.gruposNotificacao = gruposNotificacao;
	}

	public boolean getScriptSuccess() {
		return scriptSuccess;
	}

	public void setScriptSuccess(boolean scriptSuccess) {
		this.scriptSuccess = scriptSuccess;
	}

}
