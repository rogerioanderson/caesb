/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.List;

import br.com.centralit.citcorpore.rh.bean.CertificacaoDTO;
import br.com.centralit.citcorpore.rh.bean.CursoDTO;
import br.com.centralit.citcorpore.rh.bean.FormacaoAcademicaDTO;
import br.com.centralit.citcorpore.rh.bean.IdiomaDTO;

public abstract class RequisitoPerspectivaTecnicaAbstract implements RequisitoPerspectivaTecnicaInterface {

	private FormacaoAcademicaDTO formacaoAcademicaDto;
	private IdiomaDTO idiomaDto;
	private int idEscrita;
	private int idLeitura;
	private int idConversacao;
	private String experienciaAnterior;
	private List<CertificacaoDTO> listaCertificacoesDto;
	private List<CursoDTO> listaCursosDto;
	
	public FormacaoAcademicaDTO getFormacaoAcademicaDto() {
		return formacaoAcademicaDto;
	}
	public void setFormacaoAcademicaDto(FormacaoAcademicaDTO formacaoAcademicaDto) {
		this.formacaoAcademicaDto = formacaoAcademicaDto;
	}
	public IdiomaDTO getIdiomaDto() {
		return idiomaDto;
	}
	public void setIdiomaDto(IdiomaDTO idiomaDto) {
		this.idiomaDto = idiomaDto;
	}
	public int getIdEscrita() {
		return idEscrita;
	}
	public void setIdEscrita(int idEscrita) {
		this.idEscrita = idEscrita;
	}
	public int getIdLeitura() {
		return idLeitura;
	}
	public void setIdLeitura(int idLeitura) {
		this.idLeitura = idLeitura;
	}
	public int getIdConversacao() {
		return idConversacao;
	}
	public void setIdConversacao(int idConversacao) {
		this.idConversacao = idConversacao;
	}
	public String getExperienciaAnterior() {
		return experienciaAnterior;
	}
	public void setExperienciaAnterior(String experienciaAnterior) {
		this.experienciaAnterior = experienciaAnterior;
	}
	public List<CertificacaoDTO> getListaCertificacoesDto() {
		return listaCertificacoesDto;
	}
	public void setListaCertificacoesDto(List<CertificacaoDTO> listaCertificacoesDto) {
		this.listaCertificacoesDto = listaCertificacoesDto;
	}
	public List<CursoDTO> getListaCursosDto() {
		return listaCursosDto;
	}
	public void setListaCursosDto(List<CursoDTO> listaCursosDto) {
		this.listaCursosDto = listaCursosDto;
	}
	
	
	
}
