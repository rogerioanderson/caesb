/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;
import java.util.Collection;

import br.com.citframework.dto.IDto;

public class ImportarDadosDTO implements IDto {
	
	private static final long serialVersionUID = 4942283426115769824L;
	
	private Integer idImportarDados;
	private Integer idExternalConnection;
	private String importarPor;
	private String tipo;
	private String nome;
	private String script;
	private String agendarRotina;
	private String executarPor;
	private String horaExecucao;
	private Integer periodoHora;
	private Date dataFim;
	private String tabelaOrigem;
	private String tabelaDestino;
	private Collection<UploadDTO> anexos;
	private String jsonMatriz;

	
	public Collection<UploadDTO> getAnexos() {
		return anexos;
	}
	public void setAnexos(Collection<UploadDTO> anexos) {
		this.anexos = anexos;
	}
	public Integer getIdImportarDados() {
		return idImportarDados;
	}
	public void setIdImportarDados(Integer idImportarDados) {
		this.idImportarDados = idImportarDados;
	}
	public Integer getIdExternalConnection() {
		return idExternalConnection;
	}
	public void setIdExternalConnection(Integer idExternalConnection) {
		this.idExternalConnection = idExternalConnection;
	}
	public String getImportarPor() {
		return importarPor;
	}
	public void setImportarPor(String importarPor) {
		this.importarPor = importarPor;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getScript() {
		return script;
	}
	public void setScript(String script) {
		this.script = script;
	}
	public String getAgendarRotina() {
		return agendarRotina;
	}
	public void setAgendarRotina(String agendarRotina) {
		this.agendarRotina = agendarRotina;
	}
	public String getExecutarPor() {
		return executarPor;
	}
	public void setExecutarPor(String executarPor) {
		this.executarPor = executarPor;
	}
	public String getHoraExecucao() {
		return horaExecucao;
	}
	public void setHoraExecucao(String horaExecucao) {
		this.horaExecucao = horaExecucao;
	}
	public Integer getPeriodoHora() {
		return periodoHora;
	}
	public void setPeriodoHora(Integer periodoHora) {
		this.periodoHora = periodoHora;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getTabelaOrigem() {
		return tabelaOrigem;
	}
	public void setTabelaOrigem(String tabelaOrigem) {
		this.tabelaOrigem = tabelaOrigem;
	}
	public String getTabelaDestino() {
		return tabelaDestino;
	}
	public void setTabelaDestino(String tabelaDestino) {
		this.tabelaDestino = tabelaDestino;
	}
	public String getJsonMatriz() {
		return jsonMatriz;
	}
	public void setJsonMatriz(String jsonMatriz) {
		this.jsonMatriz = jsonMatriz;
	}

}
