/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

/**
 * @author rosana.godinho
 * 
 */
public class InformacaoItemConfiguracaoDTO implements IDto {

    private static final long serialVersionUID = 1L;

    private Integer idInformacaoItemConfiguracao;

    private Integer idItemConfiguracao;
    
    private String nomeGrupoItemConfiguracao;
    
    private String identificacaoItemConfiguracao;

    private String nomeGrupo;

  
    /**
     * @return valor do atributo idInformacaoItemConfiguracao.
     */
    public Integer getIdInformacaoItemConfiguracao() {
	return idInformacaoItemConfiguracao;
    }

    /**
     * Define valor do atributo idInformacaoItemConfiguracao.
     * 
     * @param idInformacaoItemConfiguracao
     */
    public void setIdInformacaoItemConfiguracao(Integer idInformacaoItemConfiguracao) {
	this.idInformacaoItemConfiguracao = idInformacaoItemConfiguracao;
    }


    /**
     * @return valor do atributo nomeGrupo.
     */
    public String getNomeGrupo() {
	return nomeGrupo;
    }

    /**
     * Define valor do atributo nomeGrupo.
     * 
     * @param nomeGrupo
     */
    public void setNomeGrupo(String nomeGrupo) {
	this.nomeGrupo = nomeGrupo;
    }

    /**
     * @return valor do atributo idItemConfiguracao.
     */
    public Integer getIdItemConfiguracao() {
	return idItemConfiguracao;
    }

    /**
     * Define valor do atributo idItemConfiguracao.
     * 
     * @param idItemConfiguracao
     */
    public void setIdItemConfiguracao(Integer idItemConfiguracao) {
	this.idItemConfiguracao = idItemConfiguracao;
    }

	/**
	 * @return the nomeGrupoItemConfiguracao
	 */
	public String getNomeGrupoItemConfiguracao() {
		return nomeGrupoItemConfiguracao;
	}

	/**
	 * @param nomeGrupoItemConfiguracao the nomeGrupoItemConfiguracao to set
	 */
	public void setNomeGrupoItemConfiguracao(String pNomeGrupoItemConfiguracao) {
		nomeGrupoItemConfiguracao = pNomeGrupoItemConfiguracao;
	}

	public String getIdentificacaoItemConfiguracao() {
		return identificacaoItemConfiguracao;
	}

	public void setIdentificacaoItemConfiguracao(
			String identificacaoItemConfiguracao) {
		this.identificacaoItemConfiguracao = identificacaoItemConfiguracao;
	}

}
