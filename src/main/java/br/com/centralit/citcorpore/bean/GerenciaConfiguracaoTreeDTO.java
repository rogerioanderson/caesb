/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class GerenciaConfiguracaoTreeDTO implements IDto {

	private static final long serialVersionUID = 1L;
	
	/* Item de Configuracao */
	private Integer idItemConfiguracao;
	private Integer idItemConfiguracaoExport;
	private Integer idItemConfiguracaoPai;
	private String identificacao;
	private String localidadeItemConfiguracao;
	private String criticidadeItemConfiguracao;
	private Integer idBaseConhecimento;

	/* Grupo Item de Configuracao */
	private Integer idGrupoItemConfiguracao;
	private String nomeGrupoItemConfiguracao;
	private Integer idGrupoItemConfiguracaoPai;
	
	private String criticidade;
	private String status;
	
	private String comboStatus;
	private String cboCriticidade;
	private String iframe;

	//atributo auxiliar para capturar o navegador usado
	private String idBrowserName;
	
	public String getIdBrowserName() {
		return idBrowserName;
	}


	public void setIdBrowserName(String idBrowserName) {
		this.idBrowserName = idBrowserName;
	}


	public String getComboStatus() {
		return comboStatus;
	}


	public void setComboStatus(String comboStatus) {
		this.comboStatus = comboStatus;
	}


	public String getCboCriticidade() {
		return cboCriticidade;
	}


	public void setCboCriticidade(String cboCriticidade) {
		this.cboCriticidade = cboCriticidade;
	}


	public String getCriticidade() {
		return criticidade;
	}


	public void setCriticidade(String criticidade) {
		this.criticidade = criticidade;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}

	


	public Integer getIdItemConfiguracao() {
		return idItemConfiguracao;
	}


	public void setIdItemConfiguracao(Integer pIdItemConfiguracao) {
		idItemConfiguracao = pIdItemConfiguracao;
	}


	public String getIdentificacao() {
		return identificacao;
	}

	public void setIdentificacao(String pIdentificacao) {
		identificacao = pIdentificacao;
	}


	public String getLocalidadeItemConfiguracao() {
		return localidadeItemConfiguracao;
	}

	public void setLocalidadeItemConfiguracao(String pLocalidadeItemConfiguracao) {
		localidadeItemConfiguracao = pLocalidadeItemConfiguracao;
	}


	public String getCriticidadeItemConfiguracao() {
		return criticidadeItemConfiguracao;
	}

	public void setCriticidadeItemConfiguracao(String pCriticidadeItemConfiguracao) {
		criticidadeItemConfiguracao = pCriticidadeItemConfiguracao;
	}


	public Integer getIdGrupoItemConfiguracao() {
		return idGrupoItemConfiguracao;
	}

	public void setIdGrupoItemConfiguracao(Integer pIdGrupoItemConfiguracao) {
		idGrupoItemConfiguracao = pIdGrupoItemConfiguracao;
	}


	public String getNomeGrupoItemConfiguracao() {
		return nomeGrupoItemConfiguracao;
	}

	public void setNomeGrupoItemConfiguracao(String pNomeGrupoItemConfiguracao) {
		nomeGrupoItemConfiguracao = pNomeGrupoItemConfiguracao;
	}

	public Integer getIdItemConfiguracaoPai() {
		return idItemConfiguracaoPai;
	}

	public void setIdItemConfiguracaoPai(Integer idItemConfiguracaoPai) {
		this.idItemConfiguracaoPai = idItemConfiguracaoPai;
	}

	public Integer getIdBaseConhecimento() {
		return idBaseConhecimento;
	}

	public void setIdBaseConhecimento(Integer idBaseConhecimento) {
		this.idBaseConhecimento = idBaseConhecimento;
	}

	public Integer getIdGrupoItemConfiguracaoPai() {
		return idGrupoItemConfiguracaoPai;
	}

	public void setIdGrupoItemConfiguracaoPai(Integer idGrupoItemConfiguracaoPai) {
		this.idGrupoItemConfiguracaoPai = idGrupoItemConfiguracaoPai;
	}

	public Integer getIdItemConfiguracaoExport() {
		return idItemConfiguracaoExport;
	}

	public void setIdItemConfiguracaoExport(Integer idItemConfiguracaoExport) {
		this.idItemConfiguracaoExport = idItemConfiguracaoExport;
	}


	public String getIframe() {
		return iframe;
	}


	public void setIframe(String iframe) {
		this.iframe = iframe;
	}
	
}
