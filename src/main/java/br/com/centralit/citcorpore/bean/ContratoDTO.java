/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 *
 * ************************************************************************************************************
 *
 * Dependentes: BI Citsmart
 *
 * Obs:
 * Qualquer altera��o nesta tabela dever� ser informada aos respons�veis pelo desenvolvimento do BI Citsmart.
 * O database do BI Citsmart precisa ter suas tabelas atualizadas de acordo com as mudan�as nesta tabela.
 *
 * ************************************************************************************************************
 *
 */

package br.com.centralit.citcorpore.bean;

import org.apache.commons.lang.StringUtils;

import br.com.citframework.dto.IDto;
import br.com.citframework.util.UtilHTML;
import br.com.citframework.util.UtilStrings;

public class ContratoDTO implements IDto {

    private static final long serialVersionUID = -5899256214152706919L;

    private String nome;

    private Integer idContrato;

    private Integer idCliente;

    private String numero;

    private String objeto;

    private java.sql.Date dataContrato;

    private java.sql.Date dataFimContrato;

    private Double valorEstimado;

    private String tipoTempoEstimado;

    private Integer tempoEstimado;

    private String tipo;

    private String situacao;

    private Integer idMoeda;

    private Integer idFluxo;

    private Double cotacaoMoeda;

    private Integer idFornecedor;

    private String deleted;

    private Integer idGrupoSolicitante;

    private java.sql.Date criadoEm;

    private String criadoPor;

    private java.sql.Date modificadoEm;

    private String modificadoPor;

    private String numeroContratoComNomeRazaoSocial;

    private String razaoSocialCliente;

	private String cadastroManualUsuario;
	
	private Integer centroDeResultado;

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Integer parm) {
        idContrato = parm;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer parm) {
        idCliente = parm;
    }

    public String getNumero() {
        return numero;
    }

    public String getNumeroHTMLEncoded() {
        return UtilHTML.encodeHTML(UtilStrings.nullToVazio(numero));
    }

    public void setNumero(String parm) {
        numero = parm;
    }

    public String getObjeto() {
        return objeto;
    }

    public void setObjeto(String parm) {
        objeto = parm;
    }

    public java.sql.Date getDataContrato() {
        return dataContrato;
    }

    public void setDataContrato(java.sql.Date parm) {
        dataContrato = parm;
    }

    public Double getValorEstimado() {
        return valorEstimado;
    }

    public void setValorEstimado(Double parm) {
        valorEstimado = parm;
    }

    public String getTipoTempoEstimado() {
        return tipoTempoEstimado;
    }

    public void setTipoTempoEstimado(String parm) {
        tipoTempoEstimado = parm;
    }

    public Integer getTempoEstimado() {
        return tempoEstimado;
    }

    public void setTempoEstimado(Integer parm) {
        tempoEstimado = parm;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String parm) {
        tipo = parm;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String parm) {
        situacao = parm;
    }

    public Integer getIdMoeda() {
        return idMoeda;
    }

    public void setIdMoeda(Integer idMoeda) {
        this.idMoeda = idMoeda;
    }

    public Integer getIdFluxo() {
        return idFluxo;
    }

    public void setIdFluxo(Integer idFluxo) {
        this.idFluxo = idFluxo;
    }

    public Double getCotacaoMoeda() {
        return cotacaoMoeda;
    }

    public void setCotacaoMoeda(Double cotacaoMoeda) {
        this.cotacaoMoeda = cotacaoMoeda;
    }

    public Integer getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(Integer idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public Integer getIdGrupoSolicitante() {
        return idGrupoSolicitante;
    }

    public void setIdGrupoSolicitante(Integer idGrupoSolicitante) {
        this.idGrupoSolicitante = idGrupoSolicitante;
    }

    /**
     * @return the dataFimContrato
     */
    public java.sql.Date getDataFimContrato() {
        return dataFimContrato;
    }

    /**
     * @param dataFimContrato
     *            the dataFimContrato to set
     */
    public void setDataFimContrato(java.sql.Date dataFimContrato) {
        this.dataFimContrato = dataFimContrato;
    }

    public java.sql.Date getCriadoEm() {
        return criadoEm;
    }

    public void setCriadoEm(java.sql.Date criadoEm) {
        this.criadoEm = criadoEm;
    }

    public String getCriadoPor() {
        return criadoPor;
    }

    public void setCriadoPor(String criadoPor) {
        this.criadoPor = criadoPor;
    }

    public java.sql.Date getModificadoEm() {
        return modificadoEm;
    }

    public void setModificadoEm(java.sql.Date modificadoEm) {
        this.modificadoEm = modificadoEm;
    }

    public String getModificadoPor() {
        return modificadoPor;
    }

    public void setModificadoPor(String modificadoPor) {
        this.modificadoPor = modificadoPor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((idContrato == null) ? 0 : idContrato.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ContratoDTO other = (ContratoDTO) obj;
        if (idContrato == null) {
            if (other.idContrato != null) {
                return false;
            }
        } else if (!idContrato.equals(other.idContrato)) {
            return false;
        }
        return true;
    }

    public String getRazaoSocialCliente() {
        return razaoSocialCliente;
    }

    public void setRazaoSocialCliente(String razaoSocialCliente) {
        this.razaoSocialCliente = razaoSocialCliente;
    }

    public String getNumeroContratoComNomeRazaoSocial() {

        if (this.getNumero() != null && this.getRazaoSocialCliente() != null && StringUtils.isNotBlank(this.getNumero()) && StringUtils.isNotBlank(this.getRazaoSocialCliente())) {
            this.setNumeroContratoComNomeRazaoSocial(this.getNumero() + " " + this.getRazaoSocialCliente());
        }

        return numeroContratoComNomeRazaoSocial;
    }

    public void setNumeroContratoComNomeRazaoSocial(String numeroContratoComNomeRazaoSocial) {
        this.numeroContratoComNomeRazaoSocial = numeroContratoComNomeRazaoSocial;
    }

	public String getCadastroManualUsuario() {
		return cadastroManualUsuario;
	}

	public void setCadastroManualUsuario(String cadastroManualUsuario) {
		this.cadastroManualUsuario = cadastroManualUsuario;
	}

	public Integer getCentroDeResultado() {
		return centroDeResultado;
	}

	public void setCentroDeResultado(Integer centroDeResultado) {
		this.centroDeResultado = centroDeResultado;
	}

}
