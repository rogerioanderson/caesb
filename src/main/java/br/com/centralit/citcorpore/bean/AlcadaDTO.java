/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Collection;

import org.json.JSONArray;

import br.com.citframework.dto.IDto;

public class AlcadaDTO implements IDto {
	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String tipoAlcada;
	
	private Double valor;
	private String usernameAprovador;
	private boolean aprovada;
	
	private JSONArray JSONResponsaveis;
	
	private Collection<EmpregadoDTO> colResponsaveis;
	private Collection<UsuarioDTO> colUsuariosResponsaveis;

	
	public AlcadaDTO() {
		
	}
	
	public AlcadaDTO(String tipoAlcada) {
		this.tipoAlcada = tipoAlcada;
	}

	
	/**
	 * Retorna o valor do atributo <code>tipoAlcada</code>
	 *
	 * @return <code>String</code>
	 */
	public String getTipoAlcada() {
	
		return tipoAlcada;
	}


	
	/**
	 * Define o valor do atributo <code>tipoAlcada</code>.
	 *
	 * @param tipoAlcada 
	 */
	public void setTipoAlcada(String tipoAlcada) {
	
		this.tipoAlcada = tipoAlcada;
	}


	
	/**
	 * Retorna o valor do atributo <code>colResponsaveis</code>
	 *
	 * @return <code>Collection<EmpregadoDTO></code>
	 */
	public Collection<EmpregadoDTO> getColResponsaveis() {
	
		return colResponsaveis;
	}


	
	/**
	 * Define o valor do atributo <code>colResponsaveis</code>.
	 *
	 * @param colResponsaveis 
	 */
	public void setColResponsaveis(Collection<EmpregadoDTO> colResponsaveis) {
	
		this.colResponsaveis = colResponsaveis;
	}


	
	/**
	 * Retorna o valor do atributo <code>colUsuariosResponsaveis</code>
	 *
	 * @return <code>Collection<UsuarioDTO></code>
	 */
	public Collection<UsuarioDTO> getColUsuariosResponsaveis() {
	
		return colUsuariosResponsaveis;
	}


	
	/**
	 * Define o valor do atributo <code>colUsuariosResponsaveis</code>.
	 *
	 * @param colUsuariosResponsaveis 
	 */
	public void setColUsuariosResponsaveis(Collection<UsuarioDTO> colUsuariosResponsaveis) {
	
		this.colUsuariosResponsaveis = colUsuariosResponsaveis;
	}


	public boolean mesmosResponsaveis(Collection<EmpregadoDTO> colComparacao) {
		if (this.getColResponsaveis() != null && colComparacao == null)
			return false;
		if (this.getColResponsaveis() == null && colComparacao != null)
			return false;
		if (this.getColResponsaveis().size() != colComparacao.size())
			return false;
		
		int i = 0;
		for (EmpregadoDTO empregadoCompDto : colComparacao) {
			for (EmpregadoDTO empregadoDto : this.getColResponsaveis()) {
				if (empregadoCompDto.getIdEmpregado().intValue() == empregadoDto.getIdEmpregado().intValue()) 
					i++;
			}
		}
		if (i != colComparacao.size())
			return false;
		
		i = 0;
		for (EmpregadoDTO empregadoDto : this.getColResponsaveis()) {
			for (EmpregadoDTO empregadoCompDto : colComparacao) {
				if (empregadoCompDto.getIdEmpregado().intValue() != empregadoDto.getIdEmpregado().intValue()) 
					i++;
			}
		}
		return i == this.getColResponsaveis().size();
	}

	
	/**
	 * Retorna o valor do atributo <code>jSONResponsaveis</code>
	 *
	 * @return <code>JSONArray</code>
	 */
	public JSONArray getJSONResponsaveis() {
	
		return JSONResponsaveis;
	}

	
	/**
	 * Define o valor do atributo <code>jSONResponsaveis</code>.
	 *
	 * @param jSONResponsaveis 
	 */
	public void setJSONResponsaveis(JSONArray jSONResponsaveis) {
	
		JSONResponsaveis = jSONResponsaveis;
	}

	
	/**
	 * Retorna o valor do atributo <code>valor</code>
	 *
	 * @return <code>Double</code>
	 */
	public Double getValor() {
	
		return valor;
	}

	
	/**
	 * Define o valor do atributo <code>valor</code>.
	 *
	 * @param valor 
	 */
	public void setValor(Double valor) {
	
		this.valor = valor;
	}

	
	/**
	 * Retorna o valor do atributo <code>usernameAprovador</code>
	 *
	 * @return <code>String</code>
	 */
	public String getUsernameAprovador() {
	
		return usernameAprovador;
	}

	
	/**
	 * Define o valor do atributo <code>usernameAprovador</code>.
	 *
	 * @param usernameAprovador 
	 */
	public void setUsernameAprovador(String usernameAprovador) {
	
		this.usernameAprovador = usernameAprovador;
	}

	
	/**
	 * Retorna o valor do atributo <code>aprovada</code>
	 *
	 * @return <code>boolean</code>
	 */
	public boolean isAprovada() {
		return aprovada;
	}

	
	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {
	
		return id;
	}

	
	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id 
	 */
	public void setId(Long id) {
	
		this.id = id;
	}

	
	/**
	 * Define o valor do atributo <code>aprovada</code>.
	 *
	 * @param aprovada 
	 */
	public void setAprovada(boolean aprovada) {
	
		this.aprovada = aprovada;
	}
	
	
}
