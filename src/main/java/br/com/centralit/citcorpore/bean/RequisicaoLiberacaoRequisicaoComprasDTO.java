/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;
/**
 * @author thiago matias
 *  
 */
/**
 * @author maycon.silva
 *
 */
public class RequisicaoLiberacaoRequisicaoComprasDTO implements IDto {
	
	private static final long serialVersionUID = 1L;
	private Integer idRequisicaoLiberacao;
	private Integer idRequisicaoLiberacaoCompras;
	private Integer idSolicitacaoServico;
	private String nomeServico;
	private String situacaoServicos;
	private Date dataFim;
	private String descricaoItem;
	private String nomeProjeto;
	private String codigoCentroResultado;
	private String nomeCentroResultado;
	private String situacaoItemRequisicao;
	private Integer idItemRequisicaoProduto;


	
	/**
	 * @return the descricaoItem
	 */
	public String getDescricaoItem() {
		return descricaoItem;
	}
	/**
	 * @param descricaoItem the descricaoItem to set
	 */
	public void setDescricaoItem(String descricaoItem) {
		this.descricaoItem = descricaoItem;
	}
	/**
	 * @return the nomeProjeto
	 */
	public String getNomeProjeto() {
		return nomeProjeto;
	}
	/**
	 * @param nomeProjeto the nomeProjeto to set
	 */
	public void setNomeProjeto(String nomeProjeto) {
		this.nomeProjeto = nomeProjeto;
	}
	/**
	 * @return the codigoCentroResultado
	 */
	public String getCodigoCentroResultado() {
		return codigoCentroResultado;
	}
	/**
	 * @param codigoCentroResultado the codigoCentroResultado to set
	 */
	public void setCodigoCentroResultado(String codigoCentroResultado) {
		this.codigoCentroResultado = codigoCentroResultado;
	}
	/**
	 * @return the nomeCentroResultado
	 */
	public String getNomeCentroResultado() {
		return nomeCentroResultado;
	}
	/**
	 * @param nomeCentroResultado the nomeCentroResultado to set
	 */
	public void setNomeCentroResultado(String nomeCentroResultado) {
		this.nomeCentroResultado = nomeCentroResultado;
	}
	/**
	 * @return the situacaoItemRequisicao
	 */
	public String getSituacaoItemRequisicao() {
		return situacaoItemRequisicao;
	}
	/**
	 * @param situacaoItemRequisicao the situacaoItemRequisicao to set
	 */
	public void setSituacaoItemRequisicao(String situacaoItemRequisicao) {
		this.situacaoItemRequisicao = situacaoItemRequisicao;
	}

	public Integer getIdRequisicaoLiberacaoCompras() {
		return idRequisicaoLiberacaoCompras;
	}
	public void setIdRequisicaoLiberacaoCompras(Integer idRequisicaoLiberacaoCompras) {
		this.idRequisicaoLiberacaoCompras = idRequisicaoLiberacaoCompras;
	}
	
	public Integer getIdRequisicaoLiberacao(){
		return this.idRequisicaoLiberacao;
	}
	public void setIdRequisicaoLiberacao(Integer parm){
		this.idRequisicaoLiberacao = parm;
	}
	public String getNomeServico() {
		return nomeServico;
	}
	public void setNomeServico(String nomeServico) {
		this.nomeServico = nomeServico;
	}
	
	public String getSituacaoServicos() {
		return situacaoServicos;
	}
	
	public void setSituacaoServicos(String situacaoServicos) {
		this.situacaoServicos = situacaoServicos;
	}
	public Integer getIdSolicitacaoServico() {
		return idSolicitacaoServico;
	}
	
	public void setIdSolicitacaoServico(Integer idSolicitacaoServico) {
		this.idSolicitacaoServico = idSolicitacaoServico;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public Integer getIdItemRequisicaoProduto() {
		return idItemRequisicaoProduto;
	}
	public void setIdItemRequisicaoProduto(Integer idItemRequisicaoProduto) {
		this.idItemRequisicaoProduto = idItemRequisicaoProduto;
	}
    

}
