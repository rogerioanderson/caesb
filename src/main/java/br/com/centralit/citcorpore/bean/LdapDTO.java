/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * 
 */
package br.com.centralit.citcorpore.bean;

import java.util.Collection;

import br.com.citframework.dto.IDto;

/**
 * @author valdoilo.damasceno
 * 
 */
public class LdapDTO implements IDto {

	private static final long serialVersionUID = -3593079788503253157L;

	private String idAtributoLdap;

	private String atributoLdap;

	private String valorAtributoLdap;

	private Collection<LdapDTO> listLdapDTO;

	public String getIdAtributoLdap() {
		return idAtributoLdap;
	}

	public void setIdAtributoLdap(String idAtributoLdap) {
		this.idAtributoLdap = idAtributoLdap;
	}

	public String getAtributoLdap() {
		return atributoLdap;
	}

	public void setAtributoLdap(String atributoLdap) {
		this.atributoLdap = atributoLdap;
	}

	public String getValorAtributoLdap() {
		return valorAtributoLdap;
	}

	public void setValorAtributoLdap(String valorAtributoLdap) {
		this.valorAtributoLdap = valorAtributoLdap;
	}

	public Collection<LdapDTO> getListLdapDTO() {
		return listLdapDTO;
	}

	public void setListLdapDTO(Collection<LdapDTO> listLdapDTO) {
		this.listLdapDTO = listLdapDTO;
	}

}
