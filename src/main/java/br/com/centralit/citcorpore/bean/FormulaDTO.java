/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

@SuppressWarnings("serial")
public class FormulaDTO implements IDto {
	public static String FORMULA_INVENTORY_PROCESS_SAVE = "INVENTORY_PROCESS_SAVE";
	public static String FORMULA_INVENTORY_PROCESS_BEFORE_CAPTURE = "INVENTORY_PROCESS_BEFORE_CAPTURE";
	
	private Integer idFormula;
	private String identificador;
	private String nome;
	private String conteudo;
	private java.sql.Date datacriacao;

	public Integer getIdFormula(){
		return this.idFormula;
	}
	public void setIdFormula(Integer parm){
		this.idFormula = parm;
	}

	public String getIdentificador(){
		return this.identificador;
	}
	public void setIdentificador(String parm){
		this.identificador = parm;
	}

	public String getNome(){
		return this.nome;
	}
	public void setNome(String parm){
		this.nome = parm;
	}

	public String getConteudo(){
		return this.conteudo;
	}
	public void setConteudo(String parm){
		this.conteudo = parm;
	}

	public java.sql.Date getDatacriacao(){
		return this.datacriacao;
	}
	public void setDatacriacao(java.sql.Date parm){
		this.datacriacao = parm;
	}

}
