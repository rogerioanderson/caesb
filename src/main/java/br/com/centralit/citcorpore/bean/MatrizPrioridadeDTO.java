/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

/**
 * 
 * @author rodrigo.oliveira
 *
 */
public class MatrizPrioridadeDTO implements IDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3987037840778547987L;
	
	private Integer idMatrizPrioridade;
	private String siglaImpacto;
	private String nivelImpacto;
	private String siglaUrgencia;
	private String nivelUrgencia;
	private Integer valorPrioridade;
	private Integer idContrato;
	private String deleted;
	
	/**
	 * @return the idMatrizPrioridade
	 */
	public Integer getIdMatrizPrioridade() {
		return idMatrizPrioridade;
	}
	/**
	 * @param idMatrizPrioridade the idMatrizPrioridade to set
	 */
	public void setIdMatrizPrioridade(Integer idMatrizPrioridade) {
		this.idMatrizPrioridade = idMatrizPrioridade;
	}
	/**
	 * @return the idImpacto
	 */
	public String getSiglaImpacto() {
		return siglaImpacto;
	}
	/**
	 * @param idImpacto the idImpacto to set
	 */
	public void setSiglaImpacto(String siglaImpacto) {
		this.siglaImpacto = siglaImpacto;
	}
	/**
	 * @return the idUrgencia
	 */
	public String getSiglaUrgencia() {
		return siglaUrgencia;
	}
	/**
	 * @param idUrgencia the idUrgencia to set
	 */
	public void setSiglaUrgencia(String siglaUrgencia) {
		this.siglaUrgencia = siglaUrgencia;
	}
	/**
	 * @return the valorPrioridade
	 */
	public Integer getValorPrioridade() {
		return valorPrioridade;
	}
	/**
	 * @param valorPrioridade the valorPrioridade to set
	 */
	public void setValorPrioridade(Integer valorPrioridade) {
		this.valorPrioridade = valorPrioridade;
	}
	/**
	 * @return the idContrato
	 */
	public Integer getIdContrato() {
		return idContrato;
	}
	/**
	 * @param idContrato the idContrato to set
	 */
	public void setIdContrato(Integer idContrato) {
		this.idContrato = idContrato;
	}
	/**
	 * @return the deleted
	 */
	public String getDeleted() {
		return deleted;
	}
	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	/**
	 * @return the nivelImpacto
	 */
	public String getNivelImpacto() {
		return nivelImpacto;
	}
	/**
	 * @param nivelImpacto the nivelImpacto to set
	 */
	public void setNivelImpacto(String nivelImpacto) {
		this.nivelImpacto = nivelImpacto;
	}
	/**
	 * @return the nivelUrgencia
	 */
	public String getNivelUrgencia() {
		return nivelUrgencia;
	}
	/**
	 * @param nivelUrgencia the nivelUrgencia to set
	 */
	public void setNivelUrgencia(String nivelUrgencia) {
		this.nivelUrgencia = nivelUrgencia;
	}
	
}
