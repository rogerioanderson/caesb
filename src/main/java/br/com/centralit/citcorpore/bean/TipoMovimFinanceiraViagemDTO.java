/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

/**
 * @author ronnie.lopes
 * 
 */
public class TipoMovimFinanceiraViagemDTO implements IDto {

    private static final long serialVersionUID = 1582364224581163482L;
    
    private Integer idtipoMovimFinanceiraViagem;
    private String nome;
    private String descricao;
    private String classificacao;
    private String situacao;
    private String exigePrestacaoConta;
    private String exigeDataHoraCotacao;
    private String permiteAdiantamento;
    private Double valorPadrao;
    private String tipo;
    private String imagem;
	
    /**
	 * @return the idtipoMovimFinanceiraViagem
	 */
	public Integer getIdtipoMovimFinanceiraViagem() {
		return idtipoMovimFinanceiraViagem;
	}
	/**
	 * @param idtipoMovimFinanceiraViagem the idtipoMovimFinanceiraViagem to set
	 */
	public void setIdtipoMovimFinanceiraViagem(Integer idtipoMovimFinanceiraViagem) {
		this.idtipoMovimFinanceiraViagem = idtipoMovimFinanceiraViagem;
	}
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * @return the classificacao
	 */
	public String getClassificacao() {
		return classificacao;
	}
	/**
	 * @param classificacao the classificacao to set
	 */
	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}
	/**
	 * @return the situacao
	 */
	public String getSituacao() {
		return situacao;
	}
	/**
	 * @param situacao the situacao to set
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	/**
	 * @return the exigePrestacaoConta
	 */
	public String getExigePrestacaoConta() {
		return exigePrestacaoConta;
	}
	/**
	 * @param exigePrestacaoConta the exigePrestacaoConta to set
	 */
	public void setExigePrestacaoConta(String exigePrestacaoConta) {
		this.exigePrestacaoConta = exigePrestacaoConta;
	}
	
	/**
	 * @return the permiteAdiantamento
	 */
	public String getPermiteAdiantamento() {
		return permiteAdiantamento;
	}
	/**
	 * @param permiteAdiantamento the permiteAdiantamento to set
	 */
	public void setPermiteAdiantamento(String permiteAdiantamento) {
		this.permiteAdiantamento = permiteAdiantamento;
	}
	/**
	 * @return the valorPadrao
	 */
	public Double getValorPadrao() {
		return valorPadrao;
	}
	/**
	 * @param valorPadrao the valorPadrao to set
	 */
	public void setValorPadrao(Double valorPadrao) {
		this.valorPadrao = valorPadrao;
	}
	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the imagem
	 */
	public String getImagem() {
		return imagem;
	}
	/**
	 * @param imagem the imagem to set
	 */
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	/**
	 * @return the exigeDataHoraCotacao
	 */
	public String getExigeDataHoraCotacao() {
		return exigeDataHoraCotacao;
	}
	/**
	 * @param exigeDataHoraCotacao the exigeDataHoraCotacao to set
	 */
	public void setExigeDataHoraCotacao(String exigeDataHoraCotacao) {
		this.exigeDataHoraCotacao = exigeDataHoraCotacao;
	}
    
}
