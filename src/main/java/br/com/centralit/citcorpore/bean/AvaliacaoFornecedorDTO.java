/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;
import java.util.Collection;

import br.com.citframework.dto.IDto;

@SuppressWarnings("serial")
public class AvaliacaoFornecedorDTO implements IDto {

	private Integer idAvaliacaoFornecedor;

	private Integer idFornecedor;

	private Integer idResponsavel;

	private Date dataAvaliacao;

	private String decisaoQualificacao;

	private String observacoesAvaliacaoFornecedor;

	private Collection<CriterioAvaliacaoFornecedorDTO> listCriterioAvaliacaoFornecedor;

	private Collection<AvaliacaoReferenciaFornecedorDTO> listAvaliacaoReferenciaFornecedor;
	
	private String nomeResponsavel ;

	
	public Integer getIdAvaliacaoFornecedor() {
		return this.idAvaliacaoFornecedor;
	}

	public void setIdAvaliacaoFornecedor(Integer parm) {
		this.idAvaliacaoFornecedor = parm;
	}

	public Integer getIdFornecedor() {
		return this.idFornecedor;
	}

	public void setIdFornecedor(Integer parm) {
		this.idFornecedor = parm;
	}

	public Integer getIdResponsavel() {
		return this.idResponsavel;
	}

	public void setIdResponsavel(Integer parm) {
		this.idResponsavel = parm;
	}

	public Date getDataAvaliacao() {
		return this.dataAvaliacao;
	}

	public void setDataAvaliacao(Date parm) {
		this.dataAvaliacao = parm;
	}

	public String getDecisaoQualificacao() {
		return this.decisaoQualificacao;
	}

	public void setDecisaoQualificacao(String parm) {
		this.decisaoQualificacao = parm;
	}


	/**
	 * @return the listCriterioAvaliacaoFornecedor
	 */
	public Collection<CriterioAvaliacaoFornecedorDTO> getListCriterioAvaliacaoFornecedor() {
		return listCriterioAvaliacaoFornecedor;
	}

	/**
	 * @param listCriterioAvaliacaoFornecedor
	 *            the listCriterioAvaliacaoFornecedor to set
	 */
	public void setListCriterioAvaliacaoFornecedor(Collection<CriterioAvaliacaoFornecedorDTO> listCriterioAvaliacaoFornecedor) {
		this.listCriterioAvaliacaoFornecedor = listCriterioAvaliacaoFornecedor;
	}

	/**
	 * @return the listAvaliacaoReferenciaFornecedor
	 */
	public Collection<AvaliacaoReferenciaFornecedorDTO> getListAvaliacaoReferenciaFornecedor() {
		return listAvaliacaoReferenciaFornecedor;
	}

	/**
	 * @param listAvaliacaoReferenciaFornecedor
	 *            the listAvaliacaoReferenciaFornecedor to set
	 */
	public void setListAvaliacaoReferenciaFornecedor(Collection<AvaliacaoReferenciaFornecedorDTO> listAvaliacaoReferenciaFornecedor) {
		this.listAvaliacaoReferenciaFornecedor = listAvaliacaoReferenciaFornecedor;
	}

	/**
	 * @return the nomeResponsavel
	 */
	public String getNomeResponsavel() {
		return nomeResponsavel;
	}

	/**
	 * @param nomeResponsavel the nomeResponsavel to set
	 */
	public void setNomeResponsavel(String nomeResponsavel) {
		this.nomeResponsavel = nomeResponsavel;
	}

	/**
	 * @return the observacoesAvaliacaoFornecedor
	 */
	public String getObservacoesAvaliacaoFornecedor() {
		return observacoesAvaliacaoFornecedor;
	}

	/**
	 * @param observacoesAvaliacaoFornecedor the observacoesAvaliacaoFornecedor to set
	 */
	public void setObservacoesAvaliacaoFornecedor(String observacoesAvaliacaoFornecedor) {
		this.observacoesAvaliacaoFornecedor = observacoesAvaliacaoFornecedor;
	}


}
