/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class CategoriaSolucaoDTO implements IDto {
	private Integer idCategoriaSolucao;
	private Integer idCategoriaSolucaoPai;
	private String descricaoCategoriaSolucao;
	private java.sql.Date dataInicio;
	private java.sql.Date dataFim;
	
	private Integer nivel;

	public Integer getIdCategoriaSolucao(){
		return this.idCategoriaSolucao;
	}
	public void setIdCategoriaSolucao(Integer parm){
		this.idCategoriaSolucao = parm;
	}

	public Integer getIdCategoriaSolucaoPai(){
		return this.idCategoriaSolucaoPai;
	}
	public void setIdCategoriaSolucaoPai(Integer parm){
		this.idCategoriaSolucaoPai = parm;
	}

	public String getDescricaoCategoriaSolucao(){
		return this.descricaoCategoriaSolucao;
	}
	public void setDescricaoCategoriaSolucao(String parm){
		this.descricaoCategoriaSolucao = parm;
	}
	
	public String getDescricaoCategoriaNivel(){
	    if (this.getNivel() == null){
		return this.descricaoCategoriaSolucao;
	    }
	    String str = "";
	    for (int i = 0; i < this.getNivel().intValue(); i++){
		str += "....";
	    }
	    return str + this.descricaoCategoriaSolucao;
	}	

	public java.sql.Date getDataInicio(){
		return this.dataInicio;
	}
	public void setDataInicio(java.sql.Date parm){
		this.dataInicio = parm;
	}

	public java.sql.Date getDataFim(){
		return this.dataFim;
	}
	public void setDataFim(java.sql.Date parm){
		this.dataFim = parm;
	}
	public Integer getNivel() {
	    return nivel;
	}
	public void setNivel(Integer nivel) {
	    this.nivel = nivel;
	}

}
