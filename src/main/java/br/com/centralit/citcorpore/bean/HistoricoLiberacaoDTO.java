/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Timestamp;

public class HistoricoLiberacaoDTO extends RequisicaoLiberacaoDTO {

	/**@author murilo.pacheco
	 * classe de historicos de altera��es das libera��es
	 */
	private static final long serialVersionUID = 1L;
	/**
	 *
	 */
	private Integer idHistoricoLiberacao;
	private Timestamp dataHoraModificacao;
	private Integer idExecutorModificacao;
	private String tipoModificacao; // C - cria��o / U - update
	private Double historicoVersao;
	private String nomeProprietario;
	private String baseLine;
	private String nomeExecutorModificacao;


	public Integer getIdHistoricoLiberacao() {
		return idHistoricoLiberacao;
	}
	public void setIdHistoricoLiberacao(Integer idHistoricoLiberacao) {
		this.idHistoricoLiberacao = idHistoricoLiberacao;
	}

	public Timestamp getDataHoraModificacao() {
		return dataHoraModificacao;
	}
	public void setDataHoraModificacao(Timestamp dataHoraModificacao) {
		this.dataHoraModificacao = dataHoraModificacao;
	}

	public Integer getIdExecutorModificacao() {
		return idExecutorModificacao;
	}
	public void setIdExecutorModificacao(Integer idExecutorModificacao) {
		this.idExecutorModificacao = idExecutorModificacao;
	}
	public String getTipoModificacao() {
		return tipoModificacao;
	}
	public void setTipoModificacao(String tipoModificacao) {
		this.tipoModificacao = tipoModificacao;
	}
	public Double getHistoricoVersao() {
		return historicoVersao;
	}
	public void setHistoricoVersao(Double historicoVersao) {
		this.historicoVersao = historicoVersao;
	}
	public String getNomeProprietario() {
		return nomeProprietario;
	}
	public void setNomeProprietario(String nomeProprietario) {
		this.nomeProprietario = nomeProprietario;
	}
	public String getBaseLine() {
		return baseLine;
	}
	public void setBaseLine(String baseLine) {
		this.baseLine = baseLine;
	}
	public String getNomeExecutorModificacao() {
		return nomeExecutorModificacao;
	}
	public void setNomeExecutorModificacao(String nomeExecutorModificacao) {
		this.nomeExecutorModificacao = nomeExecutorModificacao;
	}
}
