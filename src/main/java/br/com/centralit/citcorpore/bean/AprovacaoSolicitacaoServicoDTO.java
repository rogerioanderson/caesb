/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import br.com.citframework.dto.IDto;

public class AprovacaoSolicitacaoServicoDTO implements IDto {
	private Integer idAprovacaoSolicitacaoServico;
	private Integer idSolicitacaoServico;
	private Integer idResponsavel;
	private Integer idTarefa;
	private Integer idJustificativa;
	private Timestamp dataHora;
	private String complementoJustificativa;
	private String observacoes;
	private String aprovacao;

	public Integer getIdAprovacaoSolicitacaoServico(){
		return this.idAprovacaoSolicitacaoServico;
	}
	public void setIdAprovacaoSolicitacaoServico(Integer parm){
		this.idAprovacaoSolicitacaoServico = parm;
	}

	public Integer getIdSolicitacaoServico(){
		return this.idSolicitacaoServico;
	}
	public void setIdSolicitacaoServico(Integer parm){
		this.idSolicitacaoServico = parm;
	}

	public Integer getIdResponsavel(){
		return this.idResponsavel;
	}
	public void setIdResponsavel(Integer parm){
		this.idResponsavel = parm;
	}

	public Integer getIdTarefa(){
		return this.idTarefa;
	}
	public void setIdTarefa(Integer parm){
		this.idTarefa = parm;
	}

	public Integer getIdJustificativa(){
		return this.idJustificativa;
	}
	public void setIdJustificativa(Integer parm){
		this.idJustificativa = parm;
	}

	public Timestamp getDataHora(){
		return this.dataHora;
	}
	public void setDataHora(Timestamp parm){
		this.dataHora = parm;
	}

	public String getComplementoJustificativa(){
		return this.complementoJustificativa;
	}
	public void setComplementoJustificativa(String parm){
		this.complementoJustificativa = parm;
	}

	public String getObservacoes(){
		return this.observacoes;
	}
	public void setObservacoes(String parm){
		this.observacoes = parm;
	}

	public String getAprovacao(){
		return this.aprovacao;
	}
	public void setAprovacao(String parm){
		this.aprovacao = parm;
	}
	public String getDataHoraStr() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return format.format(dataHora);
	}
}
