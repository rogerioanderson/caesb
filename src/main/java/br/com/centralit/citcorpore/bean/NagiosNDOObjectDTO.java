/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class NagiosNDOObjectDTO implements IDto {
	private Integer object_id;
	private Integer instance_id;
	private Integer objecttype_id;
	private String name1;
	private String name2;
	private Integer is_active;

	public Integer getObject_id(){
		return this.object_id;
	}
	public void setObject_id(Integer parm){
		this.object_id = parm;
	}

	public Integer getInstance_id(){
		return this.instance_id;
	}
	public void setInstance_id(Integer parm){
		this.instance_id = parm;
	}

	public Integer getObjecttype_id(){
		return this.objecttype_id;
	}
	public void setObjecttype_id(Integer parm){
		this.objecttype_id = parm;
	}

	public String getName1(){
		return this.name1;
	}
	public void setName1(String parm){
		this.name1 = parm;
	}

	public String getName2(){
		return this.name2;
	}
	public void setName2(String parm){
		this.name2 = parm;
	}

	public Integer getIs_active(){
		return this.is_active;
	}
	public void setIs_active(Integer parm){
		this.is_active = parm;
	}

}
