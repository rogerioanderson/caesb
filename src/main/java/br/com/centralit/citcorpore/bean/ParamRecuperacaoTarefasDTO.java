/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Collection;

import br.com.citframework.dto.IDto;

/**
 * Classe que encapsula os par�metros para recupera��o de tarefas do fluxo
 *
 * @author carlos.santos
 * @since 27.01.2015 - Opera��o Usain Bolt.
 */
public class ParamRecuperacaoTarefasDTO implements IDto {

    private static final long serialVersionUID = -3415074213123939278L;

    private String loginUsuario;
    private GerenciamentoServicosDTO gerenciamentoServicosDto;
    private Collection<ContratoDTO> contratosUsuario;

    private Integer idTarefa;
    private UsuarioDTO usuarioDto;

    private boolean somenteTotalizacao;

    public ParamRecuperacaoTarefasDTO(final String loginUsuario, final GerenciamentoServicosDTO gerenciamentoServicosDto, final Collection<ContratoDTO> contratosUsuario) {
        this.loginUsuario = loginUsuario;
        this.gerenciamentoServicosDto = gerenciamentoServicosDto;
        this.contratosUsuario = contratosUsuario;
    }

    public ParamRecuperacaoTarefasDTO(final String loginUsuario) {
        this.loginUsuario = loginUsuario;
    }

    public ParamRecuperacaoTarefasDTO(final String loginUsuario, final Collection<ContratoDTO> contratosUsuario) {
        this.loginUsuario = loginUsuario;
        this.contratosUsuario = contratosUsuario;
    }

    public String getLoginUsuario() {
        return loginUsuario;
    }

    public void setLoginUsuario(final String loginUsuario) {
        this.loginUsuario = loginUsuario;
    }

    public GerenciamentoServicosDTO getGerenciamentoServicosDto() {
        return gerenciamentoServicosDto;
    }

    public void setGerenciamentoServicosDto(final GerenciamentoServicosDTO gerenciamentoServicosDto) {
        this.gerenciamentoServicosDto = gerenciamentoServicosDto;
    }

    public Collection<ContratoDTO> getContratosUsuario() {
        return contratosUsuario;
    }

    public void setContratosUsuario(final Collection<ContratoDTO> contratosUsuario) {
        this.contratosUsuario = contratosUsuario;
    }

    public Integer getIdTarefa() {
        return idTarefa;
    }

    public void setIdTarefa(final Integer idTarefa) {
        this.idTarefa = idTarefa;
    }

    public UsuarioDTO getUsuarioDto() {
        return usuarioDto;
    }

    public void setUsuarioDto(final UsuarioDTO usuarioDto) {
        this.usuarioDto = usuarioDto;
    }

    public boolean isSomenteTotalizacao() {
        return somenteTotalizacao;
    }

    public void setSomenteTotalizacao(final boolean somenteTotalizacao) {
        this.somenteTotalizacao = somenteTotalizacao;
    }

}
