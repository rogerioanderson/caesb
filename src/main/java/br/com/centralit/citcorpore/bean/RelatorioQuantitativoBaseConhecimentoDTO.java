/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Collection;

import br.com.citframework.dto.IDto;

public class RelatorioQuantitativoBaseConhecimentoDTO implements IDto {
	
	private static final long serialVersionUID = -1501060076093192977L;
	
	private Collection<BaseConhecimentoDTO> listaBaseConhecimento;
	private Collection<ComentariosDTO> listaComentarios;
	private Collection<BaseConhecimentoDTO> listaAutores;
	private Collection<BaseConhecimentoDTO> listaAprovadores;
	private Collection<BaseConhecimentoDTO> listaPublicadosPorOrigem;
	private Collection<BaseConhecimentoDTO> listaNaoPublicadosPorOrigem;
	private Collection<BaseConhecimentoDTO> listaConhecimentoQuantitativoEmLista;
	
	public Collection<BaseConhecimentoDTO> getListaBaseConhecimento() {
		return listaBaseConhecimento;
	}
	public void setListaBaseConhecimento(Collection<BaseConhecimentoDTO> listaBaseConhecimento) {
		this.listaBaseConhecimento = listaBaseConhecimento;
	}
	public Collection<ComentariosDTO> getListaComentarios() {
		return listaComentarios;
	}
	public void setListaComentarios(Collection<ComentariosDTO> listaComentarios) {
		this.listaComentarios = listaComentarios;
	}
	public Collection<BaseConhecimentoDTO> getListaAutores() {
		return listaAutores;
	}
	public void setListaAutores(Collection<BaseConhecimentoDTO> listaAutores) {
		this.listaAutores = listaAutores;
	}
	public Collection<BaseConhecimentoDTO> getListaAprovadores() {
		return listaAprovadores;
	}
	public void setListaAprovadores(Collection<BaseConhecimentoDTO> listaAprovadores) {
		this.listaAprovadores = listaAprovadores;
	}
	public Collection<BaseConhecimentoDTO> getListaPublicadosPorOrigem() {
		return listaPublicadosPorOrigem;
	}
	public void setListaPublicadosPorOrigem(Collection<BaseConhecimentoDTO> listaPublicadosPorOrigem) {
		this.listaPublicadosPorOrigem = listaPublicadosPorOrigem;
	}
	public Collection<BaseConhecimentoDTO> getListaNaoPublicadosPorOrigem() {
		return listaNaoPublicadosPorOrigem;
	}
	public void setListaNaoPublicadosPorOrigem(Collection<BaseConhecimentoDTO> listaNaoPublicadosPorOrigem) {
		this.listaNaoPublicadosPorOrigem = listaNaoPublicadosPorOrigem;
	}
	public Collection<BaseConhecimentoDTO> getListaConhecimentoQuantitativoEmLista() {
		return listaConhecimentoQuantitativoEmLista;
	}
	public void setListaConhecimentoQuantitativoEmLista(Collection<BaseConhecimentoDTO> listaConhecimentoQuantitativoEmLista) {
		this.listaConhecimentoQuantitativoEmLista = listaConhecimentoQuantitativoEmLista;
	}
	
}
