/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class PrioridadeDTO implements IDto {

    /**
	 * 
	 */
    private static final long serialVersionUID = -7848776827100833523L;

    private Integer idPrioridade;
    private Integer idEmpresa;
    private String nomePrioridade;
    private String situacao;
    private String grupoPrioridade;
    private Integer quantidade;

    public String getGrupoPrioridade() {
    	if (grupoPrioridade != null){
    		return grupoPrioridade.trim();
    	}
    	
    	return grupoPrioridade;
    }

    public void setGrupoPrioridade(String grupoPrioridade) {
	this.grupoPrioridade = grupoPrioridade;
    }

    public static long getSerialversionuid() {
	return serialVersionUID;
    }

    public String getNomePrioridade() {
	return nomePrioridade;
    }

    public void setNomePrioridade(String nomePrioridade) {
	this.nomePrioridade = nomePrioridade;
    }

    public Integer getIdPrioridade() {
	return idPrioridade;
    }

    public void setIdPrioridade(Integer idPrioridade) {
	this.idPrioridade = idPrioridade;
    }

    public Integer getIdEmpresa() {
	return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
	this.idEmpresa = idEmpresa;
    }

    public String getSituacao() {
	return situacao;
    }

    public void setSituacao(String situacao) {
	this.situacao = situacao;
    }

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

}
