/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Collection;

import br.com.citframework.dto.IDto;
import br.com.citframework.util.UtilStrings;

public class CentroResultadoDTO implements IDto {

	private static final long serialVersionUID = 5314639262309812533L;
	
	private Integer idCentroResultado;	
	private String codigoCentroResultado;
	private String nomeCentroResultado;
	private Integer idCentroResultadoPai;
	private String permiteRequisicaoProduto;
	private String situacao;
	private String nomeCentroResultadoPai;
	private Integer nivel;
	private String imagem;
	
	private Collection<AlcadaCentroResultadoDTO> colAlcadas;
	private Collection<ResponsavelCentroResultadoDTO> colResponsaveis;
	
	public Integer getIdCentroResultado() {
		return this.idCentroResultado;
	}
	
	public void setIdCentroResultado(Integer parm) {
		this.idCentroResultado = parm;
	}

	public String getCodigoCentroResultado() {
		return this.codigoCentroResultado;
	}
	
	public void setCodigoCentroResultado(String parm) {
		this.codigoCentroResultado = parm;
	}

	public String getNomeCentroResultado() {
	    return nomeCentroResultado;
	}
	
    public String getNomeHierarquizado() {
    	
        if (nomeCentroResultado == null) 
        	return "";
        
        if (this.getNivel() == null) 
        	return UtilStrings.nullToVazio(codigoCentroResultado) + " " + nomeCentroResultado;
        
        String aux = "";
        
        for (int i = 0; i < this.getNivel().intValue(); i++) {
        	aux += ".....";
        }
        
        return aux + UtilStrings.nullToVazio(codigoCentroResultado) + " " + nomeCentroResultado;
    }	
    
	public void setNomeCentroResultado(String parm) {
		this.nomeCentroResultado = parm;
	}
	
	public Integer getIdCentroResultadoPai() {
		return this.idCentroResultadoPai;
	}
	
	public void setIdCentroResultadoPai(Integer parm) {
		this.idCentroResultadoPai = parm;
	}

	public String getSituacao() {
		return this.situacao;
	}
	
	public void setSituacao(String parm) {
		this.situacao = parm;
	}
	
    public String getNomeCentroResultadoPai() {
		return nomeCentroResultadoPai;
	}

	public void setNomeCentroResultadoPai(String nomeCentroResultadoPai) {
		this.nomeCentroResultadoPai = nomeCentroResultadoPai;
	}

	public String getPermiteRequisicaoProduto() {
    	return permiteRequisicaoProduto;
    }
    
    public void setPermiteRequisicaoProduto(String permiteRequisicaoProduto) {
    	this.permiteRequisicaoProduto = permiteRequisicaoProduto;
    }
    
    public Integer getNivel() {
    	return nivel;
    }
    
    public void setNivel(Integer nivel) {
    	this.nivel = nivel;
    }
    
    public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getNomeHierarquiaHTML() {
    	
    	if (nomeCentroResultado == null) 
    		return "";
    	
        if (this.getNivel() == null) 
    		return nomeCentroResultado;
        
        String aux = "";
        
        for(int i = 0; i < this.getNivel().intValue(); i++) {
            aux += ".....";
        }
        
        return aux + nomeCentroResultado;
    }

    public Collection<AlcadaCentroResultadoDTO> getColAlcadas() {
        return colAlcadas;
    }

    public void setColAlcadas(Collection<AlcadaCentroResultadoDTO> colAlcadas) {
        this.colAlcadas = colAlcadas;
    }

	public Collection<ResponsavelCentroResultadoDTO> getColResponsaveis() {
		return colResponsaveis;
	}

	public void setColResponsaveis(
			Collection<ResponsavelCentroResultadoDTO> colResponsaveis) {
		this.colResponsaveis = colResponsaveis;
	}
}
