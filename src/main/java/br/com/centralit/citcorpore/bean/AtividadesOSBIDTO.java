/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

@SuppressWarnings("serial")
public class AtividadesOSBIDTO implements IDto {
	private Integer idAtividadesOS;
	private Integer idOS;
	private Integer sequencia;
	private Integer idAtividadeServicoContrato;
	private String descricaoAtividade;
	private String obsAtividade;
	private Double custoAtividade ;
	private Double glosaAtividade;
	private Double qtdeExecutada;
	private String complexidade;
	private String numeroOS;
	private String deleted;
	private Double custoRealizado;
	private Double custos;
	private Object listaAcordoNivelServico;
	private Object listaAtividadeOs;
	private Object listaGlosasOs;
	private String formula;
	private String contabilizar;
	private Integer idServicoContratoContabil;
	
	private Integer idConexaoBI;

	public Object getListaGlosasOs() {
		return listaGlosasOs;
	}

	public void setListaGlosasOs(Object listaGlosasOs) {
		this.listaGlosasOs = listaGlosasOs;
	}

	public Double getCustos() {
		return custos;
	}

	public void setCustos(Double custos) {
		this.custos = custos;
	}

	public Double getCustoRealizado() {
		return custoRealizado;
	}

	public void setCustoRealizado(Double custoRealizado) {
		this.custoRealizado = custoRealizado;
	}

	public Object getListaAtividadeOs() {
		return listaAtividadeOs;
	}

	public void setListaAtividadeOs(Object listaAtividadeOs) {
		this.listaAtividadeOs = listaAtividadeOs;
	}

	public Object getListaAcordoNivelServico() {
		return listaAcordoNivelServico;
	}

	public void setListaAcordoNivelServico(Object listaAcordoNivelServico) {
		this.listaAcordoNivelServico = listaAcordoNivelServico;
	}

	public Integer getIdAtividadesOS() {
		return this.idAtividadesOS;
	}

	public void setIdAtividadesOS(Integer parm) {
		this.idAtividadesOS = parm;
	}

	public Integer getIdOS() {
		return this.idOS;
	}

	public void setIdOS(Integer parm) {
		this.idOS = parm;
	}

	public Integer getSequencia() {
		return this.sequencia;
	}

	public void setSequencia(Integer parm) {
		this.sequencia = parm;
	}

	public Integer getIdAtividadeServicoContrato() {
		return this.idAtividadeServicoContrato;
	}

	public void setIdAtividadeServicoContrato(Integer parm) {
		this.idAtividadeServicoContrato = parm;
	}

	public String getDescricaoAtividade() {
		return this.descricaoAtividade;
	}

	public void setDescricaoAtividade(String parm) {
		this.descricaoAtividade = parm;
	}

	public String getObsAtividade() {
		return this.obsAtividade;
	}

	public void setObsAtividade(String parm) {
		this.obsAtividade = parm;
	}

	public Double getCustoAtividade() {
		return this.custoAtividade;
	}

	public void setCustoAtividade(Double parm) {
		this.custoAtividade = parm;
	}

	public String getComplexidade() {
		return this.complexidade;
	}

	public void setComplexidade(String parm) {
		this.complexidade = parm;
	}
	
	public String getDeleted() {
		return this.deleted;
	}
	public void setDeleted(String parm) {
		this.deleted = parm;
	}
	
	public Double getGlosaAtividade() {
		return glosaAtividade;
	}
	
	public void setGlosaAtividade(Double glosaAtividade) {
		this.glosaAtividade = glosaAtividade;
	}
	
	public Double getQtdeExecutada() {
		return qtdeExecutada;
	}
	
	public void setQtdeExecutada(Double qtdeExecutada) {
		this.qtdeExecutada = qtdeExecutada;
	}
	
	public String getNumeroOS() {
		return numeroOS;
	}
	
	public void setNumeroOS(String numeroOS) {
		this.numeroOS = numeroOS;
	}
	
	public String getFormula() {
		return formula;
	}
	
	public void setFormula(String formula) {
		this.formula = formula;
	}
	
	public String getContabilizar() {
		return contabilizar;
	}
	
	public void setContabilizar(String contabilizar) {
		this.contabilizar = contabilizar;
	}
	
	public Integer getIdServicoContratoContabil() {
		return idServicoContratoContabil;
	}
	
	public void setIdServicoContratoContabil(Integer idServicoContratoContabil) {
		this.idServicoContratoContabil = idServicoContratoContabil;
	}

	public Integer getIdConexaoBI() {
		return idConexaoBI;
	}

	public void setIdConexaoBI(Integer idConexaoBI) {
		this.idConexaoBI = idConexaoBI;
	}
	
}
