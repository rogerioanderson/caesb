/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * 
 */
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

/**
 * @author valdoilo.damasceno
 * 
 */
public class AnexoBaseConhecimentoDTO implements IDto {

    private static final long serialVersionUID = 808193377991557947L;

    private Integer idAnexoBaseConhecimento;

    private Integer idBaseConhecimento;

    private Date dataInicio;

    private Date dataFim;

    private String nomeAnexo;

    private String descricao;

    private String link;

    private String extensao;

    private String textoDocumento;

    /**
     * @return valor do atributo idAnexoBaseConhecimento.
     */
    public Integer getIdAnexoBaseConhecimento() {
	return idAnexoBaseConhecimento;
    }

    /**
     * Define valor do atributo idAnexoBaseConhecimento.
     * 
     * @param idAnexoBaseConhecimento
     */
    public void setIdAnexoBaseConhecimento(Integer idAnexoBaseConhecimento) {
	this.idAnexoBaseConhecimento = idAnexoBaseConhecimento;
    }

    /**
     * @return valor do atributo idBaseConhecimento.
     */
    public Integer getIdBaseConhecimento() {
	return idBaseConhecimento;
    }

    /**
     * Define valor do atributo idBaseConhecimento.
     * 
     * @param idBaseConhecimento
     */
    public void setIdBaseConhecimento(Integer idBaseConhecimento) {
	this.idBaseConhecimento = idBaseConhecimento;
    }

    /**
     * @return valor do atributo dataInicio.
     */
    public Date getDataInicio() {
	return dataInicio;
    }

    /**
     * Define valor do atributo dataInicio.
     * 
     * @param dataInicio
     */
    public void setDataInicio(Date dataInicio) {
	this.dataInicio = dataInicio;
    }

    /**
     * @return valor do atributo dataFim.
     */
    public Date getDataFim() {
	return dataFim;
    }

    /**
     * Define valor do atributo dataFim.
     * 
     * @param dataFim
     */
    public void setDataFim(Date dataFim) {
	this.dataFim = dataFim;
    }

    /**
     * @return valor do atributo nomeAnexo.
     */
    public String getNomeAnexo() {
	return nomeAnexo;
    }

    /**
     * Define valor do atributo nomeAnexo.
     * 
     * @param nomeAnexo
     */
    public void setNomeAnexo(String nomeAnexo) {
	this.nomeAnexo = nomeAnexo;
    }

    /**
     * @return valor do atributo link.
     */
    public String getLink() {
	return link;
    }

    /**
     * Define valor do atributo link.
     * 
     * @param link
     */
    public void setLink(String link) {
	this.link = link;
    }

    /**
     * @return valor do atributo extensao.
     */
    public String getExtensao() {
	return extensao;
    }

    /**
     * Define valor do atributo extensao.
     * 
     * @param extensao
     */
    public void setExtensao(String extensao) {
	this.extensao = extensao;
    }

    /**
     * @return valor do atributo descricao.
     */
    public String getDescricao() {
	return descricao;
    }

    /**
     * Define valor do atributo descricao.
     * 
     * @param descricao
     */
    public void setDescricao(String descricao) {
	this.descricao = descricao;
    }

    /**
     * @return valor do atributo textoDocumento.
     */
    public String getTextoDocumento() {
	return textoDocumento;
    }

    /**
     * Define valor do atributo textoDocumento.
     * 
     * @param textoDocumento
     */
    public void setTextoDocumento(String textoDocumento) {
	this.textoDocumento = textoDocumento;
    }

}
