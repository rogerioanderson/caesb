/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Timestamp;

import br.com.citframework.dto.IDto;

/**
 * @author ygor.magalhaes
 *
 */
public class HistoricoAcaoCurriculoDTO implements IDto {

	private Integer idHistoricoAcaoCurriculo;
	private Integer idCurriculo;
	private Integer idJustificativaAcaoCurriculo;
	private Integer idUsuario;
	private String complementoJustificativa;
	private Timestamp dataHora;
	private String acao;
	
	private String tela;
	private String indiceTriagem;
	
	public Integer getIdHistoricoAcaoCurriculo() {
		return idHistoricoAcaoCurriculo;
	}
	public void setIdHistoricoAcaoCurriculo(Integer idHistoricoAcaoCurriculo) {
		this.idHistoricoAcaoCurriculo = idHistoricoAcaoCurriculo;
	}
	public Integer getIdCurriculo() {
		return idCurriculo;
	}
	public void setIdCurriculo(Integer idCurriculo) {
		this.idCurriculo = idCurriculo;
	}
	public Integer getIdJustificativaAcaoCurriculo() {
		return idJustificativaAcaoCurriculo;
	}
	public void setIdJustificativaAcaoCurriculo(Integer idJustificativaAcaoCurriculo) {
		this.idJustificativaAcaoCurriculo = idJustificativaAcaoCurriculo;
	}
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getComplementoJustificativa() {
		return complementoJustificativa;
	}
	public void setComplementoJustificativa(String complementoJustificativa) {
		this.complementoJustificativa = complementoJustificativa;
	}
	public Timestamp getDataHora() {
		return dataHora;
	}
	public void setDataHora(Timestamp dataHora) {
		this.dataHora = dataHora;
	}
	public String getAcao() {
		return acao;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public String getTela() {
		return tela;
	}
	public void setTela(String tela) {
		this.tela = tela;
	}
	public String getIndiceTriagem() {
		return indiceTriagem;
	}
	public void setIndiceTriagem(String indiceTriagem) {
		this.indiceTriagem = indiceTriagem;
	}

}
