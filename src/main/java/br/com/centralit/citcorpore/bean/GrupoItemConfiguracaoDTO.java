/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

public class GrupoItemConfiguracaoDTO implements IDto {

	private static final long serialVersionUID = -2207719425970001340L;

	public static final String GRUPO_PADRAO_DESENVOLVIMENTO = "Desenvolvimento - Padr�o";
	public static final String GRUPO_PADRAO_HOMOLOGACAO = "Homologa��o - Padr�o";
	public static final String GRUPO_PADRAO_PRODUCAO = "Produ��o - Padr�o";
	
	private Integer idGrupoItemConfiguracao;
	private String nomeGrupoItemConfiguracao;
	private String emailGrupoItemConfiguracao;
	private Date dataInicio;
	private Date dataFim;
	private Integer idGrupoItemConfiguracaoPai;

	/**
	 * @return Retorna o id do grupo de item de configuracao.
	 */
	public Integer getIdGrupoItemConfiguracao() {
		return idGrupoItemConfiguracao;
	}

	/**
	 * @param pIdGrupoItemConfiguracao modifica o atributo idGrupoItemConfiguracao.
	 */
	public void setIdGrupoItemConfiguracao(Integer pIdGrupoItemConfiguracao) {
		idGrupoItemConfiguracao = pIdGrupoItemConfiguracao;
	}


	/**
	 * @return Retorna o nome do grupo de item de configuracao.
	 */
	public String getNomeGrupoItemConfiguracao() {
		return nomeGrupoItemConfiguracao;
	}

	/**
	 * @param pNomeGrupoItemConfiguracao modifica o atributo nomeGrupoItemConfiguracao.
	 */
	public void setNomeGrupoItemConfiguracao(String pNomeGrupoItemConfiguracao) {
		nomeGrupoItemConfiguracao = pNomeGrupoItemConfiguracao;
	}


	/**
	 * @return Retorna a data inicial do registro.
	 */
	public Date getDataInicio() {
		return dataInicio;
	}

	/**
	 * @param pDataInicio modifica o atributo dataInicio.
	 */
	public void setDataInicio(Date pDataInicio) {
		dataInicio = pDataInicio;
	}


	/**
	 * @return Retorna a data final do registro.
	 */
	public Date getDataFim() {
		return dataFim;
	}

	/**
	 * @param pDataFim modifica o atributo dataFim.
	 */
	public void setDataFim(Date pDataFim) {
		dataFim = pDataFim;
	}

	public String getEmailGrupoItemConfiguracao() {
		return emailGrupoItemConfiguracao;
	}

	public void setEmailGrupoItemConfiguracao(String emailGrupoItemConfiguracao) {
		this.emailGrupoItemConfiguracao = emailGrupoItemConfiguracao;
	}

	public Integer getIdGrupoItemConfiguracaoPai() {
		return idGrupoItemConfiguracaoPai;
	}

	public void setIdGrupoItemConfiguracaoPai(Integer idGrupoItemConfiguracaoPai) {
		this.idGrupoItemConfiguracaoPai = idGrupoItemConfiguracaoPai;
	}
}
