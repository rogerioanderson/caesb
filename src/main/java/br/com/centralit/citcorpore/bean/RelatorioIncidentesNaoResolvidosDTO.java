/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

public class RelatorioIncidentesNaoResolvidosDTO implements IDto {

	private static final long serialVersionUID = 1L;
	private Date dataReferencia;
	private Date periodoReferencia;
	private Integer qtdDiasAbertos;
	private Integer idContrato;
	private String contrato;
	private String funcionario;
	private String listaGrupos;
	private String listaServicos;
	private String formatoArquivoRelatorio;
	private Integer numeroSolicitacao;
	private String nomeservico;
	private String responsavel;
	private String solicitante;
	private String tipoServico;
	private String situacao;
	private String dataCriacao;
	private Integer qtdDiasAtrasos;

	public String getListaServicos() {
		return listaServicos;
	}

	public void setListaServicos(String listaServicos) {
		this.listaServicos = listaServicos;
	}

	public Integer getNumeroSolicitacao() {
		return numeroSolicitacao;
	}

	public void setNumeroSolicitacao(Integer numeroSolicitacao) {
		this.numeroSolicitacao = numeroSolicitacao;
	}

	public String getNomeservico() {
		return nomeservico;
	}

	public void setNomeservico(String nomeservico) {
		this.nomeservico = nomeservico;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public String getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}

	public String getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(String dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Integer getQtdDiasAtrasos() {
		return qtdDiasAtrasos;
	}

	public void setQtdDiasAtrasos(Integer qtdDiasAtrasos) {
		this.qtdDiasAtrasos = qtdDiasAtrasos;
	}

	public Date getDataReferencia() {
		return dataReferencia;
	}

	public void setDataReferencia(Date dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	public Date getPeriodoReferencia() {
		return periodoReferencia;
	}

	public void setPeriodoReferencia(Date periodoReferencia) {
		this.periodoReferencia = periodoReferencia;
	}

	public Integer getQtdDiasAbertos() {
		return qtdDiasAbertos;
	}

	public void setQtdDiasAbertos(Integer qtdDiasAbertos) {
		this.qtdDiasAbertos = qtdDiasAbertos;
	}

	public Integer getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Integer idContrato) {
		this.idContrato = idContrato;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(String funcionario) {
		this.funcionario = funcionario;
	}

	public String getListaGrupos() {
		return listaGrupos;
	}

	public void setListaGrupos(String listaGrupos) {
		this.listaGrupos = listaGrupos;
	}

	public String getFormatoArquivoRelatorio() {
		return formatoArquivoRelatorio;
	}

	public void setFormatoArquivoRelatorio(String formatoArquivoRelatorio) {
		this.formatoArquivoRelatorio = formatoArquivoRelatorio;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
