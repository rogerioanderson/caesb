/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class TipoProdutoDTO implements IDto {
	private Integer idTipoProduto;
	private Integer idCategoria;
	private Integer idUnidadeMedida;
	private String nomeProduto;
	private String situacao;
	private String aceitaRequisicao;
	private String nomeUnidadeMedida;
	private String nomeCategoria;
	private String siglaUnidMedida;

	public Integer getIdCategoria(){
		return this.idCategoria;
	}
	public void setIdCategoria(Integer parm){
		this.idCategoria = parm;
	}

	public Integer getIdUnidadeMedida(){
		return this.idUnidadeMedida;
	}
	public void setIdUnidadeMedida(Integer parm){
		this.idUnidadeMedida = parm;
	}

	public String getNomeProduto(){
		return this.nomeProduto;
	}
	public void setNomeProduto(String parm){
		this.nomeProduto = parm;
	}
	public String getSituacao(){
		return this.situacao;
	}
	public void setSituacao(String parm){
		this.situacao = parm;
	}

	public String getAceitaRequisicao(){
		return this.aceitaRequisicao;
	}
	public void setAceitaRequisicao(String parm){
		this.aceitaRequisicao = parm;
	}
    public String getNomeCategoria() {
        return nomeCategoria;
    }
    public void setNomeCategoria(String nomeCategoria) {
        this.nomeCategoria = nomeCategoria;
    }
    public String getSiglaUnidMedida() {
        return siglaUnidMedida;
    }
    public void setSiglaUnidMedida(String siglaUnidMedida) {
        this.siglaUnidMedida = siglaUnidMedida;
    }
    public Integer getIdTipoProduto() {
        return idTipoProduto;
    }
    public void setIdTipoProduto(Integer idTipoProduto) {
        this.idTipoProduto = idTipoProduto;
    }
	public String getNomeUnidadeMedida() {
		return nomeUnidadeMedida;
	}
	public void setNomeUnidadeMedida(String nomeUnidadeMedida) {
		this.nomeUnidadeMedida = nomeUnidadeMedida;
	}

}
