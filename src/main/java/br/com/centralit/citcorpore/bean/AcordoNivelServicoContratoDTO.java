/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class AcordoNivelServicoContratoDTO implements IDto {
	private Integer idAcordoNivelServicoContrato;
	private Integer idContrato;
	private String descricaoAcordo;
	private String detalhamentoAcordo;
	private Double valorLimite;
	private String unidadeValorLimite;
	private java.sql.Date dataInicio;
	private java.sql.Date dataFim;
	private String descricaoGlosa;
	private Integer idFormula;
	private String deleted;

	public Integer getIdAcordoNivelServicoContrato(){
		return this.idAcordoNivelServicoContrato;
	}
	public void setIdAcordoNivelServicoContrato(Integer parm){
		this.idAcordoNivelServicoContrato = parm;
	}

	public Integer getIdContrato(){
		return this.idContrato;
	}
	public void setIdContrato(Integer parm){
		this.idContrato = parm;
	}

	public String getDescricaoAcordo(){
		return this.descricaoAcordo;
	}
	public void setDescricaoAcordo(String parm){
		this.descricaoAcordo = parm;
	}

	public String getDetalhamentoAcordo(){
		return this.detalhamentoAcordo;
	}
	public void setDetalhamentoAcordo(String parm){
		this.detalhamentoAcordo = parm;
	}

	public Double getValorLimite(){
		return this.valorLimite;
	}
	public void setValorLimite(Double parm){
		this.valorLimite = parm;
	}

	public String getUnidadeValorLimite(){
		return this.unidadeValorLimite;
	}
	public void setUnidadeValorLimite(String parm){
		this.unidadeValorLimite = parm;
	}

	public java.sql.Date getDataInicio(){
		return this.dataInicio;
	}
	public void setDataInicio(java.sql.Date parm){
		this.dataInicio = parm;
	}

	public java.sql.Date getDataFim(){
		return this.dataFim;
	}
	public void setDataFim(java.sql.Date parm){
		this.dataFim = parm;
	}

	public String getDescricaoGlosa(){
		return this.descricaoGlosa;
	}
	public void setDescricaoGlosa(String parm){
		this.descricaoGlosa = parm;
	}
	public Integer getIdFormula() {
	    return idFormula;
	}
	public void setIdFormula(Integer idFormula) {
	    this.idFormula = idFormula;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

}
