/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Timestamp;

/**
 * @author euler.ramos
 * Classe utilizada para transferir os dados do cadastro de Problema para o iReport
 */
public class ProblemaJasperDTO {
	
	private Integer idProblema;
	private String titulo;
	private String contrato;
	private Timestamp dataHoraInicio;
	private Timestamp dataHoraFim;
	private Timestamp dataHoraCaptura;
	//private Timestamp dataHoraConclusao; N�o tem esse campo na tela e nem na tabela do banco.
	private String nomeSolicitante;
	private String descrSituacao;
	private String nomeProprietario;
	private String nomeGrupoAtual;
	private String descricao;
	private String fechamento;
	private String nomeCategoriaProblema;
	
	private String acoesRealizadasCorretamente;
	private String acoesRealizadasIncorretamente;
	private String possiveisMelhoriasFuturas;
	private String comoPrevenirRecorrencia;
	private String necessidadeAcompanhamento;
	private String responsabilidadeTerceiros;
	
	private String causaRaiz;
	private String mensagemErroAssociada;
	private String diagnostico;
	
	private Object listaICs;
	private Object listaMudancas;
	private Object listaSolicitacoes;
	private Object listaSolucoesContorno;
	private Object listaSolucoesDefinitivas;
	private Object listaErrosConhecidos;
	private Object listaProblemas;
	
	public Integer getIdProblema() {
		return idProblema;
	}
	public void setIdProblema(Integer idProblema) {
		this.idProblema = idProblema;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public Timestamp getDataHoraInicio() {
		return dataHoraInicio;
	}
	public void setDataHoraInicio(Timestamp dataHoraInicio) {
		this.dataHoraInicio = dataHoraInicio;
	}
	public Timestamp getDataHoraFim() {
		return dataHoraFim;
	}
	public void setDataHoraFim(Timestamp dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
	}
	public Timestamp getDataHoraCaptura() {
		return dataHoraCaptura;
	}
	public void setDataHoraCaptura(Timestamp dataHoraCaptura) {
		this.dataHoraCaptura = dataHoraCaptura;
	}
	public String getNomeSolicitante() {
		return nomeSolicitante;
	}
	public void setNomeSolicitante(String nomeSolicitante) {
		this.nomeSolicitante = nomeSolicitante;
	}
	public String getDescrSituacao() {
		return descrSituacao;
	}
	public void setDescrSituacao(String descrSituacao) {
		this.descrSituacao = descrSituacao;
	}
	public String getNomeProprietario() {
		return nomeProprietario;
	}
	public void setNomeProprietario(String nomeProprietario) {
		this.nomeProprietario = nomeProprietario;
	}
	public String getNomeGrupoAtual() {
		return nomeGrupoAtual;
	}
	public void setNomeGrupoAtual(String nomeGrupoAtual) {
		this.nomeGrupoAtual = nomeGrupoAtual;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getFechamento() {
		return fechamento;
	}
	public void setFechamento(String fechamento) {
		this.fechamento = fechamento;
	}
	public String getNomeCategoriaProblema() {
		return nomeCategoriaProblema;
	}
	public void setNomeCategoriaProblema(String nomeCategoriaProblema) {
		this.nomeCategoriaProblema = nomeCategoriaProblema;
	}
	public String getAcoesRealizadasCorretamente() {
		return acoesRealizadasCorretamente;
	}
	public void setAcoesRealizadasCorretamente(String acoesRealizadasCorretamente) {
		this.acoesRealizadasCorretamente = acoesRealizadasCorretamente;
	}
	public String getAcoesRealizadasIncorretamente() {
		return acoesRealizadasIncorretamente;
	}
	public void setAcoesRealizadasIncorretamente(String acoesRealizadasIncorretamente) {
		this.acoesRealizadasIncorretamente = acoesRealizadasIncorretamente;
	}
	public String getPossiveisMelhoriasFuturas() {
		return possiveisMelhoriasFuturas;
	}
	public void setPossiveisMelhoriasFuturas(String possiveisMelhoriasFuturas) {
		this.possiveisMelhoriasFuturas = possiveisMelhoriasFuturas;
	}
	public String getComoPrevenirRecorrencia() {
		return comoPrevenirRecorrencia;
	}
	public void setComoPrevenirRecorrencia(String comoPrevenirRecorrencia) {
		this.comoPrevenirRecorrencia = comoPrevenirRecorrencia;
	}
	public String getNecessidadeAcompanhamento() {
		return necessidadeAcompanhamento;
	}
	public void setNecessidadeAcompanhamento(String necessidadeAcompanhamento) {
		this.necessidadeAcompanhamento = necessidadeAcompanhamento;
	}
	public String getResponsabilidadeTerceiros() {
		return responsabilidadeTerceiros;
	}
	public void setResponsabilidadeTerceiros(String responsabilidadeTerceiros) {
		this.responsabilidadeTerceiros = responsabilidadeTerceiros;
	}
	public String getCausaRaiz() {
		return causaRaiz;
	}
	public void setCausaRaiz(String causaRaiz) {
		this.causaRaiz = causaRaiz;
	}
	public String getMensagemErroAssociada() {
		return mensagemErroAssociada;
	}
	public void setMensagemErroAssociada(String mensagemErroAssociada) {
		this.mensagemErroAssociada = mensagemErroAssociada;
	}
	public String getDiagnostico() {
		return diagnostico;
	}
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
	public Object getListaICs() {
		return listaICs;
	}
	public void setListaICs(Object listaICs) {
		this.listaICs = listaICs;
	}
	public Object getListaMudancas() {
		return listaMudancas;
	}
	public void setListaMudancas(Object listaMudancas) {
		this.listaMudancas = listaMudancas;
	}
	public Object getListaSolicitacoes() {
		return listaSolicitacoes;
	}
	public void setListaSolicitacoes(Object listaSolicitacoes) {
		this.listaSolicitacoes = listaSolicitacoes;
	}
	public Object getListaSolucoesContorno() {
		return listaSolucoesContorno;
	}
	public void setListaSolucoesContorno(Object listaSolucoesContorno) {
		this.listaSolucoesContorno = listaSolucoesContorno;
	}
	public Object getListaSolucoesDefinitivas() {
		return listaSolucoesDefinitivas;
	}
	public void setListaSolucoesDefinitivas(Object listaSolucoesDefinitivas) {
		this.listaSolucoesDefinitivas = listaSolucoesDefinitivas;
	}
	public Object getListaErrosConhecidos() {
		return listaErrosConhecidos;
	}
	public void setListaErrosConhecidos(Object listaErrosConhecidos) {
		this.listaErrosConhecidos = listaErrosConhecidos;
	}
	public Object getListaProblemas() {
		return listaProblemas;
	}
	public void setListaProblemas(Object listaProblemas) {
		this.listaProblemas = listaProblemas;
	}
	
}
