/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class CriterioAvaliacaoDTO implements IDto {
	private Integer idCriterio;
	private String descricao;
	private String aplicavelCotacao;
	private String aplicavelAvaliacaoSolicitante;
	private String aplicavelAvaliacaoComprador;
    private String aplicavelQualificacaoFornecedor;
    private String tipoAvaliacao;

    private Integer sequencia;
	private String obs;
	private String valor;
	
	private Integer peso;

	public Integer getIdCriterio(){
		return this.idCriterio;
	}
	public void setIdCriterio(Integer parm){
		this.idCriterio = parm;
	}

	public String getDescricao(){
		return this.descricao;
	}
	public void setDescricao(String parm){
		this.descricao = parm;
	}
	public String getAplicavelCotacao(){
		return this.aplicavelCotacao;
	}
	public void setAplicavelCotacao(String parm){
		this.aplicavelCotacao = parm;
	}

	public String getAplicavelAvaliacaoSolicitante(){
		return this.aplicavelAvaliacaoSolicitante;
	}
	public void setAplicavelAvaliacaoSolicitante(String parm){
		this.aplicavelAvaliacaoSolicitante = parm;
	}

	public String getAplicavelAvaliacaoComprador(){
		return this.aplicavelAvaliacaoComprador;
	}
	public void setAplicavelAvaliacaoComprador(String parm){
		this.aplicavelAvaliacaoComprador = parm;
	}
	public Integer getSequencia() {
		return sequencia;
	}
	public void setSequencia(Integer sequencia) {
		this.sequencia = sequencia;
	}
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
    public Integer getPeso() {
        return peso;
    }
    public void setPeso(Integer peso) {
        this.peso = peso;
    }
    public String getAplicavelQualificacaoFornecedor() {
        return aplicavelQualificacaoFornecedor;
    }
    public void setAplicavelQualificacaoFornecedor(
            String aplicavelQualificacaoFornecedor) {
        this.aplicavelQualificacaoFornecedor = aplicavelQualificacaoFornecedor;
    }
    public String getTipoAvaliacao() {
        return tipoAvaliacao;
    }
    public void setTipoAvaliacao(String tipoAvaliacao) {
        this.tipoAvaliacao = tipoAvaliacao;
    }

}
