/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class CronogramaDTO implements IDto {
	private String ta;
	private Integer idProjeto;
	private Integer idLinhaBaseProjeto;
	private String novaLinhaBaseProjeto;
	private String situacaoLinhaBaseProjetoSelecionada;
	private Integer idTemplateImpressao;
	private String marcosProjeto;

	public String getTa() {
		return ta;
	}

	public void setTa(String ta) {
		this.ta = ta;
	}

	public Integer getIdProjeto() {
		return idProjeto;
	}

	public void setIdProjeto(Integer idProjeto) {
		this.idProjeto = idProjeto;
	}

	public Integer getIdLinhaBaseProjeto() {
		return idLinhaBaseProjeto;
	}

	public void setIdLinhaBaseProjeto(Integer idLinhaBaseProjeto) {
		this.idLinhaBaseProjeto = idLinhaBaseProjeto;
	}

	public String getNovaLinhaBaseProjeto() {
		return novaLinhaBaseProjeto;
	}

	public void setNovaLinhaBaseProjeto(String novaLinhaBaseProjeto) {
		this.novaLinhaBaseProjeto = novaLinhaBaseProjeto;
	}

	public String getSituacaoLinhaBaseProjetoSelecionada() {
		return situacaoLinhaBaseProjetoSelecionada;
	}

	public void setSituacaoLinhaBaseProjetoSelecionada(
			String situacaoLinhaBaseProjetoSelecionada) {
		this.situacaoLinhaBaseProjetoSelecionada = situacaoLinhaBaseProjetoSelecionada;
	}

	public Integer getIdTemplateImpressao() {
		return idTemplateImpressao;
	}

	public void setIdTemplateImpressao(Integer idTemplateImpressao) {
		this.idTemplateImpressao = idTemplateImpressao;
	}

	public String getMarcosProjeto() {
		return marcosProjeto;
	}

	public void setMarcosProjeto(String marcosProjeto) {
		this.marcosProjeto = marcosProjeto;
	}
}
