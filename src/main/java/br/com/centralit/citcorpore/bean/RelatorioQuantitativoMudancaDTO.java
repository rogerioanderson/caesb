/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Collection;

import br.com.citframework.dto.IDto;

public class RelatorioQuantitativoMudancaDTO implements IDto {
	private static final long serialVersionUID = 1L;

	private Collection<RequisicaoMudancaDTO> listaQuantidadeERelacionamentos;
	private Collection<RequisicaoMudancaDTO> listaQuantidadePorImpacto;
	private Collection<RequisicaoMudancaDTO> listaQuantidadePorPeriodo;
	private Collection<RequisicaoMudancaDTO> listaQuantidadePorProprietario;
	private Collection<RequisicaoMudancaDTO> listaQuantidadePorSolicitante;
	private Collection<RequisicaoMudancaDTO> listaQuantidadePorStatus;
	private Collection<RequisicaoMudancaDTO> listaQuantidadePorUrgencia;
	private Collection<RequisicaoMudancaDTO> listaQuantidadeSemAprovacaoPorPeriodo;

	public Collection<RequisicaoMudancaDTO> getListaQuantidadeERelacionamentos() {
		return listaQuantidadeERelacionamentos;
	}

	public Collection<RequisicaoMudancaDTO> getListaQuantidadePorImpacto() {
		return listaQuantidadePorImpacto;
	}

	public Collection<RequisicaoMudancaDTO> getListaQuantidadePorPeriodo() {
		return listaQuantidadePorPeriodo;
	}

	public Collection<RequisicaoMudancaDTO> getListaQuantidadePorProprietario() {
		return listaQuantidadePorProprietario;
	}

	public Collection<RequisicaoMudancaDTO> getListaQuantidadePorSolicitante() {
		return listaQuantidadePorSolicitante;
	}

	public Collection<RequisicaoMudancaDTO> getListaQuantidadePorStatus() {
		return listaQuantidadePorStatus;
	}

	public Collection<RequisicaoMudancaDTO> getListaQuantidadePorUrgencia() {
		return listaQuantidadePorUrgencia;
	}

	public Collection<RequisicaoMudancaDTO> getListaQuantidadeSemAprovacaoPorPeriodo() {
		return listaQuantidadeSemAprovacaoPorPeriodo;
	}

	public void setListaQuantidadeERelacionamentos(Collection<RequisicaoMudancaDTO> listaQuantidadeERelacionamentos) {
		this.listaQuantidadeERelacionamentos = listaQuantidadeERelacionamentos;
	}

	public void setListaQuantidadePorImpacto(Collection<RequisicaoMudancaDTO> listaQuantidadePorImpacto) {
		this.listaQuantidadePorImpacto = listaQuantidadePorImpacto;
	}

	public void setListaQuantidadePorPeriodo(Collection<RequisicaoMudancaDTO> listaQuantidadePorPeriodo) {
		this.listaQuantidadePorPeriodo = listaQuantidadePorPeriodo;
	}

	public void setListaQuantidadePorProprietario(Collection<RequisicaoMudancaDTO> listaQuantidadePorProprietario) {
		this.listaQuantidadePorProprietario = listaQuantidadePorProprietario;
	}

	public void setListaQuantidadePorSolicitante(Collection<RequisicaoMudancaDTO> listaQuantidadePorSolicitante) {
		this.listaQuantidadePorSolicitante = listaQuantidadePorSolicitante;
	}

	public void setListaQuantidadePorStatus(Collection<RequisicaoMudancaDTO> listaQuantidadePorStatus) {
		this.listaQuantidadePorStatus = listaQuantidadePorStatus;
	}

	public void setListaQuantidadePorUrgencia(Collection<RequisicaoMudancaDTO> listaQuantidadePorUrgencia) {
		this.listaQuantidadePorUrgencia = listaQuantidadePorUrgencia;
	}

	public void setListaQuantidadeSemAprovacaoPorPeriodo(Collection<RequisicaoMudancaDTO> listaQuantidadeSemAprovacaoPorPeriodo) {
		this.listaQuantidadeSemAprovacaoPorPeriodo = listaQuantidadeSemAprovacaoPorPeriodo;
	}

}
