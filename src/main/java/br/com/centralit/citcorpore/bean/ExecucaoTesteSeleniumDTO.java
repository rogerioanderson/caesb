/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class ExecucaoTesteSeleniumDTO implements IDto {
	
	private static final long serialVersionUID = 4942283426115769824L;

	private String usuarioDoTeste;
	private String senhaDoTeste;
	private Integer navegadorDeExecucao;
	private Integer pacotesDeTeste;
	private String cargos;
	private String clientes;
	private String colaborador;
	private String empresa;
	private String fornecedor;
	private String unidade;
	private String usuario;
	private String tipoEventoServico;
	private String categoriaServico;
	private String categoriaSolucao;
	private String causaIncidente;
	private String condicaoOperacao;
	private String importanciaNegocio;
	private String justificativaSolicitacao;
	private String localExecucaoServico;
	private String prioridade;
	private String situacao;
	private String tipoServico;
	private String tipoSolicitacaoServico;
	private String caracteristica;
	private String grupoItem;
	private String midiaSoftware;
	private String softwareListaNegra;
	private String tipoItemConfiguracao;
	private String categoriaGaleria;
	private String palavraGemea;
	private String origem;
	private String formulaOS;
	private String formula;
	
	
	public String getUsuarioDoTeste() {
		return usuarioDoTeste;
	}
	public void setUsuarioDoTeste(String usuarioDoTeste) {
		this.usuarioDoTeste = usuarioDoTeste;
	}
	public String getSenhaDoTeste() {
		return senhaDoTeste;
	}
	public void setSenhaDoTeste(String senhaDoTeste) {
		this.senhaDoTeste = senhaDoTeste;
	}
	public Integer getNavegadorDeExecucao() {
		return navegadorDeExecucao;
	}
	public void setNavegadorDeExecucao(Integer navegadorDeExecucao) {
		this.navegadorDeExecucao = navegadorDeExecucao;
	}
	public Integer getPacotesDeTeste() {
		return pacotesDeTeste;
	}
	public void setPacotesDeTeste(Integer pacotesDeTeste) {
		this.pacotesDeTeste = pacotesDeTeste;
	}
	public String getCargos() {
		return cargos;
	}
	public void setCargos(String cargos) {
		this.cargos = cargos;
	}
	public String getClientes() {
		return clientes;
	}
	public void setClientes(String clientes) {
		this.clientes = clientes;
	}
	public String getColaborador() {
		return colaborador;
	}
	public void setColaborador(String colaborador) {
		this.colaborador = colaborador;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
	}
	public String getUnidade() {
		return unidade;
	}
	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTipoEventoServico() {
		return tipoEventoServico;
	}
	public void setTipoEventoServico(String tipoEventoServico) {
		this.tipoEventoServico = tipoEventoServico;
	}
	public String getCategoriaServico() {
		return categoriaServico;
	}
	public void setCategoriaServico(String categoriaServico) {
		this.categoriaServico = categoriaServico;
	}
	public String getCategoriaSolucao() {
		return categoriaSolucao;
	}
	public void setCategoriaSolucao(String categoriaSolucao) {
		this.categoriaSolucao = categoriaSolucao;
	}
	public String getCausaIncidente() {
		return causaIncidente;
	}
	public void setCausaIncidente(String causaIncidente) {
		this.causaIncidente = causaIncidente;
	}
	public String getCondicaoOperacao() {
		return condicaoOperacao;
	}
	public void setCondicaoOperacao(String condicaoOperacao) {
		this.condicaoOperacao = condicaoOperacao;
	}
	public String getImportanciaNegocio() {
		return importanciaNegocio;
	}
	public void setImportanciaNegocio(String importanciaNegocio) {
		this.importanciaNegocio = importanciaNegocio;
	}
	public String getJustificativaSolicitacao() {
		return justificativaSolicitacao;
	}
	public void setJustificativaSolicitacao(String justificativaSolicitacao) {
		this.justificativaSolicitacao = justificativaSolicitacao;
	}
	public String getLocalExecucaoServico() {
		return localExecucaoServico;
	}
	public void setLocalExecucaoServico(String localExecucaoServico) {
		this.localExecucaoServico = localExecucaoServico;
	}
	public String getPrioridade() {
		return prioridade;
	}
	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getTipoServico() {
		return tipoServico;
	}
	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}
	public String getTipoSolicitacaoServico() {
		return tipoSolicitacaoServico;
	}
	public void setTipoSolicitacaoServico(String tipoSolicitacaoServico) {
		this.tipoSolicitacaoServico = tipoSolicitacaoServico;
	}
	public String getCaracteristica() {
		return caracteristica;
	}
	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}
	public String getGrupoItem() {
		return grupoItem;
	}
	public void setGrupoItem(String grupoItem) {
		this.grupoItem = grupoItem;
	}
	public String getMidiaSoftware() {
		return midiaSoftware;
	}
	public void setMidiaSoftware(String midiaSoftware) {
		this.midiaSoftware = midiaSoftware;
	}
	public String getSoftwareListaNegra() {
		return softwareListaNegra;
	}
	public void setSoftwareListaNegra(String softwareListaNegra) {
		this.softwareListaNegra = softwareListaNegra;
	}
	public String getTipoItemConfiguracao() {
		return tipoItemConfiguracao;
	}
	public void setTipoItemConfiguracao(String tipoItemConfiguracao) {
		this.tipoItemConfiguracao = tipoItemConfiguracao;
	}
	public String getCategoriaGaleria() {
		return categoriaGaleria;
	}
	public void setCategoriaGaleria(String categoriaGaleria) {
		this.categoriaGaleria = categoriaGaleria;
	}
	public String getPalavraGemea() {
		return palavraGemea;
	}
	public void setPalavraGemea(String palavraGemea) {
		this.palavraGemea = palavraGemea;
	}
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public String getFormulaOS() {
		return formulaOS;
	}
	public void setFormulaOS(String formulaOS) {
		this.formulaOS = formulaOS;
	}
	public String getFormula() {
		return formula;
	}
	public void setFormula(String formula) {
		this.formula = formula;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
