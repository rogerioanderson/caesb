/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * 
 */
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

/**
 * @author valdoilo.damasceno
 * 
 */
public class PesquisaItemConfiguracaoDTO implements IDto {

	private static final long serialVersionUID = 4955623053777469110L;

	private Integer idPesquisaItemConfiguracao;

	private Integer idItemConfiguracao;

	private Integer idGrupoItemConfiguracao;

	private String ip;

	private Date dataInicio;

	private Date dataFim;

	private String instalacao;

	private String desinstalacao;

	private String inventario;

	private String nomeGrupoItemConfiguracao;
	
	private Integer idItemConfiguracaoFilho;
	
	private String itemRelacionado;


	
	public String getInventario() {
		return inventario;
	}

	public void setInventario(String inventario) {
		this.inventario = inventario;
	}


	/**
	 * @return valor do atributo idPesquisaItemConfiguracao.
	 */
	public Integer getIdPesquisaItemConfiguracao() {
		return idPesquisaItemConfiguracao;
	}

	/**
	 * Define valor do atributo idPesquisaItemConfiguracao.
	 * 
	 * @param idPesquisaItemConfiguracao
	 */
	public void setIdPesquisaItemConfiguracao(Integer idPesquisaItemConfiguracao) {
		this.idPesquisaItemConfiguracao = idPesquisaItemConfiguracao;
	}

	/**
	 * @return valor do atributo idItemConfiguracao.
	 */
	public Integer getIdItemConfiguracao() {
		return idItemConfiguracao;
	}

	/**
	 * Define valor do atributo idItemConfiguracao.
	 * 
	 * @param idItemConfiguracao
	 */
	public void setIdItemConfiguracao(Integer idItemConfiguracao) {
		this.idItemConfiguracao = idItemConfiguracao;
	}


	/**
	 * @return Retorna o valor do atributo idGrupoItemConfiguracao.
	 */
	public Integer getIdGrupoItemConfiguracao() {
		return idGrupoItemConfiguracao;
	}

	/**
	 * @param pIdGrupoItemConfiguracao modifica o atributo idGrupoItemConfiguracao.
	 */
	public void setIdGrupoItemConfiguracao(Integer pIdGrupoItemConfiguracao) {
		idGrupoItemConfiguracao = pIdGrupoItemConfiguracao;
	}

	/**
	 * @return valor do atributo ip.
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * Define valor do atributo ip.
	 * 
	 * @param ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return valor do atributo dataInicio.
	 */
	public Date getDataInicio() {
		return dataInicio;
	}

	/**
	 * Define valor do atributo dataInicio.
	 * 
	 * @param dataInicio
	 */
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	/**
	 * @return valor do atributo dataFim.
	 */
	public Date getDataFim() {
		return dataFim;
	}

	/**
	 * Define valor do atributo dataFim.
	 * 
	 * @param dataFim
	 */
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	/**
	 * @return valor do atributo instalacao.
	 */
	public String getInstalacao() {
		return instalacao;
	}

	/**
	 * Define valor do atributo instalacao.
	 * 
	 * @param instalacao
	 */
	public void setInstalacao(String instalacao) {
		this.instalacao = instalacao;
	}

	/**
	 * @return valor do atributo desinstalacao.
	 */
	public String getDesinstalacao() {
		return desinstalacao;
	}

	/**
	 * Define valor do atributo desinstalacao.
	 * 
	 * @param desinstalacao
	 */
	public void setDesinstalacao(String desinstalacao) {
		this.desinstalacao = desinstalacao;
	}


	/**
	 * @return valor do atributo nomeGrupo.
	 */
	public String getNomeGrupoItemConfiguracao() {
		return nomeGrupoItemConfiguracao;
	}

	/**
	 * @param nomeGrupo
	 */
	public void setNomeGrupoItemConfiguracao(String pNomeGrupoItemConfiguracao) {
		nomeGrupoItemConfiguracao = pNomeGrupoItemConfiguracao;
	}

	/**
	 * @return the idItemConfiguracaoFilho
	 */
	public Integer getIdItemConfiguracaoFilho() {
		return idItemConfiguracaoFilho;
	}

	/**
	 * @param idItemConfiguracaoFilho the idItemConfiguracaoFilho to set
	 */
	public void setIdItemConfiguracaoFilho(Integer idItemConfiguracaoFilho) {
		this.idItemConfiguracaoFilho = idItemConfiguracaoFilho;
	}

	/**
	 * @return the itemRelacionado
	 */
	public String getItemRelacionado() {
		return itemRelacionado;
	}

	/**
	 * @param itemRelacionado the itemRelacionado to set
	 */
	public void setItemRelacionado(String itemRelacionado) {
		this.itemRelacionado = itemRelacionado;
	}
}
