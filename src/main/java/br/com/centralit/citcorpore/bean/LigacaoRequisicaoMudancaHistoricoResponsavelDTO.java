/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

/**
 * @author murilo.pacheco
 * classe criada para fazer a liga��o da tabela de responsaveis com os hitoricos.
 *
 */
public class LigacaoRequisicaoMudancaHistoricoResponsavelDTO implements IDto {
	
	private static final long serialVersionUID = 1L;
	
	private Integer idLigacao_mud_hist_resp;
	private Integer idRequisicaoMudanca;
	private Integer idHistoricoMudanca;
	private Integer idRequisicaoMudancaResp;
	
	public Integer getIdLigacao_mud_hist_resp() {
		return idLigacao_mud_hist_resp;
	}
	public void setIdLigacao_mud_hist_resp(Integer idLigacao_mud_hist_resp) {
		this.idLigacao_mud_hist_resp = idLigacao_mud_hist_resp;
	}
	public Integer getIdRequisicaoMudanca() {
		return idRequisicaoMudanca;
	}
	public void setIdRequisicaoMudanca(Integer idRequisicaoMudanca) {
		this.idRequisicaoMudanca = idRequisicaoMudanca;
	}
	public Integer getIdHistoricoMudanca() {
		return idHistoricoMudanca;
	}
	public void setIdHistoricoMudanca(Integer idHistoricoMudanca) {
		this.idHistoricoMudanca = idHistoricoMudanca;
	}
	public Integer getIdRequisicaoMudancaResp() {
		return idRequisicaoMudancaResp;
	}
	public void setIdRequisicaoMudancaResp(Integer idRequisicaoMudancaResp) {
		this.idRequisicaoMudancaResp = idRequisicaoMudancaResp;
	}
	
}
