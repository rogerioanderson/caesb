/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

public class ExcecaoCalendarioDTO implements IDto {
	private Integer idExcecaoCalendario;
	private Integer idCalendario;
	private Integer idJornada;
	private String tipo;
	private Date dataInicio;
	private Date dataTermino;

	public Integer getIdExcecaoCalendario(){
		return this.idExcecaoCalendario;
	}
	public void setIdExcecaoCalendario(Integer parm){
		this.idExcecaoCalendario = parm;
	}

	public Integer getIdCalendario(){
		return this.idCalendario;
	}
	public void setIdCalendario(Integer parm){
		this.idCalendario = parm;
	}

	public Integer getIdJornada(){
		return this.idJornada;
	}
	public void setIdJornada(Integer parm){
		this.idJornada = parm;
	}

	public String getTipo(){
		return this.tipo;
	}
	public void setTipo(String parm){
		this.tipo = parm;
	}

	public Date getDataInicio(){
		return this.dataInicio;
	}
	public void setDataInicio(Date parm){
		this.dataInicio = parm;
	}

	public Date getDataTermino(){
		return this.dataTermino;
	}
	public void setDataTermino(Date parm){
		this.dataTermino = parm;
	}

}
