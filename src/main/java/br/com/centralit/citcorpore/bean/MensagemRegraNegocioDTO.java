/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.centralit.citcorpore.util.Enumerados.ResultadoValidacao;
import br.com.citframework.dto.IDto;
import br.com.citframework.util.Constantes;


public class MensagemRegraNegocioDTO implements IDto {
    public static final String AVISO = "A";
    public static final String ERRO = "E";

    private String tipo;
	private String mensagem;
	
    public MensagemRegraNegocioDTO() {
        this.tipo = "";
        this.mensagem = "";
    }
	   
	public MensagemRegraNegocioDTO(String tipo, String mensagem) {
	    this.tipo = tipo;
	    this.mensagem = mensagem;
	}
	
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public String getMensagem() {
        return mensagem;
    }
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
	
    public String getImagem() {
        String imagem = "vazio.gif";
        if (tipo != null && !tipo.trim().equals("")) {
            if (tipo.equalsIgnoreCase(ResultadoValidacao.V.name())) {
                imagem = "validado.png";
            }else if (tipo.equalsIgnoreCase(ResultadoValidacao.E.name()) || tipo.equalsIgnoreCase(ResultadoValidacao.I.name())) {
                imagem = "erro.png";
            }else{
                imagem = "excl.gif";
            }
        }
        return Constantes.getValue("CONTEXTO_APLICACAO")+"/imagens/"+imagem;
    }
    
}
