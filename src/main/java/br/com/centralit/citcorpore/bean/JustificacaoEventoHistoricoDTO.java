/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

/**
 * @author breno.guimaraes Entidade de relacionamento entre as tabelas
 *         JustificacaoFalhas, Evento e HistoricoTentativas.
 */
public class JustificacaoEventoHistoricoDTO {
    private String ip;
    private Integer idItemConfiguracao;
    private Integer idBaseItemConfiguracao;
    private String identificacaoItemConfiguracao;
    private String descricaoTentativa;
    private Integer idEvento;
    private String nomeGrupo;
    private String nomeUnidade;
    private Integer idEmpregado;
    private Integer idHistoricoTentativa;

    public String getIp() {
	return ip;
    }

    public void setIp(String ip) {
	this.ip = ip;
    }

    public Integer getIdItemConfiguracao() {
	return idItemConfiguracao;
    }

    public void setIdItemConfiguracao(Integer idItemConfiguracao) {
	this.idItemConfiguracao = idItemConfiguracao;
    }

    public Integer getIdBaseItemConfiguracao() {
	return idBaseItemConfiguracao;
    }

    public void setIdBaseItemConfiguracao(Integer idBaseItemConfiguracao) {
	this.idBaseItemConfiguracao = idBaseItemConfiguracao;
    }

    public String getIdentificacaoItemConfiguracao() {
	return identificacaoItemConfiguracao;
    }

    public void setIdentificacaoItemConfiguracao(String identificacaoItemConfiguracao) {
	this.identificacaoItemConfiguracao = identificacaoItemConfiguracao;
    }

    public String getDescricaoTentativa() {
	return descricaoTentativa;
    }

    public void setDescricaoTentativa(String descricaoTentativa) {
	this.descricaoTentativa = descricaoTentativa;
    }

    public Integer getIdEvento() {
	return idEvento;
    }

    public void setIdEvento(Integer idEvento) {
	this.idEvento = idEvento;
    }

    public String getNomeGrupo() {
	return nomeGrupo;
    }

    public void setNomeGrupo(String nomeGrupo) {
	this.nomeGrupo = nomeGrupo;
    }

    public String getNomeUnidade() {
	return nomeUnidade;
    }

    public void setNomeUnidade(String nomeUnidade) {
	this.nomeUnidade = nomeUnidade;
    }

    public Integer getIdEmpregado() {
	return idEmpregado;
    }

    public void setIdEmpregado(Integer idEmpregado) {
	this.idEmpregado = idEmpregado;
    }

    public Integer getIdHistoricoTentativa() {
	return idHistoricoTentativa;
    }

    public void setIdHistoricoTentativa(Integer idHistoricoTentativa) {
	this.idHistoricoTentativa = idHistoricoTentativa;
    }

}
