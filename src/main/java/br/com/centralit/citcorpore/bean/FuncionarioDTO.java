/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Timestamp;

import br.com.citframework.dto.IDto;

public class FuncionarioDTO implements IDto {
	
	private static final long serialVersionUID = -27609697957190213L;
	
	private Integer idFuncionario;
	private Integer idEmpregado;
	private String nome;
	private String cpf;
	private String banco;
	private String agencia;
	private String conta;
	private Timestamp dataInicio;
	private Timestamp dataFim;
	
	public Integer getIdEmpregado() {
		return idEmpregado;
	}
	public void setIdEmpregado(Integer idEmpregado) {
		this.idEmpregado = idEmpregado;
	}
	public Integer getIdFuncionario() {
		return idFuncionario;
	}
	public void setIdFuncionario(Integer idFuncionario) {
		this.idFuncionario = idFuncionario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getConta() {
		return conta;
	}
	public void setConta(String conta) {
		this.conta = conta;
	}
	public Timestamp getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Timestamp dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Timestamp getDataFim() {
		return dataFim;
	}
	public void setDataFim(Timestamp dataFim) {
		this.dataFim = dataFim;
	}
	
}
