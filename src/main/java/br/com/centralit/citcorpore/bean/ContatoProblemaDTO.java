/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.centralit.citcorpore.util.Util;
import br.com.citframework.dto.IDto;

public class ContatoProblemaDTO implements IDto{
	/**
	 * @author geber.costa
	 */
	private static final long serialVersionUID = 4531312747440713206L;
	private Integer idContatoProblema;
    private String nomeContato;
    private String telefoneContato;
    private String emailContato;
    private String observacao;
    private Integer idLocalidade;
    private String ramal;
    
    /**
     * @return the nomecontato
     */
    public String getNomecontato() {
        return Util.tratarAspasSimples(nomeContato );
    }
    /**
     * @param nomecontato the nomecontato to set
     */
    public void setNomecontato(String nomecontato) {
        this.nomeContato = nomecontato;
    }
    /**
     * @return the telefonecontato
     */
    public String getTelefonecontato() {
        	if(telefoneContato == null)
        		telefoneContato = " ";     	
        return telefoneContato;
    }
    /**
     * @param telefonecontato the telefonecontato to set
     */
    public void setTelefonecontato(String telefonecontato) {
        this.telefoneContato = telefonecontato;
    }
    /**
     * @return the emailcontato
     */
    public String getEmailcontato() {
    	if(emailContato == null)
    		emailContato = " ";    	
        return Util.tratarAspasSimples(this.emailContato);
    }
    /**
     * @param emailcontato the emailcontato to set
     */
    public void setEmailcontato(String emailcontato) {
        this.emailContato = emailcontato;
    }
    /**
     * @return the localizacaofisica
     */
    public String getObservacao() {
    	if(observacao == null)
    		observacao = " ";
        return observacao;
    }
    /**
     * @param observacao the localizacaofisica to set
     */
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    public String getDadosStr() {
		StringBuilder str = new StringBuilder();
		if (getNomecontato() != null) {
			str.append("Nome: "+getNomecontato() + "\n");
			if (getTelefonecontato() != null)
				str.append("Telefone: "+getTelefonecontato() + "\n");
			if (getEmailcontato() != null)
				str.append("Email: "+getEmailcontato() + "\n");
			if (getObservacao() != null && getObservacao().length() > 0)
				str.append("Observa��o: "+getObservacao());
		}
		return str.toString();
    }
	/**
	 * @return the idLocalidade
	 */
	public Integer getIdLocalidade() {
		return idLocalidade;
	}
	/**
	 * @param idLocalidade the idLocalidade to set
	 */
	public void setIdLocalidade(Integer idLocalidade) {
		this.idLocalidade = idLocalidade;
	}
	/**
	 * @return the ramal
	 */
	public String getRamal() {
		return ramal;
	}
	/**
	 * @param ramal the ramal to set
	 */
	public void setRamal(String ramal) {
		this.ramal = ramal;
	}
	public Integer getIdContatoProblema() {
		return idContatoProblema;
	}
	public void setIdContatoProblema(Integer idContatoProblema) {
		this.idContatoProblema = idContatoProblema;
	}
    
}
