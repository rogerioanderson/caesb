/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

public class TipoMudancaDTO implements IDto{
	
	/**
	 * @author geber.costa
	 * Baseado em Categoria Mudan�a, por�m sem 2 atributos : 
	 * idCategoriaMudancaPai
	 * idGrupoNivel1
	 */
	private static final long serialVersionUID = 4864126394598758208L;
	private Integer idTipoMudanca;
	//Foi verificado n�o utilizar os atributos comentados, por�m por precau��o n�o foram excluidos
	//private Integer idCategoriaMudancaPai;
	private String nomeTipoMudanca;
	private Date dataInicio;
	private Date dataFim;
	private Integer idTipoFluxo;
	private Integer idModeloEmailCriacao;
	private Integer idModeloEmailFinalizacao;
	private Integer idModeloEmailAcoes;
	//private Integer idGrupoNivel1;
	private Integer idGrupoExecutor;
	private Integer idCalendario;
	private String exigeAprovacao;
	
	private String impacto;
	
	private String urgencia;
	
	public Integer getIdTipoMudanca() {
		return idTipoMudanca;
	}
	public void setIdTipoMudanca(Integer idTipoMudanca) {
		this.idTipoMudanca = idTipoMudanca;
	}
	public String getNomeTipoMudanca() {
		return nomeTipoMudanca;
	}
	public void setNomeTipoMudanca(String nomeTipoMudanca) {
		this.nomeTipoMudanca = nomeTipoMudanca;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public Integer getIdTipoFluxo() {
		return idTipoFluxo;
	}
	public void setIdTipoFluxo(Integer idTipoFluxo) {
		this.idTipoFluxo = idTipoFluxo;
	}
	public Integer getIdModeloEmailCriacao() {
		return idModeloEmailCriacao;
	}
	public void setIdModeloEmailCriacao(Integer idModeloEmailCriacao) {
		this.idModeloEmailCriacao = idModeloEmailCriacao;
	}
	public Integer getIdModeloEmailFinalizacao() {
		return idModeloEmailFinalizacao;
	}
	public void setIdModeloEmailFinalizacao(Integer idModeloEmailFinalizacao) {
		this.idModeloEmailFinalizacao = idModeloEmailFinalizacao;
	}
	public Integer getIdModeloEmailAcoes() {
		return idModeloEmailAcoes;
	}
	public void setIdModeloEmailAcoes(Integer idModeloEmailAcoes) {
		this.idModeloEmailAcoes = idModeloEmailAcoes;
	}
	public Integer getIdGrupoExecutor() {
		return idGrupoExecutor;
	}
	public void setIdGrupoExecutor(Integer idGrupoExecutor) {
		this.idGrupoExecutor = idGrupoExecutor;
	}
	public Integer getIdCalendario() {
		return idCalendario;
	}
	public void setIdCalendario(Integer idCalendario) {
		this.idCalendario = idCalendario;
	}
	/**
	 * @return the impacto
	 */
	public String getImpacto() {
		return impacto;
	}
	/**
	 * @param impacto the impacto to set
	 */
	public void setImpacto(String impacto) {
		this.impacto = impacto;
	}
	/**
	 * @return the urgencia
	 */
	public String getUrgencia() {
		return urgencia;
	}
	/**
	 * @param urgencia the urgencia to set
	 */
	public void setUrgencia(String urgencia) {
		this.urgencia = urgencia;
	}
	public String getExigeAprovacao() {
		return exigeAprovacao;
	}
	public void setExigeAprovacao(String exigeAprovacao) {
		this.exigeAprovacao = exigeAprovacao;
	}
	
	
}
