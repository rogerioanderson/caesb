/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Timestamp;

/**
 * @author euler.ramos Classe utilizada para transferir os dados da Requisi��o de Mudan�a para o iReport
 */
public class ReqLiberacaoJasperDTO {

	private Integer idRequisicaoLiberacao;
	private String titulo;
	private Timestamp dataHoraInicio;
	private Timestamp dataHoraTermino;
	private Timestamp dataHoraInicioAgendada;
	private Timestamp dataHoraTerminoAgendada;
	private Timestamp dataHoraCaptura;
	private Timestamp dataHoraConclusao;
	private String nomeSolicitante;
	private String descrSituacao;
	private String nomeProprietario;
	private String tipo;
	private String nomeGrupoAtual;
	private String descricao;
	private String fechamento;
	private String risco;
	private String nomePrioridade;
	private String numeroContrato;
	private Object listaMudancas;
	private Object listaICs;
	private Object listaProblemas;
	private Object listaDocumentosLegais;
	private Object listaMidiasDefinitivas;
	private Object listaDocumentosGerais;
	private Object listaPapeis;
	private Object listaCompras;

	public Integer getIdRequisicaoLiberacao() {
		return idRequisicaoLiberacao;
	}

	public void setIdRequisicaoLiberacao(Integer idRequisicaoLiberacao) {
		this.idRequisicaoLiberacao = idRequisicaoLiberacao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Timestamp getDataHoraInicio() {
		return dataHoraInicio;
	}

	public void setDataHoraInicio(Timestamp dataHoraInicio) {
		this.dataHoraInicio = dataHoraInicio;
	}

	public Timestamp getDataHoraTermino() {
		return dataHoraTermino;
	}

	public void setDataHoraTermino(Timestamp dataHoraTermino) {
		this.dataHoraTermino = dataHoraTermino;
	}

	public Timestamp getDataHoraInicioAgendada() {
		return dataHoraInicioAgendada;
	}

	public Timestamp getDataHoraTerminoAgendada() {
		return dataHoraTerminoAgendada;
	}

	public void setDataHoraTerminoAgendada(Timestamp dataHoraTerminoAgendada) {
		this.dataHoraTerminoAgendada = dataHoraTerminoAgendada;
	}

	public void setDataHoraInicioAgendada(Timestamp dataHoraInicioAgendada) {
		this.dataHoraInicioAgendada = dataHoraInicioAgendada;
	}

	public Timestamp getDataHoraCaptura() {
		return dataHoraCaptura;
	}

	public void setDataHoraCaptura(Timestamp dataHoraCaptura) {
		this.dataHoraCaptura = dataHoraCaptura;
	}

	public Timestamp getDataHoraConclusao() {
		return dataHoraConclusao;
	}

	public void setDataHoraConclusao(Timestamp dataHoraConclusao) {
		this.dataHoraConclusao = dataHoraConclusao;
	}

	public String getNomeSolicitante() {
		return nomeSolicitante;
	}

	public void setNomeSolicitante(String nomeSolicitante) {
		this.nomeSolicitante = nomeSolicitante;
	}

	public String getDescrSituacao() {
		return descrSituacao;
	}

	public void setDescrSituacao(String descrSituacao) {
		this.descrSituacao = descrSituacao;
	}

	public String getNomeProprietario() {
		return nomeProprietario;
	}

	public void setNomeProprietario(String nomeProprietario) {
		this.nomeProprietario = nomeProprietario;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNomeGrupoAtual() {
		return nomeGrupoAtual;
	}

	public void setNomeGrupoAtual(String nomeGrupoAtual) {
		this.nomeGrupoAtual = nomeGrupoAtual;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getFechamento() {
		return fechamento;
	}

	public void setFechamento(String fechamento) {
		this.fechamento = fechamento;
	}

	public String getRisco() {
		return risco;
	}

	public void setRisco(String risco) {
		this.risco = risco;
	}

	public String getNomePrioridade() {
		return nomePrioridade;
	}

	public void setNomePrioridade(String nomePrioridade) {
		this.nomePrioridade = nomePrioridade;
	}

	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public Object getListaMudancas() {
		return listaMudancas;
	}

	public void setListaMudancas(Object listaMudancas) {
		this.listaMudancas = listaMudancas;
	}

	public Object getListaICs() {
		return listaICs;
	}

	public void setListaICs(Object listaICs) {
		this.listaICs = listaICs;
	}

	public Object getListaProblemas() {
		return listaProblemas;
	}

	public void setListaProblemas(Object listaProblemas) {
		this.listaProblemas = listaProblemas;
	}

	public Object getListaDocumentosLegais() {
		return listaDocumentosLegais;
	}

	public void setListaDocumentosLegais(Object listaDocumentosLegais) {
		this.listaDocumentosLegais = listaDocumentosLegais;
	}

	public Object getListaMidiasDefinitivas() {
		return listaMidiasDefinitivas;
	}

	public void setListaMidiasDefinitivas(Object listaMidiasDefinitivas) {
		this.listaMidiasDefinitivas = listaMidiasDefinitivas;
	}

	public Object getListaDocumentosGerais() {
		return listaDocumentosGerais;
	}

	public void setListaDocumentosGerais(Object listaDocumentosGerais) {
		this.listaDocumentosGerais = listaDocumentosGerais;
	}

	public Object getListaPapeis() {
		return listaPapeis;
	}

	public void setListaPapeis(Object listaPapeis) {
		this.listaPapeis = listaPapeis;
	}

	public Object getListaCompras() {
		return listaCompras;
	}

	public void setListaCompras(Object listaCompras) {
		this.listaCompras = listaCompras;
	}

}
