/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 *
 * ************************************************************************************************************
 *
 * Dependentes: BI Citsmart
 *
 * Obs:
 * Qualquer altera��o nesta tabela dever� ser informada aos respons�veis pelo desenvolvimento do BI Citsmart.
 * O database do BI Citsmart precisa ter suas tabelas atualizadas de acordo com as mudan�as nesta tabela.
 *
 * ************************************************************************************************************
 *
 */

package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class GlosaOSDTO implements IDto {
    private Integer idGlosaOS;
    private Integer idOs;
    private java.sql.Date dataCriacao;
    private java.sql.Date dataUltModificacao;
    private String descricaoGlosa;
    private String ocorrencias;
    private Double percAplicado;
    private Double custoGlosa;
    private Double numeroOcorrencias;
    private Integer sequencia;


    public Integer getIdGlosaOS(){
        return idGlosaOS;
    }
    public void setIdGlosaOS(Integer parm){
        idGlosaOS = parm;
    }

    public Integer getIdOs(){
        return idOs;
    }
    public void setIdOs(Integer parm){
        idOs = parm;
    }

    public java.sql.Date getDataCriacao(){
        return dataCriacao;
    }
    public void setDataCriacao(java.sql.Date parm){
        dataCriacao = parm;
    }

    public java.sql.Date getDataUltModificacao(){
        return dataUltModificacao;
    }
    public void setDataUltModificacao(java.sql.Date parm){
        dataUltModificacao = parm;
    }

    public String getDescricaoGlosa(){
        return descricaoGlosa;
    }
    public void setDescricaoGlosa(String parm){
        descricaoGlosa = parm;
    }

    public String getOcorrencias(){
        return ocorrencias;
    }
    public void setOcorrencias(String parm){
        ocorrencias = parm;
    }

    public Double getPercAplicado(){
        return percAplicado;
    }
    public void setPercAplicado(Double parm){
        percAplicado = parm;
    }

    public Double getCustoGlosa(){
        return custoGlosa;
    }
    public void setCustoGlosa(Double parm){
        custoGlosa = parm;
    }

    public Double getNumeroOcorrencias(){
        return numeroOcorrencias;
    }
    public void setNumeroOcorrencias(Double parm){
        numeroOcorrencias = parm;
    }
    public Integer getSequencia() {
        return sequencia;
    }
    public void setSequencia(Integer sequencia) {
        this.sequencia = sequencia;
    }

}
