/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;
import br.com.citframework.util.UtilStrings;

public class GaleriaImagensDTO implements IDto {
	private Integer idImagem;
	private Integer idCategoriaGaleriaImagem;
	private String nomeImagem;
	private String descricaoImagem;
	private String detalhamento;
	private String extensao;
	private String nomeCategoria;
	private String selecaoImagemEdicao;
	
	public Integer getIdImagem() {
		return idImagem;
	}
	public void setIdImagem(Integer idImagem) {
		this.idImagem = idImagem;
	}
	public Integer getIdCategoriaGaleriaImagem() {
		return idCategoriaGaleriaImagem;
	}
	public void setIdCategoriaGaleriaImagem(Integer idCategoriaGaleriaImagem) {
		this.idCategoriaGaleriaImagem = idCategoriaGaleriaImagem;
	}
	public String getNomeImagem() {
		return nomeImagem;
	}
	public void setNomeImagem(String nomeImagem) {
		this.nomeImagem = nomeImagem;
	}
	public void setDescricaoAndCategoriaImagem(String str) {
		
	}
	public String getDescricaoAndCategoriaImagem() {
		return UtilStrings.nullToVazio(getNomeCategoria()) + " - " + UtilStrings.nullToVazio(getDescricaoImagem());
	}
	public String getDescricaoImagem() {
		return descricaoImagem;
	}
	public void setDescricaoImagem(String descricaoImagem) {
		this.descricaoImagem = descricaoImagem;
	}
	public String getDetalhamento() {
		return detalhamento;
	}
	public void setDetalhamento(String detalhamento) {
		this.detalhamento = detalhamento;
	}
	public String getExtensao() {
		return extensao;
	}
	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}
	public String getNomeCategoria() {
		return nomeCategoria;
	}
	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}
	public String getSelecaoImagemEdicao() {
		return selecaoImagemEdicao;
	}
	public void setSelecaoImagemEdicao(String selecaoImagemEdicao) {
		this.selecaoImagemEdicao = selecaoImagemEdicao;
	}
	
}
