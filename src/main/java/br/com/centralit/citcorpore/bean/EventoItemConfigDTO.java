/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;
import java.util.List;

import br.com.citframework.dto.IDto;

@SuppressWarnings("serial")
public class EventoItemConfigDTO implements IDto {
    private Integer idEvento;
    private Integer idEmpresa;
    private String descricao;
    private String ligarCasoDesl;
    private String usuario;
    private String senha;
    private Date dataInicio;
    private Date dataFim;
    /**
     * Lista dos itens de Execu��o!
     */
    private List<ItemConfigEventoDTO> lstItemConfigEvento;
    
    private List<EventoGrupoDTO> lstGrupo;
    private List<EventoItemConfigRelDTO> lstItemConfiguracao;
    
    private Integer idItemCfg;
    private String  nomeItemCfg;
    private String linhaComando;
    
 
    /**
     * @return the linhaComando
     */
    public String getLinhaComando() {
        return linhaComando;
    }

    /**
     * @param linhaComando the linhaComando to set
     */
    public void setLinhaComando(String linhaComando) {
        this.linhaComando = linhaComando;
    }

    /**
     * @return the idItemCfg
     */
    public Integer getIdItemCfg() {
        return idItemCfg;
    }

    /**
     * @param idItemCfg the idItemCfg to set
     */
    public void setIdItemCfg(Integer idItemCfg) {
        this.idItemCfg = idItemCfg;
    }

    /**
     * @return the nomeItemCfg
     */
    public String getNomeItemCfg() {
        return nomeItemCfg;
    }

    /**
     * @param nomeItemCfg the nomeItemCfg to set
     */
    public void setNomeItemCfg(String nomeItemCfg) {
        this.nomeItemCfg = nomeItemCfg;
    }

    public Integer getIdEvento() {
	return idEvento;
    }

    public void setIdEvento(Integer idEvento) {
	this.idEvento = idEvento;
    }

    public Integer getIdEmpresa() {
	return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
	this.idEmpresa = idEmpresa;
    }

    public String getDescricao() {
	return descricao;
    }

    public void setDescricao(String descricao) {
	this.descricao = descricao;
    }

    public String getLigarCasoDesl() {
	return ligarCasoDesl;
    }

    public void setLigarCasoDesl(String ligarCasoDesl) {
	this.ligarCasoDesl = ligarCasoDesl;
    }

    public String getUsuario() {
	return usuario;
    }

    public void setUsuario(String usuario) {
	this.usuario = usuario;
    }

    public String getSenha() {
	return senha;
    }

    public void setSenha(String senha) {
	this.senha = senha;
    }

    public Date getDataInicio() {
	return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
	this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
	return dataFim;
    }

    public void setDataFim(Date dataFim) {
	this.dataFim = dataFim;
    }

    public List<ItemConfigEventoDTO> getLstItemConfigEvento() {
	return lstItemConfigEvento;
    }

    public void setLstItemConfigEvento(List<ItemConfigEventoDTO> lstItemConfigEvento) {
	this.lstItemConfigEvento = lstItemConfigEvento;
    }


	/**
	 * @return the lstGrupo
	 */
	public List<EventoGrupoDTO> getLstGrupo() {
		return lstGrupo;
	}

	/**
	 * @param lstGrupo the lstGrupo to set
	 */
	public void setLstGrupo(List<EventoGrupoDTO> lstGrupo) {
		this.lstGrupo = lstGrupo;
	}

	/**
	 * @return the lstItemConfiguracao
	 */
	public List<EventoItemConfigRelDTO> getLstItemConfiguracao() {
		return lstItemConfiguracao;
	}

	/**
	 * @param lstItemConfiguracao the lstItemConfiguracao to set
	 */
	public void setLstItemConfiguracao(
			List<EventoItemConfigRelDTO> lstItemConfiguracao) {
		this.lstItemConfiguracao = lstItemConfiguracao;
	}

}
