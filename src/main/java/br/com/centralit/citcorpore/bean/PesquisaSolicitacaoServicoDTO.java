/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import javax.servlet.http.HttpServletRequest;

import br.com.citframework.util.UtilI18N;

public class PesquisaSolicitacaoServicoDTO {

	private Date dataFim;

	private Date dataFimFechamento;

	private Date dataInicio;

	private Date dataInicioFechamento;

	private String exibirCampoDescricao;

	private String faseAtual;

	private String flag;

	private String grupoAtual;

	private Integer idContrato;

	private Integer idFaseAtual;

	private Integer idGrupoAtual;

	private Integer idItemConfiguracao;

	private Integer idLocalidade;

	private Integer idOrigem;

	private Integer idPrioridade;

	private Integer idResponsavel;

	private Integer idServico;

	private Integer idSolicitacaoServico;

	private Integer idSolicitacaoServicoPesquisa;

	private Integer idSolicitante;

	private Integer idTipoDemandaServico;

	private Integer idUnidade;

	private Integer idUsuarioResponsavelAtual;

	private String nomeItemConfiguracao;

	private String nomeResponsavel;

	private String nomeSolicitante;

	private String nomeTipoDemandaServico;

	private String nomeUsuarioResponsavelAtual;

	private String ordenacao;

	private String origem;

	private Integer pagAtual;

	private String palavraChave;

	private String prioridade;

	private String situacao;

	private String somenteMinhasUnidades;

	private Integer totalItens;

	private Integer totalItensPorPagina;

	private Integer totalPagina;

	private UsuarioDTO usuarioLogado;
	
	private boolean somenteNaoSincronizadas;
	
	private boolean somenteSincronizadas;	
	
	private String origemSincronizacao;

	/**
	 * @return the dataFim
	 */
	public Date getDataFim() {
		return dataFim;
	}

	/**
	 * @return the dataFimFechamento
	 */
	public Date getDataFimFechamento() {
		return dataFimFechamento;
	}

	/**
	 * @return the dataInicio
	 */
	public Date getDataInicio() {
		return dataInicio;
	}

	/**
	 * @return the dataInicioFechamento
	 */
	public Date getDataInicioFechamento() {
		return dataInicioFechamento;
	}

	/**
	 * @return the exibirCampoDescricao
	 */
	public String getExibirCampoDescricao() {
		return exibirCampoDescricao;
	}

	public String getFaseAtual() {
		return faseAtual;
	}

	public String getFlag() {
		return flag;
	}

	public String getGrupoAtual() {
		return grupoAtual;
	}

	/**
	 * @return the idContrato
	 */
	public Integer getIdContrato() {
		return idContrato;
	}

	/**
	 * @return the idFaseAtual
	 */
	public Integer getIdFaseAtual() {
		return idFaseAtual;
	}

	/**
	 * @return the idGrupoAtual
	 */
	public Integer getIdGrupoAtual() {
		return idGrupoAtual;
	}

	/**
	 * @return the idItemConfiguracao
	 */
	public Integer getIdItemConfiguracao() {
		return idItemConfiguracao;
	}

	public Integer getIdLocalidade() {
		return idLocalidade;
	}

	/**
	 * @return the idOrigem
	 */
	public Integer getIdOrigem() {
		return idOrigem;
	}

	/**
	 * @return the idPrioridade
	 */
	public Integer getIdPrioridade() {
		return idPrioridade;
	}

	/**
	 * @return the idResponsavel
	 */
	public Integer getIdResponsavel() {
		return idResponsavel;
	}

	/**
	 * @return the idServico
	 */
	public Integer getIdServico() {
		return idServico;
	}

	public Integer getIdSolicitacaoServico() {
		return idSolicitacaoServico;
	}

	/**
	 * @return the idSolicitacaoServicoPesquisa
	 */
	public Integer getIdSolicitacaoServicoPesquisa() {
		return idSolicitacaoServicoPesquisa;
	}

	/**
	 * @return the idSolicitante
	 */
	public Integer getIdSolicitante() {
		return idSolicitante;
	}

	/**
	 * @return the idTipoDemandaServico
	 */
	public Integer getIdTipoDemandaServico() {
		return idTipoDemandaServico;
	}

	/**
	 * @return the idUnidade
	 */
	public Integer getIdUnidade() {
		return idUnidade;
	}

	/**
	 * @return the idUsuarioResponsavelAtual
	 */
	public Integer getIdUsuarioResponsavelAtual() {
		return idUsuarioResponsavelAtual;
	}

	public String getNomeItemConfiguracao() {
		return nomeItemConfiguracao;
	}

	/**
	 * @return the nomeResponsavel
	 */

	public String getNomeResponsavel() {
		return nomeResponsavel;
	}

	public String getNomeSolicitante() {
		return nomeSolicitante;
	}

	public String getNomeTipoDemandaServico() {
		return nomeTipoDemandaServico;
	}

	/**
	 * @return the nomeUsuarioResponsavelAtual
	 */
	public String getNomeUsuarioResponsavelAtual() {
		return nomeUsuarioResponsavelAtual;
	}

	/**
	 * @return the ordenacao
	 */
	public String getOrdenacao() {
		return ordenacao;
	}

	public String getOrigem() {
		return origem;
	}

	public Integer getPagAtual() {
		return pagAtual;
	}

	/**
	 * @return the palavraChave
	 */
	public String getPalavraChave() {
		return palavraChave;
	}

	public String getPrioridade() {
		return prioridade;
	}

	/**
	 * @return the situacao
	 */
	public String getSituacao() {
		return situacao;
	}

	/**
	 * 25/09/2013 Trata a internacionaliza��o de acordo com a situa��o Foi verificado que existem todos as situa��es internacionalizadas em citcorpore.comum.{situacao} sendo (situacao} em minusculo
	 * 
	 * @param request
	 * @return String
	 * @author uelen.pereira
	 */
	public String getSituacaoInternacionalizada(HttpServletRequest request) {
		return UtilI18N.internacionaliza(request, "citcorpore.comum." + this.situacao.trim().toLowerCase());
	}

	public String getSomenteMinhasUnidades() {
		return somenteMinhasUnidades;
	}

	public Integer getTotalItensPorPagina() {
		return totalItensPorPagina;
	}

	public Integer getTotalPagina() {
		return totalPagina;
	}

	public Integer getTotalItens() {
		return totalItens;
	}

	public void setTotalItens(Integer totalItens) {
		this.totalItens = totalItens;
	}

	/**
	 * @author euler.ramos Criado para passar o usu�rio logado diretamente para o DAO que precisa, em alguns momentos das unidades que o usu�rio pode acessar.
	 * @return
	 */
	public UsuarioDTO getUsuarioLogado() {
		return usuarioLogado;
	}

	/**
	 * @param dataFim
	 *            the dataFim to set
	 */
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	/**
	 * @param dataFimFechamento
	 *            the dataFimFechamento to set
	 */
	public void setDataFimFechamento(Date dataFimFechamento) {
		this.dataFimFechamento = dataFimFechamento;
	}

	/**
	 * @param dataInicio
	 *            the dataInicio to set
	 */
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	/**
	 * @param dataInicioFechamento
	 *            the dataInicioFechamento to set
	 */
	public void setDataInicioFechamento(Date dataInicioFechamento) {
		this.dataInicioFechamento = dataInicioFechamento;
	}

	/**
	 * @param exibirCampoDescricao
	 *            the exibirCampoDescricao to set
	 */
	public void setExibirCampoDescricao(String exibirCampoDescricao) {
		this.exibirCampoDescricao = exibirCampoDescricao;
	}

	public void setFaseAtual(String faseAtual) {
		this.faseAtual = faseAtual;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public void setGrupoAtual(String grupoAtual) {
		this.grupoAtual = grupoAtual;
	}

	/**
	 * @param idContrato
	 *            the idContrato to set
	 */
	public void setIdContrato(Integer idContrato) {
		this.idContrato = idContrato;
	}

	/**
	 * @param idFaseAtual
	 *            the idFaseAtual to set
	 */
	public void setIdFaseAtual(Integer idFaseAtual) {
		this.idFaseAtual = idFaseAtual;
	}

	/**
	 * @param idGrupoAtual
	 *            the idGrupoAtual to set
	 */
	public void setIdGrupoAtual(Integer idGrupoAtual) {
		this.idGrupoAtual = idGrupoAtual;
	}

	/**
	 * @param idItemConfiguracao
	 *            the idItemConfiguracao to set
	 */
	public void setIdItemConfiguracao(Integer idItemConfiguracao) {
		this.idItemConfiguracao = idItemConfiguracao;
	}

	public void setIdLocalidade(Integer idLocalidade) {
		this.idLocalidade = idLocalidade;
	}

	/**
	 * @param idOrigem
	 *            the idOrigem to set
	 */
	public void setIdOrigem(Integer idOrigem) {
		this.idOrigem = idOrigem;
	}

	/**
	 * @param idPrioridade
	 *            the idPrioridade to set
	 */
	public void setIdPrioridade(Integer idPrioridade) {
		this.idPrioridade = idPrioridade;
	}

	/**
	 * @param idResponsavel
	 *            the idResponsavel to set
	 */
	public void setIdResponsavel(Integer idResponsavel) {
		this.idResponsavel = idResponsavel;
	}

	/**
	 * @param idServico
	 *            the idServico to set
	 */
	public void setIdServico(Integer idServico) {
		this.idServico = idServico;
	}

	public void setIdSolicitacaoServico(Integer idSolicitacaoServico) {
		this.idSolicitacaoServico = idSolicitacaoServico;
	}

	/**
	 * @param idSolicitacaoServicoPesquisa
	 *            the idSolicitacaoServicoPesquisa to set
	 */
	public void setIdSolicitacaoServicoPesquisa(Integer idSolicitacaoServicoPesquisa) {
		this.idSolicitacaoServicoPesquisa = idSolicitacaoServicoPesquisa;
	}

	/**
	 * @param idSolicitante
	 *            the idSolicitante to set
	 */
	public void setIdSolicitante(Integer idSolicitante) {
		this.idSolicitante = idSolicitante;
	}

	/**
	 * @param idTipoDemandaServico
	 *            the idTipoDemandaServico to set
	 */
	public void setIdTipoDemandaServico(Integer idTipoDemandaServico) {
		this.idTipoDemandaServico = idTipoDemandaServico;
	}

	/**
	 * @param idUnidade
	 *            the idUnidade to set
	 */
	public void setIdUnidade(Integer idUnidade) {
		this.idUnidade = idUnidade;
	}

	/**
	 * @param idUsuarioResponsavelAtual
	 *            the idUsuarioResponsavelAtual to set
	 */
	public void setIdUsuarioResponsavelAtual(Integer idUsuarioResponsavelAtual) {
		this.idUsuarioResponsavelAtual = idUsuarioResponsavelAtual;
	}

	public void setNomeItemConfiguracao(String nomeItemConfiguracao) {
		this.nomeItemConfiguracao = nomeItemConfiguracao;
	}

	/**
	 * @param nomeResponsavel
	 *            the nomeResponsavel to set
	 */
	public void setNomeResponsavel(String nomeResponsavel) {
		this.nomeResponsavel = nomeResponsavel;
	}

	public void setNomeSolicitante(String nomeSolicitante) {
		this.nomeSolicitante = nomeSolicitante;
	}

	public void setNomeTipoDemandaServico(String nomeTipoDemandaServico) {
		this.nomeTipoDemandaServico = nomeTipoDemandaServico;
	}

	/**
	 * @param nomeUsuarioResponsavelAtual
	 *            the nomeUsuarioResponsavelAtual to set
	 */
	public void setNomeUsuarioResponsavelAtual(String nomeUsuarioResponsavelAtual) {
		this.nomeUsuarioResponsavelAtual = nomeUsuarioResponsavelAtual;
	}

	/**
	 * @param ordenacao
	 *            the ordenacao to set
	 */
	public void setOrdenacao(String ordenacao) {
		this.ordenacao = ordenacao;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public void setPagAtual(Integer pagAtual) {
		this.pagAtual = pagAtual;
	}

	/**
	 * @param palavraChave
	 *            the palavraChave to set
	 */
	public void setPalavraChave(String palavraChave) {
		this.palavraChave = palavraChave;
	}

	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}

	/**
	 * @param situacao
	 *            the situacao to set
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public void setSomenteMinhasUnidades(String somenteMinhasUnidades) {
		this.somenteMinhasUnidades = somenteMinhasUnidades;
	}

	public void setTotalItensPorPagina(Integer totalItens) {
		this.totalItensPorPagina = totalItens;
	}

	public void setTotalPagina(Integer paginaTotal) {
		this.totalPagina = paginaTotal;
	}

	public void setUsuarioLogado(UsuarioDTO usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public boolean isSomenteNaoSincronizadas() {
		return somenteNaoSincronizadas;
	}

	public void setSomenteNaoSincronizadas(boolean somenteNaoSincronizadas) {
		this.somenteNaoSincronizadas = somenteNaoSincronizadas;
	}

	public boolean isSomenteSincronizadas() {
		return somenteSincronizadas;
	}

	public void setSomenteSincronizadas(boolean somenteSincronizadas) {
		this.somenteSincronizadas = somenteSincronizadas;
	}

	public String getOrigemSincronizacao() {
		return origemSincronizacao;
	}

	public void setOrigemSincronizacao(String origemSincronizacao) {
		this.origemSincronizacao = origemSincronizacao;
	}

}
