/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class LimiteAlcadaDTO implements IDto {
	private Integer idLimiteAlcada;
	private Integer idAlcada;
	private Integer idGrupo;
	private String tipoLimite;
	private String abrangenciaCentroCusto;
	private Double limiteItemUsoInterno;
	private Double limiteMensalUsoInterno;
    private Double limiteItemAtendCliente;
    private Double limiteMensalAtendCliente;
	private String situacao;

	public Integer getIdLimiteAlcada(){
		return this.idLimiteAlcada;
	}
	public void setIdLimiteAlcada(Integer parm){
		this.idLimiteAlcada = parm;
	}

	public Integer getIdAlcada(){
		return this.idAlcada;
	}
	public void setIdAlcada(Integer parm){
		this.idAlcada = parm;
	}

	public Integer getIdGrupo(){
		return this.idGrupo;
	}
	public void setIdGrupo(Integer parm){
		this.idGrupo = parm;
	}

	public String getTipoLimite(){
		return this.tipoLimite;
	}
	public void setTipoLimite(String parm){
		this.tipoLimite = parm;
	}

	public String getAbrangenciaCentroCusto(){
		return this.abrangenciaCentroCusto;
	}
	public void setAbrangenciaCentroCusto(String parm){
		this.abrangenciaCentroCusto = parm;
	}

	public String getSituacao(){
		return this.situacao;
	}
	public void setSituacao(String parm){
		this.situacao = parm;
	}
    public Double getLimiteItemUsoInterno() {
        return limiteItemUsoInterno;
    }
    public void setLimiteItemUsoInterno(Double limiteItemUsoInterno) {
        this.limiteItemUsoInterno = limiteItemUsoInterno;
    }
    public Double getLimiteMensalUsoInterno() {
        return limiteMensalUsoInterno;
    }
    public void setLimiteMensalUsoInterno(Double limiteMensalUsoInterno) {
        this.limiteMensalUsoInterno = limiteMensalUsoInterno;
    }
    public Double getLimiteItemAtendCliente() {
        return limiteItemAtendCliente;
    }
    public void setLimiteItemAtendCliente(Double limiteItemAtendCliente) {
        this.limiteItemAtendCliente = limiteItemAtendCliente;
    }
    public Double getLimiteMensalAtendCliente() {
        return limiteMensalAtendCliente;
    }
    public void setLimiteMensalAtendCliente(Double limiteMensalAtendCliente) {
        this.limiteMensalAtendCliente = limiteMensalAtendCliente;
    }

}
