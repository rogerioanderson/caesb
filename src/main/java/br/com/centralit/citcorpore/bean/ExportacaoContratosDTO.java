/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class ExportacaoContratosDTO implements IDto {
	private static final long serialVersionUID = -7252057961936714136L;
	private Integer idContrato;
	private String export;
	private Integer[] idGrupos;
	private String exportarUnidades;
	private String exportarAcordoNivelServico;
	private String exportarCatalogoServico;
	
	public Integer getIdContrato() {
		return idContrato;
	}
	public void setIdContrato(Integer idContrato) {
		this.idContrato = idContrato;
	}
	public String getExport() {
		return export;
	}
	public void setExport(String export) {
		this.export = export;
	}
	public Integer[] getIdGrupos() {
		return idGrupos;
	}
	public void setIdGrupos(Integer[] idGrupos) {
		this.idGrupos = idGrupos;
	}
	public String getExportarUnidades() {
		return exportarUnidades;
	}
	public void setExportarUnidades(String exportarUnidades) {
		this.exportarUnidades = exportarUnidades;
	}
	public String getExportarAcordoNivelServico() {
		return exportarAcordoNivelServico;
	}
	public void setExportarAcordoNivelServico(String exportarAcordoNivelServico) {
		this.exportarAcordoNivelServico = exportarAcordoNivelServico;
	}
	public String getExportarCatalogoServico() {
		return exportarCatalogoServico;
	}
	public void setExportarCatalogoServico(String exportarCatalogoServico) {
		this.exportarCatalogoServico = exportarCatalogoServico;
	}

}
