/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Collection;

import br.com.citframework.dto.IDto;

public class LimiteAprovacaoDTO implements IDto {
	private Integer idLimiteAprovacao;
	private String identificacao;
	private String tipoLimitePorValor;
	private String abrangenciaCentroResultado;
	
	private Collection<ValorLimiteAprovacaoDTO> colValores;
	private Integer[] idProcessoNegocio;
	private Integer[] idNivelAutoridade;
	
	protected Double valorMensalUsoInterno = 0.0;
	protected Double valorAnualUsoInterno = 0.0;
	protected Double valorMensalAtendCliente = 0.0;
	protected Double valorAnualAtendCliente = 0.0;
	
	private Collection<ProcessoNegocioDTO> colProcessos;
	private boolean valido;

	public boolean isValido() {
		return valido;
	}
	public void setValido(boolean valido) {
		this.valido = valido;
	}
	public Integer getIdLimiteAprovacao(){
		return this.idLimiteAprovacao;
	}
	public void setIdLimiteAprovacao(Integer parm){
		this.idLimiteAprovacao = parm;
	}

	public String getTipoLimitePorValor(){
		return this.tipoLimitePorValor;
	}
	public void setTipoLimitePorValor(String parm){
		this.tipoLimitePorValor = parm;
	}

	public String getAbrangenciaCentroResultado(){
		return this.abrangenciaCentroResultado;
	}
	public void setAbrangenciaCentroResultado(String parm){
		this.abrangenciaCentroResultado = parm;
	}
	public Collection<ValorLimiteAprovacaoDTO> getColValores() {
		return colValores;
	}
	public void setColValores(Collection<ValorLimiteAprovacaoDTO> colValores) {
		this.colValores = colValores;
	}
	public Integer[] getIdProcessoNegocio() {
		return idProcessoNegocio;
	}
	public void setIdProcessoNegocio(Integer[] idProcessoNegocio) {
		this.idProcessoNegocio = idProcessoNegocio;
	}
	public Integer[] getIdNivelAutoridade() {
		return idNivelAutoridade;
	}
	public void setIdNivelAutoridade(Integer[] idNivelAutoridade) {
		this.idNivelAutoridade = idNivelAutoridade;
	}
	public String getIdentificacao() {
		return identificacao;
	}
	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}
	public Collection<ProcessoNegocioDTO> getColProcessos() {
		return colProcessos;
	}
	public void setColProcessos(Collection<ProcessoNegocioDTO> colProcessos) {
		this.colProcessos = colProcessos;
	}
	public Double getValorMensalUsoInterno() {
		return valorMensalUsoInterno;
	}
	public void setValorMensalUsoInterno(Double valorMensalUsoInterno) {
		this.valorMensalUsoInterno = valorMensalUsoInterno;
	}
	public Double getValorAnualUsoInterno() {
		return valorAnualUsoInterno;
	}
	public void setValorAnualUsoInterno(Double valorAnualUsoInterno) {
		this.valorAnualUsoInterno = valorAnualUsoInterno;
	}
	public Double getValorMensalAtendCliente() {
		return valorMensalAtendCliente;
	}
	public void setValorMensalAtendCliente(Double valorMensalAtendCliente) {
		this.valorMensalAtendCliente = valorMensalAtendCliente;
	}
	public Double getValorAnualAtendCliente() {
		return valorAnualAtendCliente;
	}
	public void setValorAnualAtendCliente(Double valorAnualAtendCliente) {
		this.valorAnualAtendCliente = valorAnualAtendCliente;
	}
	public Double getValorMensal() {
		return valorMensalUsoInterno + valorMensalAtendCliente;
	}
	public Double getValorAnual() {
		return valorAnualUsoInterno + valorAnualAtendCliente;
	}

}
