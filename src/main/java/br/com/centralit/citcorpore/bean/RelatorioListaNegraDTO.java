/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

/**
 * @author mario.haysaki
 *
 */
public class RelatorioListaNegraDTO implements IDto {
	
	private Integer idItemConfiguracao;
	private Integer idGrupoItemConfiguracao;
	private String nomeGrupoItemConfiguracao;
	private Integer idSoftwaresListaNegra;
	private String nomeSoftwaresListaNegra;
	private String descricao;
	private Date dataInicio;
	private Date dataFim;
	private String formatoArquivoRelatorio;
	private String localidade;
	
	public Integer getIdItemConfiguracao() {
		return idItemConfiguracao;
	}
	public void setIdItemConfiguracao(Integer idItemConfiguracao) {
		this.idItemConfiguracao = idItemConfiguracao;
	}
	public Integer getIdGrupoItemConfiguracao() {
		return idGrupoItemConfiguracao;
	}
	public void setIdGrupoItemConfiguracao(Integer idGrupoItemConfiguracao) {
		this.idGrupoItemConfiguracao = idGrupoItemConfiguracao;
	}
	public String getNomeGrupoItemConfiguracao() {
		return nomeGrupoItemConfiguracao;
	}
	public void setNomeGrupoItemConfiguracao(String nomeGrupoItemConfiguracao) {
		this.nomeGrupoItemConfiguracao = nomeGrupoItemConfiguracao;
	}
	public Integer getIdSoftwaresListaNegra() {
		return idSoftwaresListaNegra;
	}
	public void setIdSoftwaresListaNegra(Integer idSoftwaresListaNegra) {
		this.idSoftwaresListaNegra = idSoftwaresListaNegra;
	}
	public String getNomeSoftwaresListaNegra() {
		return nomeSoftwaresListaNegra;
	}
	public void setNomeSoftwaresListaNegra(String nomeSoftwaresListaNegra) {
		this.nomeSoftwaresListaNegra = nomeSoftwaresListaNegra;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getFormatoArquivoRelatorio() {
		return formatoArquivoRelatorio;
	}
	public void setFormatoArquivoRelatorio(String formatoArquivoRelatorio) {
		this.formatoArquivoRelatorio = formatoArquivoRelatorio;
	}
	public String getLocalidade() {
		return localidade;
	}
	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}
	
}
