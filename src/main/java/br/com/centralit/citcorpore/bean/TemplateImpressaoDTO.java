/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class TemplateImpressaoDTO implements IDto {
	private Integer idTemplateImpressao;
	private String nomeTemplate;
	private String htmlCabecalho;
	private String htmlCorpo;
	private String htmlRodape;
	private Integer idTipoTemplateImp;
	private Integer tamCabecalho;
	private Integer tamRodape;

	public Integer getIdTemplateImpressao(){
		return this.idTemplateImpressao;
	}
	public void setIdTemplateImpressao(Integer parm){
		this.idTemplateImpressao = parm;
	}

	public String getNomeTemplate(){
		return this.nomeTemplate;
	}
	public void setNomeTemplate(String parm){
		this.nomeTemplate = parm;
	}

	public String getHtmlCabecalho(){
		return this.htmlCabecalho;
	}
	public void setHtmlCabecalho(String parm){
		this.htmlCabecalho = parm;
	}

	public String getHtmlCorpo(){
		return this.htmlCorpo;
	}
	public void setHtmlCorpo(String parm){
		this.htmlCorpo = parm;
	}

	public String getHtmlRodape(){
		return this.htmlRodape;
	}
	public void setHtmlRodape(String parm){
		this.htmlRodape = parm;
	}

	public Integer getIdTipoTemplateImp(){
		return this.idTipoTemplateImp;
	}
	public void setIdTipoTemplateImp(Integer parm){
		this.idTipoTemplateImp = parm;
	}

	public Integer getTamCabecalho(){
		return this.tamCabecalho;
	}
	public void setTamCabecalho(Integer parm){
		this.tamCabecalho = parm;
	}

	public Integer getTamRodape(){
		return this.tamRodape;
	}
	public void setTamRodape(Integer parm){
		this.tamRodape = parm;
	}

}
