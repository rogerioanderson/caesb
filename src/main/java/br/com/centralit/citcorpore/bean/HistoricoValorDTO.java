/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;


import java.sql.Timestamp;

import br.com.citframework.dto.IDto;

public class HistoricoValorDTO extends ValorDTO implements IDto {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6847076405909756057L;

	private Integer idHistoricoValor;
    private Integer idHistoricoIC;
    private Timestamp dataHoraAlteracao;    
    private Integer idAutorAlteracao;    
	private String baseLine;
	private String nomeCaracteristica;
	
	public Integer getIdHistoricoValor() {
		return idHistoricoValor;
	}

	public void setIdHistoricoValor(Integer idHistoricoValor) {
		this.idHistoricoValor = idHistoricoValor;
	}

	public Timestamp getDataHoraAlteracao() {
		return dataHoraAlteracao;
	}

	public void setDataHoraAlteracao(Timestamp dataHoraAlteracao) {
		this.dataHoraAlteracao = dataHoraAlteracao;
	}

	public Integer getIdAutorAlteracao() {
		return idAutorAlteracao;
	}

	public void setIdAutorAlteracao(Integer idAutorAlteracao) {
		this.idAutorAlteracao = idAutorAlteracao;
	}

	public String getBaseLine() {
		return baseLine;
	}

	public void setBaseLine(String baseLine) {
		this.baseLine = baseLine;
	}

	public Integer getIdHistoricoIC() {
		return idHistoricoIC;
	}

	public void setIdHistoricoIC(Integer idHistoricoIC) {
		this.idHistoricoIC = idHistoricoIC;
	}
	
	public String getNomeCaracteristica() {
		return nomeCaracteristica;
	}

	public void setNomeCaracteristica(String nomeCaracteristica) {
		this.nomeCaracteristica = nomeCaracteristica;
	}

    
	
}
