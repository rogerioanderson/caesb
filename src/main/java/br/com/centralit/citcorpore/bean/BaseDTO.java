/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * 
 */
package br.com.centralit.citcorpore.bean;

import java.util.Collection;

import br.com.citframework.dto.IDto;

/**
 * @author flavio.santana
 * DTO de referencia de base de conhecimento para os parametros do sistema
 */
public class BaseDTO implements IDto {

	private static final long serialVersionUID = -3593079788503253157L;

	private String idAtributoBase;
	private String atributoBase;
	private String valorAtributoBase;
	private Collection<BaseDTO> listBaseDTO;
	
	public String getIdAtributoBase() {
		return idAtributoBase;
	}
	public String getAtributoBase() {
		return atributoBase;
	}
	public String getValorAtributoBase() {
		return valorAtributoBase;
	}
	public Collection<BaseDTO> getListBaseDTO() {
		return listBaseDTO;
	}
	public void setIdAtributoBase(String idAtributoBase) {
		this.idAtributoBase = idAtributoBase;
	}
	public void setAtributoBase(String atributoBase) {
		this.atributoBase = atributoBase;
	}
	public void setValorAtributoBase(String valorAtributoBase) {
		this.valorAtributoBase = valorAtributoBase;
	}
	public void setListBaseDTO(Collection<BaseDTO> listBaseDTO) {
		this.listBaseDTO = listBaseDTO;
	}	
}
