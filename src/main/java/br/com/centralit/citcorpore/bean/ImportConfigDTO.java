/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Collection;
import java.util.Date;

import br.com.citframework.dto.IDto;

@SuppressWarnings("rawtypes")
public class ImportConfigDTO implements IDto {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9160239194104870607L;
	
	private Integer idImportConfig;
	private Integer idImportarDados;
	private String tipo;
	private Integer idExternalConnection;
	private String tabelaOrigem;
	private String tabelaDestino;
	private String filtroOrigem;
	private String nome;
	private Date dataFim;
	
	private Collection colDadosCampos;

	
	public Integer getIdImportarDados() {
		return idImportarDados;
	}
	public void setIdImportarDados(Integer idImportarDados) {
		this.idImportarDados = idImportarDados;
	}
	public Integer getIdImportConfig(){
		return this.idImportConfig;
	}
	public void setIdImportConfig(Integer parm){
		this.idImportConfig = parm;
	}

	public String getTipo(){
		return this.tipo;
	}
	public void setTipo(String parm){
		this.tipo = parm;
	}

	public Integer getIdExternalConnection(){
		return this.idExternalConnection;
	}
	public void setIdExternalConnection(Integer parm){
		this.idExternalConnection = parm;
	}

	public String getTabelaOrigem(){
		return this.tabelaOrigem;
	}
	public void setTabelaOrigem(String parm){
		this.tabelaOrigem = parm;
	}

	public String getTabelaDestino(){
		return this.tabelaDestino;
	}
	public void setTabelaDestino(String parm){
		this.tabelaDestino = parm;
	}

	public String getFiltroOrigem(){
		return this.filtroOrigem;
	}
	public void setFiltroOrigem(String parm){
		this.filtroOrigem = parm;
	}
	public Collection getColDadosCampos() {
		return colDadosCampos;
	}
	public void setColDadosCampos(Collection colDadosCampos) {
		this.colDadosCampos = colDadosCampos;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

}
