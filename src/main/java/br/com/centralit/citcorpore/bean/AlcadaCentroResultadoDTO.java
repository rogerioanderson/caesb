/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;
import br.com.citframework.util.UtilDatas;

public class AlcadaCentroResultadoDTO implements IDto {
	
	private static final long serialVersionUID = 4182540164619994646L;
	
	private Integer idAlcadaCentroResultado;
	private Integer idCentroResultado;
	private Integer idEmpregado;
	private Integer idAlcada;
	private Date dataInicio;
	private Date dataFim;
	private String nomeEmpregado;
	private String nomeCentroResultado;
	private String nomeAlcada;
	
	private Integer sequencia;
	private String dataInicioStr;
	private String dataFimStr;
		
	public Integer getIdAlcadaCentroResultado() {
		return idAlcadaCentroResultado;
	}
	
	public void setIdAlcadaCentroResultado(Integer idAlcadaCentroResultado) {
		this.idAlcadaCentroResultado = idAlcadaCentroResultado;
	}
	
	public Integer getIdCentroResultado() {
		return idCentroResultado;
	}
	
	public void setIdCentroResultado(Integer idCentroResultado) {
		this.idCentroResultado = idCentroResultado;
	}
	
	public Integer getIdEmpregado() {
		return idEmpregado;
	}
	
	public void setIdEmpregado(Integer idEmpregado) {
		this.idEmpregado = idEmpregado;
	}
	
	public Integer getIdAlcada() {
		return idAlcada;
	}
	
	public void setIdAlcada(Integer idAlcada) {
		this.idAlcada = idAlcada;
	}
	
	public Date getDataInicio() {
		return dataInicio;
	}
	
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
		if (dataInicio != null)
		    this.dataInicioStr = UtilDatas.dateToSTR(dataInicio);
	}
	
	public Date getDataFim() {
		return dataFim;
	}
	
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	    if (dataFim != null)
	        this.dataFimStr = UtilDatas.dateToSTR(dataFim);
	}
	
	public String getNomeEmpregado() {
		return this.nomeEmpregado;
	}
	
	public void setNomeEmpregado(String nomeEmpregado) {
		this.nomeEmpregado = nomeEmpregado;
	}
	
	public String getNomeCentroResultado() {
		return this.nomeCentroResultado;
	}
	
	public void setNomeCentroResultado(String nomeCentroResultado) {
		this.nomeCentroResultado = nomeCentroResultado;
	}
	
	public String getNomeAlcada() {
		return this.nomeAlcada;
	}
	
	public void setNomeAlcada(String nomeAlcada) {
		this.nomeAlcada = nomeAlcada;
	}

    public String getDataInicioStr() {
        return dataInicioStr;
    }

    public void setDataInicioStr(String dataInicioStr) {
        this.dataInicioStr = dataInicioStr;
    }

    public String getDataFimStr() {
        return dataFimStr;
    }

    public void setDataFimStr(String dataFimStr) {
        this.dataFimStr = dataFimStr;
    }

    public Integer getSequencia() {
        return sequencia;
    }

    public void setSequencia(Integer sequencia) {
        this.sequencia = sequencia;
    }
}
