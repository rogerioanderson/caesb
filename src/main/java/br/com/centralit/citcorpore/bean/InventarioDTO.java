/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.List;

import br.com.citframework.dto.IDto;

/**
 * @author Maycon.Fernandes
 * 
 */
@SuppressWarnings("serial")
public class InventarioDTO implements IDto {

    private Integer idInventario;
    private String ip;
    private String mask;
    private String mac;
    
    private Integer idContrato;
    @SuppressWarnings("rawtypes")
    private List<NetMapDTO> ipInvetariar;
    private java.sql.Date date;

    /**
     * @return valor do atributo ipInvetariar.
     */
    @SuppressWarnings("rawtypes")
    public List<NetMapDTO> getIpInvetariar() {
	return ipInvetariar;
    }

    /**
     * Define valor do atributo ipInvetariar.
     * 
     * @param ipInvetariar
     */
    @SuppressWarnings("rawtypes")
    public void setIpInvetariar(List<NetMapDTO> ipInvetariar) {
	this.ipInvetariar = ipInvetariar;
    }

    /**
     * @return valor do atributo idInventario.
     */
    public Integer getIdInventario() {
	return idInventario;
    }

    /**
     * Define valor do atributo idInventario.
     * 
     * @param idInventario
     */
    public void setIdInventario(Integer idInventario) {
	this.idInventario = idInventario;
    }

    /**
     * @return valor do atributo ip.
     */
    public String getIp() {
	return ip;
    }

    /**
     * Define valor do atributo ip.
     * 
     * @param ip
     */
    public void setIp(String ip) {
	this.ip = ip;
    }

    /**
     * @return valor do atributo mask.
     */
    public String getMask() {
	return mask;
    }

    /**
     * Define valor do atributo mask.
     * 
     * @param mask
     */
    public void setMask(String mask) {
	this.mask = mask;
    }

    /**
     * @return valor do atributo mac.
     */
    public String getMac() {
	return mac;
    }

    /**
     * Define valor do atributo mac.
     * 
     * @param mac
     */
    public void setMac(String mac) {
	this.mac = mac;
    }

    /**
     * @return valor do atributo date.
     */
    public java.sql.Date getDate() {
	return date;
    }

    /**
     * Define valor do atributo date.
     * 
     * @param date
     */
    public void setDate(java.sql.Date date) {
	this.date = date;
    }

	public Integer getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Integer idContrato) {
		this.idContrato = idContrato;
	}

}
