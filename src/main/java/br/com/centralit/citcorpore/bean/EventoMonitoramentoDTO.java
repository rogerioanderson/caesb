/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * 
 */
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

/**
 * @author Vadoilo Damasceno
 * 
 */
public class EventoMonitoramentoDTO implements IDto {

	private static final long serialVersionUID = 594136415602018544L;

	private Integer idEventoMonitoramento;

	private String nomeEvento;

	private String detalhamento;

	private String criadoPor;

	private String modificadoPor;

	private Date dataCriacao;

	private Date ultimaModificacao;
	
	public static String staticCriadoPor;
	public static Date staticDataCriacao;
	
	/**
	 * @return the idEventoMonitoramento
	 */
	public Integer getIdEventoMonitoramento() {
		return idEventoMonitoramento;
	}

	/**
	 * @param idEventoMonitoramento
	 *            the idEventoMonitoramento to set
	 */
	public void setIdEventoMonitoramento(Integer idEventoMonitoramento) {
		this.idEventoMonitoramento = idEventoMonitoramento;
	}

	/**
	 * @return the nomeEvento
	 */
	public String getNomeEvento() {
		return nomeEvento;
	}

	/**
	 * @param nomeEvento
	 *            the nomeEvento to set
	 */
	public void setNomeEvento(String nomeEvento) {
		this.nomeEvento = nomeEvento;
	}

	/**
	 * @return the detalhamento
	 */
	public String getDetalhamento() {
		return detalhamento;
	}

	/**
	 * @param detalhamento
	 *            the detalhamento to set
	 */
	public void setDetalhamento(String detalhamento) {
		this.detalhamento = detalhamento;
	}

	/**
	 * @return the criadoPor
	 */
	public String getCriadoPor() {
		return criadoPor;
	}

	/**
	 * @param criadoPor
	 *            the criadoPor to set
	 */
	public void setCriadoPor(String criadoPor) {
		this.criadoPor = criadoPor;
	}

	/**
	 * @return the modificadoPor
	 */
	public String getModificadoPor() {
		return modificadoPor;
	}

	/**
	 * @param modificadoPor
	 *            the modificadoPor to set
	 */
	public void setModificadoPor(String modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	/**
	 * @return the dataCriacao
	 */
	public Date getDataCriacao() {
		return dataCriacao;
	}

	/**
	 * @param dataCriacao
	 *            the dataCriacao to set
	 */
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	/**
	 * @return the ultimaModificacao
	 */
	public Date getUltimaModificacao() {
		return ultimaModificacao;
	}

	/**
	 * @param ultimaModificacao
	 *            the ultimaModificacao to set
	 */
	public void setUltimaModificacao(Date ultimaModificacao) {
		this.ultimaModificacao = ultimaModificacao;
	}

}
