/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Timestamp;

/**
 * @author euler.ramos
 * Classe utilizada para transferir os dados da Requisi��o de Mudan�a para o iReport
 */
public class ReqMudancaJasperDTO {
	
	private Integer idRequisicaoMudanca;
	private String titulo;
	private Timestamp dataHoraInicio;
	private Timestamp dataHoraTermino;
	private Timestamp dataHoraConclusao;
	private String nomeSolicitante;
	private String descrSituacao;
	private String nomeProprietario;
	private String tipo;
	private String nomeGrupoAtual;
	private String descricao;
	private String fechamento;
	private String razaoMudanca;
	private String analiseImpacto;
	private String risco;
	private Object listaVotacoesProposta; //aprovacaoproposta
	private Object listaVotacoesReqMudanca; //aprovacaomudanca
	private Object listaICs;
	private Object listaServicos;
	private Object listaProblemas;
	private Object listaSolicitacoes;
	private Object listaRiscos;
	private Object listaLiberacoes;
	private Object listaPapeis;
	private Object listaCheckList;
	private Object listaAnexosPlReversao;
	private Object listaGrupos;
	
	public Integer getIdRequisicaoMudanca() {
		return idRequisicaoMudanca;
	}
	public void setIdRequisicaoMudanca(Integer idRequisicaoMudanca) {
		this.idRequisicaoMudanca = idRequisicaoMudanca;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Timestamp getDataHoraInicio() {
		return dataHoraInicio;
	}
	public void setDataHoraInicio(Timestamp dataHoraInicio) {
		this.dataHoraInicio = dataHoraInicio;
	}
	public Timestamp getDataHoraTermino() {
		return dataHoraTermino;
	}
	public void setDataHoraTermino(Timestamp dataHoraTermino) {
		this.dataHoraTermino = dataHoraTermino;
	}
	public Timestamp getDataHoraConclusao() {
		return dataHoraConclusao;
	}
	public void setDataHoraConclusao(Timestamp dataHoraConclusao) {
		this.dataHoraConclusao = dataHoraConclusao;
	}
	public String getNomeSolicitante() {
		return nomeSolicitante;
	}
	public void setNomeSolicitante(String nomeSolicitante) {
		this.nomeSolicitante = nomeSolicitante;
	}
	public String getDescrSituacao() {
		return descrSituacao;
	}
	public void setDescrSituacao(String descrSituacao) {
		this.descrSituacao = descrSituacao;
	}
	public String getNomeProprietario() {
		return nomeProprietario;
	}
	public void setNomeProprietario(String nomeProprietario) {
		this.nomeProprietario = nomeProprietario;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getNomeGrupoAtual() {
		return nomeGrupoAtual;
	}
	public void setNomeGrupoAtual(String nomeGrupoAtual) {
		this.nomeGrupoAtual = nomeGrupoAtual;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getFechamento() {
		return fechamento;
	}
	public void setFechamento(String fechamento) {
		this.fechamento = fechamento;
	}
	public String getRazaoMudanca() {
		return razaoMudanca;
	}
	public void setRazaoMudanca(String razaoMudanca) {
		this.razaoMudanca = razaoMudanca;
	}
	public Object getListaVotacoesProposta() {
		return listaVotacoesProposta;
	}
	public void setListaVotacoesProposta(Object listaVotacoesProposta) {
		this.listaVotacoesProposta = listaVotacoesProposta;
	}
	public Object getListaVotacoesReqMudanca() {
		return listaVotacoesReqMudanca;
	}
	public void setListaVotacoesReqMudanca(Object listaVotacoesReqMudanca) {
		this.listaVotacoesReqMudanca = listaVotacoesReqMudanca;
	}
	public Object getListaICs() {
		return listaICs;
	}
	public void setListaICs(Object listaICs) {
		this.listaICs = listaICs;
	}
	public Object getListaServicos() {
		return listaServicos;
	}
	public void setListaServicos(Object listaServicos) {
		this.listaServicos = listaServicos;
	}
	public Object getListaProblemas() {
		return listaProblemas;
	}
	public void setListaProblemas(Object listaProblemas) {
		this.listaProblemas = listaProblemas;
	}
	public Object getListaSolicitacoes() {
		return listaSolicitacoes;
	}
	public void setListaSolicitacoes(Object listaSolicitacoes) {
		this.listaSolicitacoes = listaSolicitacoes;
	}
	public Object getListaRiscos() {
		return listaRiscos;
	}
	public void setListaRiscos(Object listaRiscos) {
		this.listaRiscos = listaRiscos;
	}
	public Object getListaLiberacoes() {
		return listaLiberacoes;
	}
	public void setListaLiberacoes(Object listaLiberacoes) {
		this.listaLiberacoes = listaLiberacoes;
	}
	public Object getListaPapeis() {
		return listaPapeis;
	}
	public void setListaPapeis(Object listaPapeis) {
		this.listaPapeis = listaPapeis;
	}
	public Object getListaCheckList() {
		return listaCheckList;
	}
	public void setListaCheckList(Object listaCheckList) {
		this.listaCheckList = listaCheckList;
	}
	public Object getListaAnexosPlReversao() {
		return listaAnexosPlReversao;
	}
	public void setListaAnexosPlReversao(Object listaAnexosPlReversao) {
		this.listaAnexosPlReversao = listaAnexosPlReversao;
	}
	public Object getListaGrupos() {
		return listaGrupos;
	}
	public void setListaGrupos(Object listaGrupos) {
		this.listaGrupos = listaGrupos;
	}
	public String getAnaliseImpacto() {
		return analiseImpacto;
	}
	public void setAnaliseImpacto(String analiseImpacto) {
		this.analiseImpacto = analiseImpacto;
	}
	public String getRisco() {
		return risco;
	}
	public void setRisco(String risco) {
		this.risco = risco;
	}
	
}
