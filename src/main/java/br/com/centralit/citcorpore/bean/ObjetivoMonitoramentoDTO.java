/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

@SuppressWarnings("serial")
public class ObjetivoMonitoramentoDTO implements IDto {
	private Integer idObjetivoMonitoramento;
	private Integer idObjetivoPlanoMelhoria;
	private String tituloMonitoramento;
	private String fatorCriticoSucesso;
	private String kpi;
	private String metrica;
	private String medicao;
	private String relatorios;
	private String responsavel;
	private String criadoPor;
	private String modificadoPor;
	private java.sql.Date dataCriacao;
	private java.sql.Date ultModificacao;

	private String tituloObjetivoPlanoMelhoria;
	
	private Integer sequecialObjetivoMonitoramento;
	
	

	public Integer getIdObjetivoMonitoramento() {
		return this.idObjetivoMonitoramento;
	}

	public void setIdObjetivoMonitoramento(Integer parm) {
		this.idObjetivoMonitoramento = parm;
	}

	public Integer getIdObjetivoPlanoMelhoria() {
		return this.idObjetivoPlanoMelhoria;
	}

	public void setIdObjetivoPlanoMelhoria(Integer parm) {
		this.idObjetivoPlanoMelhoria = parm;
	}

	public String getTituloMonitoramento() {
		return this.tituloMonitoramento;
	}

	public void setTituloMonitoramento(String parm) {
		this.tituloMonitoramento = parm;
	}

	public String getFatorCriticoSucesso() {
		return this.fatorCriticoSucesso;
	}

	public void setFatorCriticoSucesso(String parm) {
		this.fatorCriticoSucesso = parm;
	}

	public String getKpi() {
		return this.kpi;
	}

	public void setKpi(String parm) {
		this.kpi = parm;
	}

	public String getMetrica() {
		return this.metrica;
	}

	public void setMetrica(String parm) {
		this.metrica = parm;
	}

	public String getMedicao() {
		return this.medicao;
	}

	public void setMedicao(String parm) {
		this.medicao = parm;
	}

	public String getRelatorios() {
		return this.relatorios;
	}

	public void setRelatorios(String parm) {
		this.relatorios = parm;
	}

	public String getResponsavel() {
		return this.responsavel;
	}

	public void setResponsavel(String parm) {
		this.responsavel = parm;
	}

	public String getCriadoPor() {
		return this.criadoPor;
	}

	public void setCriadoPor(String parm) {
		this.criadoPor = parm;
	}

	public String getModificadoPor() {
		return this.modificadoPor;
	}

	public void setModificadoPor(String parm) {
		this.modificadoPor = parm;
	}

	public java.sql.Date getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(java.sql.Date parm) {
		this.dataCriacao = parm;
	}

	public java.sql.Date getUltModificacao() {
		return this.ultModificacao;
	}

	public void setUltModificacao(java.sql.Date parm) {
		this.ultModificacao = parm;
	}

	/**
	 * @return the tituloObjetivoPlanoMelhoria
	 */
	public String getTituloObjetivoPlanoMelhoria() {
		return tituloObjetivoPlanoMelhoria;
	}

	/**
	 * @param tituloObjetivoPlanoMelhoria
	 *            the tituloObjetivoPlanoMelhoria to set
	 */
	public void setTituloObjetivoPlanoMelhoria(String tituloObjetivoPlanoMelhoria) {
		this.tituloObjetivoPlanoMelhoria = tituloObjetivoPlanoMelhoria;
	}

	/**
	 * @return the sequecialObjetivoMonitoramento
	 */
	public Integer getSequecialObjetivoMonitoramento() {
		return sequecialObjetivoMonitoramento;
	}

	/**
	 * @param sequecialObjetivoMonitoramento the sequecialObjetivoMonitoramento to set
	 */
	public void setSequecialObjetivoMonitoramento(Integer sequecialObjetivoMonitoramento) {
		this.sequecialObjetivoMonitoramento = sequecialObjetivoMonitoramento;
	}

}
