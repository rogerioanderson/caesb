/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.centralit.citajax.util.JavaScriptUtil;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilFormatacao;

public class TimeSheetDTO implements IDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8184473698460030815L;
	private Integer idTimeSheet;
	private Integer idDemanda;
	private Integer idEmpregado;
	private Integer idCliente;
	private Integer idProjeto;
	private Double qtdeHoras;
	private Date data;
	private Double custoPorHora;
	private String detalhamento;
	
	private String nomeCliente;
	private String nomeProjeto;
	private String nomeEmpregado;
	
	private String detalhamentoDemanda;
	public String getDataStr() {
		if (this.data == null) return "";
		return UtilDatas.dateToSTR(data);
	}	
	public String getDataStrDet() {
		if (this.data == null) return "";
		try {
			return UtilDatas.dateToSTR(data) + " (" + UtilDatas.getDiaSemana(data) + ")";
		} catch (LogicException e) {
			e.printStackTrace();
			return "";
		}
	}		
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Integer getIdDemanda() {
		return idDemanda;
	}
	public void setIdDemanda(Integer idDemanda) {
		this.idDemanda = idDemanda;
	}
	public Integer getIdEmpregado() {
		return idEmpregado;
	}
	public void setIdEmpregado(Integer idEmpregado) {
		this.idEmpregado = idEmpregado;
	}
	public Integer getIdProjeto() {
		return idProjeto;
	}
	public void setIdProjeto(Integer idProjeto) {
		this.idProjeto = idProjeto;
	}
	public Integer getIdTimeSheet() {
		return idTimeSheet;
	}
	public void setIdTimeSheet(Integer idTimeSheet) {
		this.idTimeSheet = idTimeSheet;
	}
	public Integer getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	public Double getCustoPorHora() {
		return custoPorHora;
	}
	public void setCustoPorHora(Double custoPorHora) {
		this.custoPorHora = custoPorHora;
	}
	public String getQtdeHorasStr() {
		return UtilFormatacao.formatDouble(qtdeHoras,2);
	}	
	public String getQtdeHorasStr2() {
		if (qtdeHoras == 0){
			return "--";
		}
		return UtilFormatacao.formatDouble(qtdeHoras,2);
	}	
	public Double getQtdeHoras() {
		return qtdeHoras;
	}
	public void setQtdeHoras(Double qtdeHoras) {
		this.qtdeHoras = qtdeHoras;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public String getNomeProjeto() {
		return nomeProjeto;
	}
	public void setNomeProjeto(String nomeProjeto) {
		this.nomeProjeto = nomeProjeto;
	}
	public String getDetalhamentoConv() {
		return JavaScriptUtil.escapeJavaScript(detalhamento);
	}	
	public String getDetalhamento() {
		return detalhamento;
	}
	public void setDetalhamento(String detalhamento) {
		this.detalhamento = detalhamento;
	}
	public String getNomeEmpregado() {
		return nomeEmpregado;
	}
	public void setNomeEmpregado(String nomeEmpregado) {
		this.nomeEmpregado = nomeEmpregado;
	}
	public String getDetalhamentoDemanda() {
		return detalhamentoDemanda;
	}
	public void setDetalhamentoDemanda(String detalhamentoDemanda) {
		this.detalhamentoDemanda = detalhamentoDemanda;
	}
}
