/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.ArrayList;

import br.com.citframework.dto.IDto;

/**
 * @author breno.guimaraes
 * 
 */
public class ClienteEmailCentralServicoDTO implements IDto {

    private static final long serialVersionUID = 4004251311921076618L;

    private String host;
    private String username;
    private String password;
    private String provider;
    private Integer port;
    
    private Integer idContrato;
    private String emailOrigem;
    private String emailMessageId;
    
    private ArrayList<ClienteEmailCentralServicoMessagesDTO> emailMessages;
    private String resultMessage;
    private boolean resultSucess;

    public String getHost() {
    	return host;
    }

    public void setHost(String host) {
    	this.host = host;
    }

    public String getUsername() {
    	return username;
    }

    public void setUsername(String username) {
    	this.username = username;
    }

    public String getPassword() {
    	return password;
    }

    public void setPassword(String password) {
    	this.password = password;
    }

    public String getProvider() {
    	return provider;
    }

    public void setProvider(String provider) {
    	this.provider = provider;
    }

    public Integer getPort() {
    	return port;
    }

    public void setPort(Integer port) {
    	this.port = port;
    }

	public Integer getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Integer idContrato) {
		this.idContrato = idContrato;
	}

	public String getEmailMessageId() {
		return emailMessageId;
	}

	public void setEmailMessageId(String emailMessageId) {
		this.emailMessageId = emailMessageId;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public boolean isResultSucess() {
		return resultSucess;
	}

	public void setResultSucess(boolean resultSucess) {
		this.resultSucess = resultSucess;
	}

	public ArrayList<ClienteEmailCentralServicoMessagesDTO> getEmailMessages() {
		return emailMessages;
	}

	public void setEmailMessages(
			ArrayList<ClienteEmailCentralServicoMessagesDTO> emailMessages) {
		this.emailMessages = emailMessages;
	}

	public String getEmailOrigem() {
		return emailOrigem;
	}

	public void setEmailOrigem(String emailOrigem) {
		this.emailOrigem = emailOrigem;
	}

}
