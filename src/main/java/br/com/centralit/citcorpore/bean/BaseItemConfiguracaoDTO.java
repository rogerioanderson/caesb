/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart.
 */
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

/**
 * @author valdoilo.damasceno
 * 
 */
public class BaseItemConfiguracaoDTO implements IDto {

    private static final long serialVersionUID = 7291574008258558256L;

    private Integer id;

    private String nome;

    private Integer idTipoItemConfiguracao;

    private String executavel;

    private Date dataInicio;

    private Date dataFim;

    private String  tipoexecucao;
    
    private TipoItemConfiguracaoDTO tipoItemConfiguracao;
    
    private String comando;
    
    private Integer idBaseItemConfiguracaoPai;
    

    /**
     * @return valor do atributo comando
     */
    public String getComando() {
        return comando;
    }

    /**
     * Define valor do atributo comando
     * 
     * @param comando 
     */
    public void setComando(String comando) {
        this.comando = comando;
    }

    /**
     * 
     * @return valor do atributo id.
     */
    public Integer getId() {
	return id;
    }

    /**
     * Define valor do atributo id.
     * 
     * @param id
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return valor do atributo idTipoItemConfiguracao.
     */
    public Integer getIdTipoItemConfiguracao() {
	return idTipoItemConfiguracao;
    }

    /**
     * Define valor do atributo idTipoItemConfiguracao.
     * 
     * @param idTipoItemConfiguracao
     */
    public void setIdTipoItemConfiguracao(Integer idTipoItemConfiguracao) {
	this.idTipoItemConfiguracao = idTipoItemConfiguracao;
    }

    /**
     * @return valor do atributo nome.
     */
    public String getNome() {
	return nome;
    }

    /**
     * Define valor do atributo nome.
     * 
     * @param nome
     */
    public void setNome(String nome) {
	this.nome = nome;
    }

    /**
     * @return valor do atributo executavel.
     */
    public String getExecutavel() {
	return executavel;
    }

    /**
     * Define valor do atributo executavel.
     * 
     * @param executavel
     */
    public void setExecutavel(String executavel) {
	this.executavel = executavel;
    }

    /**
     * @return valor do atributo dataInicio.
     */
    public Date getDataInicio() {
	return dataInicio;
    }

    /**
     * Define valor do atributo dataInicio.
     * 
     * @param dataInicio
     */
    public void setDataInicio(Date dataInicio) {
	this.dataInicio = dataInicio;
    }

    /**
     * @return valor do atributo dataFim.
     */
    public Date getDataFim() {
	return dataFim;
    }

    /**
     * Define valor do atributo dataFim.
     * 
     * @param dataFim
     */
    public void setDataFim(Date dataFim) {
	this.dataFim = dataFim;
    }

    /**
     * @return valor do atributo tipoItemConfiguracao.
     */
    public TipoItemConfiguracaoDTO getTipoItemConfiguracao() {
	return tipoItemConfiguracao;
    }

    /**
     * Define valor do atributo tipoItemConfiguracao.
     * 
     * @param tipoItemConfiguracao
     */
    public void setTipoItemConfiguracao(TipoItemConfiguracaoDTO tipoItemConfiguracao) {
	this.tipoItemConfiguracao = tipoItemConfiguracao;
    }

	/**
	 * @return the tipoexecucao
	 */
	public String getTipoexecucao() {
		return tipoexecucao;
	}

	/**
	 * @param tipoexecucao the tipoexecucao to set
	 */
	public void setTipoexecucao(String tipoexecucao) {
		this.tipoexecucao = tipoexecucao;
	}

	/**
	 * @return the idBaseItemConfiguracaoPai
	 */
	public Integer getIdBaseItemConfiguracaoPai() {
		return idBaseItemConfiguracaoPai;
	}

	/**
	 * @param idBaseItemConfiguracaoPai the idBaseItemConfiguracaoPai to set
	 */
	public void setIdBaseItemConfiguracaoPai(Integer idBaseItemConfiguracaoPai) {
		this.idBaseItemConfiguracaoPai = idBaseItemConfiguracaoPai;
	}
}
