/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

public class JustificacaoFalhasDTO implements IDto {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    // Vari�veis do banco
    private Integer idHistoricoTentativa;
    private Integer idJustificacaoFalha;
    private Integer idItemConfiguracao;
    private Integer idBaseItemConfiguracao;
    private Integer idEvento;
    private Integer idEmpregado;
    private String descricao;
    private Date data;
    private String hora;

    // Vari�veis de controle espec�fico da aplica��o
    private String listaItensSerializado;
    private Integer idGrupo;
    private Integer idUnidade;
    private Date dataInicial;
    private Date dataFinal;

    public Integer getIdJustificacaoFalha() {
	return idJustificacaoFalha;
    }

    public void setIdJustificacaoFalha(Integer idJustificacaoFalha) {
	this.idJustificacaoFalha = idJustificacaoFalha;
    }

    public Integer getIdItemConfiguracao() {
	return idItemConfiguracao;
    }

    public void setIdItemConfiguracao(Integer idItemConfiguracao) {
	this.idItemConfiguracao = idItemConfiguracao;
    }

    public Integer getIdBaseItemConfiguracao() {
	return idBaseItemConfiguracao;
    }

    public void setIdBaseItemConfiguracao(Integer idBaseItemConfiguracao) {
	this.idBaseItemConfiguracao = idBaseItemConfiguracao;
    }

    public Integer getIdEvento() {
	return idEvento;
    }

    public void setIdEvento(Integer idEvento) {
	this.idEvento = idEvento;
    }

    public Integer getIdEmpregado() {
	return idEmpregado;
    }

    public void setIdEmpregado(Integer idEmpregado) {
	this.idEmpregado = idEmpregado;
    }

    public String getDescricao() {
	return descricao;
    }

    public void setDescricao(String descricao) {
	this.descricao = descricao;
    }

    public Date getData() {
	return data;
    }

    public void setData(Date data) {
	this.data = data;
    }

    public String getHora() {
	return hora;
    }

    public void setHora(String hora) {
	this.hora = hora;
    }

    public String getListaItensSerializado() {
	return listaItensSerializado;
    }

    public void setListaItensSerializado(String listaItensSerializado) {
	this.listaItensSerializado = listaItensSerializado;
    }

    public Integer getIdGrupo() {
	return idGrupo;
    }

    public void setIdGrupo(Integer idGrupo) {
	this.idGrupo = idGrupo;
    }

    public Integer getIdUnidade() {
	return idUnidade;
    }

    public void setIdUnidade(Integer idUnidade) {
	this.idUnidade = idUnidade;
    }

    public Date getDataInicial() {
	return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
	this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
	return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
	this.dataFinal = dataFinal;
    }

    public Integer getIdHistoricoTentativa() {
	return idHistoricoTentativa;
    }

    public void setIdHistoricoTentativa(Integer idHistoricoTentativa) {
	this.idHistoricoTentativa = idHistoricoTentativa;
    }

}
