/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * 
 */
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

/**
 * @author breno.guimaraes
 * 
 */
public class IncidentesRelacionadosDTO implements IDto {

    private static final long serialVersionUID = 808193377991557947L;
    
    private Integer idIncidentesRelacionados;
    private Integer idIncidente;
    private Integer idIncidenteRelacionado;
    
    private Integer idSolicitacaoServico;
    private Integer idSolicitacaoIncRel;
    
    public Integer getIdSolicitacaoServico() {
        return idSolicitacaoServico;
    }

    public void setIdSolicitacaoServico(Integer idSolicitacaoServico) {
        this.idSolicitacaoServico = idSolicitacaoServico;
    }

    private Integer[] filhasAdicionar;

    public Integer[] getFilhasAdicionar() {
	return filhasAdicionar;
    }

    public void setFilhasAdicionar(Integer[] filhasAdicionar) {
	this.filhasAdicionar = filhasAdicionar;
    }

    public Integer getIdSolicitacaoIncRel() {
        return idSolicitacaoIncRel;
    }

    public void setIdSolicitacaoIncRel(Integer idSolicitacaoIncRel) {
        this.idSolicitacaoIncRel = idSolicitacaoIncRel;
    }

	public Integer getIdIncidentesRelacionados() {
		return idIncidentesRelacionados;
	}

	public void setIdIncidentesRelacionados(Integer idIncidentesRelacionados) {
		this.idIncidentesRelacionados = idIncidentesRelacionados;
	}

	public Integer getIdIncidente() {
		return idIncidente;
	}

	public void setIdIncidente(Integer idIncidente) {
		this.idIncidente = idIncidente;
	}

	public Integer getIdIncidenteRelacionado() {
		return idIncidenteRelacionado;
	}

	public void setIdIncidenteRelacionado(Integer idIncidenteRelacionado) {
		this.idIncidenteRelacionado = idIncidenteRelacionado;
	}

}
