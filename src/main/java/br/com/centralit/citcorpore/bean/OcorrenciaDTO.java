/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.centralit.citajax.util.CitAjaxWebUtil;
import br.com.centralit.citajax.util.JavaScriptUtil;
import br.com.citframework.dto.IDto;
import br.com.citframework.util.UtilDatas;

public class OcorrenciaDTO implements IDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -682996086218596048L;
	private Integer idOcorrencia;
	private Integer idDemanda;
	private String ocorrencia;
	private String tipoOcorrencia;
	private String respostaOcorrencia;
	private Date data;
	private Integer idEmpregado;
	
	//campo estrangeiro da tabela grupo
	private String sigla;
	
	private String nomeEmpregado;
	public String getDataStr() {
		return UtilDatas.dateToSTR(data);
	}	
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Integer getIdDemanda() {
		return idDemanda;
	}
	public void setIdDemanda(Integer idDemanda) {
		this.idDemanda = idDemanda;
	}
	public Integer getIdOcorrencia() {
		return idOcorrencia;
	}
	public void setIdOcorrencia(Integer idOcorrencia) {
		this.idOcorrencia = idOcorrencia;
	}
	public String getOcorrenciaConv() {
		if (ocorrencia == null) return "";
		String ret = JavaScriptUtil.escapeJavaScript(CitAjaxWebUtil.codificaEnter(ocorrencia));
		return ret;		
	}	
	public String getOcorrencia() {
		return ocorrencia;
	}
	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}
	public String getRespostaOcorrenciaConv() {
		if (respostaOcorrencia == null) return "";
		String ret = JavaScriptUtil.escapeJavaScript(CitAjaxWebUtil.codificaEnter(respostaOcorrencia));
		return ret;
	}	
	public String getRespostaOcorrencia() {
		return respostaOcorrencia;		
	}
	public void setRespostaOcorrencia(String respostaOcorrencia) {
		this.respostaOcorrencia = respostaOcorrencia;
	}
	public String getTipoOcorrenciaStr() {
		if (tipoOcorrencia == null) return "";
		if (tipoOcorrencia.equalsIgnoreCase("D") ){
			return "D�vida";
		}
		if (tipoOcorrencia.equalsIgnoreCase("O") ){
			return "Diversos";
		}		
		return tipoOcorrencia;
	}	
	public String getTipoOcorrencia() {
		return tipoOcorrencia;
	}
	public void setTipoOcorrencia(String tipoOcorrencia) {
		this.tipoOcorrencia = tipoOcorrencia;
	}
	public Integer getIdEmpregado() {
		return idEmpregado;
	}
	public void setIdEmpregado(Integer idEmpregado) {
		this.idEmpregado = idEmpregado;
	}
	public String getNomeEmpregado() {
		return nomeEmpregado;
	}
	public void setNomeEmpregado(String nomeEmpregado) {
		this.nomeEmpregado = nomeEmpregado;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
}
