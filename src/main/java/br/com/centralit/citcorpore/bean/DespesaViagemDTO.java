/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import br.com.centralit.citcorpore.util.Enumerados;
import br.com.citframework.dto.IDto;
import br.com.citframework.util.UtilStrings;

public class DespesaViagemDTO implements IDto {
	private static final long serialVersionUID = 1L;

	// Campos do banco de dados
	private Integer idDespesaViagem;
	private Date dataInicio;
	private Date dataFim;
	private Integer idRoteiro;
	private Integer idTipo;
	private String tipoDespesa;
	private Integer idFornecedor;
	private String nomeFornecedor;
	private Double valor;
	private Double valorTotal;
	private Integer quantidade;
	private Timestamp validade;
	private String validadeDate;
	private String validadeHora;
	private String original;
	private Integer idSolicitacaoServico;
	private String prestacaoContas;
	private String situacao;
	private Integer idMoeda;
	private String nomeMoeda;
	private Integer idFormaPagamento;
	private String nomeFormaPagamento;
	private String colIntegrantesViagem_Serialize;
	private String colIntegrantesViagem_SerializeAux;
	private String observacoes;
	private TipoMovimFinanceiraViagemDTO tipoMovimFinanceiraViagem;

	// atributos para controle de requisi��o em compras
	private String cancelarRequisicao;
	private String confirma;
	private Integer idResponsavelCompra;
	private Timestamp dataHoraCompra;

	// Campos para controle de formulario
	private Integer idContrato;
	private Integer idTarefa;
	private Integer idIntegrante;
	private Integer idIntegranteAux;
	private Integer idSolicitacaoServicoAux;
	private Date prazoCotacaoAux;
	private String horaCotacaoAux;

	public Integer getIdDespesaViagem() {
		return idDespesaViagem;
	}

	public void setIdDespesaViagem(Integer idDespesaViagem) {
		this.idDespesaViagem = idDespesaViagem;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Integer getIdRoteiro() {
		return idRoteiro;
	}

	public void setIdRoteiro(Integer idRoteiro) {
		this.idRoteiro = idRoteiro;
	}

	public Integer getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(Integer idTipo) {
		this.idTipo = idTipo;
	}

	public Integer getIdFornecedor() {
		return idFornecedor;
	}

	public void setIdFornecedor(Integer idFornecedor) {
		this.idFornecedor = idFornecedor;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public String getOriginal() {
		return original;
	}

	public void setOriginal(String original) {
		this.original = original;
	}

	public Integer getIdSolicitacaoServico() {
		return idSolicitacaoServico;
	}

	public void setIdSolicitacaoServico(Integer idSolicitacaoServico) {
		this.idSolicitacaoServico = idSolicitacaoServico;
		this.idSolicitacaoServicoAux = idSolicitacaoServico;
	}

	public String getPrestacaoContas() {
		return prestacaoContas;
	}

	public void setPrestacaoContas(String prestacaoContas) {
		this.prestacaoContas = prestacaoContas;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public Timestamp getValidade() {
		return validade;
	}

	public void setValidade(Timestamp validade) {
		this.validade = validade;
	}

	public Integer getIdMoeda() {
		return idMoeda;
	}

	public void setIdMoeda(Integer idMoeda) {
		this.idMoeda = idMoeda;
	}

	public Integer getIdFormaPagamento() {
		return idFormaPagamento;
	}

	public void setIdFormaPagamento(Integer idFormaPagamento) {
		this.idFormaPagamento = idFormaPagamento;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public Double getTotal() {
		Double total = 0.0;

		if (this.valor != null && this.quantidade != null) {
			// Se foi informado o tipo da movimenta��o financeira e se � do tipo diaria
			if(this.getTipoMovimFinanceiraViagem() != null && UtilStrings.removeCaracteresEspeciais(this.getTipoMovimFinanceiraViagem().getClassificacao()).equalsIgnoreCase(Enumerados.ClassificacaoMovFinViagem.Diaria.toString())) {
				// De acordo com o item 4.3a do PO-009 deve ser acrescida uma diaria adicional. 
				total = (this.valor * this.quantidade) + this.valor;
			} else {
				// Outras despesas
				total = this.valor * this.quantidade;
			}
		}

		return total;
	}

	public String getTotalFormatado() {
		NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));

		DecimalFormat decimal = (DecimalFormat) nf;

		decimal.applyPattern("#,##0.00");

		return decimal.format(this.getTotal());
	}

	public Integer getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Integer idContrato) {
		this.idContrato = idContrato;
	}

	public Integer getIdTarefa() {
		return idTarefa;
	}

	public void setIdTarefa(Integer idTarefa) {
		this.idTarefa = idTarefa;
	}

	public Integer getIdSolicitacaoServicoAux() {
		return idSolicitacaoServicoAux;
	}

	public void setIdSolicitacaoServicoAux(Integer idSolicitacaoServicoAux) {
		this.idSolicitacaoServico = idSolicitacaoServicoAux;
		this.idSolicitacaoServicoAux = idSolicitacaoServicoAux;
	}

	public String getValidadeHora() {
		return validadeHora;
	}

	public void setValidadeHora(String validadeHora) {
		this.validadeHora = validadeHora;
	}

	public String getValidadeDate() {
		return validadeDate;
	}

	public void setValidadeDate(String validadeDate) {
		this.validadeDate = validadeDate;
	}

	public Date getPrazoCotacaoAux() {
		return prazoCotacaoAux;
	}

	public void setPrazoCotacaoAux(Date prazoCotacaoAux) {
		this.prazoCotacaoAux = prazoCotacaoAux;
	}

	public String getHoraCotacaoAux() {
		return horaCotacaoAux;
	}

	public void setHoraCotacaoAux(String horaCotacaoAux) {
		this.horaCotacaoAux = horaCotacaoAux;
	}

	public String getCancelarRequisicao() {
		return cancelarRequisicao;
	}

	public void setCancelarRequisicao(String cancelarRequisicao) {
		this.cancelarRequisicao = cancelarRequisicao;
	}

	public String getConfirma() {
		return confirma;
	}

	public void setConfirma(String confirma) {
		this.confirma = confirma;
	}

	public Integer getIdResponsavelCompra() {
		return idResponsavelCompra;
	}

	public void setIdResponsavelCompra(Integer idResponsavelCompra) {
		this.idResponsavelCompra = idResponsavelCompra;
	}

	public Timestamp getDataHoraCompra() {
		return dataHoraCompra;
	}

	public void setDataHoraCompra(Timestamp dataHoraCompra) {
		this.dataHoraCompra = dataHoraCompra;
	}

	public String getColIntegrantesViagem_Serialize() {
		return colIntegrantesViagem_Serialize;
	}

	public void setColIntegrantesViagem_Serialize(
			String colIntegrantesViagem_Serialize) {
		this.colIntegrantesViagem_Serialize = colIntegrantesViagem_Serialize;
		this.colIntegrantesViagem_SerializeAux = colIntegrantesViagem_Serialize;
	}

	public String getColIntegrantesViagem_SerializeAux() {
		return colIntegrantesViagem_SerializeAux;
	}

	public void setColIntegrantesViagem_SerializeAux(
			String colIntegrantesViagem_SerializeAux) {
		this.colIntegrantesViagem_Serialize = colIntegrantesViagem_SerializeAux;
		this.colIntegrantesViagem_SerializeAux = colIntegrantesViagem_SerializeAux;
	}

	public String getTipoDespesa() {
		return tipoDespesa;
	}

	public void setTipoDespesa(String tipoDespesa) {
		this.tipoDespesa = tipoDespesa;
	}

	public String getNomeFornecedor() {
		return nomeFornecedor;
	}

	public void setNomeFornecedor(String nomeFornecedor) {
		this.nomeFornecedor = nomeFornecedor;
	}

	public String getNomeFormaPagamento() {
		return nomeFormaPagamento;
	}

	public void setNomeFormaPagamento(String nomeFormaPagamento) {
		this.nomeFormaPagamento = nomeFormaPagamento;
	}

	public String getNomeMoeda() {
		return nomeMoeda;
	}

	public void setNomeMoeda(String nomeMoeda) {
		this.nomeMoeda = nomeMoeda;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Integer getIdIntegrante() {
		return idIntegrante;
	}

	public void setIdIntegrante(Integer idIntegrante) {
		this.idIntegrante = idIntegrante;
	}

	public Integer getIdIntegranteAux() {
		return idIntegranteAux;
	}

	public void setIdIntegranteAux(Integer idIntegranteAux) {
		this.idIntegranteAux = idIntegranteAux;
	}

	public TipoMovimFinanceiraViagemDTO getTipoMovimFinanceiraViagem() {
		return tipoMovimFinanceiraViagem;
	}

	public void setTipoMovimFinanceiraViagem(TipoMovimFinanceiraViagemDTO tipoMovimFinanceiraViagem) {
		this.tipoMovimFinanceiraViagem = tipoMovimFinanceiraViagem;
	}

}
