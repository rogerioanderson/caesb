/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

/**
 * @author Maycon.Fernandes
 *
 */
public class InventarioXMLDTO implements IDto {
	private Integer idInventarioxml;
	private Integer idNetMap;
	private String nome;
	private String conteudo;
	private java.sql.Date datainicial;
	private java.sql.Date datafinal;
	
	/**
	 * Define valor do atributo idInventarioxml.
	 *
	 * @param idInventarioxml
	 */
	public void setIdInventarioxml(Integer idInventarioxml) {
	    this.idInventarioxml = idInventarioxml;
	}
	/**
	 * @return valor do atributo idInventarioxml.
	 */
	public Integer getIdInventarioxml() {
	    return idInventarioxml;
	}
	/**
	 * Define valor do atributo idNetMap.
	 *
	 * @param idNetMap
	 */
	public void setIdNetMap(Integer idNetMap) {
	    this.idNetMap = idNetMap;
	}
	/**
	 * @return valor do atributo idNetMap.
	 */
	public Integer getIdNetMap() {
	    return idNetMap;
	}
	/**
	 * Define valor do atributo nome.
	 *
	 * @param nome
	 */
	public void setNome(String nome) {
	    this.nome = nome;
	}
	/**
	 * @return valor do atributo nome.
	 */
	public String getNome() {
	    return nome;
	}
	/**
	 * Define valor do atributo conteudo.
	 *
	 * @param conteudo
	 */
	public void setConteudo(String conteudo) {
	    this.conteudo = conteudo;
	}
	/**
	 * @return valor do atributo conteudo.
	 */
	public String getConteudo() {
	    return conteudo;
	}
	/**
	 * Define valor do atributo dataInicio.
	 *
	 * @param dataInicio
	 */
	/**
	 * Define valor do atributo datainicial.
	 *
	 * @param datainicial
	 */
	public void setDatainicial(java.sql.Date datainicial) {
	    this.datainicial = datainicial;
	}
	/**
	 * @return valor do atributo datainicial.
	 */
	public java.sql.Date getDatainicial() {
	    return datainicial;
	}
	/**
	 * Define valor do atributo datafinal.
	 *
	 * @param datafinal
	 */
	public void setDatafinal(java.sql.Date datafinal) {
	    this.datafinal = datafinal;
	}
	/**
	 * @return valor do atributo datafinal.
	 */
	public java.sql.Date getDatafinal() {
	    return datafinal;
	}
	
}
