/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

/**
 * @author Pedro
 *
 */
public class ControleContratoPagamentoDTO implements IDto {

	private Integer idCcPagamento;
	private Integer idControleContrato;
	private Date dataCcPagamento;
	private Date dataAtrasoCcPagamento;
	private Integer parcelaCcPagamento;
	
	public Integer getIdCcPagamento() {
		return idCcPagamento;
	}
	public void setIdCcPagamento(Integer idCcPagamento) {
		this.idCcPagamento = idCcPagamento;
	}
	public Integer getIdControleContrato() {
		return idControleContrato;
	}
	public void setIdControleContrato(Integer idControleContrato) {
		this.idControleContrato = idControleContrato;
	}
	public Date getDataCcPagamento() {
		return dataCcPagamento;
	}
	public void setDataCcPagamento(Date dataCcPagamento) {
		this.dataCcPagamento = dataCcPagamento;
	}
	public Date getDataAtrasoCcPagamento() {
		return dataAtrasoCcPagamento;
	}
	public void setDataAtrasoCcPagamento(Date dataAtrasoCcPagamento) {
		this.dataAtrasoCcPagamento = dataAtrasoCcPagamento;
	}
	public Integer getParcelaCcPagamento() {
		return parcelaCcPagamento;
	}
	public void setParcelaCcPagamento(Integer parcelaCcPagamento) {
		this.parcelaCcPagamento = parcelaCcPagamento;
	}
	
	

}
