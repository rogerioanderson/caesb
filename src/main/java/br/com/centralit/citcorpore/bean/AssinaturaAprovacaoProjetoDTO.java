/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;
import br.com.citframework.util.UtilHTML;

public class AssinaturaAprovacaoProjetoDTO implements IDto, Comparable<AssinaturaAprovacaoProjetoDTO> {

	private Integer idAssinaturaAprovacaoProjeto;
	private Integer idProjeto;
	private Integer idEmpregadoAssinatura;
	private String papel;
	private String ordem;
	private EmpregadoDTO empregadoDTO;
	private String nome;
	private String matricula;
	
	
	public Integer getIdAssinaturaAprovacaoProjeto() {
		return idAssinaturaAprovacaoProjeto;
	}
	public void setIdAssinaturaAprovacaoProjeto(
			Integer idAssinaturaAprovacaoProjeto) {
		this.idAssinaturaAprovacaoProjeto = idAssinaturaAprovacaoProjeto;
	}
	public Integer getIdProjeto() {
		return idProjeto;
	}
	public void setIdProjeto(Integer idProjeto) {
		this.idProjeto = idProjeto;
	}
	public Integer getIdEmpregadoAssinatura() {
		return idEmpregadoAssinatura;
	}
	public void setIdEmpregadoAssinatura(Integer idEmpregadoAssinatura) {
		this.idEmpregadoAssinatura = idEmpregadoAssinatura;
	}
	public String getPapel() {
		return papel;
	}
	public String getPapelHTMLEncoded() {
		return UtilHTML.encodeHTML(papel);
	}
	public void setPapel(String papel) {
		this.papel = papel;
	}
	public String getOrdem() {
		return ordem;
	}
	public void setOrdem(String ordem) {
		this.ordem = ordem;
	}
	public EmpregadoDTO getEmpregadoDTO() {
		return empregadoDTO;
	}
	public void setEmpregadoDTO(EmpregadoDTO empregadoDTO) {
		this.empregadoDTO = empregadoDTO;
	}
	public String getNome() {
		return nome;
	}
	public String getNomeHTMLEncoded() {
		return UtilHTML.encodeHTML(nome);
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public String getMatriculaHTMLEncoded() {
		return UtilHTML.encodeHTML(matricula);
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public int compareTo(AssinaturaAprovacaoProjetoDTO assinatura){
		return ordem.compareTo(assinatura.getOrdem());
	}
	
}
