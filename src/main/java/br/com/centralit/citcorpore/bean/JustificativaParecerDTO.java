/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.centralit.citcorpore.util.Util;
import br.com.citframework.dto.IDto;

/**
 * @author breno.guimaraes
 * 
 */
public class JustificativaParecerDTO implements IDto {

    /**
     * 
     */
    private static final long serialVersionUID = -3055161845325828805L;

    private Integer idJustificativa;
    private String descricaoJustificativa;
    private String aplicavelRequisicao;
    private String aplicavelCotacao;
    private String aplicavelInspecao;
    private String situacao;
	private Date dataInicio;
	private Date dataFim;

    public Integer getIdJustificativa() {
		return idJustificativa;
	}
	public void setIdJustificativa(Integer idJustificativa) {
		this.idJustificativa = idJustificativa;
	}
	public String getDescricaoJustificativa() {
		return Util.tratarAspasSimples(this.descricaoJustificativa);
	}
	public void setDescricaoJustificativa(String descricaoJustificativa) {
		this.descricaoJustificativa = descricaoJustificativa;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
    public String getAplicavelRequisicao() {
        return aplicavelRequisicao;
    }
    public void setAplicavelRequisicao(String aplicavelRequisicao) {
        this.aplicavelRequisicao = aplicavelRequisicao;
    }
    public String getAplicavelCotacao() {
        return aplicavelCotacao;
    }
    public void setAplicavelCotacao(String aplicavelCotacao) {
        this.aplicavelCotacao = aplicavelCotacao;
    }
    public String getAplicavelInspecao() {
        return aplicavelInspecao;
    }
    public void setAplicavelInspecao(String aplicavelInspecao) {
        this.aplicavelInspecao = aplicavelInspecao;
    }
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
}
