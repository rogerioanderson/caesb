/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;
/**
 * @author thiago matias
 *  
 */
public class RequisicaoLiberacaoResponsavelDTO implements IDto {
	
	private static final long serialVersionUID = 1L;
	private Integer idRequisicaoLiberacao;
	private Integer idResponsavel;
	//private Integer idhistoricoliberacao;
	
	private Integer idRequisicaoLiberacaoResp;
	private String nomeResponsavel;
	private String papelResponsavel;
	private String telResponsavel;
	private String emailResponsavel;
	private String nomeCargo;
	private Date dataFim;
	private Integer sequenciaEmpregado;

	
	public Integer getIdRequisicaoLiberacaoResp() {
		return idRequisicaoLiberacaoResp;
	}
	public void setIdRequisicaoLiberacaoResp(Integer idRequisicaoLiberacaoResp) {
		this.idRequisicaoLiberacaoResp = idRequisicaoLiberacaoResp;
	}
	
	public Integer getIdRequisicaoLiberacao(){
		return this.idRequisicaoLiberacao;
	}
	public void setIdRequisicaoLiberacao(Integer parm){
		this.idRequisicaoLiberacao = parm;
	}

	public Integer getIdResponsavel(){
		return this.idResponsavel;
	}
	public void setIdResponsavel(Integer parm){
		this.idResponsavel = parm;
	}
    
	/*public Integer getIdhistoricoliberacao() {
		return idhistoricoliberacao;
	}
	public void setIdhistoricoliberacao(Integer idhistoricoliberacao) {
		this.idhistoricoliberacao = idhistoricoliberacao;
	}*/
	public String getNomeResponsavel() {
		return nomeResponsavel;
	}
	public void setNomeResponsavel(String nomeResponsavel) {
		this.nomeResponsavel = nomeResponsavel;
	}
	public String getPapelResponsavel() {
		return papelResponsavel;
	}
	public void setPapelResponsavel(String papelResponsavel) {
		this.papelResponsavel = papelResponsavel;
	}
	public String getTelResponsavel() {
		return telResponsavel;
	}
	public void setTelResponsavel(String telResponsavel) {
		this.telResponsavel = telResponsavel;
	}
	public String getEmailResponsavel() {
		return emailResponsavel;
	}
	public void setEmailResponsavel(String emailResponsavel) {
		this.emailResponsavel = emailResponsavel;
	}
	public String getNomeCargo() {
		return nomeCargo;
	}
	public void setNomeCargo(String nomeCargo) {
		this.nomeCargo = nomeCargo;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	/**
	 * @return the sequenciaEmpregado
	 */
	public Integer getSequenciaEmpregado() {
		return sequenciaEmpregado;
	}
	/**
	 * @param sequenciaEmpregado the sequenciaEmpregado to set
	 */
	public void setSequenciaEmpregado(Integer sequenciaEmpregado) {
		this.sequenciaEmpregado = sequenciaEmpregado;
	}

}
