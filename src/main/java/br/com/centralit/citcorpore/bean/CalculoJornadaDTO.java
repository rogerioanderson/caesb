/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Timestamp;

import br.com.citframework.dto.IDto;

public class CalculoJornadaDTO implements IDto {
	private Integer idCalendario;
	private Timestamp dataHoraInicial;
	private Integer prazoHH;
	private Integer prazoMM;
	private Integer tempoDecorridoHH;
	private Integer tempoDecorridoMM;
	private Integer tempoDecorridoSS;
	
	private Timestamp dataHoraFinal;

	public CalculoJornadaDTO(Integer idCalendario, Timestamp dataHoraInicial, Integer prazoHH, Integer prazoMM) {
		this.idCalendario = idCalendario;
		this.dataHoraInicial = dataHoraInicial;
		this.prazoHH = prazoHH;
		this.prazoMM = prazoMM;
	}
	
	public CalculoJornadaDTO(Integer idCalendario, Timestamp dataHoraInicial) {
		this.idCalendario = idCalendario;
		this.dataHoraInicial = dataHoraInicial;
	}

	public Integer getIdCalendario() {
		return idCalendario;
	}

	public void setIdCalendario(Integer idCalendario) {
		this.idCalendario = idCalendario;
	}

	public Timestamp getDataHoraInicial() {
		return dataHoraInicial;
	}

	public void setDataHoraInicial(Timestamp dataHoraInicial) {
		this.dataHoraInicial = dataHoraInicial;
	}

	public Integer getPrazoHH() {
		return prazoHH;
	}

	public void setPrazoHH(Integer prazoHH) {
		this.prazoHH = prazoHH;
	}

	public Integer getPrazoMM() {
		return prazoMM;
	}

	public void setPrazoMM(Integer prazoMM) {
		this.prazoMM = prazoMM;
	}

	public Timestamp getDataHoraFinal() {
		return dataHoraFinal;
	}

	public void setDataHoraFinal(Timestamp dataHoraFinal) {
		this.dataHoraFinal = dataHoraFinal;
	}

	public Integer getTempoDecorridoHH() {
		return tempoDecorridoHH;
	}

	public void setTempoDecorridoHH(Integer tempoDecorridoHH) {
		this.tempoDecorridoHH = tempoDecorridoHH;
	}

	public Integer getTempoDecorridoMM() {
		return tempoDecorridoMM;
	}

	public void setTempoDecorridoMM(Integer tempoDecorridoMM) {
		this.tempoDecorridoMM = tempoDecorridoMM;
	}

	public Integer getTempoDecorridoSS() {
		return tempoDecorridoSS;
	}

	public void setTempoDecorridoSS(Integer tempoDecorridoSS) {
		this.tempoDecorridoSS = tempoDecorridoSS;
	}

}
