/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class EventTotalsDTO implements IDto {
	private int qtdeCritical;
	private int qtdeWarning;
	private int qtdeOk;
	private int qtdeUnknown;
	
	private int qtdeDown;
	private int qtdeUp;
	
	public EventTotalsDTO(){
		qtdeCritical = 0;
		qtdeWarning = 0;
		qtdeOk = 0;
		qtdeUnknown = 0;
		
		qtdeDown = 0;
		qtdeUp = 0;
	}
	public int getQtdeCritical() {
		return qtdeCritical;
	}
	public void setQtdeCritical(int qtdeCritical) {
		this.qtdeCritical = qtdeCritical;
	}
	public int getQtdeWarning() {
		return qtdeWarning;
	}
	public void setQtdeWarning(int qtdeWarning) {
		this.qtdeWarning = qtdeWarning;
	}
	public int getQtdeOk() {
		return qtdeOk;
	}
	public void setQtdeOk(int qtdeOk) {
		this.qtdeOk = qtdeOk;
	}
	public int getQtdeUnknown() {
		return qtdeUnknown;
	}
	public void setQtdeUnknown(int qtdeUnknown) {
		this.qtdeUnknown = qtdeUnknown;
	}
	public int getQtdeDown() {
		return qtdeDown;
	}
	public void setQtdeDown(int qtdeDown) {
		this.qtdeDown = qtdeDown;
	}
	public int getQtdeUp() {
		return qtdeUp;
	}
	public void setQtdeUp(int qtdeUp) {
		this.qtdeUp = qtdeUp;
	}
}
