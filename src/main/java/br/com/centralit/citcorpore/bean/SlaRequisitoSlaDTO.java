/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

/**
 * @since 14/06/2013
 * @author rodrigo.oliveira
 *
 */
public class SlaRequisitoSlaDTO implements IDto {
	
	private Integer idRequisitoSLA;
	private Integer idAcordoNivelServico;
	private java.sql.Date dataVinculacao;
	private java.sql.Date dataUltModificacao;
	private String deleted;
	
	public Integer getIdRequisitoSLA() {
		return idRequisitoSLA;
	}
	public void setIdRequisitoSLA(Integer idRequisitoSLA) {
		this.idRequisitoSLA = idRequisitoSLA;
	}
	public Integer getIdAcordoNivelServico() {
		return idAcordoNivelServico;
	}
	public void setIdAcordoNivelServico(Integer idAcordoNivelServico) {
		this.idAcordoNivelServico = idAcordoNivelServico;
	}
	public java.sql.Date getDataVinculacao() {
		return dataVinculacao;
	}
	public void setDataVinculacao(java.sql.Date dataVinculacao) {
		this.dataVinculacao = dataVinculacao;
	}
	public java.sql.Date getDataUltModificacao() {
		return dataUltModificacao;
	}
	public void setDataUltModificacao(java.sql.Date dataUltModificacao) {
		this.dataUltModificacao = dataUltModificacao;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

}
