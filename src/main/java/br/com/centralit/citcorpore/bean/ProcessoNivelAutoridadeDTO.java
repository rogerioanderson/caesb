/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class ProcessoNivelAutoridadeDTO implements IDto {
	private Integer idProcessoNegocio;
	private Integer idNivelAutoridade;
	private String permiteAprovacaoPropria;
	private String permiteSolicitacao;
	private Integer antecedenciaMinimaAprovacao;
	
	private Integer sequencia;
	private Integer hierarquia;
	private NivelAutoridadeDTO nivelAutoridadeDto;
	private LimiteAprovacaoDTO limiteAprovacaoDto;

	public Integer getIdProcessoNegocio(){
		return this.idProcessoNegocio;
	}
	public void setIdProcessoNegocio(Integer parm){
		this.idProcessoNegocio = parm;
	}

	public Integer getIdNivelAutoridade(){
		return this.idNivelAutoridade;
	}
	public void setIdNivelAutoridade(Integer parm){
		this.idNivelAutoridade = parm;
	}

	public String getPermiteAprovacaoPropria(){
		return this.permiteAprovacaoPropria;
	}
	public void setPermiteAprovacaoPropria(String parm){
		this.permiteAprovacaoPropria = parm;
	}

	public String getPermiteSolicitacao(){
		return this.permiteSolicitacao;
	}
	public void setPermiteSolicitacao(String parm){
		this.permiteSolicitacao = parm;
	}

	public Integer getAntecedenciaMinimaAprovacao(){
		return this.antecedenciaMinimaAprovacao;
	}
	public void setAntecedenciaMinimaAprovacao(Integer parm){
		this.antecedenciaMinimaAprovacao = parm;
	}
	public Integer getSequencia() {
		return sequencia;
	}
	public void setSequencia(Integer sequencia) {
		this.sequencia = sequencia;
	}
	public Integer getHierarquia() {
		return hierarquia;
	}
	public void setHierarquia(Integer hierarquia) {
		this.hierarquia = hierarquia;
	}
	public NivelAutoridadeDTO getNivelAutoridadeDto() {
		return nivelAutoridadeDto;
	}
	public void setNivelAutoridadeDto(NivelAutoridadeDTO nivelAutoridadeDto) {
		this.nivelAutoridadeDto = nivelAutoridadeDto;
	}
	public LimiteAprovacaoDTO getLimiteAprovacaoDto() {
		return limiteAprovacaoDto;
	}
	public void setLimiteAprovacaoDto(LimiteAprovacaoDTO limiteAprovacaoDto) {
		this.limiteAprovacaoDto = limiteAprovacaoDto;
	}

}
