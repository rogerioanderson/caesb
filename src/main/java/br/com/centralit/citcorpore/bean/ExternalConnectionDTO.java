/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

/**
 * @author thiago.borges
 *
 */
public class ExternalConnectionDTO implements IDto {
	/**
    Thiago Borges da Silva
    **/
	
	private static final long serialVersionUID = 1L;
	private Integer idExternalConnection;
	private String nome;
	private String tipo;
	private String urlJdbc;
	private String jdbcDbName;
	private String jdbcDriver;
	private String jdbcUser;
	private String jdbcPassword;
	private String fileName;
	private String schemaDb;
	private String deleted;
	
	
	
	/**
	 * Metodos Gets and Sets
	 */
	public Integer getIdExternalConnection() {
		return idExternalConnection;
	}
	public void setIdExternalConnection(Integer idExternalConnection) {
		this.idExternalConnection = idExternalConnection;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getUrlJdbc() {
		return urlJdbc;
	}
	public void setUrlJdbc(String urlJdbc) {
		this.urlJdbc = urlJdbc;
	}
	public String getJdbcDbName() {
		return jdbcDbName;
	}
	public void setJdbcDbName(String jdbcDbName) {
		this.jdbcDbName = jdbcDbName;
	}
	public String getJdbcDriver() {
		return jdbcDriver;
	}
	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}
	public String getJdbcUser() {
		return jdbcUser;
	}
	public void setJdbcUser(String jdbcUser) {
		this.jdbcUser = jdbcUser;
	}
	public String getJdbcPassword() {
		return jdbcPassword;
	}
	public void setJdbcPassword(String jdbcPassword) {
		this.jdbcPassword = jdbcPassword;
	}
	public String getSchemaDb() {
		return schemaDb;
	}
	public void setSchemaDb(String schemaDb) {
		this.schemaDb = schemaDb;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
