/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class ValorLimiteAprovacaoDTO implements IDto {
	private Integer idValorLimiteAprovacao;
	private Integer idLimiteAprovacao;
	private String tipoUtilizacao;
	private String tipoLimite;
	private Double valorLimite;
	private Integer intervaloDias;
	
	private Integer sequencia;

	public Integer getIdValorLimiteAprovacao(){
		return this.idValorLimiteAprovacao;
	}
	public void setIdValorLimiteAprovacao(Integer parm){
		this.idValorLimiteAprovacao = parm;
	}

	public Integer getIdLimiteAprovacao(){
		return this.idLimiteAprovacao;
	}
	public void setIdLimiteAprovacao(Integer parm){
		this.idLimiteAprovacao = parm;
	}

	public String getTipoUtilizacao(){
		return this.tipoUtilizacao;
	}
	public void setTipoUtilizacao(String parm){
		this.tipoUtilizacao = parm;
	}

	public String getTipoLimite(){
		return this.tipoLimite;
	}
	public void setTipoLimite(String parm){
		this.tipoLimite = parm;
	}

	public Double getValorLimite(){
		return this.valorLimite;
	}
	public void setValorLimite(Double parm){
		this.valorLimite = parm;
	}

	public Integer getIntervaloDias(){
		return this.intervaloDias;
	}
	public void setIntervaloDias(Integer parm){
		this.intervaloDias = parm;
	}
	public Integer getSequencia() {
		return sequencia;
	}
	public void setSequencia(Integer sequencia) {
		this.sequencia = sequencia;
	}

}
