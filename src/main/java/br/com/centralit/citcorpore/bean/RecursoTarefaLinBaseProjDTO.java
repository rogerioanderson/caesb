/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class RecursoTarefaLinBaseProjDTO implements IDto {
	private Integer idRecursoTarefaLinBaseProj;
	private Integer idTarefaLinhaBaseProjeto;
	private Integer idPerfilContrato;
	private Integer idEmpregado;
	private Double percentualAloc;
	private String tempoAloc;
	private Double percentualExec;
	private Double tempoAlocMinutos;
	private Double custo;
	private Double custoPerfil;
	private String esforcoPorOS;
	
	private EmpregadoDTO empregadoDTO;
	
	private double tempoAlocEmMinutos = 0;

	public Integer getIdRecursoTarefaLinBaseProj(){
		return this.idRecursoTarefaLinBaseProj;
	}
	public void setIdRecursoTarefaLinBaseProj(Integer parm){
		this.idRecursoTarefaLinBaseProj = parm;
	}

	public Integer getIdTarefaLinhaBaseProjeto(){
		return this.idTarefaLinhaBaseProjeto;
	}
	public void setIdTarefaLinhaBaseProjeto(Integer parm){
		this.idTarefaLinhaBaseProjeto = parm;
	}

	public Integer getIdPerfilContrato(){
		return this.idPerfilContrato;
	}
	public void setIdPerfilContrato(Integer parm){
		this.idPerfilContrato = parm;
	}

	public Integer getIdEmpregado(){
		return this.idEmpregado;
	}
	public void setIdEmpregado(Integer parm){
		this.idEmpregado = parm;
	}

	public Double getPercentualAloc(){
		return this.percentualAloc;
	}
	public void setPercentualAloc(Double parm){
		this.percentualAloc = parm;
	}
	public String getTempoAloc() {
		return tempoAloc;
	}
	public void setTempoAloc(String tempoAloc) {
		this.tempoAloc = tempoAloc;
	}
	public Double getPercentualExec() {
		return percentualExec;
	}
	public void setPercentualExec(Double percentualExec) {
		this.percentualExec = percentualExec;
	}
	public double getTempoAlocEmMinutos() {
		return tempoAlocEmMinutos;
	}
	public void setTempoAlocEmMinutos(double tempoAlocEmMinutos) {
		this.tempoAlocEmMinutos = tempoAlocEmMinutos;
	}
	public EmpregadoDTO getEmpregadoDTO() {
		return empregadoDTO;
	}
	public void setEmpregadoDTO(EmpregadoDTO empregadoDTO) {
		this.empregadoDTO = empregadoDTO;
	}
	public Double getTempoAlocMinutos() {
		return tempoAlocMinutos;
	}
	public void setTempoAlocMinutos(Double tempoAlocMinutos) {
		this.tempoAlocMinutos = tempoAlocMinutos;
	}
	public Double getCusto() {
		return custo;
	}
	public void setCusto(Double custo) {
		this.custo = custo;
	}
	public Double getCustoPerfil() {
		return custoPerfil;
	}
	public void setCustoPerfil(Double custoPerfil) {
		this.custoPerfil = custoPerfil;
	}
	public String getEsforcoPorOS() {
		return esforcoPorOS;
	}
	public void setEsforcoPorOS(String esforcoPorOS) {
		this.esforcoPorOS = esforcoPorOS;
	}

}
