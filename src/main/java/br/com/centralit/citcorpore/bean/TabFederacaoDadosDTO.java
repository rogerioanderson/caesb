/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class TabFederacaoDadosDTO implements IDto {
	private static final long serialVersionUID = 5043299658726392397L;
	private Integer idTabFederacao;
	private String nomeTabela;
	private String chaveFinal;
	private String chaveOriginal;
	private String origem;
	private java.sql.Timestamp criacao;
	private java.sql.Timestamp ultAtualiz;
	private String uuid;
	
	public String getNomeTabela(){
		return this.nomeTabela;
	}
	public void setNomeTabela(String parm){
		this.nomeTabela = parm;
	}

	public String getChaveFinal(){
		return this.chaveFinal;
	}
	public void setChaveFinal(String parm){
		this.chaveFinal = parm;
	}

	public String getChaveOriginal(){
		return this.chaveOriginal;
	}
	public void setChaveOriginal(String parm){
		this.chaveOriginal = parm;
	}

	public String getOrigem(){
		return this.origem;
	}
	public void setOrigem(String parm){
		this.origem = parm;
	}
	public java.sql.Timestamp getCriacao() {
		return criacao;
	}
	public void setCriacao(java.sql.Timestamp criacao) {
		this.criacao = criacao;
	}
	public java.sql.Timestamp getUltAtualiz() {
		return ultAtualiz;
	}
	public void setUltAtualiz(java.sql.Timestamp ultAtualiz) {
		this.ultAtualiz = ultAtualiz;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
