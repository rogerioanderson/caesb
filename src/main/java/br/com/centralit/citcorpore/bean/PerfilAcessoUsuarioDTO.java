/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * Central IT - CITSmart
 */
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

/**
 * DTO de PerfilAcessoUsuario.
 * 
 * @author valdoilo.damasceno
 */
public class PerfilAcessoUsuarioDTO implements IDto {

    private static final long serialVersionUID = 4622694451305944947L;

    private Integer idPerfilAcesso;

    private Integer idUsuario;

    private Date dataInicio;

    private Date dataFim;

    /**
     * @return valor do atributo idPerfilAcesso.
     */
    public Integer getIdPerfilAcesso() {
	return idPerfilAcesso;
    }

    /**
     * Define valor do atributo idPerfilAcesso.
     * 
     * @param idPerfilAcesso
     */
    public void setIdPerfilAcesso(Integer idPerfilAcesso) {
	this.idPerfilAcesso = idPerfilAcesso;
    }

    /**
     * @return valor do atributo idUsuario.
     */
    public Integer getIdUsuario() {
	return idUsuario;
    }

    /**
     * Define valor do atributo idUsuario.
     * 
     * @param idUsuario
     */
    public void setIdUsuario(Integer idUsuario) {
	this.idUsuario = idUsuario;
    }

    /**
     * @return valor do atributo dataInicio.
     */
    public Date getDataInicio() {
	return dataInicio;
    }

    /**
     * Define valor do atributo dataInicio.
     * 
     * @param dataInicio
     */
    public void setDataInicio(Date dataInicio) {
	this.dataInicio = dataInicio;
    }

    /**
     * @return valor do atributo dataFim.
     */
    public Date getDataFim() {
	return dataFim;
    }

    /**
     * Define valor do atributo dataFim.
     * 
     * @param dataFim
     */
    public void setDataFim(Date dataFim) {
	this.dataFim = dataFim;
    }

}
