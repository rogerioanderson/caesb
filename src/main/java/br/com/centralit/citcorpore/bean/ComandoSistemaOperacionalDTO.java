/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

/**
 * @author ygor.magalhaes
 *
 */
public class ComandoSistemaOperacionalDTO implements IDto {

    private static final long serialVersionUID = -6847753633682691321L;

    private Integer id;

    private Integer idComando;

    private Integer idSistemaOperacional;

    private String comando;

    /**
     * @return valor do atributo idComando.
     */
    public Integer getIdComando() {
	return idComando;
    }

    /**
     * Define valor do atributo idComando.
     * 
     * @param idComando
     */
    public void setIdComando(Integer idComando) {
	this.idComando = idComando;
    }

    /**
     * @return valor do atributo idSistemaOperacional.
     */
    public Integer getIdSistemaOperacional() {
	return idSistemaOperacional;
    }

    /**
     * Define valor do atributo idSistemaOperacional.
     * 
     * @param idSistemaOperacional
     */
    public void setIdSistemaOperacional(Integer idSistemaOperacional) {
	this.idSistemaOperacional = idSistemaOperacional;
    }

    /**
     * @return valor do atributo comando.
     */
    public String getComando() {
	return comando;
    }

    /**
     * Define valor do atributo comando.
     * 
     * @param comando
     */
    public void setComando(String comando) {
	this.comando = comando;
    }

    /**
     * @return valor do atributo id.
     */
    public Integer getId() {
	return id;
    }

    /**
     * Define valor do atributo id.
     * 
     * @param id
     */
    public void setId(Integer id) {
	this.id = id;
    }

}
