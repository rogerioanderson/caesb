/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Hashtable;

import javax.naming.directory.DirContext;

import br.com.citframework.dto.IDto;

@SuppressWarnings({ "rawtypes", "serial" })
public class ServidorContextoDTO implements IDto {

	private boolean ativo;
	private String baseDominio;
	private DirContext dirContext;
	private String grupoPadrao;
	private Hashtable informacoesContexto;
	private String ldpaAtributo;
	private String ldpaFiltro;
	private String numeroColaboradores;
	private String perfilAcesso;
	private String servidor;
	private String subDominio;

	public String getBaseDominio() {
		return baseDominio;
	}

	public DirContext getDirContext() {
		return dirContext;
	}

	public String getGrupoPadrao() {
		return grupoPadrao;
	}

	public Hashtable getInformacoesContexto() {
		return informacoesContexto;
	}

	public String getLdpaAtributo() {
		return ldpaAtributo;
	}

	public String getLdpaFiltro() {
		return ldpaFiltro;
	}

	public String getNumeroColaboradores() {
		return numeroColaboradores;
	}

	public String getPerfilAcesso() {
		return perfilAcesso;
	}

	public String getServidor() {
		return servidor;
	}

	public String getSubDominio() {
		return subDominio;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public void setBaseDominio(String baseDominio) {
		this.baseDominio = baseDominio;
	}

	public void setDirContext(DirContext dirContext) {
		this.dirContext = dirContext;
	}

	public void setGrupoPadrao(String grupoPadrao) {
		this.grupoPadrao = grupoPadrao;
	}

	public void setInformacoesContexto(Hashtable informacoesContexto) {
		this.informacoesContexto = informacoesContexto;
	}

	public void setLdpaAtributo(String ldpaAtributo) {
		this.ldpaAtributo = ldpaAtributo;
	}

	public void setLdpaFiltro(String ldpaFiltro) {
		this.ldpaFiltro = ldpaFiltro;
	}

	public void setNumeroColaboradores(String numeroColaboradores) {
		this.numeroColaboradores = numeroColaboradores;
	}

	public void setPerfilAcesso(String perfilAcesso) {
		this.perfilAcesso = perfilAcesso;
	}

	public void setServidor(String servidor) {
		this.servidor = servidor;
	}

	public void setSubDominio(String subDominio) {
		this.subDominio = subDominio;
	}

}
