/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

@SuppressWarnings("serial")
public class RelatorioOrdemServicoUstDTO implements IDto {

	private Double ano;
	private Integer idContrato;
	private Double custoAtividade;
	private String custoAtividadeFormatada;
	private Double periodoDouble;
	private String mes;
	private String tipoEventoServico;
	private Integer quantidadePorTipoEventoServico;

	/**
	 * @return the custoAtividade
	 */
	public Double getCustoAtividade() {
		return custoAtividade;
	}

	/**
	 * @param custoAtividade
	 *            the custoAtividade to set
	 */
	public void setCustoAtividade(Double custoAtividade) {
		this.custoAtividade = custoAtividade;
	}

	/**
	 * @return the periodo
	 */
	public Integer getPeriodo() {
		return (int) ( this.periodoDouble / 1);
	}


	/**
	 * @return the mes
	 */
	public String getMes() {
		return mes;
	}

	/**
	 * @param mes
	 *            the mes to set
	 */
	public void setMes(String mes) {
		this.mes = mes;
	}

	/**
	 * @return the custoAtividadeFormatada
	 */
	public String getCustoAtividadeFormatada() {
		return custoAtividadeFormatada;
	}

	/**
	 * @param custoAtividadeFormatada
	 *            the custoAtividadeFormatada to set
	 */
	public void setCustoAtividadeFormatada(String custoAtividadeFormatada) {
		this.custoAtividadeFormatada = custoAtividadeFormatada;
	}

	/**
	 * @return the tipoEventoServico
	 */
	public String getTipoEventoServico() {
		return tipoEventoServico;
	}

	/**
	 * @param tipoEventoServico
	 *            the tipoEventoServico to set
	 */
	public void setTipoEventoServico(String tipoEventoServico) {
		this.tipoEventoServico = tipoEventoServico;
	}

	/**
	 * @return the quantidadePorTipoEventoServico
	 */
	public Integer getQuantidadePorTipoEventoServico() {
		return quantidadePorTipoEventoServico;
	}

	/**
	 * @param quantidadePorTipoEventoServico
	 *            the quantidadePorTipoEventoServico to set
	 */
	public void setQuantidadePorTipoEventoServico(Integer quantidadePorTipoEventoServico) {
		this.quantidadePorTipoEventoServico = quantidadePorTipoEventoServico;
	}

	/**
	 * @return the idContrato
	 */
	public Integer getIdContrato() {
		return idContrato;
	}

	/**
	 * @param idContrato
	 *            the idContrato to set
	 */
	public void setIdContrato(Integer idContrato) {
		this.idContrato = idContrato;
	}

	public Double getAno() {
		return ano;
	}

	public void setAno(Double ano) {
		this.ano = ano;
	}

	public Integer getAnoInteger() {
		return (int) (this.ano / 1);
	}

	public Double getPeriodoDouble() {
		return periodoDouble;
	}

	public void setPeriodoDouble(Double periodoDouble) {
		this.periodoDouble = periodoDouble;
	}

}
