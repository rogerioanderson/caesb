/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

/**
 * @author Pedro
 *
 */
public class ControleContratoOcorrenciaDTO implements IDto {

    private Integer idCcOcorrencia;
    private Integer idControleContrato;
    private Integer idEmpregadoOcorrencia;
    
    private Date dataCcOcorrencia;
    private String assuntoCcOcorrencia;
    private String empregadoCcOcorrencia;
    
	public String getEmpregadoCcOcorrencia() {
		return empregadoCcOcorrencia;
	}
	public void setEmpregadoCcOcorrencia(String empregadoCcOcorrencia) {
		this.empregadoCcOcorrencia = empregadoCcOcorrencia;
	}
	public Integer getIdCcOcorrencia() {
		return idCcOcorrencia;
	}
	public void setIdCcOcorrencia(Integer idCcOcorrencia) {
		this.idCcOcorrencia = idCcOcorrencia;
	}
	public Integer getIdControleContrato() {
		return idControleContrato;
	}
	public void setIdControleContrato(Integer idControleContrato) {
		this.idControleContrato = idControleContrato;
	}
	public Integer getIdEmpregadoOcorrencia() {
		return idEmpregadoOcorrencia;
	}
	public void setIdEmpregadoOcorrencia(Integer idEmpregadoOcorrencia) {
		this.idEmpregadoOcorrencia = idEmpregadoOcorrencia;
	}
	public Date getDataCcOcorrencia() {
		return dataCcOcorrencia;
	}
	public void setDataCcOcorrencia(Date dataCcOcorrencia) {
		this.dataCcOcorrencia = dataCcOcorrencia;
	}
	public String getAssuntoCcOcorrencia() {
		return assuntoCcOcorrencia;
	}
	public void setAssuntoCcOcorrencia(String assuntoCcOcorrencia) {
		this.assuntoCcOcorrencia = assuntoCcOcorrencia;
	}
    
	
   

}
