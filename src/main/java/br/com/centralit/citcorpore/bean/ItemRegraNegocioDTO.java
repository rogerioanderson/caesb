/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Collection;
import java.util.Iterator;

import br.com.citframework.dto.IDto;

public class ItemRegraNegocioDTO implements IDto {
    
    private String resultadoValidacao;
    private String mensagensValidacao;
    private boolean ignoraErroImpeditivo;
    
    private String imagem;
    private String mensagensFmtHTML;
    
    public String getResultadoValidacao() {
        return resultadoValidacao;
    }
    public void setResultadoValidacao(String resultadoValidacao) {
        this.resultadoValidacao = resultadoValidacao;
    }
    public String getMensagensValidacao() {
        return mensagensValidacao;
    }

    public void setMensagensValidacao(String mensagensValidacao) {
        this.mensagensValidacao = mensagensValidacao;
    } 
    
    public String getMensagensFmtHTML() {
        if (mensagensValidacao == null || mensagensValidacao.trim().equals("")) return "";
        
        Collection colMensagens = null;
        try {
            colMensagens = getColMensagensValidacao();
        } catch (Exception e) {
        }
        if (colMensagens == null || colMensagens.size() == 0) return "";

        mensagensFmtHTML = "";
        for(Iterator it = colMensagens.iterator(); it.hasNext();){
            MensagemRegraNegocioDTO mensagemDto = (MensagemRegraNegocioDTO)it.next();
            mensagensFmtHTML += "<img src='" + mensagemDto.getImagem() + "'>" + mensagemDto.getMensagem() + "<br>";
        }
        return mensagensFmtHTML;
    }
    
    public String getImagem() {
        MensagemRegraNegocioDTO mensagemDto = new MensagemRegraNegocioDTO();
        mensagemDto.setTipo(resultadoValidacao);
        imagem = mensagemDto.getImagem();
        return imagem;
    }
    
    public Collection getColMensagensValidacao() throws Exception {
        if (this.getMensagensValidacao() == null) return null;
        return br.com.citframework.util.WebUtil.deserializeCollectionFromString(MensagemRegraNegocioDTO.class, this.getMensagensValidacao());
    }
    
    public void setColMensagensValidacao(Collection colMensagens) throws Exception {
       if (colMensagens == null) {
           setMensagensValidacao(null);
       }else{
           setMensagensValidacao(br.com.citframework.util.WebUtil.serializeObjects(colMensagens));  
       }
       
    }
    public boolean isIgnoraErroImpeditivo() {
        return ignoraErroImpeditivo;
    }
    public boolean getIgnoraErroImpeditivo() {
        return ignoraErroImpeditivo;
    }
    public void setIgnoraErroImpeditivo(boolean ignoraErroImpeditivo) {
        this.ignoraErroImpeditivo = ignoraErroImpeditivo;
    }
    public void setImagem(String imagem) {
        this.imagem = imagem;
    }
    public void setMensagensFmtHTML(String mensagensFmtHTML) {
        this.mensagensFmtHTML = mensagensFmtHTML;
    }
}
