/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

/** 
 * @author thiago.monteiro
 * Classe de objetos de categoria de ocorr�ncia.
 * Um objeto dessa classe � um DTO (Data Transfer Object) e pertence a camada de MODELO (MODEL).
 * Um objeto dessa classe � respons�vel por armazenar os dados que trafegam entre as camadas de VIS�O (VIEW) e de PERSIST�NCIA (PERSISTENCE) bidirecionalmente.
 */
public class CategoriaOcorrenciaDTO implements IDto {	
	private static final long serialVersionUID = -1128815777881092914L;
	
	private Integer idCategoriaOcorrencia;
	private String nome;
	private Date dataInicio;
	private Date dataFim;

	/** 
	 * @return idCategoriaOcorrencia
	 */
	public Integer getIdCategoriaOcorrencia() {
		return idCategoriaOcorrencia;
	}
	
	/**
	 * @param id
	 */
	public void setIdCategoriaOcorrencia(Integer idCategoriaOcorrencia) {
		this.idCategoriaOcorrencia = idCategoriaOcorrencia;
	}
	
	/**
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return dataInicio
	 */
	public Date getDataInicio() {
		return dataInicio;
	}

	/**
	 * @param dataInicio
	 */
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	/**
	 * @return dataFim
	 */
	public Date getDataFim() {
		return dataFim;
	}

	/**
	 * @param dataFim
	 */
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	
}
