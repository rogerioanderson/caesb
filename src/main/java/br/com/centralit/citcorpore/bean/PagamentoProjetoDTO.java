/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class PagamentoProjetoDTO implements IDto {
	private Integer idPagamentoProjeto;
	private Integer idProjeto;
	private java.sql.Date dataPagamento;
	private Double valorPagamento;
	private Double valorGlosa;
	private String detPagamento;
	private String situacao;
	private java.sql.Date dataUltAlteracao;
	private String horaUltAlteracao;
	private String usuarioUltAlteracao;
	
	private java.sql.Date dataPagamentoAtu;
	
	private Integer idMarcoPagamentoPrj;
	
	private Integer[] idTarefasParaPagamento;

	public Integer getIdPagamentoProjeto(){
		return this.idPagamentoProjeto;
	}
	public void setIdPagamentoProjeto(Integer parm){
		this.idPagamentoProjeto = parm;
	}

	public Integer getIdProjeto(){
		return this.idProjeto;
	}
	public void setIdProjeto(Integer parm){
		this.idProjeto = parm;
	}

	public java.sql.Date getDataPagamento(){
		return this.dataPagamento;
	}
	public void setDataPagamento(java.sql.Date parm){
		this.dataPagamento = parm;
	}

	public Double getValorPagamento(){
		return this.valorPagamento;
	}
	public void setValorPagamento(Double parm){
		this.valorPagamento = parm;
	}

	public String getDetPagamento(){
		return this.detPagamento;
	}
	public void setDetPagamento(String parm){
		this.detPagamento = parm;
	}

	public String getSituacao(){
		return this.situacao;
	}
	public void setSituacao(String parm){
		this.situacao = parm;
	}

	public java.sql.Date getDataUltAlteracao(){
		return this.dataUltAlteracao;
	}
	public void setDataUltAlteracao(java.sql.Date parm){
		this.dataUltAlteracao = parm;
	}

	public String getHoraUltAlteracao(){
		return this.horaUltAlteracao;
	}
	public void setHoraUltAlteracao(String parm){
		this.horaUltAlteracao = parm;
	}

	public String getUsuarioUltAlteracao(){
		return this.usuarioUltAlteracao;
	}
	public void setUsuarioUltAlteracao(String parm){
		this.usuarioUltAlteracao = parm;
	}
	public Double getValorGlosa() {
		return valorGlosa;
	}
	public void setValorGlosa(Double valorGlosa) {
		this.valorGlosa = valorGlosa;
	}
	public Integer[] getIdTarefasParaPagamento() {
		return idTarefasParaPagamento;
	}
	public void setIdTarefasParaPagamento(Integer[] idTarefasParaPagamento) {
		this.idTarefasParaPagamento = idTarefasParaPagamento;
	}
	public Integer getIdMarcoPagamentoPrj() {
		return idMarcoPagamentoPrj;
	}
	public void setIdMarcoPagamentoPrj(Integer idMarcoPagamentoPrj) {
		this.idMarcoPagamentoPrj = idMarcoPagamentoPrj;
	}
	public java.sql.Date getDataPagamentoAtu() {
		return dataPagamentoAtu;
	}
	public void setDataPagamentoAtu(java.sql.Date dataPagamentoAtu) {
		this.dataPagamentoAtu = dataPagamentoAtu;
	}

}
