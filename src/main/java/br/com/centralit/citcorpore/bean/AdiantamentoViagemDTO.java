/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Timestamp;

import br.com.citframework.dto.IDto;

public class AdiantamentoViagemDTO implements IDto {
	
	private static final long serialVersionUID = 1L;
	
	private Integer idAdiantamentoViagem;
	private Integer idResponsavel;
	private Integer idSolicitacaoServico;
	private Integer idEmpregado;
	private Timestamp dataHora;
	private Double valorTotalAdiantado;
	private String situacao;
	private String observacoes;
	private String cancelarRequisicao;
	
	private String adiantamentoViagemSerialize;
	
	private Integer idContrato;
	
	private String integranteFuncionario;
	
	private String nomeNaoFuncionario;

	public Integer getIdAdiantamentoViagem() {
		return idAdiantamentoViagem;
	}

	public void setIdAdiantamentoViagem(Integer idAdiantamentoViagem) {
		this.idAdiantamentoViagem = idAdiantamentoViagem;
	}

	public Integer getIdResponsavel() {
		return idResponsavel;
	}

	public void setIdResponsavel(Integer idResponsavel) {
		this.idResponsavel = idResponsavel;
	}

	public Integer getIdSolicitacaoServico() {
		return idSolicitacaoServico;
	}

	public void setIdSolicitacaoServico(Integer idSolicitacaoServico) {
		this.idSolicitacaoServico = idSolicitacaoServico;
	}

	public Integer getIdEmpregado() {
		return idEmpregado;
	}

	public void setIdEmpregado(Integer idEmpregado) {
		this.idEmpregado = idEmpregado;
	}

	public Timestamp getDataHora() {
		return dataHora;
	}

	public void setDataHora(Timestamp dataHora) {
		this.dataHora = dataHora;
	}

	public Double getValorTotalAdiantado() {
		return valorTotalAdiantado;
	}

	public void setValorTotalAdiantado(Double valorTotalAdiantado) {
		this.valorTotalAdiantado = valorTotalAdiantado;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public String getAdiantamentoViagemSerialize() {
		return adiantamentoViagemSerialize;
	}

	public void setAdiantamentoViagemSerialize(String adiantamentoViagemSerialize) {
		this.adiantamentoViagemSerialize = adiantamentoViagemSerialize;
	}

	public Integer getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Integer idContrato) {
		this.idContrato = idContrato;
	}

	public String getIntegranteFuncionario() {
		return integranteFuncionario;
	}

	public void setIntegranteFuncionario(String integranteFuncionario) {
		this.integranteFuncionario = integranteFuncionario;
	}

	public String getNomeNaoFuncionario() {
		return nomeNaoFuncionario;
	}

	public void setNomeNaoFuncionario(String nomeNaoFuncionario) {
		this.nomeNaoFuncionario = nomeNaoFuncionario;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the cancelarRequisicao
	 */
	public String getCancelarRequisicao() {
		return cancelarRequisicao;
	}

	/**
	 * @param cancelarRequisicao the cancelarRequisicao to set
	 */
	public void setCancelarRequisicao(String cancelarRequisicao) {
		this.cancelarRequisicao = cancelarRequisicao;
	}
}
