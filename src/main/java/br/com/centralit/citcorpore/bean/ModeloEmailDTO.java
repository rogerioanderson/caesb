/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class ModeloEmailDTO implements IDto {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3459262625002482637L;
	private Integer idModeloEmail;
	private String titulo;
	private String texto;
	private String situacao;
	private String campoSelecaoModeloTextual;
	private String div;
	private String tipoCampo;
	private String metodoExecutarVolta;
	private String identificador;

	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getCampoSelecaoModeloTextual() {
		return campoSelecaoModeloTextual;
	}
	public void setCampoSelecaoModeloTextual(String campoSelecaoModeloTextual) {
		this.campoSelecaoModeloTextual = campoSelecaoModeloTextual;
	}
	public String getDiv() {
		return div;
	}
	public void setDiv(String div) {
		this.div = div;
	}
	public String getTipoCampo() {
		return tipoCampo;
	}
	public void setTipoCampo(String tipoCampo) {
		this.tipoCampo = tipoCampo;
	}
    public String getMetodoExecutarVolta() {
        return metodoExecutarVolta;
    }
    public void setMetodoExecutarVolta(String metodoExecutarVolta) {
        this.metodoExecutarVolta = metodoExecutarVolta;
    }
	public Integer getIdModeloEmail() {
		return idModeloEmail;
	}
	public void setIdModeloEmail(Integer idModeloEmail) {
		this.idModeloEmail = idModeloEmail;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
}
