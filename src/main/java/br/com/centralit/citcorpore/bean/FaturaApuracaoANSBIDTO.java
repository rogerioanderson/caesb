/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

@SuppressWarnings("serial")
public class FaturaApuracaoANSBIDTO implements IDto {
	private Integer idFaturaApuracaoANS;
	private Integer idFatura;
	private Integer idAcordoNivelServicoContrato;
	private Double valorApurado;
	private String detalhamento;
	private Double percentualGlosa;
	private Double valorGlosa;
	private java.sql.Date dataApuracao;
	private String descricao;
	private Integer idConexaoBI;

	public Integer getIdFaturaApuracaoANS() {
		return this.idFaturaApuracaoANS;
	}

	public void setIdFaturaApuracaoANS(Integer parm) {
		this.idFaturaApuracaoANS = parm;
	}

	public Integer getIdFatura() {
		return this.idFatura;
	}

	public void setIdFatura(Integer parm) {
		this.idFatura = parm;
	}

	public Integer getIdAcordoNivelServicoContrato() {
		return this.idAcordoNivelServicoContrato;
	}

	public void setIdAcordoNivelServicoContrato(Integer parm) {
		this.idAcordoNivelServicoContrato = parm;
	}

	public Double getValorApurado() {
		return this.valorApurado;
	}

	public void setValorApurado(Double parm) {
		this.valorApurado = parm;
	}

	public String getDetalhamento() {
		return this.detalhamento;
	}

	public void setDetalhamento(String parm) {
		this.detalhamento = parm;
	}

	public Double getPercentualGlosa() {
		return this.percentualGlosa;
	}

	public void setPercentualGlosa(Double parm) {
		this.percentualGlosa = parm;
	}

	public Double getValorGlosa() {
		return this.valorGlosa;
	}

	public void setValorGlosa(Double parm) {
		this.valorGlosa = parm;
	}

	public java.sql.Date getDataApuracao() {
		return this.dataApuracao;
	}

	public void setDataApuracao(java.sql.Date parm) {
		this.dataApuracao = parm;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getIdConexaoBI() {
		return idConexaoBI;
	}

	public void setIdConexaoBI(Integer idConexaoBI) {
		this.idConexaoBI = idConexaoBI;
	}
	
}
