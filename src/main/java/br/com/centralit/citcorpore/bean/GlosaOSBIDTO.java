/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class GlosaOSBIDTO implements IDto {

	private static final long serialVersionUID = -5747818983870515883L;
	private Integer idGlosaOS;
	private Integer idOs;
	private java.sql.Date dataCriacao;
	private java.sql.Date dataUltModificacao;
	private String descricaoGlosa;
	private String ocorrencias;
	private Double percAplicado;
	private Double custoGlosa;
	private Double numeroOcorrencias;
	private Integer sequencia;
	private Integer idConexaoBI;

	public Integer getIdGlosaOS(){
		return this.idGlosaOS;
	}
	public void setIdGlosaOS(Integer parm){
		this.idGlosaOS = parm;
	}

	public Integer getIdOs(){
		return this.idOs;
	}
	public void setIdOs(Integer parm){
		this.idOs = parm;
	}

	public java.sql.Date getDataCriacao(){
		return this.dataCriacao;
	}
	public void setDataCriacao(java.sql.Date parm){
		this.dataCriacao = parm;
	}

	public java.sql.Date getDataUltModificacao(){
		return this.dataUltModificacao;
	}
	public void setDataUltModificacao(java.sql.Date parm){
		this.dataUltModificacao = parm;
	}

	public String getDescricaoGlosa(){
		return this.descricaoGlosa;
	}
	public void setDescricaoGlosa(String parm){
		this.descricaoGlosa = parm;
	}

	public String getOcorrencias(){
		return this.ocorrencias;
	}
	public void setOcorrencias(String parm){
		this.ocorrencias = parm;
	}

	public Double getPercAplicado(){
		return this.percAplicado;
	}
	public void setPercAplicado(Double parm){
		this.percAplicado = parm;
	}

	public Double getCustoGlosa(){
		return this.custoGlosa;
	}
	public void setCustoGlosa(Double parm){
		this.custoGlosa = parm;
	}

	public Double getNumeroOcorrencias(){
		return this.numeroOcorrencias;
	}
	public void setNumeroOcorrencias(Double parm){
		this.numeroOcorrencias = parm;
	}
	public Integer getSequencia() {
		return sequencia;
	}
	public void setSequencia(Integer sequencia) {
		this.sequencia = sequencia;
	}
	public Integer getIdConexaoBI() {
		return idConexaoBI;
	}
	public void setIdConexaoBI(Integer idConexaoBI) {
		this.idConexaoBI = idConexaoBI;
	}

}
