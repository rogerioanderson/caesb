/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.List;

import br.com.citframework.dto.IDto;

public class AcordoServicoContratoDTO implements IDto {

	private static final long serialVersionUID = 7878341453446988862L;
	
	private Integer idAcordoServicoContrato;
	private Integer idAcordoNivelServico;
	private Integer idServicoContrato;
	private java.sql.Date dataCriacao;
	private java.sql.Date dataInicio;
	private java.sql.Date dataFim;
	private java.sql.Date dataUltAtualiz;
	private Integer idRecurso;
	private String deleted;
	private String habilitado;

	private Integer idContrato;
	private List<ServicoContratoDTO> listaServicoContrato;

	public Integer getIdAcordoServicoContrato(){
		return this.idAcordoServicoContrato;
	}
	public void setIdAcordoServicoContrato(Integer parm){
		this.idAcordoServicoContrato = parm;
	}

	public Integer getIdAcordoNivelServico(){
		return this.idAcordoNivelServico;
	}
	public void setIdAcordoNivelServico(Integer parm){
		this.idAcordoNivelServico = parm;
	}

	public Integer getIdServicoContrato(){
		return this.idServicoContrato;
	}
	public void setIdServicoContrato(Integer parm){
		this.idServicoContrato = parm;
	}

	public java.sql.Date getDataCriacao(){
		return this.dataCriacao;
	}
	public void setDataCriacao(java.sql.Date parm){
		this.dataCriacao = parm;
	}

	public java.sql.Date getDataInicio(){
		return this.dataInicio;
	}
	public void setDataInicio(java.sql.Date parm){
		this.dataInicio = parm;
	}

	public java.sql.Date getDataFim(){
		return this.dataFim;
	}
	public void setDataFim(java.sql.Date parm){
		this.dataFim = parm;
	}

	public String getDeleted(){
		return this.deleted;
	}
	public void setDeleted(String parm){
		this.deleted = parm;
	}
	public java.sql.Date getDataUltAtualiz() {
		return dataUltAtualiz;
	}
	public void setDataUltAtualiz(java.sql.Date dataUltAtualiz) {
		this.dataUltAtualiz = dataUltAtualiz;
	}
	public Integer getIdRecurso() {
		return idRecurso;
	}
	public void setIdRecurso(Integer idRecurso) {
		this.idRecurso = idRecurso;
	}
	public Integer getIdContrato() {
		return idContrato;
	}
	public void setIdContrato(Integer idContrato) {
		this.idContrato = idContrato;
	}
	public String getHabilitado() {
		return habilitado;
	}
	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}
	public List<ServicoContratoDTO> getListaServicoContrato() {
		return listaServicoContrato;
	}
	public void setListaServicoContrato(List<ServicoContratoDTO> listaServicoContrato) {
		this.listaServicoContrato = listaServicoContrato;
	}

}
