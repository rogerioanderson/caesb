/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class BIItemDashBoardDTO implements IDto {
	private Integer idItemDashBoard;
	private Integer idDashBoard;
	private Integer idConsulta;
	private String titulo;
	private Integer posicao;
	private Integer itemTop;
	private Integer itemLeft;
	private Integer itemWidth;
	private Integer itemHeight;
	private String parmsSubst;

	public Integer getIdItemDashBoard(){
		return this.idItemDashBoard;
	}
	public void setIdItemDashBoard(Integer parm){
		this.idItemDashBoard = parm;
	}

	public Integer getIdDashBoard(){
		return this.idDashBoard;
	}
	public void setIdDashBoard(Integer parm){
		this.idDashBoard = parm;
	}

	public Integer getIdConsulta(){
		return this.idConsulta;
	}
	public void setIdConsulta(Integer parm){
		this.idConsulta = parm;
	}

	public String getTitulo(){
		return this.titulo;
	}
	public void setTitulo(String parm){
		this.titulo = parm;
	}

	public Integer getPosicao(){
		return this.posicao;
	}
	public void setPosicao(Integer parm){
		this.posicao = parm;
	}

	public Integer getItemTop(){
		return this.itemTop;
	}
	public void setItemTop(Integer parm){
		this.itemTop = parm;
	}

	public Integer getItemLeft(){
		return this.itemLeft;
	}
	public void setItemLeft(Integer parm){
		this.itemLeft = parm;
	}

	public Integer getItemWidth(){
		return this.itemWidth;
	}
	public void setItemWidth(Integer parm){
		this.itemWidth = parm;
	}

	public Integer getItemHeight(){
		return this.itemHeight;
	}
	public void setItemHeight(Integer parm){
		this.itemHeight = parm;
	}
	public String getParmsSubst() {
		return parmsSubst;
	}
	public void setParmsSubst(String parmsSubst) {
		this.parmsSubst = parmsSubst;
	}

}
