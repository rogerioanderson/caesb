/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Collection;
import java.util.List;

import br.com.citframework.dto.IDto;

public class BIDashBoardDTO implements IDto {
	private Integer idDashBoard;
	private String nomeDashBoard;
	private String tipo;
	private String identificacao;
	private String situacao;
	private Integer idUsuario;
	private String parametros;
	private String naoAtualizBase;
	private Integer tempoRefresh;
	
	private Collection colItens;
	private List listParameters;
	
	private String parmOK = "false";
	private boolean parametersPreenchidos = false;	

	public Integer getIdDashBoard(){
		return this.idDashBoard;
	}
	public void setIdDashBoard(Integer parm){
		this.idDashBoard = parm;
	}

	public String getNomeDashBoard(){
		return this.nomeDashBoard;
	}
	public void setNomeDashBoard(String parm){
		this.nomeDashBoard = parm;
	}

	public String getTipo(){
		return this.tipo;
	}
	public void setTipo(String parm){
		this.tipo = parm;
	}

	public String getIdentificacao(){
		return this.identificacao;
	}
	public void setIdentificacao(String parm){
		this.identificacao = parm;
	}

	public String getSituacao(){
		return this.situacao;
	}
	public void setSituacao(String parm){
		this.situacao = parm;
	}

	public Integer getIdUsuario(){
		return this.idUsuario;
	}
	public void setIdUsuario(Integer parm){
		this.idUsuario = parm;
	}
	public Collection getColItens() {
		return colItens;
	}
	public void setColItens(Collection colItens) {
		this.colItens = colItens;
	}
	public String getParametros() {
		return parametros;
	}
	public void setParametros(String parametros) {
		this.parametros = parametros;
	}
	public String getNaoAtualizBase() {
		return naoAtualizBase;
	}
	public void setNaoAtualizBase(String naoAtualizBase) {
		this.naoAtualizBase = naoAtualizBase;
	}
	public List getListParameters() {
		return listParameters;
	}
	public void setListParameters(List listParameters) {
		this.listParameters = listParameters;
	}
	public String getParmOK() {
		return parmOK;
	}
	public void setParmOK(String parmOK) {
		setParametersPreenchidos(false);
		if (parmOK != null){
			if (parmOK.equalsIgnoreCase("S") || parmOK.equalsIgnoreCase("true")){
				setParametersPreenchidos(true);
			}
		}
		this.parmOK = parmOK;
	}
	public boolean isParametersPreenchidos() {
		return parametersPreenchidos;
	}
	public boolean getParametersPreenchidos() {
		return parametersPreenchidos;
	}
	public void setParametersPreenchidos(boolean parametersPreenchidos) {
		this.parametersPreenchidos = parametersPreenchidos;
	}
	public Integer getTempoRefresh() {
		return tempoRefresh;
	}
	public void setTempoRefresh(Integer tempoRefresh) {
		this.tempoRefresh = tempoRefresh;
	}

}
