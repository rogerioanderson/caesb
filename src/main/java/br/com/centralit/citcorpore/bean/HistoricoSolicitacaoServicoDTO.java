/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;


import java.sql.Timestamp;

import br.com.citframework.dto.IDto;

public class HistoricoSolicitacaoServicoDTO implements IDto {

	private static final long serialVersionUID = 1L;
	
	private Integer idHistoricoSolicitacao;
	private Integer idSolicitacaoServico;
	private Integer idResponsavelAtual;
	private Integer idGrupo;
	private Integer idOcorrencia;
	private Timestamp dataCriacao;
	private Timestamp dataFinal;
	private Double horasTrabalhadas;
	private Integer idServicoContrato;
	private Integer idCalendario;
	private String status;
//	private String nomeUsuario;
//	private String nomeGrupo;
//	private Double soma;
	
	public Integer getIdHistoricoSolicitacao() {
		return idHistoricoSolicitacao;
	}
	public void setIdHistoricoSolicitacao(Integer idHistoricoSolicitacao) {
		this.idHistoricoSolicitacao = idHistoricoSolicitacao;
	}

	public Integer getIdResponsavelAtual() {
		return idResponsavelAtual;
	}
	public void setIdResponsavelAtual(Integer idResponsavelAtual) {
		this.idResponsavelAtual = idResponsavelAtual;
	}
	public Integer getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(Integer idGrupo) {
		this.idGrupo = idGrupo;
	}
	public Integer getIdOcorrencia() {
		return idOcorrencia;
	}
	public void setIdOcorrencia(Integer idOcorrencia) {
		this.idOcorrencia = idOcorrencia;
	}
	public Double getHorasTrabalhadas() {
		return horasTrabalhadas;
	}
	public void setHorasTrabalhadas(Double horasTrabalhadas) {
		this.horasTrabalhadas = horasTrabalhadas;
	}
	public Integer getIdSolicitacaoServico() {
		return idSolicitacaoServico;
	}
	public void setIdSolicitacaoServico(Integer idSolicitacaoServico) {
		this.idSolicitacaoServico = idSolicitacaoServico;
	}
	public Timestamp getDataCriacao() {
		return dataCriacao;
	}
	public void setDataCriacao(Timestamp timestamp) {
		this.dataCriacao = timestamp;
	}
	public Timestamp getDataFinal() {
		return dataFinal;
	}
	public void setDataFinal(Timestamp dataFinal) {
		this.dataFinal = dataFinal;
	}
	public Integer getIdServicoContrato() {
		return idServicoContrato;
	}
	public void setIdServicoContrato(Integer idServicoContrato) {
		this.idServicoContrato = idServicoContrato;
	}
	public Integer getIdCalendario() {
		return idCalendario;
	}
	public void setIdCalendario(Integer idCalendario) {
		this.idCalendario = idCalendario;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
//	public String getNomeUsuario() {
//		return nomeUsuario;
//	}
//	public void setNomeUsuario(String nomeUsuario) {
//		this.nomeUsuario = nomeUsuario;
//	}
//	public Double getSoma() {
//		return soma;
//	}
//	public void setSoma(Double soma) {
//		this.soma = soma;
//	}
//	public String getNomeGrupo() {
//		return nomeGrupo;
//	}
//	public void setNomeGrupo(String nomeGrupo) {
//		this.nomeGrupo = nomeGrupo;
//	}
}
