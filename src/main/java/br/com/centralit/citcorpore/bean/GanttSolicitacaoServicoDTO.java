/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * 
 */
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

/**
 * @author valdoilo.damasceno
 * 
 */
public class GanttSolicitacaoServicoDTO implements IDto {

    private static final long serialVersionUID = 4420655477272385856L;

    private String name;

    private String desc;

    private ValueGanttDTO values;

    private String tipoDemandaServico;

    private String idGruposSeguranca;
    private String apenasMeus;
    
    private Date dataInicio;
    private Date dataFim;
    
    private String situacao;

    /**
     * @return valor do atributo name.
     */
    public String getName() {
	return name;
    }

    /**
     * Define valor do atributo name.
     * 
     * @param name
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * @return valor do atributo desc.
     */
    public String getDesc() {
	return desc;
    }

    /**
     * Define valor do atributo desc.
     * 
     * @param desc
     */
    public void setDesc(String desc) {
	this.desc = desc;
    }

    /**
     * @return valor do atributo values.
     */
    public ValueGanttDTO getValues() {
	return values;
    }

    /**
     * Define valor do atributo values.
     * 
     * @param values
     */
    public void setValues(ValueGanttDTO values) {
	this.values = values;
    }

    /**
     * @return valor do atributo tipoDemandaServico.
     */
    public String getTipoDemandaServico() {
	return tipoDemandaServico;
    }

    /**
     * Define valor do atributo tipoDemandaServico.
     * 
     * @param tipoDemandaServico
     */
    public void setTipoDemandaServico(String tipoDemandaServico) {
	this.tipoDemandaServico = tipoDemandaServico;
    }

    /**
     * @return valor do atributo idGruposSeguranca.
     */
    public String getIdGruposSeguranca() {
	return idGruposSeguranca;
    }

    /**
     * Define valor do atributo idGruposSeguranca.
     * 
     * @param idGruposSeguranca
     */
    public void setIdGruposSeguranca(String idGruposSeguranca) {
	this.idGruposSeguranca = idGruposSeguranca;
    }

    public String getApenasMeus() {
        return apenasMeus;
    }

    public void setApenasMeus(String apenasMeus) {
        this.apenasMeus = apenasMeus;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

}
