/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

@SuppressWarnings("serial")
public class GerenciamentoMudancasDTO implements IDto {
	private Integer idFluxo;
	private Integer idTarefa;
	private Integer idRequisicao;
	private String acaoFluxo;
	
	private String numeroContratoSel;
	private String idSolicitacaoSel;
	private String atribuidaCompartilhada;
	
	private String idRequisicaoSel;
	
	public Integer getIdFluxo() {
		return idFluxo;
	}
	public void setIdFluxo(Integer idFluxo) {
		this.idFluxo = idFluxo;
	}
	public Integer getIdTarefa() {
		return idTarefa;
	}
	public void setIdTarefa(Integer idTarefa) {
		this.idTarefa = idTarefa;
	}
	public String getAcaoFluxo() {
		return acaoFluxo;
	}
	public void setAcaoFluxo(String acaoFluxo) {
		this.acaoFluxo = acaoFluxo;
	}
	public String getNumeroContratoSel() {
		return numeroContratoSel;
	}
	public void setNumeroContratoSel(String numeroContratoSel) {
		this.numeroContratoSel = numeroContratoSel;
	}
	public String getIdSolicitacaoSel() {
		return idSolicitacaoSel;
	}
	public void setIdSolicitacaoSel(String idSolicitacaoSel) {
		this.idSolicitacaoSel = idSolicitacaoSel;
	}

	public Integer getIdRequisicao() {
		return idRequisicao;
	}
	public void setIdRequisicao(Integer idRequisicao) {
		this.idRequisicao = idRequisicao;
	}
	public String getAtribuidaCompartilhada() {
	    return atribuidaCompartilhada;
	}
	public void setAtribuidaCompartilhada(String atribuidaCompartilhada) {
	    this.atribuidaCompartilhada = atribuidaCompartilhada;
	}
	/**
	 * @return the idRequisicaoSel
	 */
	public String getIdRequisicaoSel() {
		return idRequisicaoSel;
	}
	/**
	 * @param idRequisicaoSel the idRequisicaoSel to set
	 */
	public void setIdRequisicaoSel(String idRequisicaoSel) {
		this.idRequisicaoSel = idRequisicaoSel;
	}

}
