/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;
import br.com.citframework.push.DevicePlatformType;

/**
 * DTO para persist�ncia dos dados de aloca��o de um device a um atendente
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 15/11/2014
 */
public class AssociacaoDeviceAtendenteDTO implements IDto {

    private static final long serialVersionUID = 3025354135360257061L;

    private Integer id;
    private Integer idUsuario;
    private String token;
    private Integer active;
    private String connection;
    private Integer devicePlatform;
    private String nomeAtendente;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(final Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(final String connection) {
        this.connection = connection;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(final Integer active) {
        this.active = active;
    }

    public boolean isActive() {
        return this.getActive() == 1;
    }

    public Integer getDevicePlatform() {
        return devicePlatform;
    }

    public void setDevicePlatform(final Integer devicePlatform) {
        this.devicePlatform = devicePlatform;
    }

    public DevicePlatformType getDevicePlatformType() {
        if (this.getDevicePlatform() != null) {
            return DevicePlatformType.fromId(this.getDevicePlatform());
        }
        return null;
    }

    public void setDevicePlatformType(final DevicePlatformType devicePlatform) {
        if (devicePlatform != null) {
            this.setDevicePlatform(devicePlatform.getId());
        }
    }

    public String getNomeAtendente() {
        return nomeAtendente;
    }

    public void setNomeAtendente(final String nomeAtendente) {
        this.nomeAtendente = nomeAtendente;
    }

}
