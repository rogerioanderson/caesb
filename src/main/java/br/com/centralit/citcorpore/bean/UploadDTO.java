/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.centralit.citged.bean.ControleGEDDTO;
import br.com.citframework.dto.IDto;

public class UploadDTO implements IDto{
	private String id;
	private String nameFile;
	private String descricao;
	private String path;
	private String temporario;
	private String situacao;
	private String notaTecnicaUpload;
	private Integer idControleGED;
	private String versao;
	private String idMudanca;
	private String caminhoRelativo;
	
	/*Atributo criado para implementa��o do upload por servi�o dentro da grid, referente ao idServico*/
	private Integer idLinhaPai;
	
	private ControleGEDDTO controleGEDDto;
	
	public UploadDTO() {
	
	}
	
	public UploadDTO(String nameFile, String path) {
		this.nameFile = nameFile;
		this.path = path;
	}
	
	public Integer getIdControleGED() {
		return idControleGED;
	}
	public void setIdControleGED(Integer idControleGED) {
		this.idControleGED = idControleGED;
	}
	public String getNameFile() {
		return nameFile;
	}
	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getTemporario() {
		return temporario;
	}
	public void setTemporario(String temporario) {
		this.temporario = temporario;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getNotaTecnicaUpload() {
		return notaTecnicaUpload;
	}
	public void setNotaTecnicaUpload(String notaTecnicaUpload) {
		this.notaTecnicaUpload = notaTecnicaUpload;
	}

	public ControleGEDDTO getControleGEDDto() {
		return controleGEDDto;
	}

	public void setControleGEDDto(ControleGEDDTO controleGEDDto) {
		this.controleGEDDto = controleGEDDto;
	}

	public String getVersao() {
		return versao;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public String getIdMudanca() {
		return idMudanca;
	}

	public void setIdMudanca(String idMudanca) {
		this.idMudanca = idMudanca;
	}

	public String getCaminhoRelativo() {
		return caminhoRelativo;
	}

	public void setCaminhoRelativo(String caminhoRelativo) {
		this.caminhoRelativo = caminhoRelativo;
	}

	/**
	 * @return the idLinhaPai
	 */
	public Integer getIdLinhaPai() {
		return idLinhaPai;
	}

	/**
	 * @param idLinhaPai the idLinhaPai to set
	 */
	public void setIdLinhaPai(Integer idLinhaPai) {
		this.idLinhaPai = idLinhaPai;
	}
	
}
