/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Collection;

import br.com.centralit.citcorpore.bpm.negocio.ExecucaoSolicitacao;
import br.com.citframework.dto.IDto;

public class ProcessoNegocioDTO implements IDto {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1540754378060788134L;
	private Integer idProcessoNegocio;
	private Integer idGrupoExecutor;
	private Integer idGrupoAdministrador;
	private String nomeProcessoNegocio;
	private String permissaoSolicitacao;
	private Double percDispensaNovaAprovacao;
	private String permiteAprovacaoNivelInferior;
	private String alcadaPrimeiroNivel;
	
	private Integer[] idTipoFluxo;
	
	private Collection<ProcessoNivelAutoridadeDTO> colAutoridades;
	private Collection<ExecucaoSolicitacao> colExecucaoSolicitacao;
	private Collection<LimiteAprovacaoDTO> colLimitesAprovacao;
	
	private NivelAutoridadeDTO nivelAutoridadeDto;

	public Integer getIdProcessoNegocio(){
		return this.idProcessoNegocio;
	}
	public void setIdProcessoNegocio(Integer parm){
		this.idProcessoNegocio = parm;
	}

	public Integer getIdGrupoExecutor(){
		return this.idGrupoExecutor;
	}
	public void setIdGrupoExecutor(Integer parm){
		this.idGrupoExecutor = parm;
	}

	public Integer getIdGrupoAdministrador(){
		return this.idGrupoAdministrador;
	}
	public void setIdGrupoAdministrador(Integer parm){
		this.idGrupoAdministrador = parm;
	}

	public String getNomeProcessoNegocio(){
		return this.nomeProcessoNegocio;
	}
	public void setNomeProcessoNegocio(String parm){
		this.nomeProcessoNegocio = parm;
	}

	public String getPermissaoSolicitacao(){
		return this.permissaoSolicitacao;
	}
	public void setPermissaoSolicitacao(String parm){
		this.permissaoSolicitacao = parm;
	}

	public Double getPercDispensaNovaAprovacao(){
		return this.percDispensaNovaAprovacao;
	}
	public void setPercDispensaNovaAprovacao(Double parm){
		this.percDispensaNovaAprovacao = parm;
	}

	public String getPermiteAprovacaoNivelInferior(){
		return this.permiteAprovacaoNivelInferior;
	}
	public void setPermiteAprovacaoNivelInferior(String parm){
		this.permiteAprovacaoNivelInferior = parm;
	}
	public Collection<ProcessoNivelAutoridadeDTO> getColAutoridades() {
		return colAutoridades;
	}
	public void setColAutoridades(
			Collection<ProcessoNivelAutoridadeDTO> colAutoridades) {
		this.colAutoridades = colAutoridades;
	}
	public Integer[] getIdTipoFluxo() {
		return idTipoFluxo;
	}
	public void setIdTipoFluxo(Integer[] idTipoFluxo) {
		this.idTipoFluxo = idTipoFluxo;
	}
	public NivelAutoridadeDTO getNivelAutoridadeDto() {
		return nivelAutoridadeDto;
	}
	public void setNivelAutoridadeDto(NivelAutoridadeDTO nivelAutoridadeDto) {
		this.nivelAutoridadeDto = nivelAutoridadeDto;
	}
	public Collection<ExecucaoSolicitacao> getColExecucaoSolicitacao() {
		return colExecucaoSolicitacao;
	}
	public void setColExecucaoSolicitacao(
			Collection<ExecucaoSolicitacao> colExecucaoSolicitacao) {
		this.colExecucaoSolicitacao = colExecucaoSolicitacao;
	}
	public Collection<LimiteAprovacaoDTO> getColLimitesAprovacao() {
		return colLimitesAprovacao;
	}
	public void setColLimitesAprovacao(
			Collection<LimiteAprovacaoDTO> colLimitesAprovacao) {
		this.colLimitesAprovacao = colLimitesAprovacao;
	}
	public String getAlcadaPrimeiroNivel() {
		return alcadaPrimeiroNivel;
	}
	public void setAlcadaPrimeiroNivel(String alcadaPrimeiroNivel) {
		this.alcadaPrimeiroNivel = alcadaPrimeiroNivel;
	}

}
