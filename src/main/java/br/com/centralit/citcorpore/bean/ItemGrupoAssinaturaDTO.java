/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

public class ItemGrupoAssinaturaDTO implements IDto{

    private static final long serialVersionUID = -6639560890633897592L;

    private Integer idItemGrupoAssinatura;
    private Integer idGrupoAssinatura;
    private Integer idAssinatura;
    private Integer ordem;
    private Date dataInicio;
    private Date dataFim;

    //Campos auxiliares para apresenta��o na tela
    private String nomeResponsavel;
    private String papel;
    private String fase;

    public Integer getIdItemGrupoAssinatura() {
	return this.idItemGrupoAssinatura;
    }
    public void setIdItemGrupoAssinatura(Integer idItemGrupoAssinatura) {
	this.idItemGrupoAssinatura = idItemGrupoAssinatura;
    }
    public Integer getIdGrupoAssinatura() {
	return this.idGrupoAssinatura;
    }
    public void setIdGrupoAssinatura(Integer idGrupoAssinatura) {
	this.idGrupoAssinatura = idGrupoAssinatura;
    }
    public Integer getIdAssinatura() {
	return this.idAssinatura;
    }
    public void setIdAssinatura(Integer idAssinatura) {
	this.idAssinatura = idAssinatura;
    }
    public Integer getOrdem() {
	return this.ordem;
    }
    public void setOrdem(Integer ordem) {
	this.ordem = ordem;
    }
    public Date getDataInicio() {
	return this.dataInicio;
    }
    public void setDataInicio(Date dataInicio) {
	this.dataInicio = dataInicio;
    }
    public Date getDataFim() {
	return this.dataFim;
    }
    public void setDataFim(Date dataFim) {
	this.dataFim = dataFim;
    }

    public String getNomeResponsavel() {
	return this.nomeResponsavel;
    }

    public void setNomeResponsavel(String nomeResponsavel) {
	this.nomeResponsavel = nomeResponsavel;
    }

    public String getPapel() {
	return this.papel;
    }

    public void setPapel(String papel) {
	this.papel = papel;
    }

    public String getFase() {
	return this.fase;
    }

    public void setFase(String fase) {
	this.fase = fase;
    }

}
