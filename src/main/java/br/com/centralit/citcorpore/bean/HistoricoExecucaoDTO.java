/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import br.com.centralit.citajax.util.JavaScriptUtil;
import br.com.citframework.dto.IDto;
import br.com.citframework.util.UtilDatas;

public class HistoricoExecucaoDTO implements IDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 175188654082235948L;
	private Integer idHistorico;
	private Integer idExecucao;
	private Date data;
	private String situacao;
	private Integer idEmpregadoExecutor;
	private String detalhamento;
	private Long hora;
	private String nomeEmpregado;
	
	private Integer idDemanda;
	
	public String getDataStr() {
		return UtilDatas.dateToSTR(data);
	}	
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Integer getIdEmpregadoExecutor() {
		return idEmpregadoExecutor;
	}
	public void setIdEmpregadoExecutor(Integer idEmpregadoExecutor) {
		this.idEmpregadoExecutor = idEmpregadoExecutor;
	}
	public Integer getIdExecucao() {
		return idExecucao;
	}
	public void setIdExecucao(Integer idExecucao) {
		this.idExecucao = idExecucao;
	}
	public Integer getIdHistorico() {
		return idHistorico;
	}
	public void setIdHistorico(Integer idHistorico) {
		this.idHistorico = idHistorico;
	}
	public String getSituacaoDesc() {
		if (this.situacao == null) return "";
		if (this.situacao.equalsIgnoreCase("N")) return "N�o Iniciada";
		if (this.situacao.equalsIgnoreCase("I")) return "Em Execu��o";
		if (this.situacao.equalsIgnoreCase("F")) return "Finalizada";
		if (this.situacao.equalsIgnoreCase("C")) return "Paralisada - Aguard. Cliente";
		if (this.situacao.equalsIgnoreCase("P")) return "Paralisada - Interno";
		if (this.situacao.equalsIgnoreCase("T")) return "Transferida";
		return situacao;
	}	
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getDetalhamentoConv() {
		return JavaScriptUtil.escapeJavaScript(detalhamento);
	}	
	public String getDetalhamento() {
		return detalhamento;
	}
	public void setDetalhamento(String detalhamento) {
		this.detalhamento = detalhamento;
	}
	public String getHoraStr() {
		if (hora == null) return "";
		Timestamp t = new Timestamp(hora.longValue());
		
    	SimpleDateFormat format = new SimpleDateFormat("HH:mm");
    	String s = format.format(t);
		return s;
	}	
	public String getNomeEmpregado() {
		return nomeEmpregado;
	}
	public void setNomeEmpregado(String nomeEmpregado) {
		this.nomeEmpregado = nomeEmpregado;
	}
	public Integer getIdDemanda() {
		return idDemanda;
	}
	public void setIdDemanda(Integer idDemanda) {
		this.idDemanda = idDemanda;
	}
	public Long getHora() {
		return hora;
	}
	public void setHora(Long hora) {
		this.hora = hora;
	}

}
