/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

@SuppressWarnings("serial")
public class JustificativaProblemaDTO implements IDto {

	
	private Integer idJustificativaProblema ;
	
	private String   descricaoProblema  ;
	
	private  String suspensao ;
	
	private String  situacao ;
	
	private String  aprovacao ;
	
	private  String  deleted  ;

	
	/**
	 * @return the idJustificativaMudanca
	 */
	public Integer getIdJustificativaProblema() {
		return idJustificativaProblema;
	}

	/**
	 * @param idJustificativaMudanca the idJustificativaMudanca to set
	 */
	public void setIdJustificativaProblema(Integer idJustificativaProblema) {
		this.idJustificativaProblema = idJustificativaProblema;
	}
	
	/**
	 * @return the descricaoJustificativa
	 */
	public String getDescricaoProblema() {
		return descricaoProblema;
	}


	/**
	 * @param descricaoJustificativa the descricaoJustificativa to set
	 */
	public void setDescricaoProblema(String descricaoProblema) {
		this.descricaoProblema = descricaoProblema;
	}
	
	/**
	 * @return the suspensao
	 */
	public String getSuspensao() {
		return suspensao;
	}

	/**
	 * @param suspensao the suspensao to set
	 */
	public void setSuspensao(String suspensao) {
		this.suspensao = suspensao;
	}

	/**
	 * @return the situacao
	 */
	public String getSituacao() {
		return situacao;
	}

	/**
	 * @param situacao the situacao to set
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	/**
	 * @return the aprovacao
	 */
	public String getAprovacao() {
		return aprovacao;
	}

	/**
	 * @param aprovacao the aprovacao to set
	 */
	public void setAprovacao(String aprovacao) {
		this.aprovacao = aprovacao;
	}

	/**
	 * @return the deleted
	 */
	public String getDeleted() {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

}
