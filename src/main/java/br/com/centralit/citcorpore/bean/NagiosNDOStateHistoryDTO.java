/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class NagiosNDOStateHistoryDTO implements IDto {
	private Integer statehistory_id;
	private Integer instance_id;
	private java.sql.Timestamp state_time;
	private Integer state_time_usec;
	private Integer object_id;
	private Integer state_change;
	private Integer state;
	private Integer state_type;
	private Integer current_check_attempt;
	private Integer max_check_attempts;
	private Integer last_state;
	private Integer last_hard_state;
	private String output;
	private String long_output;

	public Integer getStatehistory_id(){
		return this.statehistory_id;
	}
	public void setStatehistory_id(Integer parm){
		this.statehistory_id = parm;
	}

	public Integer getInstance_id(){
		return this.instance_id;
	}
	public void setInstance_id(Integer parm){
		this.instance_id = parm;
	}

	public java.sql.Timestamp getState_time(){
		return this.state_time;
	}
	public void setState_time(java.sql.Timestamp parm){
		this.state_time = parm;
	}

	public Integer getState_time_usec(){
		return this.state_time_usec;
	}
	public void setState_time_usec(Integer parm){
		this.state_time_usec = parm;
	}

	public Integer getObject_id(){
		return this.object_id;
	}
	public void setObject_id(Integer parm){
		this.object_id = parm;
	}

	public Integer getState_change(){
		return this.state_change;
	}
	public void setState_change(Integer parm){
		this.state_change = parm;
	}

	public Integer getState(){
		return this.state;
	}
	public void setState(Integer parm){
		this.state = parm;
	}

	public Integer getState_type(){
		return this.state_type;
	}
	public void setState_type(Integer parm){
		this.state_type = parm;
	}

	public Integer getCurrent_check_attempt(){
		return this.current_check_attempt;
	}
	public void setCurrent_check_attempt(Integer parm){
		this.current_check_attempt = parm;
	}

	public Integer getMax_check_attempts(){
		return this.max_check_attempts;
	}
	public void setMax_check_attempts(Integer parm){
		this.max_check_attempts = parm;
	}

	public Integer getLast_state(){
		return this.last_state;
	}
	public void setLast_state(Integer parm){
		this.last_state = parm;
	}

	public Integer getLast_hard_state(){
		return this.last_hard_state;
	}
	public void setLast_hard_state(Integer parm){
		this.last_hard_state = parm;
	}

	public String getOutput(){
		return this.output;
	}
	public void setOutput(String parm){
		this.output = parm;
	}

	public String getLong_output(){
		return this.long_output;
	}
	public void setLong_output(String parm){
		this.long_output = parm;
	}
	public String getStatus(){
		if (this.getState() != null){
			if (this.getState().intValue() == 2){ //CRITICAL
				return "DOWN";
			}else {
				return "UP";
			}
		}
		return "";
	}	
	
}
