/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.sql.Date;

import br.com.citframework.dto.IDto;

public class ItemPrestacaoContasViagemDTO implements IDto {

	private Integer idItemPrestContasViagem;
	private Integer idPrestacaoContasViagem;
	private Integer idItemDespesaViagem;
	private Integer idFornecedor;
	private Date data;
	private String nomeFornecedor;
	private String numeroDocumento;
	private String descricao;
    private Double valor;
    private String valorAux;
    
	private static final long serialVersionUID = 1L;
    

	public Integer getIdItemPrestContasViagem() {
		return idItemPrestContasViagem;
	}
	public void setIdItemPrestContasViagem(Integer idItemPrestContasViagem) {
		this.idItemPrestContasViagem = idItemPrestContasViagem;
	}
	public Integer getIdPrestacaoContasViagem() {
		return idPrestacaoContasViagem;
	}
	public void setIdPrestacaoContasViagem(Integer idPrestacaoContasViagem) {
		this.idPrestacaoContasViagem = idPrestacaoContasViagem;
	}
	public Integer getIdItemDespesaViagem() {
		return idItemDespesaViagem;
	}
	public void setIdItemDespesaViagem(Integer idItemDespesaViagem) {
		this.idItemDespesaViagem = idItemDespesaViagem;
	}
	public Integer getIdFornecedor() {
		return idFornecedor;
	}
	public void setIdFornecedor(Integer idFornecedor) {
		this.idFornecedor = idFornecedor;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getNomeFornecedor() {
		return nomeFornecedor;
	}
	public void setNomeFornecedor(String nomeFornecedor) {
		this.nomeFornecedor = nomeFornecedor;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
		this.valorAux = valor.toString();
	}
	public String getValorAux() {
		return valorAux;
	}
	public void setValorAux(String valorAux) {
		this.valorAux = valorAux;
	}
}
