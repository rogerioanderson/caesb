/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import br.com.citframework.dto.IDto;

public class TimeSheetProjetoDTO implements IDto {
	private Integer idTimeSheetProjeto;
	private Integer idRecursoTarefaLinBaseProj;
	private java.sql.Timestamp dataHoraReg;
	private java.sql.Date data;
	private String hora;
	private Double qtdeHoras;
	private Double custo;
	private String detalhamento;
	private Double percExecutado;
	
	private Integer idLinhaBaseProjeto;

	public Integer getIdTimeSheetProjeto(){
		return this.idTimeSheetProjeto;
	}
	public void setIdTimeSheetProjeto(Integer parm){
		this.idTimeSheetProjeto = parm;
	}

	public Integer getIdRecursoTarefaLinBaseProj(){
		return this.idRecursoTarefaLinBaseProj;
	}
	public void setIdRecursoTarefaLinBaseProj(Integer parm){
		this.idRecursoTarefaLinBaseProj = parm;
	}

	public java.sql.Timestamp getDataHoraReg(){
		return this.dataHoraReg;
	}
	public void setDataHoraReg(java.sql.Timestamp parm){
		this.dataHoraReg = parm;
	}

	public java.sql.Date getData(){
		return this.data;
	}
	public void setData(java.sql.Date parm){
		this.data = parm;
	}

	public String getHora(){
		return this.hora;
	}
	public void setHora(String parm){
		this.hora = parm;
	}

	public Double getQtdeHoras(){
		return this.qtdeHoras;
	}
	public void setQtdeHoras(Double parm){
		this.qtdeHoras = parm;
	}

	public Double getCusto(){
		return this.custo;
	}
	public void setCusto(Double parm){
		this.custo = parm;
	}

	public String getDetalhamento(){
		return this.detalhamento;
	}
	public void setDetalhamento(String parm){
		this.detalhamento = parm;
	}
	public Double getPercExecutado() {
		return percExecutado;
	}
	public void setPercExecutado(Double percExecutado) {
		this.percExecutado = percExecutado;
	}
	public Integer getIdLinhaBaseProjeto() {
		return idLinhaBaseProjeto;
	}
	public void setIdLinhaBaseProjeto(Integer idLinhaBaseProjeto) {
		this.idLinhaBaseProjeto = idLinhaBaseProjeto;
	}

}
