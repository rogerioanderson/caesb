/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

//Criado por Bruno.Aquino

import java.sql.Date;
import java.util.Collection;

import br.com.citframework.dto.IDto;

public class RelatorioMudancaItemConfiguracaoDTO implements IDto {

	private static final long serialVersionUID = 1L;

	// Atributos para a mudan�a

	private Integer idNumeroMudanca;
	private String tituloMudanca;
	private String grupoMudanca;
	private Date dataMudanca;
	private String descricaoProblemaMudanca;

	// Atributos para o item de configura��o
	private Integer idItemConfig;
	private String nomeItem;
	// Atributos par aos parametros da tela
	private Date dataInicio;
	private Date dataFim;
	private String proprietario;
	private String formatoArquivoRelatorio;
	private Integer idContrato;
	private String contrato;
	private Collection<ItemConfiguracaoDTO> listaItensConfiguracao;

	public Collection<ItemConfiguracaoDTO> getListaItensConfiguracao() {
		return listaItensConfiguracao;
	}

	public void setListaItensConfiguracao(Collection<ItemConfiguracaoDTO> list) {
		this.listaItensConfiguracao = list;
	}

	public String getTituloMudanca() {
		return tituloMudanca;
	}

	public void setTituloMudanca(String tituloMudanca) {
		this.tituloMudanca = tituloMudanca;
	}

	public String getGrupoMudanca() {
		return grupoMudanca;
	}

	public void setGrupoMudanca(String grupoMudanca) {
		this.grupoMudanca = grupoMudanca;
	}

	public Date getDataMudanca() {
		return dataMudanca;
	}

	public void setDataMudanca(Date dataMudanca) {
		this.dataMudanca = dataMudanca;
	}

	public String getDescricaoProblemaMudanca() {
		return descricaoProblemaMudanca;
	}

	public void setDescricaoProblemaMudanca(String descricaoProblemaMudanca) {
		this.descricaoProblemaMudanca = descricaoProblemaMudanca;
	}

	public String getNomeItem() {
		return nomeItem;
	}

	public void setNomeItem(String nomeItem) {
		this.nomeItem = nomeItem;
	}

	public Integer getIdNumeroMudanca() {
		return idNumeroMudanca;
	}

	public void setIdNumeroMudanca(Integer idNumeroMudanca) {
		this.idNumeroMudanca = idNumeroMudanca;
	}

	public Integer getIdItemConfig() {
		return idItemConfig;
	}

	public void setIdItemConfig(Integer idItemConfig) {
		this.idItemConfig = idItemConfig;
	}

	public Integer getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Integer idContrato) {
		this.idContrato = idContrato;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public String getProprietario() {
		return proprietario;
	}

	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}

	public String getFormatoArquivoRelatorio() {
		return formatoArquivoRelatorio;
	}

	public void setFormatoArquivoRelatorio(String formatoArquivoRelatorio) {
		this.formatoArquivoRelatorio = formatoArquivoRelatorio;
	}

}
