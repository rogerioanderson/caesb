/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.bean;

import java.util.Collection;

import br.com.citframework.dto.IDto;

public class LinhaBaseProjetoDTO implements IDto {
	public static final String ATIVO = "A";
	public static final String EMEXECUCAO = "E";
	public static final String INATIVO = "I";
	
	private Integer idLinhaBaseProjeto;
	private Integer idProjeto;
	private java.sql.Date dataLinhaBase;
	private String horaLinhaBase;
	private String situacao;
	private java.sql.Date dataUltAlteracao;
	private String horaUltAlteracao;
	private String usuarioUltAlteracao;
	
	private String justificativaMudanca;
	private java.sql.Date dataSolMudanca;
	private String horaSolMudanca;
	private String usuarioSolMudanca;	
	
	private Integer idLinhaBaseProjetoUpdate;
	
	private Collection colTarefas;

	public Integer getIdLinhaBaseProjeto(){
		return this.idLinhaBaseProjeto;
	}
	public void setIdLinhaBaseProjeto(Integer parm){
		this.idLinhaBaseProjeto = parm;
	}

	public Integer getIdProjeto(){
		return this.idProjeto;
	}
	public void setIdProjeto(Integer parm){
		this.idProjeto = parm;
	}

	public java.sql.Date getDataLinhaBase(){
		return this.dataLinhaBase;
	}
	public void setDataLinhaBase(java.sql.Date parm){
		this.dataLinhaBase = parm;
	}

	public String getHoraLinhaBase(){
		return this.horaLinhaBase;
	}
	public void setHoraLinhaBase(String parm){
		this.horaLinhaBase = parm;
	}

	public String getSituacao(){
		return this.situacao;
	}
	public void setSituacao(String parm){
		this.situacao = parm;
	}

	public java.sql.Date getDataUltAlteracao(){
		return this.dataUltAlteracao;
	}
	public void setDataUltAlteracao(java.sql.Date parm){
		this.dataUltAlteracao = parm;
	}

	public String getHoraUltAlteracao(){
		return this.horaUltAlteracao;
	}
	public void setHoraUltAlteracao(String parm){
		this.horaUltAlteracao = parm;
	}

	public String getUsuarioUltAlteracao(){
		return this.usuarioUltAlteracao;
	}
	public void setUsuarioUltAlteracao(String parm){
		this.usuarioUltAlteracao = parm;
	}
	public Collection getColTarefas() {
		return colTarefas;
	}
	public void setColTarefas(Collection colTarefas) {
		this.colTarefas = colTarefas;
	}
	public Integer getIdLinhaBaseProjetoUpdate() {
		return idLinhaBaseProjetoUpdate;
	}
	public void setIdLinhaBaseProjetoUpdate(Integer idLinhaBaseProjetoUpdate) {
		this.idLinhaBaseProjetoUpdate = idLinhaBaseProjetoUpdate;
	}
	public java.sql.Date getDataSolMudanca() {
		return dataSolMudanca;
	}
	public void setDataSolMudanca(java.sql.Date dataSolMudanca) {
		this.dataSolMudanca = dataSolMudanca;
	}
	public String getHoraSolMudanca() {
		return horaSolMudanca;
	}
	public void setHoraSolMudanca(String horaSolMudanca) {
		this.horaSolMudanca = horaSolMudanca;
	}
	public String getUsuarioSolMudanca() {
		return usuarioSolMudanca;
	}
	public void setUsuarioSolMudanca(String usuarioSolMudanca) {
		this.usuarioSolMudanca = usuarioSolMudanca;
	}
	public String getJustificativaMudanca() {
		return justificativaMudanca;
	}
	public void setJustificativaMudanca(String justificativaMudanca) {
		this.justificativaMudanca = justificativaMudanca;
	}

}
