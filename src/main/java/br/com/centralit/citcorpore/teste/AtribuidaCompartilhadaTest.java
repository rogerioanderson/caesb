/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import java.util.ArrayList;
import java.util.List;

import br.com.centralit.bpm.dto.TarefaFluxoDTO;
import br.com.centralit.citcorpore.bean.GerenciamentoServicosDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.ExecucaoSolicitacaoService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilTest;

public class AtribuidaCompartilhadaTest {
	public String testAtribuirCompartilhar() {
		ExecucaoSolicitacaoService execucaoSolicitacaoService;
		try {
			execucaoSolicitacaoService = (ExecucaoSolicitacaoService) ServiceLocator.getInstance().getService(ExecucaoSolicitacaoService.class, null);
			//GerenciamentoServicosDTO gerenciamentoBean = (GerenciamentoServicosDTO) document.getBean();
			GerenciamentoServicosDTO gerenciamentoBean = new GerenciamentoServicosDTO();
			gerenciamentoBean.setAcaoFluxo("true");
			gerenciamentoBean.setIdFluxo(1);
			gerenciamentoBean.setIdSolicitacao(613);
			gerenciamentoBean.setIdSolicitacaoSel("231");
			gerenciamentoBean.setIdTarefa(1);
			gerenciamentoBean.setNomeCampoOrdenacao("teste");
			gerenciamentoBean.setNumeroContratoSel("001");
			gerenciamentoBean.setOrdenacaoAsc("true");
			
			UsuarioDTO usuario = new UsuarioDTO();
			usuario.setIdUsuario(430);
			usuario.setLogin("abc");
			
			SolicitacaoServicoDTO solicitacaoServicoDto = new SolicitacaoServicoDTO();
			solicitacaoServicoDto.setContrato("015");
			solicitacaoServicoDto.setIdSolicitacaoServicoPesquisa(14);
			
			
			List<TarefaFluxoDTO> colTarefasFiltradas = new ArrayList();
			TarefaFluxoDTO tarefaDto = new TarefaFluxoDTO();
			colTarefasFiltradas.add(tarefaDto);
			
			boolean bFiltroPorContrato = gerenciamentoBean.getNumeroContratoSel() != null && gerenciamentoBean.getNumeroContratoSel().length() > 0;
			boolean bFiltroPorSolicitacao = gerenciamentoBean.getIdSolicitacaoSel() != null && gerenciamentoBean.getIdSolicitacaoSel().length() > 0;
			return new UtilTest().testNotNull(gerenciamentoBean);
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
