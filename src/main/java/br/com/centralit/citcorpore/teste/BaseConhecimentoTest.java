/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;
import br.com.centralit.citcorpore.negocio.BaseConhecimentoService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilTest;

public class BaseConhecimentoTest {
	public String testCreateBaseConhecimento() {
		BaseConhecimentoService baseConhecimentoService;
		try {
			baseConhecimentoService = (BaseConhecimentoService) ServiceLocator.getInstance().getService(BaseConhecimentoService.class, null);
			BaseConhecimentoDTO baseConhecimentoDTO = new BaseConhecimentoDTO();
			baseConhecimentoDTO.setAcessado("S");
			baseConhecimentoDTO.setConteudo("Teste de Cria��o");
			baseConhecimentoDTO.setDataExpiracao(UtilDatas.getDataAtual());
			baseConhecimentoDTO.setDataFim(UtilDatas.getDataAtual());
			baseConhecimentoDTO.setDataInicio(new java.sql.Date(05, 02, 2012));
			baseConhecimentoDTO.setGerenciamentoDisponibilidade("");
			baseConhecimentoDTO.setStatus("S");
			baseConhecimentoDTO.setTitulo("Teste Cria��o");
			baseConhecimentoDTO.setVersao("1.5");
			baseConhecimentoDTO.setDataPublicacao(UtilDatas.getDataAtual());
			baseConhecimentoDTO.setFaq("S");
			baseConhecimentoDTO.setFonteReferencia("teste");
			baseConhecimentoDTO.setIdBaseConhecimentoPai(121);
			baseConhecimentoDTO.setIdHistoricoBaseConhecimento(3);
			baseConhecimentoDTO.setIdNotificacao(9);
			baseConhecimentoDTO.setIdPasta(1);
			baseConhecimentoDTO.setIdRequisicaoMudanca(1);
			baseConhecimentoDTO.setIdSolicitacaoServico(1);
			baseConhecimentoDTO.setUltimoAcesso("");
			baseConhecimentoDTO.setTituloNotificacao("teste");
			baseConhecimentoDTO.setSituacao("DS");
			baseConhecimentoDTO.setPrivacidade("C");
			baseConhecimentoDTO.setTipoNotificacao("");
			baseConhecimentoDTO.setOrigem("1");
			baseConhecimentoDTO.setLegislacao("");
			//baseConhecimentoDTO.setDireitoAutoral("Layanne Batista");
			baseConhecimentoDTO.setArquivado("S");
			baseConhecimentoDTO.setOcultarConteudo("N");
			baseConhecimentoDTO.setNomeUsuarioAcesso("teste");
			baseConhecimentoDTO.setJustificativaObservacao("teste");
			baseConhecimentoDTO.setIdUsuarioAprovador(2);
			baseConhecimentoDTO.setIdUsuarioAutor(2);
			baseConhecimentoService.create(baseConhecimentoDTO);
			return new UtilTest().testNotNull(baseConhecimentoDTO.getIdBaseConhecimento());
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
