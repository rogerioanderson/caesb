/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.ExecucaoSolicitacaoService;
import br.com.centralit.citcorpore.negocio.GrupoService;
import br.com.centralit.citcorpore.negocio.UsuarioService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilTest;

public class DelegacaoTarefaTest {

	public String testDelegaTarefa() {
		ExecucaoSolicitacaoService execucaoFluxoService;
		try {
			UsuarioDTO usuario = new UsuarioDTO();
			usuario.setLogin("layanne.batista");

			SolicitacaoServicoDTO solicitacaoServicoDto = new SolicitacaoServicoDTO();
			solicitacaoServicoDto.setIdTarefa(19173);
			solicitacaoServicoDto.setIdUsuarioDestino(430);
			solicitacaoServicoDto.setIdGrupoDestino(null);

			String usuarioDestino = null;
			String grupoDestino = null;
			
			if (solicitacaoServicoDto.getIdUsuarioDestino() != null) {
				UsuarioService usuarioService = (UsuarioService) ServiceLocator.getInstance().getService(UsuarioService.class, null);
				UsuarioDTO usuarioDestinoDto = new UsuarioDTO();
				usuarioDestinoDto.setIdUsuario(solicitacaoServicoDto.getIdUsuarioDestino());
				usuarioDestinoDto = (UsuarioDTO) usuarioService.restore(usuarioDestinoDto);
				if (usuarioDestinoDto != null)
					usuarioDestino = usuarioDestinoDto.getLogin();
			}
			
			GrupoDTO grupoDestinoDto = null;
			if (solicitacaoServicoDto.getIdGrupoDestino() != null) {
				GrupoService grupoService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);
				grupoDestinoDto = new GrupoDTO();
				grupoDestinoDto.setIdGrupo(solicitacaoServicoDto.getIdGrupoDestino());
				grupoDestinoDto = (GrupoDTO) grupoService.restore(grupoDestinoDto);
				if (grupoDestinoDto != null)
					grupoDestino = grupoDestinoDto.getSigla();
			}
			
			execucaoFluxoService = (ExecucaoSolicitacaoService) ServiceLocator.getInstance().getService(ExecucaoSolicitacaoService.class, null);
			execucaoFluxoService.delegaTarefa(usuario.getLogin(), solicitacaoServicoDto.getIdTarefa(), usuarioDestino, grupoDestino);
			return new UtilTest().testObj(usuario.getLogin(), solicitacaoServicoDto.getIdTarefa());
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
