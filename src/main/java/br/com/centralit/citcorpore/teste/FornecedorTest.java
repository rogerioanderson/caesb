/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import br.com.centralit.citcorpore.bean.FornecedorDTO;
import br.com.centralit.citcorpore.negocio.FornecedorService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilTest;

public class FornecedorTest {
	public String testFornecedor() {
		FornecedorService fornecedorService;
		try {
			fornecedorService = (FornecedorService) ServiceLocator.getInstance().getService(FornecedorService.class, null);
			FornecedorDTO fornecedorDto = new FornecedorDTO();
			fornecedorDto.setRazaoSocial("teste layanne");
			fornecedorDto.setNomeFantasia("teste");
			fornecedorDto.setCnpj("1021452369877");
		//	fornecedorDto.setNome("");
		//	fornecedorDto.setCnpjcpf("");
			fornecedorDto.setEmail("teste@centralit.com.br");
			fornecedorDto.setObservacao("teste");
		//	fornecedorDto.setTipoPessoa("");
			fornecedorDto.setIdEndereco(25);
			fornecedorDto.setTelefone("(62)82741596");
			fornecedorDto.setFax("32147852");
			fornecedorDto.setNomeContato("Layanne Cristine Batista");
			fornecedorDto.setInscricaoEstadual("78965412");
			fornecedorDto.setInscricaoMunicipal("78965412");
			fornecedorService.create(fornecedorDto);
			return new UtilTest().testNotNull(fornecedorDto.getIdFornecedor());
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
