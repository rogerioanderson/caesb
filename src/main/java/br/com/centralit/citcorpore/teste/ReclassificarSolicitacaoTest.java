/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.SolicitacaoServicoService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilTest;

public class ReclassificarSolicitacaoTest {
	public String testReclassificarSolicitacao() {
		SolicitacaoServicoService solicitacaoServicoService;
		try {
			solicitacaoServicoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);
			SolicitacaoServicoDTO solicitacaoServicoDto = new SolicitacaoServicoDTO();
			UsuarioDTO usuario = new UsuarioDTO();
			usuario.setIdUsuario(430);
			solicitacaoServicoDto.setReclassificar("S");
			solicitacaoServicoDto.setUsuarioDto(usuario);
			solicitacaoServicoDto.setEditar("S");
			solicitacaoServicoDto.setIdCategoriaServico(1);
			solicitacaoServicoDto.setIdContrato(1);
			solicitacaoServicoDto.setIdServico(605);
			solicitacaoServicoDto.setIdTipoDemandaServico(2);
			solicitacaoServicoDto.setUrgencia("M");
			solicitacaoServicoDto.setImpacto("B");
			solicitacaoServicoDto.setIdSolicitacaoServico(613);
			solicitacaoServicoDto.setContrato("015");
			solicitacaoServicoDto.setNomecontato("Layanne Batista");
			solicitacaoServicoDto.setIdServicoContrato(4);
			solicitacaoServicoDto.setIdContatoSolicitacaoServico(251);
			solicitacaoServicoDto.setGrupoNivel1("A");
			solicitacaoServicoDto.setIdPrioridade(5);
			solicitacaoServicoDto.setDataHoraInicio(UtilDatas.getDataHoraAtual());
			solicitacaoServicoDto.setDataHora(UtilDatas.getDataHoraAtual());
			solicitacaoServicoDto.setDataHoraFim(UtilDatas.getDataHoraAtual());
			solicitacaoServicoDto.setDataInicio(UtilDatas.getDataAtual());
			solicitacaoServicoDto.setDataFim(UtilDatas.getDataAtual());
			solicitacaoServicoDto.setDataHoraReativacao(UtilDatas.getDataHoraAtual());
			solicitacaoServicoDto.setDescricao("Teste de Reclassifica��o");
			solicitacaoServicoService.updateInfo(solicitacaoServicoDto);
			return new UtilTest().testNotNull(solicitacaoServicoDto.getReclassificar());
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
