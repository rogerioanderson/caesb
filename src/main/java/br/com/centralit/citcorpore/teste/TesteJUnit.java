/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;


import junit.framework.TestCase;

import org.junit.Test;

import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.UsuarioService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;

public class TesteJUnit extends TestCase {

	@Test
	public void testMain() {
		UsuarioService usuarioService;
			try {
				usuarioService = (UsuarioService) ServiceLocator.getInstance().getService(UsuarioService.class, null);
				UsuarioDTO usuarioDTO = new UsuarioDTO();
				usuarioDTO.setDataInicio(UtilDatas.getDataAtual());
				usuarioDTO.setDataFim(new java.sql.Date(05, 02, 2012));
				usuarioDTO.setLogin("Teste.Teste");
				usuarioDTO.setNomeUsuario("Testando");
				usuarioDTO.setSenha("abc123");
				usuarioDTO.setSenhaNovamente("abc123");
				usuarioDTO.setIdEmpregado(12313);
				usuarioDTO.setIdEmpresa(1);
				usuarioDTO.setSeguencia(12345);
				usuarioDTO.setIdGrupo(2);
				usuarioDTO.setIdPerfilAcessoUsuario(3);
				usuarioDTO.setIdUnidade(4);
				usuarioDTO.setStatus("A");
				usuarioDTO.setLdap("S");
				usuarioDTO.setLocale("Central IT");
				usuarioDTO.setUltimoAcessoPortal(UtilDatas.getDataHoraAtual());
				usuarioService.create(usuarioDTO);
				assertNotNull(usuarioDTO.getIdUsuario());
			} catch (ServiceException e) {
				e.printStackTrace();
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

}
