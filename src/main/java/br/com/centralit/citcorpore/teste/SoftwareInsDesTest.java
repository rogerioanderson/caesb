/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import br.com.centralit.citcorpore.bean.BaseItemConfiguracaoDTO;
import br.com.centralit.citcorpore.negocio.BaseItemConfiguracaoService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilTest;

public class SoftwareInsDesTest {
	public String testSoftwareInsDes() {
		BaseItemConfiguracaoService baseItemConfiguracaoService;
		try {
			baseItemConfiguracaoService = (BaseItemConfiguracaoService) ServiceLocator.getInstance().getService(BaseItemConfiguracaoService.class, null);
			BaseItemConfiguracaoDTO baseItemConfiguracaoDto = new BaseItemConfiguracaoDTO();
			baseItemConfiguracaoDto.setComando("Teste Layanne");
			baseItemConfiguracaoDto.setDataFim(UtilDatas.getDataAtual());
			baseItemConfiguracaoDto.setDataInicio(UtilDatas.getDataAtual());
			baseItemConfiguracaoDto.setExecutavel("Teste.exe");
		//	baseItemConfiguracaoDto.setIdBaseItemConfiguracaoPai(2);
			baseItemConfiguracaoDto.setIdTipoItemConfiguracao(15);
			baseItemConfiguracaoDto.setNome("Teste");
			baseItemConfiguracaoDto.setTipoexecucao("I");
			baseItemConfiguracaoService.create(baseItemConfiguracaoDto);
			return new UtilTest().testNotNull(baseItemConfiguracaoDto.getId());
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
