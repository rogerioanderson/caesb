/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import java.util.ArrayList;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.AtividadePeriodicaDTO;
import br.com.centralit.citcorpore.bean.ProgramacaoAtividadeDTO;
import br.com.centralit.citcorpore.negocio.AtividadePeriodicaService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilTest;

public class AtividadePeriodicaTest {
	public String testAtividadePeriodica() {
		try {
			AtividadePeriodicaService atividadePeriodicaService = (AtividadePeriodicaService) ServiceLocator.getInstance().getService(AtividadePeriodicaService.class, null);
			AtividadePeriodicaDTO atividadeperiodicaDTO = new AtividadePeriodicaDTO();
			atividadeperiodicaDTO.setDataInicio(UtilDatas.getDataAtual());
			atividadeperiodicaDTO.setDataCriacao(UtilDatas.getDataAtual());
			atividadeperiodicaDTO.setDescricao("Teste");
			atividadeperiodicaDTO.setIdContrato(17);
			atividadeperiodicaDTO.setCriadoPor("Layanne Cristine Batista");			
			atividadeperiodicaDTO.setDuracaoEstimada(8);
			atividadeperiodicaDTO.setHoraInicio("08:00");
			atividadeperiodicaDTO.setIdGrupoAtvPeriodica(14);
			atividadeperiodicaDTO.setIdProcedimentoTecnico(35);
			atividadeperiodicaDTO.setIdSolicitacaoServico(36);
			atividadeperiodicaDTO.setTituloAtividade("Solicita��o/incidente");
			Collection colItens = new ArrayList();
			ProgramacaoAtividadeDTO programacaoAtividadeDTO = new ProgramacaoAtividadeDTO();
			programacaoAtividadeDTO.setTipoAgendamento("U");
			programacaoAtividadeDTO.setDataInicio(UtilDatas.getDataAtual());
			programacaoAtividadeDTO.setHoraInicio("08:00");
			programacaoAtividadeDTO.setHoraFim("09:00");
			programacaoAtividadeDTO.setRepeticao("N");
			colItens.add(programacaoAtividadeDTO);
			atividadeperiodicaDTO.setColItens(colItens);
			atividadePeriodicaService.create(atividadeperiodicaDTO);
			return new UtilTest().testNotNull(atividadeperiodicaDTO.getIdAtividadePeriodica());
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
