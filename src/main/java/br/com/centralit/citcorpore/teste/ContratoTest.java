/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilTest;

public class ContratoTest {
	public String testCreateContrato() {
		ContratoService servicoContratoService;
		try {
			servicoContratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
			ContratoDTO contratoDTO = new ContratoDTO();
			contratoDTO.setCotacaoMoeda(1.20);
			contratoDTO.setDataContrato(UtilDatas.getDataAtual());
			contratoDTO.setDataFimContrato(new java.sql.Date(05, 12, 2012));
			contratoDTO.setDeleted("N");
			contratoDTO.setIdCliente(02);
			contratoDTO.setIdFluxo(78);
			contratoDTO.setIdFornecedor(1);
			contratoDTO.setIdGrupoSolicitante(1);
			contratoDTO.setIdMoeda(89);
			contratoDTO.setNumero("015");
			contratoDTO.setObjeto("Teste");
			contratoDTO.setSituacao("1");
			contratoDTO.setTempoEstimado(8);
			contratoDTO.setTipo("1");
			contratoDTO.setTipoTempoEstimado("1");
			contratoDTO.setValorEstimado(1.2);
			servicoContratoService.create(contratoDTO);
			return new UtilTest().testNotNull(contratoDTO.getIdContrato());
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
