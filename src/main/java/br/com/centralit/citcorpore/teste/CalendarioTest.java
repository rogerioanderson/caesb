/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import br.com.centralit.citcorpore.bean.CalendarioDTO;
import br.com.centralit.citcorpore.negocio.CalendarioService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilTest;

public class CalendarioTest {
	public String testCalendario() {
		CalendarioService calendarioService;
		try {
			calendarioService = (CalendarioService) ServiceLocator.getInstance().getService(CalendarioService.class, null);
			CalendarioDTO calendarioDto = new CalendarioDTO();
			calendarioDto.setDescricao("Teste");
			calendarioDto.setConsideraFeriados("S");
			calendarioDto.setIdJornadaSeg(1);
			calendarioDto.setIdJornadaTer(1);
			calendarioDto.setIdJornadaQua(1);
			calendarioDto.setIdJornadaQui(1);
			calendarioDto.setIdJornadaSex(1);
			//calendarioDto.setIdJornadaSab(1);
			//calendarioDto.setIdJornadaDom(1);
			calendarioService.create(calendarioDto);
			return new UtilTest().testNotNull(calendarioDto.getIdCalendario());
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
