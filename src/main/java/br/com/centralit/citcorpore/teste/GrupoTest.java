/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.negocio.GrupoService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilTest;

public class GrupoTest {
	public String testGrupo() {
		GrupoService grupoService;
		try {
			grupoService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);
			GrupoDTO grupoDto = new GrupoDTO();
			grupoDto.setIdEmpresa(1);
			grupoDto.setNome("teste layanne");
			grupoDto.setDataFim(UtilDatas.getDataAtual());
			grupoDto.setDataInicio(UtilDatas.getDataAtual());
			grupoDto.setDescricao("teste");
			grupoDto.setServiceDesk("S");
			grupoDto.setAbertura("S");
			grupoDto.setEncerramento("S");
			grupoDto.setAndamento("S");
			grupoDto.setSigla("TELAY");
			grupoService.create(grupoDto);
			return new UtilTest().testNotNull(grupoDto.getIdGrupo());
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
