/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import br.com.centralit.citcorpore.bean.OcorrenciaSolicitacaoDTO;
import br.com.centralit.citcorpore.negocio.OcorrenciaSolicitacaoService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilTest;

public class OcorrenciaSolicitacaoTest {
	public String testCreateOcorrencia() {
		OcorrenciaSolicitacaoService ocorrenciaSolicitacaoService;
		try {
			ocorrenciaSolicitacaoService = (OcorrenciaSolicitacaoService) ServiceLocator.getInstance().getService(OcorrenciaSolicitacaoService.class, null);
			OcorrenciaSolicitacaoDTO ocorrenciaSolicitacaoDTO = new OcorrenciaSolicitacaoDTO();
			ocorrenciaSolicitacaoDTO.setCategoria("Cria��o");
			ocorrenciaSolicitacaoDTO.setComplementoJustificativa("Teste");
			ocorrenciaSolicitacaoDTO.setDadosSolicitacao("Teste");
			ocorrenciaSolicitacaoDTO.setDataFim(UtilDatas.getDataAtual());
			ocorrenciaSolicitacaoDTO.setDataInicio(new java.sql.Date(05, 02, 2012));
			ocorrenciaSolicitacaoDTO.setDataregistro(UtilDatas.getDataAtual());
			ocorrenciaSolicitacaoDTO.setDescricao("Teste Cria��o");
			ocorrenciaSolicitacaoDTO.setHoraregistro("18:30");
			ocorrenciaSolicitacaoDTO.setIdItemTrabalho(415);
			ocorrenciaSolicitacaoDTO.setIdJustificativa(1);
			ocorrenciaSolicitacaoDTO.setIdSolicitacaoServico(355);
			ocorrenciaSolicitacaoDTO.setInformacoesContato("n�o se aplica");
			ocorrenciaSolicitacaoDTO.setOcorrencia("Teste Cria��o");
			ocorrenciaSolicitacaoDTO.setOrigem("E");
			ocorrenciaSolicitacaoDTO.setRegistradopor("Layanne Cristine Batista");
			ocorrenciaSolicitacaoDTO.setTempoGasto(0);
			ocorrenciaSolicitacaoService.create(ocorrenciaSolicitacaoDTO);
			return new UtilTest().testNotNull(ocorrenciaSolicitacaoDTO.getIdOcorrencia());
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
