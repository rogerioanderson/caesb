/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import br.com.centralit.citcorpore.bean.JornadaTrabalhoDTO;
import br.com.centralit.citcorpore.negocio.JornadaTrabalhoService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilTest;

public class JornadaTrabalhoTest {
	public String testJornadaTrabalho() {
		JornadaTrabalhoService jornadaTrabalhoService;
		try {
			jornadaTrabalhoService = (JornadaTrabalhoService) ServiceLocator.getInstance().getService(JornadaTrabalhoService.class, null);
			JornadaTrabalhoDTO jornadaTrabalhoDto = new JornadaTrabalhoDTO();
			jornadaTrabalhoDto.setDescricao("teste");
			jornadaTrabalhoDto.setDataFim(UtilDatas.getDataAtual());
			jornadaTrabalhoDto.setDataInicio(UtilDatas.getDataAtual());
			jornadaTrabalhoDto.setCargaHoraria("40:00");
			jornadaTrabalhoDto.setInicio1("08:00");
			jornadaTrabalhoDto.setTermino1("18:00");
			jornadaTrabalhoDto.setInicio2("08:00");
			jornadaTrabalhoDto.setTermino2("18:00");
			jornadaTrabalhoDto.setInicio3("08:00");
			jornadaTrabalhoDto.setTermino3("18:00");
			jornadaTrabalhoDto.setInicio4("08:00");
			jornadaTrabalhoDto.setTermino4("18:00");
			jornadaTrabalhoDto.setInicio5("08:00");
			jornadaTrabalhoDto.setTermino5("18:00");
			jornadaTrabalhoService.create(jornadaTrabalhoDto);
			return new UtilTest().testNotNull(jornadaTrabalhoDto.getIdJornada());
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
