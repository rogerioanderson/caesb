/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import br.com.centralit.citcorpore.bean.RequisicaoProdutoDTO;
import br.com.centralit.citcorpore.negocio.RequisicaoProdutoService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilTest;

public class RequisicaoProdutoTest {
	public String testRequisicaoProduto() {
		RequisicaoProdutoService requisicaoProdutoService;
		try {
			requisicaoProdutoService = (RequisicaoProdutoService) ServiceLocator.getInstance().getService(RequisicaoProdutoService.class, null);
			RequisicaoProdutoDTO requisicaoProdutoDto = new RequisicaoProdutoDTO();
			requisicaoProdutoDto.setIdProjeto(1);
			requisicaoProdutoDto.setIdCentroCusto(1);
			requisicaoProdutoDto.setFinalidade("");
			requisicaoProdutoDto.setIdEnderecoEntrega(1);
			requisicaoProdutoDto.setIdCategoriaProduto(1);
			requisicaoProdutoDto.setIdProduto(1);
			requisicaoProdutoDto.setTipoIdentificacaoItem("");
			requisicaoProdutoDto.setRejeitada("N");
			requisicaoProdutoDto.setIdFornecedorColeta(1);
			requisicaoProdutoDto.setIdItemColeta(1);
			requisicaoProdutoDto.setAcao("abrir");
			requisicaoProdutoDto.setValorAprovado(2.5);
			requisicaoProdutoDto.setIdColetaPreco(1);
			requisicaoProdutoDto.setIdItemRequisicaoProduto(1);
		    requisicaoProdutoDto.setItensRequisicao_serialize("");
		    requisicaoProdutoDto.setItensCotacao_serialize("");
		    requisicaoProdutoDto.setDemanda("");
		    requisicaoProdutoDto.setDescricao("teste");
		    requisicaoProdutoDto.setDescrSituacao("teste");
		    requisicaoProdutoDto.setDetalhamentoCausa("");
		    requisicaoProdutoDto.setEditar("S");
		    requisicaoProdutoDto.setEmailcontato("S");
		    requisicaoProdutoDto.setEnviaEmailAcoes("S");
		    requisicaoProdutoDto.setAcaoFluxo("");
		    requisicaoProdutoDto.setAlterarSituacao("S");
		    requisicaoProdutoDto.setAtendimentoPresencial("N");
		    requisicaoProdutoDto.setAtrasoSLA(1.0);
		    requisicaoProdutoDto.setAtrasoSLAStr("1.0");
		    requisicaoProdutoDto.setCaracteristica("");
		    requisicaoProdutoDto.setCentroCusto("");
		    requisicaoProdutoDto.setComplementoJustificativa("");
		    requisicaoProdutoDto.setContrato("");
			requisicaoProdutoService.create(requisicaoProdutoDto);
			return new UtilTest().testNotNull(requisicaoProdutoDto.getIdRequisicaoMudanca());
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
