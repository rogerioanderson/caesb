/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import br.com.centralit.citcorpore.bean.ServicoContratoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.ServicoContratoService;
import br.com.centralit.citcorpore.negocio.SolicitacaoServicoService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilTest;

public class SubSolicitacaoTest {
	public String testCreateSubSolicitacao() {
		try {
			SolicitacaoServicoDTO novaSolicitacaoServicoDto = new SolicitacaoServicoDTO();
			UsuarioDTO usuarioDto = new UsuarioDTO();
			usuarioDto.setIdUsuario(430);
			SolicitacaoServicoService solicitacaoServicoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);
			ServicoContratoService servicoContratoService = (ServicoContratoService) ServiceLocator.getInstance().getService(ServicoContratoService.class, null);
			SolicitacaoServicoDTO solicitacaoServicoOrigem = new SolicitacaoServicoDTO();
			ServicoContratoDTO servicoContratoDto = new ServicoContratoDTO();
			solicitacaoServicoOrigem.setIdSolicitacaoServico(4);
			solicitacaoServicoOrigem = (SolicitacaoServicoDTO) solicitacaoServicoService.restore(solicitacaoServicoOrigem);
			servicoContratoDto.setIdServicoContrato(solicitacaoServicoOrigem.getIdServicoContrato());
			servicoContratoDto = (ServicoContratoDTO) servicoContratoService.restore(servicoContratoDto);
			novaSolicitacaoServicoDto.setIdSolicitacaoServico(solicitacaoServicoOrigem.getIdSolicitacaoServico());
			novaSolicitacaoServicoDto.setIdSolicitacaoPai(solicitacaoServicoOrigem.getIdSolicitacaoServico());
			novaSolicitacaoServicoDto.setIdContatoSolicitacaoServico(solicitacaoServicoOrigem.getIdContatoSolicitacaoServico());
			novaSolicitacaoServicoDto.setIdServico(servicoContratoDto.getIdServico());
			novaSolicitacaoServicoDto.setUsuarioDto(usuarioDto);
			novaSolicitacaoServicoDto.setDescricao(solicitacaoServicoOrigem.getDescricao());
			novaSolicitacaoServicoDto.setSituacao(solicitacaoServicoOrigem.getSituacao());
			novaSolicitacaoServicoDto.setIdContrato(1);
			novaSolicitacaoServicoDto.setIdServico(605);
			novaSolicitacaoServicoDto.setNomecontato("Layanne Batista");
			novaSolicitacaoServicoDto.setRegistroexecucao(solicitacaoServicoOrigem.getDescricao());
			solicitacaoServicoService.create(novaSolicitacaoServicoDto); 
			return new UtilTest().testNotNull(novaSolicitacaoServicoDto.getIdSolicitacaoServico());
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
