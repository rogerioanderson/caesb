/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.teste;

import java.util.HashMap;

import br.com.centralit.bpm.dto.ElementoFluxoDTO;
import br.com.centralit.bpm.util.Enumerados;
import br.com.centralit.citcorpore.bean.GerenciamentoServicosDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.bpm.negocio.ExecucaoSolicitacao;
import br.com.centralit.citcorpore.integracao.ExecucaoSolicitacaoDao;
import br.com.centralit.citcorpore.negocio.SolicitacaoServicoServiceEjb;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.CrudDAO;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.util.UtilTest;

public class ExecutarTarefaTest {
	protected CrudDAO getDao() throws ServiceException {
		return new ExecucaoSolicitacaoDao();
	}
	public String testIniciarExecutarTarefa() {
		try {
			GerenciamentoServicosDTO gerenciamentoBean = new GerenciamentoServicosDTO();
			gerenciamentoBean.setAcaoFluxo("true");
			gerenciamentoBean.setIdFluxo(1);
			gerenciamentoBean.setIdSolicitacao(613);
			gerenciamentoBean.setIdSolicitacaoSel("213");
			gerenciamentoBean.setIdTarefa(1);
			gerenciamentoBean.setNomeCampoOrdenacao("");
			gerenciamentoBean.setNumeroContratoSel("1");
			gerenciamentoBean.setOrdenacaoAsc("");
			gerenciamentoBean.setDescricaoSolicitacao("");
			UsuarioDTO usuarioDto = new UsuarioDTO();
			usuarioDto.setIdUsuario(430);
			usuarioDto.setLogin("Layanne.Batista");
			SolicitacaoServicoDTO solicitacaoServicoDto = new SolicitacaoServicoDTO();
			solicitacaoServicoDto.setIdSolicitacaoServico(213);
			solicitacaoServicoDto.setUsuarioDto(usuarioDto);
			SolicitacaoServicoDTO solicitacaoAuxDto = new SolicitacaoServicoServiceEjb().restoreAll(solicitacaoServicoDto.getIdSolicitacaoServico());
			ElementoFluxoDTO elementoFluxoDto = new ElementoFluxoDTO();
			elementoFluxoDto.setIdElemento(1);
			HashMap<String, Object> objetos = new HashMap();
			TransactionControler tc ;
			tc = new TransactionControlerImpl(getDao().getAliasDB());
			new ExecucaoSolicitacao(tc).executa(solicitacaoServicoDto.getUsuarioDto().getLogin(), solicitacaoAuxDto, gerenciamentoBean.getIdTarefa(), Enumerados.ACAO_EXECUTAR, objetos);
		return new UtilTest().testNotNull(Enumerados.ACAO_EXECUTAR);
		} catch (ServiceException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
