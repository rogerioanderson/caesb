/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.TipoMovimFinanceiraViagemDTO;
import br.com.centralit.citcorpore.negocio.TipoMovimFinanceiraViagemService;
import br.com.centralit.citcorpore.util.Enumerados.ClassificacaoMovFinViagem;
import br.com.centralit.citcorpore.util.Enumerados.TipoMovFinViagem;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilI18N;

/**
 * @author ronnie.lopes
 * 
 */
@SuppressWarnings("rawtypes")
public class TipoMovimFinanceiraViagem extends AjaxFormAction {
	
	public Class getBeanClass() {
		return TipoMovimFinanceiraViagemDTO.class;
	}

	/**
	 * Inicializa os dados ao carregar a tela.
	 * @author ronnie.lopes
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		this.preencherComboClassificacao(document, request, response);	
		this.preencherComboTipo(document, request, response);
	}

	/**
	 * Inclui registro.
	 * @author ronnie.lopes
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		TipoMovimFinanceiraViagemDTO tipoMovimFinanceiraViagemDto = (TipoMovimFinanceiraViagemDTO) document.getBean();
		TipoMovimFinanceiraViagemService tipoMovimFinanceiraViagemService = (TipoMovimFinanceiraViagemService) ServiceLocator.getInstance().getService(TipoMovimFinanceiraViagemService.class, WebUtil.getUsuarioSistema(request));
		
		
		/**
		 * @author gilberto.nery
		 * @date: 19/11/2015
		 *
		 * Nao sera mais exigido data/hora cotacao
		 * 
		 * Regra de negocio em:
		 * \\10.2.1.11\Desenvolvimento\Equipes CDI\Equipe ITSM\RGN\Demanda de altera��es CitViagem - Citsmart.msg
		 *
		 */
		tipoMovimFinanceiraViagemDto.setExigeDataHoraCotacao("N");
		
		if (tipoMovimFinanceiraViagemDto.getIdtipoMovimFinanceiraViagem() == null || tipoMovimFinanceiraViagemDto.getIdtipoMovimFinanceiraViagem().intValue() == 0) {
			tipoMovimFinanceiraViagemService.create(tipoMovimFinanceiraViagemDto);
			document.alert(UtilI18N.internacionaliza(request, "MSG05"));
		} else {
			tipoMovimFinanceiraViagemService.update(tipoMovimFinanceiraViagemDto);
			document.alert(UtilI18N.internacionaliza(request, "MSG06"));
		}
		HTMLForm form = document.getForm("form");
		form.clear();
	}

	/**
	 * Restaura os dados ao clicar em um registro.
	 * @author ronnie.lopes
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		TipoMovimFinanceiraViagemDTO tipoMovimFinanceiraViagemDto = (TipoMovimFinanceiraViagemDTO) document.getBean();
		TipoMovimFinanceiraViagemService tipoMovimFinanceiraViagemService = (TipoMovimFinanceiraViagemService) ServiceLocator.getInstance().getService(TipoMovimFinanceiraViagemService.class, null);

		tipoMovimFinanceiraViagemDto = (TipoMovimFinanceiraViagemDTO) tipoMovimFinanceiraViagemService.restore(tipoMovimFinanceiraViagemDto);

		HTMLForm form = document.getForm("form");
		form.clear();
		form.setValues(tipoMovimFinanceiraViagemDto);

	}
	
	/**
	 * Deleta o dado quando for solicitado a sua exclus�o.
	 * @author ronnie.lopes
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception   
	 */
	public void delete(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		TipoMovimFinanceiraViagemDTO tipoMoimFinanceiraViagemDto = (TipoMovimFinanceiraViagemDTO) document.getBean();
		TipoMovimFinanceiraViagemService tipoMovimFinanceiraViagemService = (TipoMovimFinanceiraViagemService) ServiceLocator.getInstance().getService(TipoMovimFinanceiraViagemService.class, null);

		if (tipoMoimFinanceiraViagemDto.getIdtipoMovimFinanceiraViagem().intValue() > 0) {
			tipoMovimFinanceiraViagemService.delete((tipoMoimFinanceiraViagemDto));
		}
		
		HTMLForm form = document.getForm("form");
		form.clear();
		document.alert(UtilI18N.internacionaliza(request, "MSG07"));

	}
	
	/**
	 * Preenche o combo da classifica��o com o enumerado ClassificacaoMovFinViagem
	 * @author ronnie.lopes
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void preencherComboClassificacao(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
        HTMLSelect classificacoes = (HTMLSelect) document.getSelectById("classificacao");
        classificacoes.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
        for (ClassificacaoMovFinViagem object : ClassificacaoMovFinViagem.values()) {
        	classificacoes.addOption(object.getDescricao(), object.getDescricao());
        } 
	}
	
	/**
	 * Preenche o combo do tipo com o enumerado TipoMovFinViagem
	 * @author ronnie.lopes
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void preencherComboTipo(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
        HTMLSelect tipos = (HTMLSelect) document.getSelectById("tipo");
        tipos.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
        for (TipoMovFinViagem object : TipoMovFinViagem.values()) {
        	tipos.addOption(object.name(), UtilI18N.internacionaliza(request, object.getDescricao()));
        } 
	}
	
}
