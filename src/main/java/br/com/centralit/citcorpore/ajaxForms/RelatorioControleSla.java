/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.commons.lang.StringUtils;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.bean.PrioridadeDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.TipoServicoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.GrupoService;
import br.com.centralit.citcorpore.negocio.PrioridadeService;
import br.com.centralit.citcorpore.negocio.SolicitacaoServicoService;
import br.com.centralit.citcorpore.negocio.SolicitacaoServicoServiceEjb;
import br.com.centralit.citcorpore.negocio.TipoServicoService;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.centralit.citcorpore.util.LogoRel;
import br.com.centralit.citcorpore.util.UtilRelatorio;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class RelatorioControleSla extends AjaxFormAction {
	UsuarioDTO usuario;

	@Override
	public void load(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		this.usuario = WebUtil.getUsuario(request);
		if (this.usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}
		this.controlarMontagemDeCombos(document, request);
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#controlarMontagemDeCombos).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void controlarMontagemDeCombos(final DocumentHTML document, final HttpServletRequest request) throws Exception, ServiceException, LogicException {
		this.montarComboContratos(document, request);
		this.montarComboGruposAtuais(document, request);
		this.montarComboPrioridades(document, request);
		this.montarComboTipoServicos(document, request);
		this.montarComboPrazos(document, request);
		this.montarComboSLA(document, request);
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#montarComboSLA).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void montarComboSLA(final DocumentHTML document, final HttpServletRequest request) throws Exception {
		final SolicitacaoServicoService solicitacaoServicoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);
		document.getSelectById("sla").removeAllOptions();
		document.getSelectById("sla").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");
		final ArrayList<SolicitacaoServicoDTO> colSLA = (ArrayList<SolicitacaoServicoDTO>) solicitacaoServicoService.listarSLA();
		if (colSLA != null) {
			for (final SolicitacaoServicoDTO solicitacaoServico : colSLA) {
				if (solicitacaoServico.getPrazoHH() < 10 && solicitacaoServico.getPrazoMM() < 10) {
					document.getSelectById("sla").addOption(solicitacaoServico.getPrazoHH() + ":" + solicitacaoServico.getPrazoMM(),
							"0" + solicitacaoServico.getPrazoHH() + "h " + "0" + solicitacaoServico.getPrazoMM() + "m");
				} else if (solicitacaoServico.getPrazoHH() < 10 && solicitacaoServico.getPrazoMM() > 10) {
					document.getSelectById("sla").addOption(solicitacaoServico.getPrazoHH() + ":" + solicitacaoServico.getPrazoMM(),
							"0" + solicitacaoServico.getPrazoHH() + "h " + solicitacaoServico.getPrazoMM() + "m");
				} else if (solicitacaoServico.getPrazoHH() > 9 && solicitacaoServico.getPrazoMM() < 10) {
					document.getSelectById("sla").addOption(solicitacaoServico.getPrazoHH() + ":" + solicitacaoServico.getPrazoMM(),
							solicitacaoServico.getPrazoHH() + "h " + "0" + solicitacaoServico.getPrazoMM() + "m");
				} else {
					document.getSelectById("sla").addOption(solicitacaoServico.getPrazoHH() + ":" + solicitacaoServico.getPrazoMM(),
							solicitacaoServico.getPrazoHH() + "h " + solicitacaoServico.getPrazoMM() + "m");
				}
			}
		}
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#montarComboPrazos).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void montarComboPrazos(final DocumentHTML document, final HttpServletRequest request) throws Exception {
		document.getSelectById("prazo").removeAllOptions();
		document.getSelectById("prazo").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");
		document.getSelectById("prazo").addOption("S", UtilI18N.internacionaliza(request, "relatorioSla.semAtraso"));
		document.getSelectById("prazo").addOption("N", UtilI18N.internacionaliza(request, "relatorioSla.comAtraso"));
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#montarComboTipoServicos).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void montarComboTipoServicos(final DocumentHTML document, final HttpServletRequest request) throws Exception, ServiceException, LogicException {
		document.getSelectById("idTipoServico").removeAllOptions();
		final TipoServicoService tipoServicoService = (TipoServicoService) ServiceLocator.getInstance().getService(TipoServicoService.class, null);
		final Collection colTipos = tipoServicoService.list();
		document.getSelectById("idTipoServico").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");
		document.getSelectById("idTipoServico").addOptions(colTipos, "idTipoServico", "nomeTipoServico", null);
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#montarComboPrioridades).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void montarComboPrioridades(final DocumentHTML document, final HttpServletRequest request) throws Exception, ServiceException, LogicException {
		document.getSelectById("idPrioridade").removeAllOptions();
		final PrioridadeService prioridadeService = (PrioridadeService) ServiceLocator.getInstance().getService(PrioridadeService.class, null);
		final Collection colPrioridade = prioridadeService.list();
		document.getSelectById("idPrioridade").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");
		document.getSelectById("idPrioridade").addOptions(colPrioridade, "idPrioridade", "nomePrioridade", null);
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#montarComboGrupoAtual).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void montarComboGruposAtuais(final DocumentHTML document, final HttpServletRequest request) throws Exception, ServiceException {
		document.getSelectById("idGrupoAtual").removeAllOptions();
		final GrupoService grupoService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);
		final Collection colGrupo = grupoService.listarGruposAtivos();
		document.getSelectById("idGrupoAtual").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");
		document.getSelectById("idGrupoAtual").addOptions(colGrupo, "idGrupo", "nome", null);
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#montarComboContratos).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void montarComboContratos(final DocumentHTML document, final HttpServletRequest request) throws Exception, ServiceException, LogicException {
		document.getSelectById("idContrato").removeAllOptions();
		final ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
		final Collection colContrato = contratoService.list();
		document.getSelectById("idContrato").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");
		document.getSelectById("idContrato").addOptions(colContrato, "idContrato", "numero", null);
	}

	@Override
	public Class getBeanClass() {
		return SolicitacaoServicoDTO.class;
	}

	/**
	 * FireEvent respons�vel por gerar o Relat�rio XML de Controle SLA.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void imprimirRelatorioControleSla(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		this.usuario = WebUtil.getUsuario(request);
		if (this.usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}
		final SolicitacaoServicoDTO solicitacaoServicoDTO = (SolicitacaoServicoDTO) document.getBean();
		final HttpSession session = request.getSession();
		final TipoServicoDTO tipoServicoDTO = new TipoServicoDTO();
		final TipoServicoService tipoServicoService = (TipoServicoService) ServiceLocator.getInstance().getService(TipoServicoService.class, null);
		final PrioridadeDTO prioridadeDto = new PrioridadeDTO();
		final PrioridadeService prioridadeService = (PrioridadeService) ServiceLocator.getInstance().getService(PrioridadeService.class, null);
		final GrupoDTO grupoDto = new GrupoDTO();
		final GrupoService grupoSegurancaService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);
		Collection<SolicitacaoServicoDTO> listSolicitacaoServicoControleSla = new ArrayList();
		final SolicitacaoServicoService solicitacaoServicoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);

		// Desenvolvedor: ibimon.morais - Data: 14/08/2015 - Hor�rio: 09:04 - ID Citsmart: 176362 -
		// Motivo/Coment�rio: gerando a possibilidade de passar montar uma forma de consulta em um banco
		// espelhado, parametrizando o jdbc para reports.
		solicitacaoServicoDTO.setBaseReports(Boolean.TRUE);
		listSolicitacaoServicoControleSla = solicitacaoServicoService.relatorioControleSla(solicitacaoServicoDTO);
		listSolicitacaoServicoControleSla = this.tratarListSolicitacaoServicoRelatorioControleSla(request, solicitacaoServicoDTO, listSolicitacaoServicoControleSla);

		final ContratoDTO contratoDto = new ContratoDTO();
		final ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);

		final Date dt = new Date();
		final String strCompl = "" + dt.getTime();
		final String caminhoJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioControleSLA.jasper";
		final String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
		final String diretorioRelativoOS = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";

		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros = UtilRelatorio.trataInternacionalizacaoLocale(session, parametros);
		montarParametrosDadoComunsParaPDF(request, solicitacaoServicoDTO, parametros);
		
		this.definirParametrosDeImpressaoDetalhes(solicitacaoServicoDTO, tipoServicoDTO, tipoServicoService, prioridadeDto, prioridadeService, grupoDto, grupoSegurancaService,
				contratoDto, contratoService, parametros);

		if (listSolicitacaoServicoControleSla.size() == 0) {
			this.dispararAlertListaVazia(document, request);
			return;
		}
		this.executarImpressaoRelatorios(document, listSolicitacaoServicoControleSla, strCompl, diretorioReceita, diretorioRelativoOS, parametros, 2, caminhoJasper);
	}

		/**
		 * incidente-182372 - RelatorioControleSla.java_(#montarParametrosDadoComunsParaPDF).
		 *
		 * @since 03/12/2015
		 * @author ibimon.morais
		 * @throws Exception 
		 */
	private void montarParametrosDadoComunsParaPDF(final HttpServletRequest request, final SolicitacaoServicoDTO solicitacaoServicoDTO, Map<String, Object> parametros) throws Exception {
		parametros.put("TITULO_RELATORIO", UtilI18N.internacionaliza(request, "relatorioSla.titulo"));
		parametros.put("CIDADE", getCidadeParametrizada(request));
		parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
		parametros.put("NOME_USUARIO", this.usuario.getNomeUsuario());
		parametros.put("numero", String.valueOf(solicitacaoServicoDTO.getIdSolicitacaoServico()));
		parametros.put("solicitante", solicitacaoServicoDTO.getNomeSolicitante());
		parametros.put("dataInicio", solicitacaoServicoDTO.getDataInicio());
		parametros.put("dataFim", solicitacaoServicoDTO.getDataFim());
		parametros.put("Logo", LogoRel.getFile());
	}

	/**
	 * FireEvent respons�vel por gerar o Relat�rio XML de Controle SLA.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void imprimirRelatorioControleSlaXls(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		this.usuario = WebUtil.getUsuario(request);
		if (this.usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}
		final SolicitacaoServicoDTO solicitacaoServicoDTO = (SolicitacaoServicoDTO) document.getBean();
		final HttpSession session = request.getSession();
		final TipoServicoDTO tipoServicoDTO = new TipoServicoDTO();
		final TipoServicoService tipoServicoService = (TipoServicoService) ServiceLocator.getInstance().getService(TipoServicoService.class, null);
		final PrioridadeDTO prioridadeDto = new PrioridadeDTO();
		final PrioridadeService prioridadeService = (PrioridadeService) ServiceLocator.getInstance().getService(PrioridadeService.class, null);
		final GrupoDTO grupoDto = new GrupoDTO();
		final GrupoService grupoSegurancaService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);
		final SolicitacaoServicoService solicitacaoServicoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);
		Collection<SolicitacaoServicoDTO> listSolicitacaoServicoControleSla = new ArrayList();

		solicitacaoServicoDTO.setBaseReports(Boolean.TRUE);
		listSolicitacaoServicoControleSla = solicitacaoServicoService.relatorioControleSla(solicitacaoServicoDTO);
		listSolicitacaoServicoControleSla = this.tratarListSolicitacaoServicoRelatorioControleSla(request, solicitacaoServicoDTO, listSolicitacaoServicoControleSla);

		final ContratoDTO contratoDto = new ContratoDTO();
		final ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);

		final Date dt = new Date();
		final String strCompl = "" + dt.getTime();
		final String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
		final String diretorioRelativoOS = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";
		final Map<String, Object> parametros = this.definirParametrosFixos(request, solicitacaoServicoDTO, session);
		this.definirParametrosDeImpressaoDetalhes(solicitacaoServicoDTO, tipoServicoDTO, tipoServicoService, prioridadeDto, prioridadeService, grupoDto, grupoSegurancaService,
				contratoDto, contratoService, parametros);

		if (listSolicitacaoServicoControleSla.size() == 0) {
			this.dispararAlertListaVazia(document, request);
			return;
		}
		this.executarImpressaoRelatorios(document, listSolicitacaoServicoControleSla, strCompl, diretorioReceita, diretorioRelativoOS, parametros, 1, null);
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#definirParametrosDeImpressaoDetalhes).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void definirParametrosDeImpressaoDetalhes(final SolicitacaoServicoDTO solicitacaoServicoDTO, final TipoServicoDTO tipoServicoDTO,
			final TipoServicoService tipoServicoService, final PrioridadeDTO prioridadeDto, final PrioridadeService prioridadeService, final GrupoDTO grupoDto,
			final GrupoService grupoSegurancaService, final ContratoDTO contratoDto, final ContratoService contratoService, final Map<String, Object> parametros)
			throws LogicException, ServiceException {
		this.definirParametrosDeContrato(solicitacaoServicoDTO, contratoDto, contratoService, parametros);
		this.definirParametrosDeTipoDemantaServico(solicitacaoServicoDTO, tipoServicoDTO, tipoServicoService, parametros);
		this.definirParametrosPrioridade(solicitacaoServicoDTO, prioridadeDto, prioridadeService, parametros);
		this.definirParametrosGrupoAtualServico(solicitacaoServicoDTO, grupoDto, grupoSegurancaService, parametros);
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#dispararAlertListaVazia).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void dispararAlertListaVazia(final DocumentHTML document, final HttpServletRequest request) throws Exception {
		document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
		document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#definirParametrosFixos).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private Map<String, Object> definirParametrosFixos(final HttpServletRequest request, final SolicitacaoServicoDTO solicitacaoServicoDTO, final HttpSession session) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros = UtilRelatorio.trataInternacionalizacaoLocale(session, parametros);

		parametros.put("TITULO_RELATORIO", UtilI18N.internacionaliza(request, "relatorioSla.titulo"));
		parametros.put("numero", String.valueOf(solicitacaoServicoDTO.getIdSolicitacaoServico()));
		parametros.put("solicitante", solicitacaoServicoDTO.getNomeSolicitante());
		parametros.put("dataInicio", solicitacaoServicoDTO.getDataInicio());
		parametros.put("dataFim", solicitacaoServicoDTO.getDataFim());
		parametros.put("Logo", LogoRel.getFile());
		parametros.put("sla", solicitacaoServicoDTO.getPrazoHH());
		return parametros;
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#executarImpressaoRelatorios).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void executarImpressaoRelatorios(final DocumentHTML document, final Collection<SolicitacaoServicoDTO> listSolicitacaoServicoControleSla, final String strCompl,
			final String diretorioReceita, final String diretorioRelativoOS, final Map<String, Object> parametros, final int tipoRealatorio, final String caminhoJasper)
			throws JRException, Exception {
		if (tipoRealatorio == 1) {
			this.montarExportacaoXLS(document, listSolicitacaoServicoControleSla, strCompl, diretorioReceita, diretorioRelativoOS, parametros);
		} else {
			this.montarExportacaoPDF(document, listSolicitacaoServicoControleSla, strCompl, diretorioReceita, diretorioRelativoOS, parametros, caminhoJasper);
		}
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#montarExportacaoPDF).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void montarExportacaoPDF(final DocumentHTML document, final Collection<SolicitacaoServicoDTO> listSolicitacaoServicoControleSla, final String strCompl,
			final String diretorioReceita, final String diretorioRelativoOS, final Map<String, Object> parametros, final String caminhoJasper) throws JRException, Exception {
		final JRDataSource dataSource = new JRBeanCollectionDataSource(listSolicitacaoServicoControleSla);
		final JasperPrint print = JasperFillManager.fillReport(caminhoJasper, parametros, dataSource);
		JasperExportManager.exportReportToPdfFile(print, diretorioReceita + "/RelatorioControleSLA" + strCompl + "_" + this.usuario.getIdUsuario() + ".pdf");
		document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url="
				+ diretorioRelativoOS + "/RelatorioControleSLA" + strCompl + "_" + this.usuario.getIdUsuario() + ".pdf')");
		document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#montarExportacaoXLS).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void montarExportacaoXLS(final DocumentHTML document, final Collection<SolicitacaoServicoDTO> listSolicitacaoServicoControleSla, final String strCompl,
			final String diretorioReceita, final String diretorioRelativoOS, final Map<String, Object> parametros) throws JRException, Exception {
		final JRDataSource dataSource = new JRBeanCollectionDataSource(listSolicitacaoServicoControleSla);
		final JasperDesign desenho = JRXmlLoader.load(CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioControleSLAXls.jrxml");
		desenho.setLanguage("java");
		final JasperReport relatorio = JasperCompileManager.compileReport(desenho);
		final JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, dataSource);

		final JRXlsExporter exporter = new JRXlsExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, impressao);
		exporter.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		exporter.setParameter(JRXlsAbstractExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
		exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, diretorioReceita + "/RelatorioControleSLAXls" + strCompl + "_" + this.usuario.getIdUsuario() + ".xls");
		exporter.exportReport();
		document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url="
				+ diretorioRelativoOS + "/RelatorioControleSLAXls" + strCompl + "_" + this.usuario.getIdUsuario() + ".xls')");
		document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#definirParametrosGrupoAtualServico).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void definirParametrosGrupoAtualServico(final SolicitacaoServicoDTO solicitacaoServicoDTO, GrupoDTO grupoDto, final GrupoService grupoSegurancaService,
			final Map<String, Object> parametros) throws LogicException, ServiceException {
		if (solicitacaoServicoDTO.getIdGrupoAtual() != null && !solicitacaoServicoDTO.getIdGrupoAtual().equals(-1)) {
			grupoDto.setIdGrupo(solicitacaoServicoDTO.getIdGrupoAtual());
			grupoDto = (GrupoDTO) grupoSegurancaService.restore(grupoDto);
			solicitacaoServicoDTO.setGrupoAtual(grupoDto.getSigla());
			parametros.put("grupoSolucionador", solicitacaoServicoDTO.getGrupoAtual());
		} else {
			parametros.put("grupoSolucionador", "Todos");
		}
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#definirParametrosPrioridade).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void definirParametrosPrioridade(final SolicitacaoServicoDTO solicitacaoServicoDTO, PrioridadeDTO prioridadeDto, final PrioridadeService prioridadeService,
			final Map<String, Object> parametros) throws LogicException, ServiceException {
		if (solicitacaoServicoDTO.getIdPrioridade() != null && !solicitacaoServicoDTO.getIdPrioridade().equals(-1)) {
			prioridadeDto.setIdPrioridade(solicitacaoServicoDTO.getIdPrioridade());
			prioridadeDto = (PrioridadeDTO) prioridadeService.restore(prioridadeDto);
			solicitacaoServicoDTO.setPrioridade(prioridadeDto.getNomePrioridade());
			parametros.put("prioridade", solicitacaoServicoDTO.getPrioridade());
		} else {
			parametros.put("prioridade", "Todos");
		}
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#definirTipoDemantaServico).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void definirParametrosDeTipoDemantaServico(final SolicitacaoServicoDTO solicitacaoServicoDTO, TipoServicoDTO tipoServicoDTO,
			final TipoServicoService tipoServicoService, final Map<String, Object> parametros) throws LogicException, ServiceException {
		if (solicitacaoServicoDTO.getIdTipoDemandaServico() != null && !solicitacaoServicoDTO.getIdTipoDemandaServico().equals(-1)) {
			tipoServicoDTO.setIdTipoServico(solicitacaoServicoDTO.getIdTipoServico());
			tipoServicoDTO = (TipoServicoDTO) tipoServicoService.restore(tipoServicoDTO);
			solicitacaoServicoDTO.setNomeTipoDemandaServico(tipoServicoDTO.getNomeTipoServico());
			parametros.put("tipo", solicitacaoServicoDTO.getNomeTipoDemandaServico());
		} else {
			parametros.put("tipo", "Todos");
		}
	}

	/**
	 * incidente-182372 - RelatorioControleSla.java_(#definirParametrosDeContrato).
	 *
	 * @since 03/12/2015
	 * @author ibimon.morais
	 */
	private void definirParametrosDeContrato(final SolicitacaoServicoDTO solicitacaoServicoDTO, ContratoDTO contratoDto, final ContratoService contratoService,
			final Map<String, Object> parametros) throws LogicException, ServiceException {
		if (solicitacaoServicoDTO.getIdContrato() != null) {
			contratoDto.setIdContrato(solicitacaoServicoDTO.getIdContrato());
			contratoDto = (ContratoDTO) contratoService.restore(contratoDto);
			parametros.put("contrato", contratoDto.getNumero());
		} else {
			parametros.put("contrato", "Todos");
		}
	}

	/**
	 * Trata nos �tens da lista a Situa��o, Prazo e Atraso do SLA.
	 * 
	 * @param request
	 *            - HttpServletRequest
	 * @param solicitacaoServicoDTO
	 *            - SolicitacaoServicoDTO
	 * @param listSolicitacaoServicoControleSla
	 *            - Collection<SolicitacaoServicoDTO>
	 * @return Collection<SolicitacaoServicoDTO>
	 * @throws Exception
	 * @author valdoilo.damasceno
	 */
	private Collection<SolicitacaoServicoDTO> tratarListSolicitacaoServicoRelatorioControleSla(final HttpServletRequest request, final SolicitacaoServicoDTO solicitacaoServicoDTO,
			Collection<SolicitacaoServicoDTO> listSolicitacaoServicoControleSla) throws Exception {

		if (listSolicitacaoServicoControleSla != null) {
			final SolicitacaoServicoServiceEjb solicitacaoServicoServiceEjb = new SolicitacaoServicoServiceEjb();
			final Collection<SolicitacaoServicoDTO> listSolicitacaoServicoControleSlaAux = new ArrayList();

			for (SolicitacaoServicoDTO dto : listSolicitacaoServicoControleSla) {
				/*
				 * Desenvolvedor: Rodrigo Pecci - Data: 01/11/2013 - Hor�rio: 18h30min - ID Citsmart: 120770
				 * Motivo/Coment�rio: O m�todo anterior estava calculando o atraso da SLA mesmo se ela fosse
				 * Suspensa ou Cancelada. Foi alterado o m�todo.
				 */
				dto = solicitacaoServicoServiceEjb.verificaSituacaoSLA(dto, null);

				if (StringUtils.contains(StringUtils.upperCase(dto.getSituacao()), StringUtils.upperCase("EmAndamento"))) {
					dto.setSituacao(UtilI18N.internacionaliza(request, "citcorpore.comum.emandamento"));
				}

				dto.setDataHoraLimiteStr(UtilDatas.convertDateToString(TipoDate.TIMESTAMP_DEFAULT, dto.getDataHoraLimite(), WebUtil.getLanguage(request)));
				dto.setDataHoraSolicitacaoStr(UtilDatas.convertDateToString(TipoDate.TIMESTAMP_DEFAULT, dto.getDataHoraSolicitacao(), WebUtil.getLanguage(request)));
				dto.setDataHoraFimStr(UtilDatas.convertDateToString(TipoDate.TIMESTAMP_DEFAULT, dto.getDataHoraFim(), WebUtil.getLanguage(request)));
				/*
				 * Corre��o do prazo dos minutos, apresentava mais de 60 minutos
				 */
				String mm = dto.getPrazoMM().toString();
				String hh = dto.getPrazoHH().toString();
				if(Integer.parseInt(mm) > 59){
					hh = String.valueOf(Integer.parseInt(hh) + (Integer.parseInt(mm) / 60)); 
					mm = String.valueOf((Integer.parseInt(mm) % 60));
				}
				if (mm.length() == 1) {
					mm = "0" + mm;
				}
				dto.setPrazoMM(Integer.parseInt(mm));
				dto.setPrazoHH(Integer.parseInt(hh));

				if (dto.getAtrasoSLA() > 0) {
					dto.setPrazo(UtilI18N.internacionaliza(request, "citcorpore.comum.nao"));
					if (solicitacaoServicoDTO.getPrazo() != null && StringUtils.isNotBlank(solicitacaoServicoDTO.getPrazo())
							&& solicitacaoServicoDTO.getPrazo().trim().equalsIgnoreCase("S")) {
						continue;
					}
				} else {
					dto.setPrazo(UtilI18N.internacionaliza(request, "citcorpore.comum.sim"));
					if (solicitacaoServicoDTO.getPrazo() != null && StringUtils.isNotBlank(solicitacaoServicoDTO.getPrazo())
							&& solicitacaoServicoDTO.getPrazo().trim().equalsIgnoreCase("N")) {
						continue;
					}
				}
				listSolicitacaoServicoControleSlaAux.add(dto);
			}
			listSolicitacaoServicoControleSla = listSolicitacaoServicoControleSlaAux;
		}

		return listSolicitacaoServicoControleSla;
	}

}
