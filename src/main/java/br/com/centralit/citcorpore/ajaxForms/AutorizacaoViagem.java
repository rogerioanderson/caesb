/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.CidadesDTO;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.DadosBancariosIntegranteDTO;
import br.com.centralit.citcorpore.bean.DespesaViagemDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.IntegranteViagemDTO;
import br.com.centralit.citcorpore.bean.JustificativaSolicitacaoDTO;
import br.com.centralit.citcorpore.bean.ParceiroDTO;
import br.com.centralit.citcorpore.bean.ParecerDTO;
import br.com.centralit.citcorpore.bean.RequisicaoViagemDTO;
import br.com.centralit.citcorpore.bean.RoteiroViagemDTO;
import br.com.centralit.citcorpore.bean.TipoMovimFinanceiraViagemDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.CentroResultadoService;
import br.com.centralit.citcorpore.negocio.CidadesService;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.DadosBancariosIntegranteService;
import br.com.centralit.citcorpore.negocio.DespesaViagemService;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.IntegranteViagemService;
import br.com.centralit.citcorpore.negocio.JustificativaParecerService;
import br.com.centralit.citcorpore.negocio.JustificativaSolicitacaoService;
import br.com.centralit.citcorpore.negocio.ParceiroService;
import br.com.centralit.citcorpore.negocio.ParecerService;
import br.com.centralit.citcorpore.negocio.ProjetoService;
import br.com.centralit.citcorpore.negocio.RequisicaoViagemService;
import br.com.centralit.citcorpore.negocio.RoteiroViagemService;
import br.com.centralit.citcorpore.negocio.TipoMovimFinanceiraViagemService;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

@SuppressWarnings({"rawtypes"})
public class AutorizacaoViagem  extends AjaxFormAction{
	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		RequisicaoViagemDTO requisicaoViagemDto = (RequisicaoViagemDTO) document.getBean();

		this.preencherComboCentroResultado(document, request, response);
		this.preencherComboProjeto(document, request, response);
		this.preencherComboJustificativa(document, request, response);
		this.preencherComboFinalidade(document, request, response);
		this.restoreTreeIntegrantesViagem(document, request, response, false);

		if(requisicaoViagemDto.getIdSolicitacaoServico()!=null){
			restore(document, request, response, requisicaoViagemDto);
		}
	}

	public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response,RequisicaoViagemDTO requisicaoViagemDto) throws ServiceException, Exception{
		DespesaViagemService despesaViagemService = (DespesaViagemService) ServiceLocator.getInstance().getService(DespesaViagemService.class, null);
		RequisicaoViagemService reqViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, null);

		Double valorTotalViagem;
		NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		DecimalFormat decimal = (DecimalFormat) nf;
		decimal.applyPattern("#,##0.00");

		requisicaoViagemDto = (RequisicaoViagemDTO) reqViagemService.restore(requisicaoViagemDto);

		if(requisicaoViagemDto != null){
			requisicaoViagemDto.setNomeCidadeOrigem(this.recuperaCidade(requisicaoViagemDto.getIdCidadeOrigem()));
			requisicaoViagemDto.setNomeCidadeDestino(this.recuperaCidade(requisicaoViagemDto.getIdCidadeDestino()));
		}

		if(requisicaoViagemDto!=null){
			valorTotalViagem = despesaViagemService.buscarDespesaTotalViagem(requisicaoViagemDto.getIdSolicitacaoServico());
			StringBuilder html = new StringBuilder();
			html.append("<b>"+UtilI18N.internacionaliza(request, "requisicaoViagem.custoTotalViagem")+":</b> R$ " + decimal.format(valorTotalViagem));
			document.getElementById("valorTotalViagem").setInnerHTML(html.toString());
		}

		HTMLForm form = document.getForm("form");
        form.clear();
        form.setValues(requisicaoViagemDto);

        document.getElementById("idCentroCusto").setDisabled(true);
        document.getElementById("idProjeto").setDisabled(true);
        document.getElementById("idMotivoViagem").setDisabled(true);
        document.getElementById("finalidade").setDisabled(true);
        document.getElementById("descricaoMotivo").setDisabled(true);
	}

	/**
	 * Retorna a cidade conforme idcidade passado
	 *
	 * @param idCidade
	 * @return
	 * @throws Exception
	 * @author thiago.borges
	 */
	public String recuperaCidade(Integer idCidade) throws Exception {
		CidadesDTO cidadeDto  = new CidadesDTO();
		CidadesService cidadesService = (CidadesService) ServiceLocator.getInstance().getService(CidadesService.class, null);

		if (idCidade != null) {
			cidadeDto = (CidadesDTO) cidadesService.findCidadeUF(idCidade);
			return cidadeDto.getNomeCidade() + " - " + cidadeDto.getNomeUf();
		}

		return null;
	}

	@Override
	public Class getBeanClass() {
		return RequisicaoViagemDTO.class;
	}

	/**
	 * Preenche a combo de 'Centro Resultado' do formul�rio HTML
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author Thiago.Borges
	 */
	public void preencherComboCentroResultado(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		CentroResultadoService centroResultadoService = (CentroResultadoService) ServiceLocator.getInstance().getService(CentroResultadoService.class, WebUtil.getUsuarioSistema(request));

        HTMLSelect comboCentroCusto = (HTMLSelect) document.getSelectById("idCentroCusto");

        comboCentroCusto.removeAllOptions();
        comboCentroCusto.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));

        Collection colCCusto = centroResultadoService.listPermiteRequisicaoProduto();
        if(colCCusto != null && !colCCusto.isEmpty()){
        	 comboCentroCusto.addOptions(colCCusto, "idCentroResultado", "nomeHierarquizado", null);
        }

	}

	/**
	 * Preenche a combo de 'finalidade' do formul�rio HTML
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author thays.araujo
	 */
	public void preencherComboFinalidade(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HTMLSelect finalidade = (HTMLSelect) document.getSelectById("finalidade");
		finalidade.removeAllOptions();
		finalidade.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
        finalidade.addOption("I", UtilI18N.internacionaliza(request, "requisicaoProduto.finalidade.usoInterno"));
		finalidade.addOption("C", UtilI18N.internacionaliza(request, "requisicaoProduto.finalidade.atendimentoCliente"));
	}

	/**
	 * Preenche a combo de 'Projeto' do formul�rio HTML
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @param requisicaoViagemDto
	 * @throws Exception
	 * @author Thiago.Borges
	 */
	public void preencherComboProjeto(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		RequisicaoViagemDTO requisicaoViagemDTO = (RequisicaoViagemDTO) document.getBean();

		ParecerDTO parecerDto = new ParecerDTO();
	    ParecerService parecerService = (ParecerService) ServiceLocator.getInstance().getService(ParecerService.class, WebUtil.getUsuarioSistema(request));

		HTMLSelect comboProjeto = (HTMLSelect) document.getSelectById("idProjeto");

		comboProjeto.removeAllOptions();
		comboProjeto.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));

		if (requisicaoViagemDTO.getIdContrato() != null) {
		    ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, WebUtil.getUsuarioSistema(request));
		    ContratoDTO contratoDto = new ContratoDTO();
		    contratoDto.setIdContrato(requisicaoViagemDTO.getIdContrato());
		    contratoDto = (ContratoDTO) contratoService.restore(contratoDto);
		    if (contratoDto != null) {
		        ProjetoService projetoService = (ProjetoService) ServiceLocator.getInstance().getService(ProjetoService.class, WebUtil.getUsuarioSistema(request));
		        Collection colProjetos = projetoService.listHierarquia(contratoDto.getIdCliente(), true);
		        if(colProjetos != null && !colProjetos.isEmpty())
		            comboProjeto.addOptions(colProjetos, "idProjeto", "nomeHierarquizado", null);
		    }
		}

		if(requisicaoViagemDTO.getIdAprovacao()!=null){
			parecerDto.setIdParecer(requisicaoViagemDTO.getIdAprovacao());
			parecerDto = (ParecerDTO) parecerService.restore(parecerDto);
			if(parecerDto!=null){
				this.preencherComboJustificativaAutorizacao(document, request, response);
				requisicaoViagemDTO.setAutorizado(parecerDto.getAprovado());
				requisicaoViagemDTO.setIdJustificativaAutorizacao(parecerDto.getIdJustificativa());
				requisicaoViagemDTO.setComplemJustificativaAutorizacao(parecerDto.getComplementoJustificativa());
			}
		}
	}

	/**
	 * Preenche combo de 'justificativa solicita��o'.
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author Thiago.Borges
	 */
	public void preencherComboJustificativa(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		JustificativaSolicitacaoService justificativaSolicitacaoService = (JustificativaSolicitacaoService)ServiceLocator.getInstance().getService(JustificativaSolicitacaoService.class, null);

		Collection<JustificativaSolicitacaoDTO> colJustificativas = justificativaSolicitacaoService.listAtivasParaViagem();

		HTMLSelect comboJustificativa = (HTMLSelect) document.getSelectById("idMotivoViagem");

		comboJustificativa.removeAllOptions();
		comboJustificativa.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));

		if (colJustificativas != null){
			comboJustificativa.addOptions(colJustificativas, "idJustificativa", "descricaoJustificativa", null);
		}
	}

	/**
	 * Recupera integrante e monta treeview
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @param collapsed
	 * @throws ServiceException
	 * @throws Exception
	 * @author thiago.borges
	 */
	public void restoreTreeIntegrantesViagem(DocumentHTML document, HttpServletRequest request, HttpServletResponse response, Boolean collapsed) throws ServiceException, Exception {
		IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, null);
		DespesaViagemService despesaViagemService = (DespesaViagemService) ServiceLocator.getInstance().getService(DespesaViagemService.class, null);
		RoteiroViagemService roteiroViagemService = (RoteiroViagemService) ServiceLocator.getInstance().getService(RoteiroViagemService.class, null);
		TipoMovimFinanceiraViagemService tipoMovimFinanceiraViagemService = (TipoMovimFinanceiraViagemService) ServiceLocator.getInstance().getService(TipoMovimFinanceiraViagemService.class, null);
		CidadesService cidadeService = (CidadesService) ServiceLocator.getInstance().getService(CidadesService.class, null);
		ParceiroService parceiroService = (ParceiroService) ServiceLocator.getInstance().getService(ParceiroService.class, null);

		RequisicaoViagemDTO requisicaoViagemDTO = (RequisicaoViagemDTO) document.getBean();
		Collection<RoteiroViagemDTO> colRoteiroViagemDTO = null;
		TipoMovimFinanceiraViagemDTO tipoMovimFinanceiraViagemDTO = null;
		CidadesDTO origem = null;
		CidadesDTO destino = null;
		ParceiroDTO parceiroDTO = new ParceiroDTO();

		Collection<IntegranteViagemDTO> colIntegrantes =  integranteViagemService.recuperaIntegrantesViagemByIdSolicitacaoEstado(requisicaoViagemDTO.getIdSolicitacaoServico(), RequisicaoViagemDTO.EM_AUTORIZACAO);

		NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		DecimalFormat decimal = (DecimalFormat) nf;
		decimal.applyPattern("#,##0.00");

		DecimalFormat quantidade = (DecimalFormat) nf.clone();
		quantidade.applyPattern("##00");

		if(colIntegrantes != null){
			StringBuilder despesaViagemHTML = new StringBuilder();
			StringBuilder roteiroViagemHTML = new StringBuilder();

			for(IntegranteViagemDTO integrante: colIntegrantes){
				colRoteiroViagemDTO = roteiroViagemService.findByIdIntegranteTodos(integrante.getIdIntegranteViagem());
				if(colRoteiroViagemDTO != null && !colRoteiroViagemDTO.isEmpty()) {
					roteiroViagemHTML.append("<div class='despesa-viagem-item'>");
					roteiroViagemHTML.append("	<ul class='filetree treeview browser'>");
					roteiroViagemHTML.append("		<li>");
					roteiroViagemHTML.append("			<div class='hitarea collapsable-hitarea'></div>");

					Double custoTotalOriginal = 0.0,
							custoTotalRemarcacao = 0.0,
							totalAprovado = 0.0,
							totalParaAprovar = 0.0;
					int contador = 0;

					for(RoteiroViagemDTO roteiroViagemDTO : colRoteiroViagemDTO) {
						if(roteiroViagemDTO.getDataFim() == null) {
							origem = (CidadesDTO) ((List) cidadeService.findNomeByIdCidade(roteiroViagemDTO.getOrigem())).get(0);
							destino = (CidadesDTO) ((List) cidadeService.findNomeByIdCidade(roteiroViagemDTO.getDestino())).get(0);

							roteiroViagemHTML.append("			<span class='folder'>" + integrante.getNome() +
													" - "+UtilI18N.internacionaliza(request, "si.comum.ida")+" " + UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT, roteiroViagemDTO.getIda(), UtilI18N.getLocale(request)) +
													" - "+UtilI18N.internacionaliza(request, "si.comum.volta")+" " + UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT, roteiroViagemDTO.getVolta(), UtilI18N.getLocale(request)) +
													" - " + origem.getNomeCidade() + "/" + origem.getNomeUf() +
													" - " + destino.getNomeCidade() + "/" + destino.getNomeUf() +
													"<a href='javascript:;' class='btn-action glyphicons icon-eye-open' onclick='abrirModalIntegrante("+ integrante.getIdIntegranteViagem() +")' title='Visualiza��o avan�ada' style='background-position: -91px -114px; float: right;'><i></i></a></span>");
							roteiroViagemHTML.append("			<ul class='filetree treeview filetree-inner'>");
						}

						Collection<DespesaViagemDTO> colDespesaViagem = despesaViagemService.findTodasDespesasViagemByIdRoteiro(roteiroViagemDTO.getIdRoteiroViagem());

						if(colDespesaViagem != null && !colDespesaViagem.isEmpty()) {
							for(DespesaViagemDTO despesaViagem : colDespesaViagem) {
								tipoMovimFinanceiraViagemDTO = tipoMovimFinanceiraViagemService.findByMovimentacao(Long.parseLong(despesaViagem.getIdTipo().toString()));
								despesaViagem.setTipoMovimFinanceiraViagem(tipoMovimFinanceiraViagemDTO);

								if(tipoMovimFinanceiraViagemDTO != null){
									parceiroDTO.setIdParceiro(despesaViagem.getIdFornecedor());
									parceiroDTO = (ParceiroDTO) parceiroService.restore(parceiroDTO);

									despesaViagemHTML.append("				<li class='expandable'>");
									despesaViagemHTML.append("					<div class='hitarea collapsable-hitarea'></div>");
									despesaViagemHTML.append("					<span class='folder '><span class='glyphicons " + despesaViagem.getTipoMovimFinanceiraViagem().getImagem() + "'><i></i>&nbsp;</span>" + despesaViagem.getTipoMovimFinanceiraViagem().getClassificacao() + " - " + (despesaViagem.getOriginal() != null && despesaViagem.getOriginal().equalsIgnoreCase("N") ? "Remarca��o" : "Original") + "</span>");
									despesaViagemHTML.append("					<ul style='display: none;'>");
									despesaViagemHTML.append("						<li class='file'>");
									despesaViagemHTML.append("							<p>" + quantidade.format(despesaViagem.getQuantidade()) + " <strong>" + tipoMovimFinanceiraViagemDTO.getClassificacao() + "</strong> "+UtilI18N.internacionaliza(request, "autorizacaoViagem.noTotalDe")+" " + despesaViagem.getTotalFormatado() + " - <strong>" + parceiroDTO.getNome() + "</strong></p>");

									if(despesaViagem.getObservacoes() != "" && despesaViagem.getObservacoes() != null) {
										despesaViagemHTML.append("							<p><strong>"+UtilI18N.internacionaliza(request, "citcorpore.comum.observacoes")+": </strong><br />" + despesaViagem.getObservacoes() + "</p>");
									}

									despesaViagemHTML.append("						</li>");
									despesaViagemHTML.append("					</ul>");
									despesaViagemHTML.append("				</li>");
									if(despesaViagem.getOriginal() != null && despesaViagem.getOriginal().equalsIgnoreCase("S")) {
										custoTotalOriginal += despesaViagem.getTotal();
									} else {
										custoTotalRemarcacao += despesaViagem.getTotal();
									}

									if(despesaViagem.getSituacao() != null && (despesaViagem.getSituacao().equalsIgnoreCase("Comprado") || despesaViagem.getSituacao().equalsIgnoreCase("Adiantado"))) {
										totalAprovado += despesaViagem.getTotal();
									} else {
										totalParaAprovar += despesaViagem.getTotal();
									}
								}
							}
						}

						contador++;
					}

					if(contador > 0) {
						despesaViagemHTML.append("				<li >");
						despesaViagemHTML.append("					<strong>Custo original:</strong> R$ " + decimal.format(custoTotalOriginal)
																+ " - <strong>Custo remarca��o:</strong> R$ " + decimal.format(custoTotalRemarcacao)
																+ " - <strong>Total aprovado: </strong> R$ " + decimal.format(totalAprovado)
																+ " - <strong>Total � aprovar: </strong> R$ " + decimal.format(totalParaAprovar));
						despesaViagemHTML.append("				</li>");
					} else {
						despesaViagemHTML.append("				<li>"+UtilI18N.internacionaliza(request, "autorizacaoViagem.naoItensComprasAdicionadosIntegrante")+"!</li>");
					}

					roteiroViagemHTML.append(despesaViagemHTML.toString());

					despesaViagemHTML = new StringBuilder();

					roteiroViagemHTML.append("			</ul>");
					roteiroViagemHTML.append("		</li>");
					roteiroViagemHTML.append("	</ul>");
					roteiroViagemHTML.append("</div><!-- .despesa-viagem-item -->");
				}
			}

			document.getElementById("despesa-viagem-items-container").setInnerHTML(roteiroViagemHTML.toString());

			document.executeScript("$('.browser').treeview({collapsed: " + collapsed + "});");
		}
	}

	/**
	 * Preenche combo de 'justificativa solicita��o ' para autoziza��o.
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author thiago.borges
	 */
	public void preencherComboJustificativaAutorizacao(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		JustificativaParecerService justificativaService = (JustificativaParecerService) ServiceLocator.getInstance().getService(JustificativaParecerService.class, WebUtil.getUsuarioSistema(request));
	    Collection colJustificativas = justificativaService.listAplicaveisRequisicao();

		HTMLSelect comboJustificativaAutorizacao = (HTMLSelect) document.getSelectById("idJustificativaAutorizacao");

		comboJustificativaAutorizacao.removeAllOptions();
		comboJustificativaAutorizacao.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));

		if (colJustificativas != null){
			comboJustificativaAutorizacao.addOptions(colJustificativas, "idJustificativa", "descricaoJustificativa", null);
		}
	}

	public void carregaIntegrantePopup(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		RequisicaoViagemDTO requisicaoViagemDto = (RequisicaoViagemDTO) document.getBean();

		IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, WebUtil.getUsuarioSistema(request));
		RoteiroViagemService roteiroViagemService = (RoteiroViagemService) ServiceLocator.getInstance().getService(RoteiroViagemService.class, WebUtil.getUsuarioSistema(request));
		DadosBancariosIntegranteService dadosBancariosIntegranteService = (DadosBancariosIntegranteService) ServiceLocator.getInstance().getService(DadosBancariosIntegranteService.class, WebUtil.getUsuarioSistema(request));
		CidadesService cidadeService = (CidadesService) ServiceLocator.getInstance().getService(CidadesService.class, WebUtil.getUsuarioSistema(request));
		EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, WebUtil.getUsuarioSistema(request));

		IntegranteViagemDTO integranteViagemDTO = new IntegranteViagemDTO();
		EmpregadoDTO empregadoReponsavel = new EmpregadoDTO();
		RoteiroViagemDTO roteiroViagemDTO = new RoteiroViagemDTO();
		CidadesDTO cidadeOrigem = new CidadesDTO();
		CidadesDTO cidadeDestino = new CidadesDTO();
		DadosBancariosIntegranteDTO dadosBancariosIntegranteDTO = new DadosBancariosIntegranteDTO();

		Integer idIntegrante = requisicaoViagemDto.getIdIntegranteAux();

		integranteViagemDTO = integranteViagemService.findById(idIntegrante);

		empregadoReponsavel = empregadoService.restoreByIdEmpregado(integranteViagemDTO.getIdRespPrestacaoContas());

		roteiroViagemDTO = roteiroViagemService.findByIdIntegrante(idIntegrante);

		cidadeOrigem = cidadeService.findByIdCidade(roteiroViagemDTO.getOrigem());

		cidadeDestino = cidadeService.findByIdCidade(roteiroViagemDTO.getDestino());

		dadosBancariosIntegranteDTO = dadosBancariosIntegranteService.findByIdIntegrante(idIntegrante);

		StringBuilder html = new StringBuilder();

		html.append("<p><b>Integrante:</b> "+integranteViagemDTO.getNome()+"</p>"+
				"<p><b>Funcionario:</b> "+integranteViagemDTO.getIntegranteFuncionario()+"</p>"+
				"<p><b>Respons�vel:</b> "+empregadoReponsavel.getNome()+"</p>"+
				"<p>&nbsp;</p>"+

				"<div class='widget'>"+
	            	"<div class=widget-head>"+
	                	"<h2 class='heading' id='titulo'>Itiner�rio</h2>"+
	                "</div><!-- .widget-head -->"+

	                "<div class='widget-body'>"+
		                "<p><b>Origem:</b> "+cidadeOrigem.getNomeCidade()+"</p>"+
		                "<p><b>Aeroporto origem:</b> "+roteiroViagemDTO.getAeroportoOrigem()+"</p>"+
		                "<p><b>Destino:</b> "+cidadeDestino.getNomeCidade()+"</p>"+
		                "<p><b>Aeroporto destino:</b> "+roteiroViagemDTO.getAeroportoDestino()+"</p>"+
		                "<p><b>Data da ida:</b> "+UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT, roteiroViagemDTO.getIda(), UtilI18N.getLocale(request))+"</p>"+
		                "<p><b>Hora da ida:</b> "+roteiroViagemDTO.getHoraInicio()+"</p>"+
		                "<p><b>Data da volta:</b> "+UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT, roteiroViagemDTO.getVolta(), UtilI18N.getLocale(request))+"</p>"+
		                "<p><b>Hora da volta:</b> "+roteiroViagemDTO.getHoraFim()+"</p>"+
		                "<p><b>Hot�is de prefer�ncia:</b> "+roteiroViagemDTO.getHoteisPreferenciais()+"</p>"+
	                "</div><!-- .widget-body -->"+
                "</div><!-- .widget -->"+


				"<p>&nbsp;</p>"+

				"<div class='widget'>"+
					"<div class=widget-head>"+
						"<h2 class='heading' id='titulo'>Dados banc�rios</h2>"+
					"</div><!-- .widget-head -->"+

					"<div class='widget-body'>"+
						"<p><b>Banco:</b> "+dadosBancariosIntegranteDTO.getBanco()+"</p>"+
						"<p><b>Ag�ncia:</b> "+dadosBancariosIntegranteDTO.getAgencia()+"</p>"+
						"<p><b>Conta:</b> "+dadosBancariosIntegranteDTO.getConta()+"</p>"+
						"<p><b>Opera��o:</b> "+dadosBancariosIntegranteDTO.getOperacao()+"</p>"+
						"<p><b>CPF:</b> "+dadosBancariosIntegranteDTO.getCpf()+"</p>"+
					"</div><!-- .widget-body -->"+
				"</div><!-- .widget -->");

		document.getElementById("dadosIntegrante").setInnerHTML(html.toString());

	}

	public void visualizarResponsaveisPopup(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		RequisicaoViagemDTO requisicaoViagemDto = (RequisicaoViagemDTO) document.getBean();

		RequisicaoViagemService requisicaoViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, WebUtil.getUsuarioSistema(request));

		String responsaveis = requisicaoViagemService.getResponsaveisEtapaAtualRequisicao(requisicaoViagemDto.getIdSolicitacaoServico(), "autorizacao");

		document.getElementById("POPUP_VISUALIZARRESPONSAVEIS").setInnerHTML(responsaveis.replace(";", "<br />"));

		document.executeScript("$('#POPUP_VISUALIZARRESPONSAVEIS').dialog('open');");
	}
}
