/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.BIDashBoardSegurDTO;
import br.com.centralit.citcorpore.negocio.BIDashBoardSegurService;
import br.com.centralit.citcorpore.negocio.GrupoService;
import br.com.citframework.service.ServiceLocator;

public class CitDashboardsSeguranca extends AjaxFormAction{
	@Override
	public void load(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
        GrupoService grupoService = (GrupoService)ServiceLocator.getInstance().getService(GrupoService.class, null);
        Collection perfil = grupoService.list();
        request.setAttribute("perfil", perfil);
	}
	
	public void mostraSeguranca(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BIDashBoardSegurDTO biDashBoardSegurDTO = (BIDashBoardSegurDTO)document.getBean();
		BIDashBoardSegurService biDashBoardSegurService = (BIDashBoardSegurService)ServiceLocator.getInstance().getService(BIDashBoardSegurService.class, null);
		HTMLForm form = document.getForm("formPainel");
		document.executeScript("clearAllCheckBox()");
		
		Collection col = biDashBoardSegurService.findByIdDashBoard(biDashBoardSegurDTO.getIdDashBoard());
		if (col != null && col.size() > 0){
			biDashBoardSegurDTO.setPerfilSelecionado(new Integer[col.size()]);
			Integer[] idPerfs = new Integer[col.size()];
			int i = 0;
			for(Iterator it = col.iterator(); it.hasNext();){
				BIDashBoardSegurDTO citGerencialSegurancaDTO = (BIDashBoardSegurDTO)it.next();
				idPerfs[i] = citGerencialSegurancaDTO.getIdGrupo();
				i++;
			}
			biDashBoardSegurDTO.setPerfilSelecionado(idPerfs);
		}
		if(biDashBoardSegurDTO.getPerfilSelecionado() != null && biDashBoardSegurDTO.getPerfilSelecionado().length > 0){
		    for (int i = 0; i < biDashBoardSegurDTO.getPerfilSelecionado().length; i++) {
		        document.executeScript("selectCheckBoxByValue('" + biDashBoardSegurDTO.getPerfilSelecionado()[i] + "')");
            }
		}
		document.executeScript("hideAguarde()");
		//document.alert("Registro recuperado ! Defina os grupos de acesso e clique em Gravar!");        
	}
	
	public void gravarSeguranca(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BIDashBoardSegurDTO biDashBoardSegurDTO = (BIDashBoardSegurDTO)document.getBean();
		BIDashBoardSegurService biDashBoardSegurService = (BIDashBoardSegurService)ServiceLocator.getInstance().getService(BIDashBoardSegurService.class, null);
		biDashBoardSegurService.deleteByIdDashBoard(biDashBoardSegurDTO.getIdDashBoard());
		if (biDashBoardSegurDTO.getPerfilSelecionado() != null){
			for(int i = 0; i < biDashBoardSegurDTO.getPerfilSelecionado().length; i++){
				BIDashBoardSegurDTO citGerencialSegurancaDTO = new BIDashBoardSegurDTO();
				citGerencialSegurancaDTO.setIdDashBoard(biDashBoardSegurDTO.getIdDashBoard());
				citGerencialSegurancaDTO.setIdGrupo(biDashBoardSegurDTO.getPerfilSelecionado()[i]);
				biDashBoardSegurService.create(citGerencialSegurancaDTO);
			}
		}
		document.executeScript("hideAguarde()");
		document.alert("Perfil de seguran�a aplicado com sucesso!");    
	}	

	@Override
	public Class getBeanClass() {
		return BIDashBoardSegurDTO.class;
	}	
}
