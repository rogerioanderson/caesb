/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.ExportManualBIDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.bi.operation.BICitsmartOperation;
import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilI18N;

public class ExportManualBI extends AjaxFormAction {

	@Override
	public void load(DocumentHTML document, HttpServletRequest request,	HttpServletResponse response) throws Exception {
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request,"citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '"+ Constantes.getValue("SERVER_ADDRESS")+ request.getContextPath() + "'");
			return;
		}
	}

	@Override
	public Class getBeanClass() {
		return ExportManualBIDTO.class;
	}
	
	public void exportarDownload(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) {
		String idConexaoBI = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.BICITSMART_ID_CONEXAO, "");

		if (idConexaoBI == null || idConexaoBI.equals("")) {
			document.alert(UtilI18N.internacionaliza(request,"exportManualBI.exportFalha") + "\n" + UtilI18N.internacionaliza(request,"exportManualBI.exportFalhaIDConexaoNaoDefinido"));
		} else {
			document.executeScript("submitForm('formGetExportBI');");
		}
		
		document.executeScript("JANELA_AGUARDE_MENU.hide();");
	}
	
	public void exportar(DocumentHTML document, HttpServletRequest request,	HttpServletResponse response) throws Exception {
		ExportManualBIDTO exportManualBIDTO = (ExportManualBIDTO) document.getBean();
		exportManualBIDTO.setPasta(ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.CAMINHOEXPORTACAOMANUALBICITSMART, ""));
		BICitsmartOperation biCitsmartOperation = new BICitsmartOperation();
		
		String idConexaoBI = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.BICITSMART_ID_CONEXAO, "");
		if (idConexaoBI == null || idConexaoBI.equals("")) {
			document.executeScript("JANELA_AGUARDE_MENU.hide();");
			document.alert(UtilI18N.internacionaliza(request,"exportManualBI.exportFalha") + "\n" + UtilI18N.internacionaliza(request,"exportManualBI.exportFalhaIDConexaoNaoDefinido"));
			return;
		}
		
		if (!exportManualBIDTO.getPasta().trim().equals("")) {
			if (biCitsmartOperation.exportacaoManualBICitsmart(exportManualBIDTO.getPasta())){
				document.executeScript("JANELA_AGUARDE_MENU.hide();");
				document.alert(UtilI18N.internacionaliza(request,"exportManualBI.exportSucesso") + "\n" + UtilI18N.internacionaliza(request,"exportManualBI.caminho") + ": " + exportManualBIDTO.getPasta());
			} else {
				document.executeScript("JANELA_AGUARDE_MENU.hide();");
				document.alert(UtilI18N.internacionaliza(request,"exportManualBI.exportFalha"));
			}
		} else {
			document.executeScript("JANELA_AGUARDE_MENU.hide();");
			document.alert(UtilI18N.internacionaliza(request,"exportManualBI.exportFalha") + "\n" + UtilI18N.internacionaliza(request,"exportManualBI.exportFalhaCaminhoNaoDefinido"));			
		}
	}

}
