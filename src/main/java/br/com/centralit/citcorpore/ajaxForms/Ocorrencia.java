/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLTable;
import br.com.centralit.citcorpore.bean.OcorrenciaDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.OcorrenciaService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;

public class Ocorrencia extends AjaxFormAction {

	public Class getBeanClass() {
		return OcorrenciaDTO.class;
	}

	public void load(DocumentHTML arg0, HttpServletRequest arg1,
			HttpServletResponse arg2) throws Exception {
	}
	public void registrarRespostaOcorrencia(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		OcorrenciaService ocorrenciaService = (OcorrenciaService) ServiceLocator.getInstance().getService(OcorrenciaService.class, null);
		OcorrenciaDTO ocorrenciaBean = (OcorrenciaDTO)document.getBean();
		
		if (ocorrenciaBean.getRespostaOcorrencia()==null){
			document.alert("Resposta n�o informada! Favor informar a resposta!");
			return;
		}
		
		ocorrenciaService.updateResposta(ocorrenciaBean);
		
		document.executeScript("POPUP_ATUALIZAR_RESPOSTA_OCORR.hide()");
		
		consultarOcorrencia(document, request, response);
		
		document.alert("Resposta registrada com sucesso!");			
	}
	
	public void registrarOcorrencia(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		OcorrenciaService ocorrenciaService = (OcorrenciaService) ServiceLocator.getInstance().getService(OcorrenciaService.class, null);
		OcorrenciaDTO ocorrenciaBean = (OcorrenciaDTO)document.getBean();
		
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		ocorrenciaBean.setIdEmpregado(new Integer(usuario.getIdUsuario()));
				
		ocorrenciaService.create(ocorrenciaBean);
		
		document.executeScript("POPUP_OCORRENCIA.hide()");
		
		document.alert("Ocorr�ncia registrada com sucesso!");		
	}
	
	public void consultarOcorrencia(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		OcorrenciaService ocorrenciaService = (OcorrenciaService) ServiceLocator.getInstance().getService(OcorrenciaService.class, null);
		OcorrenciaDTO ocorrenciaBean = (OcorrenciaDTO)document.getBean();
		
		Collection col = ocorrenciaService.findByDemanda(ocorrenciaBean.getIdDemanda());
		
		HTMLTable tabelaConsultaTimeSheet = document.getTableById("tabelaConsultaOcorr");
		
		tabelaConsultaTimeSheet.deleteAllRows();
		tabelaConsultaTimeSheet.addRowsByCollection(col, 
				new String[] {"dataStr", "tipoOcorrenciaStr", "ocorrencia", "respostaOcorrencia", "nomeEmpregado"}, 
				null, 
				"J� existe registrado este registro na tabela", 
				null, 
				"CHAMA_AtualizaResposta", 
				null);
		tabelaConsultaTimeSheet.applyStyleClassInAllCells("tamanho10");		
	}	
}
