/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.AgendaAtvPeriodicasDTO;
import br.com.centralit.citcorpore.negocio.GrupoAtvPeriodicaService;
import br.com.centralit.citcorpore.negocio.MotivoSuspensaoAtividadeService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilI18N;

public class AgendaAtvPeriodicas extends AjaxFormAction {

	@SuppressWarnings("rawtypes")
	@Override
	public Class getBeanClass() {
		return AgendaAtvPeriodicasDTO.class;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void load(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int idGrupo = 0;
		String ID_GRUPO_ATV_PER = (String)request.getSession().getAttribute("ID_GRUPO_ATV_PER");
		if (ID_GRUPO_ATV_PER == null){
			ID_GRUPO_ATV_PER = "";
		}
		if (!ID_GRUPO_ATV_PER.equalsIgnoreCase("")){
			try{
				idGrupo = new Integer(ID_GRUPO_ATV_PER);
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

        request.getSession(true).setAttribute("colUploadsGED", null);
		HTMLSelect idGrupoAtvPeriodica = (HTMLSelect) document.getSelectById("idGrupoAtvPeriodica");
		GrupoAtvPeriodicaService grupoAtvPeriodicaService = (GrupoAtvPeriodicaService) ServiceLocator.getInstance().getService(GrupoAtvPeriodicaService.class, null);
		Collection colGrupos = grupoAtvPeriodicaService.listGrupoAtividadePeriodicaAtiva();
		idGrupoAtvPeriodica.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
		idGrupoAtvPeriodica.addOptions(colGrupos, "idGrupoAtvPeriodica", "nomeGrupoAtvPeriodica", "" + idGrupo);
		carregarComboMotivo(document, request, response);

        String grupoPesquisado = (String)request.getSession().getAttribute("idGrupoPesquisa");

        if (grupoPesquisado != null) {
        	int idGrupoPesquisa = Integer.parseInt(grupoPesquisado);
        	document.executeScript("setSelectGrupo(" + idGrupoPesquisa + ")");
        }



	}
	public void mudaGrupo(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		AgendaAtvPeriodicasDTO agendaAtvPeriodicasDTO = (AgendaAtvPeriodicasDTO)document.getBean();
		if (agendaAtvPeriodicasDTO != null && agendaAtvPeriodicasDTO.getIdGrupoPesquisa() != null) {
			request.getSession().setAttribute("idGrupoPesquisa", "" + agendaAtvPeriodicasDTO.getIdGrupoPesquisa());
		}
		if (agendaAtvPeriodicasDTO != null && agendaAtvPeriodicasDTO.getIdGrupoAtvPeriodica() != null){
			request.getSession().setAttribute("ID_GRUPO_ATV_PER", "" + agendaAtvPeriodicasDTO.getIdGrupoAtvPeriodica());
			document.executeScript("refresh()");
		}
	}

	public void carregarComboMotivo(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		((HTMLSelect) document.getSelectById("idMotivoSuspensao")).removeAllOptions();
		HTMLSelect idMotivoSuspensao = (HTMLSelect) document.getSelectById("idMotivoSuspensao");
		idMotivoSuspensao.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
		MotivoSuspensaoAtividadeService motivoSuspensaoService = (MotivoSuspensaoAtividadeService) ServiceLocator.getInstance().getService(MotivoSuspensaoAtividadeService.class, null);
	    Collection colMotivos = motivoSuspensaoService.listarMotivosSuspensaoAtividadeAtivos();
	    idMotivoSuspensao.addOptions(colMotivos, "idMotivo", "descricao", "");
	}
}
