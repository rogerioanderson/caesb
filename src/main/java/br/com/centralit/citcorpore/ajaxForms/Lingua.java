/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.LinguaDTO;
import br.com.centralit.citcorpore.negocio.LinguaService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;
@SuppressWarnings("rawtypes")
public class Lingua extends AjaxFormAction {

	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		
	}

	
	@Override
	public Class getBeanClass() {
		return LinguaDTO.class;
	}
	
	
	public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		LinguaDTO linguaDto = (LinguaDTO) document.getBean();
		
		LinguaService linguaService = (LinguaService) ServiceLocator.getInstance().getService(LinguaService.class, null);
		
		if(linguaDto.getIdLingua()==null || linguaDto.getIdLingua() == 0){
			if(linguaService.consultarLinguaAtivas(linguaDto)){
				document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.registroJaCadastrado"));
				return;
			}
			linguaDto.setDataInicio(UtilDatas.getDataAtual());
			linguaService.create(linguaDto);
			document.alert(UtilI18N.internacionaliza(request,"MSG05"));
			
		}else{
			if(linguaService.consultarLinguaAtivas(linguaDto)){
				document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.registroJaCadastrado"));
				return;
			}
			linguaService.update(linguaDto);
			document.alert(UtilI18N.internacionaliza(request,"MSG06"));
		}
		HTMLForm form = document.getForm("form");
		form.clear();
		
	}
	
	public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		LinguaDTO linguaDto = (LinguaDTO) document.getBean();
		
		LinguaService linguaService = (LinguaService) ServiceLocator.getInstance().getService(LinguaService.class, null);
		
		linguaDto = (LinguaDTO) linguaService.restore(linguaDto);
		
		HTMLForm form = document.getForm("form");
		form.clear();
		form.setValues(linguaDto);
	}
	
	public void delete(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		LinguaDTO linguaDto = (LinguaDTO) document.getBean();
		
		LinguaService linguaService = (LinguaService) ServiceLocator.getInstance().getService(LinguaService.class, null);
		
		if (linguaDto.getIdLingua().intValue() > 0 || linguaDto.getIdLingua()!=null) {
			linguaDto.setDataFim(UtilDatas.getDataAtual());
			linguaService.update(linguaDto);
			document.alert(UtilI18N.internacionaliza(request,"MSG07"));
		}
		
		HTMLForm form = document.getForm("form");
		form.clear();
		
	}

}
