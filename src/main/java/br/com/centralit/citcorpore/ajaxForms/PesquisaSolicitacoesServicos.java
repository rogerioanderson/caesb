/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;

import com.google.gson.Gson;

import br.com.centralit.bpm.dto.FluxoDTO;
import br.com.centralit.bpm.dto.PermissoesFluxoDTO;
import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.FaseServicoDTO;
import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.bean.OcorrenciaSolicitacaoDTO;
import br.com.centralit.citcorpore.bean.OrigemAtendimentoDTO;
import br.com.centralit.citcorpore.bean.PesquisaSolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.PrioridadeDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.TipoDemandaServicoDTO;
import br.com.centralit.citcorpore.bean.UnidadeDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.FaseServicoService;
import br.com.centralit.citcorpore.negocio.GrupoService;
import br.com.centralit.citcorpore.negocio.OcorrenciaSolicitacaoService;
import br.com.centralit.citcorpore.negocio.OrigemAtendimentoService;
import br.com.centralit.citcorpore.negocio.PermissoesFluxoService;
import br.com.centralit.citcorpore.negocio.PrioridadeService;
import br.com.centralit.citcorpore.negocio.ServicoContratoService;
import br.com.centralit.citcorpore.negocio.SolicitacaoServicoService;
import br.com.centralit.citcorpore.negocio.TipoDemandaServicoService;
import br.com.centralit.citcorpore.negocio.UnidadeService;
import br.com.centralit.citcorpore.negocio.UsuarioService;
import br.com.centralit.citcorpore.util.Arvore;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.centralit.citcorpore.util.LogoRel;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citcorpore.util.UtilRelatorio;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.centralit.citged.bean.ControleGEDDTO;
import br.com.centralit.citged.negocio.ControleGEDService;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;
import br.com.citframework.util.UtilStrings;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.fill.JRAbstractLRUVirtualizer;
import net.sf.jasperreports.engine.fill.JRGzipVirtualizer;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import net.sf.jasperreports.export.XlsxReportConfiguration;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class PesquisaSolicitacoesServicos extends AjaxFormAction {

	private UsuarioDTO usuario;

	private final String localeSession = null;

	@Override
	public Class getBeanClass() {
		return PesquisaSolicitacaoServicoDTO.class;
	}

	@Override
	public void load(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		usuario = WebUtil.getUsuario(request);

		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		document.getSelectById("idContrato").removeAllOptions();
		final ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
		final Collection colContrato = contratoService.list();
		document.getSelectById("idContrato").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");
		document.getSelectById("idContrato").addOptions(colContrato, "idContrato", "numero", null);

		document.getSelectById("idPrioridade").removeAllOptions();
		final PrioridadeService prioridadeService = (PrioridadeService) ServiceLocator.getInstance().getService(PrioridadeService.class, null);
		final Collection col = prioridadeService.list();
		document.getSelectById("idPrioridade").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");
		document.getSelectById("idPrioridade").addOptions(col, "idPrioridade", "nomePrioridade", null);

		document.getSelectById("idGrupoAtual").removeAllOptions();
		final GrupoService grupoSegurancaService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);
		final Collection colGrupos = grupoSegurancaService.findGruposAtivos();
		document.getSelectById("idGrupoAtual").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");
		document.getSelectById("idGrupoAtual").addOptions(colGrupos, "idGrupo", "nome", null);

		document.getSelectById("idFaseAtual").removeAllOptions();
		final FaseServicoService faseServicoService = (FaseServicoService) ServiceLocator.getInstance().getService(FaseServicoService.class, null);
		final Collection colFases = faseServicoService.list();
		document.getSelectById("idFaseAtual").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");
		document.getSelectById("idFaseAtual").addOptions(colFases, "idFase", "nomeFase", null);

		document.getSelectById("idOrigem").removeAllOptions();
		final OrigemAtendimentoService origemAtendimentoService = (OrigemAtendimentoService) ServiceLocator.getInstance().getService(OrigemAtendimentoService.class, null);
		final Collection colOrigem = origemAtendimentoService.list();
		document.getSelectById("idOrigem").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");
		document.getSelectById("idOrigem").addOptions(colOrigem, "idOrigem", "descricao", null);

		document.getSelectById("idTipoDemandaServico").removeAllOptions();
		final TipoDemandaServicoService tipoDemandaServicoService = (TipoDemandaServicoService) ServiceLocator.getInstance().getService(TipoDemandaServicoService.class, null);
		final Collection colTiposDemanda = tipoDemandaServicoService.list();
		document.getSelectById("idTipoDemandaServico").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");
		document.getSelectById("idTipoDemandaServico").addOptions(colTiposDemanda, "idTipoDemandaServico", "nomeTipoDemandaServico", null);

		ServiceLocator.getInstance().getService(UnidadeService.class, null);
		final String UNIDADE_AUTOCOMPLETE = ParametroUtil.getValorParametroCitSmartHashMap(br.com.centralit.citcorpore.util.Enumerados.ParametroSistema.UNIDADE_AUTOCOMPLETE, "N");
		StringBuilder objeto;

		if (UNIDADE_AUTOCOMPLETE != null && UNIDADE_AUTOCOMPLETE.equalsIgnoreCase("S")) {
			objeto = new StringBuilder();
			objeto.append("<input type='text' name='unidadeDes' id='unidadeDes' style='width: 100%;' onkeydown='onkeypressUnidadeDes();' onfocus='montaParametrosAutocompleteUnidade();'>");
			objeto.append("<input type='hidden' name='idUnidade' id='idUnidade' value='0'/>");
			document.getElementById("divUnidade").setInnerHTML(objeto.toString());
			document.executeScript("montaParametrosAutocompleteUnidade()");
		} else {
			objeto = new StringBuilder();
			objeto.append("<select  class='span12' name='idUnidade' id='idUnidade'></select>");
			document.getElementById("divUnidade").setInnerHTML(objeto.toString());
			this.preencherComboUnidade(document, request, response);
		}
		
		this.limpaRegistrosDaPesquisa(document);
	}

	/**
	 * M�todo de pagina��o para um mais eficiente.
	 * 
	 * @since 11/12/2014
	 * @author thyen.chang
	 */
	public void preencheSolicitacoesRelacionadas(final DocumentHTML document, final HttpServletRequest request,
			final HttpServletResponse response) throws ServiceException, Exception {
		
		usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript(
					"window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		request.getRequestURI();
		String paginacao;
		
		final PesquisaSolicitacaoServicoDTO pesquisaSolicitacaoServicoDto = (PesquisaSolicitacaoServicoDTO) document.getBean();

		if (request.getParameter("paginacao") != null && !request.getParameter("paginacao").equals("")) {
			paginacao = UtilStrings.decodeCaracteresEspeciais(request.getParameter("paginacao"));
		} else {
			paginacao = "0";
		}

		Integer pagAtual;
		if (pesquisaSolicitacaoServicoDto.getTotalItens().intValue()>0){
			final Integer quantidadeItensPorPagina = pesquisaSolicitacaoServicoDto.getTotalItensPorPagina();
			pagAtual = pesquisaSolicitacaoServicoDto.getPagAtual();
			
			//Definindo qual ser� a p�gina que ser� aberta
			switch (paginacao) { 
			case "-1" : 						//=>Registro anterior
				pagAtual = pagAtual - 1;
				if (pagAtual<=0){
					pagAtual = 1;
				}
				break;
			case "1" : 					    	//=>Pr�ximo registro
				pagAtual = pagAtual + 1;
				if (pagAtual>pesquisaSolicitacaoServicoDto.getTotalPagina()){
					pagAtual = pesquisaSolicitacaoServicoDto.getTotalPagina();
				}
				break;
			case "2" : 							//=>�ltimo registro
				pagAtual = pesquisaSolicitacaoServicoDto.getTotalPagina();
				break;
			default:							//paginacao="0" ou qualquer valor => Primeiro registro
				pagAtual = 1;
				break;
			}

			final SolicitacaoServicoService solicitacaoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);

			// Passamos o usu�rio para que o sistema possa obter os IDs das unidades
			// que ele pode acessar!
			pesquisaSolicitacaoServicoDto.setUsuarioLogado(usuario);

			final ArrayList<SolicitacaoServicoDTO> listaSolicitacaoServicoPorCriterios = (ArrayList<SolicitacaoServicoDTO>) solicitacaoService.listPesquisaAvancadaPaginada(pesquisaSolicitacaoServicoDto, pagAtual, quantidadeItensPorPagina);

			final StringBuilder script = new StringBuilder();
			if (listaSolicitacaoServicoPorCriterios != null) {
				document.getElementById("tblResumo").setInnerHTML(
						this.montaHTMLResumoSolicitacoes(listaSolicitacaoServicoPorCriterios, script, request, response));
			} else {
				document.getElementById("tblResumo")
						.setInnerHTML(UtilI18N.internacionaliza(request, "citcorpore.comum.validacao.criterioinformado"));
			}			
		} else {
			pagAtual = 0;
			document.getElementById("tblResumo").setInnerHTML(UtilI18N.internacionaliza(request, "citcorpore.comum.validacao.criterioinformado"));
		}
		
		document.getElementById("paginaAtual").setInnerHTML(pagAtual.toString());
		document.getElementById("pagAtual").setValue(pagAtual.toString());

		document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();

	}

	public void setNumeroPaginas(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws ServiceException, Exception {
		
		final PesquisaSolicitacaoServicoDTO pesquisaSolicitacaoServicoDto = (PesquisaSolicitacaoServicoDTO) document.getBean();

		final SolicitacaoServicoService solicitacaoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);

		Integer noTotalItens, noTotalPaginas, quantidadePorPagina, paginaAtual;

		// Passamos o usu�rio para que o sistema possa obter os IDs das unidades que ele pode acessar!
		pesquisaSolicitacaoServicoDto.setUsuarioLogado(WebUtil.getUsuario(request));

		final String origemBusca = request.getParameter("origemSolicitacao");

		final boolean isDetalhado = origemBusca.equals("pdfdetalhado")||origemBusca.equals("xlsdetalhado");

		noTotalItens = solicitacaoService.listaRelatorioGetQuantidadeRegistros(pesquisaSolicitacaoServicoDto, isDetalhado).intValue();

		if (noTotalItens.intValue()>0){
			paginaAtual = 1;
			quantidadePorPagina = Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.QUANT_RETORNO_PESQUISA, "5"));
			noTotalPaginas = (int) Math.ceil(noTotalItens.doubleValue() / quantidadePorPagina.doubleValue());
		} else {
			paginaAtual = 0;
			quantidadePorPagina = 0;
			noTotalPaginas = 0;
		}
		
		document.getElementById("paginacao").setValue("0");
		document.getElementById("pagAtual").setValue(paginaAtual.toString());
		document.getElementById("paginaAtual").setInnerHTML(paginaAtual.toString());
        document.getElementById("paginaTotal").setInnerHTML(noTotalPaginas.toString());
        document.getElementById("totalPagina").setValue(noTotalPaginas.toString());
        document.getElementById("totalItens").setValue(noTotalItens.toString());
        document.getElementById("totalItensPorPagina").setValue(quantidadePorPagina.toString());

        switch (request.getParameter("origemSolicitacao")) {
        case "pesquisa":
        	document.executeScript("executaPesquisa('pesquisa');");
        	break;
        case "pdf":
        	document.executeScript("executaPesquisa('pdf');");
        	break;
        case "xls":
        	document.executeScript("executaPesquisa('xls');");
        	break;
        case "pdfdetalhado":
        	document.executeScript("executaPesquisa('pdfdetalhado');");
        	break;
        case "xlsdetalhado":
        	document.executeScript("executaPesquisa('xlsdetalhado');");
        	break;
        }
	}

	private String montaHTMLResumoSolicitacoes(final ArrayList<SolicitacaoServicoDTO> resumo, final StringBuilder script, final HttpServletRequest request, final HttpServletResponse response)
			throws ServiceException, Exception {
		usuario = WebUtil.getUsuario(request);
		final StringBuilder html = new StringBuilder();
		final SolicitacaoServicoService solicitacaoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, WebUtil.getUsuarioSistema(request));
		final PermissoesFluxoService permissoesFluxoService = (PermissoesFluxoService) ServiceLocator.getInstance().getService(PermissoesFluxoService.class, null);
		/* Foi necess�rio diminuir a fonte e porcetagem da largura da table para adequear ao modal */
		html.append("<table class='dynamicTable table  table-bordered table-condensed dataTable' id='tbRetorno' width='98%' style = 'font-size: 9px' >");
		html.append("<tr>");
		html.append("<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>");
		html.append("<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>");
		html.append("<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>");
		html.append("<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "solicitacaoServico.solicitacao") + "</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "solicitacaoServico.solicitante") + "</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "citcorpore.comum.criadopor") + "</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "citcorpore.comum.tipo") + "</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "solicitacaoServico.datahoraabertura") + "</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "gerenciaservico.sla") + "</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "solicitacaoServico.descricao") + "</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "solicitacaoServico.solucaoResposta") + "</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "servico.servico") + "</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "solicitacaoServico.situacao") + "</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "solicitacaoServico.datahoralimite") + "</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "unidade.grupo") + "</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "solicitacaoServico.datahoraencerramento") + "</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "solicitacaoServico.temporestante"));
		html.append("<img width='20' height='20'");
		html.append("alt='" + UtilI18N.internacionaliza(request, "citcorpore.comum.ativaotemporizador") + "' id='imgAtivaTimer' style='opacity:0.5' ");
		html.append("title='" + UtilI18N.internacionaliza(request, "citcorpore.comum.ativadestemporizador") + "'");
		html.append("src='" + br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO") + "/template_new/images/cronometro.png'/>");
		html.append("</th>");
		html.append("<th>" + UtilI18N.internacionaliza(request, "solicitacaoServico.responsavelatual.desc"));
		html.append("</tr>");
		final HashMap<String, PermissoesFluxoDTO> mapPermissoes = new HashMap();
		for (final SolicitacaoServicoDTO r : resumo) {
			final SolicitacaoServicoDTO solDto = new SolicitacaoServicoDTO();
			solDto.setIdSolicitacaoServico(r.getIdSolicitacaoServico());
			final FluxoDTO fluxoDto = solicitacaoService.recuperaFluxo(solDto);
			if (fluxoDto == null) {
				continue;
			}
			PermissoesFluxoDTO permFluxoDto = mapPermissoes.get("" + fluxoDto.getIdFluxo());
			html.append("<tr>");
			html.append("<hidden id='idSolicitante' value='" + r.getIdSolicitante() + "'/>");
			html.append("<hidden id='idResponsavel' value='" + r.getIdResponsavel() + "'/>");
			html.append("<hidden id='idUsuarioResponsavelAtual' value='" + r.getIdUsuarioResponsavelAtual() + "'/>");
			if (permFluxoDto == null) {
				permFluxoDto = permissoesFluxoService.findByUsuarioAndFluxo(usuario, fluxoDto);
				if (permFluxoDto != null) {
					mapPermissoes.put("" + fluxoDto.getIdFluxo(), permFluxoDto);
				}
			}

			html.append("<td><img src='" + br.com.citframework.util.Constantes.getValue("SERVER_ADDRESS") + br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")
					+ "/imagens/search.png' border='0' title='" + UtilI18N.internacionaliza(request, "pesquisasolicitacao.consultasolicitacaoincidente") + "' onclick='consultarOcorrencias(\""
					+ r.getIdSolicitacaoServico() + "\")' style='cursor:pointer'/></td>");
			String idTarefaStr = "null";
			if (r.getIdTarefa() != null) {
				idTarefaStr = r.getIdTarefa().toString();
			}
			html.append("<td>" + "<a href='javascript:visualizarSolicitacao(" + r.getIdSolicitacaoServico() + ", " + idTarefaStr + ")' >" + "" + UtilI18N.internacionaliza(request, "citcorpore.comum.visualizar") + "</a>" + "</td>");
			if (permFluxoDto != null && permFluxoDto.getReabrir() != null && permFluxoDto.getReabrir().equalsIgnoreCase("S")) {
				if (r.encerrada()) {
					html.append("<td><img src='" + br.com.citframework.util.Constantes.getValue("SERVER_ADDRESS") + br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")
							+ "/imagens/reabrir.jpg' border='0' title='" + UtilI18N.internacionaliza(request, "pesquisasolicitacao.reabrirsol") + "' onclick='reabrir(\"" + r.getIdSolicitacaoServico()
							+ "\")' style='cursor:pointer'/></td>");
				} else {
					html.append("<td>&nbsp;</td>");
				}
			} else {
				html.append("<td>&nbsp;</td>");
			}
			final ControleGEDService controleGedService = (ControleGEDService) ServiceLocator.getInstance().getService(ControleGEDService.class, null);
			final Collection colAnexos = controleGedService.listByIdTabelaAndID(ControleGEDDTO.TABELA_SOLICITACAOSERVICO, r.getIdSolicitacaoServico());

			if (colAnexos != null && !colAnexos.isEmpty()) {
				html.append("<td><img src='" + br.com.citframework.util.Constantes.getValue("SERVER_ADDRESS") + br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")
						+ "/imagens/Paperclip4-black-32.png' width='16' height='16' border='0' title='" + UtilI18N.internacionaliza(request, "pesquisasolicitacao.visualizaranexos")
						+ "' id='btAnexos' onclick='anexos(\"" + r.getIdSolicitacaoServico() + "\")' style='cursor:pointer'/></td>");
			} else {
				html.append("<td><img src='" + br.com.citframework.util.Constantes.getValue("SERVER_ADDRESS") + br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")
						+ "/imagens/file.png' width='16' height='16' border='0' title='" + UtilI18N.internacionaliza(request, "pesquisasolicitacao.visualizaranexos")
						+ "' id='btAnexos' onclick='anexos(\"" + r.getIdSolicitacaoServico() + "\")' style='cursor:pointer'/></td>");
			}
			html.append("<td>" + r.getIdSolicitacaoServico() + "</td>");
			html.append("<td>" + r.getNomeSolicitante() + "</td>");
			html.append("<td>" + r.getResponsavel() + "</td>");
			html.append("<td>" + r.getNomeTipoDemandaServico() + "</td>");
			if (r.getSeqReabertura() == null || r.getSeqReabertura().intValue() == 0) {
				html.append("<td id='dataHoraSolicitacao'>" + UtilDatas.convertDateToString(TipoDate.TIMESTAMP_WITH_SECONDS, r.getDataHoraSolicitacao(), WebUtil.getLanguage(request)) + "</td>");
			} else {
				html.append("<td id='dataHoraSolicitacao'>" + UtilDatas.convertDateToString(TipoDate.TIMESTAMP_WITH_SECONDS, r.getDataHoraSolicitacao(), WebUtil.getLanguage(request)) + "<br><br>"
						+ UtilI18N.internacionaliza(request, "solicitacaoServico.seqreabertura") + ": <span style='color:red'><b>" + r.getSeqReabertura() + "</b></span></td>");
			}

			final boolean slaACombinar = (r.getPrazoHH() == null || r.getPrazoHH() == 0) && (r.getPrazoMM() == null || r.getPrazoMM() == 0);

			if (slaACombinar) {
				html.append("<td>" + UtilI18N.internacionaliza(request, "citcorpore.comum.aCombinar") + "</td>");
			} else {
				String hh = r.getPrazoHH().toString();
				String mm = r.getPrazoMM().toString();
				if(Integer.parseInt(mm) > 59){
					hh = String.valueOf(Integer.parseInt(hh) + (Integer.parseInt(mm) / 60)); 
					mm = String.valueOf((Integer.parseInt(mm) % 60));
				}
				if (mm.length() == 1) {
					mm = "0" + mm;
				}
				html.append("<td>" + hh + ":" + mm + "</td>");
			}

			html.append("<td>" + UtilStrings.nullToVazio(UtilStrings.unescapeJavaString(r.getDescricaoSemFormatacao())) + "</td>");
			html.append("<td>" + UtilStrings.nullToVazio(UtilStrings.unescapeJavaString(r.getResposta())) + "</td>");
			html.append("<td>" + r.getNomeServico().replace(".", ". ") + "</td>");

			/*
			 * -- Para internacionalizar corretamente -- A query possui um case when para colocar "Em Andamento" se a situa��o for "EmAndamento" Essa query � utilizada em outros programas Por esse
			 * motivo, o mais simples e menos arriscado � colocar esse teste em vez de mudar a query e arriscar bagun�ar em outro lugar Quando a query for melhorada retirando o case when, esse if n�o
			 * ser� mais utilizado Uelen Paulo - 26/09/2013
			 */
			if (r.getSituacao().equalsIgnoreCase("Em Andamento")) {

				r.setSituacao("EmAndamento");
			}

			html.append("<td>" + r.obterSituacaoInternacionalizada(request) + "</td>");

			if (r.getDataHoraLimite() != null && !slaACombinar) {
				html.append("<td>" + UtilDatas.convertDateToString(TipoDate.TIMESTAMP_WITH_SECONDS, r.getDataHoraLimite(), WebUtil.getLanguage(request)) + "</td>");
			} else {
				html.append("<td>&nbsp;</td>");
			}
			html.append("<td>" + UtilStrings.nullToVazio(r.getSiglaGrupo()) + "</td>");
			String d = "";
			if (r.getDataHoraFim() != null) {
				d = UtilDatas.convertDateToString(TipoDate.TIMESTAMP_WITH_SECONDS, r.getDataHoraFim(), WebUtil.getLanguage(request));
			}
			html.append("<td id='dataHoraFimSolicitacao'>" + d + "</td>");
			if (r.getSituacao().equals("EmAndamento")) {
				script.append("temporizador.addOuvinte(new Solicitacao('tempoRestante" + r.getIdSolicitacaoServico() + "', " + "'barraProgresso" + r.getIdSolicitacaoServico() + "', " + "'"
						+ r.getDataHoraSolicitacao() + "', '" + r.getDataHoraLimite() + "'));");
			}
			html.append("<td><label id='tempoRestante" + r.getIdSolicitacaoServico() + "'></label>");
			html.append("<div id='barraProgresso" + r.getIdSolicitacaoServico() + "'></div></td>");

			if (r.getResponsavelAtual() != null) {
				html.append("<td>" + r.getResponsavelAtual() + "</td>");
			} else {
				html.append("<td> </td>");
			}

			html.append("</tr>");
		}
		html.append("</table>");
		return html.toString();
	}

	public void reabre(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws ServiceException, Exception {
		usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}
		final PesquisaSolicitacaoServicoDTO pesquisaSolicitacaoServicoDto = (PesquisaSolicitacaoServicoDTO) document.getBean();
		SolicitacaoServicoDTO solicitacaoServicoDto = new SolicitacaoServicoDTO();
		final SolicitacaoServicoService solicitacaoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);
		if (pesquisaSolicitacaoServicoDto.getIdSolicitacaoServico() == null) {
			document.alert(UtilI18N.internacionaliza(request, "pesquisasolicitacao.informeReabrir"));
			return;
		} else {
			solicitacaoServicoDto.setIdSolicitacaoServico(pesquisaSolicitacaoServicoDto.getIdSolicitacaoServico());
			solicitacaoServicoDto = (SolicitacaoServicoDTO) solicitacaoService.restore(solicitacaoServicoDto);

			int numDias;
			int numDiasParametro;
			boolean permiteReabrir = false;

			try {

				numDias = UtilDatas.dataDiff(solicitacaoServicoDto.getDataHoraFim(), UtilDatas.getDataAtual());
				numDiasParametro = Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.DIAS_LIMITE_REABERTURA_INCIDENTE_REQUISICAO, null));

				if (numDias <= numDiasParametro) {
					permiteReabrir = true;
				}
			} catch (final NumberFormatException ne) {
				document.alert(UtilI18N.internacionaliza(request, "pesquisasolicitacao.prazoReaberturaNaoConfigurado"));
				return;
			}

			if (!permiteReabrir) {
				document.alert(UtilI18N.internacionaliza(request, "pesquisasolicitacao.prazoReaberturaExcedido"));
				return;
			} else {
				solicitacaoService.reabre(usuario, solicitacaoServicoDto);
			}
		}

		document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.reaberta"));
		document.executeScript("filtrar()");
	}

	public void verificaServicoAtivoSolicitacao(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws ServiceException, Exception {
		usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}
		final PesquisaSolicitacaoServicoDTO pesquisaSolicitacaoServicoDto = (PesquisaSolicitacaoServicoDTO) document.getBean();
		final ServicoContratoService servicoContratoService = (ServicoContratoService) ServiceLocator.getInstance().getService(ServicoContratoService.class, null);
		if (pesquisaSolicitacaoServicoDto.getIdSolicitacaoServico() == null) {
			document.alert(UtilI18N.internacionaliza(request, "pesquisasolicitacao.informeReabrir"));
			return;
		} else {
			final boolean seServicoAtivo = servicoContratoService.verificaServicoEstaVinculadoContrato(pesquisaSolicitacaoServicoDto.getIdSolicitacaoServico());
			if (!seServicoAtivo) {
				document.executeScript("verificaServico('" + pesquisaSolicitacaoServicoDto.getIdSolicitacaoServico() + "')");
			} else {
				this.reabre(document, request, response);
			}
		}

	}

	/**
	 * alterado por rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
	 * data altera��o rcs: 02/10/2015
	 * coment�rio altera��o rcs: modulariza��o de c�digo.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
        public void imprimirRelatorio(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
            usuario = WebUtil.getUsuario(request);
    
            if (usuario == null) {
                document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
                document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
                document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
                return;
            }
    
            final PesquisaSolicitacaoServicoDTO pesquisaSolicitacaoServicoDto = (PesquisaSolicitacaoServicoDTO) document.getBean();
    
            if (!Objects.equals(pesquisaSolicitacaoServicoDto, null)) {
                final SolicitacaoServicoService solicitacaoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);
                pesquisaSolicitacaoServicoDto.setUsuarioLogado(usuario);// Passamos o usu�rio para que o sistema possa obter os IDs das unidades que ele pode acessar!
                
                ArrayList<SolicitacaoServicoDTO> listaSolicitacaoServicoPorCriterios = (ArrayList<SolicitacaoServicoDTO>) solicitacaoService.listPesquisaAvancadaRelatorio(pesquisaSolicitacaoServicoDto);
                
                if (CollectionUtils.isEmpty(listaSolicitacaoServicoPorCriterios)) {
                    document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
                } else {
                	
            		if (!ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.QUANTIDADE_REGISTROS_PESQUISA_AVANCADA, "-1").equals("-1")
            				&& listaSolicitacaoServicoPorCriterios.size() > Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.QUANTIDADE_REGISTROS_PESQUISA_AVANCADA, "0"))) {
            			document.alert(UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.registroMaiorPermitido"));
            			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
            			return;
            		}
                	
                    final StringBuilder sb_podeReceberVariosValores = new StringBuilder();
                    this.trataCamposDasSolicitacoesParaImprimirRelatorio(pesquisaSolicitacaoServicoDto, listaSolicitacaoServicoPorCriterios, request);
                    Map<String, Object> parametros = new HashMap<String, Object>();
                    parametros = UtilRelatorio.trataInternacionalizacaoLocale(request.getSession(), parametros);
                    this.putParametrosParaImprimirRelatorio(parametros, pesquisaSolicitacaoServicoDto, request, listaSolicitacaoServicoPorCriterios.size());
                    
                    sb_podeReceberVariosValores.append(CITCorporeUtil.CAMINHO_REAL_APP );
                    sb_podeReceberVariosValores.append(Constantes.getValue("CAMINHO_RELATORIOS"));
                    sb_podeReceberVariosValores.append("RelatorioPesquisaSolicitacaoServico.jasper");
                    final String caminhoJasper = sb_podeReceberVariosValores.toString();
                    
                    sb_podeReceberVariosValores.setLength(0);
                    sb_podeReceberVariosValores.append(CITCorporeUtil.CAMINHO_REAL_APP);
                    sb_podeReceberVariosValores.append("/tempFiles");
                    final String diretorioReceita = sb_podeReceberVariosValores.toString();
                    
                    sb_podeReceberVariosValores.setLength(0);
                    sb_podeReceberVariosValores.append(Constantes.getValue("SERVER_ADDRESS"));
                    sb_podeReceberVariosValores.append(Constantes.getValue("CONTEXTO_APLICACAO"));
                    sb_podeReceberVariosValores.append("/tempFiles");
                    final String diretorioRelativoOS = sb_podeReceberVariosValores.toString();
                    
                    sb_podeReceberVariosValores.setLength(0);
                    final String strCompl = sb_podeReceberVariosValores.append(new Date().getTime()).toString();
                    
                    final Timestamp ts1 = UtilDatas.getDataHoraAtual();
    
                    try {
                        final JasperPrint jp = JasperFillManager.fillReport(caminhoJasper, parametros, new JRBeanCollectionDataSource(listaSolicitacaoServicoPorCriterios));
                        final Timestamp ts2  = UtilDatas.getDataHoraAtual();
                        final double tempo   = UtilDatas.calculaDiferencaTempoEmMilisegundos(ts2, ts1);
                        
                        sb_podeReceberVariosValores.setLength(0);
                        sb_podeReceberVariosValores.append("########## Tempo fillReport: ");
                        sb_podeReceberVariosValores.append(tempo);
                        System.out.println(sb_podeReceberVariosValores.toString());
                        
                        sb_podeReceberVariosValores.setLength(0);
                        sb_podeReceberVariosValores.append(diretorioReceita);
                        sb_podeReceberVariosValores.append("/RelatorioSolicitacaoServico");
                        sb_podeReceberVariosValores.append(strCompl);
                        sb_podeReceberVariosValores.append("_");
                        sb_podeReceberVariosValores.append(usuario.getIdUsuario());
                        sb_podeReceberVariosValores.append(".pdf");
                        JasperExportManager.exportReportToPdfFile(jp, sb_podeReceberVariosValores.toString());
                        
                        sb_podeReceberVariosValores.setLength(0);
                        sb_podeReceberVariosValores.append("window.open('");
                        sb_podeReceberVariosValores.append(Constantes.getValue("SERVER_ADDRESS"));
                        sb_podeReceberVariosValores.append(Constantes.getValue("CONTEXTO_APLICACAO"));
                        sb_podeReceberVariosValores.append("/printPDF/printPDF.jsp?url=");
                        sb_podeReceberVariosValores.append(diretorioRelativoOS);
                        sb_podeReceberVariosValores.append("/RelatorioSolicitacaoServico");
                        sb_podeReceberVariosValores.append(strCompl);
                        sb_podeReceberVariosValores.append("_");
                        sb_podeReceberVariosValores.append(usuario.getIdUsuario());
                        sb_podeReceberVariosValores.append(".pdf')");
                        document.executeScript(sb_podeReceberVariosValores.toString());
                    } catch (JRException jRExc) {
                        jRExc.printStackTrace();
                    } catch (final OutOfMemoryError e) { // TODO capturar error n�o n� amig�o??
                        /*
                         * Desenvolvedor: Thiago Matias - Data: 30/10/2013 - Hor�rio: 15h35min - ID Citsmart: 122665 - Motivo/Coment�rio: alterando o a chave de citcorpore.erro.memoria para
                         * citsmart.erro.memoria
                         */
                        e.printStackTrace();
                        document.alert(UtilI18N.internacionaliza(request, "citsmart.erro.memoria"));
                    } catch (Exception exc) {
                        exc.printStackTrace();
                    }
                }
            }
    
            document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
            this.limpaRegistrosDaPesquisa(document);
        }
        
        final private void putParametrosParaImprimirRelatorio(final Map<String, Object> parametros, final PesquisaSolicitacaoServicoDTO pesquisaSolicitacaoServicoDto, final HttpServletRequest request,
                final int qtdSolicitacoes) throws Exception {
    
            if (!Objects.equals(pesquisaSolicitacaoServicoDto, null) && !Objects.equals(parametros, null)) {
                UsuarioDTO usuarioDto = new UsuarioDTO();
                final UsuarioService usuarioService = (UsuarioService) ServiceLocator.getInstance().getService(UsuarioService.class, null);
    
                parametros.put("TITULO_RELATORIO", UtilI18N.internacionaliza(request, "citcorporeRelatorio.pesquisaSolicitacoesServicos"));
                parametros.put("CIDADE", getCidadeParametrizada(request));
                parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
                parametros.put("NOME_USUARIO", usuario.getNomeUsuario());
                parametros.put("dataInicio", pesquisaSolicitacaoServicoDto.getDataInicio() == null ? pesquisaSolicitacaoServicoDto.getDataInicioFechamento() : pesquisaSolicitacaoServicoDto.getDataInicio());
                parametros.put("dataFim", pesquisaSolicitacaoServicoDto.getDataFim() == null ? pesquisaSolicitacaoServicoDto.getDataFimFechamento() : pesquisaSolicitacaoServicoDto.getDataFim());
                parametros.put("Logo", LogoRel.getFile());
                parametros.put("exibirCampoDescricao", pesquisaSolicitacaoServicoDto.getExibirCampoDescricao());
                parametros.put("quantidade", qtdSolicitacoes);
                parametros.put("criado_por", UtilI18N.internacionaliza(request, "citcorpore.comum.criadopor") + ":");
    
                if (StringUtils.isNotBlank(pesquisaSolicitacaoServicoDto.getNomeItemConfiguracao())) {
                    parametros.put("nomeItemConfiguracao", pesquisaSolicitacaoServicoDto.getNomeItemConfiguracao());
                } else {
                    parametros.put("nomeItemConfiguracao", null);
                }
    
                if (StringUtils.isNotBlank(pesquisaSolicitacaoServicoDto.getNomeSolicitante())) {
                    parametros.put("nomeSolicitante", pesquisaSolicitacaoServicoDto.getNomeSolicitante());
                } else {
                    parametros.put("nomeSolicitante", null);
                }
    
                if (pesquisaSolicitacaoServicoDto.getIdTipoDemandaServico() != null) {
                    final TipoDemandaServicoService tipoDemandaServicoService = (TipoDemandaServicoService) ServiceLocator.getInstance().getService(TipoDemandaServicoService.class, null);
                    TipoDemandaServicoDTO tipoDemandaServicoDto = new TipoDemandaServicoDTO();
                    tipoDemandaServicoDto.setIdTipoDemandaServico(pesquisaSolicitacaoServicoDto.getIdTipoDemandaServico());
                    tipoDemandaServicoDto = (TipoDemandaServicoDTO) tipoDemandaServicoService.restore(tipoDemandaServicoDto);
                    pesquisaSolicitacaoServicoDto.setNomeTipoDemandaServico(tipoDemandaServicoDto.getNomeTipoDemandaServico());
                    parametros.put("tipo", pesquisaSolicitacaoServicoDto.getNomeTipoDemandaServico());
                } else {
                    parametros.put("tipo", pesquisaSolicitacaoServicoDto.getNomeTipoDemandaServico());
                }
    
                if (pesquisaSolicitacaoServicoDto.getIdSolicitacaoServicoPesquisa() != null) {
                    parametros.put("numero", pesquisaSolicitacaoServicoDto.getIdSolicitacaoServicoPesquisa());
                } else {
                    parametros.put("numero", pesquisaSolicitacaoServicoDto.getIdSolicitacaoServicoPesquisa());
                }
    
                if (StringUtils.isNotBlank(pesquisaSolicitacaoServicoDto.getSituacao())) {
                    parametros.put("situacao", pesquisaSolicitacaoServicoDto.getSituacaoInternacionalizada(request));
                } else {
                    parametros.put("situacao", null);
                }
    
                if (pesquisaSolicitacaoServicoDto.getIdGrupoAtual() != null) {
                    GrupoDTO grupoDto = new GrupoDTO();
                    final GrupoService grupoSegurancaService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);
                    grupoDto.setIdGrupo(pesquisaSolicitacaoServicoDto.getIdGrupoAtual());
                    grupoDto = (GrupoDTO) grupoSegurancaService.restore(grupoDto);
                    pesquisaSolicitacaoServicoDto.setGrupoAtual(grupoDto.getSigla());
                    parametros.put("grupoSolucionador", pesquisaSolicitacaoServicoDto.getGrupoAtual());
                } else {
                    parametros.put("grupoSolucionador", pesquisaSolicitacaoServicoDto.getGrupoAtual());
                }
    
                if (pesquisaSolicitacaoServicoDto.getIdOrigem() != null) {
                    OrigemAtendimentoDTO origemDto = new OrigemAtendimentoDTO();
                    final OrigemAtendimentoService origemAtendimentoService = (OrigemAtendimentoService) ServiceLocator.getInstance().getService(OrigemAtendimentoService.class, null);
                    origemDto.setIdOrigem(pesquisaSolicitacaoServicoDto.getIdOrigem());
                    origemDto = (OrigemAtendimentoDTO) origemAtendimentoService.restore(origemDto);
                    pesquisaSolicitacaoServicoDto.setOrigem(UtilStrings.unescapeJavaString(origemDto.getDescricao()));
                    parametros.put("origem", pesquisaSolicitacaoServicoDto.getOrigem());
                } else {
                    parametros.put("origem", pesquisaSolicitacaoServicoDto.getOrigem());
                }
    
                if (pesquisaSolicitacaoServicoDto.getIdFaseAtual() != null) {
                    FaseServicoDTO faseDto = new FaseServicoDTO();
                    final FaseServicoService faseServicoService = (FaseServicoService) ServiceLocator.getInstance().getService(FaseServicoService.class, null);
                    faseDto.setIdFase(pesquisaSolicitacaoServicoDto.getIdFaseAtual());
                    faseDto = (FaseServicoDTO) faseServicoService.restore(faseDto);
                    pesquisaSolicitacaoServicoDto.setFaseAtual(faseDto.getNomeFase());
                    parametros.put("fase", pesquisaSolicitacaoServicoDto.getFaseAtual());
                } else {
                    parametros.put("fase", pesquisaSolicitacaoServicoDto.getFaseAtual());
                }
    
                if (pesquisaSolicitacaoServicoDto.getIdPrioridade() != null) {
                    PrioridadeDTO prioridadeDto = new PrioridadeDTO();
                    final PrioridadeService prioridadeService = (PrioridadeService) ServiceLocator.getInstance().getService(PrioridadeService.class, null);
                    prioridadeDto.setIdPrioridade(pesquisaSolicitacaoServicoDto.getIdPrioridade());
                    prioridadeDto = (PrioridadeDTO) prioridadeService.restore(prioridadeDto);
                    pesquisaSolicitacaoServicoDto.setPrioridade(prioridadeDto.getNomePrioridade());
                    parametros.put("prioridade", pesquisaSolicitacaoServicoDto.getPrioridade());
                } else {
                    parametros.put("prioridade", pesquisaSolicitacaoServicoDto.getPrioridade());
                }
    
                if (pesquisaSolicitacaoServicoDto.getIdContrato() != null) {
                    ContratoDTO contratoDto = new ContratoDTO();
                    final ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
                    contratoDto.setIdContrato(pesquisaSolicitacaoServicoDto.getIdContrato());
                    contratoDto = (ContratoDTO) contratoService.restore(contratoDto);
                    parametros.put("contrato", contratoDto.getNumero());
                } else {
                    parametros.put("contrato", null);
                }
    
                if (pesquisaSolicitacaoServicoDto.getIdResponsavel() != null) {
                    usuarioDto.setIdUsuario(pesquisaSolicitacaoServicoDto.getIdResponsavel());
                    usuarioDto = (UsuarioDTO) usuarioService.restore(usuarioDto);
                    parametros.put("responsavel", usuarioDto.getNomeUsuario());
                } else {
                    parametros.put("responsavel", null);
                }
    
                if (pesquisaSolicitacaoServicoDto.getIdUsuarioResponsavelAtual() != null) {
                    usuarioDto.setIdUsuario(pesquisaSolicitacaoServicoDto.getIdUsuarioResponsavelAtual());
                    usuarioDto = (UsuarioDTO) usuarioService.restore(usuarioDto);
                    parametros.put("nomeUsuarioResponsavelAtual", usuarioDto.getNomeUsuario());
                } else {
                    parametros.put("nomeUsuarioResponsavelAtual", null);
                }
    
                if (pesquisaSolicitacaoServicoDto.getIdUnidade() != null) {
                    UnidadeDTO unidadeDto = new UnidadeDTO();
                    final UnidadeService unidadeService = (UnidadeService) ServiceLocator.getInstance().getService(UnidadeService.class, null);
                    unidadeDto.setIdUnidade(pesquisaSolicitacaoServicoDto.getIdUnidade());
                    unidadeDto = (UnidadeDTO) unidadeService.restore(unidadeDto);
    
                    if (unidadeDto != null && StringUtils.isNotBlank(unidadeDto.getNome())) {
                        parametros.put("unidade", unidadeDto.getNome());
                    } else {
                        parametros.put("unidade", null);
                    }
                } else {
                    parametros.put("unidade", null);
                }
    
                parametros.put("nomeTarefaString", UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.nomeTarefa"));
                parametros.put(JRParameter.REPORT_VIRTUALIZER, new JRGzipVirtualizer(50));// Seta o parametro REPORT_VIRTUALIZER com a inst�ncia da virtualiza��o
            }
        }
        
    final private void trataCamposDasSolicitacoesParaImprimirRelatorio(final PesquisaSolicitacaoServicoDTO pesquisaSolicitacaoServicoDto,
			final ArrayList<SolicitacaoServicoDTO> listaSolicitacaoServicoPorCriterios, final HttpServletRequest request) {
		if (!Objects.equals(pesquisaSolicitacaoServicoDto, null) && CollectionUtils.isNotEmpty(listaSolicitacaoServicoPorCriterios)) {
			boolean nomeUsuarioRespAtualNotBlank = StringUtils.isNotBlank(pesquisaSolicitacaoServicoDto.getNomeUsuarioResponsavelAtual());

			for (final SolicitacaoServicoDTO solicitacaoServico : listaSolicitacaoServicoPorCriterios) {

				String resposta = tratarRespostaSolicitacao(solicitacaoServico);
				solicitacaoServico.setResposta(UtilStrings.unescapeJavaString(resposta));

				if ((solicitacaoServico.getPrazoHH() == null || solicitacaoServico.getPrazoHH() == 0)
						&& (solicitacaoServico.getPrazoMM() == null || solicitacaoServico.getPrazoMM() == 0)) {
					solicitacaoServico.setSlaACombinar("S");
				} else {
					String mm = solicitacaoServico.getPrazoMM().toString();
					String hh = solicitacaoServico.getPrazoHH().toString();
					if(Integer.parseInt(mm) > 59){
						hh = String.valueOf(Integer.parseInt(hh) + (Integer.parseInt(mm) / 60)); 
						mm = String.valueOf((Integer.parseInt(mm) % 60));
					}
					if (mm.length() == 1) {
						mm = "0" + mm;
					}
					solicitacaoServico.setPrazoMM(Integer.parseInt(mm));
					solicitacaoServico.setPrazoHH(Integer.parseInt(hh));
					solicitacaoServico.setSlaACombinar("N");
				}
				String descricaoSemFormatacao = tratarDescricaoSemFormatacao(solicitacaoServico);
				solicitacaoServico.setDescricaoSemFormatacao(UtilStrings.unescapeJavaString(descricaoSemFormatacao));
				solicitacaoServico.setDescricao(UtilStrings.unescapeJavaString(solicitacaoServico.getDescricao()));
				solicitacaoServico.setSituacao(solicitacaoServico.obterSituacaoInternacionalizada(request));

				if (nomeUsuarioRespAtualNotBlank) {
					solicitacaoServico.setNomeUsuarioResponsavelAtual(pesquisaSolicitacaoServicoDto.getNomeUsuarioResponsavelAtual());
				}

				if (StringUtils.isBlank(solicitacaoServico.getResponsavelAtual())) {
					solicitacaoServico.setResponsavelAtual(UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.semResponsavelAtual"));
				}

				if (StringUtils.isBlank(solicitacaoServico.getNomeTarefa())) {
					solicitacaoServico.setNomeTarefa(UtilI18N.internacionaliza(request, "citcorpore.comum.naoInformado"));
				}
			}
		}
    }

	public void imprimirRelatorioXlsDetalhado(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final HttpSession session = request.getSession();
		final PesquisaSolicitacaoServicoDTO pesquisaSolicitacaoServicoDto = (PesquisaSolicitacaoServicoDTO) document.getBean();
		final SolicitacaoServicoService solicitacaoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);
		TipoDemandaServicoDTO tipoDemandaServicoDto = new TipoDemandaServicoDTO();
		final TipoDemandaServicoService tipoDemandaServicoService = (TipoDemandaServicoService) ServiceLocator.getInstance().getService(TipoDemandaServicoService.class, null);
		GrupoDTO grupoDto = new GrupoDTO();
		final GrupoService grupoSegurancaService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);
		OrigemAtendimentoDTO origemDto = new OrigemAtendimentoDTO();
		final OrigemAtendimentoService origemAtendimentoService = (OrigemAtendimentoService) ServiceLocator.getInstance().getService(OrigemAtendimentoService.class, null);
		FaseServicoDTO faseDto = new FaseServicoDTO();
		final FaseServicoService faseServicoService = (FaseServicoService) ServiceLocator.getInstance().getService(FaseServicoService.class, null);
		PrioridadeDTO prioridadeDto = new PrioridadeDTO();
		final PrioridadeService prioridadeService = (PrioridadeService) ServiceLocator.getInstance().getService(PrioridadeService.class, null);
		ContratoDTO contratoDto = new ContratoDTO();
		final ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
		UsuarioDTO usuarioDto = new UsuarioDTO();
		final UsuarioService usuarioService = (UsuarioService) ServiceLocator.getInstance().getService(UsuarioService.class, null);
		UnidadeDTO unidadeDto = new UnidadeDTO();
		final UnidadeService unidadeService = (UnidadeService) ServiceLocator.getInstance().getService(UnidadeService.class, null);

		usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		// Passamos o usu�rio para que o sistema possa obter os IDs das unidades que ele pode acessar!
		pesquisaSolicitacaoServicoDto.setUsuarioLogado(usuario);

		final ArrayList<SolicitacaoServicoDTO> listaSolicitacaoServicoPorCriterios = (ArrayList<SolicitacaoServicoDTO>) solicitacaoService
				.listPesquisaAvancadaRelatorioDetalhado(pesquisaSolicitacaoServicoDto);

		if (listaSolicitacaoServicoPorCriterios == null || listaSolicitacaoServicoPorCriterios.size() == 0) {
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
			return;
		}
		
		if (!ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.QUANTIDADE_REGISTROS_PESQUISA_AVANCADA, "-1").equals("-1")
				&& listaSolicitacaoServicoPorCriterios.size() > Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.QUANTIDADE_REGISTROS_PESQUISA_AVANCADA, "0"))) {
			document.alert(UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.registroMaiorPermitido"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		for (final SolicitacaoServicoDTO solicitacaoServico : listaSolicitacaoServicoPorCriterios) {
			String resposta = UtilStrings.unescapeJavaString(solicitacaoServico.getResposta());
			if (resposta != null && resposta.length() > 1500) {
				resposta = resposta.substring(0, 1500);
			}
			solicitacaoServico.setResposta(UtilStrings.unescapeJavaString(resposta));
			if ((solicitacaoServico.getPrazoHH() == null || solicitacaoServico.getPrazoHH() == 0) && (solicitacaoServico.getPrazoMM() == null || solicitacaoServico.getPrazoMM() == 0)) {
				solicitacaoServico.setSlaACombinar("S");
			} else {
				solicitacaoServico.setSlaACombinar("N");
			}
			
			String descricaoSemFormatacao = tratarDescricaoSemFormatacao(solicitacaoServico);
			solicitacaoServico.setDescricaoSemFormatacao(UtilStrings.unescapeJavaString(descricaoSemFormatacao));
			solicitacaoServico.setDescricao(UtilStrings.unescapeJavaString(solicitacaoServico.getDescricao()));

			/*
			 * Internacionaliza a situa��o
			 */
			solicitacaoServico.setSituacao(solicitacaoServico.obterSituacaoInternacionalizada(request));

			if (pesquisaSolicitacaoServicoDto.getNomeUsuarioResponsavelAtual() != null) {
				solicitacaoServico.setNomeUsuarioResponsavelAtual(pesquisaSolicitacaoServicoDto.getNomeUsuarioResponsavelAtual());
			}
			/**
			 * Cria SLA para solicitacao
			 *
			 * @author thyen.chang
			 */
			if (solicitacaoServico != null && solicitacaoServico.getPrazoHH() != null && solicitacaoServico.getPrazoMM() != null) {
				solicitacaoServico.setSla((solicitacaoServico.getPrazoHH() < 10 ? "0" + solicitacaoServico.getPrazoHH().toString() : solicitacaoServico.getPrazoHH().toString()) + ":"
						+ (solicitacaoServico.getPrazoMM() < 10 ? "0" + solicitacaoServico.getPrazoMM().toString() : solicitacaoServico.getPrazoMM().toString()));
			} else {
				solicitacaoServico.setSla("");
			}
			/**
			 * Determina localidade caso n�o exista
			 *
			 * @author thyen.chang
			 */
			if (solicitacaoServico != null && (solicitacaoServico.getLocalidade() == null || solicitacaoServico.getLocalidade().equals(""))) {
				solicitacaoServico.setLocalidade(UtilI18N.internacionaliza(request, "citcorpore.comum.naoInformado"));
			}
			/**
			 * Determina respons�vel caso n�o exista
			 *
			 * @author thyen.chang
			 */
			if (solicitacaoServico != null && solicitacaoServico.getResponsavelAtual() == null) {
				solicitacaoServico.setResponsavelAtual(UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.semResponsavelAtual"));
			}

			if (solicitacaoServico != null && solicitacaoServico.getNomeTarefa() == null) {
				solicitacaoServico.setNomeTarefa(UtilI18N.internacionaliza(request, "citcorpore.comum.naoInformado"));
			}

			if (solicitacaoServico != null && solicitacaoServico.getResposta() != null) {
				solicitacaoServico.setResposta(formataCaracteresInv�lidos(solicitacaoServico.getResposta()));
			}
		}

		final Date dt = new Date();
		final String strCompl = "" + dt.getTime();
		final String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
		final String diretorioRelativoOS = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";

		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros = UtilRelatorio.trataInternacionalizacaoLocale(session, parametros);

		parametros.put("TITULO_RELATORIO", UtilI18N.internacionaliza(request, "citcorporeRelatorio.pesquisaSolicitacoesServicosDetalhado"));
		parametros.put("CIDADE", getCidadeParametrizada(request));
		parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
		parametros.put("NOME_USUARIO", usuario.getNomeUsuario());
		parametros.put("dataInicio", pesquisaSolicitacaoServicoDto.getDataInicio() == null ? pesquisaSolicitacaoServicoDto.getDataInicioFechamento() : pesquisaSolicitacaoServicoDto.getDataInicio());
		parametros.put("dataFim", pesquisaSolicitacaoServicoDto.getDataFim() == null ? pesquisaSolicitacaoServicoDto.getDataFimFechamento() : pesquisaSolicitacaoServicoDto.getDataFim());
		parametros.put("Logo", LogoRel.getFile());
		parametros.put("exibirCampoDescricao", pesquisaSolicitacaoServicoDto.getExibirCampoDescricao());
		parametros.put("quantidade", listaSolicitacaoServicoPorCriterios.size());
		parametros.put("criado_por", UtilI18N.internacionaliza(request, "citcorpore.comum.criadopor") + ":");

		if (pesquisaSolicitacaoServicoDto.getNomeItemConfiguracao() != null && !pesquisaSolicitacaoServicoDto.getNomeItemConfiguracao().equalsIgnoreCase("")) {
			parametros.put("nomeItemConfiguracao", pesquisaSolicitacaoServicoDto.getNomeItemConfiguracao());
		} else {
			parametros.put("nomeItemConfiguracao", null);
		}
		if (pesquisaSolicitacaoServicoDto.getNomeSolicitante() != null && !pesquisaSolicitacaoServicoDto.getNomeSolicitante().equalsIgnoreCase("")) {
			parametros.put("nomeSolicitante", pesquisaSolicitacaoServicoDto.getNomeSolicitante());
		} else {
			parametros.put("nomeSolicitante", null);
		}

		if (pesquisaSolicitacaoServicoDto.getIdTipoDemandaServico() != null) {
			tipoDemandaServicoDto.setIdTipoDemandaServico(pesquisaSolicitacaoServicoDto.getIdTipoDemandaServico());
			tipoDemandaServicoDto = (TipoDemandaServicoDTO) tipoDemandaServicoService.restore(tipoDemandaServicoDto);
			pesquisaSolicitacaoServicoDto.setNomeTipoDemandaServico(tipoDemandaServicoDto.getNomeTipoDemandaServico());
			parametros.put("tipo", pesquisaSolicitacaoServicoDto.getNomeTipoDemandaServico());
		} else {
			parametros.put("tipo", pesquisaSolicitacaoServicoDto.getNomeTipoDemandaServico());
		}
		if (pesquisaSolicitacaoServicoDto.getIdSolicitacaoServicoPesquisa() != null) {
			parametros.put("numero", pesquisaSolicitacaoServicoDto.getIdSolicitacaoServicoPesquisa());
		} else {
			parametros.put("numero", pesquisaSolicitacaoServicoDto.getIdSolicitacaoServicoPesquisa());
		}
		if (pesquisaSolicitacaoServicoDto.getSituacao() != null && !pesquisaSolicitacaoServicoDto.getSituacao().equals("")) {
			parametros.put("situacao", pesquisaSolicitacaoServicoDto.getSituacaoInternacionalizada(request));
		} else {
			parametros.put("situacao", null);
		}

		if (pesquisaSolicitacaoServicoDto.getIdGrupoAtual() != null) {
			grupoDto.setIdGrupo(pesquisaSolicitacaoServicoDto.getIdGrupoAtual());
			grupoDto = (GrupoDTO) grupoSegurancaService.restore(grupoDto);
			pesquisaSolicitacaoServicoDto.setGrupoAtual(grupoDto.getSigla());
			parametros.put("grupoSolucionador", pesquisaSolicitacaoServicoDto.getGrupoAtual());
		} else {
			parametros.put("grupoSolucionador", pesquisaSolicitacaoServicoDto.getGrupoAtual());
		}
		if (pesquisaSolicitacaoServicoDto.getIdOrigem() != null) {
			origemDto.setIdOrigem(pesquisaSolicitacaoServicoDto.getIdOrigem());
			origemDto = (OrigemAtendimentoDTO) origemAtendimentoService.restore(origemDto);
			pesquisaSolicitacaoServicoDto.setOrigem(UtilStrings.unescapeJavaString(origemDto.getDescricao()));
			parametros.put("origem", pesquisaSolicitacaoServicoDto.getOrigem());
		} else {
			parametros.put("origem", pesquisaSolicitacaoServicoDto.getOrigem());
		}
		if (pesquisaSolicitacaoServicoDto.getIdFaseAtual() != null) {
			faseDto.setIdFase(pesquisaSolicitacaoServicoDto.getIdFaseAtual());
			faseDto = (FaseServicoDTO) faseServicoService.restore(faseDto);
			pesquisaSolicitacaoServicoDto.setFaseAtual(faseDto.getNomeFase());
			parametros.put("fase", pesquisaSolicitacaoServicoDto.getFaseAtual());
		} else {
			parametros.put("fase", pesquisaSolicitacaoServicoDto.getFaseAtual());
		}
		if (pesquisaSolicitacaoServicoDto.getIdPrioridade() != null) {
			prioridadeDto.setIdPrioridade(pesquisaSolicitacaoServicoDto.getIdPrioridade());
			prioridadeDto = (PrioridadeDTO) prioridadeService.restore(prioridadeDto);
			pesquisaSolicitacaoServicoDto.setPrioridade(prioridadeDto.getNomePrioridade());
			parametros.put("prioridade", pesquisaSolicitacaoServicoDto.getPrioridade());
		} else {
			parametros.put("prioridade", pesquisaSolicitacaoServicoDto.getPrioridade());
		}

		if (pesquisaSolicitacaoServicoDto.getIdContrato() != null) {
			contratoDto.setIdContrato(pesquisaSolicitacaoServicoDto.getIdContrato());
			contratoDto = (ContratoDTO) contratoService.restore(contratoDto);
			parametros.put("contrato", contratoDto.getNumero());
		} else {
			parametros.put("contrato", contratoDto.getNumero());
		}

		if (pesquisaSolicitacaoServicoDto.getIdResponsavel() != null) {
			usuarioDto.setIdUsuario(pesquisaSolicitacaoServicoDto.getIdResponsavel());
			usuarioDto = (UsuarioDTO) usuarioService.restore(usuarioDto);
			parametros.put("responsavel", usuarioDto.getNomeUsuario());
		} else {
			parametros.put("responsavel", null);
		}

		if (pesquisaSolicitacaoServicoDto.getIdUsuarioResponsavelAtual() != null) {
			usuarioDto.setIdUsuario(pesquisaSolicitacaoServicoDto.getIdUsuarioResponsavelAtual());
			usuarioDto = (UsuarioDTO) usuarioService.restore(usuarioDto);
			parametros.put("nomeUsuarioResponsavelAtual", usuarioDto.getNomeUsuario());
		} else {
			parametros.put("nomeUsuarioResponsavelAtual", null);
		}

		if (pesquisaSolicitacaoServicoDto.getIdUnidade() != null) {
			unidadeDto.setIdUnidade(pesquisaSolicitacaoServicoDto.getIdUnidade());
			unidadeDto = (UnidadeDTO) unidadeService.restore(unidadeDto);
			if (unidadeDto != null && unidadeDto.getNome() != null) {
				parametros.put("unidade", unidadeDto.getNome());
			} else {
				parametros.put("unidade", null);
			}
		} else {
			parametros.put("unidade", null);
		}

		parametros.put("nomeTarefaString", UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.nomeTarefa"));

		try {
			final JRDataSource dataSource = new JRBeanCollectionDataSource(listaSolicitacaoServicoPorCriterios);
			final JasperDesign desenho = JRXmlLoader.load(CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioPesquisaSolicitacaoServicoXlsDetalhado.jrxml");
			final JasperReport relatorio = JasperCompileManager.compileReport(desenho);
			final JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, dataSource);
			final JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(impressao));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(diretorioReceita + "/RelatorioPesquisaSolicitacaoServicoXlsDetalhado" + strCompl + "_" + usuario.getIdUsuario() + ".xlsx"));
            XlsxReportConfiguration xlsxReportConfiguration = new SimpleXlsxReportConfiguration();
            exporter.setConfiguration(xlsxReportConfiguration);
			exporter.exportReport();
			document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + diretorioRelativoOS
					+ "/RelatorioPesquisaSolicitacaoServicoXlsDetalhado" + strCompl + "_" + usuario.getIdUsuario() + ".xlsx')");
		} catch (final OutOfMemoryError e) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.erro.erroServidor"));
		}
		document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
		this.limpaRegistrosDaPesquisa(document);
	}

	public void restoreUpload(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws ServiceException, Exception {
		request.getSession(true).setAttribute("colUploadsGED", null);
		/* Realida o refresh do iframe */
		document.executeScript("document.getElementById('fraUpload_uploadAnexos').contentWindow.location.reload(true)");

		usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}
		final PesquisaSolicitacaoServicoDTO pesquisaSolicitacaoServicoDto = (PesquisaSolicitacaoServicoDTO) document.getBean();

		if (pesquisaSolicitacaoServicoDto.getIdSolicitacaoServico() == null) {
			return;
		}
		final ControleGEDService controleGedService = (ControleGEDService) ServiceLocator.getInstance().getService(ControleGEDService.class, null);
		final Collection colAnexos = controleGedService.listByIdTabelaAndID(ControleGEDDTO.TABELA_SOLICITACAOSERVICO, pesquisaSolicitacaoServicoDto.getIdSolicitacaoServico());
		final Collection colAnexosUploadDTO = controleGedService.convertListControleGEDToUploadDTO(colAnexos);

		request.getSession(true).setAttribute("colUploadsGED", colAnexosUploadDTO);
		document.executeScript("$('#POPUP_menuAnexos').dialog('open');");
	}

	public String mostrarOcorrencia() throws Exception {
		final OcorrenciaSolicitacaoService ocorrenciaSolicitacaoService = (OcorrenciaSolicitacaoService) ServiceLocator.getInstance().getService(OcorrenciaSolicitacaoService.class, null);
		@SuppressWarnings("unchecked")
		final Collection<OcorrenciaSolicitacaoDTO> col = ocorrenciaSolicitacaoService.findByIdSolicitacaoServico(13458);
		for (final OcorrenciaSolicitacaoDTO ocorrenciaSolicitacaoDTO : col) {
			String dadosSolicitacao = UtilStrings.nullToVazio(ocorrenciaSolicitacaoDTO.getDadosSolicitacao());
			SolicitacaoServicoDTO solicitacaoDto = null;
			if (dadosSolicitacao.length() > 0) {
				try {
					solicitacaoDto = new Gson().fromJson(dadosSolicitacao, SolicitacaoServicoDTO.class);
					if (solicitacaoDto != null) {
						dadosSolicitacao = solicitacaoDto.getDadosStr();

						if (solicitacaoDto.getSituacao().equals("Resolvido") && !solicitacaoDto.getRegistradoPor().equals("Autom�tico")) {
							return solicitacaoDto.getRegistradoPor();
						}
					}
				} catch (final Exception e) {
					dadosSolicitacao = "";
				}
			}
			System.out.println(dadosSolicitacao);
		}
		return "";
	}

	public void imprimirRelatorioDetalhado(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final HttpSession session = request.getSession();
		final PesquisaSolicitacaoServicoDTO pesquisaSolicitacaoServicoDto = (PesquisaSolicitacaoServicoDTO) document.getBean();
		final SolicitacaoServicoService solicitacaoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);
		TipoDemandaServicoDTO tipoDemandaServicoDto = new TipoDemandaServicoDTO();
		final TipoDemandaServicoService tipoDemandaServicoService = (TipoDemandaServicoService) ServiceLocator.getInstance().getService(TipoDemandaServicoService.class, null);
		GrupoDTO grupoDto = new GrupoDTO();
		final GrupoService grupoSegurancaService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);
		OrigemAtendimentoDTO origemDto = new OrigemAtendimentoDTO();
		final OrigemAtendimentoService origemAtendimentoService = (OrigemAtendimentoService) ServiceLocator.getInstance().getService(OrigemAtendimentoService.class, null);
		FaseServicoDTO faseDto = new FaseServicoDTO();
		final FaseServicoService faseServicoService = (FaseServicoService) ServiceLocator.getInstance().getService(FaseServicoService.class, null);
		PrioridadeDTO prioridadeDto = new PrioridadeDTO();
		final PrioridadeService prioridadeService = (PrioridadeService) ServiceLocator.getInstance().getService(PrioridadeService.class, null);
		ContratoDTO contratoDto = new ContratoDTO();
		final ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
		UsuarioDTO usuarioDto = new UsuarioDTO();
		final UsuarioService usuarioService = (UsuarioService) ServiceLocator.getInstance().getService(UsuarioService.class, null);
		UnidadeDTO unidadeDto = new UnidadeDTO();
		final UnidadeService unidadeService = (UnidadeService) ServiceLocator.getInstance().getService(UnidadeService.class, null);

		usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		// Passamos o usu�rio para que o sistema possa obter os IDs das unidades que ele pode acessar!
		pesquisaSolicitacaoServicoDto.setUsuarioLogado(usuario);

		ArrayList<SolicitacaoServicoDTO> listaSolicitacaoServicoPorCriterios = (ArrayList<SolicitacaoServicoDTO>) solicitacaoService.listPesquisaAvancadaRelatorioDetalhado(pesquisaSolicitacaoServicoDto);

		if (listaSolicitacaoServicoPorCriterios == null || listaSolicitacaoServicoPorCriterios.size() == 0) {
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
			return;
		}
		
		if (!ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.QUANTIDADE_REGISTROS_PESQUISA_AVANCADA, "-1").equals("-1")
				&& listaSolicitacaoServicoPorCriterios.size() > Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.QUANTIDADE_REGISTROS_PESQUISA_AVANCADA, "0"))) {
			document.alert(UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.registroMaiorPermitido"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		for (final SolicitacaoServicoDTO solicitacaoServico : listaSolicitacaoServicoPorCriterios) {
			String resposta = UtilStrings.unescapeJavaString(solicitacaoServico.getResposta());
			if (resposta != null && resposta.length() > 1500) {
				resposta = resposta.substring(0, 1500);
			}
			
			if ((solicitacaoServico.getPrazoHH() == null || solicitacaoServico.getPrazoHH() == 0) && (solicitacaoServico.getPrazoMM() == null || solicitacaoServico.getPrazoMM() == 0)) {
				solicitacaoServico.setSlaACombinar("S");
			} else {
				solicitacaoServico.setSlaACombinar("N");
			}
			
			String descricaoSemFormatacao = tratarDescricaoSemFormatacao(solicitacaoServico);
			solicitacaoServico.setDescricaoSemFormatacao(UtilStrings.unescapeJavaString(descricaoSemFormatacao));
			solicitacaoServico.setDescricao(UtilStrings.unescapeJavaString(solicitacaoServico.getDescricao()));

			/*
			 * Internacionaliza a situa��o
			 */
			solicitacaoServico.setSituacao(solicitacaoServico.obterSituacaoInternacionalizada(request));

			if (pesquisaSolicitacaoServicoDto.getNomeUsuarioResponsavelAtual() != null) {
				solicitacaoServico.setNomeUsuarioResponsavelAtual(pesquisaSolicitacaoServicoDto.getNomeUsuarioResponsavelAtual());
			}
			/**
			 * Cria SLA para solicitacao
			 *
			 * @author thyen.chang
			 */
			if (solicitacaoServico != null && solicitacaoServico.getPrazoHH() != null && solicitacaoServico.getPrazoMM() != null) {
				solicitacaoServico.setSla((solicitacaoServico.getPrazoHH() < 10 ? "0" + solicitacaoServico.getPrazoHH().toString() : solicitacaoServico.getPrazoHH().toString()) + ":"
						+ (solicitacaoServico.getPrazoMM() < 10 ? "0" + solicitacaoServico.getPrazoMM().toString() : solicitacaoServico.getPrazoMM().toString()));
			} else {
				solicitacaoServico.setSla("");
			}
			/**
			 * Determina localidade caso n�o exista
			 *
			 * @author thyen.chang
			 */
			if (solicitacaoServico != null && (solicitacaoServico.getLocalidade() == null || solicitacaoServico.getLocalidade().equals(""))) {
				solicitacaoServico.setLocalidade(UtilI18N.internacionaliza(request, "citcorpore.comum.naoInformado"));
			}
			/**
			 * Determina respons�vel caso n�o exista
			 *
			 * @author thyen.chang
			 */
			if (solicitacaoServico != null && solicitacaoServico.getResponsavelAtual() == null) {
				solicitacaoServico.setResponsavelAtual(UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.semResponsavelAtual"));
			}

			if (solicitacaoServico != null && solicitacaoServico.getNomeTarefa() == null) {
				solicitacaoServico.setNomeTarefa(UtilI18N.internacionaliza(request, "citcorpore.comum.naoInformado"));
			}

		}

		final Date dt = new Date();
		final String strCompl = "" + dt.getTime();
		final String caminhoJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioPesquisaSolicitacaoServicoDetalhado.jasper";
		final String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
		final String diretorioRelativoOS = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";

		Map<String, Object> parametros = new HashMap<String, Object>();

		parametros = UtilRelatorio.trataInternacionalizacaoLocale(session, parametros);

		parametros.put("TITULO_RELATORIO", UtilI18N.internacionaliza(request, "citcorporeRelatorio.pesquisaSolicitacoesServicosDetalhado"));
		parametros.put("CIDADE", getCidadeParametrizada(request));
		parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
		parametros.put("NOME_USUARIO", usuario.getNomeUsuario());
		parametros.put("dataInicio", pesquisaSolicitacaoServicoDto.getDataInicio() == null ? pesquisaSolicitacaoServicoDto.getDataInicioFechamento() : pesquisaSolicitacaoServicoDto.getDataInicio());
		parametros.put("dataFim", pesquisaSolicitacaoServicoDto.getDataFim() == null ? pesquisaSolicitacaoServicoDto.getDataFimFechamento() : pesquisaSolicitacaoServicoDto.getDataFim());
		parametros.put("Logo", LogoRel.getFile());
		parametros.put("exibirCampoDescricao", UtilStrings.unescapeJavaString(pesquisaSolicitacaoServicoDto.getExibirCampoDescricao()));
		parametros.put("quantidade", listaSolicitacaoServicoPorCriterios.size());
		parametros.put("criado_por", UtilI18N.internacionaliza(request, "citcorpore.comum.criadopor") + ":");

		if (pesquisaSolicitacaoServicoDto.getNomeItemConfiguracao() != null && !pesquisaSolicitacaoServicoDto.getNomeItemConfiguracao().equalsIgnoreCase("")) {
			parametros.put("nomeItemConfiguracao", pesquisaSolicitacaoServicoDto.getNomeItemConfiguracao());
		} else {
			parametros.put("nomeItemConfiguracao", null);
		}
		if (pesquisaSolicitacaoServicoDto.getNomeSolicitante() != null && !pesquisaSolicitacaoServicoDto.getNomeSolicitante().equalsIgnoreCase("")) {
			parametros.put("nomeSolicitante", pesquisaSolicitacaoServicoDto.getNomeSolicitante());
		} else {
			parametros.put("nomeSolicitante", null);
		}

		if (pesquisaSolicitacaoServicoDto.getIdTipoDemandaServico() != null) {
			tipoDemandaServicoDto.setIdTipoDemandaServico(pesquisaSolicitacaoServicoDto.getIdTipoDemandaServico());
			tipoDemandaServicoDto = (TipoDemandaServicoDTO) tipoDemandaServicoService.restore(tipoDemandaServicoDto);
			pesquisaSolicitacaoServicoDto.setNomeTipoDemandaServico(tipoDemandaServicoDto.getNomeTipoDemandaServico());
			parametros.put("tipo", pesquisaSolicitacaoServicoDto.getNomeTipoDemandaServico());
		} else {
			parametros.put("tipo", pesquisaSolicitacaoServicoDto.getNomeTipoDemandaServico());
		}
		if (pesquisaSolicitacaoServicoDto.getIdSolicitacaoServicoPesquisa() != null) {
			parametros.put("numero", pesquisaSolicitacaoServicoDto.getIdSolicitacaoServicoPesquisa());
		} else {
			parametros.put("numero", pesquisaSolicitacaoServicoDto.getIdSolicitacaoServicoPesquisa());
		}
		if (pesquisaSolicitacaoServicoDto.getSituacao() != null && !pesquisaSolicitacaoServicoDto.getSituacao().equals("")) {
			parametros.put("situacao", pesquisaSolicitacaoServicoDto.getSituacaoInternacionalizada(request));
		} else {
			parametros.put("situacao", null);
		}

		if (pesquisaSolicitacaoServicoDto.getIdGrupoAtual() != null) {
			grupoDto.setIdGrupo(pesquisaSolicitacaoServicoDto.getIdGrupoAtual());
			grupoDto = (GrupoDTO) grupoSegurancaService.restore(grupoDto);
			pesquisaSolicitacaoServicoDto.setGrupoAtual(grupoDto.getSigla());
			parametros.put("grupoSolucionador", pesquisaSolicitacaoServicoDto.getGrupoAtual());
		} else {
			parametros.put("grupoSolucionador", pesquisaSolicitacaoServicoDto.getGrupoAtual());
		}
		if (pesquisaSolicitacaoServicoDto.getIdOrigem() != null) {
			origemDto.setIdOrigem(pesquisaSolicitacaoServicoDto.getIdOrigem());
			origemDto = (OrigemAtendimentoDTO) origemAtendimentoService.restore(origemDto);
			pesquisaSolicitacaoServicoDto.setOrigem(UtilStrings.unescapeJavaString(origemDto.getDescricao()));
			parametros.put("origem", pesquisaSolicitacaoServicoDto.getOrigem());
		} else {
			parametros.put("origem", pesquisaSolicitacaoServicoDto.getOrigem());
		}
		if (pesquisaSolicitacaoServicoDto.getIdFaseAtual() != null) {
			faseDto.setIdFase(pesquisaSolicitacaoServicoDto.getIdFaseAtual());
			faseDto = (FaseServicoDTO) faseServicoService.restore(faseDto);
			pesquisaSolicitacaoServicoDto.setFaseAtual(faseDto.getNomeFase());
			parametros.put("fase", pesquisaSolicitacaoServicoDto.getFaseAtual());
		} else {
			parametros.put("fase", pesquisaSolicitacaoServicoDto.getFaseAtual());
		}
		if (pesquisaSolicitacaoServicoDto.getIdPrioridade() != null) {
			prioridadeDto.setIdPrioridade(pesquisaSolicitacaoServicoDto.getIdPrioridade());
			prioridadeDto = (PrioridadeDTO) prioridadeService.restore(prioridadeDto);
			pesquisaSolicitacaoServicoDto.setPrioridade(prioridadeDto.getNomePrioridade());
			parametros.put("prioridade", pesquisaSolicitacaoServicoDto.getPrioridade());
		} else {
			parametros.put("prioridade", pesquisaSolicitacaoServicoDto.getPrioridade());
		}

		if (pesquisaSolicitacaoServicoDto.getIdContrato() != null) {
			contratoDto.setIdContrato(pesquisaSolicitacaoServicoDto.getIdContrato());
			contratoDto = (ContratoDTO) contratoService.restore(contratoDto);
			parametros.put("contrato", contratoDto.getNumero());
		} else {
			parametros.put("contrato", contratoDto.getNumero());
		}

		if (pesquisaSolicitacaoServicoDto.getIdResponsavel() != null) {
			usuarioDto.setIdUsuario(pesquisaSolicitacaoServicoDto.getIdResponsavel());
			usuarioDto = (UsuarioDTO) usuarioService.restore(usuarioDto);
			parametros.put("responsavel", usuarioDto.getNomeUsuario());
		} else {
			parametros.put("responsavel", null);
		}

		if (pesquisaSolicitacaoServicoDto.getIdUsuarioResponsavelAtual() != null) {
			usuarioDto.setIdUsuario(pesquisaSolicitacaoServicoDto.getIdUsuarioResponsavelAtual());
			usuarioDto = (UsuarioDTO) usuarioService.restore(usuarioDto);
			parametros.put("nomeUsuarioResponsavelAtual", usuarioDto.getNomeUsuario());
		} else {
			parametros.put("nomeUsuarioResponsavelAtual", null);
		}

		if (pesquisaSolicitacaoServicoDto.getIdUnidade() != null) {
			unidadeDto.setIdUnidade(pesquisaSolicitacaoServicoDto.getIdUnidade());
			unidadeDto = (UnidadeDTO) unidadeService.restore(unidadeDto);
			if (unidadeDto != null && unidadeDto.getNome() != null) {
				parametros.put("unidade", unidadeDto.getNome());
			} else {
				parametros.put("unidade", null);
			}
		} else {
			parametros.put("unidade", null);
		}

		parametros.put("nomeTarefaString", UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.nomeTarefa"));

		try {
			JRDataSource dataSource = new JRBeanCollectionDataSource(listaSolicitacaoServicoPorCriterios);

			JRAbstractLRUVirtualizer virtualizer = new JRGzipVirtualizer(50);

			// Seta o parametro REPORT_VIRTUALIZER com a inst�ncia da virtualiza��o
			parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);

			// Preenche o relat�rio e exibe numa GUI
			final Timestamp ts1 = UtilDatas.getDataHoraAtual();
			final JasperPrint jp = JasperFillManager.fillReport(caminhoJasper, parametros, dataSource);
			final Timestamp ts2 = UtilDatas.getDataHoraAtual();
			final double tempo = UtilDatas.calculaDiferencaTempoEmMilisegundos(ts2, ts1);
			System.out.println("########## Tempo fillReport: " + tempo);

			JasperExportManager.exportReportToPdfFile(jp, diretorioReceita + "/RelatorioSolicitacaoServicoDetalhado" + strCompl + "_" + usuario.getIdUsuario() + ".pdf");
			document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + diretorioRelativoOS
					+ "/RelatorioSolicitacaoServicoDetalhado" + strCompl + "_" + usuario.getIdUsuario() + ".pdf')");
			virtualizer = null;
			dataSource = null;
			listaSolicitacaoServicoPorCriterios = null;
		} catch (final OutOfMemoryError e) { // TODO capturar error n�o n� amig�o??
			/*
			 * Desenvolvedor: Thiago Matias - Data: 30/10/2013 - Hor�rio: 15h35min - ID Citsmart: 122665 - Motivo/Coment�rio: alterando o a chave de citcorpore.erro.memoria para citsmart.erro.memoria
			 */
			document.alert(UtilI18N.internacionaliza(request, "citsmart.erro.memoria"));
		}
		document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
		this.limpaRegistrosDaPesquisa(document);
	}

	public void imprimirRelatorioXls(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final HttpSession session = request.getSession();
		final PesquisaSolicitacaoServicoDTO pesquisaSolicitacaoServicoDto = (PesquisaSolicitacaoServicoDTO) document.getBean();
		final SolicitacaoServicoService solicitacaoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);
		TipoDemandaServicoDTO tipoDemandaServicoDto = new TipoDemandaServicoDTO();
		final TipoDemandaServicoService tipoDemandaServicoService = (TipoDemandaServicoService) ServiceLocator.getInstance().getService(TipoDemandaServicoService.class, null);
		GrupoDTO grupoDto = new GrupoDTO();
		final GrupoService grupoSegurancaService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);
		OrigemAtendimentoDTO origemDto = new OrigemAtendimentoDTO();
		final OrigemAtendimentoService origemAtendimentoService = (OrigemAtendimentoService) ServiceLocator.getInstance().getService(OrigemAtendimentoService.class, null);
		FaseServicoDTO faseDto = new FaseServicoDTO();
		final FaseServicoService faseServicoService = (FaseServicoService) ServiceLocator.getInstance().getService(FaseServicoService.class, null);
		PrioridadeDTO prioridadeDto = new PrioridadeDTO();
		final PrioridadeService prioridadeService = (PrioridadeService) ServiceLocator.getInstance().getService(PrioridadeService.class, null);
		ContratoDTO contratoDto = new ContratoDTO();
		final ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
		UsuarioDTO usuarioDto = new UsuarioDTO();
		final UsuarioService usuarioService = (UsuarioService) ServiceLocator.getInstance().getService(UsuarioService.class, null);
		UnidadeDTO unidadeDto = new UnidadeDTO();
		final UnidadeService unidadeService = (UnidadeService) ServiceLocator.getInstance().getService(UnidadeService.class, null);

		usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		// Passamos o usu�rio para que o sistema possa obter os IDs das unidades que ele pode acessar!
		pesquisaSolicitacaoServicoDto.setUsuarioLogado(usuario);

		final List<SolicitacaoServicoDTO> listaSolicitacaoServicoPorCriterios = (ArrayList<SolicitacaoServicoDTO>) solicitacaoService.listPesquisaAvancadaRelatorio(pesquisaSolicitacaoServicoDto);

		if (listaSolicitacaoServicoPorCriterios == null || listaSolicitacaoServicoPorCriterios.size() == 0) {
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
			return;
		}
		
		if (!ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.QUANTIDADE_REGISTROS_PESQUISA_AVANCADA, "-1").equals("-1")
				&& listaSolicitacaoServicoPorCriterios.size() > Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.QUANTIDADE_REGISTROS_PESQUISA_AVANCADA, "0"))) {
			document.alert(UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.registroMaiorPermitido"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		for (final SolicitacaoServicoDTO solicitacaoServico : listaSolicitacaoServicoPorCriterios) {
			String resposta = tratarRespostaSolicitacao(solicitacaoServico);
			solicitacaoServico.setResposta(UtilStrings.unescapeJavaString(resposta));

			if ((solicitacaoServico.getPrazoHH() == null || solicitacaoServico.getPrazoHH() == 0) && (solicitacaoServico.getPrazoMM() == null || solicitacaoServico.getPrazoMM() == 0)) {
				solicitacaoServico.setSlaACombinar("S");
			} else {
				solicitacaoServico.setSlaACombinar("N");
			}
			
			String descricaoSemFormatacao = tratarDescricaoSemFormatacao(solicitacaoServico);
			solicitacaoServico.setDescricaoSemFormatacao(UtilStrings.unescapeJavaString(descricaoSemFormatacao));
			solicitacaoServico.setDescricao(UtilStrings.unescapeJavaString(solicitacaoServico.getDescricao()));

			/*
			 * Internacionaliza a situa��o
			 */
			solicitacaoServico.setSituacao(solicitacaoServico.obterSituacaoInternacionalizada(request));

			if (pesquisaSolicitacaoServicoDto.getNomeUsuarioResponsavelAtual() != null) {
				solicitacaoServico.setNomeUsuarioResponsavelAtual(pesquisaSolicitacaoServicoDto.getNomeUsuarioResponsavelAtual());
			}

			if (solicitacaoServico != null && solicitacaoServico.getResponsavelAtual() == null) {
				solicitacaoServico.setResponsavelAtual(UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.semResponsavelAtual"));
			}

			if (solicitacaoServico != null && solicitacaoServico.getNomeTarefa() == null) {
				solicitacaoServico.setNomeTarefa(UtilI18N.internacionaliza(request, "citcorpore.comum.naoInformado"));
			}

			if (solicitacaoServico != null && solicitacaoServico.getResposta() != null) {
				solicitacaoServico.setResposta(formataCaracteresInv�lidos(solicitacaoServico.getResposta()));
			}

		}

		final Date dt = new Date();
		final String strCompl = "" + dt.getTime();
		final String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
		final String diretorioRelativoOS = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";

		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros = UtilRelatorio.trataInternacionalizacaoLocale(session, parametros);

		parametros.put("TITULO_RELATORIO", UtilI18N.internacionaliza(request, "citcorporeRelatorio.pesquisaSolicitacoesServicos"));
		parametros.put("CIDADE", getCidadeParametrizada(request));
		parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
		parametros.put("NOME_USUARIO", usuario.getNomeUsuario());
		parametros.put("dataInicio", pesquisaSolicitacaoServicoDto.getDataInicio() == null ? pesquisaSolicitacaoServicoDto.getDataInicioFechamento() : pesquisaSolicitacaoServicoDto.getDataInicio());
		parametros.put("dataFim", pesquisaSolicitacaoServicoDto.getDataFim() == null ? pesquisaSolicitacaoServicoDto.getDataFimFechamento() : pesquisaSolicitacaoServicoDto.getDataFim());
		parametros.put("Logo", LogoRel.getFile());
		parametros.put("exibirCampoDescricao", pesquisaSolicitacaoServicoDto.getExibirCampoDescricao());
		parametros.put("quantidade", listaSolicitacaoServicoPorCriterios.size());
		parametros.put("criado_por", UtilI18N.internacionaliza(request, "citcorpore.comum.criadopor") + ":");

		if (pesquisaSolicitacaoServicoDto.getNomeItemConfiguracao() != null && !pesquisaSolicitacaoServicoDto.getNomeItemConfiguracao().equalsIgnoreCase("")) {
			parametros.put("nomeItemConfiguracao", pesquisaSolicitacaoServicoDto.getNomeItemConfiguracao());
		} else {
			parametros.put("nomeItemConfiguracao", null);
		}
		if (pesquisaSolicitacaoServicoDto.getNomeSolicitante() != null && !pesquisaSolicitacaoServicoDto.getNomeSolicitante().equalsIgnoreCase("")) {
			parametros.put("nomeSolicitante", pesquisaSolicitacaoServicoDto.getNomeSolicitante());
		} else {
			parametros.put("nomeSolicitante", null);
		}

		if (pesquisaSolicitacaoServicoDto.getIdTipoDemandaServico() != null) {
			tipoDemandaServicoDto.setIdTipoDemandaServico(pesquisaSolicitacaoServicoDto.getIdTipoDemandaServico());
			tipoDemandaServicoDto = (TipoDemandaServicoDTO) tipoDemandaServicoService.restore(tipoDemandaServicoDto);
			pesquisaSolicitacaoServicoDto.setNomeTipoDemandaServico(tipoDemandaServicoDto.getNomeTipoDemandaServico());
			parametros.put("tipo", pesquisaSolicitacaoServicoDto.getNomeTipoDemandaServico());
		} else {
			parametros.put("tipo", pesquisaSolicitacaoServicoDto.getNomeTipoDemandaServico());
		}
		if (pesquisaSolicitacaoServicoDto.getIdSolicitacaoServicoPesquisa() != null) {
			parametros.put("numero", pesquisaSolicitacaoServicoDto.getIdSolicitacaoServicoPesquisa());
		} else {
			parametros.put("numero", pesquisaSolicitacaoServicoDto.getIdSolicitacaoServicoPesquisa());
		}
		if (pesquisaSolicitacaoServicoDto.getSituacao() != null && !pesquisaSolicitacaoServicoDto.getSituacao().equals("")) {
			parametros.put("situacao", pesquisaSolicitacaoServicoDto.getSituacaoInternacionalizada(request));
		} else {
			parametros.put("situacao", null);
		}

		if (pesquisaSolicitacaoServicoDto.getIdGrupoAtual() != null) {
			grupoDto.setIdGrupo(pesquisaSolicitacaoServicoDto.getIdGrupoAtual());
			grupoDto = (GrupoDTO) grupoSegurancaService.restore(grupoDto);
			pesquisaSolicitacaoServicoDto.setGrupoAtual(grupoDto.getSigla());
			parametros.put("grupoSolucionador", pesquisaSolicitacaoServicoDto.getGrupoAtual());
		} else {
			parametros.put("grupoSolucionador", pesquisaSolicitacaoServicoDto.getGrupoAtual());
		}
		if (pesquisaSolicitacaoServicoDto.getIdOrigem() != null) {
			origemDto.setIdOrigem(pesquisaSolicitacaoServicoDto.getIdOrigem());
			origemDto = (OrigemAtendimentoDTO) origemAtendimentoService.restore(origemDto);
			pesquisaSolicitacaoServicoDto.setOrigem(UtilStrings.unescapeJavaString(origemDto.getDescricao()));
			parametros.put("origem", pesquisaSolicitacaoServicoDto.getOrigem());
		} else {
			parametros.put("origem", pesquisaSolicitacaoServicoDto.getOrigem());
		}
		if (pesquisaSolicitacaoServicoDto.getIdFaseAtual() != null) {
			faseDto.setIdFase(pesquisaSolicitacaoServicoDto.getIdFaseAtual());
			faseDto = (FaseServicoDTO) faseServicoService.restore(faseDto);
			pesquisaSolicitacaoServicoDto.setFaseAtual(faseDto.getNomeFase());
			parametros.put("fase", pesquisaSolicitacaoServicoDto.getFaseAtual());
		} else {
			parametros.put("fase", pesquisaSolicitacaoServicoDto.getFaseAtual());
		}
		if (pesquisaSolicitacaoServicoDto.getIdPrioridade() != null) {
			prioridadeDto.setIdPrioridade(pesquisaSolicitacaoServicoDto.getIdPrioridade());
			prioridadeDto = (PrioridadeDTO) prioridadeService.restore(prioridadeDto);
			pesquisaSolicitacaoServicoDto.setPrioridade(prioridadeDto.getNomePrioridade());
			parametros.put("prioridade", pesquisaSolicitacaoServicoDto.getPrioridade());
		} else {
			parametros.put("prioridade", pesquisaSolicitacaoServicoDto.getPrioridade());
		}

		if (pesquisaSolicitacaoServicoDto.getIdContrato() != null) {
			contratoDto.setIdContrato(pesquisaSolicitacaoServicoDto.getIdContrato());
			contratoDto = (ContratoDTO) contratoService.restore(contratoDto);
			parametros.put("contrato", contratoDto.getNumero());
		} else {
			parametros.put("contrato", contratoDto.getNumero());
		}

		if (pesquisaSolicitacaoServicoDto.getIdResponsavel() != null) {
			usuarioDto.setIdUsuario(pesquisaSolicitacaoServicoDto.getIdResponsavel());
			usuarioDto = (UsuarioDTO) usuarioService.restore(usuarioDto);
			parametros.put("responsavel", usuarioDto.getNomeUsuario());
		} else {
			parametros.put("responsavel", null);
		}

		if (pesquisaSolicitacaoServicoDto.getIdUsuarioResponsavelAtual() != null) {
			usuarioDto.setIdUsuario(pesquisaSolicitacaoServicoDto.getIdUsuarioResponsavelAtual());
			usuarioDto = (UsuarioDTO) usuarioService.restore(usuarioDto);
			parametros.put("nomeUsuarioResponsavelAtual", usuarioDto.getNomeUsuario());
		} else {
			parametros.put("nomeUsuarioResponsavelAtual", null);
		}

		if (pesquisaSolicitacaoServicoDto.getIdUnidade() != null) {
			unidadeDto.setIdUnidade(pesquisaSolicitacaoServicoDto.getIdUnidade());
			unidadeDto = (UnidadeDTO) unidadeService.restore(unidadeDto);
			if (unidadeDto != null && unidadeDto.getNome() != null) {
				parametros.put("unidade", unidadeDto.getNome());
			} else {
				parametros.put("unidade", null);
			}
		} else {
			parametros.put("unidade", null);
		}

		parametros.put("nomeTarefaString", UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.nomeTarefa"));

		try {
			final JRDataSource dataSource = new JRBeanCollectionDataSource(listaSolicitacaoServicoPorCriterios);
			final JasperDesign desenho = JRXmlLoader.load(CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioPesquisaSolicitacaoServicoXls.jrxml");
			final JasperReport relatorio = JasperCompileManager.compileReport(desenho);
			final JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, dataSource);
			final JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(impressao));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(diretorioReceita + "/RelatorioPesquisaSolicitacaoServicoXls" + strCompl + "_" + usuario.getIdUsuario() + ".xlsx"));
            XlsxReportConfiguration xlsxReportConfiguration = new SimpleXlsxReportConfiguration();
            exporter.setConfiguration(xlsxReportConfiguration);
			exporter.exportReport();
			document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + diretorioRelativoOS
					+ "/RelatorioPesquisaSolicitacaoServicoXls" + strCompl + "_" + usuario.getIdUsuario() + ".xlsx')");
		} catch (final OutOfMemoryError e) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.erro.erroServidor"));
		}
		document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
		this.limpaRegistrosDaPesquisa(document);
	}

	/**
	 * defeito-182765 - PesquisaSolicitacoesServicos.java_(#tratarDescricaoSemFormatacao).
	 *
	 * @since 10/12/2015
	 * @author ibimon.morais
	 */
	private String tratarDescricaoSemFormatacao(final SolicitacaoServicoDTO solicitacaoServico) {
		String descricaoSemFormatacao = solicitacaoServico.getDescricaoSemFormatacao();
		if (descricaoSemFormatacao != null && descricaoSemFormatacao.length() > 1500) {
			descricaoSemFormatacao = solicitacaoServico.getDescricaoSemFormatacao().substring(0, 1500);
		}
		return descricaoSemFormatacao;
	}

	/**
	 * defeito-182765 - PesquisaSolicitacoesServicos.java_(#tratarRespostaSolicitacao).
	 *
	 * @since 10/12/2015
	 * @author ibimon.morais
	 */
	private String tratarRespostaSolicitacao(final SolicitacaoServicoDTO solicitacaoServico) {
		String resposta = solicitacaoServico.getResposta();
		if (resposta != null && resposta.length() > 1500) {
			resposta = resposta.substring(0, 1500);
		}
		return resposta;
	}

	public void preencherComboUnidade(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) {

		final String UNIDADE_AUTOCOMPLETE = ParametroUtil.getValorParametroCitSmartHashMap(br.com.centralit.citcorpore.util.Enumerados.ParametroSistema.UNIDADE_AUTOCOMPLETE, "N");
		if (UNIDADE_AUTOCOMPLETE == null || !UNIDADE_AUTOCOMPLETE.equalsIgnoreCase("S")) {
			usuario = WebUtil.getUsuario(request);
			final PesquisaSolicitacaoServicoDTO pesquisaSolicitacaoServicoDto = (PesquisaSolicitacaoServicoDTO) document.getBean();
			final String UNIDADE_VINC_CONTRATOS = ParametroUtil.getValorParametroCitSmartHashMap(br.com.centralit.citcorpore.util.Enumerados.ParametroSistema.UNIDADE_VINC_CONTRATOS, "N");

			Integer idContrato = 0;
			if (UNIDADE_VINC_CONTRATOS != null && UNIDADE_VINC_CONTRATOS.equalsIgnoreCase("S")) {
				idContrato = pesquisaSolicitacaoServicoDto.getIdContrato() != null && pesquisaSolicitacaoServicoDto.getIdContrato().intValue() > 0 ? pesquisaSolicitacaoServicoDto.getIdContrato()
						.intValue() : -1;
			}

			EmpregadoDTO empregadoDTO = new EmpregadoDTO();
			try {
				final EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);

				empregadoDTO.setIdEmpregado(usuario.getIdEmpregado());
				empregadoDTO = (EmpregadoDTO) empregadoService.restore(empregadoDTO);
			} catch (final LogicException e1) {
				e1.printStackTrace();
			} catch (final ServiceException e1) {
				e1.printStackTrace();
			} catch (final Exception e) {
				e.printStackTrace();
			}
			final Integer idUnidadeColaborador = empregadoDTO.getIdUnidade() != null && empregadoDTO.getIdUnidade().intValue() > 0 ? empregadoDTO.getIdUnidade() : 0;

			final String tipoHierarquia = ParametroUtil.getValorParametroCitSmartHashMap(br.com.centralit.citcorpore.util.Enumerados.ParametroSistema.TIPO_HIERARQUIA_UNIDADE, "1");

			new ArrayList<UnidadeDTO>();
			Arvore arvore = new Arvore();
			try {
				final UnidadeService unidadeService = (UnidadeService) ServiceLocator.getInstance().getService(UnidadeService.class, null);
				arvore = unidadeService.obtemArvoreUnidades("", idContrato, idUnidadeColaborador, tipoHierarquia, 0);
			} catch (final Exception e1) {
				e1.printStackTrace();
			}

			HTMLSelect comboUnidade;
			try {
				comboUnidade = document.getSelectById("idUnidade");
				if (comboUnidade != null) {
					comboUnidade.removeAllOptions();
					comboUnidade.addOption("0", UtilI18N.internacionaliza(request, "citcorpore.comum.todas"));

					for (int i = 0; i < arvore.getListaID().size(); i++) {
						comboUnidade.addOption(arvore.getListaID().get(i).toString(), StringEscapeUtils.escapeJavaScript(arvore.getListaTexto().get(i)));
					}
				}
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Fun��o para retirar caracteres HTML inv�lidos no relat�rio XLS
	 * 
	 * @param texto
	 * @return
	 */
	public static String formataCaracteresInv�lidos(final String texto) {
		return Jsoup.parse(texto).text();
	}
	
	public void limpaRegistrosDaPesquisa(final DocumentHTML document) throws Exception{
		document.getElementById("paginacao").setValue("0");
		document.getElementById("pagAtual").setValue("0");
		document.getElementById("paginaAtual").setInnerHTML("0");
        document.getElementById("paginaTotal").setInnerHTML("0");
        document.getElementById("totalPagina").setValue("0");
        document.getElementById("totalItens").setValue("0");
        document.getElementById("totalItensPorPagina").setValue("0");
        
        document.getElementById("tblResumo").setInnerHTML(" ");
	}

}
