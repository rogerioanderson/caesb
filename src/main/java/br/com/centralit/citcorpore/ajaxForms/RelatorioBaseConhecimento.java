/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.fill.JRAbstractLRUVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;
import br.com.centralit.citcorpore.bean.PastaDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.BaseConhecimentoService;
import br.com.centralit.citcorpore.negocio.ContadorAcessoService;
import br.com.centralit.citcorpore.negocio.PastaService;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.LogoRel;
import br.com.centralit.citcorpore.util.UtilRelatorio;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
public class RelatorioBaseConhecimento extends AjaxFormAction {

	private UsuarioDTO usuario;

	private String localeSession = null;

	private BaseConhecimentoService baseConhecimentoService;

	private ContadorAcessoService contadorAcessoService;

	private PastaService pastaService;

	@Override
	/**
	 * Gera Relat�rio PDF da Base de Conhecimento. Moficado em 30.03.2015 por valdoilo.damasceno
	 */
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		WebUtil.validarSeUsuarioEstaNaSessao(request, document);

		this.preencherComboPasta(document, request, response);

		this.preencherComboAprovadas(document, request, response);

		this.preencerComboTipoPeriodo(document, request, response);

	}

	/**
	 * Gera Relat�rio PDF da Base de Conhecimento. Moficado em 30.03.2015 por valdoilo.damasceno
	 */
	public void imprimirRelatorioBaseConhecimento(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		WebUtil.validarSeUsuarioEstaNaSessao(request, document);

		BaseConhecimentoDTO baseConhecimentoDto = (BaseConhecimentoDTO) document.getBean();

		usuario = WebUtil.getUsuario(request);

		if (baseConhecimentoDto != null && baseConhecimentoDto.getTipoPeriodo().equalsIgnoreCase("PERIODO_ACESSO") && baseConhecimentoDto.getUltimoAcesso() != null
				&& baseConhecimentoDto.getUltimoAcesso().equals("N")) {
			document.alert(UtilI18N.internacionaliza(request, "relatorioBaseConhecimento.selecioneExibirUltimoPeriodo"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		HttpSession session = ((HttpServletRequest) request).getSession();

		BaseConhecimentoDTO baseConhecimento = new BaseConhecimentoDTO();

		PastaDTO pasta = new PastaDTO();

		CompararBaseConhecimento comparaAcesso = new CompararBaseConhecimento();

		CompararBaseConhecimentoMedia comparaMedia = new CompararBaseConhecimentoMedia();

		CompararBaseConhecimentoPorVersao comparaPorVersao = new CompararBaseConhecimentoPorVersao();

		List<BaseConhecimentoDTO> listaGeral = new ArrayList<BaseConhecimentoDTO>();

		Collection<BaseConhecimentoDTO> listaBaseConhecimento = this.getBaseConhecimentoService().listaBaseConhecimento(baseConhecimentoDto);

		Collection<BaseConhecimentoDTO> listaUltimasVersoes = this.getBaseConhecimentoService().listaBaseConhecimentoUltimasVersoes(baseConhecimentoDto);

		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros = UtilRelatorio.trataInternacionalizacaoLocale(session, parametros);

		parametros.put("TITULO_RELATORIO", UtilI18N.internacionaliza(request, "relatorioBaseConhecimento.relatorioBaseConhecimento"));
		parametros.put("CIDADE", getCidadeParametrizada(request));
		parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
		parametros.put("NOME_USUARIO", usuario.getNomeUsuario());
		parametros.put("ocularCampoConteudo", baseConhecimentoDto.getOcultarConteudo());
		parametros.put("dataInicio", baseConhecimentoDto.getDataInicio());
		parametros.put("dataFim", baseConhecimentoDto.getDataFim());
		parametros.put("dataInicioPublicada", baseConhecimentoDto.getDataInicioPublicacao());
		parametros.put("dataFimPublicada", baseConhecimentoDto.getDataFimPublicacao());
		parametros.put("dataInicioExpiracao", baseConhecimentoDto.getDataInicioExpiracao());
		parametros.put("dataFimExpiracao", baseConhecimentoDto.getDataFimExpiracao());
		parametros.put("dataInicioAcesso", baseConhecimentoDto.getDataInicioAcesso());
		parametros.put("dataFimAcesso", baseConhecimentoDto.getDataFimAcesso());
		parametros.put("Logo", LogoRel.getFile());
		parametros.put("TipoPeriodo", baseConhecimentoDto.getTipoPeriodo());
		if (baseConhecimentoDto.getIdPasta() != null) {
			pasta.setId(baseConhecimentoDto.getIdPasta());
			pasta = (PastaDTO) this.getPastaService().restore(pasta);
			parametros.put("nomePasta", pasta.getNome());
		} else {
			parametros.put("nomePasta", null);
		}
		if (baseConhecimentoDto.getIdBaseConhecimento() != null) {
			baseConhecimento = (BaseConhecimentoDTO) this.getBaseConhecimentoService().restore(baseConhecimentoDto);
			parametros.put("baseConhecimentoTitulo", baseConhecimento.getTitulo());
		} else {
			parametros.put("baseConhecimentoTitulo", null);
		}
		if (baseConhecimentoDto.getTermoPesquisaNota() != null) {
			if (baseConhecimentoDto.getTermoPesquisaNota().equalsIgnoreCase("S")) {
				parametros.put("nota", UtilI18N.internacionaliza(request, "relatorioBaseConhecimento.semAvaliacao"));
			} else {
				if (baseConhecimentoDto.getTermoPesquisaNota().equalsIgnoreCase("")) {
					parametros.put("nota", UtilI18N.internacionaliza(request, "citcorpore.comum.todos"));
				} else {
					parametros.put("nota", baseConhecimentoDto.getTermoPesquisaNota());
				}
			}
		} else {
			parametros.put("nota", null);
		}
		if (baseConhecimentoDto.getStatus() != null) {
			if (baseConhecimentoDto.getStatus().equalsIgnoreCase("")) {
				parametros.put("situacao", UtilI18N.internacionaliza(request, "citcorpore.comum.todos"));
			} else {
				baseConhecimentoDto.setStatus(baseConhecimentoDto.getStatus() != null && baseConhecimentoDto.getStatus().equals("S") ? UtilI18N.internacionaliza(request,
						"pesquisaBaseConhecimento.publicado") : UtilI18N.internacionaliza(request, "pesquisaBaseConhecimento.naoPublicado"));
				parametros.put("situacao", baseConhecimentoDto.getStatus());
			}
		} else {
			parametros.put("situacao", null);
		}

		if (baseConhecimentoDto.getUltimoAcesso() != null && baseConhecimentoDto.getUltimoAcesso().equals("S")) {
			parametros.put("ultimoacesso", UtilI18N.internacionaliza(request, "citcorpore.comum.sim"));
		} else {
			parametros.put("ultimoacesso", UtilI18N.internacionaliza(request, "citcorpore.comum.nao"));
		}

		JRDataSource dataSource = null;
		if (listaBaseConhecimento == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		if (baseConhecimentoDto.getAcessado() == null) {
			listaGeral.addAll(this.listarBaseConhecimento(request, listaBaseConhecimento, baseConhecimentoDto));
		}

		else if (baseConhecimentoDto.getAcessado() != null && baseConhecimentoDto.getAcessado().equalsIgnoreCase("A")) {
			parametros.put("ordenacao", UtilI18N.internacionaliza(request, "relatorioBaseConhecimento.ordenarPorQuantidadeAcessos"));

			listaGeral.addAll(this.listarBaseConhecimento(request, listaBaseConhecimento, baseConhecimentoDto));
			Collections.sort(listaGeral, comparaAcesso);

		}

		else if (baseConhecimentoDto.getAcessado() != null && baseConhecimentoDto.getAcessado().equalsIgnoreCase("M")) {
			parametros.put("ordenacao", UtilI18N.internacionaliza(request, "relatorioBaseConhecimento.ordenarPorMediaAvaliacao"));
			listaGeral.addAll(this.listarBaseConhecimento(request, listaBaseConhecimento, baseConhecimentoDto));
			Collections.sort(listaGeral, comparaMedia);
		} else {
			parametros.put("ordenacao", UtilI18N.internacionaliza(request, "relatorioBaseConhecimento.ordenarPorVersao"));
			listaGeral.addAll(this.listarBaseConhecimento(request, listaBaseConhecimento, baseConhecimentoDto));
			Collections.sort(listaGeral, comparaPorVersao);
		}
		if (baseConhecimentoDto.getUltimaVersao() != null && baseConhecimentoDto.getUltimaVersao().equals("S")) {
			parametros.put("ultimasVersoes", UtilI18N.internacionaliza(request, "relatorioBaseConhecimento.ultimasVersoes"));
			if (listaUltimasVersoes != null) {
				listaGeral = new ArrayList<BaseConhecimentoDTO>();
				listaGeral.addAll(this.listarBaseConhecimento(request, listaUltimasVersoes, baseConhecimentoDto));
				if (baseConhecimentoDto.getAcessado() != null && baseConhecimentoDto.getAcessado().equalsIgnoreCase("A")) {
					Collections.sort(listaGeral, comparaAcesso);
				} else if (baseConhecimentoDto.getAcessado() != null && baseConhecimentoDto.getAcessado().equalsIgnoreCase("M")) {
					Collections.sort(listaGeral, comparaMedia);
				} else {
					Collections.sort(listaGeral, comparaPorVersao);
				}
			}
		}

		if (listaGeral.size() == 0) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}
		parametros.put("totalBaseConhecimento", listaGeral.size());
		try {
			dataSource = new JRBeanCollectionDataSource(listaGeral);

			Date dt = new Date();
			String strCompl = "" + dt.getTime();
			String caminhoJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioBaseConhecimento.jasper";
			String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
			String diretorioRelativoOS = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";

			JRSwapFile arquivoSwap = new JRSwapFile(diretorioReceita, 4096, 25);

			// Instancia o virtualizador
			JRAbstractLRUVirtualizer virtualizer = new JRSwapFileVirtualizer(25, arquivoSwap, true);

			// Seta o parametro REPORT_VIRTUALIZER com a inst�ncia da virtualiza��o
			parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);

			// Preenche o relat�rio e exibe numa GUI
			JasperPrint print = JasperFillManager.fillReport(caminhoJasper, parametros, dataSource);
			// JasperViewer.viewReport(print,false);

			JasperExportManager.exportReportToPdfFile(print, diretorioReceita + "/RelatorioBaseConhecimento" + strCompl + "_" + usuario.getIdUsuario() + ".pdf");

			document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + diretorioRelativoOS
					+ "/RelatorioBaseConhecimento" + strCompl + "_" + usuario.getIdUsuario() + ".pdf')");

		} catch (OutOfMemoryError e) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.erro.erroServidor"));
		}
		document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
	}

	/**
	 * Gera Relat�rio XLS da Base de Conhecimento. Moficado em 30.03.2015 por valdoilo.damasceno
	 */
	public void imprimirRelatorioBaseConhecimentoXls(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		WebUtil.validarSeUsuarioEstaNaSessao(request, document);

		BaseConhecimentoDTO baseConhecimentoDto = (BaseConhecimentoDTO) document.getBean();

		HttpSession session = ((HttpServletRequest) request).getSession();

		BaseConhecimentoDTO baseConhecimento = new BaseConhecimentoDTO();

		usuario = WebUtil.getUsuario(request);

		if (baseConhecimentoDto != null && baseConhecimentoDto.getTipoPeriodo().equalsIgnoreCase("PERIODO_ACESSO") && baseConhecimentoDto.getUltimoAcesso() != null
				&& baseConhecimentoDto.getUltimoAcesso().equals("N")) {
			document.alert(UtilI18N.internacionaliza(request, "relatorioBaseConhecimento.selecioneExibirUltimoPeriodo"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		PastaDTO pasta = new PastaDTO();
		CompararBaseConhecimento comparaAcesso = new CompararBaseConhecimento();
		CompararBaseConhecimentoMedia comparaMedia = new CompararBaseConhecimentoMedia();
		CompararBaseConhecimentoPorVersao comparaPorVersao = new CompararBaseConhecimentoPorVersao();
		List<BaseConhecimentoDTO> listaGeral = new ArrayList<BaseConhecimentoDTO>();
		List<BaseConhecimentoDTO> listaBaseConhecimentoUltimasVersoes = new ArrayList<BaseConhecimentoDTO>();
		Collection<BaseConhecimentoDTO> listaBaseConhecimento = this.getBaseConhecimentoService().listaBaseConhecimento(baseConhecimentoDto);
		Collection<BaseConhecimentoDTO> listaUltimasVersoes = this.getBaseConhecimentoService().listaBaseConhecimentoUltimasVersoes(baseConhecimentoDto);

		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros = UtilRelatorio.trataInternacionalizacaoLocale(session, parametros);

		parametros.put("TITULO_RELATORIO", UtilI18N.internacionaliza(request, "relatorioBaseConhecimento.relatorioBaseConhecimento"));
		parametros.put("CIDADE", getCidadeParametrizada(request));
		parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
		parametros.put("NOME_USUARIO", usuario.getNomeUsuario());
		parametros.put("ocularCampoConteudo", baseConhecimentoDto.getOcultarConteudo());
		parametros.put("dataInicio", baseConhecimentoDto.getDataInicio());
		parametros.put("dataFim", baseConhecimentoDto.getDataFim());
		parametros.put("dataInicioPublicada", baseConhecimentoDto.getDataInicioPublicacao());
		parametros.put("dataFimPublicada", baseConhecimentoDto.getDataFimPublicacao());
		parametros.put("dataInicioExpiracao", baseConhecimentoDto.getDataInicioExpiracao());
		parametros.put("dataFimExpiracao", baseConhecimentoDto.getDataFimExpiracao());
		parametros.put("dataInicioAcesso", baseConhecimentoDto.getDataInicioAcesso());
		parametros.put("dataFimAcesso", baseConhecimentoDto.getDataFimAcesso());
		parametros.put("Logo", LogoRel.getFile());
		parametros.put("TipoPeriodo", baseConhecimentoDto.getTipoPeriodo());

		if (baseConhecimentoDto.getIdPasta() != null) {
			pasta.setId(baseConhecimentoDto.getIdPasta());
			pasta = (PastaDTO) this.getPastaService().restore(pasta);
			parametros.put("nomePasta", pasta.getNome());
		} else {
			parametros.put("nomePasta", null);
		}

		if (baseConhecimentoDto.getIdBaseConhecimento() != null) {
			baseConhecimento = (BaseConhecimentoDTO) this.getBaseConhecimentoService().restore(baseConhecimentoDto);
			parametros.put("baseConhecimentoTitulo", baseConhecimento.getTitulo());
		} else {
			parametros.put("baseConhecimentoTitulo", null);
		}

		if (baseConhecimentoDto.getTermoPesquisaNota() != null) {
			if (baseConhecimentoDto.getTermoPesquisaNota().equalsIgnoreCase("S")) {
				parametros.put("nota", "Sem Coment�rio");
			} else {
				if (baseConhecimentoDto.getTermoPesquisaNota().equalsIgnoreCase("")) {
					parametros.put("nota", "Todos");
				} else {
					parametros.put("nota", baseConhecimentoDto.getTermoPesquisaNota());
				}
			}
		} else {
			parametros.put("nota", null);
		}

		if (baseConhecimentoDto.getUltimoAcesso() != null && baseConhecimentoDto.getUltimoAcesso().equals("S")) {
			parametros.put("ultimoacesso", "Sim");
		} else {
			parametros.put("ultimoacesso", "N�o");
		}

		if (baseConhecimentoDto.getUltimoAcesso() != null && baseConhecimentoDto.getUltimoAcesso().equals("S")) {
			parametros.put("ultimoacesso", "Sim");
		} else {
			parametros.put("ultimoacesso", "N�o");
		}

		if (baseConhecimentoDto.getStatus() != null) {
			if (baseConhecimentoDto.getStatus().equalsIgnoreCase("")) {
				parametros.put("situacao", "Todos");
			} else {
				baseConhecimentoDto.setStatus(baseConhecimentoDto.getStatus() != null && baseConhecimentoDto.getStatus().equals("S") ? UtilI18N.internacionaliza(request,
						"pesquisaBaseConhecimento.publicado") : UtilI18N.internacionaliza(request, "pesquisaBaseConhecimento.naoPublicado"));
				parametros.put("situacao", baseConhecimentoDto.getStatus());
			}
		} else {
			parametros.put("situacao", null);
		}

		JRDataSource dataSource = null;
		if (listaBaseConhecimento == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		if (baseConhecimentoDto.getAcessado() == null) {
			listaGeral.addAll(this.listarBaseConhecimento(request, listaBaseConhecimento, baseConhecimentoDto));
		}

		else if (baseConhecimentoDto.getAcessado() != null && baseConhecimentoDto.getAcessado().equalsIgnoreCase("A")) {
			parametros.put("ordenacao", UtilI18N.internacionaliza(request, "relatorioBaseConhecimento.ordenarPorQuantidadeAcessos"));

			listaGeral.addAll(this.listarBaseConhecimento(request, listaBaseConhecimento, baseConhecimentoDto));
			Collections.sort(listaGeral, comparaAcesso);

		}

		else if (baseConhecimentoDto.getAcessado() != null && baseConhecimentoDto.getAcessado().equalsIgnoreCase("M")) {
			parametros.put("ordenacao", UtilI18N.internacionaliza(request, "relatorioBaseConhecimento.ordenarPorMediaAvaliacao"));
			listaGeral.addAll(this.listarBaseConhecimento(request, listaBaseConhecimento, baseConhecimentoDto));
			Collections.sort(listaGeral, comparaMedia);
		} else {
			parametros.put("ordenacao", UtilI18N.internacionaliza(request, "relatorioBaseConhecimento.ordenarPorVersao"));
			listaGeral.addAll(this.listarBaseConhecimento(request, listaBaseConhecimento, baseConhecimentoDto));
			Collections.sort(listaGeral, comparaPorVersao);
		}
		if (baseConhecimentoDto.getUltimaVersao() != null && baseConhecimentoDto.getUltimaVersao().equals("S")) {
			parametros.put("ultimasVersoes", UtilI18N.internacionaliza(request, "relatorioBaseConhecimento.ultimasVersoes"));
			if (listaUltimasVersoes != null) {
				listaGeral.addAll(this.listarBaseConhecimento(request, listaBaseConhecimentoUltimasVersoes, baseConhecimentoDto));
				if (baseConhecimentoDto.getAcessado() != null && baseConhecimentoDto.getAcessado().equalsIgnoreCase("A")) {
					Collections.sort(listaGeral, comparaAcesso);
				} else if (baseConhecimentoDto.getAcessado() != null && baseConhecimentoDto.getAcessado().equalsIgnoreCase("M")) {
					Collections.sort(listaGeral, comparaMedia);
				} else {
					Collections.sort(listaGeral, comparaPorVersao);
				}
			}

		}

		if (listaGeral.size() == 0) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}
		parametros.put("totalBaseConhecimento", listaGeral.size());
		dataSource = new JRBeanCollectionDataSource(listaGeral);

		Date dt = new Date();
		String strCompl = "" + dt.getTime();
		String caminhoJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioBaseConhecimentoXls.jasper";
		String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
		String diretorioRelativoOS = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";

		JasperDesign desenho = JRXmlLoader.load(CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioBaseConhecimentoXls.jrxml");

		JasperReport relatorio = JasperCompileManager.compileReport(desenho);

		JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, dataSource);

		JRXlsExporter exporter = new JRXlsExporter();
		exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, impressao);
		exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
		exporter.setParameter(JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED, Boolean.TRUE);
		exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, diretorioReceita + "/RelatorioBaseConhecimentoXls" + strCompl + "_" + usuario.getIdUsuario() + ".xls");

		exporter.exportReport();

		document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + diretorioRelativoOS
				+ "/RelatorioBaseConhecimentoXls" + strCompl + "_" + usuario.getIdUsuario() + ".xls')");
		document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();

	}

	public List<BaseConhecimentoDTO> listarBaseConhecimento(HttpServletRequest request, Collection<BaseConhecimentoDTO> listaBaseConhecimento, BaseConhecimentoDTO baseConhecimentoDto)
			throws Exception {
		List<BaseConhecimentoDTO> listaGeral = new ArrayList<BaseConhecimentoDTO>();
		List<BaseConhecimentoDTO> listaBaseConhecimentoRetorno = new ArrayList<BaseConhecimentoDTO>();

		for (BaseConhecimentoDTO base : listaBaseConhecimento) {
			if (baseConhecimentoDto.getTermoPesquisaNota() != null && !baseConhecimentoDto.getTermoPesquisaNota().equalsIgnoreCase("S")
					&& !baseConhecimentoDto.getTermoPesquisaNota().equalsIgnoreCase("")) {
				base.setMedia(this.calcularMdia(base));
				base.setContadorCliques(this.getContadorAcessoService().quantidadesDeAcessoPorBaseConhecimnto(base));
				if (base.getMedia().equalsIgnoreCase(baseConhecimentoDto.getTermoPesquisaNota())) {
					listaBaseConhecimentoRetorno.add(base);
					listaGeral = listaBaseConhecimentoRetorno;
				}
			} else {
				base.setContadorCliques(this.getContadorAcessoService().quantidadesDeAcessoPorBaseConhecimnto(base));
				base.setMedia(this.calcularMdia(base));
				listaBaseConhecimentoRetorno.add(base);
				listaGeral = listaBaseConhecimentoRetorno;
			}
			base.setStatus(base.getStatus() != null && base.getStatus().equals("S") ? UtilI18N.internacionaliza(request, "pesquisaBaseConhecimento.publicado") : UtilI18N.internacionaliza(request,
					"pesquisaBaseConhecimento.naoPublicado"));
		}

		return listaGeral;
	}

	@Override
	public Class getBeanClass() {
		return BaseConhecimentoDTO.class;
	}

	public void preencherComboPasta(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		HTMLSelect comboPasta = (HTMLSelect) document.getSelectById("idPasta");

		ArrayList<PastaDTO> listaPastas = (ArrayList) this.getPastaService().list();

		inicializaCombo(comboPasta, request);
		for (PastaDTO pasta : listaPastas)
			if (pasta.getDataFim() == null)
				comboPasta.addOption(pasta.getId().toString(), pasta.getNome());
	}

	private void inicializaCombo(HTMLSelect componenteCombo, HttpServletRequest request) {
		componenteCombo.removeAllOptions();
		componenteCombo.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.todos"));
	}

	public void preencherComboBaseConhecimentoPorPasta(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		BaseConhecimentoDTO baseConhecimentoDto = (BaseConhecimentoDTO) document.getBean();

		HTMLSelect comboBaseConhecimento = (HTMLSelect) document.getSelectById("idBaseConhecimento");

		PastaDTO pastaDto = new PastaDTO();

		pastaDto.setId(baseConhecimentoDto.getIdPasta());

		ArrayList<BaseConhecimentoDTO> listaBaseConhecimentoPorPasta = (ArrayList) this.getBaseConhecimentoService().listarBaseConhecimentoByPastaRelatorio(pastaDto);

		inicializaCombo(comboBaseConhecimento, request);

		if (listaBaseConhecimentoPorPasta != null) {
			for (BaseConhecimentoDTO base : listaBaseConhecimentoPorPasta) {
				comboBaseConhecimento.addOption(base.getIdBaseConhecimento().toString(), base.getTitulo());
			}
		}
	}

	/**
	 * Preenche Combo com os Tipos de Per�odo do enumerado TipoPeriodoBaseConhecimento.
	 * 
	 * @author valdoilo.damasceno
	 * @since 27.03.2015
	 */
	private void preencerComboTipoPeriodo(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		HTMLSelect comboTipoPeriodo = (HTMLSelect) document.getSelectById("idTipoPeriodo");

		comboTipoPeriodo.removeAllOptions();

		for (Enumerados.TipoPeriodoBaseConhecimento tipo : Enumerados.TipoPeriodoBaseConhecimento.values()) {
			comboTipoPeriodo.addOption(tipo.name(), UtilI18N.internacionaliza(request, tipo.getNomePeriodo()));
		}
	}

	public void preencherComboAprovadas(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HTMLSelect comboAprovados = (HTMLSelect) document.getSelectById("status");
		inicializaCombo(comboAprovados, request);
		comboAprovados.addOption("S", UtilI18N.internacionaliza(request, "pesquisaBaseConhecimento.publicado"));
		comboAprovados.addOption("N", UtilI18N.internacionaliza(request, "pesquisaBaseConhecimento.naoPublicado"));

	}

	public String calcularMdia(BaseConhecimentoDTO baseConhecimento) throws Exception {

		Double media = this.getBaseConhecimentoService().calcularNota(baseConhecimento.getIdBaseConhecimento());

		if (media != null) {
			return media.toString();
		} else {
			return "-1";
		}
	}

	/**
	 * Retorna inst�ncia de BaseConhecimentoService.
	 * 
	 * @return BaseConhecimentoService
	 * @throws ServiceException
	 * @author valdoilo.damasceno
	 * @since 27.03.2015
	 */
	public BaseConhecimentoService getBaseConhecimentoService() throws ServiceException {
		if (baseConhecimentoService == null) {
			baseConhecimentoService = (BaseConhecimentoService) ServiceLocator.getInstance().getService(BaseConhecimentoService.class, null);
		}

		return baseConhecimentoService;
	}

	/**
	 * Retorna inst�ncia de ContadorAcessoService.
	 * 
	 * @return ContadorAcessoService
	 * @throws ServiceException
	 * @author valdoilo.damasceno
	 * @since 27.03.2015
	 */
	public ContadorAcessoService getContadorAcessoService() throws ServiceException {

		if (contadorAcessoService == null) {
			contadorAcessoService = (ContadorAcessoService) ServiceLocator.getInstance().getService(ContadorAcessoService.class, null);
		}

		return contadorAcessoService;
	}

	/**
	 * Retorna inst�ncia de PastaService.
	 * 
	 * @return PastaService
	 * @throws ServiceException
	 * @author valdoilo.damasceno
	 * @since 27.03.2015
	 */
	public PastaService getPastaService() throws ServiceException {

		if (pastaService == null) {
			pastaService = (PastaService) ServiceLocator.getInstance().getService(PastaService.class, null);
		}

		return pastaService;
	}

}
