/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.NotificacaoDTO;
import br.com.centralit.citcorpore.negocio.NotificacaoService;
import br.com.centralit.citcorpore.util.Enumerados.TipoNotificacao;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilI18N;

public class VisualizarNotificacoes extends AjaxFormAction {

	@Override
	public Class getBeanClass() {
		return NotificacaoDTO.class;
	}

	@Override
	public void load(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		NotificacaoDTO notificacaoDTO = (NotificacaoDTO)document.getBean();
		NotificacaoService serviceNotificacao = (NotificacaoService) ServiceLocator.getInstance().getService(NotificacaoService.class, null);

		Collection colNotificacaoServico = serviceNotificacao.consultarNotificacaoAtivosOrigemServico(notificacaoDTO.getIdContratoNotificacao());
		List colFinal = new ArrayList();
		
		if (colNotificacaoServico != null){
			for(Iterator it = colNotificacaoServico.iterator(); it.hasNext();){
				NotificacaoDTO notificacaoAux = (NotificacaoDTO)it.next();
				if(!notificacaoAux.getTipoNotificacao().isEmpty()){
					if(notificacaoAux.getTipoNotificacao().equalsIgnoreCase("T")){
						notificacaoAux.setNomeTipoNotificacao(UtilI18N.internacionaliza(request, TipoNotificacao.ServTodasAlt.getDescricao()));
					}else if(notificacaoAux.getTipoNotificacao().equalsIgnoreCase("C")){
						notificacaoAux.setNomeTipoNotificacao(UtilI18N.internacionaliza(request, TipoNotificacao.ServADICIONADOS.getDescricao()));
					}else if(notificacaoAux.getTipoNotificacao().equalsIgnoreCase("A")){
						notificacaoAux.setNomeTipoNotificacao(UtilI18N.internacionaliza(request, TipoNotificacao.ServALTERADOS.getDescricao()));
					}else{
						notificacaoAux.setNomeTipoNotificacao(UtilI18N.internacionaliza(request, TipoNotificacao.ServEXCLUIDOS.getDescricao()));
					}
				}
					colFinal.add(notificacaoAux);
				
			}
		}
	//	Collections.sort(colFinal, new ObjectSimpleComparator("getNomeServico", ObjectSimpleComparator.ASC));
		request.setAttribute("listaNotificacoes", colFinal);
	}

}
