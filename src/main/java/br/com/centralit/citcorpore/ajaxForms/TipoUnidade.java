/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.TipoUnidadeDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.TipoUnidadeService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

public class TipoUnidade extends AjaxFormAction {

	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		UsuarioDTO usuario = WebUtil.getUsuario(request);

		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		/*
		 * if (!WebUtil.isUserInGroup(request, Constantes.getValue("GRUPO_DIRETORIA"))){ document.alert("Voc� n�o tem permiss�o para acessar esta funcionalidade!" );
		 * document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "/pages/index/index.jsp'"); return; }
		 */

		// document.focusInFirstActivateField(null);
	}

	public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		TipoUnidadeDTO tipoUnidade = (TipoUnidadeDTO) document.getBean();

		TipoUnidadeService tipoUnidadeService = (TipoUnidadeService) ServiceLocator.getInstance().getService(TipoUnidadeService.class, null);

		if (!tipoUnidadeService.jaExisteUnidadeComMesmoNome(tipoUnidade.getNomeTipoUnidade())) {
			if (tipoUnidade.getIdTipoUnidade() == null || tipoUnidade.getIdTipoUnidade().intValue() == 0) {
				tipoUnidade.setIdEmpresa(WebUtil.getIdEmpresa(request));

				tipoUnidadeService.create(tipoUnidade);
				document.alert(UtilI18N.internacionaliza(request, "MSG05"));
			} else {
				tipoUnidadeService.update(tipoUnidade);
				document.alert(UtilI18N.internacionaliza(request, "MSG06"));
			}
		} else {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.registroJaCadastrado"));
		}
		HTMLForm form = document.getForm("form");
		form.clear();

		document.executeScript("limpar_LOOKUP_TIPOUNIDADE()");

	}

	public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		TipoUnidadeDTO tipoUnidade = (TipoUnidadeDTO) document.getBean();
		TipoUnidadeService testeService = (TipoUnidadeService) ServiceLocator.getInstance().getService(TipoUnidadeService.class, null);

		tipoUnidade = (TipoUnidadeDTO) testeService.restore(tipoUnidade);

		HTMLForm form = document.getForm("form");
		form.clear();
		form.setValues(tipoUnidade);

	}

	public void delete(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		TipoUnidadeDTO tipoUnidade = (TipoUnidadeDTO) document.getBean();
		TipoUnidadeService tipoUnidadeService = (TipoUnidadeService) ServiceLocator.getInstance().getService(TipoUnidadeService.class, null);

		if (tipoUnidade.getIdTipoUnidade() != null && tipoUnidade.getIdTipoUnidade() != 0) {
			tipoUnidade.setDataFim(UtilDatas.getDataAtual());
			tipoUnidadeService.update(tipoUnidade);

			HTMLForm form = document.getForm("form");
			form.clear();
			document.alert(UtilI18N.internacionaliza(request, "MSG07"));
		}

		document.executeScript("limpar_LOOKUP_TIPOUNIDADE()");
	}

	@SuppressWarnings("rawtypes")
	public Class getBeanClass() {
		return TipoUnidadeDTO.class;
	}
}
