/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.AssinaturaDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.ItemGrupoAssinaturaDTO;
import br.com.centralit.citcorpore.negocio.AssinaturaService;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.ItemGrupoAssinaturaService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

/**
 * @author euler.ramos
 *
 */
@SuppressWarnings("rawtypes")
public class Assinatura extends AjaxFormAction {

    @Override
    public void load(DocumentHTML document, HttpServletRequest request,	HttpServletResponse response) throws Exception {
	StringBuilder objeto;
	objeto = new StringBuilder();
	objeto.append("<option> "+UtilI18N.internacionaliza(request, "citcorpore.comum.solicitacao"));
	objeto.append("<option> "+UtilI18N.internacionaliza(request, "citcorpore.comum.autorizacao"));
	objeto.append("<option> "+UtilI18N.internacionaliza(request, "citcorpore.comum.aprovacao"));
	objeto.append("<option> "+UtilI18N.internacionaliza(request, "citcorpore.comum.execucao"));
	document.getElementById("listaFases").setInnerHTML(objeto.toString());

	objeto = new StringBuilder();
	objeto.append("<option> "+UtilI18N.internacionaliza(request, "citcorpore.comum.solicitanteServicos"));
	objeto.append("<option> "+UtilI18N.internacionaliza(request, "citcorpore.comum.gestorOperacionalContrato"));
	objeto.append("<option> "+UtilI18N.internacionaliza(request, "citcorpore.comum.gestorContrato"));
	objeto.append("<option> "+UtilI18N.internacionaliza(request, "citcorpore.comum.prepostoContratada"));
	document.getElementById("listaPapeis").setInnerHTML(objeto.toString());
    }

    @Override
    public Class getBeanClass() {
	return AssinaturaDTO.class;
    }

    public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
	AssinaturaDTO assinaturaDTO = (AssinaturaDTO) document.getBean();
	AssinaturaService assinaturaService = (AssinaturaService) ServiceLocator.getInstance().getService(AssinaturaService.class,null);

	// H� possibilidade de o usu�rio desejar uma assinatura sem empregado.
	if ((assinaturaDTO.getIdEmpregado() != null)
		&& (assinaturaDTO.getIdEmpregado().equals(0))) {
	    assinaturaDTO.setIdEmpregado(null);
	}

	if (!assinaturaService.violaIndiceUnico(assinaturaDTO)) {
	    if (assinaturaDTO.getIdAssinatura() != null) {
		assinaturaService.update(assinaturaDTO);
		document.alert(UtilI18N.internacionaliza(request,
			"assinatura.assinaturaAtualizada"));
	    } else {
		assinaturaDTO.setDataInicio(UtilDatas.getDataAtual());
		assinaturaService.create(assinaturaDTO);
		document.alert(UtilI18N.internacionaliza(request,
			"assinatura.assinaturaCadastrada"));
	    }
	    HTMLForm form = document.getForm("form");
	    form.clear();
	} else {
	    document.alert(UtilI18N.internacionaliza(request,
		    "citcorpore.comum.registroJaAdicionado"));
	}
    }


    public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
	AssinaturaDTO assinaturaDTO = (AssinaturaDTO) document.getBean();
	AssinaturaService assinaturaService = (AssinaturaService) ServiceLocator.getInstance().getService(AssinaturaService.class,null);

	assinaturaDTO = (AssinaturaDTO) assinaturaService.restore(assinaturaDTO);

	if (assinaturaDTO.getIdEmpregado()!= null){
	    EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
	    EmpregadoDTO empregadoDTO = new EmpregadoDTO();
	    empregadoDTO.setIdEmpregado(assinaturaDTO.getIdEmpregado());
	    empregadoDTO = (EmpregadoDTO) empregadoService.restore(empregadoDTO);
	    assinaturaDTO.setNomeResponsavel(empregadoDTO.getNome());
	}

	HTMLForm form = document.getForm("form");
	form.clear();
	form.setValues(assinaturaDTO);
    }

    public void excluir(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
	AssinaturaDTO assinaturaDTO = (AssinaturaDTO) document.getBean();

	ItemGrupoAssinaturaService itemGrupoAssinaturaService = (ItemGrupoAssinaturaService) ServiceLocator.getInstance().getService(ItemGrupoAssinaturaService.class, null);
	ArrayList<ItemGrupoAssinaturaDTO> listaItemGrupoAssinaturas = (ArrayList<ItemGrupoAssinaturaDTO>) itemGrupoAssinaturaService.findByIdAssinatura(assinaturaDTO.getIdAssinatura());

	if ((listaItemGrupoAssinaturas == null) || (listaItemGrupoAssinaturas.size() <= 0)) {
	    if (assinaturaDTO.getIdAssinatura() != null) {
		AssinaturaService assinaturaService = (AssinaturaService) ServiceLocator.getInstance().getService(AssinaturaService.class, null);
		assinaturaDTO.setDataFim(UtilDatas.getDataAtual());
		assinaturaService.update(assinaturaDTO);
		document.alert(UtilI18N.internacionaliza(request, "assinatura.assinaturaExcluida"));
	    }
	    HTMLForm form = document.getForm("form");
	    form.clear();
	} else {
	    document.alert(UtilI18N.internacionaliza(request, "assinatura.alerta.exclusaoNaoPermitida"));
	}

    }

}
