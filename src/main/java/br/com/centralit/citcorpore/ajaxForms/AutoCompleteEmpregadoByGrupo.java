/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.AutoCompleteDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.citframework.service.ServiceLocator;

import com.google.gson.Gson;

/**
 * AutoComplete para {@link EmpregadoDTO} que considera na consulta o {@link GrupoDTO}
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 31/10/2014
 *
 */
public class AutoCompleteEmpregadoByGrupo extends AjaxFormAction {

    private static final Gson GSON = new Gson();

    @Override
    public Class<EmpregadoDTO> getBeanClass() {
        return EmpregadoDTO.class;
    }

    @Override
    public void load(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        final String nomeEmpregado = request.getParameter("query");
        final String idGrupoStr = request.getParameter("idGrupo");
        final Integer idGrupo = StringUtils.isNotBlank(idGrupoStr) ? Integer.parseInt(idGrupoStr.trim()) : 0;

        final Collection<EmpregadoDTO> empregados = this.getService().findByNomeEmpregadoAndGrupo(nomeEmpregado, idGrupo);

        final List<Integer> listIDs = new ArrayList<>();
        final List<String> listNames = new ArrayList<>();

        for (final EmpregadoDTO empregado : empregados) {
            listIDs.add(empregado.getIdEmpregado());
            listNames.add(empregado.getNome());
        }

        final AutoCompleteDTO autoCompleteDTO = new AutoCompleteDTO();
        autoCompleteDTO.setQuery(nomeEmpregado);
        autoCompleteDTO.setSuggestions(listNames);
        autoCompleteDTO.setData(listIDs);

        request.setAttribute("json_response", GSON.toJson(autoCompleteDTO));
    }

    private static EmpregadoService service;

    private EmpregadoService getService() throws Exception {
        if (service == null) {
            service = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
        }
        return service;
    }

}
