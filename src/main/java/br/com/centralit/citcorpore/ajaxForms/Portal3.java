/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLElement;
import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;
import br.com.centralit.citcorpore.bean.PortalDTO;
import br.com.centralit.citcorpore.negocio.BaseConhecimentoService;
import br.com.citframework.service.ServiceLocator;

public class Portal3 extends AjaxFormAction {

	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		BaseConhecimentoService baseServicoService = (BaseConhecimentoService) ServiceLocator.getInstance().getService(BaseConhecimentoService.class, null);
		
		Collection<BaseConhecimentoDTO> listBaseFaq = baseServicoService.listarBaseConhecimentoFAQ();
		
		StringBuilder strFaq = new StringBuilder();
				if (listBaseFaq != null){
					for (BaseConhecimentoDTO baseDTO : listBaseFaq) {
						if(baseDTO.getDataFim() == null){
							
							strFaq.append("<div class='accordion-heading'>");//Inicio div Cabe�alho
    						strFaq.append("<a class='accordion-toggle glyphicons circle_question_mark' data-toggle='collapse' data-parent='#accordion' href='#collapse-"+baseDTO.getIdBaseConhecimento()+"'>"); 
    						strFaq.append("<i></i>"+ baseDTO.getTitulo());//Titulo   								
    						strFaq.append("</a>");//Fim da div Cabe�alho	
    								
    						HTMLElement divFaq = document.getElementById("grupo");//Encontrando o elemento no html
    						divFaq.setInnerHTML(strFaq.toString());//Adicionando o elemento no html
    						strFaq.append("</div>");// Fim div cabe�alho
    						
    						strFaq.append("<div id='collapse-"+baseDTO.getIdBaseConhecimento()+"' class='accordion-body collapse'>");//Inicio div Corpo
    						
    						strFaq.append("<div class='accordion-inner' sytle='text-align:center;'>"+baseDTO.getConteudo()+"<br></div>");//Corpo do conteudo
    						
    						strFaq.append("</div>");//Fim div corpo
    						divFaq.setInnerHTML(strFaq.toString());//Adicionando o elemento no html
    						
						}//Fim do if					
					}//Fim do for
				}//Fim do if
	}//Fim do metodo
		
    public Class<PortalDTO> getBeanClass(){
    	return PortalDTO.class;
    }
}
