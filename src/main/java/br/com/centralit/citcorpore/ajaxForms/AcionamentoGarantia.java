/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citajax.html.HTMLTable;
import br.com.centralit.citcorpore.bean.EntregaItemRequisicaoDTO;
import br.com.centralit.citcorpore.bean.ParecerDTO;
import br.com.centralit.citcorpore.bean.RequisicaoProdutoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.EntregaItemRequisicaoService;
import br.com.centralit.citcorpore.negocio.JustificativaParecerService;
import br.com.centralit.citcorpore.negocio.ParecerService;
import br.com.centralit.citcorpore.negocio.RequisicaoProdutoService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilI18N;

@SuppressWarnings({ "rawtypes" })
public class AcionamentoGarantia extends RequisicaoProduto {

    @Override
    public Class getBeanClass() {
        return RequisicaoProdutoDTO.class;
    }

    @Override
    protected String getAcao() {
        return RequisicaoProdutoDTO.ACAO_GARANTIA;
    }

    @Override
    public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
        JustificativaParecerService justificativaService = (JustificativaParecerService) ServiceLocator.getInstance().getService(JustificativaParecerService.class, WebUtil.getUsuarioSistema(request));
        HTMLSelect idJustificativa = (HTMLSelect) document.getSelectById("item#idJustificativa");
        idJustificativa.removeAllOptions();
        idJustificativa.addOption("", "---");
        Collection colJustificativas = justificativaService.listAplicaveisInspecao();
        if(colJustificativas != null && !colJustificativas.isEmpty())
            idJustificativa.addOptions(colJustificativas, "idJustificativa", "descricaoJustificativa", null);

        super.load(document, request, response);
    }

    public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
        UsuarioDTO usuario = WebUtil.getUsuario(request);
        if (usuario == null) {
            document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
            document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
            return;
        }

        RequisicaoProdutoDTO requisicaoProdutoDto = (RequisicaoProdutoDTO) document.getBean();
        String editar = requisicaoProdutoDto.getEditar();
        Integer idTarefa = requisicaoProdutoDto.getIdTarefa();
        RequisicaoProdutoService requisicaoProdutoService = (RequisicaoProdutoService) ServiceLocator.getInstance().getService(RequisicaoProdutoService.class, WebUtil.getUsuarioSistema(request));

        requisicaoProdutoDto = (RequisicaoProdutoDTO) requisicaoProdutoService.restore(requisicaoProdutoDto);
        if (requisicaoProdutoDto == null)
            return;

        requisicaoProdutoDto.setRejeitada("N");

        requisicaoProdutoDto.setIdTarefa(idTarefa);
        requisicaoProdutoDto.setEditar(editar);
        if (requisicaoProdutoDto.getEditar() == null)
            requisicaoProdutoDto.setEditar("S");

        HTMLTable tblItensRequisicao = document.getTableById("tblItensRequisicao");
        tblItensRequisicao.deleteAllRows();

        EntregaItemRequisicaoService entregaItemRequisicaoService = (EntregaItemRequisicaoService) ServiceLocator.getInstance().getService(EntregaItemRequisicaoService.class, WebUtil.getUsuarioSistema(request));
        Collection<EntregaItemRequisicaoDTO> itensRequisicao = entregaItemRequisicaoService.findNaoAprovadasByIdSolicitacaoServico(requisicaoProdutoDto.getIdSolicitacaoServico());
        if (itensRequisicao != null) {
            ParecerService parecerService = (ParecerService) ServiceLocator.getInstance().getService(ParecerService.class, WebUtil.getUsuarioSistema(request));
            for (EntregaItemRequisicaoDTO entregaItemRequisicaoDto : itensRequisicao) {
                if (entregaItemRequisicaoDto.getIdParecer() == null)
                    continue;
                ParecerDTO parecerDto = new ParecerDTO();
                parecerDto.setIdParecer(entregaItemRequisicaoDto.getIdParecer());
                parecerDto = (ParecerDTO) parecerService.restore(parecerDto);
                if (parecerDto != null) {
                    entregaItemRequisicaoDto.setIdJustificativa(parecerDto.getIdJustificativa());
                    entregaItemRequisicaoDto.setComplementoJustificativa(parecerDto.getComplementoJustificativa());
                }
            }
            tblItensRequisicao.addRowsByCollection(itensRequisicao,
                                                new String[] {"","descricaoItem","nomeFornecedor","numeroPedido","quantidadeEntregue","dataEntrega","descrSituacao"},
                                                null,
                                                "",
                                                new String[] {"gerarImg"},
                                                "editarItem",
                                                null);
        }

        requisicaoProdutoDto.setAcao(getAcao());
        HTMLForm form = document.getForm("form");
        form.clear();
        form.setValues(requisicaoProdutoDto);
    }


}
