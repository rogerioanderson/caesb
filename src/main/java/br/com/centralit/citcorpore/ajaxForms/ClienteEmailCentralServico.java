/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Objects;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.ClienteEmailCentralServicoDTO;
import br.com.centralit.citcorpore.bean.ClienteEmailCentralServicoMessagesDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.ClienteEmailCentralServicoService;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.centralit.citcorpore.util.Enumerados.TipoOrigemLeituraEmail;
import br.com.centralit.citcorpore.util.Util;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

/**
 * @author breno.guimaraes Controla as transa��es para manipula��o de Emails.
 *
 */
@SuppressWarnings({ "rawtypes", "unused" })
public class ClienteEmailCentralServico extends AjaxFormAction {
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		loadMails(document, request, response);
	}

        public void loadMails(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
            ClienteEmailCentralServicoDTO clienteEmailCentralServicoDTO = (ClienteEmailCentralServicoDTO) document.getBean();
            ClienteEmailCentralServicoService clienteEmailService = (ClienteEmailCentralServicoService) ServiceLocator.getInstance().getService(ClienteEmailCentralServicoService.class, null);
            StringBuilder html = new StringBuilder();
            StringBuilder htmlBody = new StringBuilder();
            ArrayList<ClienteEmailCentralServicoMessagesDTO> emailMessages;
    
            ClienteEmailCentralServicoDTO emailMessagesDto = clienteEmailService.getMessagesByLimitAndNoRequest(document, request, response);
    
            if (emailMessagesDto.isResultSucess()) {
                emailMessages = emailMessagesDto.getEmailMessages();
    
                if (emailMessages == null || emailMessages.isEmpty()) {
                    document.alert(UtilI18N.internacionaliza(request, "MSG04"));
                    document.executeScript("toggleClass('widgetEmails', 'hide');");
                } else {
                    html.append("<table id='tblEmails' class='dynamicTable table table-striped table-bordered table-condensed dataTable'>");
                    html.append("<tr>");
                    html.append("<th>" + UtilI18N.internacionaliza(request, "eventoItemConfiguracao.data") + "</th>");
                    html.append("<th>" + UtilI18N.internacionaliza(request, "citcorpore.comum.de") + "</th>");
                    html.append("<th>" + UtilI18N.internacionaliza(request, "requisitosla.assunto") + "</th>");
                    html.append("<th>" + UtilI18N.internacionaliza(request, "questionario.acoes") + "</th>");
                    html.append("</tr>");
    
                    for (ClienteEmailCentralServicoMessagesDTO message : emailMessages) {
                        html.append("<tr " + (!message.isSeen() ? "style='font-weight: bold;'" : "") + ">");
                        html.append("<td style='width: 20%;'>" + UtilDatas.convertDateToString(TipoDate.TIMESTAMP_DEFAULT, message.getMessageReceivedDate(), WebUtil.getLanguage(request)) + "</td>");
                        html.append("<td style='width: 25%;overflow: hidden;'>" + message.getMessageEmail() + "</td>");
                        html.append("<td style='width: 40%;overflow: hidden;'>" + message.getMessageSubject() + "</td>");
                        html.append("<td>");
    
                        String RO = request.getParameter("valorEditarFormEmail");
                        if ((RO == null) || (!RO.equalsIgnoreCase("RO"))) {
                            html.append("<button class='btn btn-mini btn-primary light' id='verificarEmails" + message.getMessageNumber() + "' name='verificarEmails" + message.getMessageNumber()
                                    + "' onclick='copiaEmail(" + message.getMessageNumber() + ");return false;'><i></i>" + UtilI18N.internacionaliza(request, "solicitacaoServico.copiarMsg")
                                    + "</button>");
                        }
    
                        html.append("</td>");
    
                        html.append("</tr>");
    
                        htmlBody.append("<input type='hidden' value='" + message.getMessageId() + "' name='emailMessageId" + message.getMessageNumber() + "' id='emailMessageId" + message
                                .getMessageNumber() + "'/>");
                    }
    
                    html.append("</table>");
                    html.append(htmlBody.toString());
    
                    document.getElementById("divEmails").setInnerHTML(html.toString());
                    document.executeScript("toggleClass('widgetEmails', 'show');");
                }
            } else {
                document.alert(emailMessagesDto.getResultMessage());
                document.executeScript("toggleClass('widgetEmails', 'hide');");
            }
    
            document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
        }

        /**
         * alterado por rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a> 
         * data altera��o rcs: 14/10/2015 
         * coment�rio altera��o rcs: tratamento de null pointer, em dados faltantes do remetente de email. Incidente 177922.
         * 
         * @param DocumentHTML
         *            document
         * @param HttpServletRequest
         *            request
         * @param HttpServletResponse
         *            response
         * @throws Exception
         */
        public void readMail(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
            ClienteEmailCentralServicoService clienteEmailService = (ClienteEmailCentralServicoService) ServiceLocator.getInstance().getService(ClienteEmailCentralServicoService.class, null);
            ClienteEmailCentralServicoDTO clienteEmailCentralServicoDTO = (ClienteEmailCentralServicoDTO) document.getBean();
            ArrayList<ClienteEmailCentralServicoMessagesDTO> emailMessages = null;
    
            if (clienteEmailCentralServicoDTO != null && clienteEmailCentralServicoDTO.getEmailMessageId() != null && clienteEmailCentralServicoDTO.getIdContrato() != null) {
                ClienteEmailCentralServicoDTO cecsDto = clienteEmailService.readMessage(document, request, response, clienteEmailCentralServicoDTO.getEmailMessageId());
    
                if (cecsDto.isResultSucess()) {
                    emailMessages = cecsDto.getEmailMessages();
    
                    if (CollectionUtils.isEmpty(emailMessages)) {
                        document.alert(UtilI18N.internacionaliza(request, "MSG04"));
                        document.executeScript("toggleClass('widgetEmails', 'hide');");
                    } else {
                        final EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
                        EmpregadoDTO empregadoDTO = null;
    
                        for (ClienteEmailCentralServicoMessagesDTO message : emailMessages) {
                            document.getElementById("messageId").setValue(clienteEmailCentralServicoDTO.getEmailMessageId());
                            document.executeScript("setDescricao('" + message.getMessageContent() + "');");
    
                            if (StringUtils.isBlank(message.getMessageEmail())) {
                                document.alert(UtilI18N.internacionaliza(request, "email.mensagemSemEnderecoRemetente"));
                            } else {
                                // Se for empregado ir� setar o restante das informa��es
                                empregadoDTO = empregadoService.listEmpregadoContrato(clienteEmailCentralServicoDTO.getIdContrato(), message.getMessageEmail());
    
                                if (!Objects.equals(empregadoDTO, null)){
                                    document.getElementById("idSolicitante").setValue(empregadoDTO.getIdEmpregado()!=null?empregadoDTO.getIdEmpregado().toString():"0");
                                    document.getElementById("solicitante").setValue(empregadoDTO.getNome()!=null?empregadoDTO.getNome():"");
                                    document.getElementById("idUnidade").setValue(empregadoDTO.getIdUnidade()!=null?Util.tratarAspasSimples(empregadoDTO.getIdUnidade().toString()):"");
                                    document.getElementById("nomecontato").setValue(empregadoDTO.getNome()!=null?empregadoDTO.getNome():"");
                                    document.getElementById("emailcontato").setValue(message.getMessageEmail());
                                    document.getElementById("telefonecontato").setValue(empregadoDTO.getTelefone()!=null?empregadoDTO.getTelefone():"");
                                }
    
                                if (TipoOrigemLeituraEmail.SOLICITACAO_SERVICO.getTipoOrigemLeituraEmail().equalsIgnoreCase(clienteEmailCentralServicoDTO.getEmailOrigem())) {
                                	document.getElementById("idOrigem").setValue("3");
                                    document.executeScript("renderizaInformacoesSolicitante();");
                                } else if (TipoOrigemLeituraEmail.PROBLEMA.getTipoOrigemLeituraEmail().equalsIgnoreCase(clienteEmailCentralServicoDTO.getEmailOrigem())) {
                                	document.getElementById("idOrigemAtendimento").setValue("3");
                                }
                            }
                        }
    
                        document.executeScript("fechaModalLeituraEmail();");
                    }
                } else {
                    document.alert(cecsDto.getResultMessage());
                    document.executeScript("toggleClass('widgetEmails', 'hide');");
                }
    
                document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
            }
        }

	public Class getBeanClass() {
		return ClienteEmailCentralServicoDTO.class;
	}
	
}
