/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.JustificacaoEventoHistoricoDTO;
import br.com.centralit.citcorpore.bean.JustificacaoFalhasDTO;
import br.com.centralit.citcorpore.negocio.JustificacaoFalhasService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.WebUtil;
@SuppressWarnings({"rawtypes","unchecked","unused"})
public class JustificacaoFalhas extends AjaxFormAction {
    
    JustificacaoFalhasService justificacaoService;
    
   
	@Override
    public Class getBeanClass() {
	return JustificacaoFalhasDTO.class;
    }

    @Override
    public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response)
	    throws Exception {
	
	
    }
    
	public void salvarJustificativa(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {	
	JustificacaoFalhasService justificacaoService = (JustificacaoFalhasService) ServiceLocator
						.getInstance().getService(JustificacaoFalhasService.class, null);
	
	ArrayList<JustificacaoFalhasDTO> listaItens = (ArrayList) WebUtil.deserializeCollectionFromRequest(JustificacaoFalhasDTO.class,"listaItensSerializado", request);
	
	
	if(listaItens != null){	    
	    for(JustificacaoFalhasDTO j : listaItens){
		justificacaoService.create(j);
	    }
	} /*else {
	}*/
	pesquisar(document, request, response);
    }
    
	public void pesquisar(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {	
	JustificacaoFalhasDTO justificacao = (JustificacaoFalhasDTO) document.getBean();
	JustificacaoFalhasService justificacaoService = (JustificacaoFalhasService) ServiceLocator
						.getInstance().getService(JustificacaoFalhasService.class, null);
	ArrayList<JustificacaoEventoHistoricoDTO> resultados = (ArrayList<JustificacaoEventoHistoricoDTO>) justificacaoService.findEventosComFalha(justificacao);
	
	String identificacaoAux = "";
/*	if(resultados != null){	    
        	for(JustificacaoEventoHistoricoDTO o : resultados){
        	    if(identificacaoAux == ""){
        		identificacaoAux = o.getIdentificacaoItemConfiguracao();
        		System.out.println("=================FIRST IN ACTION==================");
        		System.out.println("Identifica��o: " + o.getIdentificacaoItemConfiguracao());
        		
        	    }
        	    if(!identificacaoAux.equals(o.getIdentificacaoItemConfiguracao())){			
        		System.out.println("---------------------------------------------");
        		System.out.println("Identifica��o: " + o.getIdentificacaoItemConfiguracao());
        		identificacaoAux = o.getIdentificacaoItemConfiguracao();
        		
        	    }
        	    System.out.println("Descri��o do Falha: " + o.getDescricaoTentativa());
        	}
	}*/	    	
	justificacao.setListaItensSerializado(WebUtil.serializeObjects(resultados, WebUtil.getLanguage(request)));
	//System.out.println("Serializado: " + justificacao.getListaItensSerializado());
	
	document.getForm("form").setValues(justificacao);
	document.executeScript("$('#loading_overlay').hide();");
	document.executeScript("alimentaListaFalhas();");

    }
}
