/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.AtividadePeriodicaDTO;
import br.com.centralit.citcorpore.bean.ExecucaoAtividadePeriodicaDTO;
import br.com.centralit.citcorpore.bean.UploadDTO;
import br.com.centralit.citcorpore.negocio.AtividadePeriodicaService;
import br.com.centralit.citcorpore.negocio.ExecucaoAtividadePeriodicaService;
import br.com.centralit.citcorpore.negocio.MotivoSuspensaoAtividadeService;
import br.com.centralit.citged.bean.ControleGEDDTO;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class ExecucaoAtividadePeriodica extends AjaxFormAction {

	private ExecucaoAtividadePeriodicaService execucaoAtividadePeriodicaService;

	@Override
	public Class getBeanClass() {
		return ExecucaoAtividadePeriodicaDTO.class;
	}

	@Override
	public void load(final DocumentHTML arg0, final HttpServletRequest arg1, final HttpServletResponse arg2) throws Exception {
		arg1.getSession(true).setAttribute("colUploadsGEDExclusaoDefinida", null);
	}

	/**
	 * incidente-186200 - ExecucaoAtividadePeriodica.java_(#save).
	 *
	 * @since 24/02/2016
	 * @author ibimon.morais
	 */
	public void save(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final ExecucaoAtividadePeriodicaDTO execucaoAtividadePeriodicaDTO = (ExecucaoAtividadePeriodicaDTO) document.getBean();

		if (execucaoAtividadePeriodicaDTO.getSituacao() == null) {
			document.alert("Informe a situa��o");
			return;
		}
		if (execucaoAtividadePeriodicaDTO.getSituacao().equals("S") && execucaoAtividadePeriodicaDTO.getIdMotivoSuspensao() == null) {
			document.alert("Informe o motivo da suspens�o");
			return;
		}
		definirValoresExecucaoAtividade(request, execucaoAtividadePeriodicaDTO);
		// Atualizar formulario
		document.executeScript("refreshEvents()");
		document.alert(UtilI18N.internacionaliza(request, "MSG05"));
	}

	/**
	 * incidente-186200 - ExecucaoAtividadePeriodica.java_(#definirValoresExecucaoAtividade).
	 *
	 * @since 24/02/2016
	 * @author ibimon.morais
	 */
	private void definirValoresExecucaoAtividade(final HttpServletRequest request, final ExecucaoAtividadePeriodicaDTO execucaoAtividadePeriodicaDTO) throws LogicException,
			ServiceException {
		definirDataHoraDeRegistro(execucaoAtividadePeriodicaDTO);
		definirColunaUploadsGED(request, execucaoAtividadePeriodicaDTO);
		executarAcaoAtividadePeriodica(execucaoAtividadePeriodicaDTO);
		request.getSession(true).setAttribute("colUploadsGEDExclusaoDefinida", null);
	}

	/**
	 * incidente-186200 - ExecucaoAtividadePeriodica.java_(#definirDataHoraDeRegistro).
	 *
	 * @since 24/02/2016
	 * @author ibimon.morais
	 */
	private void definirDataHoraDeRegistro(final ExecucaoAtividadePeriodicaDTO execucaoAtividadePeriodicaDTO) {
		execucaoAtividadePeriodicaDTO.setDataRegistro(UtilDatas.getDataAtual());
		execucaoAtividadePeriodicaDTO.setHoraRegistro(UtilDatas.formatHoraFormatadaStr(UtilDatas.getHoraAtual()));
	}

	/**
	 * incidente-186200 - ExecucaoAtividadePeriodica.java_(#executarAcaoAtividadePeriodica).
	 *
	 * @since 24/02/2016
	 * @author ibimon.morais
	 */
	private void executarAcaoAtividadePeriodica(final ExecucaoAtividadePeriodicaDTO execucaoAtividadePeriodicaDTO) throws LogicException, ServiceException {
		if (execucaoAtividadePeriodicaDTO.getIdExecucaoAtividadePeriodica() == null || execucaoAtividadePeriodicaDTO.getIdExecucaoAtividadePeriodica().intValue() == 0) {
			this.getExecucaoAtividadePeriodicaService().create(execucaoAtividadePeriodicaDTO);
		} else {
			this.getExecucaoAtividadePeriodicaService().update(execucaoAtividadePeriodicaDTO);
		}
	}

	/**
	 * 
	 * incidente-186200 - ExecucaoAtividadePeriodica.java_(#definirColunaUploadsGED).
	 *
	 * @since 24/02/2016
	 * @author ibimon.morais
	 */
	private void definirColunaUploadsGED(final HttpServletRequest request, final ExecucaoAtividadePeriodicaDTO execucaoAtividadePeriodicaDTO) {
		Collection colUploadsGED = (Collection) request.getSession(true).getAttribute("colUploadsGED");
		Collection colUploadsGEDExclusaoDefinida = (Collection) request.getSession(true).getAttribute("colUploadsGEDExclusaoDefinida");
		execucaoAtividadePeriodicaDTO.setColArquivosUploadExcluir(colUploadsGEDExclusaoDefinida);
		execucaoAtividadePeriodicaDTO.setColArquivosUpload(colUploadsGED);
	}
	
	public void restore(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		ExecucaoAtividadePeriodicaDTO execucaoAtividadePeriodicaDTO = (ExecucaoAtividadePeriodicaDTO) document.getBean();

		request.getSession(true).setAttribute("colUploadsGED", null);
		request.getSession(true).setAttribute("colUploadsGEDExclusaoDefinida", null);
		document.executeScript("uploadAnexos.clear()");
		document.executeScript("uploadAnexos.refresh()");

		if (execucaoAtividadePeriodicaDTO.getIdExecucaoAtividadePeriodica() == null || execucaoAtividadePeriodicaDTO.getIdExecucaoAtividadePeriodica().intValue() == 0) {
			return;
		}

		execucaoAtividadePeriodicaDTO = (ExecucaoAtividadePeriodicaDTO) this.getExecucaoAtividadePeriodicaService().restore(execucaoAtividadePeriodicaDTO);

		final Collection colUploadsGED = new ArrayList();
		if (execucaoAtividadePeriodicaDTO.getColArquivosUpload() != null) {
			for (final Iterator it = execucaoAtividadePeriodicaDTO.getColArquivosUpload().iterator(); it.hasNext();) {
				final ControleGEDDTO controleGEDDTO = (ControleGEDDTO) it.next();

				final UploadDTO uploadDTO = new UploadDTO();
				uploadDTO.setDescricao(controleGEDDTO.getDescricaoArquivo());
				uploadDTO.setNameFile(controleGEDDTO.getNomeArquivo());
				uploadDTO.setSituacao("Arquivado");
				uploadDTO.setTemporario("N");
				uploadDTO.setPath("ID=" + controleGEDDTO.getIdControleGED());

				colUploadsGED.add(uploadDTO);
			}
		}
		request.getSession(true).setAttribute("colUploadsGED", colUploadsGED);

		final HTMLForm form = document.getForm("form");
		form.setValues(execucaoAtividadePeriodicaDTO);
		document.executeScript("configuraMotivoSuspensao('" + execucaoAtividadePeriodicaDTO.getSituacao() + "');");
		document.executeScript("document.getElementById('tabTela').tabber.tabShow(0);");
		document.executeScript("uploadAnexos.refresh()");
	}

	public void visualizarOrientacoes(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final ExecucaoAtividadePeriodicaDTO execucaoAtividadePeriodicaDTO = (ExecucaoAtividadePeriodicaDTO) document.getBean();
		final AtividadePeriodicaService atividadePeriodicaService = (AtividadePeriodicaService) ServiceLocator.getInstance().getService(AtividadePeriodicaService.class, null);

		AtividadePeriodicaDTO atvDto = new AtividadePeriodicaDTO();
		atvDto.setIdAtividadePeriodica(execucaoAtividadePeriodicaDTO.getIdAtividadePeriodica());
		atvDto = (AtividadePeriodicaDTO) atividadePeriodicaService.restore(atvDto);

		if (atvDto != null) {
			String str = atvDto.getOrientacaoTecnica();
			if (str == null) {
				str = "";
			}
			str = str.replaceAll("'", "");
			str = str.replaceAll("\n", "<br>");
			document.getElementById("divOrientacao").setInnerHTML(str);
			document.executeScript("$('#POPUP_ORIENTACAO').dialog('open')");
		}
	}

	public void carregarComboMotivo(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		document.getSelectById("idMotivoSuspensao").removeAllOptions();

		final HTMLSelect idMotivoSuspensao = document.getSelectById("idMotivoSuspensao");
		idMotivoSuspensao.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));

		final MotivoSuspensaoAtividadeService motivoSuspensaoService = (MotivoSuspensaoAtividadeService) ServiceLocator.getInstance().getService(
				MotivoSuspensaoAtividadeService.class, null);

		final Collection colMotivos = motivoSuspensaoService.listarMotivosSuspensaoAtividadeAtivos();
		idMotivoSuspensao.addOptions(colMotivos, "idMotivo", "descricao", "");
	}

	public ExecucaoAtividadePeriodicaService getExecucaoAtividadePeriodicaService() throws ServiceException {
		if (this.execucaoAtividadePeriodicaService == null) {
			this.execucaoAtividadePeriodicaService = (ExecucaoAtividadePeriodicaService) ServiceLocator.getInstance().getService(ExecucaoAtividadePeriodicaService.class, null);
		}
		return this.execucaoAtividadePeriodicaService;
	}

	public void setExecucaoAtividadePeriodicaService(final ExecucaoAtividadePeriodicaService execucaoAtividadePeriodicaService) {
		this.execucaoAtividadePeriodicaService = execucaoAtividadePeriodicaService;
	}

}
