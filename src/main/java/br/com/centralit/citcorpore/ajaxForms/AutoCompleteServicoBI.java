/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.AutoCompleteDTO;
import br.com.centralit.citcorpore.bean.ServicoBIDTO;
import br.com.centralit.citcorpore.negocio.ServicoBIService;
import br.com.citframework.service.ServiceLocator;

import com.google.gson.Gson;

@SuppressWarnings("rawtypes")
public class AutoCompleteServicoBI extends AjaxFormAction {

	public Class getBeanClass() {
		return ServicoBIDTO.class;
	}

	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String consulta = request.getParameter("query");

		String idConexaoBIStr = request.getParameter("idConexaoBI");
		Integer idConexaoBI = null;
		if (idConexaoBIStr != null && !idConexaoBIStr.equals("-1")) {
			idConexaoBI = Integer.parseInt(idConexaoBIStr);
		}

		Collection<ServicoBIDTO> listServicoBIDTO = new ArrayList<ServicoBIDTO>();

		if (idConexaoBI != null) {
			ServicoBIService servicoBIService = (ServicoBIService) ServiceLocator.getInstance().getService(ServicoBIService.class, null);
			listServicoBIDTO = servicoBIService.findByNomeEconexaoBI(consulta, idConexaoBI);
		}

		AutoCompleteDTO autoCompleteDTO = new AutoCompleteDTO();
		Gson gson = new Gson();

		List<String> listNome = new ArrayList<String>();
		List<Integer> listIdServico = new ArrayList<Integer>();

		if (listServicoBIDTO != null) {
			for (ServicoBIDTO servicoBIDTO : listServicoBIDTO) {
				if (servicoBIDTO.getIdServico() != null) {
					listNome.add(servicoBIDTO.getNomeServico());
					listIdServico.add(servicoBIDTO.getIdServico());
				}
			}
		}
		autoCompleteDTO.setQuery(consulta);
		autoCompleteDTO.setSuggestions(listNome);
		autoCompleteDTO.setData(listIdServico);

		String json = "";

		if (request.getParameter("colection") != null) {
			json = gson.toJson(listServicoBIDTO);
		} else {
			json = gson.toJson(autoCompleteDTO);
		}

		request.setAttribute("json_response", json);
	}
}
