/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.SistemaOperacionalDTO;
import br.com.centralit.citcorpore.negocio.SistemaOperacionalService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilI18N;

/**
 * @author ygor.magalhaes
 *
 */
@SuppressWarnings("rawtypes")
public class SistemaOperacional extends AjaxFormAction {

    @Override
    public void load(DocumentHTML arg0, HttpServletRequest arg1, HttpServletResponse arg2) throws Exception {

    }

    public void SistemaOperacional_onsave(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		SistemaOperacionalDTO soDTO = (SistemaOperacionalDTO) document.getBean();
		SistemaOperacionalService soService = (SistemaOperacionalService) ServiceLocator.getInstance().getService(SistemaOperacionalService.class, null);
		
		// Verificando a exist�ncia do objeto de servi�o.
		if (soService != null) {
			// Inserindo o SO.
			if (soDTO.getId() == null || soDTO.getId().intValue() == 0) {
				Collection soJaCadastrado = soService.find(soDTO);
				
				// Verificando se o SO j� foi cadastrado.
				if (soJaCadastrado != null && !soJaCadastrado.isEmpty() ) {
					// Se verdadeiro, ent�o alerta o usu�rio e pede para tentar outro SO.
					document.alert(UtilI18N.internacionaliza(request, "MSE01") );
				} else { // Inserindo o SO.
					soService.create(soDTO);
				    document.alert(UtilI18N.internacionaliza(request, "MSG05") );
				}			    
			} else { // Atualizando o SO
			    soService.update(soDTO);
			    document.alert(UtilI18N.internacionaliza(request, "MSG06") );
			}
			
			HTMLForm form = document.getForm("form");
			form.clear();
		}
    }

    public void SistemaOperacional_onrestore(DocumentHTML document,
	    javax.servlet.http.HttpServletRequest request, HttpServletResponse response)
	    throws Exception {
	SistemaOperacionalDTO sistema = (SistemaOperacionalDTO) document.getBean();
	SistemaOperacionalService sistemaService = (SistemaOperacionalService) ServiceLocator
		.getInstance().getService(SistemaOperacionalService.class, null);

	sistema = (SistemaOperacionalDTO) sistemaService.restore(sistema);

	HTMLForm form = document.getForm("form");
	form.clear();
	form.setValues(sistema);
    }

    
    /**
     * Dele��o l�gica.
     * @param document
     * @param request
     * @param response
     * @throws Exception
     */
    public void updateDataFim(DocumentHTML document, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {
	SistemaOperacionalDTO sistema = (SistemaOperacionalDTO) document.getBean();
	SistemaOperacionalService tipoUnidadeService = (SistemaOperacionalService) ServiceLocator.getInstance()
		.getService(SistemaOperacionalService.class, null);

	if (sistema.getId() != null && sistema.getId() != 0) {
	   // sistema.set (UtilDatas.getDataAtual());
	    tipoUnidadeService.update(sistema);

	    HTMLForm form = document.getForm("form");
	    form.clear();
	    document.alert(UtilI18N.internacionaliza(request, "MSG07"));
	}
    }
    
    
    
	public Class getBeanClass() {
	return SistemaOperacionalDTO.class;
    }
}
