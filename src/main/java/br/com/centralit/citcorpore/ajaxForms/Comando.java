/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.ComandoDTO;
import br.com.centralit.citcorpore.negocio.ComandoService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilI18N;

/**
 * @author ygor.magalhaes
 *
 */
@SuppressWarnings("rawtypes")
public class Comando extends AjaxFormAction {

    public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

    }


	public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	ComandoDTO comandoDTO = (ComandoDTO) document.getBean();
    	ComandoService comandoService = (ComandoService) ServiceLocator.getInstance().getService(ComandoService.class, null);

    	Collection comandoJaCadastrado = comandoService.find(comandoDTO);
    	if (comandoJaCadastrado != null && comandoJaCadastrado.size() > 0 ) {
			document.alert(UtilI18N.internacionaliza(request, "MSE01") );
			return;
		}

    		if (comandoDTO.getId() == null || comandoDTO.getId().intValue() == 0) {

    			if (comandoJaCadastrado != null && !comandoJaCadastrado.isEmpty() ) {
    				document.alert(UtilI18N.internacionaliza(request, "MSE01") );
    			} else {
    				comandoService.create(comandoDTO);
            		document.alert(UtilI18N.internacionaliza(request, "MSG05") );
    			}
        	} else {

        		comandoService.update(comandoDTO);
        		document.alert(UtilI18N.internacionaliza(request, "MSG06") );
        	}

        	HTMLForm form = document.getForm("form");
        	form.clear();

    }

    public void restore(DocumentHTML document, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {
	ComandoDTO comando = (ComandoDTO) document.getBean();
	ComandoService comandoService = (ComandoService) ServiceLocator.getInstance().getService(
		ComandoService.class, null);

	comando = (ComandoDTO) comandoService.restore(comando);

	HTMLForm form = document.getForm("form");
	form.clear();
	form.setValues(comando);
    }

    public Class getBeanClass() {
	return ComandoDTO.class;
    }
}
