/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.BIDashBoardDTO;
import br.com.centralit.citcorpore.bean.BIItemDashBoardDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.BIDashBoardService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.dto.Usuario;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilI18N;

public class DashBoardBuilder extends AjaxFormAction {

	@Override
	public void load(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		UsuarioDTO usuarioDto = WebUtil.getUsuario(request);
		if (usuarioDto == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}		
	}
	
	public void saveDash(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Usuario usuarioDto = WebUtil.getUsuarioSistema(request);
		if (usuarioDto == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}
		BIDashBoardService biDashBoardService = (BIDashBoardService) ServiceLocator.getInstance().getService(BIDashBoardService.class, usuarioDto);
		BIDashBoardDTO biDashBoardDTO = (BIDashBoardDTO)document.getBean();
		Collection colItensDash = br.com.citframework.util.WebUtil.deserializeCollectionFromRequest(BIItemDashBoardDTO.class, "colItensSerialize", request);
		if (colItensDash == null || colItensDash.size() == 0){
			document.alert(UtilI18N.internacionaliza(request, "dashboard.selecione") );
			return;
		}
		for (Iterator it = colItensDash.iterator(); it.hasNext();){
			BIItemDashBoardDTO biItemDashBoardDTO = (BIItemDashBoardDTO)it.next();
			if (biItemDashBoardDTO.getIdConsulta() == null){
				document.alert(UtilI18N.internacionaliza(request, "dashboard.existeItemSemDados") );
				return;				
			}
			if (biItemDashBoardDTO.getItemTop() == null){
				biItemDashBoardDTO.setItemTop(0);
			}
			if (biItemDashBoardDTO.getItemLeft() == null){
				biItemDashBoardDTO.setItemLeft(0);
			}
			if (biItemDashBoardDTO.getItemWidth() == null){
				biItemDashBoardDTO.setItemWidth(0);
			}
			if (biItemDashBoardDTO.getItemHeight() == null){
				biItemDashBoardDTO.setItemHeight(0);
			}	
			if (biItemDashBoardDTO.getPosicao() == null){
				biItemDashBoardDTO.setPosicao(0);
			}			
		}
		biDashBoardDTO.setColItens(colItensDash);
		if (biDashBoardDTO.getIdDashBoard() == null || biDashBoardDTO.getIdDashBoard().intValue() == 0){
			biDashBoardDTO = (BIDashBoardDTO) biDashBoardService.create(biDashBoardDTO);
			document.getElementById("idDashBoard").setValue("" + biDashBoardDTO.getIdDashBoard());
			document.alert(UtilI18N.internacionaliza(request, "MSG05") );
		}else{
			biDashBoardService.update(biDashBoardDTO);
			document.alert(UtilI18N.internacionaliza(request, "MSG06") );
		}
		document.executeScript("$( \"#POPUP_SALVAR\" ).dialog( \"close\" );");
	}

	@Override
	public Class getBeanClass() {
		return BIDashBoardDTO.class;
	}

}
