/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * CentralIT - CITSmart
 */
package br.com.centralit.citcorpore.ajaxForms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.SituacaoServicoDTO;
import br.com.centralit.citcorpore.negocio.SituacaoServicoService;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.Util;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilI18N;

@SuppressWarnings("rawtypes")
public class SituacaoServico extends AjaxFormAction {

	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

	}

	public void SituacaoServico_onsave(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		SituacaoServicoDTO situacao = (SituacaoServicoDTO) document.getBean();
		SituacaoServicoService situacaoService = (SituacaoServicoService) ServiceLocator.getInstance().getService(SituacaoServicoService.class, null);

		if (situacao.getIdSituacaoServico() == null || situacao.getIdSituacaoServico().intValue() == 0) {
			if (!situacaoService.jaExisteSituacaoComMesmoNome(situacao.getNomeSituacaoServico())) {
				situacao.setIdEmpresa(WebUtil.getIdEmpresa(request));
				situacaoService.create(situacao);
				document.alert(UtilI18N.internacionaliza(request, "MSG05") );
			} else {
				document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.registroJaCadastrado") );
			}
		} else {
			situacaoService.update(situacao);
			document.alert(UtilI18N.internacionaliza(request, "MSG06") );
		}
		HTMLForm form = document.getForm("form");
		form.clear();
		
		document.executeScript("limpar_LOOKUP_SITUACAOSERVICO()");
	}

	public void SituacaoServico_onrestore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		SituacaoServicoDTO situacao = (SituacaoServicoDTO) document.getBean();
		SituacaoServicoService situacaoService = (SituacaoServicoService) ServiceLocator.getInstance().getService(SituacaoServicoService.class, null);
		situacao = (SituacaoServicoDTO) situacaoService.restore(situacao);
		HTMLForm form = document.getForm("form");
		form.clear();
		form.setValues(situacao);
	}

	public void delete(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		SituacaoServicoDTO situacao = (SituacaoServicoDTO) document.getBean();
		SituacaoServicoService situacaoService = (SituacaoServicoService) ServiceLocator.getInstance().getService(SituacaoServicoService.class, null);
		situacao.setDataFim(Util.getSqlDataAtual());

		boolean existeServicoAssociado = situacaoService.existeServicoAssociado(situacao.getIdSituacaoServico());
		if(existeServicoAssociado){
			document.alert(UtilI18N.internacionaliza(request,"centroResultado.naoPodeSerExcluido.possuiRelacionamentos"));
			HTMLForm form = document.getForm("form");
			form.clear();
			return;
		}
		
		situacaoService.update(situacao);
		document.alert(UtilI18N.internacionaliza(request, "MSG07") );
		CITCorporeUtil.limparFormulario(document);
		
		document.executeScript("limpar_LOOKUP_SITUACAOSERVICO()");
	}

	public Class getBeanClass() {
		return SituacaoServicoDTO.class;
	}
}
