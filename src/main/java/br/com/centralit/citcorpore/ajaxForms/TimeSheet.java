/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLTable;
import br.com.centralit.citajax.html.HTMLTextBox;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.TimeSheetDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.TimeSheetService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;

public class TimeSheet extends AjaxFormAction {

	public Class getBeanClass() {
		return TimeSheetDTO.class;
	}

	public void load(DocumentHTML arg0, HttpServletRequest arg1,
			HttpServletResponse arg2) throws Exception {
	}

	public void registrarTimeSheet(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		TimeSheetService timeSheetService = (TimeSheetService) ServiceLocator.getInstance().getService(TimeSheetService.class, null);
		TimeSheetDTO timeSheetBean = (TimeSheetDTO)document.getBean();
		
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		timeSheetBean.setIdEmpregado(new Integer(usuario.getIdUsuario()));
		
		EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
		EmpregadoDTO empregadoBean = new EmpregadoDTO();
		empregadoBean.setIdEmpregado(timeSheetBean.getIdEmpregado());
		empregadoBean = (EmpregadoDTO) empregadoService.restore(empregadoBean);
		
		if (empregadoBean == null){
			document.alert("ERRO: N�o foi possivel recuperar dados do empregado logado!");
			return;
		}
		
		timeSheetBean.setCustoPorHora(empregadoBean.getCustoPorHora());
		
		timeSheetService.create(timeSheetBean);
		
		document.alert("TimeSheet registrado com sucesso!");
		
		HTMLTextBox dataTimeSheet = document.getTextBoxById("dataTimeSheet");
		HTMLTextBox qtdeHorasTimeSheet = document.getTextBoxById("qtdeHorasTimeSheet");
		HTMLTextBox detalhamentoTimeSheet = document.getTextBoxById("detalhamentoTimeSheet");
		
		dataTimeSheet.setValue("");
		qtdeHorasTimeSheet.setValue("");
		detalhamentoTimeSheet.setValue("");
	}
	
	public void consultarTimeSheet(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		TimeSheetService timeSheetService = (TimeSheetService) ServiceLocator.getInstance().getService(TimeSheetService.class, null);
		TimeSheetDTO timeSheetBean = (TimeSheetDTO)document.getBean();
		
		Collection col = timeSheetService.findByDemanda(timeSheetBean.getIdDemanda());
		
		HTMLTable tabelaConsultaTimeSheet = document.getTableById("tabelaConsultaTimeSheet");
		
		tabelaConsultaTimeSheet.deleteAllRows();
		tabelaConsultaTimeSheet.addRowsByCollection(col, 
				new String[] {"dataStr", "qtdeHorasStr", "nomeEmpregado", "detalhamento"}, 
				null, 
				"J� existe registrado este registro na tabela", 
				null, 
				null, 
				null);
		tabelaConsultaTimeSheet.applyStyleClassInAllCells("tamanho10");		
	}	
}
