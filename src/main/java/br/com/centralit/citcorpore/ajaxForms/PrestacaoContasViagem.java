/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLTable;
import br.com.centralit.citcorpore.bean.CentroResultadoDTO;
import br.com.centralit.citcorpore.bean.CidadesDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.IntegranteViagemDTO;
import br.com.centralit.citcorpore.bean.ItemPrestacaoContasViagemDTO;
import br.com.centralit.citcorpore.bean.JustificativaSolicitacaoDTO;
import br.com.centralit.citcorpore.bean.PrestacaoContasViagemDTO;
import br.com.centralit.citcorpore.bean.RequisicaoViagemDTO;
import br.com.centralit.citcorpore.bean.RoteiroViagemDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.CentroResultadoService;
import br.com.centralit.citcorpore.negocio.CidadesService;
import br.com.centralit.citcorpore.negocio.DespesaViagemService;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.IntegranteViagemService;
import br.com.centralit.citcorpore.negocio.ItemPrestacaoContasViagemService;
import br.com.centralit.citcorpore.negocio.JustificativaSolicitacaoService;
import br.com.centralit.citcorpore.negocio.PrestacaoContasViagemService;
import br.com.centralit.citcorpore.negocio.RequisicaoViagemService;
import br.com.centralit.citcorpore.negocio.RoteiroViagemService;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.LogoRel;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

@SuppressWarnings({"rawtypes", "unchecked" })
public class PrestacaoContasViagem extends AjaxFormAction {

	public Class getBeanClass() {
		return PrestacaoContasViagemDTO.class;
	}

	/**
	 * Inicializa os dados ao carregar a tela.
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author renato.jesus
	 */
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		RequisicaoViagemService requisicaoViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, null);
		IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, null);
		PrestacaoContasViagemService prestacaoContasViagemService = (PrestacaoContasViagemService) ServiceLocator.getInstance().getService(PrestacaoContasViagemService.class, null);
		DespesaViagemService despesaViagemService = (DespesaViagemService) ServiceLocator.getInstance().getService(DespesaViagemService.class, null);

		PrestacaoContasViagemDTO prestacaoContasViagemDTO = (PrestacaoContasViagemDTO) document.getBean();
		RequisicaoViagemDTO requisicaoViagemDTO = new RequisicaoViagemDTO();

		requisicaoViagemDTO.setIdSolicitacaoServico(prestacaoContasViagemDTO.getIdSolicitacaoServico());
		requisicaoViagemDTO = (RequisicaoViagemDTO) requisicaoViagemService.restore(requisicaoViagemDTO);

		if(requisicaoViagemDTO != null){
			requisicaoViagemDTO.setIdTarefa(prestacaoContasViagemDTO.getIdTarefa());

			prestacaoContasViagemDTO.setIntegranteViagemDto(integranteViagemService.getIntegranteByIdSolicitacaoAndTarefa(prestacaoContasViagemDTO.getIdSolicitacaoServico(), prestacaoContasViagemDTO.getIdTarefa()));
		}

		DecimalFormat decimal = (DecimalFormat) NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		decimal.applyPattern("#,##0.00");
		Double totalPrestacaoContas = 0.0;

		if(prestacaoContasViagemDTO.getIntegranteViagemDto() != null) {
			totalPrestacaoContas = despesaViagemService.getTotalDespesaViagemPrestacaoContas(prestacaoContasViagemDTO.getIdSolicitacaoServico(), prestacaoContasViagemDTO.getIntegranteViagemDto().getIdEmpregado());
		}

		prestacaoContasViagemDTO.setValorDiferenca(totalPrestacaoContas);
		prestacaoContasViagemDTO.setTotalLancamentos(0.0);
		prestacaoContasViagemDTO.setTotalPrestacaoContas(totalPrestacaoContas);

		if(prestacaoContasViagemDTO.getIntegranteViagemDto() != null){
			RoteiroViagemDTO roteiroViagemDTO = new RoteiroViagemDTO();
			CidadesDTO origem = null;
			CidadesDTO destino = null;

			RoteiroViagemService roteiroViagemService = (RoteiroViagemService) ServiceLocator.getInstance().getService(RoteiroViagemService.class, null);
			CidadesService cidadesService = (CidadesService) ServiceLocator.getInstance().getService(CidadesService.class, null);

			StringBuilder textoNomeIntegranteViagem = new StringBuilder();

			roteiroViagemDTO = roteiroViagemService.findByIdIntegrante(prestacaoContasViagemDTO.getIntegranteViagemDto().getIdIntegranteViagem());
			origem = (CidadesDTO) ((List) cidadesService.findNomeByIdCidade(roteiroViagemDTO.getOrigem())).get(0);
			destino = (CidadesDTO) ((List) cidadesService.findNomeByIdCidade(roteiroViagemDTO.getDestino())).get(0);

			// Verifica se o funcionario tem um responsavel pela prestacao de contas
			if(!prestacaoContasViagemDTO.getIntegranteViagemDto().getIdRespPrestacaoContas().equals(prestacaoContasViagemDTO.getIntegranteViagemDto().getIdEmpregado())) {
				EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);

				EmpregadoDTO empregadoDTO = new EmpregadoDTO();
				empregadoDTO.setIdEmpregado(prestacaoContasViagemDTO.getIntegranteViagemDto().getIdRespPrestacaoContas());
				empregadoDTO = (EmpregadoDTO) empregadoService.restore(empregadoDTO);

				textoNomeIntegranteViagem.append(empregadoDTO.getNome() + " em nome de " + prestacaoContasViagemDTO.getIntegranteViagemDto().getNome());
			} else {
				textoNomeIntegranteViagem.append(prestacaoContasViagemDTO.getIntegranteViagemDto().getNome());
			}

			textoNomeIntegranteViagem.append(" - ida " + UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT, roteiroViagemDTO.getIda(), UtilI18N.getLocale(request)));
			textoNomeIntegranteViagem.append(" - volta " + UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT, roteiroViagemDTO.getVolta(), UtilI18N.getLocale(request)));
			textoNomeIntegranteViagem.append(" - " + origem.getNomeCidade() + "/" + origem.getNomeUf());
			textoNomeIntegranteViagem.append(" - " + destino.getNomeCidade() + "/" + destino.getNomeUf());

			document.getElementById("nomeIntegranteViagem").setInnerHTML(textoNomeIntegranteViagem.toString());

			prestacaoContasViagemDTO.setIdEmpregado(prestacaoContasViagemDTO.getIntegranteViagemDto().getIdEmpregado());
			prestacaoContasViagemDTO.setIdResponsavel(prestacaoContasViagemDTO.getIntegranteViagemDto().getIdRespPrestacaoContas());
			prestacaoContasViagemDTO.setIdIntegrante(prestacaoContasViagemDTO.getIntegranteViagemDto().getIdIntegranteViagem());

			Integer idPrestacaoContasViagem = prestacaoContasViagemService.recuperaIdPrestacaoSeExistirByIdResponsavelPrestacaoContas(prestacaoContasViagemDTO.getIdSolicitacaoServico(), prestacaoContasViagemDTO.getIntegranteViagemDto().getIdEmpregado(), prestacaoContasViagemDTO.getIntegranteViagemDto().getIdRespPrestacaoContas());
			prestacaoContasViagemDTO.setIdPrestacaoContasViagem(idPrestacaoContasViagem);
		}

		if(prestacaoContasViagemDTO.getIdPrestacaoContasViagem() != null) {
			this.restore(document, request, response, prestacaoContasViagemDTO);
		} else {
			HTMLForm form = document.getForm("form");
			form.clear();
			form.setValues(prestacaoContasViagemDTO);
		}

//		document.getElementById("valorDiferencaAux").setValue(decimal.format(prestacaoContasViagemDTO.getValorDiferenca()));
//		document.getElementById("valorDiferenca").setValue(prestacaoContasViagemDTO.getValorDiferenca().toString());
	}

	/**
	 * Restaura os dados ao clicar em um registro.
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @param prestacaoContasViagemDto
	 * @throws Exception
	 * @author renato.jesus
	 */
	public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response, PrestacaoContasViagemDTO prestacaoContasViagemDTO) throws Exception {
		ItemPrestacaoContasViagemService itemPrestacaoContasViagemService = (ItemPrestacaoContasViagemService) ServiceLocator.getInstance().getService(ItemPrestacaoContasViagemService.class, null);

		Collection<ItemPrestacaoContasViagemDTO> colItemPrestacaoContasViagemDTO = itemPrestacaoContasViagemService.recuperaItensPrestacao(prestacaoContasViagemDTO);

		if(colItemPrestacaoContasViagemDTO != null && !colItemPrestacaoContasViagemDTO.isEmpty()) {
			Double totalLancamentos = 0.0;

			for(ItemPrestacaoContasViagemDTO itemPrestacaoContasViagemDTO : colItemPrestacaoContasViagemDTO) {
				totalLancamentos += itemPrestacaoContasViagemDTO.getValor();
			}

			prestacaoContasViagemDTO.setTotalLancamentos(totalLancamentos);
			prestacaoContasViagemDTO.setValorDiferenca(prestacaoContasViagemDTO.getTotalPrestacaoContas() - totalLancamentos);
			prestacaoContasViagemDTO.setIdIntegrante(prestacaoContasViagemDTO.getIntegranteViagemDto().getIdIntegranteViagem());

			HTMLTable tabelaItemPrestacaoContasViagem = document.getTableById("tabelaItemPrestacaoContasViagem");

			tabelaItemPrestacaoContasViagem.deleteAllRows();
			tabelaItemPrestacaoContasViagem.addRowsByCollection(
					colItemPrestacaoContasViagemDTO,
					new String[] {"numeroDocumento", "data", "nomeFornecedor", "valor", "descricao", ""},
					new String[] {"numeroDocumento"},
					UtilI18N.internacionaliza(request, "citcorpore.comum.registroJaAdicionado"),
					new String[] {"gerarButtonsTable"},
					null,
					null);
		}

		HTMLForm form = document.getForm("form");
		form.clear();
		form.setValues(prestacaoContasViagemDTO);
	}

	/**
	 * Inclui registro.
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author renato.jesus
	 */
	public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PrestacaoContasViagemDTO prestacaoContasViagemDto = (PrestacaoContasViagemDTO) document.getBean();
		PrestacaoContasViagemService prestacaoContasViagemService = (PrestacaoContasViagemService) ServiceLocator.getInstance().getService(PrestacaoContasViagemService.class, null);

		prestacaoContasViagemDto.setListaItemPrestacaoContasViagemDTO((ArrayList<ItemPrestacaoContasViagemDTO>) br.com.citframework.util.WebUtil.deserializeCollectionFromRequest(ItemPrestacaoContasViagemDTO.class, "listItens", request));

		if (prestacaoContasViagemDto.getIdPrestacaoContasViagem() == null || prestacaoContasViagemDto.getIdPrestacaoContasViagem().intValue() == 0) {
			prestacaoContasViagemService.create(prestacaoContasViagemDto);
			document.alert(UtilI18N.internacionaliza(request, "MSG05"));
		} else {
			prestacaoContasViagemService.update(prestacaoContasViagemDto);
			document.alert(UtilI18N.internacionaliza(request, "MSG06"));
		}

		HTMLForm form = document.getForm("form");
		form.clear();

		document.executeScript("deleteAllRows()");
	}

	/**
	 * Calcula e atualiza os valores dos campos "Total Presta��o de Contas", "Total Lan�amentos" e "Valor Diferen�a"
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author renato.jesus
	 */
	public void atualizarValores(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PrestacaoContasViagemDTO prestacaoContasViagemDto = (PrestacaoContasViagemDTO) document.getBean();

		DecimalFormat decimal = (DecimalFormat) NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));

		Double valorDiferenca = prestacaoContasViagemDto.getTotalPrestacaoContas() - prestacaoContasViagemDto.getTotalLancamentosAux();
		decimal.applyPattern("#,##0.00");
		document.getElementById("valorDiferenca").setValue(decimal.format(valorDiferenca));

		if(prestacaoContasViagemDto.getTotalLancamentosAux() > 0.99 || prestacaoContasViagemDto.getTotalLancamentosAux() == 0.0){
			document.getElementById("totalLancamentos").setValue(decimal.format(prestacaoContasViagemDto.getTotalLancamentosAux()));
		} else {
			decimal.applyPattern("#,#0.00");
			document.getElementById("totalLancamentos").setValue(decimal.format(prestacaoContasViagemDto.getTotalLancamentosAux()));
		}
		prestacaoContasViagemDto.setValor(null);

		document.executeScript("limparCamposFormulario();");
	}

	/**
	 * Adiciona o item a grid de itens de presta��o de contas e verifica se a nota fiscal foi emitida h� mais de 3 meses.
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void adicionarItem(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PrestacaoContasViagemDTO prestacaoContasViagemDto = (PrestacaoContasViagemDTO) document.getBean();
		RoteiroViagemService roteiroViagemService = (RoteiroViagemService) ServiceLocator.getInstance().getService(RoteiroViagemService.class, null);

		RoteiroViagemDTO roteiroViagemDTO = roteiroViagemService.findByIdIntegrante(prestacaoContasViagemDto.getIdIntegrante());

		if(roteiroViagemDTO != null && prestacaoContasViagemDto.getData() != null && !prestacaoContasViagemDto.getData().equals("")){
			Date dataViagem = UtilDatas.alteraData(roteiroViagemDTO.getIda(), -3, Calendar.MONTH ) ;
			Date dataPrestacao = UtilDatas.convertStringToDate(TipoDate.FORMAT_DATABASE , prestacaoContasViagemDto.getData().toString(), UtilI18N.getLocale(request));

			if(dataViagem.compareTo(dataPrestacao) > 0){
				document.alert("Nota fiscal emitida h� mais de 3 meses!");
				document.getElementById("data").setValue("");
				document.getElementById("data").setFocus();
				return;
			}
			
			// Verifica se a data da nota � maior que a data atual
			Date dataAtual = UtilDatas.getDataAtual();
			if(dataPrestacao.after(dataAtual)) {
				document.alert("A data da nota fiscal � maior que a data atual!");
				document.getElementById("data").setValue("");
				document.getElementById("data").setFocus();
				return;
			}
		}
		document.executeScript("adicionarItem()");
	}
	
	/**
	 * Remove um item de prestacao de cotnas 
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 * @author renato.jesus
	 */
	public void removeItemPrestacaoContasViagem(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		document.executeScript("JANELA_AGUARDE_MENU.show()");
		
		PrestacaoContasViagemDTO prestacaoContasViagemDto = (PrestacaoContasViagemDTO) document.getBean();
		ItemPrestacaoContasViagemService itemPrestacaoContasViagemService = (ItemPrestacaoContasViagemService) ServiceLocator.getInstance().getService(ItemPrestacaoContasViagemService.class, null);
		
		Integer idItemPrestacaoContasViagem = prestacaoContasViagemDto.getIdItemPrestacaoContasViagem();
		ItemPrestacaoContasViagemDTO itemPrestacaoContasViagemDto = new ItemPrestacaoContasViagemDTO();
		itemPrestacaoContasViagemDto.setIdItemPrestContasViagem(idItemPrestacaoContasViagem);
		
		itemPrestacaoContasViagemDto = itemPrestacaoContasViagemService.restore(itemPrestacaoContasViagemDto);
		
		itemPrestacaoContasViagemService.delete(itemPrestacaoContasViagemDto);
		
		document.getElementById("idItemPrestacaoContasViagem").setValue("");
		
		this.atualizarValores(document, request, response);
		
		document.executeScript("JANELA_AGUARDE_MENU.hide()");
	}
	
	public void gravarPrestacaoContasImprimirRelatorio(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		document.executeScript("JANELA_AGUARDE_MENU.show()");
		
		IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, WebUtil.getUsuarioSistema(request));
		
		PrestacaoContasViagemDTO prestacaoContasViagemDto = (PrestacaoContasViagemDTO) document.getBean();
		
		PrestacaoContasViagemService prestacaoContasViagemService = (PrestacaoContasViagemService) ServiceLocator.getInstance().getService(PrestacaoContasViagemService.class, null);
		prestacaoContasViagemDto = prestacaoContasViagemService.create(prestacaoContasViagemDto);
		
		if(prestacaoContasViagemDto.getIdPrestacaoContasViagem() != null) {
			document.getElementById("idPrestacaoContasViagem").setValue(prestacaoContasViagemDto.getIdPrestacaoContasViagem().toString());
			
			HTMLTable tabelaItemPrestacaoContasViagem = document.getTableById("tabelaItemPrestacaoContasViagem");

			tabelaItemPrestacaoContasViagem.deleteAllRows();
			tabelaItemPrestacaoContasViagem.addRowsByCollection(
					prestacaoContasViagemDto.getListaItemPrestacaoContasViagemDTO(),
					new String[] {"numeroDocumento", "data", "nomeFornecedor", "valor", "descricao", ""},
					new String[] {"numeroDocumento"},
					UtilI18N.internacionaliza(request, "citcorpore.comum.registroJaAdicionado"),
					new String[] {"gerarButtonsTable"},
					null,
					null);
		}
		
		IntegranteViagemDTO integranteViagemDTO = (IntegranteViagemDTO) integranteViagemService.restore(prestacaoContasViagemDto.getIntegranteViagemDto());
		
		this.montaRelatorio(document, request, response, integranteViagemDTO);
	}
	
	/**
	 * Metodo monta o relatorio de reembolso 
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 * @author thiago.borges
	 */
	private void montaRelatorio(DocumentHTML document, HttpServletRequest request, HttpServletResponse response, IntegranteViagemDTO integranteViagemDTO) throws ServiceException, Exception {
		
		IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, WebUtil.getUsuarioSistema(request));
		
		PrestacaoContasViagemDTO prestacaoContasViagemDTO = new PrestacaoContasViagemDTO();
		
        PrestacaoContasViagemService prestacaoContasViagemService = (PrestacaoContasViagemService) ServiceLocator.getInstance().getService(PrestacaoContasViagemService.class, WebUtil.getUsuarioSistema(request));
        
        EmpregadoDTO empregadoDTO = new EmpregadoDTO();
		
        EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, WebUtil.getUsuarioSistema(request));
        
        RequisicaoViagemDTO requisicaoViagemDTO = new RequisicaoViagemDTO();
		
        RequisicaoViagemService requisicaoViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, WebUtil.getUsuarioSistema(request));
        
        CentroResultadoDTO centroResultadoDTO = new CentroResultadoDTO();
		
        CentroResultadoService centroResultadoService = (CentroResultadoService) ServiceLocator.getInstance().getService(CentroResultadoService.class, WebUtil.getUsuarioSistema(request));
        
        DespesaViagemService despesaViagemService = (DespesaViagemService) ServiceLocator.getInstance().getService(DespesaViagemService.class, WebUtil.getUsuarioSistema(request));
        
        JustificativaSolicitacaoDTO justificativaSolicitacaoDTO = new JustificativaSolicitacaoDTO();
		
        JustificativaSolicitacaoService justificativaSolicitacaoService = (JustificativaSolicitacaoService) ServiceLocator.getInstance().getService(JustificativaSolicitacaoService.class, WebUtil.getUsuarioSistema(request));
        
        Collection<ItemPrestacaoContasViagemDTO> itensPrestacao = new ArrayList<>();
		
        ItemPrestacaoContasViagemService itemPrestacaoContasViagemService = (ItemPrestacaoContasViagemService) ServiceLocator.getInstance().getService(ItemPrestacaoContasViagemService.class, WebUtil.getUsuarioSistema(request));
        
        prestacaoContasViagemDTO.setIdPrestacaoContasViagem(integranteViagemDTO.getIdPrestacaoContas());
        
        integranteViagemDTO = integranteViagemService.restore(integranteViagemDTO);
        
        empregadoDTO = empregadoService.restoreByIdEmpregado(integranteViagemDTO.getIdRespPrestacaoContas());
        
        integranteViagemDTO.setNomeEmpregado(empregadoDTO.getNome());

        Integer idPrestacaoContasViagem = prestacaoContasViagemService.recuperaIdPrestacaoSeExistirByIdResponsavelPrestacaoContas(integranteViagemDTO.getIdSolicitacaoServico(), integranteViagemDTO.getIdEmpregado(), integranteViagemDTO.getIdRespPrestacaoContas());
        prestacaoContasViagemDTO.setIdPrestacaoContasViagem(idPrestacaoContasViagem);
        prestacaoContasViagemDTO = prestacaoContasViagemService.restore(prestacaoContasViagemDTO);
        
        NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		DecimalFormat decimal = (DecimalFormat) nf;
		decimal.applyPattern("#,##0.00");
        
        Double valorAdiantamento = 0d;
        
        if(prestacaoContasViagemDTO != null){
        	
        	itensPrestacao = itemPrestacaoContasViagemService.recuperaItensPrestacao(prestacaoContasViagemDTO);
        	
        	requisicaoViagemDTO.setIdSolicitacaoServico(prestacaoContasViagemDTO.getIdSolicitacaoServico());
        	
        	requisicaoViagemDTO = requisicaoViagemService.restore(requisicaoViagemDTO);
        	
        	centroResultadoDTO.setIdCentroResultado(requisicaoViagemDTO.getIdCentroCusto());
        	
        	centroResultadoDTO = centroResultadoService.restore(centroResultadoDTO);
        	
        	justificativaSolicitacaoDTO.setIdJustificativa(requisicaoViagemDTO.getIdMotivoViagem());
        	
        	justificativaSolicitacaoDTO = justificativaSolicitacaoService.restore(justificativaSolicitacaoDTO);
        	
        	requisicaoViagemDTO.setJustificativa(justificativaSolicitacaoDTO.getDescricaoJustificativa());
        	
        	valorAdiantamento = despesaViagemService.buscaTotalParaAdiantamento(integranteViagemDTO.getIdIntegranteViagem());
        	
        	double totalPrestado = 0.0;
        	
        	if(itensPrestacao != null && !itensPrestacao.isEmpty()){
        		
        		for(ItemPrestacaoContasViagemDTO dto : itensPrestacao){
        			dto.setValorAux(decimal.format(dto.getValor()));
        			totalPrestado = totalPrestado + dto.getValor();
        		}
        	}
        	
        	this.imprimirRelatorio(document, prestacaoContasViagemDTO, itensPrestacao, integranteViagemDTO, requisicaoViagemDTO, decimal.format(valorAdiantamento), centroResultadoDTO.getCodigoCentroResultado(), decimal.format(totalPrestado));
        	
        }
        
	}
	
	/**
	 * Metodo mostra o relatorio
	 * 
	 * @param document
	 * @param prestacaoContasViagemDTO
	 * @param itensPrestacao
	 * @param integranteViagemDTO
	 * @param requisicaoViagemDTO
	 * @param valorAdiantamento
	 * @param centroResultado
	 * @throws ServiceException
	 * @throws Exception
	 * @author thiago.borges
	 */
	private void imprimirRelatorio(DocumentHTML document, PrestacaoContasViagemDTO prestacaoContasViagemDTO, Collection<ItemPrestacaoContasViagemDTO> itensPrestacao, IntegranteViagemDTO integranteViagemDTO, RequisicaoViagemDTO requisicaoViagemDTO, String valorAdiantamento, String centroResultado, String totalPrestado) throws ServiceException, Exception {
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		
		parametros.put("Logo", LogoRel.getFile());
		
		parametros.put("nome", integranteViagemDTO.getNomeEmpregado());
		
		parametros.put("dataPrestacao", prestacaoContasViagemDTO.getDataHora());
		
		parametros.put("justificativa", requisicaoViagemDTO.getJustificativa());
		
		parametros.put("numeroAdiantamento", integranteViagemDTO.getIdSolicitacaoServico().toString());
		
		parametros.put("valorAdiantamento", valorAdiantamento);
		
		parametros.put("centroResultado", centroResultado);
		
		parametros.put("totalPrestado", totalPrestado);
		
		String caminhoJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioReembolsoPrestacao.jasper";
		
		String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
		
		String diretorioRelativoOS = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";
		
		JRDataSource dataSource = new JRBeanCollectionDataSource(itensPrestacao, false);

		JasperPrint print = JasperFillManager.fillReport(caminhoJasper, parametros, dataSource);

		JasperExportManager.exportReportToPdfFile(print, diretorioReceita + "/RelatorioReembolsoPrestacao.pdf");
		
		document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + diretorioRelativoOS
				+ "/RelatorioReembolsoPrestacao.pdf')");
		
		document.executeScript("JANELA_AGUARDE_MENU.hide()");

	}
}
