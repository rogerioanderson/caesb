/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.sql.Date;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLTable;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.IntegranteViagemDTO;
import br.com.centralit.citcorpore.bean.ItemPrestacaoContasViagemDTO;
import br.com.centralit.citcorpore.bean.PrestacaoContasViagemDTO;
import br.com.centralit.citcorpore.bean.RequisicaoViagemDTO;
import br.com.centralit.citcorpore.bean.RoteiroViagemDTO;
import br.com.centralit.citcorpore.bean.TipoMovimFinanceiraViagemDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.DespesaViagemService;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.IntegranteViagemService;
import br.com.centralit.citcorpore.negocio.ItemPrestacaoContasViagemService;
import br.com.centralit.citcorpore.negocio.PrestacaoContasViagemService;
import br.com.centralit.citcorpore.negocio.RequisicaoViagemService;
import br.com.centralit.citcorpore.negocio.RoteiroViagemService;
import br.com.centralit.citcorpore.negocio.TipoMovimFinanceiraViagemService;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.LogoRel;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilI18N;
import br.com.citframework.util.UtilStrings;


@SuppressWarnings({"rawtypes","unchecked"})
public class RelatorioPrestacoesContas extends AjaxFormAction {

	private UsuarioDTO usuario;

	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!WebUtil.validarSeUsuarioEstaNaSessao(request, document))
			return;

		this.comboIntegranteEstado(document, request, response);
	}

	@Override
	public Class getBeanClass() {
		return IntegranteViagemDTO.class;
	}

	/**
	 * Busca integrantes de viagem que precisam de adiantamento
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author thiago.borges
	 */
	public void pesquisaRequisicoesViagem(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

			UsuarioDTO usuario = WebUtil.getUsuario(request);
	        if (usuario == null) {
	            document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
	            document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
	            return;
	        }

	        document.executeScript("JANELA_AGUARDE_MENU.show()");

	        IntegranteViagemDTO integranteViagemDTO = (IntegranteViagemDTO) document.getBean();
	        IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, WebUtil.getUsuarioSistema(request));
	        RequisicaoViagemDTO requisicaoViagemDTO = new RequisicaoViagemDTO();
	        RequisicaoViagemService requisicaoViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, WebUtil.getUsuarioSistema(request));
	        PrestacaoContasViagemService prestacaoContasViagemService = (PrestacaoContasViagemService) ServiceLocator.getInstance().getService(PrestacaoContasViagemService.class, WebUtil.getUsuarioSistema(request));
	        PrestacaoContasViagemDTO prestacaoContasViagemDto = new PrestacaoContasViagemDTO();
	        ItemPrestacaoContasViagemService itemPrestacaoContasViagemService = (ItemPrestacaoContasViagemService) ServiceLocator.getInstance().getService(ItemPrestacaoContasViagemService.class, WebUtil.getUsuarioSistema(request));
	        RoteiroViagemDTO roteiroViagemDTO = new RoteiroViagemDTO();
	        RoteiroViagemService roteiroViagemService = (RoteiroViagemService) ServiceLocator.getInstance().getService(RoteiroViagemService.class, WebUtil.getUsuarioSistema(request));
	        TipoMovimFinanceiraViagemDTO tipoMovimFinanceiraViagemDTO = new TipoMovimFinanceiraViagemDTO();
	        TipoMovimFinanceiraViagemService tipoMovimFinanceiraViagemService = (TipoMovimFinanceiraViagemService) ServiceLocator.getInstance().getService(TipoMovimFinanceiraViagemService.class, WebUtil.getUsuarioSistema(request));
	        DespesaViagemService despesaViagemService = (DespesaViagemService) ServiceLocator.getInstance().getService(DespesaViagemService.class, WebUtil.getUsuarioSistema(request));
	        tipoMovimFinanceiraViagemDTO = tipoMovimFinanceiraViagemService.buscaDiaria();
	        EmpregadoDTO empregadoDTO = new EmpregadoDTO();
	        EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, WebUtil.getUsuarioSistema(request));
	        NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
			DecimalFormat decimal = (DecimalFormat) nf;
			decimal.applyPattern("#,##0.00");

	        Double totalAdiantamentos = 0d;
	        Double totalJaPrestado = 0d;
	        Double totalPrestacaoContas = 0d;
	        Integer idPrestacaoContas = null;
	        Double totaisAdiantamentos = 0d;

	        //metodo que recupera a cole��o dos itens da pagina referenciada
	        Collection<IntegranteViagemDTO> colIntegrantes = integranteViagemService.recuperaIntegrantesPrestacaoContas(integranteViagemDTO, integranteViagemDTO.getEOu());

	        Collection<IntegranteViagemDTO> colIntegrantesAux = new ArrayList<IntegranteViagemDTO>();

	        if(colIntegrantes != null && !colIntegrantes.isEmpty()){
		        for(IntegranteViagemDTO dto: colIntegrantes){
		        	dto = (IntegranteViagemDTO) integranteViagemService.restore(dto);
		        	roteiroViagemDTO = roteiroViagemService.findByIdIntegrante(dto.getIdIntegranteViagem());
		        	requisicaoViagemDTO = requisicaoViagemService.recuperaRequisicaoPelaSolicitacao(dto.getIdSolicitacaoServico());
		        	empregadoDTO = empregadoService.restoreByIdEmpregado(dto.getIdRespPrestacaoContas());

		        	idPrestacaoContas = prestacaoContasViagemService.recuperaIdPrestacaoSeExistirByIdResponsavelPrestacaoContas(dto.getIdSolicitacaoServico(), dto.getIdEmpregado(), dto.getIdRespPrestacaoContas());
		        	totalJaPrestado = 0d;

		        	if(idPrestacaoContas != null){
		        		prestacaoContasViagemDto.setIdPrestacaoContasViagem(idPrestacaoContas);
		        		prestacaoContasViagemDto = prestacaoContasViagemService.restore(prestacaoContasViagemDto);
		        		totalJaPrestado = this.calculaValorPrestadoContas(itemPrestacaoContasViagemService.recuperaItensPrestacao(prestacaoContasViagemDto));
		        		totalPrestacaoContas += totalJaPrestado;
		        		dto.setValorPrestado(decimal.format(totalJaPrestado));
		        	}else{
		        		dto.setValorPrestado(decimal.format(0.0));
		        	}

		        	totalAdiantamentos = despesaViagemService.buscaTotalParaAdiantamento(dto.getIdIntegranteViagem());
		        	totaisAdiantamentos += totalAdiantamentos;

		        	dto.setVolta(roteiroViagemDTO.getVolta());
		        	dto.setQtdDiarias(this.calculaDiarias(roteiroViagemDTO.getIda(), roteiroViagemDTO.getVolta()));
		        	dto.setValorAdiantamento(totalAdiantamentos.toString());
		        	dto.setValorDespesaExtra(decimal.format(despesaViagemService.getTotalDespesaExtraViagem(dto.getIdIntegranteViagem())));
		        	dto.setValorTotalAdiantamento(decimal.format(totalAdiantamentos));
		        	dto.setRespPrestacaoContas(empregadoDTO.getNome());
		        	colIntegrantesAux.add(dto);
		        }

		        this.imprimirRelatorio(document, request, response, colIntegrantesAux, decimal.format(totaisAdiantamentos), decimal.format(totalJaPrestado), decimal.format(totalPrestacaoContas));

	       }else{
	    	   document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.resultado"));
	    	   document.executeScript("JANELA_AGUARDE_MENU.hide()");
	    	   return;
	       }

	        document.executeScript("JANELA_AGUARDE_MENU.hide()");
	}

	public Double calculaValorPrestadoContas(Collection<ItemPrestacaoContasViagemDTO> itens){
		Double total = 0d;
		if(itens != null && !itens.isEmpty()){
			for(ItemPrestacaoContasViagemDTO item : itens){
				total = total + item.getValor();
			}
		}

		return total;
	}

	public Integer calculaDiarias(Date ida, Date volta) throws Exception{
		int qtdDiarias = 0;
		if(ida != null &&  volta != null){

			GregorianCalendar ini = new GregorianCalendar();
			GregorianCalendar fim = new GregorianCalendar();
			SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd");
			ini.setTime(sdf.parse(ida.toString()));
			fim.setTime(sdf.parse(volta.toString()));
			long dt1 = ini.getTimeInMillis();
			long dt2 = fim.getTimeInMillis();
			qtdDiarias = (int) ((((dt2 - dt1) / 86400000)+1));
		}

		return qtdDiarias;
	}

	public void imprimirRelatorio(DocumentHTML document, HttpServletRequest request, HttpServletResponse response, Collection<IntegranteViagemDTO> integrantes, String totalAdiantamentos, String valorPrestado, String totalPrestacao) throws ServiceException, Exception {

		document.executeScript("JANELA_AGUARDE_MENU.show()");

		Map<String, Object> parametros = new HashMap<String, Object>();

		parametros.put("Logo", LogoRel.getFile());

		parametros.put("totalAdiantamentos", totalAdiantamentos);

		parametros.put("valorPrestado", valorPrestado);

		parametros.put("totalPrestacao", totalPrestacao);

		String caminhoJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioPrestacoesContas.jasper";

		String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";

		String diretorioRelativoOS = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";

		JRDataSource dataSource = new JRBeanCollectionDataSource(integrantes, false);

		JasperPrint print = JasperFillManager.fillReport(caminhoJasper, parametros, dataSource);

		JasperExportManager.exportReportToPdfFile(print, diretorioReceita + "/RelatorioPrestacoesContas.pdf");

		document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + diretorioRelativoOS
				+ "/RelatorioPrestacoesContas.pdf')");

		document.executeScript("JANELA_AGUARDE_MENU.hide()");

	}

	/**
	 * Monta a tabela das presta��es de contas pendentes
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author renato.jesus
	 * @throws ServiceException
	 */
	public void montaPrestacoesContasPendentes(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		UsuarioDTO usuario = WebUtil.getUsuario(request);
        if (usuario == null) {
            document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
            document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
            return;
        }

        document.executeScript("JANELA_AGUARDE_MENU.show()");

        IntegranteViagemDTO integranteViagemDTO = (IntegranteViagemDTO) document.getBean();
        IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, WebUtil.getUsuarioSistema(request));
        RequisicaoViagemDTO requisicaoViagemDTO = new RequisicaoViagemDTO();
        RequisicaoViagemService requisicaoViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, WebUtil.getUsuarioSistema(request));
        PrestacaoContasViagemService prestacaoContasViagemService = (PrestacaoContasViagemService) ServiceLocator.getInstance().getService(PrestacaoContasViagemService.class, WebUtil.getUsuarioSistema(request));
        PrestacaoContasViagemDTO prestacaoContasViagemDto = new PrestacaoContasViagemDTO();
        ItemPrestacaoContasViagemService itemPrestacaoContasViagemService = (ItemPrestacaoContasViagemService) ServiceLocator.getInstance().getService(ItemPrestacaoContasViagemService.class, WebUtil.getUsuarioSistema(request));
        RoteiroViagemDTO roteiroViagemDTO = new RoteiroViagemDTO();
        RoteiroViagemService roteiroViagemService = (RoteiroViagemService) ServiceLocator.getInstance().getService(RoteiroViagemService.class, WebUtil.getUsuarioSistema(request));
        TipoMovimFinanceiraViagemDTO tipoMovimFinanceiraViagemDTO = new TipoMovimFinanceiraViagemDTO();
        TipoMovimFinanceiraViagemService tipoMovimFinanceiraViagemService = (TipoMovimFinanceiraViagemService) ServiceLocator.getInstance().getService(TipoMovimFinanceiraViagemService.class, WebUtil.getUsuarioSistema(request));
        DespesaViagemService despesaViagemService = (DespesaViagemService) ServiceLocator.getInstance().getService(DespesaViagemService.class, WebUtil.getUsuarioSistema(request));
        tipoMovimFinanceiraViagemDTO = tipoMovimFinanceiraViagemService.buscaDiaria();
        EmpregadoDTO empregadoDTO = new EmpregadoDTO();
        EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, WebUtil.getUsuarioSistema(request));
        NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		DecimalFormat decimal = (DecimalFormat) nf;
		decimal.applyPattern("#,##0.00");

        Double totalAdiantamentos = 0d;
        Double totalJaPrestado = 0d;
        Integer idPrestacaoContas = null;

        //metodo que recupera a cole��o dos itens da pagina referenciada
        Collection<IntegranteViagemDTO> colIntegrantes = integranteViagemService.recuperaIntegrantesPrestacaoContas(integranteViagemDTO, integranteViagemDTO.getEOu());

        Collection<IntegranteViagemDTO> colIntegrantesAux = new ArrayList<IntegranteViagemDTO>();

        HTMLTable tblPrestacoesContasPendentes = document.getTableById("tblPrestacoesContasPendentes");

        if(colIntegrantes != null && !colIntegrantes.isEmpty()){
	        for(IntegranteViagemDTO dto: colIntegrantes){
	        	dto = (IntegranteViagemDTO) integranteViagemService.restore(dto);
	        	roteiroViagemDTO = roteiroViagemService.findByIdIntegrante(dto.getIdIntegranteViagem());
	        	requisicaoViagemDTO = requisicaoViagemService.recuperaRequisicaoPelaSolicitacao(dto.getIdSolicitacaoServico());
	        	empregadoDTO = empregadoService.restoreByIdEmpregado(dto.getIdRespPrestacaoContas());

	        	idPrestacaoContas = prestacaoContasViagemService.recuperaIdPrestacaoSeExistirByIdResponsavelPrestacaoContas(dto.getIdSolicitacaoServico(), dto.getIdEmpregado(), dto.getIdRespPrestacaoContas());
	        	totalJaPrestado = 0d;

	        	if(idPrestacaoContas != null){
	        		prestacaoContasViagemDto.setIdPrestacaoContasViagem(idPrestacaoContas);
	        		prestacaoContasViagemDto = prestacaoContasViagemService.restore(prestacaoContasViagemDto);
	        		totalJaPrestado = this.calculaValorPrestadoContas(itemPrestacaoContasViagemService.recuperaItensPrestacao(prestacaoContasViagemDto));
	        		dto.setValorPrestado(decimal.format(totalJaPrestado));
	        	}else{
	        		dto.setValorPrestado(decimal.format(0.0));
	        	}

	        	totalAdiantamentos = despesaViagemService.buscaTotalParaAdiantamento(dto.getIdIntegranteViagem());

	        	dto.setVolta(roteiroViagemDTO.getVolta());
	        	dto.setQtdDiarias(this.calculaDiarias(roteiroViagemDTO.getIda(), roteiroViagemDTO.getVolta()));
	        	dto.setValorAdiantamento(totalAdiantamentos.toString());
	        	dto.setValorDespesaExtra(decimal.format(despesaViagemService.getTotalDespesaExtraViagem(dto.getIdIntegranteViagem())));
	        	dto.setValorTotalAdiantamento(decimal.format(totalAdiantamentos));
	        	dto.setRespPrestacaoContas(empregadoDTO.getNome());
	        	colIntegrantesAux.add(dto);
	        }

	        tblPrestacoesContasPendentes.deleteAllRows();

	        tblPrestacoesContasPendentes.addRowsByCollection(colIntegrantesAux, new String[]{"idSolicitacaoServico", "volta", "respPrestacaoContas", "estado", "valorTotalAdiantamento", "valorPrestado", ""}, null, null, new String[]{"gerarButton"}, null, null);
       }else{
    	   document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.resultado"));
    	   document.executeScript("JANELA_AGUARDE_MENU.hide()");
    	   tblPrestacoesContasPendentes.deleteAllRows();
    	   return;
       }

        document.executeScript("JANELA_AGUARDE_MENU.hide()");
	}

	/**
	 * Expirar a presta��o de contas pendente
	 *
	 * @author renato.jesus
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 */
	public void expiraPrestacaoContasPendente(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		UsuarioDTO usuario = WebUtil.getUsuario(request);
        if (usuario == null) {
            document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
            document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
            return;
        }

        document.executeScript("JANELA_AGUARDE_MENU.show()");

        IntegranteViagemDTO integranteViagemDTO = (IntegranteViagemDTO) document.getBean();

        IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, WebUtil.getUsuarioSistema(request));

        integranteViagemService.expiraPrestacaoContasPendente(integranteViagemDTO);

        document.getElementById("idIntegranteViagem").setValue("");

        document.alert(UtilI18N.internacionaliza(request, "relatorioPrestacoesContas.expiradoComSucesso"));

        this.montaPrestacoesContasPendentes(document, request, response);

        document.executeScript("JANELA_AGUARDE_MENU.hide()");
	}

	private void comboIntegranteEstado(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		document.getSelectById("comboIntegranteEstado").addOptionIfNotExists("", "Todos");
		document.getSelectById("comboIntegranteEstado").addOptionIfNotExists("Aguardando Corre��o", "Aguardando Corre��o");
		document.getSelectById("comboIntegranteEstado").addOptionIfNotExists("Em Confer�ncia", "Em Confer�ncia");
		document.getSelectById("comboIntegranteEstado").addOptionIfNotExists("Em Presta��o de Contas", "Em Presta��o de Contas");
		document.getSelectById("comboIntegranteEstado").addOptionIfNotExists("Prazo de presta��o de contas expirado", "Prazo de presta��o de contas expirado");
	}
}
