/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.InternacionalizarDTO;
import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.util.XmlReadLookup;

public class Internacionalizar extends AjaxFormAction {

    @Override
    public void load(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {

    }

    public void internacionaliza(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        //final InternacionalizarDTO bean = (InternacionalizarDTO) document.getBean();

    	WebUtil webUtil = new WebUtil(); 
    	InternacionalizarDTO bean = (InternacionalizarDTO) document.getBean();

        final String idiomaPadrao = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.IDIOMAPADRAO, "pt").toLowerCase();

        request.getSession(true).setAttribute("menu", null);
        request.getSession(true).setAttribute("menuPadrao", null);

        if (bean != null && StringUtils.isNotBlank(bean.getLocale())) {
        	webUtil.setLocale(bean.getLocale().trim(), request);
            XmlReadLookup.getInstance(new Locale(bean.getLocale().trim()));
        } else {
        	webUtil.setLocale(idiomaPadrao, request);
            XmlReadLookup.getInstance(new Locale(idiomaPadrao));
        }

        document.executeScript("window.location.reload(true)");
    }

    /**
     * Realiza a internacionaliza��o das op��es de Sim(S)/N�o(N) do sistema
     *
     * <p>
     * Ex: Se o usu�rio estiver utilizando o sistema com o idioma em ingl�s, ele dever� entrar com o valor "Y" para Sim e "N" para N�o. O sistema deve converter o valor inserido no
     * input para o valor utilizado internamente. Ex: Y = S
     * </p>
     *
     * @param request
     * @param method
     * @param opt
     * @return String
     * @author rodrigo.acorse
     * @since 21/03/2014
     */
    public static String internacionalizaOptionSN(final HttpServletRequest request, final String method, String opt) {
        final String lang = (String) request.getSession().getAttribute("locale");

        if (lang != null && lang.equalsIgnoreCase("en") && method.equalsIgnoreCase("save")) {
            if (opt.trim().equalsIgnoreCase("y")) {
                opt = "S";
            }
        } else if (lang != null && lang.equalsIgnoreCase("en") && method.equalsIgnoreCase("restore")) {
            if (opt.trim().equalsIgnoreCase("s")) {
                opt = "Y";
            }
        }

        return opt;
    }

    @Override
    public Class<InternacionalizarDTO> getBeanClass() {
        return InternacionalizarDTO.class;
    }

}
