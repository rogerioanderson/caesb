/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.io.File;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.UploadDTO;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.util.UtilStrings;
import br.com.citframework.util.UtilTratamentoArquivos;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class VisualizarUploadTemp extends AjaxFormAction {

	@Override
	public Class getBeanClass() {
		return UploadDTO.class;
	}

	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		/** Inclu�do por valdoilo.damasceno 07.04.2015 como tratatativa para erro de sess�o. */
		WebUtil.validarSeUsuarioEstaNaSessao(request, document);

		UploadDTO uploadDto = (UploadDTO) document.getBean();
		if (uploadDto.getPath() != null) {
			if (uploadDto.getPath().startsWith("ID=")) {
				
				String strId = UtilStrings.apenasNumeros(uploadDto.getPath());
				Integer idControleGed = null;
				idControleGed = new Integer(strId);
				if (idControleGed != null) {
					String pathRecuperado = getFromGed(idControleGed);
					String gedFileName = getNameFromGed(idControleGed);
					byte[] bytes = UtilTratamentoArquivos.getBytesFromFile(new File(pathRecuperado));

					if (bytes != null) {
						ServletOutputStream outputStream = response.getOutputStream();
						response.setHeader("Content-Disposition", "attachment; filename=" + gedFileName);
						response.setContentLength(bytes.length);
						outputStream.write(bytes);
						outputStream.flush();
						outputStream.close();
					}

				}
			} else {
				String pathRecuperado = uploadDto.getPath();

				byte[] bytes = UtilTratamentoArquivos.getBytesFromFile(new File(pathRecuperado));
				ServletOutputStream outputStream = response.getOutputStream();
				response.setHeader("Content-Disposition", "attachment; filename=TEMPORARIO." + CITCorporeUtil.getNameFile(pathRecuperado));
				if (bytes != null) {
					response.setContentLength(bytes.length);
					outputStream.write(bytes);
				}
				outputStream.flush();
				outputStream.close();
			}
		}
	}
}
