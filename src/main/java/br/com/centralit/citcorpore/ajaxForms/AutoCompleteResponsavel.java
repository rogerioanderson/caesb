/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.AutoCompleteDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.UsuarioService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilI18N;

import com.google.gson.Gson;

public class AutoCompleteResponsavel extends AjaxFormAction {

	private UsuarioService usuarioService;

	private final Gson gson = new Gson();

	@Override
	public Class<EmpregadoDTO> getBeanClass() {
		return EmpregadoDTO.class;
	}

	@Override
	public void load(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {

		if (request.getParameter("query") != null) {
			final String consulta = new String(request.getParameter("query").getBytes("ISO-8859-1"), "UTF-8");
			// Corrige o enconding do par�metro desejado.

			final String idContratoStr = request.getParameter("contrato");
			Integer idContrato = null;
			if (idContratoStr != null && !idContratoStr.equals("-1") && !idContratoStr.equals("")) {
				idContrato = Integer.parseInt(idContratoStr);
			}

			Collection<UsuarioDTO> listUsuarioDto = new ArrayList<>();

			String idUnidadeStr = request.getParameter("unidade");
			Integer idUnidade = null;
			if (idUnidadeStr != null && !idUnidadeStr.equals("-1") && !idUnidadeStr.equals("")) {
				idUnidade = Integer.parseInt(idUnidadeStr);
			}

			/* Criado para permitir filtrar as solicita��es que n�o possuem respons�vel. valdoilo.damasceno */
			UsuarioDTO semResponsavel = new UsuarioDTO();

			semResponsavel.setNomeUsuario(UtilI18N.internacionaliza(request, "citsmart.comum.semresponsavel"));
			semResponsavel.setIdUsuario(-1);

			listUsuarioDto.add(semResponsavel);

			if (idContrato != null) {
				listUsuarioDto.addAll(this.getUsuarioService().findUsuarioByNomeAndIdContratoAndIdUnidade(consulta, idContrato, idUnidade));
			}

			final AutoCompleteDTO autoCompleteDTO = new AutoCompleteDTO();

			final List<String> listNome = new ArrayList<>();
			final List<Integer> listIdUsuario = new ArrayList<>();

			/* Criado para permitir filtrar as solicita��es que n�o possuem respons�vel. valdoilo.damasceno */
			listNome.add(UtilI18N.internacionaliza(request, "citsmart.comum.semresponsavel"));
			listIdUsuario.add(-1);

			if (listUsuarioDto != null && !listUsuarioDto.isEmpty()) {

				for (final UsuarioDTO usuarioDto : listUsuarioDto) {

					if (usuarioDto.getIdUsuario() != null) {
						listNome.add(usuarioDto.getNomeUsuario());
						listIdUsuario.add(usuarioDto.getIdUsuario());
					}
				}
			}
			autoCompleteDTO.setQuery(consulta);
			autoCompleteDTO.setSuggestions(listNome);
			autoCompleteDTO.setData(listIdUsuario);

			String json = "";

			if (request.getParameter("colection") != null) {
				json = gson.toJson(listUsuarioDto);
			} else {
				json = gson.toJson(autoCompleteDTO);
			}

			request.setAttribute("json_response", json);
		}
	}

	/**
	 * Retorna inst�ncia de UsuarioService;
	 * 
	 * @return UsuarioService
	 * @author valdoilo.damasceno
	 * @since 27.02.2015
	 */
	public UsuarioService getUsuarioService() throws ServiceException {

		if (usuarioService == null) {
			usuarioService = (UsuarioService) ServiceLocator.getInstance().getService(UsuarioService.class, null);
		}
		return usuarioService;
	}
}
