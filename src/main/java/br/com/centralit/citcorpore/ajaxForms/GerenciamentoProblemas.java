/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;

import br.com.centralit.bpm.dto.TarefaFluxoDTO;
import br.com.centralit.bpm.util.Enumerados;
import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.GerenciamentoProblemasDTO;
import br.com.centralit.citcorpore.bean.ProblemaDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.ProblemaDAO;
import br.com.centralit.citcorpore.negocio.CategoriaProblemaService;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.ExecucaoProblemaService;
import br.com.centralit.citcorpore.negocio.ItemConfiguracaoService;
import br.com.centralit.citcorpore.negocio.PastaService;
import br.com.centralit.citcorpore.negocio.ProblemaItemConfiguracaoService;
import br.com.centralit.citcorpore.negocio.ProblemaService;
import br.com.centralit.citcorpore.util.Enumerados.SituacaoProblema;
import br.com.centralit.citcorpore.util.Enumerados.SituacaoRequisicaoProblema;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

@SuppressWarnings({ "rawtypes", "unchecked","unused" })
public class GerenciamentoProblemas extends AjaxFormAction {
	private ProblemaService problemaService;
	private EmpregadoService empregadoService;
	private CategoriaProblemaService categoriaProblemaService;
	private ProblemaItemConfiguracaoService problemaItemConfiguracaoService;
	private ItemConfiguracaoService itemConfiguracaoService;
	private PastaService pastaService;
	private UsuarioDTO usuario;

	@Override
	public Class getBeanClass() {
		return GerenciamentoProblemasDTO.class;
	}

	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		/*usuario = WebUtil.getUsuario(request);
		
		if(usuario!=null){
			
			Problema problema = new Problema();
			
			problema.notificarPrazoSolucionarProblemaExpirou(document, request, response, usuario);
			
		}*/
		
		exibeTarefas(document, request, response);
		
		
	}

	
	public void exibeTarefas(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ProblemaDTO problemaDto = new ProblemaDTO();
		
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert("Sessao expirada! Favor efetuar logon novamente!");
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		GerenciamentoProblemasDTO gerenciamentoBean = (GerenciamentoProblemasDTO) document.getBean();

		ExecucaoProblemaService execucaoProblemaService = (ExecucaoProblemaService) ServiceLocator.getInstance().getService(ExecucaoProblemaService.class, null);
		List<TarefaFluxoDTO> colTarefas = execucaoProblemaService.recuperaTarefas(usuario.getLogin());
		if (colTarefas == null)
			return;

		boolean bFiltroPorSolicitacao = false;
		if(gerenciamentoBean != null){
			bFiltroPorSolicitacao = gerenciamentoBean.getIdProblemaSel() != null && gerenciamentoBean.getIdProblemaSel().length() > 0;
		}
		List<TarefaFluxoDTO> colTarefasFiltradas = new ArrayList();
		if (!bFiltroPorSolicitacao)
			colTarefasFiltradas.addAll(colTarefas);
		else {
			for (TarefaFluxoDTO tarefaDto : colTarefas) {
				boolean bAdicionar = false;
				String idProblema = "" + ((ProblemaDTO) tarefaDto.getProblemaDto()).getIdProblema();
				bAdicionar = idProblema.indexOf(gerenciamentoBean.getIdProblemaSel()) >= 0;
				if (bAdicionar)
					colTarefasFiltradas.add(tarefaDto);
			}
		}
		List colTarefasFiltradasFinal = new ArrayList();
		HashMap mapAtr = new HashMap();
		mapAtr.put("-- Sem Atribui��o --", "-- Sem Atribui��o --");
		for (TarefaFluxoDTO tarefaDto : colTarefasFiltradas) {
			problemaDto = (ProblemaDTO) tarefaDto.getProblemaDto();
			problemaDto.setDataHoraInicioSLAStr("");
			problemaDto.setDescricao("");
			int prazoHH = 0;
			int prazoMM = 0;
			if (problemaDto.getPrazoHH() != null) {
				prazoHH = problemaDto.getPrazoHH();
			}
			if (problemaDto.getPrazoMM() != null) {
				prazoMM = problemaDto.getPrazoMM();
			}
			
			problemaDto.setDataHoraLimiteStr(UtilDatas.convertDateToString(TipoDate.TIMESTAMP_DEFAULT, problemaDto.getDataHoraLimite(), WebUtil.getLanguage(request)));
			problemaDto.setDataHoraInicioSLAStr(UtilDatas.convertDateToString(TipoDate.TIMESTAMP_DEFAULT, problemaDto.getDataHoraInicioSLA(), WebUtil.getLanguage(request)));
			problemaDto.setDataHoraCapturaStr(UtilDatas.convertDateToString(TipoDate.TIMESTAMP_DEFAULT, problemaDto.getDataHoraCaptura(), WebUtil.getLanguage(request)));

			if (problemaDto.getResponsavel() != null) {
				if (!mapAtr.containsKey(problemaDto.getResponsavel())) {
					mapAtr.put(problemaDto.getResponsavel(), problemaDto.getResponsavel());
				}
			}

			if (gerenciamentoBean.getAtribuidaCompartilhada() == null || gerenciamentoBean.getAtribuidaCompartilhada().trim().equalsIgnoreCase("")) {
				colTarefasFiltradasFinal.add(tarefaDto);
			} else {
				if (gerenciamentoBean.getAtribuidaCompartilhada().trim().equalsIgnoreCase(UtilI18N.internacionaliza(request, "citcorpore.comum.sematribuicao"))) {
					if (tarefaDto.getResponsavelAtual() == null || tarefaDto.getResponsavelAtual().trim().equalsIgnoreCase("")) {
						colTarefasFiltradasFinal.add(tarefaDto);
					}
				} else {
					if (gerenciamentoBean.getAtribuidaCompartilhada().trim().equalsIgnoreCase(tarefaDto.getResponsavelAtual())) {
						colTarefasFiltradasFinal.add(tarefaDto);
					}
				}
			}
			
			String status = this.setStatusInternacionalidados(document, request, response, problemaDto);
			if(status!=null){
				problemaDto.setStatus(status);
			}
		
		}
		
			if (gerenciamentoBean != null && (gerenciamentoBean.getIdProblemaSel() != null && !gerenciamentoBean.getIdProblemaSel().trim().equalsIgnoreCase(""))){
			        /**
			         * Por excesso(ou n�o?) de zelo, foi colocado esse try-catch na convers�o do valor retornado por "gerenciamentoBean.getIdProblemaSel()", que � uma String, em Integer.
			         * O m�todo que � da api Java padr�o, "Integer.valueOf(String s)", emite uma exce��o quando n�o consegue fazer o parse, da� a necessidade do try-catch.
			         * alterado por rcs (Rafael C�sar Soyer) - Analista Desenvolvedor
			         * data: 07/04/2015
			         * email: rafael.soyer@centrait.com.br
			         */
			        try {
			            Integer integer_idProblemaSel = Integer.valueOf(gerenciamentoBean.getIdProblemaSel());
			            problemaDto.setIdProblema(integer_idProblemaSel);
			        }
			        catch(NumberFormatException numb_exc){
			            numb_exc.printStackTrace();
			        }
			}
			
			String tarefasStr = serializaTarefas(colTarefasFiltradasFinal, request);
			
			document.executeScript("exibirTarefas('" + tarefasStr + "');");
			
			
	}
		
	private String serializaTarefas(List<TarefaFluxoDTO> colTarefas, HttpServletRequest request) throws Exception {
		if (colTarefas == null)
			return null;
		for (TarefaFluxoDTO tarefaDto : colTarefas) {
			String elementoFluxo_serialize = StringEscapeUtils.escapeJavaScript(br.com.citframework.util.WebUtil.serializeObject(tarefaDto.getElementoFluxoDto(), WebUtil.getLanguage(request)));
			String problema_serialize = StringEscapeUtils.escapeJavaScript(br.com.citframework.util.WebUtil.serializeObject(tarefaDto.getProblemaDto(), WebUtil.getLanguage(request)));
			
			tarefaDto.setElementoFluxo_serialize(elementoFluxo_serialize);
			tarefaDto.setProblema_serialize(problema_serialize);
		}
		return br.com.citframework.util.WebUtil.serializeObjects(colTarefas, WebUtil.getLanguage(request));
	}

	public void preparaExecucaoTarefa(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert("Sess�o expirada! Favor efetuar logon novamente!");
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		GerenciamentoProblemasDTO gerenciamentoBean = (GerenciamentoProblemasDTO) document.getBean();
		if (gerenciamentoBean.getIdTarefa() == null)
			return;
		
		if(gerenciamentoBean.getIdProblema() == null)
			return;

		ExecucaoProblemaService execucaoProblemaService = (ExecucaoProblemaService) ServiceLocator.getInstance().getService(ExecucaoProblemaService.class, null);
		TarefaFluxoDTO tarefaDto = execucaoProblemaService.recuperaTarefa(usuario.getLogin(), gerenciamentoBean.getIdTarefa());
		if (tarefaDto == null || tarefaDto.getElementoFluxoDto() == null || !tarefaDto.getExecutar().equals("S") || tarefaDto.getElementoFluxoDto().getTipoInteracao() == null)
			return;

		if (tarefaDto.getElementoFluxoDto().getTipoInteracao().equals(Enumerados.INTERACAO_VISAO)) {
			if (tarefaDto.getIdVisao() != null) {
				document.executeScript("exibirVisao('Executar tarefa " + tarefaDto.getElementoFluxoDto().getDocumentacao() + "','" + tarefaDto.getIdVisao() + "','"
						+ tarefaDto.getElementoFluxoDto().getIdFluxo() + "','" + tarefaDto.getIdItemTrabalho() + "','" + gerenciamentoBean.getAcaoFluxo() + "');");
			} else {
				document.alert("Vis�o para tarefa \"" + tarefaDto.getElementoFluxoDto().getDocumentacao() + "\" n�o encontrada");
			}
		} else {
			String caracterParmURL = "?";
			if (tarefaDto.getElementoFluxoDto().getUrl().indexOf("?") > -1) { // Se na URL ja tiver ?, entao colocar &
				caracterParmURL = "&";
			}
			document.executeScript("exibirUrl('Executar tarefa " + tarefaDto.getElementoFluxoDto().getDocumentacao() + "','" + tarefaDto.getElementoFluxoDto().getUrl()
					+ caracterParmURL + "idProblema=" + gerenciamentoBean.getIdProblema() + "&idTarefa="
					+ tarefaDto.getIdItemTrabalho() + "&acaoFluxo=" + gerenciamentoBean.getAcaoFluxo() + "');");
		}
	}

	public void reativaProblema(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert("Sess�o expirada! Favor efetuar logon novamente!");
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		GerenciamentoProblemasDTO gerenciamentoBean = (GerenciamentoProblemasDTO) document.getBean();
		if (gerenciamentoBean.getIdProblema() == null)
			return;

		ProblemaService problemaService = (ProblemaService) ServiceLocator.getInstance().getService(ProblemaService.class, null);
		ProblemaDTO problemaDto = problemaService.restoreAll(gerenciamentoBean.getIdProblema());
		problemaService.reativa(usuario, problemaDto);
		exibeTarefas(document, request, response);
		document.executeScript("JANELA_AGUARDE_MENU.hide()");
	}
	
	public void capturaTarefa(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		GerenciamentoProblemasDTO problemaBean = (GerenciamentoProblemasDTO) document.getBean();

		if (problemaBean.getIdTarefa() == null) {
			return;
		}

		if (problemaBean.getIdProblema() == null){
			return;
		}
		
		ProblemaDTO problemaDto = new ProblemaDTO();
		ProblemaDAO problemaDao = new ProblemaDAO();
		problemaDto.setIdProblema(problemaBean.getIdProblema());
		problemaDto.setIdProprietario(usuario.getIdUsuario());
		problemaDao.updateNotNull(problemaDto);
		
		
		
		ExecucaoProblemaService execucaoProblemaService = (ExecucaoProblemaService) ServiceLocator.getInstance().getService(ExecucaoProblemaService.class, null);
		execucaoProblemaService.executa(usuario, problemaBean.getIdTarefa(), Enumerados.ACAO_INICIAR);
		exibeTarefas(document, request, response);

		problemaBean = null;
		
		document.executeScript("JANELA_AGUARDE_MENU.hide()");
	}
	
	
	private ProblemaService getProblemaService() throws ServiceException, Exception {
		if (problemaService == null) {
			problemaService = (ProblemaService) ServiceLocator.getInstance().getService(ProblemaService.class, null);
		}
		return problemaService;
	}

	private EmpregadoService getEmpregadoService() throws ServiceException, Exception {
		if (empregadoService == null) {
			empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
		}
		return empregadoService;
	}

	private CategoriaProblemaService getCategoriaProblemaService() throws ServiceException, Exception {
		if (categoriaProblemaService == null) {
			categoriaProblemaService = (CategoriaProblemaService) ServiceLocator.getInstance().getService(CategoriaProblemaService.class, null);
		}
		return categoriaProblemaService;
	}

	private ProblemaItemConfiguracaoService getProblemaItemConfiguracaoService() throws ServiceException, Exception {
		if (problemaItemConfiguracaoService == null) {
			problemaItemConfiguracaoService = (ProblemaItemConfiguracaoService) ServiceLocator.getInstance().getService(ProblemaItemConfiguracaoService.class, null);
		}
		return problemaItemConfiguracaoService;
	}

	private ItemConfiguracaoService getItemConfiguracaoService() throws ServiceException, Exception {
		if (itemConfiguracaoService == null) {
			itemConfiguracaoService = (ItemConfiguracaoService) ServiceLocator.getInstance().getService(ItemConfiguracaoService.class, null);
		}
		return itemConfiguracaoService;
	}

	
	private PastaService getPastaService() throws ServiceException, Exception {
		if (pastaService == null) {
			pastaService = (PastaService) ServiceLocator.getInstance().getService(PastaService.class, null);
		}
		return pastaService;
	}
	
	/**Metodo para internacionalizar status de problema
	 * @param document
	 * @param request
	 * @param response
	 * @param problemaDto
	 * @return
	 * @throws Exception
	 * @author thays.araujo
	 */
	public String setStatusInternacionalidados(DocumentHTML document, HttpServletRequest request, HttpServletResponse response,ProblemaDTO problemaDto) throws Exception{
		
		String status = "";
		
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Registrada.getDescricao())){
			status = UtilI18N.internacionaliza(request, "citcorpore.comum.registrada");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.EmInvestigacao.getDescricao())){
			status = UtilI18N.internacionaliza(request, "gerenciamentoProblema.emInvestigacao");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Aprovada.getDescricao())){
			status = UtilI18N.internacionaliza(request, "citcorpore.comum.aprovacao");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Planejada.getDescricao())){
			status = UtilI18N.internacionaliza(request, "citcorpore.comum.planejada");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.EmExecucao.getDescricao())){
			status = UtilI18N.internacionaliza(request, "citcorpore.comum.emExecucao");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Executada.getDescricao())){
			status = UtilI18N.internacionaliza(request, "perfil.executada");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Suspensa.getDescricao())){
			status = UtilI18N.internacionaliza(request, "citcorpore.comum.suspensa");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Cancelada.getDescricao())){
			status = UtilI18N.internacionaliza(request, "citcorpore.comum.cancelada");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Rejeitada.getDescricao())){
			status = UtilI18N.internacionaliza(request, "citcorpore.comum.rejeitada");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Resolvida.getDescricao())){
			status = UtilI18N.internacionaliza(request, "citcorpore.comum.resolvida");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Reaberta.getDescricao())){
			status = UtilI18N.internacionaliza(request, "citcorpore.comum.reaberta");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Concluida.getDescricao())){
			status = UtilI18N.internacionaliza(request, "citcorpore.comum.concluida");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.SolucaoContorno.getDescricao())){
			status = UtilI18N.internacionaliza(request, "problema.solucao_contorno");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Revisado.getDescricao())){
			status = UtilI18N.internacionaliza(request, "problema.revisado");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Resolucao.getDescricao())){
			status = UtilI18N.internacionaliza(request, "pesquisaProblema.resolucao");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Encerramento.getDescricao())){
			status = UtilI18N.internacionaliza(request, "grupo.encerramento");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Revisar.getDescricao())){
			status = UtilI18N.internacionaliza(request, "gerenciamentoProblema.revisar");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.RegistroErroConhecido.getDescricao())){
			status = UtilI18N.internacionaliza(request, "problema.registroErroConhecido");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoProblema.EmAndamento.getDescricao())){
			status = UtilI18N.internacionaliza(request, "citcorpore.comum.emandamento");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoProblema.Fechada.getDescricao())){
			status = UtilI18N.internacionaliza(request, "citcorpore.comum.fechada");
			return status;
		}
		if(problemaDto.getStatus().equalsIgnoreCase(SituacaoProblema.ReClassificada.getDescricao())){
			status = UtilI18N.internacionaliza(request, "citcorpore.comum.reclassificada");
			return status;
		}
		return null;
	}
	
	
}
