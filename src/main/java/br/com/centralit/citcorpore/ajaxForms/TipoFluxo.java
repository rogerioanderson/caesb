/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.bpm.dto.TipoFluxoDTO;
import br.com.centralit.bpm.servico.TipoFluxoService;
import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;

public class TipoFluxo extends AjaxFormAction {

    @Override
    public Class getBeanClass() {
	return TipoFluxoDTO.class;
    }

    private TipoFluxoService getTipoFluxoService() throws ServiceException, Exception {
	return (TipoFluxoService) ServiceLocator.getInstance().getService(TipoFluxoService.class, null);
    }

    @Override
    public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
	UsuarioDTO usuario = WebUtil.getUsuario(request);
	if (usuario == null) {
	    document.alert("Sess�o expirada! Favor efetuar logon novamente!");
	    document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
	    return;
	}
    }

    public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
	TipoFluxoDTO fluxoDto = (TipoFluxoDTO) document.getBean();
	fluxoDto = (TipoFluxoDTO) getTipoFluxoService().restore(fluxoDto);
	if (fluxoDto != null) {
	    HTMLForm form = document.getForm("form");
	    form.setValues(fluxoDto);
	}
    }

    public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	TipoFluxoDTO fluxoDto = (TipoFluxoDTO) document.getBean();

    	if (fluxoDto.getIdTipoFluxo() == null || fluxoDto.getIdTipoFluxo().intValue() == 0) {
    		getTipoFluxoService().create(fluxoDto);
    	} else {
    		getTipoFluxoService().update(fluxoDto);
    	}

    	document.alert("Registro gravado com sucesso!");

    	HTMLForm form = document.getForm("form");
    	form.clear();
        }

}
