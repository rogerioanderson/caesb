/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.TemplateImpressaoDTO;
import br.com.centralit.citcorpore.negocio.TemplateImpressaoService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilI18N;

public class TemplateImpressao extends AjaxFormAction {

	@Override
	public void load(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
	}
	
	public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		TemplateImpressaoDTO templateImpressaoDTO = (TemplateImpressaoDTO) document.getBean();
		TemplateImpressaoService templateImpressaoService = (TemplateImpressaoService) ServiceLocator.getInstance().getService(TemplateImpressaoService.class, WebUtil.getUsuarioSistema(request) );
		if (templateImpressaoDTO.getIdTemplateImpressao() == null){
			templateImpressaoService.create(templateImpressaoDTO);
		}else{
			templateImpressaoService.update(templateImpressaoDTO);
		}
		
		document.alert(UtilI18N.internacionaliza(request, "dinamicview.gravadocomsucesso"));
		
		HTMLForm form = document.getForm("form");
		form.clear();
		
		document.executeScript("limpar()");		
	}
	
	public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		TemplateImpressaoDTO templateImpressaoDTO = (TemplateImpressaoDTO) document.getBean();
		TemplateImpressaoService templateImpressaoService = (TemplateImpressaoService) ServiceLocator.getInstance().getService(TemplateImpressaoService.class, WebUtil.getUsuarioSistema(request) );
		templateImpressaoDTO = (TemplateImpressaoDTO) templateImpressaoService.restore(templateImpressaoDTO);

		HTMLForm form = document.getForm("form");
		form.clear();		
		form.setValues(templateImpressaoDTO);

		document.executeScript("setDataEditor()");
	}	

	@Override
	public Class getBeanClass() {
		return TemplateImpressaoDTO.class;
	}

}
