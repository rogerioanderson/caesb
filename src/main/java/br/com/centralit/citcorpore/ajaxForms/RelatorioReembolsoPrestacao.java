/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLTable;
import br.com.centralit.citcorpore.bean.CentroResultadoDTO;
import br.com.centralit.citcorpore.bean.CidadesDTO;
import br.com.centralit.citcorpore.bean.DespesaViagemDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.bean.IntegranteViagemDTO;
import br.com.centralit.citcorpore.bean.ItemPrestacaoContasViagemDTO;
import br.com.centralit.citcorpore.bean.JustificativaSolicitacaoDTO;
import br.com.centralit.citcorpore.bean.PrestacaoContasViagemDTO;
import br.com.centralit.citcorpore.bean.RequisicaoViagemDTO;
import br.com.centralit.citcorpore.bean.RoteiroViagemDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.CentroResultadoService;
import br.com.centralit.citcorpore.negocio.CidadesService;
import br.com.centralit.citcorpore.negocio.DespesaViagemService;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.GrupoService;
import br.com.centralit.citcorpore.negocio.IntegranteViagemService;
import br.com.centralit.citcorpore.negocio.ItemPrestacaoContasViagemService;
import br.com.centralit.citcorpore.negocio.JustificativaSolicitacaoService;
import br.com.centralit.citcorpore.negocio.PrestacaoContasViagemService;
import br.com.centralit.citcorpore.negocio.RequisicaoViagemService;
import br.com.centralit.citcorpore.negocio.RoteiroViagemService;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.LogoRel;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilI18N;


@SuppressWarnings({"rawtypes","unchecked"})
public class RelatorioReembolsoPrestacao extends AjaxFormAction {

	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(!WebUtil.validarSeUsuarioEstaNaSessao(request, document))
			return;
		
		Collection<GrupoDTO> grupos = new ArrayList<>();
		
        GrupoService grupoService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, WebUtil.getUsuarioSistema(request));
		
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		
		grupos = grupoService.getGruposByIdEmpregado(usuario.getIdEmpregado());

		document.getElementById("nomeEmpregado").setDisabled(true);
		
		if(grupos != null && !grupos.isEmpty()){
			for(GrupoDTO grupoDTO : grupos){
				if(grupoDTO.getIdGrupo() == Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.ID_GRUPO_PADRAO_RESPONSAVEL_CONFERENCIA_VIAGEM, null))){
					document.getElementById("nomeEmpregado").setDisabled(false);
					break;
				}
			}
		}
	}

	@Override
	public Class getBeanClass() {
		return IntegranteViagemDTO.class;
	}
	
	/**
	 * Busca presta��es de contas do usuario logado
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author thiago.borges
	 */
	public void pesquisaPrestacoesContas(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		document.executeScript("JANELA_AGUARDE_MENU.show()");
		
		IntegranteViagemDTO integranteViagemDTO = (IntegranteViagemDTO) document.getBean();
		
		IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, WebUtil.getUsuarioSistema(request));
		
		Collection<GrupoDTO> grupos = new ArrayList<>();
		
        GrupoService grupoService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, WebUtil.getUsuarioSistema(request));
        
        PrestacaoContasViagemService prestacaoContasViagemService = (PrestacaoContasViagemService) ServiceLocator.getInstance().getService(PrestacaoContasViagemService.class, WebUtil.getUsuarioSistema(request));
        
        CidadesService cidadesService = (CidadesService) ServiceLocator.getInstance().getService(CidadesService.class, WebUtil.getUsuarioSistema(request));
        
        CidadesDTO cidadesDTO = new CidadesDTO();
        
        RoteiroViagemService roteiroViagemService = (RoteiroViagemService) ServiceLocator.getInstance().getService(RoteiroViagemService.class, WebUtil.getUsuarioSistema(request));
        
        RoteiroViagemDTO roteiroViagemDTO = new RoteiroViagemDTO();
        
        Collection<PrestacaoContasViagemDTO> colPrestacoes = new ArrayList<>();
		
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		
		grupos = grupoService.getGruposByIdEmpregado(usuario.getIdEmpregado());
		
		boolean pertenceAoGrupoResponsavelConferencia = false;
		
		if(grupos != null && !grupos.isEmpty()){
			
			for(GrupoDTO grupoDTO : grupos){
				
				if(grupoDTO.getIdGrupo() == Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.ID_GRUPO_PADRAO_RESPONSAVEL_CONFERENCIA_VIAGEM, null))){
					pertenceAoGrupoResponsavelConferencia = true;
					break;
				}
			}
		}
		
		Collection<IntegranteViagemDTO> integrantes = new ArrayList<>();
		
		if(pertenceAoGrupoResponsavelConferencia){
			integrantes = integranteViagemService.findAllIntegrantesViagemPrestouContas(integranteViagemDTO);
		} else {
			integranteViagemDTO.setIdRespPrestacaoContas(usuario.getIdEmpregado());
			
			integrantes = integranteViagemService.recuperaIntegrantesByIdResponsavel(integranteViagemDTO, integranteViagemDTO.getEOu());
		}
		
		Integer idPrestacao = null;
		
		if(integrantes != null && !integrantes.isEmpty()){
			
			for(IntegranteViagemDTO integrante : integrantes){
				
				integrante = integranteViagemService.restore(integrante);
				
				idPrestacao = prestacaoContasViagemService.recuperaIdPrestacaoSeExistirByIdResponsavelPrestacaoContas(integrante.getIdSolicitacaoServico(), integrante.getIdEmpregado(), integrante.getIdRespPrestacaoContas());
				
				if(idPrestacao != null){
					
					roteiroViagemDTO = roteiroViagemService.findByIdIntegrante(integrante.getIdIntegranteViagem());
					
					cidadesDTO = cidadesService.findByIdCidade(roteiroViagemDTO.getDestino());
					
					PrestacaoContasViagemDTO prestacaoContasViagemDTO = new PrestacaoContasViagemDTO();
					
					prestacaoContasViagemDTO.setIdPrestacaoContasViagem(idPrestacao);
					
					prestacaoContasViagemDTO = prestacaoContasViagemService.restore(prestacaoContasViagemDTO);
					
					prestacaoContasViagemDTO.setIda(roteiroViagemDTO.getIda());
					
					prestacaoContasViagemDTO.setVolta(roteiroViagemDTO.getVolta());
					
					prestacaoContasViagemDTO.setNomeDestino(cidadesDTO.getNomeCidade()+ " - " + cidadesDTO.getNomeUf());
					
					prestacaoContasViagemDTO.setIdIntegrante(integrante.getIdIntegranteViagem());
					
					colPrestacoes.add(prestacaoContasViagemDTO);
				}
			}
		}
		
		HTMLTable tblPrestacoesContas = document.getTableById("tblPrestacoesContas");
		tblPrestacoesContas.deleteAllRows();
        
		if (colPrestacoes != null && !colPrestacoes.isEmpty()){
			tblPrestacoesContas.addRowsByCollection(colPrestacoes, new String[]{"idSolicitacaoServico","ida","volta", "nomeDestino", "dataHora", ""}, null, null, new String[]{"gerarImg"}, null, null);
		}else{
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.resultado"));
		}
		
		document.executeScript("JANELA_AGUARDE_MENU.hide()");
	}
	
	/**
	 * Metodo monta o relatorio de reembolso 
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 * @author thiago.borges
	 */
	public void montaRelatorio(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		
		document.executeScript("JANELA_AGUARDE_MENU.show()");
		
		IntegranteViagemDTO integranteViagemDTO = (IntegranteViagemDTO) document.getBean();
		
        IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, WebUtil.getUsuarioSistema(request));
		
		PrestacaoContasViagemDTO prestacaoContasViagemDTO = new PrestacaoContasViagemDTO();
		
        PrestacaoContasViagemService prestacaoContasViagemService = (PrestacaoContasViagemService) ServiceLocator.getInstance().getService(PrestacaoContasViagemService.class, WebUtil.getUsuarioSistema(request));
        
        EmpregadoDTO empregadoDTO = new EmpregadoDTO();
		
        EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, WebUtil.getUsuarioSistema(request));
        
        RequisicaoViagemDTO requisicaoViagemDTO = new RequisicaoViagemDTO();
		
        RequisicaoViagemService requisicaoViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, WebUtil.getUsuarioSistema(request));
        
        CentroResultadoDTO centroResultadoDTO = new CentroResultadoDTO();
		
        CentroResultadoService centroResultadoService = (CentroResultadoService) ServiceLocator.getInstance().getService(CentroResultadoService.class, WebUtil.getUsuarioSistema(request));
        
        DespesaViagemService despesaViagemService = (DespesaViagemService) ServiceLocator.getInstance().getService(DespesaViagemService.class, WebUtil.getUsuarioSistema(request));
        
        JustificativaSolicitacaoDTO justificativaSolicitacaoDTO = new JustificativaSolicitacaoDTO();
		
        JustificativaSolicitacaoService justificativaSolicitacaoService = (JustificativaSolicitacaoService) ServiceLocator.getInstance().getService(JustificativaSolicitacaoService.class, WebUtil.getUsuarioSistema(request));
        
        Collection<ItemPrestacaoContasViagemDTO> itensPrestacao = new ArrayList<>();
		
        ItemPrestacaoContasViagemService itemPrestacaoContasViagemService = (ItemPrestacaoContasViagemService) ServiceLocator.getInstance().getService(ItemPrestacaoContasViagemService.class, WebUtil.getUsuarioSistema(request));
        
        prestacaoContasViagemDTO.setIdPrestacaoContasViagem(integranteViagemDTO.getIdPrestacaoContas());
        
        integranteViagemDTO = integranteViagemService.restore(integranteViagemDTO);
        
        empregadoDTO = empregadoService.restoreByIdEmpregado(integranteViagemDTO.getIdRespPrestacaoContas());
        
        integranteViagemDTO.setNomeEmpregado(empregadoDTO.getNome());

        prestacaoContasViagemDTO = prestacaoContasViagemService.restore(prestacaoContasViagemDTO);
        
        NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		DecimalFormat decimal = (DecimalFormat) nf;
		decimal.applyPattern("#,##0.00");
        
        Double valorAdiantamento = 0d;
        
        if(prestacaoContasViagemDTO != null){
        	
        	itensPrestacao = itemPrestacaoContasViagemService.recuperaItensPrestacao(prestacaoContasViagemDTO);
        	
        	requisicaoViagemDTO.setIdSolicitacaoServico(prestacaoContasViagemDTO.getIdSolicitacaoServico());
        	
        	requisicaoViagemDTO = requisicaoViagemService.restore(requisicaoViagemDTO);
        	
        	centroResultadoDTO.setIdCentroResultado(requisicaoViagemDTO.getIdCentroCusto());
        	
        	centroResultadoDTO = centroResultadoService.restore(centroResultadoDTO);
        	
        	justificativaSolicitacaoDTO.setIdJustificativa(requisicaoViagemDTO.getIdMotivoViagem());
        	
        	justificativaSolicitacaoDTO = justificativaSolicitacaoService.restore(justificativaSolicitacaoDTO);
        	
        	requisicaoViagemDTO.setJustificativa(justificativaSolicitacaoDTO.getDescricaoJustificativa());
        	
        	valorAdiantamento = despesaViagemService.buscaTotalParaAdiantamento(integranteViagemDTO.getIdIntegranteViagem());
        	
        	double totalPrestado = 0.0;
        	
        	if(itensPrestacao != null && !itensPrestacao.isEmpty()){
        		
        		for(ItemPrestacaoContasViagemDTO dto : itensPrestacao){
        			dto.setValorAux(decimal.format(dto.getValor()));
        			totalPrestado = totalPrestado + dto.getValor();
        		}
        	}
        	
        	this.imprimirRelatorio(document, prestacaoContasViagemDTO, itensPrestacao, integranteViagemDTO, requisicaoViagemDTO, decimal.format(valorAdiantamento), centroResultadoDTO.getCodigoCentroResultado(), decimal.format(totalPrestado));
        	
        }
        
	}
	
	/**
	 * Metodo mostra o relatorio
	 * 
	 * @param document
	 * @param prestacaoContasViagemDTO
	 * @param itensPrestacao
	 * @param integranteViagemDTO
	 * @param requisicaoViagemDTO
	 * @param valorAdiantamento
	 * @param centroResultado
	 * @throws ServiceException
	 * @throws Exception
	 * @author thiago.borges
	 */
	public void imprimirRelatorio(DocumentHTML document, PrestacaoContasViagemDTO prestacaoContasViagemDTO, Collection<ItemPrestacaoContasViagemDTO> itensPrestacao, IntegranteViagemDTO integranteViagemDTO, 
				RequisicaoViagemDTO requisicaoViagemDTO, String valorAdiantamento, String centroResultado, String totalPrestado) throws ServiceException, Exception {
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		
		parametros.put("Logo", LogoRel.getFile());
		
		parametros.put("nome", integranteViagemDTO.getNomeEmpregado());
		
		parametros.put("dataPrestacao", prestacaoContasViagemDTO.getDataHora());
		
		parametros.put("justificativa", requisicaoViagemDTO.getJustificativa());
		
		parametros.put("numeroAdiantamento", integranteViagemDTO.getIdSolicitacaoServico().toString());
		
		parametros.put("valorAdiantamento", valorAdiantamento);
		
		parametros.put("centroResultado", centroResultado);
		
		parametros.put("totalPrestado", totalPrestado);
		
		String caminhoJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioReembolsoPrestacao.jasper";
		
		String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
		
		String diretorioRelativoOS = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";
		
		JRDataSource dataSource = new JRBeanCollectionDataSource(itensPrestacao, false);

		JasperPrint print = JasperFillManager.fillReport(caminhoJasper, parametros, dataSource);

		String nomeRelatorio = "RelatorioReembolsoPrestacao_" + integranteViagemDTO.getIdIntegranteViagem() + ".pdf";
		JasperExportManager.exportReportToPdfFile(print, diretorioReceita + "/" + nomeRelatorio);
		
		document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + diretorioRelativoOS
				+ "/" + nomeRelatorio + "')");
		
		document.executeScript("JANELA_AGUARDE_MENU.hide()");

	}
	
}
