/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.RelatorioQuantitativoServicoAnaliticoDTO;
import br.com.centralit.citcorpore.bean.ServicoDTO;
import br.com.centralit.citcorpore.bean.TipoDemandaServicoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.ServicoService;
import br.com.centralit.citcorpore.negocio.TipoDemandaServicoService;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.LogoRel;
import br.com.centralit.citcorpore.util.UtilRelatorio;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.fill.JRAbstractLRUVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import net.sf.jasperreports.export.XlsxReportConfiguration;

@SuppressWarnings({ "unchecked", "rawtypes"})
public class RelatorioQuantitativoServicoAnalitico extends AjaxFormAction {
	UsuarioDTO usuario;

	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		usuario = WebUtil.getUsuario(request);

		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		// Preenchendo a combobox de contratos.
		HTMLSelect comboContrato = document.getSelectById("idContrato");
		comboContrato.removeAllOptions();
		ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
		Collection colContrato = contratoService.listAtivos();
		comboContrato.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.todos"));
		comboContrato.addOptions(colContrato, "idContrato", "numero", null);

		// Preenchendo a combobox de tipo de servico.
		TipoDemandaServicoService tipoDemandaService = (TipoDemandaServicoService) ServiceLocator.getInstance().getService(TipoDemandaServicoService.class, null);
		HTMLSelect comboTipoDemandaServico = document.getSelectById("idTipoDemandaServico");
		comboTipoDemandaServico.removeAllOptions();
		comboTipoDemandaServico.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.todos"));
		Collection col = tipoDemandaService.listSolicitacoes();
		if (col != null) {
			comboTipoDemandaServico.addOptions(col, "idTipoDemandaServico", "nomeTipoDemandaServico", null);
		}
		
		/**
		 * Preencher combo TOP List
		 * 
		 * @author thyen.chang
		 */
		for(Enumerados.TopListEnum valor : Enumerados.TopListEnum.values())
			document.getSelectById("topList").addOption(valor.getValorTopList().toString(), UtilI18N.internacionaliza(request, valor.getNomeTopList()));
		

	}

	@Override
	public Class getBeanClass() {
		return ServicoDTO.class;
	}

	public void imprimirRelatorioQuantitativo(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = ((HttpServletRequest) request).getSession();
		Set<String> setSolicitacao = new HashSet<>();
		Set<String> setServico = new HashSet<>();

		ServicoDTO servicoDTO = (ServicoDTO) document.getBean();
		ServicoService servicoService = (ServicoService) ServiceLocator.getInstance().getService(ServicoService.class, null);
		ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);

		usuario = WebUtil.getUsuario(request);

		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		if (servicoDTO.getDataInicio() == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.validacao.datainicio"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		if (servicoDTO.getDataFim() == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.validacao.datafim"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		Collection<RelatorioQuantitativoServicoAnaliticoDTO> listaDadosRelatorio = new ArrayList<RelatorioQuantitativoServicoAnaliticoDTO>();
		
		servicoDTO.setBaseReports(Boolean.TRUE);
		Collection<ServicoDTO> listaQuantidadeServicoAnalitico = servicoService.listaQuantidadeServicoAnalitico(servicoDTO);

		if(listaQuantidadeServicoAnalitico != null){
			for (ServicoDTO servicoDTO2 : listaQuantidadeServicoAnalitico) {
				setSolicitacao.add(servicoDTO2.getIdSolicitacao() != null ? servicoDTO2.getIdSolicitacao().toString() : "");
				setServico.add(servicoDTO2.getNomeServico());
				
				if (servicoDTO2.getTempoAtendimentoHH() == null && servicoDTO2.getTempoAtendimentoMM() == null) {
					if (servicoDTO2.getSituacao().equalsIgnoreCase("EmAndamento")) {
						servicoDTO2.setTempoDecorrido(servicoDTO2.getSituacao());
					}else{
						servicoDTO2.setTempoDecorrido(UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.semTempoAtendimento"));
					}
				}else{
					servicoDTO2.setTempoDecorrido(servicoDTO2.getTempoAtendimentoHH() + "h " + servicoDTO2.getTempoAtendimentoMM() + "m");
				}
			}
		}
		
		if (listaQuantidadeServicoAnalitico != null) {
			RelatorioQuantitativoServicoAnaliticoDTO relatorioQuantitativoServicoAnaliticoDTO = new RelatorioQuantitativoServicoAnaliticoDTO();
			relatorioQuantitativoServicoAnaliticoDTO.setListaPorPeriodo(listaQuantidadeServicoAnalitico);
			listaDadosRelatorio.add(relatorioQuantitativoServicoAnaliticoDTO);
		}

		Date dt = new Date();

		String strCompl = "" + dt.getTime();
		String caminhoJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioQuantitativoServicoAnalitico.jasper";
		String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
		String diretorioRelativoOS = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";

		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros = UtilRelatorio.trataInternacionalizacaoLocale(session, parametros);

		parametros.put("TITULO_RELATORIO", UtilI18N.internacionaliza(request, "relatorioQuantitativo.relatorioQuantitativoServicoConcluidosAnalitico"));
		parametros.put("CIDADE", getCidadeParametrizada(request));
		parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
		parametros.put("NOME_USUARIO", usuario.getNomeUsuario());
		parametros.put("dataInicio", servicoDTO.getDataInicio());
		parametros.put("dataFim", servicoDTO.getDataFim());
		parametros.put("Logo", LogoRel.getFile());
		parametros.put("TotalRegistros", listaQuantidadeServicoAnalitico.size());
		parametros.put("TotalSolicitacoes", setSolicitacao.size());
		parametros.put("TotalServicos", setServico.size());

		ContratoDTO contratoDto = new ContratoDTO();
		if (servicoDTO.getIdContrato() != null) {
			contratoDto.setIdContrato(servicoDTO.getIdContrato());
			contratoDto = (ContratoDTO) contratoService.restore(contratoDto);
		}

		parametros.put("contrato", contratoDto.getNumero());
		TipoDemandaServicoService tipoDemandaService = (TipoDemandaServicoService) ServiceLocator.getInstance().getService(TipoDemandaServicoService.class, null);
		TipoDemandaServicoDTO tipoDemandaServicoDto = new TipoDemandaServicoDTO();
		
		if (servicoDTO.getIdTipoDemandaServico() != null) {
			tipoDemandaServicoDto.setIdTipoDemandaServico(servicoDTO.getIdTipoDemandaServico());
			tipoDemandaServicoDto = (TipoDemandaServicoDTO) tipoDemandaService.restore(tipoDemandaServicoDto);
		}
		
		parametros.put("nomeTipoDemandaServico", tipoDemandaServicoDto.getNomeTipoDemandaServico());

		if (listaDadosRelatorio.size() == 0 || listaQuantidadeServicoAnalitico.size() == 0) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		try {
			JRDataSource dataSource = new JRBeanCollectionDataSource(listaDadosRelatorio);

			JRSwapFile arquivoSwap = new JRSwapFile(diretorioReceita, 4096, 25);
			JRAbstractLRUVirtualizer virtualizer = new JRSwapFileVirtualizer(25, arquivoSwap, true);
			parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
			JasperPrint print = JasperFillManager.fillReport(caminhoJasper, parametros, dataSource);
			// JasperViewer.viewReport(print,false);

			JasperExportManager.exportReportToPdfFile(print, diretorioReceita + "/RelatorioQuantitativoServicoAnalitico" + strCompl + "_" + usuario.getIdUsuario() + ".pdf");

			document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + diretorioRelativoOS
					+ "/RelatorioQuantitativoServicoAnalitico" + strCompl + "_" + usuario.getIdUsuario() + ".pdf')");
		} catch (OutOfMemoryError e) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.erro.erroServidor"));
		}

		document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
	}

	public void imprimirRelatorioQuantitativoXls(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = ((HttpServletRequest) request).getSession();
		ServicoDTO servicoDTO = (ServicoDTO) document.getBean();
		ServicoService servicoService = (ServicoService) ServiceLocator.getInstance().getService(ServicoService.class, null);
		ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
		Set<String> setSolicitacao = new HashSet<>();
		Set<String> setServico = new HashSet<>();
		
		usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		if (servicoDTO.getDataInicio() == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.validacao.datainicio"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		if (servicoDTO.getDataFim() == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.validacao.datafim"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		Collection<RelatorioQuantitativoServicoAnaliticoDTO> listaDadosRelatorio = new ArrayList<RelatorioQuantitativoServicoAnaliticoDTO>();
		
		servicoDTO.setBaseReports(Boolean.TRUE);
		Collection<ServicoDTO> listaQuantidadeServicoAnalitico = servicoService.listaQuantidadeServicoAnalitico(servicoDTO);

		if(listaQuantidadeServicoAnalitico != null){
			for (ServicoDTO servicoDTO2 : listaQuantidadeServicoAnalitico) {
				setSolicitacao.add(servicoDTO2.getIdSolicitacao() != null ? servicoDTO2.getIdSolicitacao().toString() : "");
				setServico.add(servicoDTO2.getNomeServico());
				
				if (servicoDTO2.getTempoAtendimentoHH() == null && servicoDTO2.getTempoAtendimentoMM() == null) {
					if (servicoDTO2.getSituacao().equalsIgnoreCase("EmAndamento")) {
						servicoDTO2.setTempoDecorrido(servicoDTO2.getSituacao());
					}else{
						servicoDTO2.setTempoDecorrido(UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.semTempoAtendimento"));
					}
				}else{
					servicoDTO2.setTempoDecorrido(servicoDTO2.getTempoAtendimentoHH() + "h " + servicoDTO2.getTempoAtendimentoMM() + "m");
				}
			}
		}
		
		if (listaQuantidadeServicoAnalitico != null && !listaQuantidadeServicoAnalitico.isEmpty()) {
			RelatorioQuantitativoServicoAnaliticoDTO relatorioQuantitativoServicoAnaliticoDTO = new RelatorioQuantitativoServicoAnaliticoDTO();
			relatorioQuantitativoServicoAnaliticoDTO.setListaPorPeriodo(listaQuantidadeServicoAnalitico);
			listaDadosRelatorio.add(relatorioQuantitativoServicoAnaliticoDTO);
		}
		
		Date dt = new Date();
		String strCompl = "" + dt.getTime();
		String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
		String diretorioRelativoOS = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";
		String caminhoSubRelatorioJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS");

		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros = UtilRelatorio.trataInternacionalizacaoLocale(session, parametros);
		
		parametros.put("TITULO_RELATORIO", UtilI18N.internacionaliza(request, "relatorioQuantitativo.relatorioQuantitativoServicoConcluidosAnalitico"));
		parametros.put("CIDADE", getCidadeParametrizada(request));
		parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
		parametros.put("NOME_USUARIO", usuario.getNomeUsuario());
		parametros.put("dataInicio", servicoDTO.getDataInicio());
		parametros.put("dataFim", servicoDTO.getDataFim());
		parametros.put("Logo", LogoRel.getFile());
		parametros.put("SUBREPORT_DIR", caminhoSubRelatorioJasper);
		parametros.put("TotalRegistros", listaQuantidadeServicoAnalitico.size());
		parametros.put("TotalSolicitacoes", setSolicitacao.size());
		parametros.put("TotalServicos", setServico.size());

		ContratoDTO contratoDto = new ContratoDTO();
		if (servicoDTO.getIdContrato() != null) {
			contratoDto.setIdContrato(servicoDTO.getIdContrato());
			contratoDto = (ContratoDTO) contratoService.restore(contratoDto);
		}
		parametros.put("contrato", contratoDto.getNumero());

		TipoDemandaServicoService tipoDemandaService = (TipoDemandaServicoService) ServiceLocator.getInstance().getService(TipoDemandaServicoService.class, null);
		TipoDemandaServicoDTO tipoDemandaServicoDto = new TipoDemandaServicoDTO();
		
		if (servicoDTO.getIdTipoDemandaServico() != null) {
			tipoDemandaServicoDto.setIdTipoDemandaServico(servicoDTO.getIdTipoDemandaServico());
			tipoDemandaServicoDto = (TipoDemandaServicoDTO) tipoDemandaService.restore(tipoDemandaServicoDto);
		}
		parametros.put("nomeTipoDemandaServico", tipoDemandaServicoDto.getNomeTipoDemandaServico());

		if (listaDadosRelatorio.size() == 0) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		try
		{
			final JRDataSource dataSource = new JRBeanCollectionDataSource(listaDadosRelatorio);
			final JasperDesign desenho = JRXmlLoader.load(CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioQuantitativoServicoAnaliticoXls.jrxml");
			desenho.setLanguage("java");
			final JasperReport relatorio = JasperCompileManager.compileReport(desenho);
			final JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, dataSource);
			final JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(impressao));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(diretorioReceita + "/RelatorioQuantitativoServicoAnaliticoXls" + strCompl+ "_" + usuario.getIdUsuario() + ".xlsx"));
            XlsxReportConfiguration xlsxReportConfiguration = new SimpleXlsxReportConfiguration();
            exporter.setConfiguration(xlsxReportConfiguration);
			exporter.exportReport();
			document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + diretorioRelativoOS
					+ "/RelatorioQuantitativoServicoAnaliticoXls" + strCompl + "_" + usuario.getIdUsuario() + ".xlsx')");
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
		} catch(OutOfMemoryError e) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.erro.erroServidor"));
		}
	}
}
