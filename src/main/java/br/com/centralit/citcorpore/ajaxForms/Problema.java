/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.htmlparser.jericho.Source;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import br.com.centralit.bpm.dto.TarefaFluxoDTO;
import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citajax.html.HTMLTable;
import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;
import br.com.centralit.citcorpore.bean.CalendarioDTO;
import br.com.centralit.citcorpore.bean.CategoriaOcorrenciaDTO;
import br.com.centralit.citcorpore.bean.CategoriaProblemaDTO;
import br.com.centralit.citcorpore.bean.CausaIncidenteDTO;
import br.com.centralit.citcorpore.bean.ConhecimentoProblemaDTO;
import br.com.centralit.citcorpore.bean.EmailSolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.bean.ItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.JustificativaProblemaDTO;
import br.com.centralit.citcorpore.bean.LocalidadeDTO;
import br.com.centralit.citcorpore.bean.LocalidadeUnidadeDTO;
import br.com.centralit.citcorpore.bean.OcorrenciaProblemaDTO;
import br.com.centralit.citcorpore.bean.OrigemAtendimentoDTO;
import br.com.centralit.citcorpore.bean.PastaDTO;
import br.com.centralit.citcorpore.bean.ProblemaDTO;
import br.com.centralit.citcorpore.bean.ProblemaItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.ProblemaMudancaDTO;
import br.com.centralit.citcorpore.bean.ProblemaRelacionadoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.ServicoContratoDTO;
import br.com.centralit.citcorpore.bean.ServicoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoProblemaDTO;
import br.com.centralit.citcorpore.bean.SolucaoContornoDTO;
import br.com.centralit.citcorpore.bean.SolucaoDefinitivaDTO;
import br.com.centralit.citcorpore.bean.TemplateSolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.UnidadeDTO;
import br.com.centralit.citcorpore.bean.UnidadesAccServicosDTO;
import br.com.centralit.citcorpore.bean.UploadDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.OcorrenciaProblemaDAO;
import br.com.centralit.citcorpore.mail.MensagemEmail;
import br.com.centralit.citcorpore.negocio.CalendarioService;
import br.com.centralit.citcorpore.negocio.CategoriaProblemaService;
import br.com.centralit.citcorpore.negocio.CategoriaSolucaoService;
import br.com.centralit.citcorpore.negocio.CausaIncidenteService;
import br.com.centralit.citcorpore.negocio.ConhecimentoProblemaService;
import br.com.centralit.citcorpore.negocio.ContatoProblemaService;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.EmailSolicitacaoServicoService;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.ExecucaoProblemaService;
import br.com.centralit.citcorpore.negocio.GrupoService;
import br.com.centralit.citcorpore.negocio.ItemConfiguracaoService;
import br.com.centralit.citcorpore.negocio.JustificativaProblemaService;
import br.com.centralit.citcorpore.negocio.LocalidadeService;
import br.com.centralit.citcorpore.negocio.LocalidadeUnidadeService;
import br.com.centralit.citcorpore.negocio.OcorrenciaProblemaService;
import br.com.centralit.citcorpore.negocio.OrigemAtendimentoService;
import br.com.centralit.citcorpore.negocio.PastaService;
import br.com.centralit.citcorpore.negocio.ProblemaItemConfiguracaoService;
import br.com.centralit.citcorpore.negocio.ProblemaMudancaService;
import br.com.centralit.citcorpore.negocio.ProblemaService;
import br.com.centralit.citcorpore.negocio.RequisicaoMudancaService;
import br.com.centralit.citcorpore.negocio.ServicoContratoService;
import br.com.centralit.citcorpore.negocio.ServicoService;
import br.com.centralit.citcorpore.negocio.SolicitacaoServicoProblemaService;
import br.com.centralit.citcorpore.negocio.SolicitacaoServicoService;
import br.com.centralit.citcorpore.negocio.SolucaoContornoService;
import br.com.centralit.citcorpore.negocio.SolucaoDefinitivaService;
import br.com.centralit.citcorpore.negocio.TemplateSolicitacaoServicoService;
import br.com.centralit.citcorpore.negocio.UnidadeService;
import br.com.centralit.citcorpore.negocio.UnidadesAccServicosService;
import br.com.centralit.citcorpore.negocio.UsuarioService;
import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.Enumerados.SituacaoRequisicaoProblema;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.centralit.citcorpore.util.Enumerados.TipoOrigemLeituraEmail;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citged.bean.ControleGEDDTO;
import br.com.centralit.citged.negocio.ControleGEDService;
import br.com.centralit.impressao.ImpressaoCadProblema;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilHTML;
import br.com.citframework.util.UtilI18N;
import br.com.citframework.util.UtilStrings;
import br.com.citframework.util.WebUtil;

/**
 * @author breno.guimaraes
 *
 */
@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
public class Problema extends AjaxFormAction {

	private ProblemaService problemaService;
	private EmpregadoService usuarioService;
	private CategoriaProblemaService categoriaProblemaService;
	private ProblemaItemConfiguracaoService problemaItemConfiguracaoService;
	private ItemConfiguracaoService itemConfiguracaoService;
	private PastaService pastaService;
	private SolicitacaoServicoProblemaService solicitacaoServicoProblemaService;
	private UsuarioDTO usuario;

	private ProblemaMudancaService problemaMudancaService;
	private RequisicaoMudancaService requisicaoMudancaService;

	private ProblemaDTO problemaDto;
	
	String str_nomeFormProblema = "form";
	
	HTMLForm formProblema;

    	public String getStr_nomeFormProblema() {
            return str_nomeFormProblema;
        }
    
        public void setStr_nomeFormProblema(String str_nomeFormProblema) {
            this.str_nomeFormProblema = str_nomeFormProblema;
        }
        
        public HTMLForm getFormProblema() {
            return formProblema;
        }
    
        public void setFormProblema(HTMLForm formProblema) {
            this.formProblema = formProblema;
        }

    @Override
	public Class getBeanClass() {
		return ProblemaDTO.class;
	}

	public ProblemaDTO getProblemaDto() {
		return problemaDto;
	}

	public void setProblemaDto(ProblemaDTO problemaDto) {
		this.problemaDto = problemaDto;
	}

	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		String UNIDADE_AUTOCOMPLETE = ParametroUtil.getValorParametroCitSmartHashMap(br.com.centralit.citcorpore.util.Enumerados.ParametroSistema.UNIDADE_AUTOCOMPLETE, "N");
		StringBuilder objeto;
		if ((UNIDADE_AUTOCOMPLETE != null) && (UNIDADE_AUTOCOMPLETE.equalsIgnoreCase("S"))) {
			objeto = new StringBuilder();
			objeto.append("<select  id='idContrato' name='idContrato' class='Valid[Required] Description[\"contrato.contrato\"]' ");
			objeto.append("onchange='setaValorLookup(this);'>");
			objeto.append("</select>");
			document.getElementById("divContrato").setInnerHTML(objeto.toString());

			objeto = new StringBuilder();
			objeto.append("<input type='text' name='unidadeDes' id='unidadeDes' style='width: 100%;' onkeypress='onkeypressUnidadeDes();'>");
			objeto.append("<input type='hidden' name='idUnidade' id='idUnidade' value='0'/>");
			document.getElementById("divUnidade").setInnerHTML(objeto.toString());
			document.executeScript("montaParametrosAutocompleteUnidade()");
		} else {
			objeto = new StringBuilder();
			objeto.append("<select  id='idContrato' name='idContrato' class='Valid[Required] Description[\"contrato.contrato\"]' ");
			objeto.append("onchange='setaValorLookup(this);' onclick= 'document.form.fireEvent(\"carregaUnidade\");'>");
			objeto.append("</select>");
			document.getElementById("divContrato").setInnerHTML(objeto.toString());

			objeto = new StringBuilder();
			objeto.append("<select name='idUnidade' id = 'idUnidade' onchange='document.form.fireEvent(\"preencherComboLocalidade\")' class='Valid[Required] Description[colaborador.cadastroUnidade]'></select>");
			document.getElementById("divUnidade").setInnerHTML(objeto.toString());
		}

		/**
		 * Adicionado para fazer limpeza da se��o quando for gerenciamento de Servi�o
		 *
		 * @author mario.junior
		 * @since 28/10/2013 08:21
		 */
		request.getSession(true).setAttribute("colUploadRequisicaoProblemaGED", null);
		 this.setProblemaDto((ProblemaDTO) document.getBean());

		document.executeScript("$('#abas').hide()");
		document.executeScript("$('#statusProblema').hide()");
		document.executeScript("$('#divResolvido').hide()");
		// document.executeScript("$('#relacionarErrosConhecidos').hide()");
		// document.executeScript("$('#abaMudancas').hide()");
		// document.executeScript("$('#abaRevisaoProblemaGrave').hide()");
		// document.executeScript("$('#divProblemaGrave').hide()");
		// document.executeScript("$('#divPrecisaMudanca').hide()");
		// document.executeScript("$('#divPrecisaSolucaoContorno').hide()");
		document.executeScript("$('#divBotaoFecharFrame').hide()");
		document.executeScript("$('#statusCancelada').hide()");

		// In�cio do load.
		if ( this.getProblemaDto() == null ||  this.getProblemaDto().getIdProblema() == null) {
			document.getElementById("btOcorrencias").setVisible(false);
		}

		String descricaoSolicitacao = (String) request.getSession().getAttribute("DescricaoSolicitacao");
		request.getSession().removeAttribute("DescricaoSolicitacao");
		String iframeSolicitacao = request.getParameter("solicitacaoServico");

		if (descricaoSolicitacao != null && !descricaoSolicitacao.equalsIgnoreCase("")) {
			document.getElementById("DescricaoAuxliar").setInnerHTML(descricaoSolicitacao);
			document.executeScript("setarDescricao()");
		}

		document.getElementById("enviaEmailCriacao").setDisabled(true);

		document.getElementById("enviaEmailFinalizacao").setDisabled(true);

		usuario = br.com.centralit.citcorpore.util.WebUtil.getUsuario(request);

		if (usuario == null) {

			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));

			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");

			return;
		}

		HTMLSelect idGrupoAtual = document.getSelectById("idGrupo");

		idGrupoAtual.removeAllOptions();

		idGrupoAtual.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));

		GrupoService grupoSegurancaService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);
		Collection colGrupos = grupoSegurancaService.listaGruposAtivos();

		if (colGrupos != null) {
			idGrupoAtual.addOptions(colGrupos, "idGrupo", "nome", null);
		}

		ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
		contratoService.carregarContratos(this.getProblemaDto().getEditar(), usuario, document, request);

			this.carregaServicosMulti(document, request, response);
			if ((UNIDADE_AUTOCOMPLETE != null) && (!UNIDADE_AUTOCOMPLETE.equalsIgnoreCase("S"))) {
				this.carregaUnidade(document, request, response);
			}

		this.alimentaComboCategoriaProblema(request, document);

		this.preencherComboCausa(document, request, response);

		this.preencherComboOrigem(document, request, response);

		// this.preencherComboCalendario(document, request, response);

		if (request.getParameter("chamarTelaProblema") != null && !request.getParameter("chamarTelaProblema").equalsIgnoreCase("")) {
			Integer tarefa = this.obterIdTarefa( this.getProblemaDto(), request);
			if (tarefa > 0) {
				 this.getProblemaDto().setIdTarefa(tarefa);
			}
		}

		String tarefaAssociada = "N";

		if (  this.getProblemaDto() != null &&   this.getProblemaDto().getIdTarefa() != null) {

			tarefaAssociada = "S";

			request.setAttribute("tarefaAssociada", tarefaAssociada);
		}

		if (  this.getProblemaDto() != null &&   this.getProblemaDto().getIdContrato() != null) {
			document.getElementById("idContrato").setValue("" + problemaDto.getIdContrato());
		}

                if (this.getProblemaDto() != null && this.getProblemaDto().getIdProblema() != null) {
                    this.restore(document, request, response);
                    this.preencherComboCategoriaSolucaoMesmoQuandoDeleted(document, request, response);
                    this.atualizaValuesFormProblema(document, request, response);
                } else {
                    this.preencherComboCategoriaSolucao(document, request, response);
                }

		this.carregarInformacaoProblemaAnaliseTendencia(document, request, response);

		document.getElementById("iframeSolicitacao").setValue(iframeSolicitacao);

        if (  this.getProblemaDto() != null
                &&   this.getProblemaDto().getIdProblema() != null
                &&   this.getProblemaDto().getIdProblema().intValue() > 0) {
            document.getElementById("btnImprimir").setDisabled(false);
        } else {
            document.getElementById("btnImprimir").setDisabled(true);
	}

         this.setProblemaDto(null);
		document.executeScript("parent.JANELA_AGUARDE_MENU.hide()");
	}

	/**
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 * @author maycon.silva
	 *
	 *         O m�todo carrega as informa��es de um problema caso haja tendencia identifcada no relatorio de Analise de Tend�ncia
	 *
	 */
	private void carregarInformacaoProblemaAnaliseTendencia(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		if (request.getParameter("tendenciaProblema") != null && request.getParameter("tendenciaProblema").equalsIgnoreCase("S")) {
			String tipo = request.getParameter("tipo");

			if (request.getParameter("idContrato") != null && !request.getParameter("idContrato").equalsIgnoreCase("")) {
				this.getProblemaDto().setIdContrato(new Integer(request.getParameter("idContrato")));

				String UNIDADE_AUTOCOMPLETE = ParametroUtil.getValorParametroCitSmartHashMap(br.com.centralit.citcorpore.util.Enumerados.ParametroSistema.UNIDADE_AUTOCOMPLETE, "N");
				if ((UNIDADE_AUTOCOMPLETE != null) && (!UNIDADE_AUTOCOMPLETE.equalsIgnoreCase("S"))) {
					this.carregaUnidade(document, request, response);
				}

				document.executeScript("setaValorLookup('" + this.getProblemaDto().getIdContrato() + "');");
			}

			if (tipo != null && !tipo.trim().equalsIgnoreCase("")) {
				if (tipo.equalsIgnoreCase("servico")) {
					this.preencheServicoProblemaByTendenciaProblema(document, request, response);
				}
				if (tipo.equalsIgnoreCase("causa")) {
					this.preencheCausaProblemaByTendenciaProblema(document, request, response);
				}

				if (tipo.equalsIgnoreCase("itemConfiguracao")) {
					this.preencheItemConfiguracaoProblemaByTendenciaProblema(document, request, response);
				}
			}

			HTMLForm form = document.getForm("form");
			form.setValues(  this.getProblemaDto());
		}
	}

	/**
	 * Preenche o servi�o caso seja uma tendencio do tipo servi�o, e seta o servi�o na descri��o do problema
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 * @author maycon.silva
	 *
	 *
	 */
	private void preencheServicoProblemaByTendenciaProblema(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		if (request.getParameter("id") != null && !request.getParameter("id").equalsIgnoreCase("")) {
			ServicoContratoService servicoContratoService = (ServicoContratoService) ServiceLocator.getInstance().getService(ServicoContratoService.class, null);
			ServicoContratoDTO servicoContratoDTO = new ServicoContratoDTO();
			servicoContratoDTO.setIdServicoContrato(new Integer(request.getParameter("id")));
			servicoContratoDTO = servicoContratoService.findByIdServicoContrato(servicoContratoDTO.getIdServicoContrato(), this.getProblemaDto().getIdContrato());
			this.getProblemaDto().setDescricao("Servi�o do Contrato: " + servicoContratoDTO.getNomeServico());
		}
	}

	/**
	 * Preenche a Causa caso seja uma tendencio do tipo Causa, e seta o Causa na descri��o do problema
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 * @author maycon.silva
	 *
	 *
	 */
	private void preencheCausaProblemaByTendenciaProblema(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		if (request.getParameter("id") != null && !request.getParameter("id").equalsIgnoreCase("")) {
			CausaIncidenteService causaIncidenteService = (CausaIncidenteService) ServiceLocator.getInstance().getService(CausaIncidenteService.class, null);
			CausaIncidenteDTO causaIncidenteDTO = new CausaIncidenteDTO();
			causaIncidenteDTO.setIdCausaIncidente(new Integer(request.getParameter("id")));
			causaIncidenteDTO = (CausaIncidenteDTO) causaIncidenteService.restore(causaIncidenteDTO);
			this.getProblemaDto().setDescricao("Causa: " + causaIncidenteDTO.getDescricaoCausa());
			this.getProblemaDto().setIdCausa(new Integer(request.getParameter("id")));
		}
	}

	/**
	 * Preenche o Item configura��o caso seja uma tendencio do tipo Item configura��o, e seta o Item configura��o na descri��o do problema
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 * @author maycon.silva
	 *
	 *
	 */
	private void preencheItemConfiguracaoProblemaByTendenciaProblema(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		if (request.getParameter("id") != null && !request.getParameter("id").equalsIgnoreCase("")) {
			ItemConfiguracaoService itemConfiguracaoService = (ItemConfiguracaoService) ServiceLocator.getInstance().getService(ItemConfiguracaoService.class, null);
			ItemConfiguracaoDTO itemConfiguracaoDto = new ItemConfiguracaoDTO();
			itemConfiguracaoDto = itemConfiguracaoService.restoreByIdItemConfiguracao(new Integer(request.getParameter("id")));
			this.getProblemaDto().setDescricao("Item Configura��o: " + itemConfiguracaoDto.getIdentificacao());
		}
	}

	/**
	 * Carrega combo de Origem do conhecimento
	 *
	 * @param document
	 * @param request
	 * @throws Exception
	 * @author thays.araujo
	 */
	public void preencherComboOrigem(DocumentHTML document, HttpServletRequest request) throws Exception {

		HTMLSelect combo = document.getSelectById("comboOrigem");
		combo.removeAllOptions();

		combo.addOption(BaseConhecimentoDTO.CONHECIMENTO.toString(), UtilI18N.internacionaliza(request, "baseConhecimento.conhecimento"));
		combo.addOption(BaseConhecimentoDTO.EVENTO.toString(), UtilI18N.internacionaliza(request, "justificacaoFalhas.evento"));
		combo.addOption(BaseConhecimentoDTO.MUDANCA.toString(), UtilI18N.internacionaliza(request, "requisicaMudanca.mudanca"));
		combo.addOption(BaseConhecimentoDTO.INCIDENTE.toString(), UtilI18N.internacionaliza(request, "solicitacaoServico.incidente"));
		combo.addOption(BaseConhecimentoDTO.SERVICO.toString(), UtilI18N.internacionaliza(request, "servico.servico"));
		combo.addOption(BaseConhecimentoDTO.PROBLEMA.toString(), UtilI18N.internacionaliza(request, "problema.problema"));
	}

	/**
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author breno.guimaraes
	 */
	public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		/**
		 * Adicionado para fazer limpeza da se��o quando for gerenciamento de Servi�o
		 *
		 * @author mario.junior
		 * @since 28/10/2013 08:21
		 */
		request.getSession(true).setAttribute("colUploadRequisicaoProblemaGED", null);
		this.setProblemaDto((ProblemaDTO) document.getBean());

		UsuarioDTO usuarioDto = br.com.centralit.citcorpore.util.WebUtil.getUsuario(request);

		ProblemaDTO problemaAuxDto = this.getProblemaDto();

		if (request.getParameter("chamarTelaProblema") != null && !request.getParameter("chamarTelaProblema").equalsIgnoreCase("")) {
			this.setProblemaDto(this.getProblemaService(request).restoreAll(problemaDto));
		} else {
			this.setProblemaDto(this.getProblemaService(request).restoreAll(problemaDto.getIdProblema()));
		}

		if (this.getProblemaDto() != null) {
			ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
			contratoService.restauraContrato(this.getProblemaDto().getIdContrato(), usuarioDto, document, request);

		document.executeScript("$('#abas').show()");

		document.executeScript("$('#statusProblema').show()");
		document.executeScript("$('#divProblemaGrave').show()");
		document.executeScript("$('#divPrecisaMudanca').show()");
		document.executeScript("$('#divPrecisaSolucaoContorno').show()");

		this.atualizaGridProblema(document, request, response);

		Integer idTarefa = problemaAuxDto.getIdTarefa();

		String acaoFluxo = problemaAuxDto.getAcaoFluxo();

		String escalar = problemaAuxDto.getEscalar();

		String alterarSituacao = problemaAuxDto.getAlterarSituacao();

		String fase = problemaAuxDto.getFase();

		String editar = problemaAuxDto.getEditar();

		ContatoProblemaService contatoPloblemaService = (ContatoProblemaService) ServiceLocator.getInstance().getService(ContatoProblemaService.class, null);

			if (this.getProblemaDto().getPrecisaSolucaoContorno() != null) {
			// inicio: thiago.monteiro
				if (this.getProblemaDto().getPrecisaSolucaoContorno().equalsIgnoreCase("S")) {

				document.executeScript("$('input[type=radio][name=precisaSolucaoContorno][value=\"S\"]').attr('checked', true)");

			} else {

				document.executeScript("$('input[type=radio][name=precisaSolucaoContorno][value=\"N\"]').attr('checked', true)");

			}
		}

			if (this.getProblemaDto().getPrecisaMudanca() != null) {
				if (this.getProblemaDto().getPrecisaMudanca().equalsIgnoreCase("S")) {
				document.executeScript("$('#abaMudancas').show()");
				document.executeScript("$('input[type=radio][name=precisaMudanca][value=\"S\"]').attr('checked', true)");

			} else {

				document.executeScript("$('input[type=radio][name=precisaMudanca][value=\"N\"]').attr('checked', true)");

			}
		}

			if (this.getProblemaDto().getGrave() != null) {
				if (this.getProblemaDto().getGrave().equalsIgnoreCase("S")) {
				document.executeScript("$('#abaRevisaoProblemaGrave').show()");
				document.executeScript("$('input[type=radio][name=grave][value=\"S\"]').attr('checked', true)");

			} else {

				document.executeScript("$('input[type=radio][name=grave][value=\"N\"]').attr('checked', true)");

			}
		}

			if (this.getProblemaDto().getPrecisaSolucaoContorno() != null) {
				if (this.getProblemaDto().getPrecisaSolucaoContorno().equalsIgnoreCase("S")) {

				// Utilizando o evento click para evitar o erro relacionado ao recarregamento do form.
				document.executeScript("$('input[type=radio][name=precisaSolucaoContorno][value=\"S\"]').click()");

			} else {

				document.executeScript("$('input[type=radio][name=precisaSolucaoContorno][value=\"N\"]').click()");

			}
			// fim: thiago.monteiro
		}

		OrigemAtendimentoService origemAtendimentoService = (OrigemAtendimentoService) ServiceLocator.getInstance().getService(OrigemAtendimentoService.class, null);

		OrigemAtendimentoDTO origemAtendimentoDto = new OrigemAtendimentoDTO();

			if (this.getProblemaDto().getIdOrigemAtendimento() != null && this.getProblemaDto().getIdOrigemAtendimento() > 0) {

				origemAtendimentoDto.setIdOrigem(this.getProblemaDto().getIdOrigemAtendimento());

			origemAtendimentoDto = (OrigemAtendimentoDTO) origemAtendimentoService.restore(origemAtendimentoDto);

		}

			atribuirNomeProprietarioECriadorParaRequisicaoDto(this.getProblemaDto());

		atualizaInformacoesRelacionamentos(document);

		this.alimentaComboCategoriaProblema(request, document);

		String UNIDADE_AUTOCOMPLETE = ParametroUtil.getValorParametroCitSmartHashMap(br.com.centralit.citcorpore.util.Enumerados.ParametroSistema.UNIDADE_AUTOCOMPLETE, "N");
		if ((UNIDADE_AUTOCOMPLETE != null) && (UNIDADE_AUTOCOMPLETE.equalsIgnoreCase("S"))) {
			UnidadeService unidadeService = (UnidadeService) ServiceLocator.getInstance().getService(UnidadeService.class, null);
			UnidadeDTO unidadeDTO = new UnidadeDTO();
				unidadeDTO.setIdUnidade(this.getProblemaDto().getIdUnidade());
			unidadeDTO = (UnidadeDTO) unidadeService.restore(unidadeDTO);
			if(unidadeDTO != null)
				problemaDto.setUnidadeDes(unidadeDTO.getNome());
		} else {
				this.restoreComboUnidade(editar,this.getProblemaDto(), document, request, response);
		}

			this.restoreComboLocalidade(this.getProblemaDto(), document, request, response);

			this.restoreComboOrigemAtendimento(this.getProblemaDto(), document, request, response);

		this.preencherComboCausa(document, request, response);

		this.carregaProblemaRelacionado(request, document, problemaAuxDto);

			/**
			 * @author geber.costa verifica se no banco o acompanhamento est� setado como 'N' ou 'S'
			 * */

			if (this.getProblemaDto().getAcompanhamento() == null || (this.getProblemaDto().getAcompanhamento()).equalsIgnoreCase("N") || (this.getProblemaDto().getAcompanhamento()).equalsIgnoreCase("")) {

			document.getElementById("acompanhamento").setValue("N");

			} else if (this.getProblemaDto().getAcompanhamento().equals("S")) {

			document.getElementById("acompanhamento").setValue("S");

		}

			SolucaoContornoDTO solucaoContorno = this.verificaSolucaoContorno(request, response, document, this.getProblemaDto());
			SolucaoDefinitivaDTO solucaoDefinitiva = this.verificaSolucaoDefinitiva(request, response, document, this.getProblemaDto());

		this.getProblemaDto().setIdTarefa(idTarefa);

		this.getProblemaDto().setAcaoFluxo(acaoFluxo);

		this.getProblemaDto().setEscalar(escalar);

		this.getProblemaDto().setAlterarSituacao(alterarSituacao);

		this.getProblemaDto().setFase(fase);

		if (problemaAuxDto.getChamarTelaProblema() != null && problemaAuxDto.getChamarTelaProblema().equalsIgnoreCase("S")) {
				this.getProblemaDto().setChamarTelaProblema(problemaAuxDto.getChamarTelaProblema());
			document.executeScript("$('#divBotoes').hide()");
			document.executeScript("$('#divBotaoFecharFrame').show()");
		}

		if (solucaoContorno != null) {
			this.getProblemaDto().setIdSolucaoContorno(solucaoContorno.getIdSolucaoContorno());
			this.getProblemaDto().setTituloSolCon(solucaoContorno.getTitulo());
			this.getProblemaDto().setDescSolCon(solucaoContorno.getDescricao());
		}
		if (solucaoDefinitiva != null) {
			this.getProblemaDto().setIdSolucaoDefinitiva(solucaoDefinitiva.getIdSolucaoDefinitiva());
			this.getProblemaDto().setTituloSolDefinitiva(solucaoDefinitiva.getTitulo());
			this.getProblemaDto().setDescSolDefinitiva(solucaoDefinitiva.getDescricao());
		}

			verificaUltimaAtualizacao(document, request, this.getProblemaDto().getIdProblema());
			if (this.getProblemaDto().getCausaRaiz() != null && this.getProblemaDto().getSolucaoContorno() != null) {
				if (!this.getProblemaDto().getCausaRaiz().equalsIgnoreCase("") && !this.getProblemaDto().getSolucaoContorno().equalsIgnoreCase("")) {
				document.executeScript("$('#relacionarErrosConhecidos').show()");
			}
		}

			document.setBean(this.getProblemaDto());

		HTMLForm form = document.getForm("form");

		form.clear();

			if (this.getProblemaDto().getStatus() != null && this.getProblemaDto().getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.Registrada.getDescricao())) {
			document.executeScript("$('#rotuloCausaRaiz').removeClass('campoObrigatorio')");
			document.executeScript("$('#rotuloSolucaoContorno').removeClass('campoObrigatorio')");
		}

			if (this.getProblemaDto().getIdContrato() != null) {
			document.getSelectById("idContrato").setDisabled(true);
		}

			if (this.getProblemaDto().getIdCategoriaProblema() != null) {
			document.getSelectById("idCategoriaProblema").setDisabled(true);
		}

			form.setValues(this.getProblemaDto());
		String statusSetado = "";
			if (this.getProblemaDto().getStatus().equalsIgnoreCase("Registrada") || this.getProblemaDto().getStatus().equalsIgnoreCase("Registrado")) {
				statusSetado = "<input type='radio' id='status' name='status' value='" + this.getProblemaDto().getStatus() + "' checked='checked' />"
					+ UtilI18N.internacionaliza(request, "citcorpore.comum.registrada") + "";
			} else if (this.getProblemaDto().getStatus().equalsIgnoreCase("Resolvido")) {
				statusSetado = "<input type='radio' id='status' name='status' value='" + this.getProblemaDto().getStatus() + "' checked='checked' />" + UtilI18N.internacionaliza(request, "problema.resolvido")
						+ "";
			} else if (this.getProblemaDto().getStatus().equalsIgnoreCase("Suspensa")) {
				statusSetado = "<input type='radio' id='status' name='status' value='" + this.getProblemaDto().getStatus() + "' checked='checked' />"
					+ UtilI18N.internacionaliza(request, "solicitacaoServico.situacao.Suspensa") + "";
			} else if (this.getProblemaDto().getStatus().equalsIgnoreCase("Conclu�da")) {
				statusSetado = "<input type='radio' id='status' name='status' value='" + this.getProblemaDto().getStatus() + "' checked='checked' />"
					+ UtilI18N.internacionaliza(request, "citcorpore.comum.concluida") + "";
			} else if (this.getProblemaDto().getStatus().equalsIgnoreCase("Cancelada")) {
				statusSetado = "<input type='radio' id='status' name='status' value='" + this.getProblemaDto().getStatus() + "' checked='checked' />"
					+ UtilI18N.internacionaliza(request, "citcorpore.comum.cancelada") + "";
			} else if (this.getProblemaDto().getStatus().equalsIgnoreCase("Registo de Erro Conhecido")) {
				statusSetado = "<input type='radio' id='status' name='status' value='" + this.getProblemaDto().getStatus() + "' checked='checked' />"
					+ UtilI18N.internacionaliza(request, "citcorpore.comum.cancelada") + "";
		} else {
				statusSetado = "<input type='radio' id='status' name='status' value='" + this.getProblemaDto().getStatus() + "' checked='checked' />" + this.getProblemaDto().getStatus() + "";
		}
		document.getElementById("statusSetado").setInnerHTML(statusSetado);
		document.executeScript("restaurar()");
			document.executeScript("informaNumeroSolicitacao('" + this.getProblemaDto().getIdProblema() + "')");

		if (editar == null || editar.equalsIgnoreCase("")) {
			this.getProblemaDto().setEditar("S");
		} else if (editar.equalsIgnoreCase("N")) {
			document.executeScript("$('#divBarraFerramentas').hide()");
			document.executeScript("$('#divBotoes').hide()");
			document.getForm("form").lockForm();
			} else if (editar.equalsIgnoreCase("RO")) {
				// verifca se � para mostrar
				// a barra de ferramentas,
				// mas com os anexos e
				// ocorr�ncias, apenas para
				// leitura(RO - Read Only).
				// Bot�es tamb�m s�o
				// desabilitados
				document.executeScript("$('#divBarraFerramentas').show()");
				document.executeScript("$('#divBotoes').hide()");
				document.getForm("form").lockForm();
				document.getElementById("btnAdduploadRequisicaoProblema").setDisabled(true);
				document.getElementById("file_uploadRequisicaoProblema").setDisabled(true);
				document.executeScript("$('#fraUpload_uploadRequisicaoProblema').attr('src', '/citsmart/pages/uploadRequisicaoProblema/uploadRequisicaoProblema.load?editar=RO')");
				document.executeScript("$('#abaIC').remove()");
				document.executeScript("$('#abaMudanca').remove()");
				document.executeScript("$('#abaSolicitacao').remove()");
				document.executeScript("$('#abaSolucaoRetorno').remove()");
				document.executeScript("$('#abaSolucaoDefinitiva').remove()");
				document.executeScript("$('#divBaseConhecimento').remove()");
				document.executeScript("$('#abaProblemaRelacionado').remove()");
				document.executeScript("$('#ocorrenciaAba2').remove()");
				document.executeScript("$('#tblSolicitacaoServico').find('img').remove()");
				document.executeScript("$('#tblICs').find('img').remove()");
				document.executeScript("$('#tblRDM').find('img').remove()");
				document.executeScript("$('#tblErrosConhecidos').find('#btEditBaseConhecimento').remove()");
				document.executeScript("$('#tblProblema').find('img').remove()");
				document.executeScript("$('#categoria').find('#btAbreCadastroCategoriaProblema').remove()");
				document.executeScript("$('#valorEditarFormEmail').val('RO')");
		}

			document.executeScript("parent.JANELA_AGUARDE_MENU.hide()");

			/*
			 * geber.costa M�todo listInfoRegExecucaoProblema verifica se o id do problema � v�lido , caso sim ele traz a lista de ocorrencias de problemas
			 */

		if (this.listInfoRegExecucaoProblema(this.getProblemaDto(), request) != null) {

				document.getElementById("tblOcorrencias").setInnerHTML(listInfoRegExecucaoProblema(this.getProblemaDto(), request));

		}

		// if(problemaAuxDto.getChamarTelaProblema() == null || problemaAuxDto.getChamarTelaProblema().equalsIgnoreCase("")){
		// Verificando se o prazo para contornar/solucionar o problema expirou.
		// notificarPrazoSolucionarProblemaExpirou(document, request, response,usuarioDto);
		// }

			this.carregaInformacoesComplementares(document, request, this.getProblemaDto());

		/**
		 * Adicionado para restaurar anexo
		 *
		 * @author mario.junior
		 * @since 31/10/2013 08:21
		 */
		ControleGEDService controleGedService = (ControleGEDService) ServiceLocator.getInstance().getService(ControleGEDService.class, null);
			Collection colAnexos = controleGedService.listByIdTabelaAndID(ControleGEDDTO.TABELA_PROBLEMA, this.getProblemaDto().getIdProblema());
		Collection colAnexosUploadDTO = controleGedService.convertListControleGEDToUploadDTO(colAnexos);
		request.getSession(true).setAttribute("colUploadRequisicaoProblemaGED", colAnexosUploadDTO);
		request.getSession().setAttribute("colUploadRequisicaoProblemaGED", colAnexosUploadDTO);

		GrupoService grupoService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);
		Collection<GrupoDTO> lstGrupos = grupoService.getGruposByEmpregado(usuarioDto.getIdEmpregado());

		if (lstGrupos != null) {
			for (GrupoDTO g : lstGrupos) {
					if (this.getProblemaService(request).verificaPermissaoGrupoCancelar(this.getProblemaDto().getIdCategoriaProblema(), g.getIdGrupo())) {
					document.executeScript("$('#statusCancelada').show()");
					break;
				}
			}
		}
		}

	}

	private void carregaProblemaRelacionado(HttpServletRequest request, DocumentHTML document, ProblemaDTO problemaDto) throws Exception {
		HTMLTable tblProblmea = document.getTableById("tblProblema");
		tblProblmea.deleteAllRows();
		ProblemaService problemaservice = (ProblemaService) ServiceLocator.getInstance().getService(ProblemaService.class, null);
		if (problemaDto != null) {
			ProblemaRelacionadoDTO problemaRelacionadoDTO = new ProblemaRelacionadoDTO();
			problemaRelacionadoDTO.setIdProblema(problemaDto.getIdProblema());
			Collection col = problemaservice.findByProblemaRelacionado(problemaRelacionadoDTO);
			if (col != null) {
				tblProblmea.addRowsByCollection(col, new String[] { "idProblema", "titulo", "status", " " }, new String[] { "idProblema" }, "Problema j� cadastrado!!",
						new String[] { "exibeIconesProblema" }, null, null);
				document.executeScript("HTMLUtils.applyStyleClassInAllCells('tblProblema', 'tblProblema');");
			}
		}

	}

	/**
	 * Popula combo categoria hierarquicamente.
	 *
	 * @param document
	 * @throws Exception
	 */
	private void alimentaComboCategoriaProblema(HttpServletRequest request, DocumentHTML document) throws Exception {
		HTMLSelect combo = document.getSelectById("idCategoriaProblema");
		inicializaCombo(request, combo);

		CategoriaProblemaService categoriaProblemaService = (CategoriaProblemaService) ServiceLocator.getInstance().getService(CategoriaProblemaService.class, null);

		ArrayList<CategoriaProblemaDTO> listaCategoriaAux = (ArrayList<CategoriaProblemaDTO>) categoriaProblemaService.getAtivos();
		if (listaCategoriaAux != null) {
			for (CategoriaProblemaDTO categoriaProblemaDto : listaCategoriaAux) {
				combo.addOption(categoriaProblemaDto.getIdCategoriaProblema().toString(), StringEscapeUtils.escapeJavaScript(categoriaProblemaDto.getNomeCategoria()));

			}
		}

	}

	private void inicializaCombo(HttpServletRequest request, HTMLSelect componenteCombo) {
		componenteCombo.removeAllOptions();
		componenteCombo.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
	}

	/**
	 * Centraliza atualiza��o de informa��es dos objetos que se relacionam com a mudan�a.
	 *
	 * @throws ServiceException
	 * @throws Exception
	 */
	private void atualizaInformacoesRelacionamentos(DocumentHTML document) throws ServiceException, Exception {
		// informa��es dos ics relacionados

		SolicitacaoServicoService solicitacaoServicoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);
		ArrayList<ProblemaItemConfiguracaoDTO> listaICsRelacionados = (ArrayList<ProblemaItemConfiguracaoDTO>) getProblemaItemConfiguracaoService().findByIdProblema(problemaDto.getIdProblema());
		if (listaICsRelacionados != null) {
			ItemConfiguracaoDTO itemConfiguracaoDTO = new ItemConfiguracaoDTO();
			for (ProblemaItemConfiguracaoDTO problemaItemConfiguracaoDTO : listaICsRelacionados) {
				itemConfiguracaoDTO = getItemConfiguracaoService().restoreByIdItemConfiguracao(problemaItemConfiguracaoDTO.getIdItemConfiguracao());
				if (itemConfiguracaoDTO != null) {
					problemaItemConfiguracaoDTO.setNomeItemConfiguracao(itemConfiguracaoDTO.getIdentificacao());
				}

			}
			problemaDto.setItensConfiguracaoRelacionadosSerializado(WebUtil.serializeObjects(listaICsRelacionados, document.getLanguage()));
		}

		ArrayList<SolicitacaoServicoProblemaDTO> listaSolicitacaoServico = (ArrayList<SolicitacaoServicoProblemaDTO>) getSolicitacaoServicoProblemaService().findByIdProblema(
				problemaDto.getIdProblema());

		if (listaSolicitacaoServico != null && listaSolicitacaoServico.size() > 0) {
			for (SolicitacaoServicoProblemaDTO solicitacaoServicoProblemaDto : listaSolicitacaoServico) {
				SolicitacaoServicoDTO solicitacaoServicoDto = new SolicitacaoServicoDTO();
				if (solicitacaoServicoProblemaDto.getIdSolicitacaoServico() != null) {
					solicitacaoServicoDto = solicitacaoServicoService.restoreAll(solicitacaoServicoProblemaDto.getIdSolicitacaoServico());

				}

				if (solicitacaoServicoDto != null) {
					solicitacaoServicoProblemaDto.setNomeServico(solicitacaoServicoDto.getNomeServico());
				}

			}

			problemaDto.setSolicitacaoServicoSerializado(WebUtil.serializeObjects(listaSolicitacaoServico, document.getLanguage()));
		}

		ArrayList<ProblemaMudancaDTO> listaRequisicoesMudancasRelacionadas = (ArrayList<ProblemaMudancaDTO>) getProblemaMudancaService().findByIdProblema(problemaDto.getIdProblema());

		if (listaRequisicoesMudancasRelacionadas != null) {
			for (ProblemaMudancaDTO problemaMudanca : listaRequisicoesMudancasRelacionadas) {
				RequisicaoMudancaDTO requisicaoMudancaDto = new RequisicaoMudancaDTO();
				if (problemaMudanca.getIdRequisicaoMudanca() != null) {
					requisicaoMudancaDto = getRequisicaoMudancaService().restoreAll(problemaMudanca.getIdRequisicaoMudanca());
				}

				if (requisicaoMudancaDto != null) {
					problemaMudanca.setIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
					problemaMudanca.setTitulo(requisicaoMudancaDto.getTitulo());
					problemaMudanca.setStatus(requisicaoMudancaDto.getStatus());
				}
			}
			problemaDto.setRequisicaoMudancaSerializado(WebUtil.serializeObjects(listaRequisicoesMudancasRelacionadas, document.getLanguage()));
		}

	}

        /**
         * @param document
         * @param request
         * @param response
         * @throws Exception
         * @author breno.guimaraes
         */
        public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
            document.getJanelaPopupById("JANELA_AGUARDE_MENU").show();
            this.setProblemaDto((ProblemaDTO) document.getBean());
    
            if (this.getProblemaDto() != null) {
                UsuarioDTO usuarioDto = br.com.centralit.citcorpore.util.WebUtil.getUsuario(request);
    
                ArrayList<ProblemaItemConfiguracaoDTO> ics = (ArrayList<ProblemaItemConfiguracaoDTO>) WebUtil.deserializeCollectionFromRequest(ProblemaItemConfiguracaoDTO.class,
                        "itensConfiguracaoRelacionadosSerializado", request);
    
                this.getProblemaDto().setListProblemaItemConfiguracaoDTO(ics);
    
                ArrayList<SolicitacaoServicoDTO> listIdSolicitacaoServico = (ArrayList<SolicitacaoServicoDTO>) WebUtil.deserializeCollectionFromRequest(SolicitacaoServicoDTO.class,
                        "solicitacaoServicoSerializado", request);
    
                this.getProblemaDto().setListIdSolicitacaoServico(listIdSolicitacaoServico);
    
                ArrayList<ProblemaMudancaDTO> rdm = (ArrayList<ProblemaMudancaDTO>) WebUtil.deserializeCollectionFromRequest(ProblemaMudancaDTO.class, "RequisicaoMudancaSerializado", request);
    
                ArrayList<ProblemaDTO> problemaRelacionados = (ArrayList<ProblemaDTO>) WebUtil.deserializeCollectionFromRequest(ProblemaDTO.class, "colItensProblema_Serialize", request);
                if (problemaRelacionados != null) {
                    this.getProblemaDto().setListProblemaRelacionadoDTO(problemaRelacionados);
                }
    
                this.getProblemaDto().setListProblemaMudancaDTO(rdm);
    
                this.getProblemaDto().setUsuarioDto(usuarioDto);
    
                this.getProblemaDto().setEnviaEmailCriacao("S");
    
                this.getProblemaDto().setEnviaEmailFinalizacao("S");
    
                this.getProblemaDto().setEnviaEmailPrazoSolucionarExpirou("S");
    
                ProblemaDTO problemaAuxDto = new ProblemaDTO();
                if (this.getProblemaDto().getIdProblema() != null) {
                    problemaAuxDto.setIdProblema(this.getProblemaDto().getIdProblema());
                    problemaAuxDto = (ProblemaDTO) this.getProblemaService(request).restore(problemaAuxDto);
                    if (problemaAuxDto.getIdContrato() != null) {
                        this.getProblemaDto().setIdContrato(problemaAuxDto.getIdContrato());
                    }
                    if (problemaAuxDto.getIdCategoriaProblema() != null) {
                        this.getProblemaDto().setIdCategoriaProblema(problemaAuxDto.getIdCategoriaProblema());
                    }
                }
    
                this.getProblemaService(request).deserializaInformacoesComplementares(this.getProblemaDto());
    
                /**
                 * Adicionado para salvar anexos
                 *
                 * @author mario.junior
                 * @since 31/10/2013 08:21
                 */
                Collection<UploadDTO> arquivosUpados = (Collection<UploadDTO>) request.getSession(true).getAttribute("colUploadRequisicaoProblemaGED");
                this.getProblemaDto().setColArquivosUpload(arquivosUpados);
    
                if (this.getProblemaDto().getIdProblema() == null) {
    
                    this.setProblemaDto(getProblemaService(request).create(this.getProblemaDto()));
    
                    // Registra o email se tiver sido utilizado
                    EmailSolicitacaoServicoService emailSolicitacaoServicoService = (EmailSolicitacaoServicoService) ServiceLocator.getInstance().getService(EmailSolicitacaoServicoService.class, null);
                    if (this.getProblemaDto() != null && problemaDto.getMessageId() != null && problemaDto.getMessageId().trim().length() > 0) {
                        EmailSolicitacaoServicoDTO emailDto = new EmailSolicitacaoServicoDTO();
                        emailDto.setIdSolicitacao(this.getProblemaDto().getIdProblema());
                        emailDto.setIdMessage(this.getProblemaDto().getMessageId());
                        emailDto.setOrigem(TipoOrigemLeituraEmail.PROBLEMA.toString());
                        emailSolicitacaoServicoService.create(emailDto);
                    }
    
                    String comando = "mostraMensagemInsercao('" + UtilI18N.internacionaliza(request, "MSG05") + ".<br>" + UtilI18N.internacionaliza(request, "problema.numero") + " <b><u>" + this
                            .getProblemaDto().getIdProblema() + "</u></b> " + UtilI18N.internacionaliza(request, "citcorpore.comum.criado") + ".<br><br>" + UtilI18N.internacionaliza(request,
                                    "prioridade.prioridade") + ": " + this.getProblemaDto().getPrioridade() + "' ";
                    comando = comando + ", " + this.getProblemaDto().getIdProblema() + " )";
    
                    document.executeScript("verificaRelacionado(" + this.getProblemaDto().getIdProblema() + ")");
    
                    document.executeScript(comando);
                    document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
                    if (this.getProblemaDto() != null && this.getProblemaDto().getIframeSolicitacao().equalsIgnoreCase("true")) {
                        document.executeScript("parent.inserirProblemaNalista('" + this.getProblemaDto().getIdProblema() + "')");
                    }
    
                    return;
    
                } else {
                    /**
                     * @author geber.costa se o problema for criado a aba de revis�o n�o aparecer�, por�m a partir do problema criado pode ser usada a aba de revis�o
                     */
    
                    if (this.getProblemaDto().getAcompanhamento() == null || (this.getProblemaDto().getAcompanhamento()).equals("N")) {
    
                        this.getProblemaDto().setAcompanhamento("N");
    
                    } else {
    
                        this.getProblemaDto().setAcompanhamento("S");
    
                    }
    
                    getProblemaService(request).update(this.getProblemaDto());
    
                    try {
                        // Registra o email se tiver sido utilizado
                        EmailSolicitacaoServicoService emailSolicitacaoServicoService = (EmailSolicitacaoServicoService) ServiceLocator.getInstance().getService(EmailSolicitacaoServicoService.class,
                                null);
                        if (this.getProblemaDto() != null && this.getProblemaDto().getMessageId() != null && this.getProblemaDto().getMessageId().trim().length() > 0) {
                            EmailSolicitacaoServicoDTO emailDto = emailSolicitacaoServicoService.getEmailByIdSolicitacaoAndOrigem(this.getProblemaDto().getIdSolicitacaoServico(),
                                    TipoOrigemLeituraEmail.PROBLEMA.toString());
    
                            if (emailDto != null && emailDto.getIdEmail() != null) {
                                emailDto.setIdMessage(this.getProblemaDto().getMessageId());
                                emailDto.setOrigem(TipoOrigemLeituraEmail.PROBLEMA.toString());
                                emailSolicitacaoServicoService.update(emailDto);
                            } else {
                                emailDto = new EmailSolicitacaoServicoDTO();
                                emailDto.setIdSolicitacao(this.getProblemaDto().getIdProblema());
                                emailDto.setIdMessage(this.getProblemaDto().getMessageId());
                                emailDto.setOrigem(TipoOrigemLeituraEmail.PROBLEMA.toString());
                                emailSolicitacaoServicoService.create(emailDto);
                            }
                        }
    
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
    
                    document.alert(UtilI18N.internacionaliza(request, "MSG06"));
                }
                limparFormularioConsiderandoFCKEditores(document, "form");
                document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
                document.executeScript("fechar('" + this.getProblemaDto().getIdProblema() + "');");
            } else {
                document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
            }
        }

	/**
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author breno.guimaraes
	 */
	public void delete(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		problemaDto = (ProblemaDTO) document.getBean();

		getProblemaService(request).deleteAll(problemaDto);

		if (problemaDto.getIdProblema() != null) {
			limparFormularioConsiderandoFCKEditores(document, "form");
			document.alert(UtilI18N.internacionaliza(request, "MSG07"));
		}
	}

	/**
	 * Atualiza as informa��es de nome de proprietario e nome de solicitante em uma requisicaoMudancaDto, caso haja.
	 *
	 * @param requisicaoMudancaDto
	 * @throws ServiceException
	 * @throws Exception
	 */
	private void atribuirNomeProprietarioECriadorParaRequisicaoDto(ProblemaDTO problemaDto) throws ServiceException, Exception {
		if (problemaDto == null) {
			return;
		}

		Integer idProprietario = problemaDto.getIdProprietario();
		Integer idSolicitante = problemaDto.getIdSolicitante();

		if (idProprietario != null && idSolicitante != null) {

			// problemaDto.setNomeProprietario(usuarioDto.getNomeUsuario());
			problemaDto.setSolicitante(getEmpregadoService().restoreByIdEmpregado(idSolicitante).getNome());
		}
	}

	public void carregaServicosMulti(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProblemaDTO problemaDto = (ProblemaDTO) document.getBean();
		if (problemaDto.getIdContrato() != null && problemaDto.getIdContrato().intValue() == 0) {
		HTMLSelect idServico = document.getSelectById("idServico");
		idServico.removeAllOptions();

		if (problemaDto.getIdContrato() == null) {
			document.alert(UtilI18N.internacionaliza(request, "solicitacaoservico.validacao.contrato"));
			return;
		}

		String controleAccUnidade = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.CONTROLE_ACC_UNIDADE_INC_SOLIC, "N");

		if (!UtilStrings.isNotVazio(controleAccUnidade)) {
			controleAccUnidade = "N";
		}

		EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
		int idUnidade = -999;
		if (controleAccUnidade.trim().equalsIgnoreCase("S")) {
			EmpregadoDTO empregadoDto = new EmpregadoDTO();
			empregadoDto.setIdEmpregado(problemaDto.getIdProprietario());
			if (problemaDto.getIdProprietario() != null) {
				empregadoDto = (EmpregadoDTO) empregadoService.restore(empregadoDto);
				if (empregadoDto != null && empregadoDto.getIdUnidade() != null) {
					idUnidade = empregadoDto.getIdUnidade().intValue();
				}
			}
		}

		ServicoService servicoService = (ServicoService) ServiceLocator.getInstance().getService(ServicoService.class, null);
		UnidadesAccServicosService unidadeAccService = (UnidadesAccServicosService) ServiceLocator.getInstance().getService(UnidadesAccServicosService.class, null);
		idServico.removeAllOptions();
		Collection col = servicoService.findByIdTipoDemandaAndIdContrato(3, problemaDto.getIdContrato(), null);

		int cont = 0;
		Integer idServicoCasoApenas1 = null;
		if (col != null) {
			// this.verificaImpactoUrgencia(document, request, response);
			/* if (col.size() > 1) */

			idServico.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));

			for (Iterator it = col.iterator(); it.hasNext();) {
				ServicoDTO servicoDTO = (ServicoDTO) it.next();
				if (servicoDTO.getDeleted() == null || servicoDTO.getDeleted().equalsIgnoreCase("N")) {
					if (servicoDTO.getIdSituacaoServico().intValue() == 1) { // ATIVO
						if (controleAccUnidade.trim().equalsIgnoreCase("S")) {
							UnidadesAccServicosDTO unidadesAccServicosDTO = new UnidadesAccServicosDTO();
							unidadesAccServicosDTO.setIdServico(servicoDTO.getIdServico());
							unidadesAccServicosDTO.setIdUnidade(idUnidade);
							unidadesAccServicosDTO = (UnidadesAccServicosDTO) unidadeAccService.restore(unidadesAccServicosDTO);
							if (unidadesAccServicosDTO != null) {// Se existe acesso
								idServico.addOptionIfNotExists("" + servicoDTO.getIdServico(), servicoDTO.getNomeServico());
								idServicoCasoApenas1 = servicoDTO.getIdServico();
								cont++;
							}
						} else {
							idServico.addOptionIfNotExists("" + servicoDTO.getIdServico(), servicoDTO.getNomeServico());
							idServicoCasoApenas1 = servicoDTO.getIdServico();
							cont++;
						}
					}
				}
			}
			// --- RETITRADO POR EMAURI EM 16/07 - TRATAMENTO DE DELETED --> idServico.addOptions(col, "idServico", "nomeServico", null);
		}
		if (cont == 1) { // Se for apenas um servico encontrado, ja executa o carrega contratos.
			problemaDto.setIdServico(idServicoCasoApenas1);
		}
	}
	}

	/**
	 * form.clear n�o funciona para os FCKEditores. Esta fun��o garante que todo o formul�rio ser� limpado.
	 *
	 * @param document
	 * @param nomeFormulario
	 * @author breno.guimaraes
	 */
	private void limparFormularioConsiderandoFCKEditores(DocumentHTML document, String nomeFormulario) {
		document.executeScript("limpar(document." + nomeFormulario + ")");
	}

	/**
	 * @throws ServiceException
	 * @throws Exception
	 * @author breno.guimaraes
	 */
	private ProblemaService getProblemaService(HttpServletRequest request) throws ServiceException, Exception {

		if (problemaService == null) {
			problemaService = (ProblemaService) ServiceLocator.getInstance().getService(ProblemaService.class, br.com.centralit.citcorpore.util.WebUtil.getUsuarioSistema(request));
		}
		return problemaService;
	}

	/**
	 * @throws ServiceException
	 * @throws Exception
	 * @author thays.araujo
	 */
	private SolicitacaoServicoProblemaService getSolicitacaoServicoProblemaService() throws ServiceException, Exception {
		if (solicitacaoServicoProblemaService == null) {
			solicitacaoServicoProblemaService = (SolicitacaoServicoProblemaService) ServiceLocator.getInstance().getService(SolicitacaoServicoProblemaService.class, null);
		}
		return solicitacaoServicoProblemaService;
	}

	private EmpregadoService getEmpregadoService() throws ServiceException, Exception {
		if (usuarioService == null) {
			usuarioService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
		}
		return usuarioService;
	}

	private CategoriaProblemaService getCategoriaProblemaService() throws ServiceException, Exception {
		if (categoriaProblemaService == null) {
			categoriaProblemaService = (CategoriaProblemaService) ServiceLocator.getInstance().getService(CategoriaProblemaService.class, null);
		}
		return categoriaProblemaService;
	}

	private ProblemaItemConfiguracaoService getProblemaItemConfiguracaoService() throws ServiceException, Exception {
		if (problemaItemConfiguracaoService == null) {
			problemaItemConfiguracaoService = (ProblemaItemConfiguracaoService) ServiceLocator.getInstance().getService(ProblemaItemConfiguracaoService.class, null);
		}
		return problemaItemConfiguracaoService;
	}

	private ProblemaMudancaService getProblemaMudancaService() throws ServiceException, Exception {
		if (problemaMudancaService == null) {
			problemaMudancaService = (ProblemaMudancaService) ServiceLocator.getInstance().getService(ProblemaMudancaService.class, null);
		}
		return problemaMudancaService;
	}

	private ItemConfiguracaoService getItemConfiguracaoService() throws ServiceException, Exception {
		if (itemConfiguracaoService == null) {
			itemConfiguracaoService = (ItemConfiguracaoService) ServiceLocator.getInstance().getService(ItemConfiguracaoService.class, null);
		}
		return itemConfiguracaoService;
	}

	private RequisicaoMudancaService getRequisicaoMudancaService() throws ServiceException, Exception {
		if (requisicaoMudancaService == null) {
			requisicaoMudancaService = (RequisicaoMudancaService) ServiceLocator.getInstance().getService(RequisicaoMudancaService.class, null);
		}
		return requisicaoMudancaService;
	}

	private PastaService getPastaService() throws ServiceException, Exception {
		if (pastaService == null) {
			pastaService = (PastaService) ServiceLocator.getInstance().getService(PastaService.class, null);
		}
		return pastaService;
	}

	public void preencherComboCausa(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		CausaIncidenteService causaIncidenteService = (CausaIncidenteService) ServiceLocator.getInstance().getService(CausaIncidenteService.class, null);
		Collection colCausas = causaIncidenteService.listHierarquia();
		HTMLSelect comboCausa = document.getSelectById("idCausa");

		comboCausa.removeAllOptions();
		comboCausa.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
		if (colCausas != null) {
			comboCausa.addOptions(colCausas, "idCausaIncidente", "descricaoCausaNivel", null);
		}
	}

	public void preencherComboCategoriaSolucao(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		CategoriaSolucaoService categoriaSolucaoService = (CategoriaSolucaoService) ServiceLocator.getInstance().getService(CategoriaSolucaoService.class, null);
		
		Collection colCategSolucao = categoriaSolucaoService.listHierarquia();
		HTMLSelect idCategoriaSolucao = document.getSelectById("idCategoriaSolucao");
		idCategoriaSolucao.removeAllOptions();
		idCategoriaSolucao.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
		if (colCategSolucao != null) {
			idCategoriaSolucao.addOptions(colCategSolucao, "idCategoriaSolucao", "descricaoCategoriaNivel", null);
		}
	}
	
        /**
	 * Quando um "Problema" foi cadastrado, sendo associado a uma "Categoria Solu��o" que foi posteriormente exclu�da l�gicamente, surgia o problema de que n�o era mais trazido a "Categoria Solu��o"
	 * associada ao "Problema". Assim, como solu��o tem-se este outro m�todo de preenchimento do combo de Categoria solu��o, que traz todas as "Categorias Solu��o" cadastradas e v�lidas, seguindo as
	 * mesma regras do m�todo "problema.preencherComboCategoriaSolucao()", al�m de trazer tamb�m apenas uma "Categoria Solu��o" exclu�da, se houver associa��o.
	 * 
	 * @author rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @since 08/05/2015
	 */
        public void preencherComboCategoriaSolucaoMesmoQuandoDeleted(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
    
            CategoriaSolucaoService categoriaSolucaoService = (CategoriaSolucaoService) ServiceLocator.getInstance().getService(CategoriaSolucaoService.class, null);
            Collection colCategSolucao = categoriaSolucaoService.buscaCategoriasSolucoesMesmoQuandoDeleted(this.problemaDto.getIdCategoriaSolucao());
            HTMLSelect idCategoriaSolucao = document.getSelectById("idCategoriaSolucao");
            idCategoriaSolucao.removeAllOptions();
            idCategoriaSolucao.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
            if (colCategSolucao != null) {
                idCategoriaSolucao.addOptions(colCategSolucao, "idCategoriaSolucao", "descricaoCategoriaNivel", null);
            }
        }

	public void restoreColaboradorSolicitante(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProblemaDTO problemaDto = (ProblemaDTO) document.getBean();
		EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);

		EmpregadoDTO empregadoDto = new EmpregadoDTO();
		if (problemaDto.getIdSolicitante() != null) {
			empregadoDto.setIdEmpregado(problemaDto.getIdSolicitante());
			empregadoDto = (EmpregadoDTO) empregadoService.restore(empregadoDto);

			problemaDto.setSolicitante(empregadoDto.getNome());
			problemaDto.setNomeContato(empregadoDto.getNome());
			problemaDto.setTelefoneContato(empregadoDto.getTelefone());
			problemaDto.setRamal(empregadoDto.getRamal());
			problemaDto.setEmailContato(empregadoDto.getEmail().trim());
			problemaDto.setObservacao(empregadoDto.getObservacoes());
			problemaDto.setIdUnidade(empregadoDto.getIdUnidade());
			this.preencherComboLocalidade(document, request, response);
		}

		document.executeScript("$('#POPUP_SOLICITANTE').dialog('close')");

		HTMLForm form = document.getForm("form");
		// form.clear();
		form.setValues(problemaDto);
		form.setValueText("ramal", null, problemaDto.getRamal());
		form.setValueText("telefoneContato", null, problemaDto.getTelefoneContato());
		// form.setValueText("emailContato", null, problemaDto.getEmailContato());
		document.executeScript("fecharPopup(\"#POPUP_EMPREGADO\")");

		problemaDto = null;
	}

	public void preencherComboLocalidade(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProblemaDTO problemaDto = (ProblemaDTO) document.getBean();

		LocalidadeUnidadeService localidadeUnidadeService = (LocalidadeUnidadeService) ServiceLocator.getInstance().getService(LocalidadeUnidadeService.class, null);

		LocalidadeService localidadeService = (LocalidadeService) ServiceLocator.getInstance().getService(LocalidadeService.class, null);

		LocalidadeDTO localidadeDto = new LocalidadeDTO();

		Collection<LocalidadeUnidadeDTO> listaIdlocalidadePorUnidade = null;

		Collection<LocalidadeDTO> listaIdlocalidade = null;

		String TIRAR_VINCULO_LOCALIDADE_UNIDADE = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.TIRAR_VINCULO_LOCALIDADE_UNIDADE, "N");

		HTMLSelect comboLocalidade = document.getSelectById("idLocalidade");
		comboLocalidade.removeAllOptions();
		if (TIRAR_VINCULO_LOCALIDADE_UNIDADE.trim().equalsIgnoreCase("N") || TIRAR_VINCULO_LOCALIDADE_UNIDADE.trim().equalsIgnoreCase("")) {
			if (problemaDto.getIdUnidade() != null) {
				listaIdlocalidadePorUnidade = localidadeUnidadeService.listaIdLocalidades(problemaDto.getIdUnidade());
			}
			if (listaIdlocalidadePorUnidade != null) {
				inicializarComboLocalidade(comboLocalidade, request);
				for (LocalidadeUnidadeDTO localidadeUnidadeDto : listaIdlocalidadePorUnidade) {
					localidadeDto.setIdLocalidade(localidadeUnidadeDto.getIdLocalidade());
					localidadeDto = (LocalidadeDTO) localidadeService.restore(localidadeDto);
					comboLocalidade.addOption(localidadeDto.getIdLocalidade().toString(), localidadeDto.getNomeLocalidade());
				}

			}
		} else {
			listaIdlocalidade = localidadeService.listLocalidade();
			if (listaIdlocalidade != null) {
				inicializarComboLocalidade(comboLocalidade, request);
				for (LocalidadeDTO localidadeDTO : listaIdlocalidade) {
					localidadeDto.setIdLocalidade(localidadeDTO.getIdLocalidade());
					localidadeDto = (LocalidadeDTO) localidadeService.restore(localidadeDto);
					comboLocalidade.addOption(localidadeDto.getIdLocalidade().toString(), localidadeDto.getNomeLocalidade());
				}
			}

		}

	}

	private void inicializarComboLocalidade(HTMLSelect componenteCombo, HttpServletRequest request) {
		componenteCombo.removeAllOptions();
		componenteCombo.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
	}

	public void carregaUnidade(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProblemaDTO problemaDto = (ProblemaDTO) document.getBean();
		String validarComboUnidade = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.UNIDADE_VINC_CONTRATOS, "N");

		if (problemaDto.getIdContrato()!=null){
		UnidadeService unidadeService = (UnidadeService) ServiceLocator.getInstance().getService(UnidadeService.class, null);
		HTMLSelect comboUnidade = document.getSelectById("idUnidade");
		inicializarCombo(comboUnidade, request);
		if (validarComboUnidade.trim().equalsIgnoreCase("S")) {
			Integer idContrato = problemaDto.getIdContrato();
			ArrayList<UnidadeDTO> unidades = (ArrayList) unidadeService.listHierarquiaMultiContratos(idContrato);
			if (unidades != null) {
				for (UnidadeDTO unidade : unidades) {
					if (unidade.getDataFim() == null) {
						comboUnidade.addOption(unidade.getIdUnidade().toString(), StringEscapeUtils.escapeJavaScript(unidade.getNomeNivel()));
					}

				}
			}
		} else {
			ArrayList<UnidadeDTO> unidades = (ArrayList) unidadeService.listHierarquia();
			if (unidades != null) {
				for (UnidadeDTO unidade : unidades) {
					if (unidade.getDataFim() == null) {
						comboUnidade.addOption(unidade.getIdUnidade().toString(), StringEscapeUtils.escapeJavaScript(unidade.getNomeNivel()));
					}
				}
			}
		}
		}

	}

	private void inicializarCombo(HTMLSelect componenteCombo, HttpServletRequest request) {
		componenteCombo.removeAllOptions();
		componenteCombo.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
	}

	public void preencherComboGrupoExecutor(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		document.getSelectById("idGrupo").removeAllOptions();

		GrupoService grupoService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);

		Collection<GrupoDTO> listGrupo = grupoService.listaGruposAtivos();

		document.getSelectById("idGrupo").addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));

		document.getSelectById("idGrupo").addOptions(listGrupo, "idGrupo", "nome", null);

	}

	public void preencherComboOrigem(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		OrigemAtendimentoService origemService = (OrigemAtendimentoService) ServiceLocator.getInstance().getService(OrigemAtendimentoService.class, null);
		HTMLSelect idOrigem = document.getSelectById("idOrigemAtendimento");
		idOrigem.removeAllOptions();
		document.getSelectById("idOrigemAtendimento").addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));

		Collection col = origemService.recuperaAtivos();
		String origemPadrao = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.ORIGEM_PADRAO_SOLICITACAO, "").trim();
		if (col != null) {
			if (origemPadrao != null && !origemPadrao.equals("")) {
				idOrigem.addOptions(col, "idOrigem", "descricao", origemPadrao);
			} else {
				idOrigem.addOptions(col, "idOrigem", "descricao", null);
			}
		}
	}

	/**
	 *
	 * @param problemaDto
	 * @param request
	 * @return
	 * @throws ServiceException
	 * @throws Exception
	 * @author geber.costa retorna uma lista com os registros de execu��o de um problema
	 */

	public String listInfoRegExecucaoProblema(ProblemaDTO problemaDto, HttpServletRequest request) throws ServiceException, Exception {

		JustificativaProblemaService justificativaProblemaService = (JustificativaProblemaService) ServiceLocator.getInstance().getService(JustificativaProblemaService.class, null);

		OcorrenciaProblemaService ocorrenciaProblemaService = (OcorrenciaProblemaService) ServiceLocator.getInstance().getService(OcorrenciaProblemaService.class, null);

		Collection<OcorrenciaProblemaDTO> col = ocorrenciaProblemaService.findByIdProblema(problemaDto.getIdProblema());

		CategoriaOcorrenciaDTO categoriaOcorrenciaDTO = new CategoriaOcorrenciaDTO();

		String strBuffer = "<table class='dynamicTable table table-striped table-bordered table-condensed dataTable' style='table-layout: fixed;'>";
		strBuffer += "<tr>";
		strBuffer += "<th style='width:20%'>";
		strBuffer += UtilI18N.internacionaliza(request, "citcorpore.comum.datahora");
		strBuffer += "</th>";
		strBuffer += "<th>";
		strBuffer += UtilI18N.internacionaliza(request, "solicitacaoServico.informacaoexecucao");
		strBuffer += "</th>";
		strBuffer += "</tr>";

		if (col != null) {

			for (OcorrenciaProblemaDTO ocorrenciaProblemaDto : col) {

				if (ocorrenciaProblemaDto.getOcorrencia() != null) {
					Source source = new Source(ocorrenciaProblemaDto.getOcorrencia());
					ocorrenciaProblemaDto.setOcorrencia(source.getTextExtractor().toString());
				}

				String ocorrencia = UtilStrings.nullToVazio(ocorrenciaProblemaDto.getOcorrencia());
				String descricao = UtilStrings.nullToVazio(ocorrenciaProblemaDto.getDescricao());
				String informacoesContato = UtilStrings.nullToVazio(ocorrenciaProblemaDto.getInformacoesContato());
				ocorrencia = ocorrencia.replaceAll("\"", "");
				descricao = descricao.replaceAll("\"", "");
				informacoesContato = informacoesContato.replaceAll("\"", "");
				ocorrencia = ocorrencia.replaceAll("\n", "<br>");
				descricao = descricao.replaceAll("\n", "<br>");
				informacoesContato = informacoesContato.replaceAll("\n", "<br>");
				ocorrencia = UtilHTML.encodeHTML(ocorrencia.replaceAll("\'", ""));
				descricao = UtilHTML.encodeHTML(descricao.replaceAll("\'", ""));
				informacoesContato = UtilHTML.encodeHTML(informacoesContato.replaceAll("\'", ""));
				strBuffer += "<tr>";
				strBuffer += "<td >";
				strBuffer += "<b>" + UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT, ocorrenciaProblemaDto.getDataregistro(), WebUtil.getLanguage(request)) + " - "
						+ ocorrenciaProblemaDto.getHoraregistro();
				strBuffer += " - </b>" + UtilI18N.internacionaliza(request, "ocorrenciaSolicitacao.registradopor") + ": <b>" + ocorrenciaProblemaDto.getRegistradopor() + "</b>";
				strBuffer += "</td>";
				strBuffer += "<td style='word-wrap: break-word;overflow:hidden;'>";
				strBuffer += "<b>" + ocorrenciaProblemaDto.getDescricao() + "<br><br></b>";
				strBuffer += "<b>" + ocorrencia + "<br><br></b>";

				if (ocorrenciaProblemaDto.getCategoria() != null) {
					if (ocorrenciaProblemaDto.getCategoria().equalsIgnoreCase(Enumerados.CategoriaOcorrencia.Suspensao.toString())
							|| ocorrenciaProblemaDto.getCategoria().equalsIgnoreCase(Enumerados.CategoriaOcorrencia.MudancaSLA.toString())) {
						JustificativaProblemaDTO justificativaSolicitacaoDTO = new JustificativaProblemaDTO();
						if (ocorrenciaProblemaDto.getIdJustificativa() != null) {
							justificativaSolicitacaoDTO.setIdJustificativaProblema(ocorrenciaProblemaDto.getIdJustificativa());
							justificativaSolicitacaoDTO = (JustificativaProblemaDTO) justificativaProblemaService.restore(justificativaSolicitacaoDTO);
							if (justificativaSolicitacaoDTO != null) {

								strBuffer += UtilI18N.internacionaliza(request, "citcorpore.comum.justificativa") + ": <b>" + justificativaSolicitacaoDTO.getDescricaoProblema() + "<br><br></b>";
							}
						}
						if (!UtilStrings.nullToVazio(ocorrenciaProblemaDto.getComplementoJustificativa()).trim().equalsIgnoreCase("")) {
							strBuffer += "<b>" + UtilStrings.nullToVazio(ocorrenciaProblemaDto.getComplementoJustificativa()) + "<br><br></b>";
						}
					}

				}

				if (ocorrenciaProblemaDto.getOcorrencia() != null) {
					if (categoriaOcorrenciaDTO.getNome() != null && !categoriaOcorrenciaDTO.getNome().equals("")) {
						if (categoriaOcorrenciaDTO.getNome().equalsIgnoreCase(Enumerados.CategoriaOcorrencia.Suspensao.toString())
								|| categoriaOcorrenciaDTO.getNome().equalsIgnoreCase(Enumerados.CategoriaOcorrencia.MudancaSLA.toString())) {
							JustificativaProblemaDTO justificativaSolicitacaoDTO = new JustificativaProblemaDTO();
							if (ocorrenciaProblemaDto.getIdJustificativa() != null) {

								justificativaSolicitacaoDTO.setIdJustificativaProblema(ocorrenciaProblemaDto.getIdJustificativa());
								justificativaSolicitacaoDTO = (JustificativaProblemaDTO) justificativaProblemaService.restore(justificativaSolicitacaoDTO);
								if (justificativaSolicitacaoDTO != null) {
									strBuffer += UtilI18N.internacionaliza(request, "citcorpore.comum.justificativa") + ": <b>" + justificativaSolicitacaoDTO.getDescricaoProblema() + "<br><br></b>";
								}
							}
							if (!UtilStrings.nullToVazio(ocorrenciaProblemaDto.getComplementoJustificativa()).trim().equalsIgnoreCase("")) {
								strBuffer += "<b>" + UtilStrings.nullToVazio(ocorrenciaProblemaDto.getComplementoJustificativa()) + "<br><br></b>";
							}
						}
					}
				}

				strBuffer += "</td>";
				strBuffer += "</tr>";
			}
		}
		strBuffer += "</table>";

		return strBuffer;
	}

	public void restoreComboUnidade(String editar, ProblemaDTO problemaDto, DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String UNIDADE_AUTOCOMPLETE = ParametroUtil.getValorParametroCitSmartHashMap(br.com.centralit.citcorpore.util.Enumerados.ParametroSistema.UNIDADE_AUTOCOMPLETE, "N");
		if ((UNIDADE_AUTOCOMPLETE != null) && (!UNIDADE_AUTOCOMPLETE.equalsIgnoreCase("S"))) {
		UnidadeService unidadeService = (UnidadeService) ServiceLocator.getInstance().getService(UnidadeService.class, null);
			HTMLSelect comboUnidade = document.getSelectById("idUnidade");
			if ((editar == null) || (editar.equalsIgnoreCase("") || (editar.equalsIgnoreCase("S")))) {
			String validarComboUnidade = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.UNIDADE_VINC_CONTRATOS, "N");
				if (problemaDto.getIdUnidade() != null) {
					inicializarCombo(comboUnidade, request);
					if (validarComboUnidade.trim().equalsIgnoreCase("S")) {
						if ((problemaDto.getIdContrato() != null) && (problemaDto.getIdContrato().intValue() > 0)) {
							ArrayList<UnidadeDTO> unidades = (ArrayList) unidadeService.listHierarquiaMultiContratos(problemaDto.getIdContrato());
						if (unidades != null) {
							for (UnidadeDTO unidade : unidades) {
								if (unidade.getDataFim() == null) {
									comboUnidade.addOption(unidade.getIdUnidade().toString(), StringEscapeUtils.escapeJavaScript(unidade.getNomeNivel()));
								}
							}
						}
						}
					} else {
						ArrayList<UnidadeDTO> unidades = (ArrayList) unidadeService.listHierarquia();
						if (unidades != null) {
							for (UnidadeDTO unidade : unidades) {
								if (unidade.getDataFim() == null) {
									comboUnidade.addOption(unidade.getIdUnidade().toString(), StringEscapeUtils.escapeJavaScript(unidade.getNomeNivel()));
								}
							}
						}
					}
				}
			} else {
				if (problemaDto.getIdUnidade() != null) {
					comboUnidade.removeAllOptions();
					UnidadeDTO unidadeDTO = new UnidadeDTO();
					unidadeDTO.setIdUnidade(problemaDto.getIdUnidade());
					unidadeDTO = (UnidadeDTO) unidadeService.restore(unidadeDTO);
					if ((unidadeDTO!=null)&&(unidadeDTO.getIdUnidade()!=null)){
						comboUnidade.addOption(unidadeDTO.getIdUnidade().toString(), StringEscapeUtils.escapeJavaScript(unidadeDTO.getNome()));
			}
		}
	}
		}
	}

	public void restoreComboLocalidade(ProblemaDTO ProblemaDto, DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		if (ProblemaDto.getIdProblema() != null && ProblemaDto.getIdProblema().intValue() > 0) {

			String TIRAR_VINCULO_LOCALIDADE_UNIDADE = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.TIRAR_VINCULO_LOCALIDADE_UNIDADE, "N");

			if (ProblemaDto.getIdLocalidade() != null) {

				LocalidadeUnidadeService localidadeUnidadeService = (LocalidadeUnidadeService) ServiceLocator.getInstance().getService(LocalidadeUnidadeService.class, null);
				LocalidadeService localidadeService = (LocalidadeService) ServiceLocator.getInstance().getService(LocalidadeService.class, null);
				LocalidadeDTO localidadeDto = new LocalidadeDTO();
				Collection<LocalidadeUnidadeDTO> listaIdlocalidadePorUnidade = null;
				Collection<LocalidadeDTO> listaIdlocalidade = null;

				HTMLSelect comboLocalidade = document.getSelectById("idLocalidade");
				comboLocalidade.removeAllOptions();
				if (TIRAR_VINCULO_LOCALIDADE_UNIDADE.trim().equalsIgnoreCase("N") || TIRAR_VINCULO_LOCALIDADE_UNIDADE.trim().equalsIgnoreCase("")) {
					if (ProblemaDto.getIdUnidade() != null) {
						listaIdlocalidadePorUnidade = localidadeUnidadeService.listaIdLocalidades(ProblemaDto.getIdUnidade());
					}
					if (listaIdlocalidadePorUnidade != null) {
						inicializarComboLocalidade(comboLocalidade, request);
						for (LocalidadeUnidadeDTO localidadeUnidadeDto : listaIdlocalidadePorUnidade) {
							localidadeDto.setIdLocalidade(localidadeUnidadeDto.getIdLocalidade());
							localidadeDto = (LocalidadeDTO) localidadeService.restore(localidadeDto);
							comboLocalidade.addOption(localidadeDto.getIdLocalidade().toString(), localidadeDto.getNomeLocalidade());
						}

					}
				} else {
					listaIdlocalidade = localidadeService.listLocalidade();
					if (listaIdlocalidade != null) {
						inicializarComboLocalidade(comboLocalidade, request);
						for (LocalidadeDTO localidadeDTO : listaIdlocalidade) {
							localidadeDto.setIdLocalidade(localidadeDTO.getIdLocalidade());
							localidadeDto = (LocalidadeDTO) localidadeService.restore(localidadeDto);
							comboLocalidade.addOption(localidadeDto.getIdLocalidade().toString(), localidadeDto.getNomeLocalidade());
						}
					}
				}
			}
		}
	}

	public void preencherComboOrigemAtendimento(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		HTMLSelect comboOrigem = document.getSelectById("idOrigemAtendimento");

		OrigemAtendimentoService origemAtendimentoService = (OrigemAtendimentoService) ServiceLocator.getInstance().getService(OrigemAtendimentoService.class, null);
		Collection<OrigemAtendimentoDTO> listOrigem = origemAtendimentoService.list();

		comboOrigem.removeAllOptions();
		comboOrigem.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
		if (listOrigem != null) {
			for (OrigemAtendimentoDTO origemDTO : listOrigem) {
				if (origemDTO.getIdOrigem() != null || origemDTO.getIdOrigem() > 0) {
					comboOrigem.addOption(origemDTO.getIdOrigem().toString(), StringEscapeUtils.escapeJavaScript(origemDTO.getDescricao()));
				}
			}
		}

	}

	public void restoreComboOrigemAtendimento(ProblemaDTO problemaDto, DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (problemaDto.getIdProblema() != null && problemaDto.getIdProblema().intValue() > 0) {
			if (problemaDto.getIdOrigemAtendimento() != null && problemaDto.getIdOrigemAtendimento().intValue() > 0) {
				this.preencherComboOrigem(document, request, response);
			}
		}
	}

	/**
	 * Restaura Problemas na Grid.
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author thays.araujo
	 */
	public void atualizaGridProblema(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProblemaDTO problemaDTO = (ProblemaDTO) document.getBean();
		ConhecimentoProblemaService conhecimentoProblemaService = (ConhecimentoProblemaService) ServiceLocator.getInstance().getService(ConhecimentoProblemaService.class, null);

		ConhecimentoProblemaDTO erroConhecido = null;

		if (problemaDTO.getIdProblema() != null) {
			erroConhecido = conhecimentoProblemaService.getErroConhecidoByProblema(problemaDTO.getIdProblema());
		}

		HTMLTable tblErrosConhecidos = document.getTableById("tblErrosConhecidos");
		tblErrosConhecidos.deleteAllRows();

		if (erroConhecido != null) {
			tblErrosConhecidos.addRow(erroConhecido, new String[] { "", "titulo", "status", "" }, null, null, new String[] { "exibeIconesEditarBaseConhecimento" }, null, null);
			document.executeScript("$('#divBaseConhecimento').hide()");
		} else {
			document.executeScript("$('#divBaseConhecimento').show()");
		}

	}

	public void atualizaGridProblemaRelacionados(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProblemaDTO bean = (ProblemaDTO) document.getBean();
		ProblemaService problemaService = (ProblemaService) ServiceLocator.getInstance().getService(ProblemaService.class, null);
		ProblemaDTO problemaDTO = new ProblemaDTO();

		problemaDTO.setIdProblema(bean.getIdProblemaRelacionado());

		problemaDTO = (ProblemaDTO) problemaService.restore(problemaDTO);
		if (problemaDTO == null) {
			return;
		}
		HTMLTable tblProblema = document.getTableById("tblProblema");

		if (problemaDTO.getSequenciaProblema() == null) {
			tblProblema.addRow(problemaDTO, new String[] { "idProblema", "titulo", "status", " " }, new String[] { "idProblema" }, "Problema j� cadastrado!!", new String[] { "exibeIconesProblema" },
					null, null);
		} else {
			tblProblema.updateRow(problemaDTO, new String[] { "idProblema", "titulo", "status", " " }, new String[] { "idProblema" }, "Problema j� cadastrado!!",
					new String[] { "exibeIconesProblema" }, "", null, problemaDTO.getSequenciaProblema());
		}
		document.executeScript("HTMLUtils.applyStyleClassInAllCells('tblProblema', 'tblProblema');");
		document.executeScript("fecharModalProblema();");

		bean = null;
	}

	public void limparTabelas(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HTMLTable tblICs = document.getTableById("tblICs");
		tblICs.deleteAllRows();
		HTMLTable tblRDM = document.getTableById("tblRDM");
		tblRDM.deleteAllRows();
		HTMLTable tblSolicitacaoServico = document.getTableById("tblSolicitacaoServico");
		tblSolicitacaoServico.deleteAllRows();
	}

	/**
	 * preencher comobo de calendario
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author geber.costa
	 */
	public void preencherComboCalendario(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		CalendarioService calendarioService = (CalendarioService) ServiceLocator.getInstance().getService(CalendarioService.class, null);
		HTMLSelect comboCalendario = document.getSelectById("idCalendario");
		Collection<CalendarioDTO> calendarioDTO = calendarioService.list();

		comboCalendario.removeAllOptions();

		if (calendarioDTO != null) {
			for (CalendarioDTO calendario : calendarioDTO) {

				comboCalendario.addOption(calendario.getIdCalendario().toString(), StringEscapeUtils.escapeJavaScript(calendario.getDescricao()));
			}
		}

	}

	/**
	 * M�todo respons�vel pela valida��o do avan�o do fluxo. O fluxo s� ser� v�lido e portanto s� poder� avan�ar caso o problema em quest�o esteja associado a uma base de conhecimento.
	 *
	 * @autor thiago.monterio
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void validacaoAvancaFluxo(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		ProblemaDTO problemaDTO = (ProblemaDTO) document.getBean();

		ProblemaService problemaService = (ProblemaService) ServiceLocator.getInstance().getService(ProblemaService.class, null);

		if (problemaDTO != null && problemaService != null) {
			if (problemaDTO.getStatus() != null && problemaDTO.getStatus().equalsIgnoreCase(SituacaoRequisicaoProblema.RegistroErroConhecido.getDescricao())) {
				boolean avancarFluxo = problemaService.validacaoAvancaFluxo(problemaDTO, null);

				if (!avancarFluxo) {

					document.alert(UtilI18N.internacionaliza(request, "problema.mensagem.necessarioExistirErroConhecido"));

				} else {

					document.executeScript("gravar('form')");

				}
			} else {
				document.executeScript("gravar('form')");
			}

		}
	}

	/**
	 * M�todo respons�vel por enviar mensagens de notifica��o no e-mail do respons�vel (propriet�rio) e de cada um dos usu�rios que fazem parte do grupo executor na resolu��o de um problema.
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @autor thiago.monteiro / thays.araujo
	 */
	public void notificarPrazoSolucionarProblemaExpirou(DocumentHTML document, HttpServletRequest request, HttpServletResponse response, UsuarioDTO usuario) throws Exception {

		EmpregadoDTO empregadoDto = new EmpregadoDTO();

		ProblemaService problemaService = (ProblemaService) ServiceLocator.getInstance().getService(ProblemaService.class, null);

		UsuarioService usuarioService = (UsuarioService) ServiceLocator.getInstance().getService(UsuarioService.class, null);

		EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);

		GrupoService grupoService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);

		String enviarNotificacao = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.NOTIFICAR_RESPONSAVEL_GRUPO_PRAZO_SOLUCAO_CONTORNO_PROBLEMA_EXPIRADO, "S");

		if (enviarNotificacao.equalsIgnoreCase("S")) {

			Collection<ProblemaDTO> listaProblemasComPrazoLimiteContornoExpirado = problemaService.listaProblemasComPrazoLimiteContornoExpirado(usuario);

			if (usuario != null) {
				if (usuario.getIdEmpregado() != null) {
					empregadoDto.setIdEmpregado(usuario.getIdEmpregado());
					empregadoDto = (EmpregadoDTO) empregadoService.restore(empregadoDto);
				}
			}

			if (problemaService != null && usuarioService != null) {

				if (listaProblemasComPrazoLimiteContornoExpirado != null) {

					String ID_MODELO_EMAIL_PRAZO_SOLUCAO_CONTORNO_PROBLEMA_EXPIRADO = ParametroUtil.getValorParametroCitSmartHashMap(
							ParametroSistema.ID_MODELO_EMAIL_PRAZO_SOLUCAO_CONTORNO_PROBLEMA_EXPIRADO, "38");

					Set emailsIntegrantesGrupoExecutor = new HashSet();

					Collection<String> emailsPorGrupo = null;

					for (ProblemaDTO problemaDto : listaProblemasComPrazoLimiteContornoExpirado) {

						if (problemaDto.getIdGrupo() != null) {

							emailsPorGrupo = grupoService.listarEmailsPorGrupo(problemaDto.getIdGrupo());

							if (emailsPorGrupo != null && !emailsPorGrupo.isEmpty()) {

								for (String email : emailsPorGrupo) {
									emailsIntegrantesGrupoExecutor.add(email);
								}

							}

						}
					}

					MensagemEmail mensagemEmail = new MensagemEmail(Integer.parseInt(ID_MODELO_EMAIL_PRAZO_SOLUCAO_CONTORNO_PROBLEMA_EXPIRADO.trim()), new IDto[] { problemaDto });

					mensagemEmail.envia(empregadoDto.getEmail(), StringUtils.remove(StringUtils.remove(emailsIntegrantesGrupoExecutor.toString(), "["), "]"),
							ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_RemetenteNotificacoesSolicitacao, "10"));
				}
			}
		}
	}

	public void gravarSolContorno(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProblemaDTO problemaDto = (ProblemaDTO) document.getBean();
		SolucaoContornoDTO solucaoContornoDto = new SolucaoContornoDTO();
		SolucaoContornoService solucaoContornoService = (SolucaoContornoService) ServiceLocator.getInstance().getService(SolucaoContornoService.class, null);
		ProblemaService problemaService = (ProblemaService) ServiceLocator.getInstance().getService(ProblemaService.class, null);

		if (problemaDto.getIdProblema() != null) {
			solucaoContornoDto.setIdProblema(problemaDto.getIdProblema());

			if (problemaDto.getIdSolucaoContorno() != null && problemaDto.getIdSolucaoContorno().intValue() > 0) {

				solucaoContornoDto.setIdSolucaoContorno(problemaDto.getIdSolucaoContorno());
				solucaoContornoDto = (SolucaoContornoDTO) solucaoContornoService.restore(solucaoContornoDto);

				if (problemaDto.getSolucaoContorno() != null && !problemaDto.getSolucaoContorno().equals("")) {
					solucaoContornoDto.setDescricao(problemaDto.getSolucaoContorno());
				} else {
					document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.descricaoObrigatorio"));
					return;
				}

				if (problemaDto.getTituloSolucaoContorno() != null && !problemaDto.getTituloSolucaoContorno().equals("")) {
					solucaoContornoDto.setTitulo(problemaDto.getTituloSolucaoContorno());
				} else {
					document.alert(UtilI18N.internacionaliza(request, "baseConhecimento.tituloObrigatorio"));
					return;
				}

				solucaoContornoService.update(solucaoContornoDto);
				document.alert(UtilI18N.internacionaliza(request, "MSG06"));

			} else {

				if (problemaDto.getSolucaoContorno() != null && !problemaDto.getSolucaoContorno().equals("")) {
					solucaoContornoDto.setDescricao(problemaDto.getSolucaoContorno());
				} else {
					document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.descricaoObrigatorio"));
					return;
				}

				if (problemaDto.getTituloSolucaoContorno() != null && !problemaDto.getTituloSolucaoContorno().equals("")) {
					solucaoContornoDto.setTitulo(problemaDto.getTituloSolucaoContorno());
				} else {
					document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.tituloObrigatorio"));
					return;
				}
				solucaoContornoDto.setDataHoraCriacao(UtilDatas.getDataHoraAtual());
				solucaoContornoDto = solucaoContornoService.create(solucaoContornoDto);
				document.alert(UtilI18N.internacionaliza(request, "MSG05"));

			}
			problemaService.updateNotNull(problemaDto);
		} else {
			return;
		}
		this.montarTabela(solucaoContornoDto, request, response, document);
		document.executeScript("fecharSolContorno()");

	}

	public SolucaoContornoDTO verificaSolucaoContorno(HttpServletRequest request, HttpServletResponse response, DocumentHTML document, ProblemaDTO problema) throws ServiceException, Exception {
		SolucaoContornoDTO solucaoContorno = new SolucaoContornoDTO();
		SolucaoContornoService solucaoContornoService = (SolucaoContornoService) ServiceLocator.getInstance().getService(SolucaoContornoService.class, null);

		solucaoContorno.setIdProblema(problema.getIdProblema());
		solucaoContorno = solucaoContornoService.findByIdProblema(solucaoContorno);

		if (solucaoContorno != null) {
			this.montarTabela(solucaoContorno, request, response, document);
		}

		return solucaoContorno;
	}

	public void montarTabela(SolucaoContornoDTO solucaoContornoDto, HttpServletRequest request, HttpServletResponse response, DocumentHTML document) throws Exception {
		HTMLTable tblSolContorno = document.getTableById("tblSolContorno");
		tblSolContorno.deleteAllRows();
		if (solucaoContornoDto.getIdSolucaoContorno() != null) {
			solucaoContornoDto.setDataHoraCriacaoStr(UtilDatas.convertDateToString(TipoDate.TIMESTAMP_WITH_SECONDS, solucaoContornoDto.getDataHoraCriacao(), WebUtil.getLanguage(request)));
			tblSolContorno.addRow(solucaoContornoDto, new String[] { "titulo", "dataHoraCriacaoStr", "descricao" }, null, null, null, null, null);
			// document.executeScript("$('#divTblSolContorno').show();");
		}
	}

	public void gravarSolDefinitiva(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProblemaDTO problemaDto = (ProblemaDTO) document.getBean();
		SolucaoDefinitivaDTO solucaoDefinitivaDto = new SolucaoDefinitivaDTO();
		SolucaoDefinitivaService solucaoDefinitivaService = (SolucaoDefinitivaService) ServiceLocator.getInstance().getService(SolucaoDefinitivaService.class, null);

		if (problemaDto.getIdProblema() != null) {
			solucaoDefinitivaDto.setIdProblema(problemaDto.getIdProblema());

			if (problemaDto.getIdSolucaoDefinitiva() != null && problemaDto.getIdSolucaoDefinitiva().intValue() > 0) {

				solucaoDefinitivaDto.setIdSolucaoDefinitiva(problemaDto.getIdSolucaoDefinitiva());
				solucaoDefinitivaDto = (SolucaoDefinitivaDTO) solucaoDefinitivaService.restore(solucaoDefinitivaDto);

				if (problemaDto.getSolucaoDefinitiva() != null && !problemaDto.getSolucaoDefinitiva().equals("")) {
					solucaoDefinitivaDto.setDescricao(problemaDto.getSolucaoDefinitiva());
				} else {
					document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.descricaoObrigatorio"));
					return;
				}

				if (problemaDto.getTituloSolucaoDefinitiva() != null && !problemaDto.getTituloSolucaoDefinitiva().equals("")) {
					solucaoDefinitivaDto.setTitulo(problemaDto.getTituloSolucaoDefinitiva());
				} else {
					document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.tituloObrigatorio"));
					return;
				}

				solucaoDefinitivaService.update(solucaoDefinitivaDto);
				document.alert(UtilI18N.internacionaliza(request, "MSG06"));

			} else {

				if (problemaDto.getSolucaoDefinitiva() != null && !problemaDto.getSolucaoDefinitiva().equals("")) {
					solucaoDefinitivaDto.setDescricao(problemaDto.getSolucaoDefinitiva());
				} else {
					document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.descricaoObrigatorio"));
					return;
				}

				if (problemaDto.getTituloSolucaoDefinitiva() != null && !problemaDto.getTituloSolucaoDefinitiva().equals("")) {
					solucaoDefinitivaDto.setTitulo(problemaDto.getTituloSolucaoDefinitiva());
				} else {
					document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.tituloObrigatorio"));
					return;
				}
				solucaoDefinitivaDto.setDataHoraCriacao(UtilDatas.getDataHoraAtual());
				solucaoDefinitivaDto = solucaoDefinitivaService.create(solucaoDefinitivaDto);
				document.alert(UtilI18N.internacionaliza(request, "MSG05"));

			}
		} else {
			return;
		}

		this.montarTabela(solucaoDefinitivaDto, request, response, document);
		document.executeScript("fecharSolDefinitiva()");

	}

	public SolucaoDefinitivaDTO verificaSolucaoDefinitiva(HttpServletRequest request, HttpServletResponse response, DocumentHTML document, ProblemaDTO problema) throws ServiceException, Exception {
		SolucaoDefinitivaDTO solucaoDefinitiva = new SolucaoDefinitivaDTO();
		SolucaoDefinitivaService solucaoDefinitivaService = (SolucaoDefinitivaService) ServiceLocator.getInstance().getService(SolucaoDefinitivaService.class, null);

		solucaoDefinitiva.setIdProblema(problema.getIdProblema());
		solucaoDefinitiva = solucaoDefinitivaService.findByIdProblema(solucaoDefinitiva);

		if (solucaoDefinitiva != null) {
			this.montarTabela(solucaoDefinitiva, request, response, document);
		}

		return solucaoDefinitiva;
	}

	public void montarTabela(SolucaoDefinitivaDTO solucaoDefinitivaDto, HttpServletRequest request, HttpServletResponse response, DocumentHTML document) throws Exception {
		HTMLTable tblSolDefinitiva = document.getTableById("tblSolDefinitiva");
		tblSolDefinitiva.deleteAllRows();
		if (solucaoDefinitivaDto.getIdSolucaoDefinitiva() != null) {
			solucaoDefinitivaDto.setDataHoraCriacaoStr(UtilDatas.convertDateToString(TipoDate.TIMESTAMP_WITH_SECONDS, solucaoDefinitivaDto.getDataHoraCriacao(), WebUtil.getLanguage(request)));
			tblSolDefinitiva.addRow(solucaoDefinitivaDto, new String[] { "titulo", "dataHoraCriacaoStr", "descricao" }, null, null, null, null, null);
		}
	}

	/**
	 * @author geber.costa
	 * @param document
	 * @param idProblema
	 * @throws Exception
	 *             Traz uma lista para verifica��o de ultima data e hora da ocorr�ncia, esses dados ser�o retornados na p�gina do cliente.
	 */
	public void verificaUltimaAtualizacao(DocumentHTML document, HttpServletRequest request, int idProblema) throws Exception {

		OcorrenciaProblemaDAO ocorrenciaDao = new OcorrenciaProblemaDAO();
		List<OcorrenciaProblemaDTO> lista = (List<OcorrenciaProblemaDTO>) ocorrenciaDao.listByUltimaDataEHora(idProblema);

		Date data = null;
		String hora = "";
		String registradoPor = "";

		if (lista != null) {

			for (OcorrenciaProblemaDTO l : lista) {

				/**
				 *
				 * valida para pegar a ultima data de registro e a ultima hora, por�m ele ir� pegar a partiro do momento que a ocorrencia n�o for nula e vazia ou se ela tiver alguma das descri��es
				 * setadas
				 */

				if (l.getOcorrencia() != null && !l.getOcorrencia().equalsIgnoreCase("") || l.getDescricao().equalsIgnoreCase("Encerramento da Solicita��o")
						|| l.getDescricao().equalsIgnoreCase("Suspens�o da Solicita��o") || l.getDescricao().equalsIgnoreCase("Reativa��o da Solicita��o")
						|| l.getDescricao().equalsIgnoreCase("Agendamento da Atividade") || l.getDescricao().equalsIgnoreCase("Registro de Execu��o")) {

					if (l.getDataregistro() != null) {
						data = l.getDataregistro();
					}

					if (!l.getHoraregistro().equalsIgnoreCase("")) {
						hora = l.getHoraregistro();
					}
					registradoPor = l.getRegistradopor();

				}

			}

			//
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String date = sdf.format(data);

			// String registradoPor = lista.get(lista.size()-1).getRegistradopor();

			document.getElementById("dataHoraUltimaAtualizacao").setInnerHTML(
					"<br/>" + UtilI18N.internacionaliza(request, "carteiraTrabalho.data") + ":<b>&nbsp;" + date + "&nbsp;&nbsp;&nbsp;</b>"
							+ UtilI18N.internacionaliza(request, "carteiraTrabalho.hora") + ":<b>&nbsp;" + hora + "&nbsp;&nbsp;&nbsp;</b>"
							+ UtilI18N.internacionaliza(request, "ocorrenciaProblema.registradopor") + ":<b>&nbsp;" + registradoPor + "&nbsp;&nbsp;</b>");
		}
	}

	public Integer obterIdTarefa(ProblemaDTO problema, HttpServletRequest request) throws ServiceException, Exception {
		int res = 0;
		ExecucaoProblemaService execucaoProblemaService = (ExecucaoProblemaService) ServiceLocator.getInstance().getService(ExecucaoProblemaService.class, null);
		usuario = br.com.centralit.citcorpore.util.WebUtil.getUsuario(request);
		TarefaFluxoDTO tarefaFluxo = execucaoProblemaService.recuperaTarefa(usuario.getLogin(), problema);
		if (tarefaFluxo != null) {
			res = tarefaFluxo.getIdItemTrabalho();
		}
		return res;
	}

	public void carregaInformacoesComplementares(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		carregaInformacoesComplementares(document, request, (ProblemaDTO) document.getBean());
	}

	private void carregaInformacoesComplementares(DocumentHTML document, HttpServletRequest request, ProblemaDTO problemaDto) throws Exception {

		TemplateSolicitacaoServicoService templateService = (TemplateSolicitacaoServicoService) ServiceLocator.getInstance().getService(TemplateSolicitacaoServicoService.class,
				br.com.centralit.citcorpore.util.WebUtil.getUsuarioSistema(request));

		// document.executeScript("exibirInformacoesAprovacao(\"" + getProblemaService(request).getUrlInformacoesComplementares(problemaDto) + "\");");
		document.executeScript("exibirInformacoesComplementares('" + getProblemaService(request).getUrlInformacoesComplementares(problemaDto) + "');");
		// document.executeScript("exibirInformacoesComplementares(localhost:8080/citsmart/pages/\"" + getProblemaService(request).getUrlInformacoesComplementares(problemaDto) + "\");");
		TemplateSolicitacaoServicoDTO templateDto = templateService.recuperaTemplateProblema(problemaDto);

		if (templateDto != null) {
			/*
			 * if (templateDto.getScriptAposRecuperacao() != null) document.executeScript(templateDto.getScriptAposRecuperacao()); if (!templateDto.getHabilitaDirecionamento().equalsIgnoreCase("S"))
			 * document.executeScript("document.getElementById('divGrupoAtual').style.display = 'none';"); if (!templateDto.getHabilitaSituacao().equalsIgnoreCase("S"))
			 * document.executeScript("document.getElementById('divSituacao').style.display = 'none';"); if (!templateDto.getHabilitaSolucao().equalsIgnoreCase("S"))
			 * document.executeScript("document.getElementById('solucao').style.display = 'none';"); if (!templateDto.getHabilitaUrgenciaImpacto().equalsIgnoreCase("S")) {
			 * document.executeScript("document.getElementById('divUrgencia').style.display = 'none';"); document.executeScript("document.getElementById('divImpacto').style.display = 'none';"); } if
			 * (!templateDto.getHabilitaNotificacaoEmail().equalsIgnoreCase("S")) document.executeScript("document.getElementById('divNotificacaoEmail').style.display = 'none';"); if
			 * (!templateDto.getHabilitaProblema().equalsIgnoreCase("S")) document.executeScript("document.getElementById('divProblema').style.display = 'none';"); if
			 * (!templateDto.getHabilitaMudanca().equalsIgnoreCase("S")) document.executeScript("document.getElementById('divMudanca').style.display = 'none';"); if
			 * (!templateDto.getHabilitaItemConfiguracao().equalsIgnoreCase("S")) document.executeScript("document.getElementById('divItemConfiguracao').style.display = 'none';"); if
			 * (!templateDto.getHabilitaSolicitacaoRelacionada().equalsIgnoreCase("S")) document.executeScript("document.getElementById('divSolicitacaoRelacionada').style.display = 'none';"); if
			 * (!templateDto.getHabilitaGravarEContinuar().equalsIgnoreCase("S") && problemaDto.getIdTarefa() != null)
			 * document.executeScript("document.getElementById('btnGravarEContinuar').style.display = 'none';");
			 */
			if (templateDto.getAlturaDiv() != null) {
				document.executeScript("document.getElementById('divInformacoesComplementares').style.height = '" + templateDto.getAlturaDiv().intValue() + "px';");

			}

		}
		document.executeScript("escondeJanelaAguarde()");
	}

	public void restoreImpactoUrgenciaPorCategoriaProblema(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		ProblemaDTO problemaDto = (ProblemaDTO) document.getBean();

		CategoriaProblemaDTO categoriaProblemaDto = new CategoriaProblemaDTO();

		CategoriaProblemaService categoriaProblemaService = (CategoriaProblemaService) ServiceLocator.getInstance().getService(CategoriaProblemaService.class, null);

		if (problemaDto.getIdCategoriaProblema() != null) {
			categoriaProblemaDto.setIdCategoriaProblema(problemaDto.getIdCategoriaProblema());
			categoriaProblemaDto = (CategoriaProblemaDTO) categoriaProblemaService.restore(categoriaProblemaDto);
		}

		if (categoriaProblemaDto != null) {
			problemaDto.setImpacto(categoriaProblemaDto.getImpacto());
			problemaDto.setUrgencia(categoriaProblemaDto.getUrgencia());

		}

		HTMLForm form = document.getForm("form");
		form.setValues(problemaDto);
		document.executeScript("atualizaPrioridade()");
	}

	public void verificarItensRelacionados(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		ProblemaDTO RequisicaoMudancaDTO = (ProblemaDTO) document.getBean();

		ArrayList<SolicitacaoServicoDTO> listIdSolicitacaoServico = (ArrayList<SolicitacaoServicoDTO>) br.com.citframework.util.WebUtil.deserializeCollectionFromRequest(SolicitacaoServicoDTO.class,
				"solicitacaoServicoSerializado", request);
		ArrayList<RequisicaoMudancaItemConfiguracaoDTO> listRequisicaoMudancaItemConfiguracaoDTO = (ArrayList<RequisicaoMudancaItemConfiguracaoDTO>) br.com.citframework.util.WebUtil
				.deserializeCollectionFromRequest(RequisicaoMudancaItemConfiguracaoDTO.class, "itensConfiguracaoRelacionadosSerializado", request);

		boolean existeItensRelaiconados = false;

		if (listIdSolicitacaoServico != null && listIdSolicitacaoServico.size() > 0) {
			existeItensRelaiconados = true;
		} else if (listRequisicaoMudancaItemConfiguracaoDTO != null && listRequisicaoMudancaItemConfiguracaoDTO.size() > 0) {
			existeItensRelaiconados = true;
		}

		if (existeItensRelaiconados) {
			document.executeScript("verificarItensRelacionados(false)");
		} else {
			this.validacaoAvancaFluxo(document, request, response);
		}
	}

	public void verificarParametroAnexos(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		String DISKFILEUPLOAD_REPOSITORYPATH = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.DISKFILEUPLOAD_REPOSITORYPATH, "");
		if (DISKFILEUPLOAD_REPOSITORYPATH == null) {
			DISKFILEUPLOAD_REPOSITORYPATH = "";
		}
		if (DISKFILEUPLOAD_REPOSITORYPATH.equals("")) {
			throw new LogicException(UtilI18N.internacionaliza(request, "citcorpore.comum.anexosUploadSemParametro"));
		}
		File pasta = new File(DISKFILEUPLOAD_REPOSITORYPATH);
		if (!pasta.exists()) {
			throw new LogicException(UtilI18N.internacionaliza(request, "citcorpore.comum.pastaIndicadaNaoExiste"));
		}
	}

	public void restaurarItemConfiguracao(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) {
		problemaDto = (ProblemaDTO) document.getBean();
		ItemConfiguracaoDTO itemConfiguracaoDTO = new ItemConfiguracaoDTO();
		try {

			ItemConfiguracaoService itemConfiguracaoService = (ItemConfiguracaoService) ServiceLocator.getInstance().getService(ItemConfiguracaoService.class, null);
			problemaDto.getHiddenIdItemConfiguracao();
			if (problemaDto != null && problemaDto.getHiddenIdItemConfiguracao() != null && Integer.SIZE > 0) {
				itemConfiguracaoDTO.setIdItemConfiguracao(problemaDto.getHiddenIdItemConfiguracao());
				itemConfiguracaoDTO = (ItemConfiguracaoDTO) itemConfiguracaoService.restore(itemConfiguracaoDTO);
				document.getElementById("hiddenIdItemConfiguracao").setValue(itemConfiguracaoDTO.getIdItemConfiguracao().toString());

				document.executeScript("atualizarTabelaICs('" + itemConfiguracaoDTO.getIdItemConfiguracao() + "','" + itemConfiguracaoDTO.getIdentificacao() + "')");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * M�todo necess�rio para os casos de PoppupManager, pois h� uma fun��o que quando a popup � fechada, � chamado um fireevent para carregar a combo, por�m n�o tinha como acessar o metodo
	 * alimentaComboCategoriaProblema por ser private e era necessario ter um metodo que recebia os parametro (DocumentHTML, HttpServletRequest, HttpServletResponse)
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void alimentaComboCatProblemaAposCadastro(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		this.alimentaComboCategoriaProblema(request, document);
	}
	
    /**
	 * @author euler.ramos Gera a impress�o do cadastro de problema juntamente com suas informa��es complementares.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 */
	public void imprimirCadastroPdf(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) {
		ImpressaoCadProblema impressaoCadProblema;
		try {
			impressaoCadProblema = new ImpressaoCadProblema(document, request);
			impressaoCadProblema.gerarRelatorio();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			impressaoCadProblema = null;
			try {
				document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			} catch (Exception e) {
				e.printStackTrace();
}
		}
	}
	
        /**
         * @author rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
         * @param document
         * @param request
         * @param response
         * @since 08/05/2015
         */
        public void atualizaValuesFormProblema(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) {
            this.setFormProblema(document.getForm(this.getStr_nomeFormProblema()));
            this.formProblema.setValues(this.getProblemaDto());
        }
}
