/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Comparator;

import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;

public class CompararBaseConhecimento implements Comparator<BaseConhecimentoDTO> {

	public int compare(BaseConhecimentoDTO o1, BaseConhecimentoDTO o2) {
		return (((Integer) o1.getContadorCliques()).compareTo((Integer) o2.getContadorCliques()) * -1);
	}

}

class CompararBaseConhecimentoGrauImportancia implements Comparator<BaseConhecimentoDTO> {

	public int compare(BaseConhecimentoDTO baseConhecimento1, BaseConhecimentoDTO baseConhecimento2) {
		
			return (((Integer) baseConhecimento1.getGrauImportancia()).compareTo((Integer) baseConhecimento2.getGrauImportancia()) * -1);
		
	}

}

class CompararBaseConhecimentoMedia implements Comparator<BaseConhecimentoDTO> {

	@Override
	public int compare(BaseConhecimentoDTO baseConhecimento1, BaseConhecimentoDTO baseConhecimento2) {

		return ((baseConhecimento1.getMedia()).compareTo(baseConhecimento2.getMedia()) * -1);
	}

}

class CompararBaseConhecimentoPorVersao implements Comparator<BaseConhecimentoDTO> {

	@Override
	public int compare(BaseConhecimentoDTO baseConhecimento1, BaseConhecimentoDTO baseConhecimento2) {

		return ((baseConhecimento1.getVersao()).compareTo(baseConhecimento2.getVersao()) * -1);
	}
}
