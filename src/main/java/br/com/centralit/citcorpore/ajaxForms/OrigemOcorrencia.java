/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.OrigemOcorrenciaDTO;
import br.com.centralit.citcorpore.negocio.OrigemOcorrenciaService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

/**
 * @author thiago.monteiro
 */

@SuppressWarnings("rawtypes")
public class OrigemOcorrencia extends AjaxFormAction {
	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		document.focusInFirstActivateField(null);
	}

	@Override
	public Class getBeanClass() {
		return OrigemOcorrenciaDTO.class;
	}
	
	/**
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */

	public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		OrigemOcorrenciaDTO origemOcorrenciaDTO = (OrigemOcorrenciaDTO) document.getBean();
		OrigemOcorrenciaService origemOcorrenciaService = (OrigemOcorrenciaService) ServiceLocator.getInstance().getService(OrigemOcorrenciaService.class, null);
		
		// Verifica se o DTO e o servico existem.
		if (origemOcorrenciaDTO != null && origemOcorrenciaService != null) {
			origemOcorrenciaDTO.setDataInicio(UtilDatas.getDataAtual() );
			// Inserir
			if (origemOcorrenciaDTO.getIdOrigemOcorrencia() == null) {			
				origemOcorrenciaService.create(origemOcorrenciaDTO);
				document.alert(UtilI18N.internacionaliza(request, "MSG05") );	
			} else {
				// Atualiar
				origemOcorrenciaService.update(origemOcorrenciaDTO);
				document.alert(UtilI18N.internacionaliza(request, "MSG06") );
			}
			HTMLForm form = document.getForm("formOrigemOcorrencia");
			form.clear();
		}
	}
	
	/**
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */

	public void delete(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		OrigemOcorrenciaDTO origemOcorrenciaDTO = (OrigemOcorrenciaDTO) document.getBean();
		OrigemOcorrenciaService origemOcorrenciaService = (OrigemOcorrenciaService) ServiceLocator.getInstance().
				getService(OrigemOcorrenciaService.class, WebUtil.getUsuarioSistema(request) );
		
		if (origemOcorrenciaDTO != null && origemOcorrenciaService != null) {
			if (origemOcorrenciaDTO.getIdOrigemOcorrencia() != null && (origemOcorrenciaDTO.getIdOrigemOcorrencia().intValue() > 0) ) {
				origemOcorrenciaService.deletarOrigemOcorrencia(origemOcorrenciaDTO, document);
				HTMLForm form = document.getForm("formOrigemOcorrencia");
				form.clear();
			}
		}
	}

	/**
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		OrigemOcorrenciaDTO origemOcorrenciaDTO = (OrigemOcorrenciaDTO) document.getBean();
		OrigemOcorrenciaService origemOcorrenciaService = (OrigemOcorrenciaService) ServiceLocator.getInstance().
				getService(OrigemOcorrenciaService.class, null);
		
		if (origemOcorrenciaDTO != null && origemOcorrenciaService != null) {
			origemOcorrenciaDTO = (OrigemOcorrenciaDTO) origemOcorrenciaService.restore(origemOcorrenciaDTO);		
			HTMLForm form = document.getForm("formOrigemOcorrencia");
			form.clear();
			form.setValues(origemOcorrenciaDTO);
		}		
	}	
}
