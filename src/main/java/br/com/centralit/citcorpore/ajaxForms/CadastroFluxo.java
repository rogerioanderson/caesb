/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.bpm.dto.FluxoDTO;
import br.com.centralit.bpm.servico.FluxoService;
import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;

public class CadastroFluxo extends AjaxFormAction {

    @Override
    public Class<FluxoDTO> getBeanClass() {
        return FluxoDTO.class;
    }

    private FluxoService fluxoService;

    private FluxoService getFluxoService() throws Exception {
        if (fluxoService == null) {
            fluxoService = (FluxoService) ServiceLocator.getInstance().getService(FluxoService.class, null);
        }
        return fluxoService;
    }

    @Override
    public void load(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        final UsuarioDTO usuario = WebUtil.getUsuario(request);
        if (usuario == null) {
            document.alert("Sess�o expirada! Favor efetuar logon novamente!");
            document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
            return;
        }
        final FluxoDTO fluxoDto = (FluxoDTO) document.getBean();
        if (fluxoDto.getIdFluxo() != null) {
            this.restore(document, request, response);
        }
    }

    public void save(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        FluxoDTO fluxoDto = (FluxoDTO) document.getBean();

        String variaveis = fluxoDto.getVariaveis();
        variaveis = variaveis.replaceAll("\n", ";");
        fluxoDto.setVariaveis(variaveis);
        if (fluxoDto.getIdFluxo() == null || fluxoDto.getIdFluxo().intValue() == 0) {
            fluxoDto = (FluxoDTO) this.getFluxoService().create(fluxoDto);
        } else {
            this.getFluxoService().update(fluxoDto);
        }

        document.alert("Registro gravado com sucesso");
        document.executeScript("parent.atualizar(" + fluxoDto.getIdFluxo() + ");");
    }

    public void restore(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        FluxoDTO fluxoDto = (FluxoDTO) document.getBean();
        fluxoDto = (FluxoDTO) this.getFluxoService().restore(fluxoDto);
        if (fluxoDto.getDataFim() != null) {
            fluxoDto = this.getFluxoService().findByTipoFluxo(fluxoDto.getIdTipoFluxo());
        }

        if (fluxoDto != null) {
            if (fluxoDto.getVariaveis() != null) {
                String variaveis = fluxoDto.getVariaveis();
                variaveis = variaveis.replaceAll(";", "\n");
                fluxoDto.setVariaveis(variaveis);
            }
            final HTMLForm form = document.getForm("form");
            form.setValues(fluxoDto);
        }
    }

    public void delete(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        final FluxoDTO fluxoDto = (FluxoDTO) document.getBean();

        if (fluxoDto.getIdFluxo() == null || fluxoDto.getIdFluxo().intValue() == 0) {
            return;
        }

        this.getFluxoService().delete(fluxoDto);
        document.alert("Fluxo exclu�do com sucesso");
        document.executeScript("parent.atualizar(" + fluxoDto.getIdFluxo() + ");");
    }

}
