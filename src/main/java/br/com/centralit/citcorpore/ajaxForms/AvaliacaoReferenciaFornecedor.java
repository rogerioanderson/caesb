/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLTable;
import br.com.centralit.citcorpore.bean.AvaliacaoReferenciaFornecedorDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilI18N;

public class AvaliacaoReferenciaFornecedor extends AjaxFormAction {

	@Override
	public void load(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
	}
	
	
	
	 public void atualizaGridAvaliacao(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		 AvaliacaoReferenciaFornecedorDTO avaliacaoReferenciaFornecedorDTO = (AvaliacaoReferenciaFornecedorDTO) document.getBean();
	        
	        
	        HTMLTable tblAvaliacao = document.getTableById("tblAprovacao");
	        
	        if(avaliacaoReferenciaFornecedorDTO.getIdEmpregado()==null){
	        	document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.campo_obrigatorio"));
	        	return;
	        }
	        
	        if(avaliacaoReferenciaFornecedorDTO.getDecisao()==null){
	            avaliacaoReferenciaFornecedorDTO.setDecisao("S");
	        }
	        
			if (avaliacaoReferenciaFornecedorDTO.getDecisao().equalsIgnoreCase("S")) {
				avaliacaoReferenciaFornecedorDTO.setDecisao("Sim");
			} else {
				avaliacaoReferenciaFornecedorDTO.setDecisao("N�o");
			}
	        
	        if (avaliacaoReferenciaFornecedorDTO.getSequencia() == null){
	        	
	        	tblAvaliacao.addRow(avaliacaoReferenciaFornecedorDTO, 
	                                    new String[] {"", "", "nome" ,"telefone", "observacoes"}, 
	                                    new String[] { "idEmpregado"}, 
	                                    "Empregado j� cadastrado!!", 
	                                    new String[] {"exibeIconesAprovacao"}, 
	                                    null, 
	                                    null);  
	        }else{
	        	
	        	tblAvaliacao.updateRow(avaliacaoReferenciaFornecedorDTO, 
	        							new String[] {"", "", "nome","telefone", "observacoes"}, 
	                                    null, 
	                                    "", 
	                                    new String[] {"exibeIconesAprovacao"}, 
	                                    null, 
	                                    null,
	                                    avaliacaoReferenciaFornecedorDTO.getSequencia());  
	        }
	        document.executeScript("HTMLUtils.applyStyleClassInAllCells('tblAprovacao', 'tblAprovacao');");
	        document.executeScript("fechaAprovacao();");
	    }
	 
		public void preencheEmpregado(DocumentHTML document, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			 AvaliacaoReferenciaFornecedorDTO avaliacaoReferenciaFornecedorDTO = (AvaliacaoReferenciaFornecedorDTO) document.getBean();
			 
			 EmpregadoService empService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
			 EmpregadoDTO empregadoDto = new EmpregadoDTO();
			 empregadoDto.setIdEmpregado(avaliacaoReferenciaFornecedorDTO.getIdEmpregado());
			 empregadoDto = (EmpregadoDTO) empService.restore(empregadoDto);
			 
             document.executeScript("document.formAprovacao.nome.value = '" + empregadoDto.getNome() + "';");
			 document.executeScript("document.formAprovacao.telefone.value = '" + empregadoDto.getTelefone() + "';");
			 document.executeScript("fechaEmpregado()");
		}
	 
	 
	 

	@SuppressWarnings("rawtypes")
	@Override
	public Class getBeanClass() {
		return AvaliacaoReferenciaFornecedorDTO.class;
	}

}
