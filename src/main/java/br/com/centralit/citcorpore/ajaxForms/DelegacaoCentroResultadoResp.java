/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.CentroResultadoDTO;
import br.com.centralit.citcorpore.bean.DelegacaoCentroResultadoDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.ResponsavelCentroResultadoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.CentroResultadoService;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.ResponsavelCentroResultadoService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilI18N;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class DelegacaoCentroResultadoResp extends DelegacaoCentroResultado {

	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}
		
        EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, WebUtil.getUsuarioSistema(request));
		EmpregadoDTO empregadoDto = empregadoService.restoreByIdEmpregado(usuario.getIdEmpregado());
		if (empregadoDto == null) {
			document.alert(UtilI18N.internacionaliza(request, "delegacaoCentroResultado.empregadoNaoEncontrado"));
			return;
		}

		ResponsavelCentroResultadoService responsavelCentroResultadoService = (ResponsavelCentroResultadoService) ServiceLocator.getInstance().getService(ResponsavelCentroResultadoService.class, null);
		Collection<ResponsavelCentroResultadoDTO> colResp = responsavelCentroResultadoService.findByIdResponsavel(empregadoDto.getIdEmpregado());
		if (colResp == null || colResp.isEmpty()) {
			document.alert(UtilI18N.internacionaliza(request, "delegacaoCentroResultado.empregadoNaoPossuiCentroResultado"));
			return;			
		}
		
        DelegacaoCentroResultadoDTO delegacaoCentroResultadoDto = (DelegacaoCentroResultadoDTO) document.getBean();
        delegacaoCentroResultadoDto.setNomeResponsavel(empregadoDto.getNome());
        delegacaoCentroResultadoDto.setIdResponsavel(empregadoDto.getIdEmpregado());
		HTMLForm form = document.getForm("form");
		form.clear();
		form.setValues(delegacaoCentroResultadoDto);
		
		Collection colCCusto = new ArrayList();
		CentroResultadoService centroResultadoService = (CentroResultadoService) ServiceLocator.getInstance().getService(CentroResultadoService.class, WebUtil.getUsuarioSistema(request));
		
		for (ResponsavelCentroResultadoDTO respDto : colResp) {
			CentroResultadoDTO centroResultadoDto = new CentroResultadoDTO();
			centroResultadoDto.setIdCentroResultado(respDto.getIdCentroResultado());
			centroResultadoDto = (CentroResultadoDTO) centroResultadoService.restore(centroResultadoDto);
			if (centroResultadoDto != null)
				colCCusto.add(centroResultadoDto);
		}
		
        HTMLSelect idCentroResultado = (HTMLSelect) document.getSelectById("idCentroResultado");
        idCentroResultado.removeAllOptions();
        idCentroResultado.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
        if(colCCusto != null && !colCCusto.isEmpty())
        	idCentroResultado.addOptions(colCCusto, "idCentroResultado", "nomeHierarquizado", null);

	}

}
