/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.FluxoServicoDTO;
import br.com.centralit.citcorpore.bean.ServicoContratoDTO;
import br.com.centralit.citcorpore.negocio.ServicoContratoService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilI18N;

/**
 * 
 * @author Cledson.junior
 *
 */
public class ServicoContratoUnico extends ServicoContrato {
	
	/**
	 * Inclui registro.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ServicoContratoDTO servicoContratoDTO = (ServicoContratoDTO) document.getBean();
		ServicoContratoService servicoContratoService = (ServicoContratoService) ServiceLocator.getInstance().getService(ServicoContratoService.class, WebUtil.getUsuarioSistema(request));
		List<FluxoServicoDTO> fluxoServicoDTOs = (ArrayList<FluxoServicoDTO>) br.com.citframework.util.WebUtil.deserializeCollectionFromRequest(FluxoServicoDTO.class, "fluxosSerializados", request);
		
		servicoContratoDTO.setListaFluxo((fluxoServicoDTOs == null ? new ArrayList<FluxoServicoDTO>() : fluxoServicoDTOs ));
		
		if (servicoContratoDTO.getIdServicoContrato() == null || servicoContratoDTO.getIdServicoContrato().intValue() == 0) {
			if(servicoContratoService.findByIdServicoContrato(servicoContratoDTO.getIdServico(), servicoContratoDTO.getIdContrato()) != null) {
				document.alert(UtilI18N.internacionaliza(request, "servicoContrato.servicoJaVinculadoContrato"));
				return;
			}
			servicoContratoDTO = (ServicoContratoDTO)servicoContratoService.create(servicoContratoDTO);
			document.alert(UtilI18N.internacionaliza(request, "MSG05"));
		}else {
			servicoContratoService.update(servicoContratoDTO);
			document.alert(UtilI18N.internacionaliza(request, "MSG06"));
		}
		document.executeScript("closePopup(" + servicoContratoDTO.getIdServicoContrato() + ");");
	}
	
	/**
	 * Restaura os dados ao clicar em um registro.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	
	public Class<ServicoContratoDTO> getBeanClass() {
		return ServicoContratoDTO.class;
	}
	
}
