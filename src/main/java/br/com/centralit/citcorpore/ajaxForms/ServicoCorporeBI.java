/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.DeParaCatalogoServicosBIDTO;
import br.com.centralit.citcorpore.bean.ServicoCorporeBIDTO;
import br.com.centralit.citcorpore.negocio.DeParaCatalogoServicosBIService;
import br.com.centralit.citcorpore.negocio.ServicoCorporeBIService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilI18N;

public class ServicoCorporeBI extends AjaxFormAction {

    @Override
    public void load(DocumentHTML document, HttpServletRequest request,	HttpServletResponse response) throws Exception {

    }

    public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse respose) throws Exception {
        ServicoCorporeBIDTO servicoCorporeBIDTO = (ServicoCorporeBIDTO) document.getBean();

        ServicoCorporeBIService service = (ServicoCorporeBIService) ServiceLocator.getInstance().getService(ServicoCorporeBIService.class, null);

        if (servicoCorporeBIDTO.getIdServicoCorpore() == null) {
            servicoCorporeBIDTO = (ServicoCorporeBIDTO) service.create(servicoCorporeBIDTO);
            document.alert(UtilI18N.internacionaliza(request, "servicoCorporeBI.cadastrado"));
        } else {
            service.update(servicoCorporeBIDTO);
            document.alert(UtilI18N.internacionaliza(request, "servicoCorporeBI.atualizado"));
        }

        HTMLForm form = document.getForm("form");
        form.clear();
    }

    public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ServicoCorporeBIDTO servicoCorporeBIDTO = (ServicoCorporeBIDTO) document.getBean();

        ServicoCorporeBIService service = (ServicoCorporeBIService) ServiceLocator.getInstance().getService(ServicoCorporeBIService.class, null);

        servicoCorporeBIDTO = (ServicoCorporeBIDTO) service.restore(servicoCorporeBIDTO);

        HTMLForm form = document.getForm("form");
        form.clear();
        form.setValues(servicoCorporeBIDTO);
    }

    public void excluir(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ServicoCorporeBIDTO servicoCorporeBIDTO = (ServicoCorporeBIDTO) document.getBean();

        ServicoCorporeBIService service = (ServicoCorporeBIService) ServiceLocator.getInstance().getService(ServicoCorporeBIService.class, null);
        DeParaCatalogoServicosBIService deParaCatalogoServicoService = (DeParaCatalogoServicosBIService) ServiceLocator.getInstance().getService(DeParaCatalogoServicosBIService.class, null);

        if (servicoCorporeBIDTO.getIdServicoCorpore() != null) {
            Collection<DeParaCatalogoServicosBIDTO> servicosDePara = deParaCatalogoServicoService.findByidServicoPara(servicoCorporeBIDTO.getIdServicoCorpore(), null);

            if (servicosDePara != null && servicosDePara.size() > 0) {
                document.alert(UtilI18N.internacionaliza(request, "deParaCatalogoServicos.naoFoiPossivelExcluirRelacionado"));
            } else {
                service.delete(servicoCorporeBIDTO);
                document.alert(UtilI18N.internacionaliza(request, "servicoCorporeBI.excluido"));
            }
        } else {
            document.alert(UtilI18N.internacionaliza(request, "deParaCatalogoServicos.naoFoiPossivelExcluir"));
        }

        HTMLForm form = document.getForm("form");

        form.clear();
    }

    @Override
    public Class getBeanClass() {
        return ServicoCorporeBIDTO.class;
    }

}
