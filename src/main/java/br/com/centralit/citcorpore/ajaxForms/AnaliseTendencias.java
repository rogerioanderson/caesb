/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.commons.collections4.CollectionUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citajax.html.HTMLTable;
import br.com.centralit.citcorpore.bean.AnaliseTendenciasDTO;
import br.com.centralit.citcorpore.bean.CausaIncidenteDTO;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.bean.ImpactoDTO;
import br.com.centralit.citcorpore.bean.ItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.ServicoDTO;
import br.com.centralit.citcorpore.bean.TendenciaDTO;
import br.com.centralit.citcorpore.bean.TendenciaGanttDTO;
import br.com.centralit.citcorpore.bean.TipoDemandaServicoDTO;
import br.com.centralit.citcorpore.bean.UrgenciaDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.AnaliseTendenciasService;
import br.com.centralit.citcorpore.negocio.CausaIncidenteService;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.GrupoService;
import br.com.centralit.citcorpore.negocio.ImpactoService;
import br.com.centralit.citcorpore.negocio.ItemConfiguracaoService;
import br.com.centralit.citcorpore.negocio.ServicoService;
import br.com.centralit.citcorpore.negocio.TipoDemandaServicoService;
import br.com.centralit.citcorpore.negocio.UrgenciaService;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.centralit.citcorpore.util.LogoRel;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citcorpore.util.UtilRelatorio;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

@SuppressWarnings({ "rawtypes" })
public class AnaliseTendencias extends AjaxFormAction {

	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		this.carregarComboContratos(document);

		this.carregarComboTipoDemandaServico(document, request);

		this.carregarComboUrgencia(document, request);

		this.carregarComboImpacto(document, request);

		this.carregarComboCausaIncidente(document, request);

		this.carregarComboGrupoExecutor(document, request);
	}

	/**
	 * Carrega Combo Grupo Executor.
	 *
	 * @param document
	 * @param request
	 * @throws Exception
	 * @throws ServiceException
	 * @author valdoilo.damasceno
	 * @since 04.06.2014
	 */
	private void carregarComboGrupoExecutor(DocumentHTML document, HttpServletRequest request) throws Exception, ServiceException {
		document.getSelectById("idGrupoExecutor").removeAllOptions();

		GrupoService grupoServoce = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);

		Collection<GrupoDTO> listGrupoServiceDesk = grupoServoce.listGruposServiceDesk();

		document.getSelectById("idGrupoExecutor").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");

		document.getSelectById("idGrupoExecutor").addOptions(listGrupoServiceDesk, "idGrupo", "nome", null);
	}

	/**
	 * Alterado por rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
         * rcs: Faz-se agora, atrav�s da linha de c�digo "causaIncidenteService.filtraColCausasIncidentesPorDataAtiva(colCausas);" uma filtragem das causas de incidentes.
         * S� ir�o aparecer no combo, as causas que foram registradas com data igual ou anterior a data atual.
         * data de altera��o: 27/04/2015
	 * 
	 * 
	 * Carrega Combo Causa.
	 *
	 * @param document
	 * @param request
	 * @throws ServiceException
	 * @throws Exception
	 * @author valdoilo.damasceno
	 * @since 04.06.2014
	 */
	private void carregarComboCausaIncidente(DocumentHTML document, HttpServletRequest request) throws ServiceException, Exception {

		document.getSelectById("idCausaIncidente").removeAllOptions();

		CausaIncidenteService causaIncidenteService = (CausaIncidenteService) ServiceLocator.getInstance().getService(CausaIncidenteService.class, null);

		Collection<CausaIncidenteDTO> colCausas = causaIncidenteService.listHierarquia();
		
		causaIncidenteService.filtraColCausasIncidentesPorDataAtiva(colCausas);

		HTMLSelect idCausaIncidente = (HTMLSelect) document.getSelectById("idCausaIncidente");

		idCausaIncidente.removeAllOptions();

		idCausaIncidente.addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todas") + " --");

		if (colCausas != null && !colCausas.isEmpty()) {
			idCausaIncidente.addOptions(colCausas, "idCausaIncidente", "descricaoCausaNivel", null);
		}
	}

	/**
	 * Carrega Combo Impacto.
	 *
	 * @param document
	 * @param request
	 * @throws ServiceException
	 * @throws Exception
	 * @throws LogicException
	 * @author valdoilo.damasceno
	 * @since 04.06.2014
	 */
	private void carregarComboImpacto(DocumentHTML document, HttpServletRequest request) throws ServiceException, Exception, LogicException {

		document.getSelectById("impacto").removeAllOptions();

		ImpactoService impactoService = (ImpactoService) ServiceLocator.getInstance().getService(ImpactoService.class, null);

		Collection<ImpactoDTO> listImpacto = impactoService.list();

		HTMLSelect comboImpacto = document.getSelectById("impacto");

		document.getSelectById("impacto").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");

		comboImpacto.addOptions(listImpacto, "idImpacto", "nivelImpacto", null);
	}

	/**
	 * Carrega Combo Urg�ncia.
	 *
	 * @param document
	 * @param request
	 * @throws ServiceException
	 * @throws Exception
	 * @author valdoilo.damasceno
	 * @since 04.06.2014
	 */
	private void carregarComboUrgencia(DocumentHTML document, HttpServletRequest request) throws ServiceException, Exception, LogicException {

		document.getSelectById("urgencia").removeAllOptions();

		UrgenciaService urgenciaService = (UrgenciaService) ServiceLocator.getInstance().getService(UrgenciaService.class, null);

		Collection<UrgenciaDTO> listUrgencia = urgenciaService.list();

		HTMLSelect comboUrgencia = document.getSelectById("urgencia");

		document.getSelectById("urgencia").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todas") + " --");

		comboUrgencia.addOptions(listUrgencia, "idUrgencia", "nivelUrgencia", null);
	}

	/**
	 * Carrega Combo Tipo Demanda Servi�o.
	 *
	 * @param document
	 * @param request
	 * @throws ServiceException
	 * @throws Exception
	 * @author valdoilo.damasceno
	 * @since 04.06.2014
	 */
	private void carregarComboTipoDemandaServico(DocumentHTML document, HttpServletRequest request) throws ServiceException, Exception {

		document.getSelectById("idTipoDemandaServico").removeAllOptions();

		TipoDemandaServicoService tipoDemandaServicoService = (TipoDemandaServicoService) ServiceLocator.getInstance().getService(TipoDemandaServicoService.class, null);

		Collection<TipoDemandaServicoDTO> listTipoDemandaServicoDto = tipoDemandaServicoService.listSolicitacoes();

		HTMLSelect comboTipoDemandaServico = document.getSelectById("idTipoDemandaServico");

		document.getSelectById("idTipoDemandaServico").addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");

		comboTipoDemandaServico.addOptions(listTipoDemandaServicoDto, "idTipoDemandaServico", "nomeTipoDemandaServico", null);
	}

	/**
	 * Carrega Combo de Contratos.
	 *
	 * @param document
	 * @throws ServiceException
	 * @throws Exception
	 * @author valdoilo.damasceno
	 * @since 04.06.2014
	 */
	private void carregarComboContratos(DocumentHTML document) throws ServiceException, Exception {

		document.getSelectById("idContrato").removeAllOptions();

		ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);

		Collection<ContratoDTO> listContratoDto = contratoService.listAtivosWithNomeRazaoSocialCliente();

		HTMLSelect comboContrato = document.getSelectById("idContrato");

		comboContrato.addOptions(listContratoDto, "idContrato", "numeroContratoComNomeRazaoSocial", null);
	}

	public void buscarTendencia (DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		AnaliseTendenciasDTO analiseTendenciasDTO = (AnaliseTendenciasDTO) document.getBean();
		AnaliseTendenciasService analiseTendenciasService = (AnaliseTendenciasService) ServiceLocator.getInstance().getService(AnaliseTendenciasService.class, null);

		Collection<TendenciaDTO> colServico = analiseTendenciasService.buscarTendenciasServico(analiseTendenciasDTO);

		HTMLTable tblServicos = document.getTableById("tblServicos");
		tblServicos.deleteAllRows();
		if(colServico != null && colServico.size() >0){
			if (colServico != null && !colServico.isEmpty() && colServico.size() > 0) {
				tblServicos.addRowsByCollection(colServico, new String[] {"id", "descricao", "qtdeCritica",""}, null, "", new String[] { "exibeTendenciaServico" }, null, null);
			}
		}

		Collection<TendenciaDTO> colCausa = analiseTendenciasService.buscarTendenciasCausa(analiseTendenciasDTO);

		HTMLTable tblCausa = document.getTableById("tblCausa");
		tblCausa.deleteAllRows();
		if(colCausa != null && colCausa.size() >0){
			if (colCausa != null && !colCausa.isEmpty() && colCausa.size() > 0) {
				tblCausa.addRowsByCollection(colCausa, new String[] {"id", "descricao", "qtdeCritica",""}, null, "", new String[] { "exibeTendenciaCausa" }, null, null);
			}
		}

		Collection<TendenciaDTO> colItemConfiguracao = analiseTendenciasService.buscarTendenciasItemConfiguracao(analiseTendenciasDTO);

		HTMLTable tblItemConfiguracao = document.getTableById("tblItemConfiguracao");
		tblItemConfiguracao.deleteAllRows();
		if(colItemConfiguracao != null && colItemConfiguracao.size() >0){
			if (colItemConfiguracao != null && !colItemConfiguracao.isEmpty() && colItemConfiguracao.size() > 0) {
				tblItemConfiguracao.addRowsByCollection(colItemConfiguracao, new String[] {"id", "descricao", "qtdeCritica",""}, null, "", new String[] { "exibeTendenciaItemConfig" }, null, null);
			}
		}

		document.executeScript("JANELA_AGUARDE_MENU.hide();");
		document.executeScript("showResult();");
	}

        /**
         * alterado por rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a> 
         * data altera��o rcs: 24/09/2015
         * 
         * @param document
         * @param request
         * @param response
         * @throws Exception
         */
        public void gerarRelatorio(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
            final AnaliseTendenciasDTO analiseTendenciasDTO = (AnaliseTendenciasDTO) document.getBean();
    
            if (!Objects.equals(analiseTendenciasDTO, null)) {
                List<TendenciaGanttDTO> listTendenciasDto = null;
                AnaliseTendenciasService analiseTendenciaService = (AnaliseTendenciasService) ServiceLocator.getInstance().getService(AnaliseTendenciasService.class, null);
                StringBuilder sbAux_essaVarialPodeReceberVariosValores = new StringBuilder();
    
                if ("servico".equalsIgnoreCase(analiseTendenciasDTO.getTipoRelatorio())) {
                	/*
                	  Euler.Ramos - Incidente:186766 - 04/03/2016
 				      Foi alterado o front-end de forma que o valor do id que se deseja pesquisar se encontra no campo idRelatorio;
 				      estou adicionando ele no campo correto.
                	 */
                	analiseTendenciasDTO.setIdServico(analiseTendenciasDTO.getIdRelatorio());
                	
                	listTendenciasDto = analiseTendenciaService.listarGraficoGanttServico(analiseTendenciasDTO);
                    ServicoService servicoService = (ServicoService) ServiceLocator.getInstance().getService(ServicoService.class, null);
                    ServicoDTO servicoDto = servicoService.findById(analiseTendenciasDTO.getIdRelatorio());
    
                    if (servicoDto != null) {
                        sbAux_essaVarialPodeReceberVariosValores.append(UtilI18N.internacionaliza(request, "citcorpore.comum.servico"));
                        sbAux_essaVarialPodeReceberVariosValores.append(": ");
                        sbAux_essaVarialPodeReceberVariosValores.append(servicoDto.getNomeServico());
                    }
                } else if ("causa".equalsIgnoreCase(analiseTendenciasDTO.getTipoRelatorio())) {
                	/*
              	  	  Euler.Ramos - Incidente:186766 - 04/03/2016
                	 */
                	analiseTendenciasDTO.setIdCausaIncidente(analiseTendenciasDTO.getIdRelatorio());

                	listTendenciasDto = analiseTendenciaService.listarGraficoGanttCausa(analiseTendenciasDTO, analiseTendenciasDTO.getIdRelatorio());
                    CausaIncidenteService causaIncidenteService = (CausaIncidenteService) ServiceLocator.getInstance().getService(CausaIncidenteService.class, null);
                    CausaIncidenteDTO causaIncidenteDto = new CausaIncidenteDTO();
                    causaIncidenteDto.setIdCausaIncidente(analiseTendenciasDTO.getIdRelatorio());
                    causaIncidenteDto = (CausaIncidenteDTO) causaIncidenteService.restore(causaIncidenteDto);
    
                    if (causaIncidenteDto != null) {
                        sbAux_essaVarialPodeReceberVariosValores.append(UtilI18N.internacionaliza(request, "problema.causa"));
                        sbAux_essaVarialPodeReceberVariosValores.append(": ");
                        sbAux_essaVarialPodeReceberVariosValores.append(causaIncidenteDto.getDescricaoCausa());
                    }
                } else if ("itemConfiguracao".equalsIgnoreCase(analiseTendenciasDTO.getTipoRelatorio())) {
                	/*
              	  	  Euler.Ramos - Incidente:186766 - 04/03/2016
				      Foi alterado o front-end de forma que o valor do id que se deseja pesquisar se encontra no campo idRelatorio;
				      estou adicionando ele no campo correto.
                	*/
                	analiseTendenciasDTO.setIdItemConfiguracao(analiseTendenciasDTO.getIdRelatorio());

                	listTendenciasDto = analiseTendenciaService.listarGraficoGanttItemConfiguracao(analiseTendenciasDTO, analiseTendenciasDTO.getIdRelatorio());
                    ItemConfiguracaoService causaIncidenteService = (ItemConfiguracaoService) ServiceLocator.getInstance().getService(ItemConfiguracaoService.class, null);
                    ItemConfiguracaoDTO itemConfiguracaoDto = causaIncidenteService.restoreByIdItemConfiguracao(analiseTendenciasDTO.getIdRelatorio());
    
                    if (itemConfiguracaoDto != null) {
                        sbAux_essaVarialPodeReceberVariosValores.append(UtilI18N.internacionaliza(request, "itemConfiguracao.itemConfiguracao"));
                        sbAux_essaVarialPodeReceberVariosValores.append(": ");
                        sbAux_essaVarialPodeReceberVariosValores.append(itemConfiguracaoDto.getIdentificacao());
                    }
                }
    
                if (CollectionUtils.isNotEmpty(listTendenciasDto)) {
                    final String tituloDoRelatorio = sbAux_essaVarialPodeReceberVariosValores.toString();
                    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
                    UsuarioDTO usuario = WebUtil.getUsuario(request);
                    String dataTendencia = null;
                    File file = null;
    
                    for (TendenciaGanttDTO tendencia : listTendenciasDto) {
                        dataTendencia = UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT, tendencia.getData(), WebUtil.getLanguage(request));
                        dataset.addValue(tendencia.getQtde(), tituloDoRelatorio, dataTendencia);
                    }
    
                    dataTendencia = null;
    
                    JFreeChart chart  = ChartFactory.createBarChart("", UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.data"), UtilI18N.internacionaliza(request, "visao.quantidade"), dataset, PlotOrientation.VERTICAL, false, true, false);
                    CategoryPlot plot = chart.getCategoryPlot();
                    CategoryAxis axis = (CategoryAxis) plot.getDomainAxis();
                    axis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
    
                    try {
                        sbAux_essaVarialPodeReceberVariosValores.setLength(0);
                        sbAux_essaVarialPodeReceberVariosValores.append(CITCorporeUtil.CAMINHO_REAL_APP);
                        sbAux_essaVarialPodeReceberVariosValores.append("tempFiles");
                        sbAux_essaVarialPodeReceberVariosValores.append("/ChartAnaliseTendencias.png");
                        final String diretorioRelativo = sbAux_essaVarialPodeReceberVariosValores.toString();
                        file = new File(diretorioRelativo);
                        ChartUtilities.saveChartAsPNG(file, chart, 1200, 650);
                    } catch (IOException e) {
                        System.err.println("Problem occurred creating chart.");
                        e.printStackTrace();
                    }
    
                    Map<String, Object> parametros = new HashMap<String, Object>();
                    parametros = UtilRelatorio.trataInternacionalizacaoLocale(request.getSession(true), parametros);
                    parametros.put("TITULO_RELATORIO", UtilI18N.internacionaliza(request, "problema.analiseTendencias.titulo"));
                    parametros.put("NOME_USUARIO", usuario.getNomeUsuario());
                    parametros.put("CIDADE", getCidadeParametrizada(request));
                    parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
                    parametros.put("Logo", LogoRel.getFile());
                    parametros.put("TipoRelatorio", tituloDoRelatorio);
                    parametros.put("ChartAnaliseTendencias", file);
    
                    sbAux_essaVarialPodeReceberVariosValores.setLength(0);
                    sbAux_essaVarialPodeReceberVariosValores.append(new Date().getTime());
                    final String strCompl = sbAux_essaVarialPodeReceberVariosValores.toString();
    
                    sbAux_essaVarialPodeReceberVariosValores.setLength(0);
                    sbAux_essaVarialPodeReceberVariosValores.append(CITCorporeUtil.CAMINHO_REAL_APP);
                    sbAux_essaVarialPodeReceberVariosValores.append(Constantes.getValue("CAMINHO_RELATORIOS"));
                    sbAux_essaVarialPodeReceberVariosValores.append("RelatorioAnaliseTendencias.jasper");
                    final String caminhoJasper = sbAux_essaVarialPodeReceberVariosValores.toString();
                    JasperPrint print = JasperFillManager.fillReport(caminhoJasper, parametros);
    
                    sbAux_essaVarialPodeReceberVariosValores.setLength(0);
                    sbAux_essaVarialPodeReceberVariosValores.append(CITCorporeUtil.CAMINHO_REAL_APP);
                    sbAux_essaVarialPodeReceberVariosValores.append("/tempFiles");
                    final String diretorioReceita = sbAux_essaVarialPodeReceberVariosValores.toString();
    
                    sbAux_essaVarialPodeReceberVariosValores.setLength(0);
                    sbAux_essaVarialPodeReceberVariosValores.append(Constantes.getValue("SERVER_ADDRESS"));
                    sbAux_essaVarialPodeReceberVariosValores.append(Constantes.getValue("CONTEXTO_APLICACAO"));
                    sbAux_essaVarialPodeReceberVariosValores.append("/tempFiles");
                    final String diretorioRelativoOS = sbAux_essaVarialPodeReceberVariosValores.toString();
    
                    sbAux_essaVarialPodeReceberVariosValores.setLength(0);
                    sbAux_essaVarialPodeReceberVariosValores.append(diretorioReceita);
                    sbAux_essaVarialPodeReceberVariosValores.append("/RelatorioAnaliseTendencias");
                    sbAux_essaVarialPodeReceberVariosValores.append(strCompl);
                    sbAux_essaVarialPodeReceberVariosValores.append(".pdf");
                    JasperExportManager.exportReportToPdfFile(print, sbAux_essaVarialPodeReceberVariosValores.toString());
    
                    sbAux_essaVarialPodeReceberVariosValores.setLength(0);
                    sbAux_essaVarialPodeReceberVariosValores.append("window.open('");
                    sbAux_essaVarialPodeReceberVariosValores.append(Constantes.getValue("SERVER_ADDRESS"));
                    sbAux_essaVarialPodeReceberVariosValores.append(Constantes.getValue("CONTEXTO_APLICACAO"));
                    sbAux_essaVarialPodeReceberVariosValores.append("/printPDF/printPDF.jsp?url=");
                    sbAux_essaVarialPodeReceberVariosValores.append(diretorioRelativoOS);
                    sbAux_essaVarialPodeReceberVariosValores.append("/RelatorioAnaliseTendencias");
                    sbAux_essaVarialPodeReceberVariosValores.append(strCompl);
                    sbAux_essaVarialPodeReceberVariosValores.append(".pdf')");
                    document.executeScript(sbAux_essaVarialPodeReceberVariosValores.toString());
                } else {
                    document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
                }
            }
            
            document.executeScript("JANELA_AGUARDE_MENU.hide();");
        }

	@Override
	public Class getBeanClass() {
		return AnaliseTendenciasDTO.class;
	}

}
