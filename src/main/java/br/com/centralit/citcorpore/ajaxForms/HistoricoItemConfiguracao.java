/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.HistoricoItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.HistoricoValorDTO;
import br.com.centralit.citcorpore.bean.ItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.ItemConfiguracaoTreeDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.bean.ValorDTO;
import br.com.centralit.citcorpore.negocio.HistoricoItemConfiguracaoService;
import br.com.centralit.citcorpore.negocio.HistoricoValorService;
import br.com.centralit.citcorpore.negocio.ItemConfiguracaoService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Reflexao;
import br.com.citframework.util.UtilI18N;
import br.com.citframework.util.WebUtil;

public class HistoricoItemConfiguracao extends ItemConfiguracaoTree {

	@SuppressWarnings("unchecked")
	public void saveBaseline(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		UsuarioDTO usrDto = (UsuarioDTO) br.com.centralit.citcorpore.util.WebUtil.getUsuario(request);
    	if(usrDto == null){
    		return;
    	}
		HistoricoItemConfiguracaoDTO histItem = (HistoricoItemConfiguracaoDTO) document.getBean();
		HistoricoItemConfiguracaoService service = (HistoricoItemConfiguracaoService) ServiceLocator.getInstance().getService(HistoricoItemConfiguracaoService.class, null);
		List<HistoricoItemConfiguracaoDTO> set = (ArrayList<HistoricoItemConfiguracaoDTO>) WebUtil.deserializeCollectionFromRequest(HistoricoItemConfiguracaoDTO.class, "baselinesSerializadas", request);
		if(set!=null) {
			for (HistoricoItemConfiguracaoDTO historicoItemConfiguracaoDTO : set) {
				HistoricoItemConfiguracaoDTO novo = new HistoricoItemConfiguracaoDTO();
				novo.setBaseLine("SIM");
				if(historicoItemConfiguracaoDTO.getIdHistoricoIC()!=null) {
					novo.setIdHistoricoIC(historicoItemConfiguracaoDTO.getIdHistoricoIC());
					service.updateNotNull(novo);
				}
			}
			document.alert(UtilI18N.internacionaliza(request, "itemConfiguracaoTree.baselineGravadasSucesso"));
		}		
		
		if(histItem.getIdItemConfiguracao()!=null)
		{
			ItemConfiguracaoTreeDTO i = new ItemConfiguracaoTreeDTO();
			i.setIdItemConfiguracao(histItem.getIdItemConfiguracao());
			document.setBean(i);
			super.load(document, request, response);
		}
		/**
		 * Oculta o janela aguarde
		 * @author flavio.santana
		 * 25/10/2013
		 */
		document.executeScript("JANELA_AGUARDE_MENU.hide();");
	}

	/**
	 * Alterado por
	 * desenvolvedor: rcs (Rafael C�sar Soyer)
	 * data: 12/01/2015
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void restaurarBaseline(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		UsuarioDTO usrDto = (UsuarioDTO) br.com.centralit.citcorpore.util.WebUtil.getUsuario(request);
    	if(usrDto == null){
    		return;
    	}
		HistoricoItemConfiguracaoDTO histItem = (HistoricoItemConfiguracaoDTO) document.getBean();
		
		HistoricoItemConfiguracaoService serviceHistItem = (HistoricoItemConfiguracaoService) ServiceLocator.getInstance().getService(HistoricoItemConfiguracaoService.class, null);
		HistoricoValorService serviceHistValor = (HistoricoValorService) ServiceLocator.getInstance().getService(HistoricoValorService.class, null);
		ItemConfiguracaoService serviceItem = (ItemConfiguracaoService) ServiceLocator.getInstance().getService(ItemConfiguracaoService.class, null);
		
		histItem = (HistoricoItemConfiguracaoDTO) serviceHistItem.restore(histItem);
			
		List<HistoricoValorDTO> listValoresDtos = serviceHistValor.listHistoricoValorByIdHistoricoIc(histItem.getIdHistoricoIC());

		/*Realizando a Reflex�o de Item de Configura��o*/
		ItemConfiguracaoDTO item = new ItemConfiguracaoDTO();
		Reflexao.copyPropertyValues(histItem, item);
		
		List<ValorDTO> listValorDto = new ArrayList<ValorDTO>();
		
		for (HistoricoValorDTO historicoValorDTO : listValoresDtos) {
			ValorDTO v = new ValorDTO();
				Reflexao.copyPropertyValues(historicoValorDTO, v);
				v.getIdValor();
				listValorDto.add(v);
		}
		
		item.setValores(listValorDto);
		serviceItem.restaurarBaseline(item, usrDto);
		
		ItemConfiguracaoTreeDTO i = new ItemConfiguracaoTreeDTO();
		i.setIdItemConfiguracao(histItem.getIdItemConfiguracao());
		document.setBean(i);
		super.load(document, request, response);
		document.alert(UtilI18N.internacionaliza(request, "itemConfiguracaoTree.restauraHistoricoSuceso"));
	        document.executeScript("JANELA_AGUARDE_MENU.hide();");
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public Class getBeanClass() {
		return HistoricoItemConfiguracaoDTO.class;
		
	}
	

}
