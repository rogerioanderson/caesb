/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.AutoCompleteDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.ServicoDTO;
import br.com.centralit.citcorpore.negocio.ServicoService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilStrings;

import com.google.gson.Gson;

public class AutoCompleteServico extends AjaxFormAction {

    private final Gson gson = new Gson();

    @Override
    public Class<EmpregadoDTO> getBeanClass() {
        return EmpregadoDTO.class;
    }

    @Override
    public void load(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		if (request.getParameter("query") != null) {
			String consulta = UtilStrings.fixEncoding(request.getParameter("query"));

			final String tipoDemanda = request.getParameter("tipoDemanda");
			Integer idTipoDemanda = null;
			if (StringUtils.isNotBlank(tipoDemanda)) {
				idTipoDemanda = Integer.parseInt(tipoDemanda);
			}

			final String contrato = request.getParameter("contrato");
			Integer idContrato = null;
			if (StringUtils.isNotBlank(contrato)) {
				idContrato = Integer.parseInt(contrato);
			}

			final String categoria = request.getParameter("categoria");
			Integer idCategoria = null;
			if (StringUtils.isNotBlank(categoria)) {
				idCategoria = Integer.parseInt(categoria);
			}

			final Collection colRetorno = this.getServicoService().findByNomeAndContratoAndTipoDemandaAndCategoria(idTipoDemanda, idContrato, idCategoria, consulta);
			final AutoCompleteDTO autoCompleteDTO = new AutoCompleteDTO();

			final List<String> lst = new ArrayList<>();
			final List<Integer> lstVal = new ArrayList<>();

			if (colRetorno != null) {
				for (final Iterator it = colRetorno.iterator(); it.hasNext();) {
					final ServicoDTO servicoDto = (ServicoDTO) it.next();
					if (servicoDto.getIdServico() != null) {
						lst.add(servicoDto.getNomeServico());
						lstVal.add(servicoDto.getIdServico());
					}
				}
			}
			autoCompleteDTO.setQuery(consulta);
			autoCompleteDTO.setSuggestions(lst);
			autoCompleteDTO.setData(lstVal);

			final String json = gson.toJson(autoCompleteDTO);
			request.setAttribute("json_response", json);
		}
    }

    private ServicoService servicoService;

    private ServicoService getServicoService() throws Exception {
        if (servicoService == null) {
            servicoService = (ServicoService) ServiceLocator.getInstance().getService(ServicoService.class, null);
        }
        return servicoService;
    }

}
