/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.FormaPagamentoDTO;
import br.com.centralit.citcorpore.negocio.FormaPagamentoService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilI18N;

public class FormaPagamento extends AjaxFormAction{

	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		document.focusInFirstActivateField(null);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class getBeanClass() {
		return FormaPagamentoDTO.class;
	}

	/**
	 * Metodo de salvar
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author geber.costa
	 */

	public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		FormaPagamentoDTO formaPagamento = (FormaPagamentoDTO) document.getBean();
		FormaPagamentoService formaPagamentoService = (FormaPagamentoService) ServiceLocator.getInstance().getService(FormaPagamentoService.class, null);
		
		if (formaPagamento.getIdFormaPagamento() == null || formaPagamento.getIdFormaPagamento() == 0) {
			if (formaPagamentoService.consultarFormaPagamento(formaPagamento)) {
				document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.registroJaCadastrado"));
				return;
			}
			formaPagamento.setSituacao("A");
			formaPagamentoService.create(formaPagamento);
			document.alert(UtilI18N.internacionaliza(request, "MSG05"));

		} else {
			if (formaPagamentoService.consultarFormaPagamento(formaPagamento)) {
				document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.registroJaCadastrado"));
				return;
			}
			formaPagamento.setSituacao("A");
			formaPagamentoService.update(formaPagamento);
			document.alert(UtilI18N.internacionaliza(request, "MSG06"));
		}

		HTMLForm form = document.getForm("form");
		form.clear();

		document.executeScript("limpar_LOOKUP_FORMAPAGAMENTO()");
	}

	/**
	 * Metodo colocar status Inativo quando for solicitado a exclus�o do usuario.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void delete(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		FormaPagamentoDTO formaPagamentoDto = (FormaPagamentoDTO) document.getBean();
		FormaPagamentoService formaPagamentoService = (FormaPagamentoService) ServiceLocator.getInstance().getService(FormaPagamentoService.class, null);
		

		if (formaPagamentoDto.getIdFormaPagamento().intValue() > 0) {
			formaPagamentoService.deletarFormaPagamento(formaPagamentoDto, document,request);
		}

		HTMLForm form = document.getForm("form");
		form.clear();

		document.executeScript("limpar_LOOKUP_FORMAPAGAMENTO()");
	}

	/**
	 * Metodo para restaura os campos.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		FormaPagamentoDTO formaPagamentoDto = (FormaPagamentoDTO) document.getBean();
		FormaPagamentoService formaPagamentoService = (FormaPagamentoService) ServiceLocator.getInstance().getService(FormaPagamentoService.class, null);
		formaPagamentoDto = (FormaPagamentoDTO) formaPagamentoService.restore(formaPagamentoDto);
		HTMLForm form = document.getForm("form");
		form.clear();
		form.setValues(formaPagamentoDto);
	}
}
