/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;
import br.com.centralit.citcorpore.bean.ComentariosDTO;
import br.com.centralit.citcorpore.negocio.BaseConhecimentoService;
import br.com.centralit.citcorpore.negocio.ComentariosService;
import br.com.centralit.citcorpore.negocio.ContadorAcessoService;
import br.com.centralit.lucene.Lucene;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;



public class Comentarios extends AjaxFormAction {

    @SuppressWarnings("rawtypes")
    @Override
    public Class getBeanClass() {

	return ComentariosDTO.class;
    }

    @Override
    public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
	
    }
    
    public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
	   
		ComentariosDTO comentariosDto = (ComentariosDTO) document.getBean();
	
		ComentariosService comentariosService = (ComentariosService) ServiceLocator.getInstance().getService(ComentariosService.class, null);
		if (comentariosDto.getIdComentario() == null || comentariosDto.getIdComentario().intValue() == 0) {
		    if(comentariosDto.getNota()==null){
			comentariosDto.setNota("0");
		    }
		    if(!comentariosDto.getComentario().equalsIgnoreCase("")){
			comentariosDto.setDataInicio(UtilDatas.getDataAtual());
			Integer idBaseConhecimento = (Integer)request.getSession().getAttribute("idBaseConhecimento");
			comentariosDto.setIdBaseConhecimento(idBaseConhecimento);
			comentariosService.create(comentariosDto);
			
			//Atualizando �ndice Lucene
			// Avalia��o - M�dia da nota dada pelos usu�rios
			BaseConhecimentoService baseConhecimentoService = (BaseConhecimentoService) ServiceLocator.getInstance().getService(BaseConhecimentoService.class, null);
			BaseConhecimentoDTO baseConhecimentoDto = new BaseConhecimentoDTO();
			baseConhecimentoDto.setIdBaseConhecimento(idBaseConhecimento);
			baseConhecimentoDto = (BaseConhecimentoDTO) baseConhecimentoService.restore(baseConhecimentoDto);
			Double media = baseConhecimentoService.calcularNota(baseConhecimentoDto.getIdBaseConhecimento());
			if (media != null)
				baseConhecimentoDto.setMedia(media.toString());
			else
				baseConhecimentoDto.setMedia(null);
			ContadorAcessoService contadorAcessoService = (ContadorAcessoService) ServiceLocator.getInstance().getService(ContadorAcessoService.class, null);
			// Qtde de cliques
			Integer quantidadeDeCliques = contadorAcessoService.quantidadesDeAcessoPorBaseConhecimnto(baseConhecimentoDto);
			if (quantidadeDeCliques != null)
				baseConhecimentoDto.setContadorCliques(quantidadeDeCliques);
			else
				baseConhecimentoDto.setContadorCliques(0);
			Lucene lucene = new Lucene();
			lucene.indexarBaseConhecimento(baseConhecimentoDto);
			
			document.alert(UtilI18N.internacionaliza(request, "MSG05"));
			
		    }else{
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.camposObrigatorios"));
			document.executeScript("document.getElementById('comentario').focus();");
			return;
		    }
		    
			
		} else {
		    comentariosService.update(comentariosDto);
		    document.alert(UtilI18N.internacionaliza(request, "MSG06"));
		}
		
		 HTMLForm form = document.getForm("formPopup");
		    
		 form.clear();
		 document.executeScript("fecharPopup()");
		 
			/* @autor edu.braz
			 *  05/03/2014 */	
			/* atualiza a quantidade de comentarios.*/
		 Integer idBaseConhecimento = (Integer)request.getSession().getAttribute("idBaseConhecimento");	 
		 document.executeScript("comentariosAbertosPorBaseConhecimnto(" + idBaseConhecimento + ");");//Chamada da fun��o que esta dentro de baseConhecimentoView.jsp 		 
    }
}
