/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.JustificativaRequisicaoLiberacaoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoLiberacaoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.JustificativaRequisicaoLiberacaoService;
import br.com.centralit.citcorpore.negocio.RequisicaoLiberacaoService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilI18N;
@SuppressWarnings("rawtypes")
public class SuspensaoLiberacao  extends AjaxFormAction  {

	
	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null){
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}
		HTMLForm form = document.getForm("form");
		form.clear();	
		RequisicaoLiberacaoDTO requisicaoLiberacaoDto = (RequisicaoLiberacaoDTO)document.getBean();
		RequisicaoLiberacaoService requisicaoLiberacaoService = (RequisicaoLiberacaoService) ServiceLocator.getInstance().getService(RequisicaoLiberacaoService.class, WebUtil.getUsuarioSistema(request));
		JustificativaRequisicaoLiberacaoService justificativaRequisicaoLiberacaoService = (JustificativaRequisicaoLiberacaoService) ServiceLocator.getInstance().getService(JustificativaRequisicaoLiberacaoService.class, null);
		if(requisicaoLiberacaoDto.getIdRequisicaoLiberacao()!=null){
			requisicaoLiberacaoDto = requisicaoLiberacaoService.restoreAll(requisicaoLiberacaoDto.getIdRequisicaoLiberacao());
		}
		
		request.setAttribute("dataHoraSolicitacao", requisicaoLiberacaoDto.getDataHoraSolicitacaoStr());
		
		Collection<JustificativaRequisicaoLiberacaoDTO> colJustificativas = justificativaRequisicaoLiberacaoService.listAtivasParaSuspensao();
		
		HTMLSelect comboJustificativa = (HTMLSelect) document.getSelectById("idJustificativa");
		document.getSelectById("idJustificativa").removeAllOptions();
		comboJustificativa.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
		if (colJustificativas != null){
			for(JustificativaRequisicaoLiberacaoDTO justificativa : colJustificativas){
				comboJustificativa.addOption(justificativa.getIdJustificativaLiberacao().toString(), justificativa.getDescricaoJustificativa());
			}
		}
		form.setValues(requisicaoLiberacaoDto);
		
	}

	 public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
			UsuarioDTO usuario = WebUtil.getUsuario(request);
			if (usuario == null) {
				document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
				document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
				return;
			}
			
			RequisicaoLiberacaoDTO requisicaoLiberacaoDto = (RequisicaoLiberacaoDTO)document.getBean();	
			if (requisicaoLiberacaoDto.getIdRequisicaoLiberacao() == null)
				return;

			if (requisicaoLiberacaoDto.getIdJustificativa() == null) {
				document.alert(UtilI18N.internacionaliza(request,"gerenciaservico.suspensaosolicitacao.validacao.justificanaoinformada"));
				return;
			}

			RequisicaoLiberacaoService requisicaoLiberacaoService = (RequisicaoLiberacaoService) ServiceLocator.getInstance().getService(RequisicaoLiberacaoService.class, WebUtil.getUsuarioSistema(request));
			RequisicaoLiberacaoDTO requisicaoLiberacaoDtoAuxiliar = requisicaoLiberacaoService.restoreAll(requisicaoLiberacaoDto.getIdRequisicaoLiberacao());
			requisicaoLiberacaoDtoAuxiliar.setIdJustificativa(requisicaoLiberacaoDto.getIdJustificativa());
			requisicaoLiberacaoDtoAuxiliar.setComplementoJustificativa(requisicaoLiberacaoDto.getComplementoJustificativa());
			requisicaoLiberacaoService.suspende(usuario, requisicaoLiberacaoDtoAuxiliar);
	    	document.executeScript("fechar();");
	    }
	
	@Override
	public Class getBeanClass() {
		// TODO Auto-generated method stub
		return RequisicaoLiberacaoDTO.class;
	}

}
