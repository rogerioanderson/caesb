/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.bpm.dto.FluxoDTO;
import br.com.centralit.bpm.dto.PermissoesFluxoDTO;
import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.CidadesDTO;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.DadosBancariosIntegranteDTO;
import br.com.centralit.citcorpore.bean.DespesaViagemDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.IntegranteViagemDTO;
import br.com.centralit.citcorpore.bean.JustificativaParecerDTO;
import br.com.centralit.citcorpore.bean.JustificativaSolicitacaoDTO;
import br.com.centralit.citcorpore.bean.ParceiroDTO;
import br.com.centralit.citcorpore.bean.ParecerDTO;
import br.com.centralit.citcorpore.bean.RequisicaoViagemDTO;
import br.com.centralit.citcorpore.bean.RoteiroViagemDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.TipoMovimFinanceiraViagemDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.UsuarioDao;
import br.com.centralit.citcorpore.negocio.CentroResultadoService;
import br.com.centralit.citcorpore.negocio.CidadesService;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.DadosBancariosIntegranteService;
import br.com.centralit.citcorpore.negocio.DespesaViagemService;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.FormaPagamentoService;
import br.com.centralit.citcorpore.negocio.GrupoService;
import br.com.centralit.citcorpore.negocio.IntegranteViagemService;
import br.com.centralit.citcorpore.negocio.JustificativaParecerService;
import br.com.centralit.citcorpore.negocio.JustificativaSolicitacaoService;
import br.com.centralit.citcorpore.negocio.MoedaService;
import br.com.centralit.citcorpore.negocio.ParceiroService;
import br.com.centralit.citcorpore.negocio.ParecerService;
import br.com.centralit.citcorpore.negocio.PermissoesFluxoService;
import br.com.centralit.citcorpore.negocio.ProjetoService;
import br.com.centralit.citcorpore.negocio.RequisicaoViagemService;
import br.com.centralit.citcorpore.negocio.RoteiroViagemService;
import br.com.centralit.citcorpore.negocio.SolicitacaoServicoService;
import br.com.centralit.citcorpore.negocio.TipoMovimFinanceiraViagemService;
import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;
import br.com.citframework.util.UtilStrings;


@SuppressWarnings({"rawtypes","unused", "unchecked"})
public class DespesaViagem extends AjaxFormAction {
	
	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();
		
		if(despesaViagemDTO.getIdContrato() != null){
			request.getSession().setAttribute("idContratoViagem", despesaViagemDTO.getIdContrato());
		} else if(request.getSession() != null && request.getSession().getAttribute("idContratoViagem") != null){
			despesaViagemDTO.setIdContrato((Integer) request.getSession().getAttribute("idContratoViagem"));
		}
			
		
		this.preencherComboCentroResultado(document, request, response);
		this.preencherComboProjeto(document, request, response);
		this.preencherComboJustificativa(document, request, response);
		this.preencherComboFinalidade(document, request, response);
		this.restoreTreeIntegrantesViagem(document, request, response, true);
		this.restoreTitulo(document, request, response);
		this.removeBotoesDaTela(document, request, response, usuario, despesaViagemDTO);

		if(despesaViagemDTO.getIdSolicitacaoServico() != null) {
			this.restore(document, request, response);
		}
	}

	public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		RequisicaoViagemService reqViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, null);
		DespesaViagemService despesaViagemService = (DespesaViagemService) ServiceLocator.getInstance().getService(DespesaViagemService.class, null);
		IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, null);

		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();
		RequisicaoViagemDTO requisicaoViagemDTO = new RequisicaoViagemDTO();
		Collection<IntegranteViagemDTO> colIntegrantes = null;

		requisicaoViagemDTO.setIdSolicitacaoServico(despesaViagemDTO.getIdSolicitacaoServico());
		requisicaoViagemDTO.setIdContrato(despesaViagemDTO.getIdContrato());

		if(despesaViagemDTO.getIdSolicitacaoServico()!=null){
			requisicaoViagemDTO = (RequisicaoViagemDTO) reqViagemService.restore(requisicaoViagemDTO);

			if(requisicaoViagemDTO != null){
				if(requisicaoViagemDTO.getIdCidadeOrigem() != null) {
					requisicaoViagemDTO.setNomeCidadeOrigem(this.recuperaCidade(requisicaoViagemDTO.getIdCidadeOrigem()));
				}

				if(requisicaoViagemDTO.getIdCidadeDestino() != null) {
					requisicaoViagemDTO.setNomeCidadeDestino(this.recuperaCidade(requisicaoViagemDTO.getIdCidadeDestino()));
				}
			}

			colIntegrantes = integranteViagemService.recuperaIntegrantesViagemByIdSolicitacaoEstado(despesaViagemDTO.getIdSolicitacaoServico(), RequisicaoViagemDTO.AGUARDANDO_PLANEJAMENTO);
		}

		HTMLForm form = document.getForm("form");
        form.clear();
        form.setValues(requisicaoViagemDTO);

        /*
        document.getElementById("idCentroCusto").setDisabled(true);
        document.getElementById("finalidade").setDisabled(true);
        document.getElementById("idProjeto").setDisabled(true);
        document.getElementById("idMotivoViagem").setDisabled(true);
        document.getElementById("descricaoMotivo").setDisabled(true);
        */
        
        if(colIntegrantes != null) {
        	document.getElementById("colIntegrantesViagem_Serialize").setValue(br.com.citframework.util.WebUtil.serializeObjects(colIntegrantes));
        }
	}

	/**
	 * Retorna o html com a treeview dos integrantes e seu respectivos itens
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @param collapsed
	 * @throws ServiceException
	 * @throws Exception
	 */
	public void restoreTreeIntegrantesViagem(DocumentHTML document, HttpServletRequest request, HttpServletResponse response, Boolean collapsed) throws ServiceException, Exception {
		RequisicaoViagemService reqViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, null);
		DespesaViagemService despesaViagemService = (DespesaViagemService) ServiceLocator.getInstance().getService(DespesaViagemService.class, null);
		RoteiroViagemService roteiroViagemService = (RoteiroViagemService) ServiceLocator.getInstance().getService(RoteiroViagemService.class, null);
		TipoMovimFinanceiraViagemService tipoMovimFinanceiraViagemService = (TipoMovimFinanceiraViagemService) ServiceLocator.getInstance().getService(TipoMovimFinanceiraViagemService.class, null);
		CidadesService cidadeService = (CidadesService) ServiceLocator.getInstance().getService(CidadesService.class, null);
		IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, null);

		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();
		RoteiroViagemDTO roteiroViagemDTO = null;
		TipoMovimFinanceiraViagemDTO tipoMovimFinanceiraViagemDTO = null;
		CidadesDTO origem = null;
		CidadesDTO destino = null;

		Collection<IntegranteViagemDTO> colIntegrantes =  integranteViagemService.recuperaIntegrantesViagemByIdSolicitacaoEstado(despesaViagemDTO.getIdSolicitacaoServico(), RequisicaoViagemDTO.AGUARDANDO_PLANEJAMENTO);

		NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		DecimalFormat decimal = (DecimalFormat) nf;
		decimal.applyPattern("#,##0.00");

		if(colIntegrantes != null) {
			StringBuilder html = new StringBuilder();

			for(IntegranteViagemDTO integrante: colIntegrantes) {
				roteiroViagemDTO = roteiroViagemService.findByIdIntegrante(integrante.getIdIntegranteViagem());
				Collection<DespesaViagemDTO> colDespesaViagem = despesaViagemService.findDespesasAtivasViagemByIdRoteiro(roteiroViagemDTO.getIdRoteiroViagem());
				origem = (CidadesDTO) ((List) cidadeService.findNomeByIdCidade(roteiroViagemDTO.getOrigem())).get(0);
				destino = (CidadesDTO) ((List) cidadeService.findNomeByIdCidade(roteiroViagemDTO.getDestino())).get(0);

				html.append("<div class='despesa-viagem-item'>");
				html.append("	<ul class='filetree treeview browser'>");
				html.append("		<li>");
				html.append("			<span class='folder'>" + integrante.getNome() +
											" - ida " + UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT, roteiroViagemDTO.getIda(), UtilI18N.getLocale(request)) +
											" - volta " + UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT, roteiroViagemDTO.getVolta(), UtilI18N.getLocale(request)) +
											" - " + origem.getNomeCidade() + "/" + origem.getNomeUf() +
											" - " + destino.getNomeCidade() + "/" + destino.getNomeUf() +
											"<button class='btn btn-default btn-primary' style='float: right;' type='button' title='Adicionar itens viagem' onclick='abrirPopupItemControleFinanceiro("+ integrante.getIdIntegranteViagem() +")'><i class='icon-white icon-plus'></i></button>"+
											"&nbsp;"+"<button class='btn btn-default btn-primary' style='float: right;' type='button' title='Visualiza��o avan�ada' onclick='abrirModalIntegrante("+ integrante.getIdIntegranteViagem() +")'><i class='icon-white icon-eye-open'></i></button></span>");
				html.append("			<ul>");
				html.append("				<li>");
				html.append("					<div class='file'>");
				html.append("						<table class='table_integrante_controle'>");

				if(colDespesaViagem != null && !colDespesaViagem.isEmpty()) {
					Double total = 0.0;
					for(DespesaViagemDTO despesaViagem : colDespesaViagem) {
						tipoMovimFinanceiraViagemDTO = tipoMovimFinanceiraViagemService.findByMovimentacao(Long.parseLong(despesaViagem.getIdTipo().toString()));
						despesaViagem.setTipoMovimFinanceiraViagem(tipoMovimFinanceiraViagemDTO);

						html.append("							<tr>");
						html.append("								<td width='5%'><span class='glyphicons " + despesaViagem.getTipoMovimFinanceiraViagem().getImagem() + "'><i></i>&nbsp;</span></td>");
						html.append("								<td width='50%'>" + despesaViagem.getTipoMovimFinanceiraViagem().getClassificacao() + "</td>");
						html.append("								<td width='15%'>" + despesaViagem.getTotalFormatado() + "</td>");
						html.append("								<td width='30%'><a class='btn-editar-item-despesa btn-action btn-success glyphicons edit' href='javascript:;' onclick='editarDespesaViagem(" + despesaViagem.getIdDespesaViagem() + ")'><i></i></a> <a class='btn-action btn-excluir-item-despesa btn-danger glyphicons remove_2' href='javascript:;' onclick='excluirDespesaViagem(" + despesaViagem.getIdDespesaViagem() + ")'><i></i></a></td>");
						html.append("							</tr>");
						total += despesaViagem.getTotal();
					}

					html.append("							<tr>");
					html.append("								<td></td>");
					html.append("								<td class='strong'>Valor total</td>");
					html.append("								<td class='strong' colspan='2'>" + decimal.format(total) + "</td>");
					html.append("							</tr>");
				} else {
					html.append("							<tr>");
					html.append("								<td colspan='3' style='padding: 0;'>N�o h� itens adicionados para este integrante!</td>");
					html.append("							</tr>");
				}

				html.append("						</table>");
				html.append("					</div><!-- .file -->");
				html.append("				</li>");
				html.append("			</ul>");
				html.append("		</li>");
				html.append("	</ul>");
				html.append("</div><!-- .despesa-viagem-item -->");
			}

			document.getElementById("despesa-viagem-items-container").setInnerHTML(html.toString());

			document.executeScript("$('.browser').treeview({collapsed: " + collapsed + "});");
		}
	}

	/**
	 * Carrega a popup para adicionar item de despesa
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void carregarPopup(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();

		HTMLForm formItem = document.getForm("formItem");
//		formItem.clear();

		if(despesaViagemDTO.getIdSolicitacaoServico() != null) {
			document.getElementById("idSolicitacaoServicoAux").setValue(despesaViagemDTO.getIdSolicitacaoServico().toString());
		}

		this.preencherComboTipoDespesa(document, request, response);
		this.preencherComboMoeda(document, request, response);
		this.preencherComboFormaPagamento(document, request, response);

		// Verifica se � altera��o do item
		if(despesaViagemDTO.getIdDespesaViagem() != null) {
			DespesaViagemService despesaViagemService = (DespesaViagemService) ServiceLocator.getInstance().getService(DespesaViagemService.class, null);
			ParceiroService parceiroService = (ParceiroService) ServiceLocator.getInstance().getService(ParceiroService.class, null);

			ParceiroDTO parceiro = new ParceiroDTO();

			despesaViagemDTO = (DespesaViagemDTO) despesaViagemService.restore(despesaViagemDTO);

			if(despesaViagemDTO != null) {
				formItem.setValues(despesaViagemDTO);

				// Tipo da despesa
				document.getSelectById("tipoDespesa").setValue(despesaViagemDTO.getIdTipo().toString());

				// Parceiro
				parceiro.setIdParceiro(despesaViagemDTO.getIdFornecedor());
				parceiro = (ParceiroDTO) parceiroService.restore(parceiro);
				if(parceiro != null) {
					document.getElementById("nomeFornecedor").setValue(parceiro.getNome());
				}

				// Moeda
				if(despesaViagemDTO.getIdFormaPagamento() != null) {
					document.getElementById("idMoeda").setValue(despesaViagemDTO.getIdMoeda().toString());
					document.getSelectById("idMoedaAux").setValue(despesaViagemDTO.getIdMoeda().toString());
				}

				// Forma de pagamento
				if(despesaViagemDTO.getIdFormaPagamento() != null) {
					document.getSelectById("idFormaPagamento").setValue(despesaViagemDTO.getIdFormaPagamento().toString());
				}

				if(despesaViagemDTO.getValidade() != null) {
					document.getElementById("prazoCotacaoAux").setValue(UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT, despesaViagemDTO.getValidade(), UtilI18N.getLocale(request)));
					document.getElementById("horaCotacaoAux").setValue(UtilDatas.getHoraHHMM(despesaViagemDTO.getValidade()));
				}

				document.getElementById("valorAdiantamento").setValue(despesaViagemDTO.getTotalFormatado());

				this.tratarValoresTipoMovimentacao(document, request, response, despesaViagemDTO);
//				this.carregaIntegranteViagem(document, request, response, despesaViagemDTO);
			}

		}
	}

	/**
	 * Realiza o calculo da quantidade vezes o valor e retorna para a tela do usuario
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 */
	public void calcularTotal(DocumentHTML document, HttpServletRequest request, HttpServletResponse response)throws ServiceException,Exception {
		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();

		document.getElementById("valorAdiantamento").setValue(despesaViagemDTO.getTotalFormatado());
	}

	/**
	 * Criado apenas para ser chamado do JS pelo .fireEvent('');
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 * @author renato.jesus
	 */
	public void tratarValoresTipoMovimentacao(DocumentHTML document, HttpServletRequest request, HttpServletResponse response)throws ServiceException,Exception {
		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();

		this.tratarValoresTipoMovimentacao(document, request, response, despesaViagemDTO);
	}

	/**
	 * Faz o tratamento do tipo da movimenta��o financeira.
	 * Se a Classifica��o for igual a di�ria ent�o o adiantamento = valorUnit�rio * (quantidade + 1), se a classifica��o for qualquer outro diferente
	 * ent�o adiantamento = valorUnit�rio * di�ria
	 * O tratamento para o adiantamento � feito , ele calcula e seta o valor na tela automaticamente.
	 * Esse m�todo tamb�m faz o tratamento para casas decimais
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @param despesaViagemDTO
	 * @throws ServiceException
	 * @throws Exception
	 * @author renato.jesus
	 */
	private void tratarValoresTipoMovimentacao(DocumentHTML document, HttpServletRequest request, HttpServletResponse response, DespesaViagemDTO despesaViagemDTO)throws ServiceException,Exception {
		
		if(despesaViagemDTO.getIdTipo() != null && !despesaViagemDTO.getIdTipo().toString().equals("")) {
			
			TipoMovimFinanceiraViagemService tipoMovimentacaoService = (TipoMovimFinanceiraViagemService) ServiceLocator.getInstance().getService(TipoMovimFinanceiraViagemService.class, null);
			IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, null);
			RoteiroViagemService roteiroViagemService = (RoteiroViagemService) ServiceLocator.getInstance().getService(RoteiroViagemService.class, null);

			TipoMovimFinanceiraViagemDTO tipoMovimFinanceiraDto = new TipoMovimFinanceiraViagemDTO();

			IntegranteViagemDTO integranteViagemDTO = new IntegranteViagemDTO();
			RoteiroViagemDTO roteiroViagemDTO = new RoteiroViagemDTO();
			integranteViagemDTO = integranteViagemService.findById(despesaViagemDTO.getIdIntegrante());
			roteiroViagemDTO = roteiroViagemService.findByIdIntegrante(despesaViagemDTO.getIdIntegrante());

			tipoMovimFinanceiraDto.setIdtipoMovimFinanceiraViagem(despesaViagemDTO.getIdTipo());
			tipoMovimFinanceiraDto = (TipoMovimFinanceiraViagemDTO) tipoMovimentacaoService.restore(tipoMovimFinanceiraDto);

			despesaViagemDTO.setValor(0d);
			
			if(tipoMovimFinanceiraDto != null) {
				despesaViagemDTO.setTipoMovimFinanceiraViagem(tipoMovimFinanceiraDto);

				if(despesaViagemDTO.getIdDespesaViagem() == null) {
					document.getElementById("valor").setReadonly(false);
					String classificacao = "";

					classificacao = UtilStrings.removeCaracteresEspeciais(despesaViagemDTO.getTipoMovimFinanceiraViagem().getClassificacao());

					if(despesaViagemDTO.getTipoMovimFinanceiraViagem().getValorPadrao() != null) {
						NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
						DecimalFormat decimalFormat = (DecimalFormat) nf;
						decimalFormat.applyPattern("#,##0.00");
						String valorUnit = decimalFormat.format(despesaViagemDTO.getTipoMovimFinanceiraViagem().getValorPadrao());
						despesaViagemDTO.setValor(despesaViagemDTO.getTipoMovimFinanceiraViagem().getValorPadrao());
						document.getElementById("valor").setValue(valorUnit);
					} else {
						document.getElementById("valor").setValue("");
					}

					if(classificacao.equalsIgnoreCase(Enumerados.ClassificacaoMovFinViagem.Diaria.toString())) {
						int qtdDiarias = 0;
						if(roteiroViagemDTO.getIda() != null &&  roteiroViagemDTO.getVolta() != null){

							GregorianCalendar ini = new GregorianCalendar();
							GregorianCalendar fim = new GregorianCalendar();
							SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd");
							ini.setTime(sdf.parse(roteiroViagemDTO.getIda().toString()));
							fim.setTime(sdf.parse(roteiroViagemDTO.getVolta().toString()));
							long dt1 = ini.getTimeInMillis();
							long dt2 = fim.getTimeInMillis();
							qtdDiarias = (int) ((((dt2 - dt1) / 86400000)+1));
						}

						despesaViagemDTO.setQuantidade(qtdDiarias);
						if(qtdDiarias > 0){
							document.getElementById("quantidade").setReadonly(true);
						}
						document.getElementById("valor").setReadonly(true);
						document.getElementById("quantidade").setValue(despesaViagemDTO.getQuantidade().toString());
						this.calcularTotal(document, request, response);
					} else {
						document.getElementById("quantidade").setValue("1");
						document.getElementById("quantidade").setReadonly(false);
					}

					document.getElementById("valorAdiantamento").setValue(despesaViagemDTO.getTotalFormatado());
				}

				if(tipoMovimFinanceiraDto.getExigeDataHoraCotacao().equalsIgnoreCase("S")) {
					document.executeScript("$('#labelPrazoCotacao, #labelHoraCotacao').addClass('campoObrigatorio');");
					document.getElementById("prazoCotacaoAux").setDisabled(false);
					document.getElementById("horaCotacaoAux").setDisabled(false);
				} else {
					document.executeScript("$('#labelPrazoCotacao, #labelHoraCotacao').removeClass('campoObrigatorio');");
					document.getElementById("prazoCotacaoAux").setDisabled(true);
					document.getElementById("horaCotacaoAux").setDisabled(true);
				}
			}
		} else {
			document.getElementById("quantidade").setValue("");
			document.getElementById("valor").setValue("");
			document.getElementById("valorAdiantamento").setValue("");
			document.getElementById("quantidade").setReadonly(false);
		}
	}

	/**
	 * Armazena o item de despesa e configura-o para cada integrante selecionado
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void adicionarDespesaViagem(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		RoteiroViagemService roteiroViagemService = (RoteiroViagemService) ServiceLocator.getInstance().getService(RoteiroViagemService.class, null);
		DespesaViagemService despesaViagemService = (DespesaViagemService) ServiceLocator.getInstance().getService(DespesaViagemService.class, null);
		IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, null);

		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();

		IntegranteViagemDTO integranteViagemDTO = integranteViagemService.findById(despesaViagemDTO.getIdIntegrante());

		if(despesaViagemDTO.getPrazoCotacaoAux() != null && despesaViagemDTO.getHoraCotacaoAux() != null) {
			despesaViagemDTO.setValidade(Timestamp.valueOf(despesaViagemDTO.getPrazoCotacaoAux() + " " + Time.valueOf(despesaViagemDTO.getHoraCotacaoAux() + ":00")));
		}

		if(despesaViagemDTO.getIdDespesaViagem() == null || despesaViagemDTO.getIdDespesaViagem().equals("")) {
			despesaViagemDTO.setDataInicio(UtilDatas.getDataAtual());

			RoteiroViagemDTO roteiroViagemDTO = null;

			if(integranteViagemDTO != null && integranteViagemDTO.getIdIntegranteViagem() != null) {
				roteiroViagemDTO = roteiroViagemService.findByIdIntegrante(integranteViagemDTO.getIdIntegranteViagem());

				/**
				 * Removido a pedido da diretoria
				 * @author gilberto.nery
				 * @date: 19/11/2015
				 * 
				 * Regra de negocio em:
				 * \\10.2.1.11\Desenvolvimento\Equipes CDI\Equipe ITSM\RGN\Demanda de altera��es CitViagem - Citsmart.msg
				 *
				 */
				/*
				if(despesaViagemDTO.getPrazoCotacaoAux() != null && !despesaViagemDTO.getPrazoCotacaoAux().equals("") && roteiroViagemDTO.getVolta() != null && !roteiroViagemDTO.getVolta().equals("")){
					if(despesaViagemDTO.getPrazoCotacaoAux().compareTo(roteiroViagemDTO.getVolta()) > 0){
						document.alert(UtilI18N.internacionaliza(request, "despesaViagem.dataCotacaoMaiorFimViagem"));
						return;
					}
				}*/

				despesaViagemDTO.setIdRoteiro(roteiroViagemDTO.getIdRoteiroViagem());

				if(integranteViagemDTO.getRemarcacao().equalsIgnoreCase("S")){
					despesaViagemDTO.setOriginal("N");
				}else{
					despesaViagemDTO.setOriginal("S");
				}
				despesaViagemService.create(despesaViagemDTO);
			}
		} else {
			despesaViagemService.update(despesaViagemDTO);
		}

		limparCamposDosItensDeDespesaEFecharPopup(document, request, response);

		this.restoreTreeIntegrantesViagem(document, request, response, false);
	}

	public void limparCamposDosItensDeDespesaEFecharPopup(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		document.executeScript("$('#POPUP_ITEMCONTROLEFINANCEIRO').dialog('close')");
		HTMLForm formItem = document.getForm("formItem");
		HTMLForm form = document.getForm("form");
		formItem.clear();
		
	}

	/**
	 * Exclui o item de despesa conforme dados passados
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void excluirDespesaViagem(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		DespesaViagemService despesaViagemService = (DespesaViagemService) ServiceLocator.getInstance().getService(DespesaViagemService.class, null);

		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();

		if(despesaViagemDTO != null && despesaViagemDTO.getIdDespesaViagem() != null) {
			despesaViagemService.delete(despesaViagemDTO);

			this.restoreTreeIntegrantesViagem(document, request, response, false);
		}
	}

	/**
	 * Preenche a combo de 'Centro Resultado' do formul�rio HTML
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author renato.jesus
	 */
	public void preencherComboCentroResultado(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		CentroResultadoService centroResultadoService = (CentroResultadoService) ServiceLocator.getInstance().getService(CentroResultadoService.class, WebUtil.getUsuarioSistema(request));

        HTMLSelect comboCentroCusto = (HTMLSelect) document.getSelectById("idCentroCusto");

        comboCentroCusto.removeAllOptions();
        comboCentroCusto.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));

        Collection colCCusto = centroResultadoService.listPermiteRequisicaoProduto();
        if(colCCusto != null && !colCCusto.isEmpty()){
        	 comboCentroCusto.addOptions(colCCusto, "idCentroResultado", "nomeHierarquizado", null);
        }

	}

	/**
	 * Preenche a combo de 'finalidade' do formul�rio HTML
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author thays.araujo
	 */
	public void preencherComboFinalidade(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HTMLSelect finalidade = (HTMLSelect) document.getSelectById("finalidade");
		finalidade.removeAllOptions();
		finalidade.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
        finalidade.addOption("I", UtilI18N.internacionaliza(request, "requisicaoProduto.finalidade.usoInterno"));
		finalidade.addOption("C", UtilI18N.internacionaliza(request, "requisicaoProduto.finalidade.atendimentoCliente"));
	}

	/**
	 * Preenche a combo de 'Projeto' do formul�rio HTML
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @param requisicaoViagemDto
	 * @throws Exception
	 * @author renato.jesus
	 */
	public void preencherComboProjeto(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();

		HTMLSelect comboProjeto = (HTMLSelect) document.getSelectById("idProjeto");

		comboProjeto.removeAllOptions();
		comboProjeto.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));

		if (despesaViagemDTO.getIdContrato() != null) {
		    ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, WebUtil.getUsuarioSistema(request));
		    ContratoDTO contratoDto = new ContratoDTO();
		    contratoDto.setIdContrato(despesaViagemDTO.getIdContrato());
		    contratoDto = (ContratoDTO) contratoService.restore(contratoDto);
		    if (contratoDto != null) {
		        ProjetoService projetoService = (ProjetoService) ServiceLocator.getInstance().getService(ProjetoService.class, WebUtil.getUsuarioSistema(request));
		        Collection colProjetos = projetoService.listHierarquia(contratoDto.getIdCliente(), true);
		        if(colProjetos != null && !colProjetos.isEmpty())
		            comboProjeto.addOptions(colProjetos, "idProjeto", "nomeHierarquizado", null);
		    }
		}
	}

	/**
	 * Preenche combo de 'justificativa solicita��o'.
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author renato.jesus
	 */
	public void preencherComboJustificativa(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		JustificativaSolicitacaoService justificativaSolicitacaoService = (JustificativaSolicitacaoService)ServiceLocator.getInstance().getService(JustificativaSolicitacaoService.class, null);

		Collection<JustificativaSolicitacaoDTO> colJustificativas = justificativaSolicitacaoService.listAtivasParaViagem();

		HTMLSelect comboJustificativa = (HTMLSelect) document.getSelectById("idMotivoViagem");
		comboJustificativa.removeAllOptions();

		comboJustificativa.removeAllOptions();
		comboJustificativa.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));

		if (colJustificativas != null){
			comboJustificativa.addOptions(colJustificativas, "idJustificativa", "descricaoJustificativa", null);
		}
	}

	/**
	 * Preenche combo de 'Tipo de despesa'.
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author renato.jesus
	 */
	public void preencherComboTipoDespesa(DocumentHTML document,HttpServletRequest request, HttpServletResponse response)throws Exception{
		TipoMovimFinanceiraViagemService tipoService = (TipoMovimFinanceiraViagemService) ServiceLocator.getInstance().getService(TipoMovimFinanceiraViagemService.class, null);

		String classificacao = "";
		List<TipoMovimFinanceiraViagemDTO> listaTipoMovimentacaoFinanceiraViagem = new ArrayList<TipoMovimFinanceiraViagemDTO>();

		HTMLSelect comboDespesa = document.getSelectById("tipoDespesa");

		comboDespesa.removeAllOptions();
		comboDespesa.addOption("", "" + UtilI18N.internacionaliza(request, "citcorpore.comum.selecione") + "");

		for(Enumerados.ClassificacaoMovFinViagem classificacaoItem : Enumerados.ClassificacaoMovFinViagem.values()){
			classificacao = UtilStrings.removeCaracteresEspeciais(classificacaoItem.getDescricao());

			listaTipoMovimentacaoFinanceiraViagem =  tipoService.listByClassificacao(classificacao);

			for(TipoMovimFinanceiraViagemDTO tipoMov : listaTipoMovimentacaoFinanceiraViagem) {
				comboDespesa.addOption(tipoMov.getIdtipoMovimFinanceiraViagem().toString(), classificacao.trim() + " (" + (tipoMov.getNome()).trim() + ")");
			}
		}
	}

	/**
	 * Preenche combo de 'Moeda'.
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author renato.jesus
	 */
	public void preencherComboMoeda(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		MoedaService moedaService = (MoedaService) ServiceLocator.getInstance().getService(MoedaService.class, null);
        HTMLSelect comboMoeda = (HTMLSelect) document.getSelectById("idMoedaAux");

        comboMoeda.removeAllOptions();
        comboMoeda.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));

        Collection colMoedas = moedaService.findAllAtivos();
        if(colMoedas != null && !colMoedas.isEmpty()){
        	comboMoeda.addOptions(colMoedas, "idMoeda", "nomeMoeda", null);
        }

        document.getElementById("idMoeda").setValue("1");

        comboMoeda.setValue("1");
        comboMoeda.setDisabled(true);
	}

	/**
	 * Preenche combo de 'Forma de pagamento'.
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author renato.jesus
	 */
	public void preencherComboFormaPagamento(DocumentHTML document,HttpServletRequest request, HttpServletResponse response)throws Exception{
		FormaPagamentoService formaPagamentoService = (FormaPagamentoService) ServiceLocator.getInstance().getService(FormaPagamentoService.class, null);

		HTMLSelect comboFormaPagamento = document.getSelectById("idFormaPagamento");

		comboFormaPagamento.removeAllOptions();
		comboFormaPagamento.addOption("", "-- " + UtilI18N.internacionaliza(request, "citcorpore.comum.todos") + " --");

		Collection colTipoMovimentacaoFinanceira = formaPagamentoService.list();
		if(colTipoMovimentacaoFinanceira!=null){
			comboFormaPagamento.addOptions(colTipoMovimentacaoFinanceira, "idFormaPagamento", "nomeFormaPagamento", null);
		}
	}


	@Override
	public Class getBeanClass() {
		return DespesaViagemDTO.class;
	}

	/**
	 * Retorna a cidade conforme idcidade passado
	 *
	 * @param idCidade
	 * @return
	 * @throws Exception
	 * @author renato.jesus
	 */
	public String recuperaCidade(Integer idCidade) throws Exception {
		CidadesDTO cidadeDto  = new CidadesDTO();

		CidadesService cidadesService = (CidadesService) ServiceLocator.getInstance().getService(CidadesService.class, null);

		if (idCidade != null) {
			cidadeDto = (CidadesDTO) cidadesService.findCidadeUF(idCidade);
			return cidadeDto.getNomeCidade() + " - " + cidadeDto.getNomeUf();
		}

		return null;
	}

	/**
	 * Remove o bot�o adicionar itens caso o usuario n�o tenha permiss�o
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author renato.jesus
	 */
	private void removeBotoesDaTela(DocumentHTML document,HttpServletRequest request, HttpServletResponse response, UsuarioDTO usuario, DespesaViagemDTO despesaViagemDTO) throws Exception {
		SolicitacaoServicoService solicitacaoServicoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);

		SolicitacaoServicoDTO solicitacaoServicoDTO = new SolicitacaoServicoDTO();

		solicitacaoServicoDTO.setIdSolicitacaoServico(despesaViagemDTO.getIdSolicitacaoServico());
		solicitacaoServicoDTO = (SolicitacaoServicoDTO) solicitacaoServicoService.restore(solicitacaoServicoDTO);
		FluxoDTO fluxoDTO = solicitacaoServicoService.recuperaFluxo(solicitacaoServicoDTO);

		PermissoesFluxoService permissoesFluxoService = (PermissoesFluxoService) ServiceLocator.getInstance().getService(PermissoesFluxoService.class, null);
		GrupoService grupoService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);

		UsuarioDTO usuarioDTO = (UsuarioDTO) (new UsuarioDao()).restore(usuario);

		usuarioDTO.setColGrupos(grupoService.getGruposByEmpregado(usuarioDTO.getIdEmpregado()));

		PermissoesFluxoDTO permissoesFluxoDTO = permissoesFluxoService.findByUsuarioAndFluxo(usuarioDTO, fluxoDTO);

		if(permissoesFluxoDTO == null || (permissoesFluxoDTO != null && !permissoesFluxoDTO.getExecutar().equalsIgnoreCase("S"))) {
			document.executeScript("$('#add_itens').remove();$('.btn-editar-item-despesa').remove();$('.btn-excluir-item-despesa').remove();");
		}
	}


	/**
	 * Restaura a justificativa informada quando a autoriza��o � negada.
	 *
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 */
	public void restoreTitulo(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		RequisicaoViagemService reqViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, null);
		ParecerService parecerService = (ParecerService) ServiceLocator.getInstance().getService(ParecerService.class, null);
		JustificativaParecerService justificativaParecerService = (JustificativaParecerService) ServiceLocator.getInstance().getService(JustificativaParecerService.class, null);
		ParecerDTO parecerDTO = new ParecerDTO();
		JustificativaParecerDTO justificativaParecerDTO = new JustificativaParecerDTO();

		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();
		RequisicaoViagemDTO requisicaoViagemDTO = reqViagemService.recuperaRequisicaoPelaSolicitacao(despesaViagemDTO.getIdSolicitacaoServico());

		if(requisicaoViagemDTO != null && requisicaoViagemDTO.getIdAprovacao() != null){
			parecerDTO.setIdParecer(requisicaoViagemDTO.getIdAprovacao());
			parecerDTO = (ParecerDTO) parecerService.restore(parecerDTO);
			if(parecerDTO != null && parecerDTO.getAprovado().equalsIgnoreCase("N")){
				document.getElementById("titulo").setValue(UtilI18N.internacionaliza(request, "requisicaoViagem.replanejamentoReservasViagem"));
				if(parecerDTO.getIdJustificativa() != null){
					justificativaParecerDTO.setIdJustificativa(parecerDTO.getIdJustificativa());
					justificativaParecerDTO = (JustificativaParecerDTO) justificativaParecerService.restore(justificativaParecerDTO);
				}
				StringBuilder html = new StringBuilder();

				html.append("<label><b>"+UtilI18N.internacionaliza(request, "citcorpore.comum.justificativa")+":</b> ");
				html.append(justificativaParecerDTO.getDescricaoJustificativa() == null ? "" : justificativaParecerDTO.getDescricaoJustificativa());
				if(parecerDTO.getComplementoJustificativa() != null && !parecerDTO.getComplementoJustificativa().equalsIgnoreCase("")){
					html.append(" ("+parecerDTO.getComplementoJustificativa()+")");
				}
				html.append("</label>");
				document.executeScript("$('#divNaoAprovada').show()");
				document.getElementById("naoAprovada").setInnerHTML(html.toString());

			}else{
				document.getElementById("titulo").setValue(UtilI18N.internacionaliza(request, "requisicaoViagem.planejamentoReservasViagem"));
				document.executeScript("$('#divNaoAprovada').hide()");
			}
		}else{
			document.getElementById("titulo").setValue(UtilI18N.internacionaliza(request, "requisicaoViagem.planejamentoReservasViagem"));
			document.executeScript("$('#divNaoAprovada').hide()");
		}
	}

	public void carregaIntegrantePopup(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();

		IntegranteViagemService integranteViagemService = (IntegranteViagemService) ServiceLocator.getInstance().getService(IntegranteViagemService.class, WebUtil.getUsuarioSistema(request));
		RoteiroViagemService roteiroViagemService = (RoteiroViagemService) ServiceLocator.getInstance().getService(RoteiroViagemService.class, WebUtil.getUsuarioSistema(request));
		DadosBancariosIntegranteService dadosBancariosIntegranteService = (DadosBancariosIntegranteService) ServiceLocator.getInstance().getService(DadosBancariosIntegranteService.class, WebUtil.getUsuarioSistema(request));
		CidadesService cidadeService = (CidadesService) ServiceLocator.getInstance().getService(CidadesService.class, WebUtil.getUsuarioSistema(request));
		EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, WebUtil.getUsuarioSistema(request));

		IntegranteViagemDTO integranteViagemDTO = new IntegranteViagemDTO();
		EmpregadoDTO empregadoReponsavel = new EmpregadoDTO();
		RoteiroViagemDTO roteiroViagemDTO = new RoteiroViagemDTO();
		CidadesDTO cidadeOrigem = new CidadesDTO();
		CidadesDTO cidadeDestino = new CidadesDTO();
		DadosBancariosIntegranteDTO dadosBancariosIntegranteDTO = new DadosBancariosIntegranteDTO();

		Integer idIntegrante = despesaViagemDTO.getIdIntegranteAux();

		integranteViagemDTO = integranteViagemService.findById(idIntegrante);

		empregadoReponsavel = empregadoService.restoreByIdEmpregado(integranteViagemDTO.getIdRespPrestacaoContas());

		roteiroViagemDTO = roteiroViagemService.findByIdIntegrante(idIntegrante);

		cidadeOrigem = cidadeService.findByIdCidade(roteiroViagemDTO.getOrigem());

		cidadeDestino = cidadeService.findByIdCidade(roteiroViagemDTO.getDestino());

		dadosBancariosIntegranteDTO = dadosBancariosIntegranteService.findByIdIntegrante(idIntegrante);

		StringBuilder html = new StringBuilder();

		html.append("<p><b>Integrante:</b> "+integranteViagemDTO.getNome()+"</p>"+
				"<p><b>Funcionario:</b> "+integranteViagemDTO.getIntegranteFuncionario()+"</p>"+
				"<p><b>Respons�vel:</b> "+empregadoReponsavel.getNome()+"</p>"+
				"<p>&nbsp;</p>"+

				"<div class='widget'>"+
	            	"<div class=widget-head>"+
	                	"<h2 class='heading' id='titulo'>Itiner�rio</h2>"+
	                "</div><!-- .widget-head -->"+

	                "<div class='widget-body'>"+
		                "<p><b>Origem:</b> "+cidadeOrigem.getNomeCidade()+" - "+cidadeOrigem.getNomeUf()+"</p>"+
		                "<p><b>Aeroporto origem:</b> "+roteiroViagemDTO.getAeroportoOrigem()+"</p>"+
		                "<p><b>Destino:</b> "+cidadeDestino.getNomeCidade()+"</p>"+
		                "<p><b>Aeroporto destino:</b> "+roteiroViagemDTO.getAeroportoDestino()+"</p>"+
		                "<p><b>Data da ida:</b> "+UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT,roteiroViagemDTO.getIda(), UtilI18N.getLocale(request))+"</p>"+
		                "<p><b>Hora da ida:</b> "+roteiroViagemDTO.getHoraInicio()+"</p>"+
		                "<p><b>Data da volta:</b> "+UtilDatas.convertDateToString(TipoDate.DATE_DEFAULT,roteiroViagemDTO.getVolta(), UtilI18N.getLocale(request))+"</p>"+
		                "<p><b>Hora da volta:</b> "+roteiroViagemDTO.getHoraFim()+"</p>"+
		                "<p><b>Hot�is de prefer�ncia:</b> "+roteiroViagemDTO.getHoteisPreferenciais()+"</p>"+
	                "</div><!-- .widget-body -->"+
                "</div><!-- .widget -->"+


				"<p>&nbsp;</p>"+

				"<div class='widget'>"+
					"<div class=widget-head>"+
						"<h2 class='heading' id='titulo'>Dados banc�rios</h2>"+
					"</div><!-- .widget-head -->"+

					"<div class='widget-body'>"+
						"<p><b>Banco:</b> "+dadosBancariosIntegranteDTO.getBanco()+"</p>"+
						"<p><b>Ag�ncia:</b> "+dadosBancariosIntegranteDTO.getAgencia()+"</p>"+
						"<p><b>Conta:</b> "+dadosBancariosIntegranteDTO.getConta()+"</p>"+
						"<p><b>Opera��o:</b> "+dadosBancariosIntegranteDTO.getOperacao()+"</p>"+
						"<p><b>CPF:</b> "+dadosBancariosIntegranteDTO.getCpf()+"</p>"+
					"</div><!-- .widget-body -->"+
				"</div><!-- .widget -->");

		document.getElementById("dadosIntegrante").setInnerHTML(html.toString());

	}

	public void visualizarResponsaveisPopup(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();

		RequisicaoViagemService requisicaoViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, WebUtil.getUsuarioSistema(request));

		String responsaveis = requisicaoViagemService.getResponsaveisEtapaAtualRequisicao(despesaViagemDTO.getIdSolicitacaoServico(), "planejamento");

		document.getElementById("POPUP_VISUALIZARRESPONSAVEIS").setInnerHTML(responsaveis.replace(";", "<br />"));

		document.executeScript("$('#POPUP_VISUALIZARRESPONSAVEIS').dialog('open');");
	}

	public void adicionarIntegranteViagem (DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();
		request.getSession().setAttribute("idSolicitacaoServico", despesaViagemDTO.getIdSolicitacaoServico());
		request.getSession().setAttribute("addIntegranteViagem", "S");
		document.executeScript("exibirModalIntegrantes()");
	}

	public void removerAddIntegranteViagemDaSessao (DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		DespesaViagemDTO despesaViagemDTO = (DespesaViagemDTO) document.getBean();
		request.getSession().removeAttribute("addIntegranteViagem");
		
		load(document, request, response);
		
	}
}
