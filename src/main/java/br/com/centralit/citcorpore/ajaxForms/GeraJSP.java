/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.BIConsultaDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.BIConsultaColunasService;
import br.com.centralit.citcorpore.negocio.BIConsultaService;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citgerencial.util.WebUtilGerencial;
import br.com.citframework.dto.Usuario;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilI18N;
import br.com.citframework.util.UtilTratamentoArquivos;

public class GeraJSP extends AjaxFormAction{
	@Override
	public Class getBeanClass() {
		return BIConsultaDTO.class;
	}
	@Override
	public void load(DocumentHTML document, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BIConsultaDTO biConsultaParm = (BIConsultaDTO)document.getBean();
		BIConsultaService biConsultaService = (BIConsultaService) ServiceLocator.getInstance().getService(BIConsultaService.class, null);
		BIConsultaColunasService biConsultaColunasService = (BIConsultaColunasService) ServiceLocator.getInstance().getService(BIConsultaColunasService.class, null);
		Usuario user = WebUtilGerencial.getUsuario(request);
		if (user == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			return;
		}	
		UsuarioDTO userAux = br.com.centralit.citcorpore.util.WebUtil.getUsuario(request);
		BIConsultaDTO biConsultaDTO = new BIConsultaDTO();
		biConsultaDTO.setIdConsulta(biConsultaParm.getIdConsulta());
		try{
			biConsultaDTO = (BIConsultaDTO) biConsultaService.restore(biConsultaDTO);
		}catch(Exception e){
			document.alert("Consulta inexistente!");
			return;
		}
		if (biConsultaDTO == null){
			document.alert("Consulta inexistente!");
			return;
		}
		String conteudoJSP = biConsultaDTO.getScriptExec();
		/**
		 * Remove e substitui classes para n�o causar erro de compila��o
		 */
		conteudoJSP = filtraJsp(conteudoJSP);
		if (conteudoJSP == null || conteudoJSP.trim().equalsIgnoreCase("")){
			conteudoJSP = biConsultaDTO.getTemplate();
		}
		File f = new File(CITCorporeUtil.CAMINHO_REAL_APP + "/jspEmbedded");
		if (!f.exists()){
			f.mkdirs();
		}		
		f = new File(CITCorporeUtil.CAMINHO_REAL_APP + "/jspEmbedded/" + userAux.getIdUsuario());
		if (!f.exists()){
			f.mkdirs();
		}
		
		String strPathTemplate = CITCorporeUtil.CAMINHO_REAL_APP + "/jspEmbedded/" + userAux.getIdUsuario() + "/jsp_" + biConsultaDTO.getIdConsulta() + "_process.jsp"; 
		UtilTratamentoArquivos.geraFileTxtFromString(strPathTemplate, conteudoJSP);		
		request.setAttribute("url", Constantes.getValue("CONTEXTO_APLICACAO") + "/jspEmbedded/" + userAux.getIdUsuario() + "/jsp_" + biConsultaDTO.getIdConsulta() + "_process.jsp");
	}
	
	/**
	 * Filta conte�do do JSP para remover as tags e elementos que n�o s�o mais utilizados
	 * 
	 * @author thyen.chang
	 * @since 16/06/2015
	 * @param jsp
	 * @return
	 */
	private String filtraJsp(String jsp){
		/**
		 * Classe noCache.jsp foi excluida na Opera��o Usain Bolt
		 */
		jsp = removeTag(jsp, "noCache.jsp");
		/**
		 * Tags cit e i18n foram removidas na Opera��o Usain Bolt
		 */
		jsp = removeTag(jsp, "tags/cit");
		jsp = removeTag(jsp, "tags/i18n");
		/**
		 * Adicionando import java da classe UtilI18N, caso n�o exista no c�digo
		 */
		jsp = adicionaImportI18N(jsp);
		/**
		 * Tags i18n foram substituidas pelas tags fmt
		 */
		jsp = jsp.replaceAll("i18n:message", "fmt:message");
		/**
		 * Constante caminho_real_app foi renomeada para CAMINHO_REAL_APP
		 */
		jsp = jsp.replaceAll("caminho_real_app", "CAMINHO_REAL_APP");
		return jsp;
	}
	
	/**
	 * Remove a linha  do jsp contendo a tag
	 * @author thyen.chang
	 * @since 16/03/2015
	 * @param jsp
	 * @param tag
	 * @return
	 */
	private String removeTag(String jsp, String tag){
 		int index = jsp.indexOf(tag);
 		if(index != -1){
			String antes = jsp.substring(0, index + 1), depois = jsp.substring(index, jsp.length());
			int inicio = antes.lastIndexOf("<%"), fim = depois.indexOf("%>") + 2;
			jsp = jsp.substring(0, inicio) + jsp.substring(index + fim, jsp.length());
 		}
		return jsp;
	}
	
	/**
	 * Adiciona import da classe UtilI18N caso n�o exista
	 * @author thyen.chang
	 * @since 16/03/2015
	 * @param jsp
	 * @return
	 */
	private String adicionaImportI18N(String jsp){
		int index = jsp.indexOf("br.com.citframework.util.UtilI18N");
		if(index == -1){
			jsp = "<%@page import=\"br.com.citframework.util.UtilI18N\"%>" + jsp;
		}
		return jsp;
	}
}
