/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLElement;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citajax.html.HTMLTable;
import br.com.centralit.citcorpore.bean.CidadesDTO;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.FuncionarioDTO;
import br.com.centralit.citcorpore.bean.IntegranteViagemDTO;
import br.com.centralit.citcorpore.bean.JustificativaSolicitacaoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoViagemDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.CentroResultadoService;
import br.com.centralit.citcorpore.negocio.CidadesService;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.GrupoEmpregadoService;
import br.com.centralit.citcorpore.negocio.JustificativaParecerService;
import br.com.centralit.citcorpore.negocio.JustificativaSolicitacaoService;
import br.com.centralit.citcorpore.negocio.ProjetoService;
import br.com.centralit.citcorpore.negocio.RequisicaoViagemService;
import br.com.centralit.citcorpore.rh.negocio.FuncionarioService;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilI18N;

/**
 * @author thays.araujo
 * 
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class RequisicaoViagem extends AjaxFormAction {
	
	private RequisicaoViagemService reqViagemService;

	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		UsuarioDTO usuario = WebUtil.getUsuario(request);

		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}
		
		RequisicaoViagemDTO requisicaoViagemDto = (RequisicaoViagemDTO) document.getBean();
		
		if(request.getSession().getAttribute("addIntegranteViagem") != null && request.getSession().getAttribute("addIntegranteViagem").equals("S")){
			requisicaoViagemDto.setIdSolicitacaoServico((Integer) request.getSession().getAttribute("idSolicitacaoServico"));
			requisicaoViagemDto.setIdContrato((Integer) request.getSession().getAttribute("idContratoViagem"));
			document.executeScript("$('#divAdicionarItegrantes').css('display','block')");
		} 

		this.preencherComboProjeto(document, request, response, requisicaoViagemDto);
		this.preencherComboCentroResultado(document, request, response);
		this.preencherComboJustificativa(document, request, response);
		this.preencherComboFinalidade(document, request, response);
		
		if (requisicaoViagemDto.getIdSolicitacaoServico() != null) {
			restore(document, request, response, requisicaoViagemDto);
		} 

		requisicaoViagemDto.setIntegranteViagemSerialize(null);
	}

	public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response, RequisicaoViagemDTO requisicaoViagemDto) throws ServiceException, Exception {

		RequisicaoViagemService reqViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, null);

		if (requisicaoViagemDto.getIdSolicitacaoServico() != null) {
			requisicaoViagemDto = (RequisicaoViagemDTO) reqViagemService.restore(requisicaoViagemDto);

			// Limpando os campos na tela
			requisicaoViagemDto.setDataInicioViagem(null);
			requisicaoViagemDto.setDataFimViagem(null);
			
		}
		
		this.montarGridIntegrateViagem(document, request, response);

		HTMLForm form = document.getForm("form");
		form.clear();
		form.setValues(requisicaoViagemDto);

	}

	@Override
	public Class getBeanClass() {
		return RequisicaoViagemDTO.class;
	}
	
	/**
	 * Preenche a combo de 'finalidade' do formul�rio HTML
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author thays.araujo
	 */
	public void preencherComboFinalidade(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HTMLSelect finalidade = (HTMLSelect) document.getSelectById("finalidade");
		finalidade.removeAllOptions();
		finalidade.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
        finalidade.addOption("I", UtilI18N.internacionaliza(request, "requisicaoProduto.finalidade.usoInterno"));
		finalidade.addOption("C", UtilI18N.internacionaliza(request, "requisicaoProduto.finalidade.atendimentoCliente"));
	}

	/**
	 * Preenche a combo de 'Centro Resultado' do formul�rio HTML
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author thays.araujo
	 */
	public void preencherComboCentroResultado(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		CentroResultadoService centroResultadoService = (CentroResultadoService) ServiceLocator.getInstance().getService(CentroResultadoService.class, WebUtil.getUsuarioSistema(request));
		HTMLSelect idCentroCusto = (HTMLSelect) document.getSelectById("idCentroCusto");
		idCentroCusto.removeAllOptions();
		
		idCentroCusto.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
		
		Collection colCentroCusto = centroResultadoService.listPermiteRequisicaoProduto(false, false);
		
		if (colCentroCusto != null && !colCentroCusto.isEmpty()) {
			idCentroCusto.addOptions(colCentroCusto, "idCentroResultado", "nomeHierarquizado", null);
		}
	}

	/**
	 * Preenche a combo de 'Projeto' do formul�rio HTML
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @param requisicaoViagemDto
	 * @throws Exception
	 * @author thays.araujo
	 */
	public void preencherComboProjeto(DocumentHTML document, HttpServletRequest request, HttpServletResponse response, RequisicaoViagemDTO requisicaoViagemDto) throws Exception {
		HTMLSelect idProjeto = (HTMLSelect) document.getSelectById("idProjeto");
		idProjeto.removeAllOptions();
		idProjeto.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
		if (requisicaoViagemDto.getIdContrato() != null) {
			ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, WebUtil.getUsuarioSistema(request));
			ContratoDTO contratoDto = new ContratoDTO();
			contratoDto.setIdContrato(requisicaoViagemDto.getIdContrato());
			contratoDto = (ContratoDTO) contratoService.restore(contratoDto);
			if (contratoDto != null) {
				ProjetoService projetoService = (ProjetoService) ServiceLocator.getInstance().getService(ProjetoService.class, WebUtil.getUsuarioSistema(request));
				Collection colProjetos = projetoService.listHierarquia(contratoDto.getIdCliente(), true);
				if (colProjetos != null && !colProjetos.isEmpty())
					idProjeto.addOptions(colProjetos, "idProjeto", "nomeHierarquizado", null);
			}
		}
	}

	/**
	 * Preenche as combos de 'Cidade Origem' e Cidade Destino.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author thays.araujo
	 */
	public void preencherComboCidades(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		CidadesService cidadesService = (CidadesService) ServiceLocator.getInstance().getService(CidadesService.class, null);

		HTMLSelect comboCidadeOrigem = (HTMLSelect) document.getSelectById("idCidadeOrigem");
		HTMLSelect comboCidadeDestino = (HTMLSelect) document.getSelectById("idCidadeDestino");

		ArrayList<CidadesDTO> listCidade = (ArrayList) cidadesService.list();

		this.inicializaCombo(comboCidadeOrigem, request);
		this.inicializaCombo(comboCidadeDestino, request);
		if (listCidade != null) {
			comboCidadeOrigem.addOptions(listCidade, "idCidade", "nomeCidade", null);
			comboCidadeDestino.addOptions(listCidade, "idCidade", "nomeCidade", null);
		}
	}

	/**
	 * Preenche combo de 'justificativa solicita��o'.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author thays.araujo
	 */
	public void preencherComboJustificativa(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		JustificativaSolicitacaoService justificativaSolicitacaoService = (JustificativaSolicitacaoService) ServiceLocator.getInstance().getService(JustificativaSolicitacaoService.class, null);

		Collection<JustificativaSolicitacaoDTO> colJustificativas = justificativaSolicitacaoService.listAtivasParaViagem();

		HTMLSelect comboJustificativa = (HTMLSelect) document.getSelectById("idMotivoViagem");
		document.getSelectById("idMotivoViagem").removeAllOptions();
		inicializaCombo(comboJustificativa, request);
		if (colJustificativas != null) {
			comboJustificativa.addOptions(colJustificativas, "idJustificativa", "descricaoJustificativa", null);
		}
	}

	/**
	 * Executa uma inicializa��o padr�o para as combos. Basicamente deleta todas as op��es, caso haja, e insere aprimeira linha com o valor "-- Selecione --".
	 * 
	 * @param componenteCombo
	 * @param request
	 */
	public void inicializaCombo(HTMLSelect componenteCombo, HttpServletRequest request) {
		componenteCombo.removeAllOptions();
		componenteCombo.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
	}

	/**
	 * Recupera cidade conforme idcidade passado
	 * 
	 * @param idCidade
	 * @return
	 * @throws Exception
	 */
	public String recuperaCidade(Integer idCidade) throws Exception {
		CidadesDTO cidadeDto = new CidadesDTO();
		CidadesService cidadesService = (CidadesService) ServiceLocator.getInstance().getService(CidadesService.class, null);
		if (idCidade != null) {
			cidadeDto = (CidadesDTO) cidadesService.findCidadeUF(idCidade);
			return cidadeDto.getNomeCidade() + " - " + cidadeDto.getNomeUf();
		}
		return null;
	}


	/**
	 * Preenche combo de 'justificativa solicita��o ' para autoziza��o.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author thays.araujo
	 */
	public void preencherComboJustificativaAutorizacao(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		JustificativaParecerService justificativaService = (JustificativaParecerService) ServiceLocator.getInstance().getService(JustificativaParecerService.class, WebUtil.getUsuarioSistema(request));

		Collection colJustificativas = justificativaService.listAplicaveisRequisicao();

		HTMLSelect comboJustificativaAutorizacao = (HTMLSelect) document.getSelectById("idJustificativaAutorizacao");

		document.getSelectById("idJustificativaAutorizacao").removeAllOptions();

		inicializaCombo(comboJustificativaAutorizacao, request);

		if (colJustificativas != null) {
			comboJustificativaAutorizacao.addOptions(colJustificativas, "idJustificativa", "descricaoJustificativa", null);
		}
	}

	/**
	 * Restaura a grid de integrantes
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 */
	public void montarGridIntegrateViagem(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {

		RequisicaoViagemDTO requisicaoViagemDto = (RequisicaoViagemDTO) document.getBean();

		RequisicaoViagemService reqViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, null);

		Collection<IntegranteViagemDTO> colIntegrantes = reqViagemService.recuperaIntegrantesViagemBySolicitacao(requisicaoViagemDto.getIdSolicitacaoServico());

		if (colIntegrantes != null) {
			HTMLTable tblIntegranteViagem;
			tblIntegranteViagem = document.getTableById("tblIntegranteViagem");
			tblIntegranteViagem.deleteAllRows();
			tblIntegranteViagem.addRowsByCollection(colIntegrantes, new String[] { "nome", "integranteFuncionario", "respPrestacaoContas", "nomeNaoFuncionario", "" }, null, null, new String[] { "gerarButtonEditDelete" }, null, null);
		}
	}
	
	
	/**
	 * Atualiza os integrantes da viagem
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 * 
	 */
	public void atualizarNovosIntegrantesViagem(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws ServiceException, Exception {
		
		UsuarioDTO usuario = WebUtil.getUsuario(request);
		
		RequisicaoViagemDTO requisicaoViagemDto = (RequisicaoViagemDTO) document.getBean();
		
		if(requisicaoViagemDto.getIntegranteViagemSerialize() == null || requisicaoViagemDto.getIntegranteViagemSerialize().equals("")){
			document.alert(UtilI18N.internacionaliza(request, "requisicaoViagem.integranteViagemCampoObrigatorio"));
			return;
		}
		
		RequisicaoViagemService reqViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, null);
		List<String> listaDelecao = obterListaParaExclusaoDosIntegrantes(document);
		reqViagemService.tratarIntegranteSolicitacaoViagem(requisicaoViagemDto, listaDelecao, usuario);

		//Fechar frame na tela de despesa viagem
		document.executeScript("fecharFrame();");	
	}
	
	
	/**
	 * Fecha a popup de alteracao dos integrantes da viagem
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 * 
	 */
	public void fecharAlteracaoIntegranteViagem(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws ServiceException, Exception {
		
		request.getSession().removeAttribute("addIntegranteViagem");
		
		HTMLTable tblIntegranteViagem;
		tblIntegranteViagem = document.getTableById("tblIntegranteViagem");
		tblIntegranteViagem.deleteAllRows();
		
		// Limpar formul�rio
		HTMLForm form = document.getForm("form");
		form.clear();
		
	}
	
	private List<String> obterListaParaExclusaoDosIntegrantes(final DocumentHTML document) throws Exception {
		final HTMLElement element = document.getElementById("colItens_Delecao_IntegranteViagem");
		final Map<String, String> mapValores = element.getDocument().getValuesForm();
		// Elemento no formulario
		final String valorElemento = StringUtils.removeEnd(mapValores.get("COLITENS_DELECAO_INTEGRANTEVIAGEM"), ",");
		List<String> listaretorno = new ArrayList<String>();
		if (StringUtils.isNotEmpty(valorElemento)) {
			listaretorno = Arrays.asList(valorElemento.split(","));
		}
		return listaretorno;
	}


	/**
	 * Verifica se o empregado esta no grupo que permite a atribui��o de contas a outro empregado, se sim, habilita a div com os campos para atribui��o, caso contrario esconde a div
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 */
	public void validaAtribuicao(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {

		RequisicaoViagemDTO requisicaoViagemDto = (RequisicaoViagemDTO) document.getBean();
		Integer idGrupo;
		
		if (requisicaoViagemDto != null && requisicaoViagemDto.getIdEmpregado() != null) {

			try {
				idGrupo = Integer.parseInt(ParametroUtil.getValor(ParametroSistema.GRUPO_PERMISSAO_DELEGAR_PRESTACAO_VIAGEM));
			} catch (Exception e) {
				document.executeScript("$('#divResponsavelEmpregado').hide();");
				return;
			}

			GrupoEmpregadoService grupoEmpregadoService = (GrupoEmpregadoService) ServiceLocator.getInstance().getService(GrupoEmpregadoService.class, null);

			if (grupoEmpregadoService.grupoempregado(requisicaoViagemDto.getIdEmpregado(), idGrupo))
				document.executeScript("$('#divResponsavelEmpregado').show();");
			else
				document.executeScript("$('#divResponsavelEmpregado').hide();");

		} else
			document.executeScript("$('#divResponsavelEmpregado').hide();");
	}
	
	/**
	 * seta dados bancarios
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws ServiceException
	 * @throws Exception
	 */
	public void setaDadosBancarios(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		RequisicaoViagemDTO requisicaoViagemDto = (RequisicaoViagemDTO) document.getBean();
		
		FuncionarioDTO funcionarioDTO = new FuncionarioDTO();
		
		FuncionarioService funcionarioService = (FuncionarioService) ServiceLocator.getInstance().getService(FuncionarioService.class, null);

		if (requisicaoViagemDto != null && requisicaoViagemDto.getIdEmpregado() != null) {
			
			funcionarioDTO = funcionarioService.findByIdEmpregado(requisicaoViagemDto.getIdEmpregado());
			document.getElementById("banco").setValue(funcionarioDTO.getBanco());
			document.getElementById("agencia").setValue(funcionarioDTO.getAgencia());
			document.getElementById("conta").setValue(funcionarioDTO.getConta());
			document.getElementById("cpf").setValue(funcionarioDTO.getCpf());
		}
	}
	
	/**
	 * Metodo cria noa funcionario na tabela de empregados caso n�o encontre uma referencia no autocomplete
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void createNaoFuncionario(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		RequisicaoViagemDTO requisicaoViagemDto = (RequisicaoViagemDTO) document.getBean();
		
		EmpregadoDTO empregadoDto = new EmpregadoDTO();
		EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
		
		
		empregadoDto.setNome(requisicaoViagemDto.getNomeNaoFuncionario());
		empregadoDto.setObservacoes(requisicaoViagemDto.getInfoNaoFuncionario());
		empregadoDto.setTipo("N");
		empregadoDto.setIdSituacaoFuncional(1);
		
		if(requisicaoViagemDto.getIdEmpregado() == null || requisicaoViagemDto.getIdEmpregado().equals("")){
			empregadoDto = (EmpregadoDTO) empregadoService.create(empregadoDto);
		}else{
			empregadoDto.setIdEmpregado(requisicaoViagemDto.getIdEmpregado());
			empregadoService.update(empregadoDto);
			document.getElementById("nomeNaoFuncionario").setValue(empregadoDto.getNome());
			document.getElementById("nomeNaoFuncionarioAux").setValue(empregadoDto.getNome());
			document.getElementById("infoNaoFuncionario").setValue(empregadoDto.getObservacoes());
			document.getElementById("infoNaoFuncionarioAux").setValue(empregadoDto.getObservacoes());
		}
		
		document.getElementById("idEmpregado").setValue(empregadoDto.getIdEmpregado().toString());
		document.executeScript("adicionarEmpregado()");
		
		document.executeScript("decodificaTextarea();");
	}
	
	/**
	 * Restaura as informa��es do n�o funcionario caso tenha informa��es
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void restoreInfNaoFuncionario(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		RequisicaoViagemDTO requisicaoViagemDto = (RequisicaoViagemDTO) document.getBean();
		
		EmpregadoDTO empregadoDto = new EmpregadoDTO();
		EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
		
		empregadoDto = (EmpregadoDTO) empregadoService.restoreByIdEmpregado(requisicaoViagemDto.getIdEmpregado());
		
		document.getElementById("infoNaoFuncionario").setValue(empregadoDto.getObservacoes().toString());
		document.getElementById("infoNaoFuncionarioAux").setValue(empregadoDto.getObservacoes().toString());
		
		document.executeScript("decodificaTextarea();");
	}
	
	public void guardarListaSerializadaSessao(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		request.getSession().setAttribute("tblIntegranteNovoViagemSession", null);
		if(request.getSession().getAttribute("addIntegranteViagem") != null && request.getSession().getAttribute("addIntegranteViagem").equals("S")){
			HTMLTable tblIntegranteViagem;
			tblIntegranteViagem = document.getTableById("tblIntegranteViagem");
			if(tblIntegranteViagem != null){
				request.getSession().setAttribute("tblIntegranteNovoViagemSession", tblIntegranteViagem);
			}
		}
	}

	public RequisicaoViagemService getReqViagemService() throws ServiceException {
		if(reqViagemService == null){
			reqViagemService = (RequisicaoViagemService) ServiceLocator.getInstance().getService(RequisicaoViagemService.class, null);
		}
		return reqViagemService;
	}

}
