/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.HistoricoAtendimentoDTO;
import br.com.centralit.citcorpore.bean.result.HistoricoAtendimentoResultDTO;
import br.com.centralit.citcorpore.negocio.HistoricoAtendimentoService;
import br.com.centralit.citcorpore.negocio.HistoricoAtendimentoServiceEjb;
import br.com.centralit.citcorpore.util.Enumerados.SituacaoSolicitacaoServicoNaRota;
import br.com.citframework.util.UtilI18N;

/**
 * Controlador para visualiza��o de hist�rico de atendimento
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 06/10/2014
 */
public class HistoricoAtendimento extends AbstractGestaoForcaAtendimento<HistoricoAtendimentoDTO, HistoricoAtendimentoResultDTO> {

    @Override
    public Class<HistoricoAtendimentoDTO> getBeanClass() {
        return HistoricoAtendimentoDTO.class;
    }

    @Override
    public void load(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        super.load(document, request, response);

        this.loadComboSituacoes(document, request);

        this.loadComboUFs(document, request, response);

        final List<HTMLSelect> combos = new ArrayList<>();
        combos.add(document.getSelectById(ID_CIDADE));
        combos.add(document.getSelectById(ID_GRUPO));
        combos.add(document.getSelectById(ID_CONTRATO));
        this.inicializarCombosOnLoad(combos, request);
    }

    @Override
    protected List<HistoricoAtendimentoResultDTO> getListResultSearch(final DocumentHTML document, final HttpServletRequest request) throws Exception {
        final HistoricoAtendimentoDTO normalizedFilter = this.normalizeDates((HistoricoAtendimentoDTO) document.getBean());
        return this.getService().listHistoricoAtendimentoWithSolicitationInfo(normalizedFilter);
    }

    private void loadComboSituacoes(final DocumentHTML document, final HttpServletRequest request) throws Exception {
        final HTMLSelect comboSituacoes = document.getSelectById("idSituacao");
        this.inicializaCombo(comboSituacoes, request);

        final SituacaoSolicitacaoServicoNaRota[] situacoes = SituacaoSolicitacaoServicoNaRota.values();

        for (final SituacaoSolicitacaoServicoNaRota situacao : situacoes) {
            comboSituacoes.addOption(situacao.getId().toString(), UtilI18N.internacionaliza(request, situacao.getDescription()));
        }
    }

    private HistoricoAtendimentoService service;

    private HistoricoAtendimentoService getService() {
        if (service == null) {
            service = new HistoricoAtendimentoServiceEjb();
        }
        return service;
    }

}
