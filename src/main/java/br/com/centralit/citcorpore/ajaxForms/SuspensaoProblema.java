/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.JustificativaProblemaDTO;
import br.com.centralit.citcorpore.bean.ProblemaDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.JustificativaProblemaService;
import br.com.centralit.citcorpore.negocio.ProblemaService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilI18N;
@SuppressWarnings("rawtypes")
public class SuspensaoProblema extends AjaxFormAction {

   
	@Override
    public Class getBeanClass() {
	return ProblemaDTO.class;
    }

    @Override
    public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null){
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}
		HTMLForm form = document.getForm("form");
		form.clear();	
		ProblemaDTO problemaDto = (ProblemaDTO)document.getBean();
		ProblemaService problemaService = (ProblemaService) ServiceLocator.getInstance().getService(ProblemaService.class, WebUtil.getUsuarioSistema(request));
		JustificativaProblemaService justificativaProblemaService = (JustificativaProblemaService) ServiceLocator.getInstance().getService(JustificativaProblemaService.class, null);
		if(problemaDto.getIdProblema()!=null){
			problemaDto = problemaService.restoreAll(problemaDto.getIdProblema());
		}
		
		request.setAttribute("dataHoraCaptura", problemaDto.getDataHoraCaptura());
		
		Collection<JustificativaProblemaDTO> colJustificativas = justificativaProblemaService.listAtivasParaSuspensao();
		
		HTMLSelect comboJustificativa = (HTMLSelect) document.getSelectById("idJustificativaProblema");
		document.getSelectById("idJustificativaProblema").removeAllOptions();
		comboJustificativa.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
		if (colJustificativas != null){
			for(JustificativaProblemaDTO justificativa : colJustificativas){
				comboJustificativa.addOption(justificativa.getIdJustificativaProblema().toString(), StringEscapeUtils.escapeJavaScript(justificativa.getDescricaoProblema()));
			}
		}
		form.setValues(problemaDto);
		
		//quando chamado pelo gerenciamento problema, deve-se fechar a janela de aguarde
		document.executeScript("parent.JANELA_AGUARDE_MENU.hide()");
    }
    
    public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
    	UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}
		
		ProblemaDTO problemaAuxDto = (ProblemaDTO)document.getBean();	
		if (problemaAuxDto.getIdProblema() == null)
			return;

		if (problemaAuxDto.getIdJustificativaProblema() == null) {
			document.alert(UtilI18N.internacionaliza(request,"gerenciaservico.suspensaosolicitacao.validacao.justificanaoinformada"));
			return;
		}

		ProblemaService problemaService = (ProblemaService) ServiceLocator.getInstance().getService(ProblemaService.class, WebUtil.getUsuarioSistema(request));
		ProblemaDTO problemaDto = problemaService.restoreAll(problemaAuxDto.getIdProblema());
		problemaDto.setIdJustificativaProblema(problemaAuxDto.getIdJustificativaProblema());
		problemaDto.setComplementoJustificativa(problemaAuxDto.getComplementoJustificativa());
		problemaService.suspende(usuario, problemaDto);
      	        document.executeScript("fechar();");
      	        document.executeScript("JANELA_AGUARDE_MENU.hide()");
    }
}
