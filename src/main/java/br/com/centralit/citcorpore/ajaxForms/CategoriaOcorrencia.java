/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.CategoriaOcorrenciaDTO;
import br.com.centralit.citcorpore.negocio.CategoriaOcorrenciaService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

/**
 * @author thiago.monteiro
 * 
 */
@SuppressWarnings("rawtypes")
public class CategoriaOcorrencia extends AjaxFormAction {
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		document.focusInFirstActivateField(null);
	}

	@Override
	public Class getBeanClass() {
		return CategoriaOcorrenciaDTO.class;
	}

	/**
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */

	public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		// Cria uma inst�ncia de CategoriaOcorrenciaDTO com os dados
		// provenientes do formul�rio
		// na p�gina JSP.
		CategoriaOcorrenciaDTO categoriaOcorrenciaDTO = (CategoriaOcorrenciaDTO) document.getBean();
		CategoriaOcorrenciaService categoriaOcorrenciaService = (CategoriaOcorrenciaService) ServiceLocator.getInstance().
				getService(CategoriaOcorrenciaService.class, null);		
		
		// Verifica se o DTO e o servi�o existem
		if (categoriaOcorrenciaDTO != null && categoriaOcorrenciaService != null) {
			categoriaOcorrenciaDTO.setDataInicio(UtilDatas.getDataAtual() );			
			// Inserir		
			if (categoriaOcorrenciaDTO.getIdCategoriaOcorrencia() == null) {
				categoriaOcorrenciaService.create(categoriaOcorrenciaDTO);
				document.alert(UtilI18N.internacionaliza(request, "MSG05") );
			} else {
				// Atualiar			
				categoriaOcorrenciaService.update(categoriaOcorrenciaDTO);
				document.alert(UtilI18N.internacionaliza(request, "MSG06") );
			}
			HTMLForm form = document.getForm("formCategoriaOcorrencia");
			form.clear();
		}
	}

	/**
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */

	public void delete(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		CategoriaOcorrenciaDTO categoriaOcorrenciaDTO = (CategoriaOcorrenciaDTO) document.getBean();
		CategoriaOcorrenciaService categoriaOcorrenciaService = (CategoriaOcorrenciaService) ServiceLocator.getInstance().
				getService(CategoriaOcorrenciaService.class, WebUtil.getUsuarioSistema(request) );
		
		if (categoriaOcorrenciaDTO != null & categoriaOcorrenciaService != null) {
			if (categoriaOcorrenciaDTO.getIdCategoriaOcorrencia() != null && (categoriaOcorrenciaDTO.getIdCategoriaOcorrencia().intValue() > 0) ) {
				categoriaOcorrenciaService.deletarCategoriaOcorrencia(categoriaOcorrenciaDTO, document);
				HTMLForm form = document.getForm("formCategoriaOcorrencia");
				form.clear();
			}
		}
	}

	/**
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		// Recupera o objeto que cont�m os dados preenchidos no formul�rio
		CategoriaOcorrenciaDTO categoriaOcorrenciaDTO = (CategoriaOcorrenciaDTO) document.getBean();
		CategoriaOcorrenciaService categoriaOcorrenciaService = (CategoriaOcorrenciaService) ServiceLocator.getInstance().
				getService(CategoriaOcorrenciaService.class, null);
		
		if (categoriaOcorrenciaDTO != null && categoriaOcorrenciaService != null) {
			// Recupera dados a partir do banco de dados
			categoriaOcorrenciaDTO = (CategoriaOcorrenciaDTO) categoriaOcorrenciaService.restore(categoriaOcorrenciaDTO);		
			HTMLForm form = document.getForm("formCategoriaOcorrencia");
			form.clear();
			form.setValues(categoriaOcorrenciaDTO);
		}
	}
}
