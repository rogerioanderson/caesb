/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.ChatSmartDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.util.WebUtil;

public class ChatSmart extends AjaxFormAction {
	UsuarioDTO usuario = null;

	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		this.usuario = WebUtil.getUsuario(request);
	}

	public synchronized void fechaTelaChat(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) {
		System.out.println("Entrei aq-----------------------------------------------------");
	    ChatSmartDTO bean =(ChatSmartDTO) document.getBean();
		String nome = "";
		String listaUsuarios = (String)request.getSession().getAttribute("listaChat"); 
		 if(listaUsuarios!=null && !listaUsuarios.trim().equalsIgnoreCase("")){ 
			String[] array = listaUsuarios.split("#");
			for(String nomeUsuario : array){
				if(!bean.getNomeUsuarioConversando().equals(nomeUsuario)){
					nome += nomeUsuario+"#";
				}
			}
			request.getSession().setAttribute("listaChat",nome);
			System.out.println(request.getSession().getAttribute("listaChat"));
		 } 
	}

	@Override
	public Class getBeanClass() {
		return ChatSmartDTO.class;
	}

}
