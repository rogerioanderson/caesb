/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.bpm.dto.TarefaFluxoDTO;
import br.com.centralit.bpm.util.Enumerados;
import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citajax.html.HTMLTable;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.TipoDemandaServicoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.mail.MensagemEmail;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.ExecucaoSolicitacaoService;
import br.com.centralit.citcorpore.negocio.GrupoService;
import br.com.centralit.citcorpore.negocio.SolicitacaoServicoService;
import br.com.centralit.citcorpore.negocio.TipoDemandaServicoService;
import br.com.centralit.citcorpore.negocio.UsuarioService;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilI18N;
import br.com.citframework.util.UtilStrings;

/**
 * 
 * @author citsmartITSMs
 *
 */
public class DelegacaoTarefa extends AjaxFormAction {

	private EmpregadoService empregadoService;
	private SolicitacaoServicoService solicitacaoServicoService;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Class getBeanClass() {
		return SolicitacaoServicoDTO.class;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void load(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final UsuarioDTO usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}
		SolicitacaoServicoDTO solicitacaoServicoDto = (SolicitacaoServicoDTO) document.getBean();
		if (solicitacaoServicoDto.getIdTarefa() == null) {
			return;
		}

		/* Recebendo os itens antes do restore */
		final Integer idTarefa = solicitacaoServicoDto.getIdTarefa();

		final ExecucaoSolicitacaoService execucaoSolicitacaoService = (ExecucaoSolicitacaoService) ServiceLocator.getInstance().getService(ExecucaoSolicitacaoService.class, null);

		final TarefaFluxoDTO tarefaFluxoDto = execucaoSolicitacaoService.recuperaTarefa(usuario.getLogin(), idTarefa);

		request.setAttribute("nomeTarefa", tarefaFluxoDto.getElementoFluxoDto().getDocumentacao());

		final HTMLForm form = document.getForm("form");
		form.clear();

		final HTMLSelect idGrupoDestino = document.getSelectById("idGrupoDestino");
		idGrupoDestino.removeAllOptions();
		idGrupoDestino.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione"));
		final GrupoService grupoSegurancaService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);

		final SolicitacaoServicoService solicitacaoServicoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class,
				WebUtil.getUsuarioSistema(request));
		solicitacaoServicoDto = solicitacaoServicoService.restoreAll(solicitacaoServicoDto.getIdSolicitacaoServico());

		Collection listGruposContrato = null;

		if (solicitacaoServicoDto != null) {

			if (solicitacaoServicoDto.getIdContrato() != null) {
				listGruposContrato = grupoSegurancaService.listGruposPorUsuario(usuario.getIdUsuario());
			}

			if (listGruposContrato != null) {
				idGrupoDestino.addOptions(listGruposContrato, "idGrupo", "nome", null);
			}

			request.setAttribute("dataHoraSolicitacao", solicitacaoServicoDto.getDataHoraSolicitacaoStr());
			solicitacaoServicoDto.setIdTarefa(idTarefa);
			solicitacaoServicoDto.setAcaoFluxo(Enumerados.ACAO_DELEGAR);

			form.setValues(solicitacaoServicoDto);
		}
	}
	
	/**
	 * Delegar tarefa. 
	 **/
	public void save(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final UsuarioDTO usuario = WebUtil.getUsuario(request);
		
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		SolicitacaoServicoDTO solicitacaoServicoDto = (SolicitacaoServicoDTO) document.getBean();

		if (solicitacaoServicoDto.getIdTarefa() == null || (solicitacaoServicoDto.getIdUsuarioDestino() == null && solicitacaoServicoDto.getIdGrupoDestino() == null)) {
			return;
		}

		String usuarioDestino = null;
		String grupoDestino = null;
		UsuarioDTO usuarioDestinoDto = new UsuarioDTO();

		if (solicitacaoServicoDto.getIdUsuarioDestino() != null) {
			final UsuarioService usuarioService = (UsuarioService) ServiceLocator.getInstance().getService(UsuarioService.class, null);
			usuarioDestinoDto.setIdUsuario(solicitacaoServicoDto.getIdUsuarioDestino());
			usuarioDestinoDto = (UsuarioDTO) usuarioService.restore(usuarioDestinoDto);
			if (usuarioDestinoDto != null) {
				usuarioDestino = usuarioDestinoDto.getLogin();
			}
		}

		GrupoDTO grupoDestinoDto = null;
		if (solicitacaoServicoDto.getIdGrupoDestino() != null) {
			final GrupoService grupoService = (GrupoService) ServiceLocator.getInstance().getService(GrupoService.class, null);
			grupoDestinoDto = new GrupoDTO();
			grupoDestinoDto.setIdGrupo(solicitacaoServicoDto.getIdGrupoDestino());
			grupoDestinoDto = (GrupoDTO) grupoService.restore(grupoDestinoDto);
			if (grupoDestinoDto != null) {
				grupoDestino = grupoDestinoDto.getSigla();
			}
		}

		try {
			final String enviarNotificacao = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.NOTIFICAR_GRUPO_RECEPCAO_SOLICITACAO, "N");
			if (enviarNotificacao.equalsIgnoreCase("S") && grupoDestinoDto != null) {

				this.enviaEmailGrupo(request, Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.ID_MODELO_EMAIL_GRUPO_DESTINO, null)),
						grupoDestinoDto, solicitacaoServicoDto);
			}
			// Caso n�o tenha um grupo e tenha apenas o usu�rio de destino
			else if (solicitacaoServicoDto.getIdUsuarioDestino() != null) {

				final String remetente = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_RemetenteNotificacoesSolicitacao, null);
				if (remetente == null) {
					throw new LogicException(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
				}
				final Integer idTarefa = solicitacaoServicoDto.getIdTarefa();
				solicitacaoServicoDto = this.getSolicitacaoServicoService(request).restoreAll(solicitacaoServicoDto.getIdSolicitacaoServico());
				solicitacaoServicoDto.setIdTarefa(idTarefa);
				final TipoDemandaServicoService tipoDemandaServicoService = (TipoDemandaServicoService) ServiceLocator.getInstance().getService(TipoDemandaServicoService.class,
						WebUtil.getUsuarioSistema(request));
				TipoDemandaServicoDTO tipoDemandaServicoDTO = new TipoDemandaServicoDTO();

				if (solicitacaoServicoDto.getIdTipoDemandaServico() != null) {
					tipoDemandaServicoDTO.setIdTipoDemandaServico(solicitacaoServicoDto.getIdTipoDemandaServico());
					tipoDemandaServicoDTO = (TipoDemandaServicoDTO) tipoDemandaServicoService.restore(tipoDemandaServicoDTO);
					solicitacaoServicoDto.setDemanda(tipoDemandaServicoDTO.getNomeTipoDemandaServico());
				}
				solicitacaoServicoDto.setServico(this.solicitacaoServicoService.listaServico(solicitacaoServicoDto.getIdSolicitacaoServico()));

				final MensagemEmail mensagem = new MensagemEmail(Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.ID_MODELO_EMAIL_GRUPO_DESTINO,
						null)), new IDto[] { solicitacaoServicoDto, usuarioDestinoDto });
				try {
					EmpregadoDTO aux = null;
					aux = this.getEmpregadoService().restoreByIdEmpregado(usuarioDestinoDto.getIdEmpregado());
					if (aux != null && aux.getEmail() != null && !aux.getEmail().trim().equalsIgnoreCase("")) {
						mensagem.envia(aux.getEmail(), "", remetente);
					}
				} catch (final Exception e) {
					System.out.println("N�o foi poss�vel enviar o e-mail. \n" + e.getMessage());
				}
			}

		} catch (final NumberFormatException e) {
			System.out.println("N�o h� modelo de e-mail setado nos par�metros.");
		}

		final ExecucaoSolicitacaoService execucaoFluxoService = (ExecucaoSolicitacaoService) ServiceLocator.getInstance().getService(ExecucaoSolicitacaoService.class, null);
		execucaoFluxoService.delegaTarefa(usuario.getLogin(), solicitacaoServicoDto.getIdTarefa(), usuarioDestino, grupoDestino);

		document.executeScript("parent.refreshTelaGerenciamento();");
	}

	/**
	 * @param idModeloEmail
	 * @throws Exception
	 */
	public void enviaEmailGrupo(final HttpServletRequest request, final Integer idModeloEmail, final GrupoDTO grupoDestino, SolicitacaoServicoDTO solicitacaoServicoDto)
			throws Exception {
		if (grupoDestino == null) {
			return;
		}

		if (idModeloEmail == null) {
			return;
		}
		final ArrayList<EmpregadoDTO> empregados = (ArrayList<EmpregadoDTO>) this.getEmpregadoService().listEmpregadosByIdGrupo(grupoDestino.getIdGrupo());

		final String remetente = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_RemetenteNotificacoesSolicitacao, null);
		if (remetente == null) {
			throw new LogicException(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
		}

		solicitacaoServicoDto = this.getSolicitacaoServicoService(request).restoreAll(solicitacaoServicoDto.getIdSolicitacaoServico());
		final TipoDemandaServicoService tipoDemandaServicoService = (TipoDemandaServicoService) ServiceLocator.getInstance().getService(TipoDemandaServicoService.class,
				WebUtil.getUsuarioSistema(request));
		TipoDemandaServicoDTO tipoDemandaServicoDTO = new TipoDemandaServicoDTO();

		if (solicitacaoServicoDto.getIdTipoDemandaServico() != null) {
			tipoDemandaServicoDTO.setIdTipoDemandaServico(solicitacaoServicoDto.getIdTipoDemandaServico());
			tipoDemandaServicoDTO = (TipoDemandaServicoDTO) tipoDemandaServicoService.restore(tipoDemandaServicoDTO);
			solicitacaoServicoDto.setDemanda(tipoDemandaServicoDTO.getNomeTipoDemandaServico());
		}
		solicitacaoServicoDto.setServico(this.solicitacaoServicoService.listaServico(solicitacaoServicoDto.getIdSolicitacaoServico()));

		final MensagemEmail mensagem = new MensagemEmail(idModeloEmail, new IDto[] { solicitacaoServicoDto });
		try {

			EmpregadoDTO aux = null;
			for (final EmpregadoDTO e : empregados) {

				aux = (EmpregadoDTO) this.getEmpregadoService().restore(e);
				if (aux != null && aux.getEmail() != null && !aux.getEmail().trim().equalsIgnoreCase("")) {
					mensagem.envia(aux.getEmail(), "", remetente);
				}
			}
		} catch (final Exception e) {
		}
	}

	public void listarUsuariosDosGruposDoUsuarioLogado(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final UsuarioDTO usuario = WebUtil.getUsuario(request);
		final String txtFiltro = request.getParameter("txtFiltro");

		if (usuario == null && UtilStrings.isNotVazio(txtFiltro)) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		final HTMLTable tblListaUsuarios = document.getTableById("tblListaUsuarios");
		tblListaUsuarios.deleteAllRows();

		final UsuarioService usuarioService = (UsuarioService) ServiceLocator.getInstance().getService(UsuarioService.class, null);
		final Collection<UsuarioDTO> colUsuarios = usuarioService.getUsuariosDosGruposDoUsuarioLogado(usuario.getIdUsuario(), txtFiltro, 101);

		if (colUsuarios != null && !colUsuarios.isEmpty()) {
			tblListaUsuarios.addRowsByCollection(colUsuarios, new String[] { "idUsuario", "loginConcatenadoComNome" }, null, null, null, "selecionarUsuario", null);
			document.executeScript("$('#modal_infoUsuarios').modal('show')");
		}

		document.getElementById("divConsultaEstourouLimite").setVisible(colUsuarios != null && colUsuarios.size() > 100);
		document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
	}
	
	private EmpregadoService getEmpregadoService() throws ServiceException, Exception {
		if (this.empregadoService == null) {
			this.empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
		}
		return this.empregadoService;
	}

	private SolicitacaoServicoService getSolicitacaoServicoService(final HttpServletRequest request) throws ServiceException, Exception {
		if (this.solicitacaoServicoService == null) {
			this.solicitacaoServicoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class,
					WebUtil.getUsuarioSistema(request));
		}
		return this.solicitacaoServicoService;
	}
}
