/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citcorpore.bean.EmailAlteracaoSenhaDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.GrupoDTO;
import br.com.centralit.citcorpore.bean.InternacionalizarDTO;
import br.com.centralit.citcorpore.bean.LoginDTO;
import br.com.centralit.citcorpore.bean.PerfilAcessoDTO;
import br.com.centralit.citcorpore.bean.PerfilAcessoUsuarioDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.integracao.ad.LDAPUtils;
import br.com.centralit.citcorpore.mail.MensagemEmail;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.GrupoService;
import br.com.centralit.citcorpore.negocio.InstalacaoService;
import br.com.centralit.citcorpore.negocio.PerfilAcessoService;
import br.com.centralit.citcorpore.negocio.PerfilAcessoUsuarioService;
import br.com.centralit.citcorpore.negocio.UsuarioService;
import br.com.centralit.citcorpore.negocio.VersaoService;
import br.com.centralit.citcorpore.util.CitCorporeConstantes;
import br.com.centralit.citcorpore.util.CriptoUtils;
import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.dto.IDto;
import br.com.citframework.dto.Usuario;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.integracao.PersistenceEngine;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.Reflexao;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;
import br.com.citframework.util.XmlReadLookup;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class Login extends AjaxFormAction {

	@Override
	public Class getBeanClass() {
		return LoginDTO.class;
	}

	public static String recarregaPagina = "S";
	
	@Override
	public void load(final DocumentHTML document,
			final HttpServletRequest request, final HttpServletResponse response)
			throws Exception {

		final InstalacaoService instalacaoService = (InstalacaoService) ServiceLocator
				.getInstance().getService(InstalacaoService.class, null);
		if (!instalacaoService.isSucessoInstalacao()
				&& request.getSession().getAttribute("passoInstalacao") == null) {
			document.executeScript("mostraMensagemInsercao('" + "<label>"
					+ UtilI18N.internacionaliza(request,
							"instalacao.mensagemInsucesso1")
					+ "</label>"
					+ "<label>"
					+ UtilI18N.internacionaliza(request,
							"instalacao.mensagemInsucesso2")
					+ "</label>"
					+ "<label>"
					+ UtilI18N.internacionaliza(request,
							"instalacao.mensagemInsucesso3")
					+ "</label>"
					+ "<label>"
					+ UtilI18N.internacionaliza(request,
							"instalacao.mensagemInsucessoAviso1")
					+ "</label>"
					+ "<label>"
					+ UtilI18N.internacionaliza(request,
							"instalacao.mensagemInsucessoAviso2")
					+ "</label>"
					+ "<label>"
					+ UtilI18N.internacionaliza(request,
							"instalacao.mensagemInsucessoAviso3")
					+ "</label>"
					+ "<label>"
					+ UtilI18N.internacionaliza(request,
							"instalacao.mensagemInsucessoAviso4")
					+ "</label>"
					+ "<label>"
					+ UtilI18N.internacionaliza(request,
							"instalacao.mensagemInsucessoAvisoManual")
					+ "</label>" + "')");
		}

		final HTMLForm form = document.getForm("form");

		document.focusInFirstActivateField(form);
		if (request.getParameter("logout") != null
				&& "yes".equalsIgnoreCase(request.getParameter("logout"))) {
			request.getSession()
					.setAttribute(
							Constantes.getValue("USUARIO_SESSAO")
									+ "_CITCORPORE", null);
			request.getSession().setAttribute("acessosUsuario", null);
			request.getSession().setAttribute("menu", null);
			request.getSession().removeAttribute("menuPadrao");
			request.getSession().removeAttribute(Constantes.getValue("USUARIO_SESSAO") + "_PERFILACESSOMENU_CITCORPORE");
			/**
			 * Ao usu�rio realizar logout, invalida a sess�o que, consequentemente, ir� limpar todos os objetos dela
			 * 
			 * @author thyen.chang
			 * @since 05/03/2015
			 */
			request.getSession().invalidate();
			Login.recarregaPagina = "S";

			document.executeScript("window.location.href = '"
					+ CitCorporeConstantes.CAMINHO_SERVIDOR
					+ request.getContextPath() + "/pages/login/login.load'");
		}
		// limpa cache das sessoes.
		final ServletContext context = request.getSession().getServletContext();
		if (context.getAttribute("instalacao") == null
				&& request.getSession().getAttribute("passoInstalacao") == null
				&& (request.getSession().getAttribute("locale") == null || request
						.getSession().getAttribute("locale").equals(""))
				&& request.getSession().getAttribute("abrePortal") == null) {
			request.getSession().invalidate();
		}

		
		/**
		 * @gilberto.nery 
		 * 
		 * Setar o idioma padrao do sistema, caso nao exista nenhum ele captura o do browser, caso nenhum seja encontrado seta com default o portugues
		 * 
		 */
		if(request.getSession() == null || request.getSession().getAttribute("locale") == null || request.getSession().getAttribute("locale") == ""){
			
			String idiomaPadrao = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.IDIOMAPADRAO, "").toLowerCase();
			
			if((idiomaPadrao == null || idiomaPadrao.trim().equals(""))){
				
				idiomaPadrao = "pt";
				if(request.getHeader("Accept-Language") != null && !request.getHeader("Accept-Language").trim().equals("")){
					String linguagem = request.getHeader("Accept-Language");
					if(linguagem.length() > 2){
	
						linguagem = linguagem.substring(0, 2);
						
						if(linguagem.contains("en")){
							idiomaPadrao = "en";	
						} else if(linguagem.contains("es")){
							idiomaPadrao = "es";
						}					
					}
				}
			}
			
			WebUtil webUtil = new WebUtil(); 
			webUtil.setLocale(idiomaPadrao, request);
			XmlReadLookup.getInstance(new Locale(idiomaPadrao));
			recarregaPagina = "N";
		}
		
	}

	public void Login_onsave(final DocumentHTML document,
			final HttpServletRequest request, final HttpServletResponse response)
			throws Exception {

		final InstalacaoService instalacaoService = (InstalacaoService) ServiceLocator
				.getInstance().getService(InstalacaoService.class, null);
		if (!instalacaoService.isSucessoInstalacao()
				&& request.getSession().getAttribute("passoInstalacao") == null) {
			this.load(document, request, response);
			document.executeScript("fechar_aguarde();");
			return;
		}

		final LoginDTO login = (LoginDTO) document.getBean();
		document.getForm("form");
		boolean isAdmin = false;
		if (login != null) {
			/**
			 * Motivo: Iners�o da valida��o de login Autor: Fl�vio.santana Data/Hora: 05/11/2013 17:30
			 */
			if (login.getUser() == null
					|| login.getUser().trim().equalsIgnoreCase("")) {
				document.executeScript("fechar_aguarde();");
				document.alert(UtilI18N.internacionaliza(request,
						"login.digite_login"));
				return;
			}
			if (login.getSenha() == null
					|| login.getSenha().trim().equalsIgnoreCase("")) {
				document.alert(UtilI18N.internacionaliza(request,
						"login.digite_senha"));
				document.executeScript("fechar_aguarde();");
				return;
			}
		} else {
			document.alert(UtilI18N.internacionaliza(request,
					"login.nao_confere"));
			document.executeScript("fechar_aguarde();");
			return;
		}

		final UsuarioDTO usrDto = new UsuarioDTO();
		final UsuarioService usuarioService = (UsuarioService) ServiceLocator
				.getInstance().getService(UsuarioService.class, null);

		final String metodoAutenticacao = ParametroUtil
				.getValorParametroCitSmartHashMap(
						ParametroSistema.METODO_AUTENTICACAO_Pasta, "2");

		isAdmin = "admin".equalsIgnoreCase(login.getUser())
				|| "consultor".equalsIgnoreCase(login.getUser());

		if (metodoAutenticacao != null
				&& metodoAutenticacao.trim().equalsIgnoreCase("2")) {
			if (!isAdmin) {
				if (request.getSession().getAttribute("passoInstalacao") != null) {
					document.executeScript("fechar_aguarde();");
					document.alert(UtilI18N.internacionaliza(request,
							"usuario.permissaoInstalacao"));
					return;
				} else {
					usuarioService.sincronizaUsuarioAD(
							LDAPUtils.autenticacaoAD(login.getUser(),
									login.getSenha()), login, false);
				}
			}
		} else {
			System.out.println(UtilI18N.internacionaliza(request,
					"login.configNaoSincronizarComAD"));
		}

		final boolean veririficaVazio = usuarioService.listSeVazio();

		String algoritmo = br.com.citframework.util.Constantes
				.getValue("ALGORITMO_CRIPTOGRAFIA_SENHA");
		if (algoritmo == null || !algoritmo.trim().equalsIgnoreCase("")) {

			algoritmo = "SHA-1";
		}

		login.setSenha(CriptoUtils.generateHash(login.getSenha(), algoritmo));

		if (!veririficaVazio && "admin".equalsIgnoreCase(login.getUser())) {

			usrDto.setDataInicio(UtilDatas.getDataAtual());
			usrDto.setLogin(login.getUser());
			usrDto.setSenha(login.getSenha());
			usrDto.setNomeUsuario("Administrador");
			usrDto.setStatus("A");

			usuarioService.createFirs(usrDto);
		}
		final UsuarioDTO usuarioBean = usuarioService.restoreByLogin(
				login.getUser(), login.getSenha());
		if (usuarioBean == null) {
			document.executeScript("fechar_aguarde();");
			document.alert(UtilI18N.internacionaliza(request,
					"login.nao_confere"));
			return;
		}

		if (metodoAutenticacao == null
				|| metodoAutenticacao.trim().equalsIgnoreCase("")) {
			document.executeScript("fechar_aguarde();");
			document.alert(UtilI18N.internacionaliza(request,
					"login.metodoAutenticaoNaoConfigurado"));
			return;
		}

		if (metodoAutenticacao != null) {
			if (usuarioBean.getStatus().equalsIgnoreCase("A")
					&& login.getSenha().equals(usuarioBean.getSenha())) {
				if (usuarioBean.getIdEmpresa() == null) {
					usuarioBean.setIdEmpresa(1);
				}
				final Usuario usuarioFramework = new Usuario();
				final UsuarioDTO usr = new UsuarioDTO();
				usr.setIdUsuario(usuarioBean.getIdUsuario());
				usr.setNomeUsuario(usuarioBean.getNomeUsuario());
				usr.setIdGrupo(usuarioBean.getIdGrupo());
				usr.setIdEmpresa(usuarioBean.getIdEmpresa());
				usr.setIdEmpregado(usuarioBean.getIdEmpregado());
				usr.setLogin(usuarioBean.getLogin());
				usr.setStatus(usuarioBean.getStatus());
				usr.setIdPerfilAcessoUsuario(this
						.getIdPerfilAcessoUsuario(usuarioBean.getIdUsuario()));
				/**
				 * Se � 'admin' o acesso � irrestrito
				 */
				if (isAdmin) {
					usr.setAcessoCitsmart("S");
				} else {
					usr.setAcessoCitsmart(this.getAcessoCitsmart(
							usr.getIdPerfilAcessoUsuario(), usr.getIdUsuario()));
				}
				usr.setEmail(usuarioBean.getEmail());
				// utilizado para log
				PersistenceEngine.setUsuarioSessao(usuarioBean);
				Reflexao.copyPropertyValues(usr, usuarioFramework);
				br.com.citframework.util.WebUtil.setUsuario(usuarioFramework,
						request);
				final GrupoService grupoSegService = (GrupoService) ServiceLocator
						.getInstance().getService(GrupoService.class, null);
				final Collection<GrupoDTO> colGrupos = grupoSegService
						.getGruposByPessoa(usuarioBean.getIdEmpregado());
				GrupoDTO grpSeg;

				String[] grupos = null;
				if (colGrupos != null) {
					grupos = new String[colGrupos.size()];
					for (int i = 0; i < colGrupos.size(); i++) {
						grpSeg = (GrupoDTO) ((List) colGrupos).get(i);
						grupos[i] = grpSeg.getSigla();
					}
				} else {
					grupos = new String[1];
					grupos[0] = "";
				}

				usr.setGrupos(grupos);
				usr.setColGrupos(colGrupos);

				WebUtil.setUsuario(usr, request);

				if (usr != null) {

					final Problema problema = new Problema();

					problema.notificarPrazoSolucionarProblemaExpirou(document,
							request, response, usr);

				}

				// Verifica��o de instala��o
				final String sessao = (String) request.getSession()
						.getAttribute("passoInstalacao");

				if (sessao != null) {
					document.executeScript("window.location = '"
							+ CitCorporeConstantes.CAMINHO_SERVIDOR
							+ request.getContextPath()
							+ "/pages/start/start.load';");
				} else {

					final VersaoService versaoService = (VersaoService) ServiceLocator
							.getInstance()
							.getService(VersaoService.class, null);
					final String idPerfilAcessoAdministrador = ParametroUtil
							.getValorParametroCitSmartHashMap(
									ParametroSistema.ID_PERFIL_ACESSO_ADMINISTRADOR,
									"1");
					final boolean usuarioTemPerfilDeAdministrador = usr
							.getIdPerfilAcessoUsuario() != null
							&& usr.getIdPerfilAcessoUsuario().toString().trim()
									.equals(idPerfilAcessoAdministrador.trim());
					if (versaoService.haVersoesSemValidacao()
							&& !usuarioTemPerfilDeAdministrador) {
						document.executeScript("fechar_aguarde();");
						document.alert(UtilI18N.internacionaliza(request,
								"citcorpore.comum.citsmartAtualizado"));
						return;
					}

					document.executeScript("logar()");
				}

				/**
				 * Caso tenha ficado alguma carga de menu no session, remove a refer�ncia para peg�-la novamente ao carregar o menu
				 * 
				 * @author thyen.chang
				 * @since 12/02/2015
				 */
				if (request.getSession(true).getAttribute("menuPadrao") != null) {
					request.getSession(true).removeAttribute("menuPadrao");
				}

				/*
				 * Desenvolvedor: Thiago Matias - Data: 05/11/2013 - Hor�rio: 16:00 - ID Citsmart: 123357 - Motivo/Coment�rio: redirecionando para o portal quando o parametro do mesmo estiver
				 * habilitado.
				 */
				final String abrePortal = (String) request.getSession()
						.getAttribute("abrePortal");
				final String parametroPortal = ParametroUtil
						.getValorParametroCitSmartHashMap(
								ParametroSistema.LOGIN_PORTAL, "N");
				if (parametroPortal.equalsIgnoreCase("S")) {
					document.executeScript("window.location = '"
							+ CitCorporeConstantes.CAMINHO_SERVIDOR
							+ request.getContextPath()
							+ "/pages/portal/portal.load';");
				} else if (abrePortal != null
						&& abrePortal.equalsIgnoreCase("S")) {
					document.executeScript("window.location = '"
							+ CitCorporeConstantes.CAMINHO_SERVIDOR
							+ request.getContextPath()
							+ "/pages/portal/portal.load';");
					request.getSession().setAttribute("abrePortal", null);
				}

			} else {
				document.executeScript("fechar_aguarde();");
				document.alert(UtilI18N.internacionaliza(request,
						"login.nao_confere"));
				request.getSession().invalidate();
				return;
			}
		}
	}

	/**
	 * Adicionado tratamento para quando usu�rio n�o possui perfil de acesso 30/12/2014 - 10:18
	 * 
	 *
	 * @author thyen.chang
	 * @param idPerfilAcessoUsuario
	 * @param idUsuario
	 * @return
	 * @throws ServiceException
	 * @throws Exception
	 */
	private String getAcessoCitsmart(final Integer idPerfilAcessoUsuario,
			final Integer idUsuario) throws ServiceException, Exception {

		final PerfilAcessoService perfilAcessoService = (PerfilAcessoService) ServiceLocator
				.getInstance().getService(PerfilAcessoService.class, null);

		PerfilAcessoDTO perfilAcessoDTO = new PerfilAcessoDTO();

		perfilAcessoDTO.setIdPerfilAcesso(idPerfilAcessoUsuario);

		perfilAcessoDTO = perfilAcessoService
				.findByIdPerfilAcesso(perfilAcessoDTO);

		if (perfilAcessoDTO != null
				&& perfilAcessoDTO.getAcessoSistemaCitsmart() != null
				&& !perfilAcessoDTO.getAcessoSistemaCitsmart().isEmpty()
				&& perfilAcessoDTO.getAcessoSistemaCitsmart().equals("S")) {
			return perfilAcessoDTO.getAcessoSistemaCitsmart();
		} else {
			return perfilAcessoService.getAcessoCitsmartByUsuario(idUsuario);
	}
	}

	private Integer getIdPerfilAcessoUsuario(final Integer idUsuario)
			throws ServiceException, Exception {
		final PerfilAcessoUsuarioService perfilAcessoService = (PerfilAcessoUsuarioService) ServiceLocator
				.getInstance().getService(PerfilAcessoUsuarioService.class,
						null);
		PerfilAcessoUsuarioDTO perfilAcessoDTO = new PerfilAcessoUsuarioDTO();
		perfilAcessoDTO.setIdUsuario(idUsuario);
		perfilAcessoDTO = perfilAcessoService.listByIdUsuario(perfilAcessoDTO);
		if (perfilAcessoDTO == null) {
			return null;
		} else {
			return perfilAcessoDTO.getIdPerfilAcesso();
		}
	}

	public void redefinirSenha(final DocumentHTML document,
			final HttpServletRequest request, final HttpServletResponse response)
			throws Exception {

		// Obtendo o login ou e-mail informado na popup de recupera��o de senha.
		final LoginDTO loginDTO = (LoginDTO) document.getBean();

		if (loginDTO != null) {

			// Obtendo servi�os para consultar informa��es de usu�rio e
			// empregado.
			final UsuarioService usuarioService = (UsuarioService) ServiceLocator
					.getInstance().getService(UsuarioService.class, null);

			final EmpregadoService empregadoService = (EmpregadoService) ServiceLocator
					.getInstance().getService(EmpregadoService.class, null);

			try {

				if (usuarioService != null) {

					UsuarioDTO usuarioDTO = null;

					EmpregadoDTO empregadoDTO = null;

					// Verificando se foi informado um valor.
					if (loginDTO.getLogin() != null) {

						if (loginDTO.getLogin().trim().equals("")) {

							document.alert(UtilI18N.internacionaliza(request,
									"login.necessarioInformarEmailOuLogin"));

							document.executeScript("$('#login').focus()");

							return;
						}

						// Verificando se o valor informado � um e-mail.
						if (loginDTO
								.getLogin()
								.matches(
										"\\b[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}\\b")) {

							// Obtendo o empregado a partir o e-mail.
							empregadoDTO = empregadoService
									.restoreByEmail(loginDTO.getLogin());

							usuarioDTO = usuarioService
									.restoreByIdEmpregado(empregadoDTO
											.getIdEmpregado());

						} else { // caso n�o seja, assume que � login.

							// Restaurando o usu�rio atrav�s do login.
							usuarioDTO = usuarioService.restoreByLogin(loginDTO
									.getLogin());

							empregadoDTO = empregadoService
									.restoreByIdUsuario(usuarioDTO
											.getIdUsuario());

							// Obtendo o empregado.
							if (empregadoDTO != null) {
								empregadoDTO = empregadoService
										.restoreEmpregadosAtivosById(empregadoDTO
												.getIdEmpregado());
						}
						}

						if (empregadoDTO.getIdEmpregado() != null
								&& empregadoDTO.getIdEmpregado() > 0) {

							String algoritmo = br.com.citframework.util.Constantes
									.getValue("ALGORITMO_CRIPTOGRAFIA_SENHA");

							if (algoritmo == null
									|| !algoritmo.trim().equalsIgnoreCase("")) {

								algoritmo = "SHA-1";
							}

							final EmailAlteracaoSenhaDTO emailAlteracaoSenhaDTO = new EmailAlteracaoSenhaDTO();

							emailAlteracaoSenhaDTO.setLogin(usuarioDTO
									.getLogin());

							emailAlteracaoSenhaDTO
									.setNomeEmpregado(empregadoDTO.getNome());

							final UUID uuid = UUID.randomUUID();

							String novaSenha = uuid.toString();

							novaSenha = novaSenha.replaceAll("-", "");

							novaSenha = novaSenha.substring(0, 7);

							emailAlteracaoSenhaDTO.setNovaSenha(novaSenha);

							usuarioDTO.setSenha(CriptoUtils.generateHash(
									novaSenha, algoritmo));

							usuarioService.updateNotNull(usuarioDTO);

							emailAlteracaoSenhaDTO
									.setLink(ParametroUtil
											.getValorParametroCitSmartHashMap(
													ParametroSistema.URL_Sistema,
													"33"));

							final String ID_MODELO_EMAIL_ALTERACAO_SENHA = ParametroUtil
									.getValorParametroCitSmartHashMap(
											ParametroSistema.ID_MODELO_EMAIL_ALTERACAO_SENHA,
											"18");

							final MensagemEmail mensagem = new MensagemEmail(
									Integer.parseInt(ID_MODELO_EMAIL_ALTERACAO_SENHA
											.trim()),
									new IDto[] { emailAlteracaoSenhaDTO });

							mensagem.envia(
									empregadoDTO.getEmail(),
									"",
									ParametroUtil
											.getValorParametroCitSmartHashMap(
ParametroSistema.SMTP_ENVIO_RemetenteNotificacoesSolicitacao,
													"10"));

							document.alert(String.format(
									UtilI18N.internacionaliza(request,
											"login.alteracaoSenha.notificacaoEnvioEmail"),
									empregadoDTO.getNome(), empregadoDTO
											.getEmail()));
						}
					}
				}
			} catch (final Exception e) {

				document.alert(UtilI18N.internacionaliza(request,
						"login.alteracaoSenha.cancelamento"));

				e.printStackTrace();
			}
		}
	}

	public void internacionaliza(final DocumentHTML document,
			final HttpServletRequest request, final HttpServletResponse response)
			throws Exception {
		final InternacionalizarDTO bean = (InternacionalizarDTO) document
				.getBean();

		String IDIOMAPADRAO = ParametroUtil.getValorParametroCitSmartHashMap(
				Enumerados.ParametroSistema.IDIOMAPADRAO, " ").toLowerCase();

		if (IDIOMAPADRAO == null) {
			IDIOMAPADRAO = "";
		}

		request.getSession(true).setAttribute("menu", null);
		request.getSession(true).setAttribute("menuPadrao", null);

		WebUtil webUtil = new WebUtil();
		
		if (bean != null) {

			if (bean.getLocale() != null) {
				webUtil.setLocale(bean.getLocale().trim(), request);
				XmlReadLookup.getInstance(new Locale(bean.getLocale().trim()));
			} else {
				webUtil.setLocale(IDIOMAPADRAO, request);
				XmlReadLookup.getInstance(new Locale(IDIOMAPADRAO));
			}
		} else {
			webUtil.setLocale(IDIOMAPADRAO, request);
			XmlReadLookup.getInstance(new Locale(IDIOMAPADRAO));
		}
		document.executeScript("window.location.reload()");
	}

}
