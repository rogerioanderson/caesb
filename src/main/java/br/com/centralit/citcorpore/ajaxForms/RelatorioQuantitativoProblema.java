/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.RelatorioQuantitativoProblemaDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.ProblemaService;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.LogoRel;
import br.com.centralit.citcorpore.util.UtilRelatorio;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import net.sf.jasperreports.export.XlsxReportConfiguration;
@SuppressWarnings("rawtypes")
public class RelatorioQuantitativoProblema extends AjaxFormAction {

	private UsuarioDTO usuario;
	private String localeSession = null;
	
	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		usuario = WebUtil.getUsuario(request);
		
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}
		
		this.preencherComboContrato(document, request, response);
	}

	
	@Override
	public Class getBeanClass() {
		// TODO Auto-generated method stub
		return RelatorioQuantitativoProblemaDTO.class;
	}
	
	public void imprimirRelatorio(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception{
		
		HttpSession session = ((HttpServletRequest) request).getSession();
		
		RelatorioQuantitativoProblemaDTO relatorioQuantitativoProblemaDto = (RelatorioQuantitativoProblemaDTO) document.getBean();
		
		RelatorioQuantitativoProblemaDTO relatorioQuantitativoProblemaDtoAux =  new RelatorioQuantitativoProblemaDTO();
		
		usuario = WebUtil.getUsuario(request);
		
		ProblemaService problemaService = (ProblemaService) ServiceLocator.getInstance().getService(ProblemaService.class, null);
		
		 // Desenvolvedor: ibimon.morais Data: 13/08/2015 Hor�rio: 14h30min Solicita��o: 176362 Motivo/Coment�rio: Altera��o feita para melhorar
		 // a consulta de dados para relatorio passando a consultar em um novo banco de dados espelhado, de acordo com citsmart.cfg
		relatorioQuantitativoProblemaDto.setBaseConsulta(Boolean.TRUE);
		
		Collection<RelatorioQuantitativoProblemaDTO> listaDadosGeral = new ArrayList<RelatorioQuantitativoProblemaDTO>();
		Collection<RelatorioQuantitativoProblemaDTO> listaQuantitativoProblemaPorSituacao = problemaService.listaQuantidadeProblemaPorSituacao(relatorioQuantitativoProblemaDto);
		Collection<RelatorioQuantitativoProblemaDTO> listaQuantitativoProblemaPorGrupo = problemaService.listaQuantidadeProblemaPorGrupo(relatorioQuantitativoProblemaDto);
		Collection<RelatorioQuantitativoProblemaDTO> listaQuantitativoProblemaPorOrigem = problemaService.listaQuantidadeProblemaPorOrigem(relatorioQuantitativoProblemaDto);
		Collection<RelatorioQuantitativoProblemaDTO> listaQuantitativoProblemaPorSolicitante = problemaService.listaQuantidadeProblemaPorSolicitante(relatorioQuantitativoProblemaDto);
		Collection<RelatorioQuantitativoProblemaDTO> listaQuantitativoProblemaPorPrioridade = problemaService.listaQuantidadeProblemaPorPrioridade(relatorioQuantitativoProblemaDto);
		Collection<RelatorioQuantitativoProblemaDTO> listaQuantitativoProblemaPorCategoria = problemaService.listaQuantidadeProblemaPorCategoria(relatorioQuantitativoProblemaDto);
		Collection<RelatorioQuantitativoProblemaDTO> listaQuantitativoProblemaPorProprietario = problemaService.listaQuantidadeProblemaPorProprietario(relatorioQuantitativoProblemaDto);
		Collection<RelatorioQuantitativoProblemaDTO> listaQuantitativoProblemaPorImpacto = problemaService.listaQuantidadeProblemaPorImpacto(relatorioQuantitativoProblemaDto);
		Collection<RelatorioQuantitativoProblemaDTO> listaQuantitativoProblemaPorUrgencia = problemaService.listaQuantidadeProblemaPorUrgencia(relatorioQuantitativoProblemaDto);
		
		
		if(listaQuantitativoProblemaPorSituacao!=null){
			relatorioQuantitativoProblemaDtoAux.setListaQuantidadeProblemaPorSituacao(listaQuantitativoProblemaPorSituacao);
		}
		
		if(listaQuantitativoProblemaPorGrupo!=null){
			relatorioQuantitativoProblemaDtoAux.setListaQuantidadeProblemaPorGrupo(listaQuantitativoProblemaPorGrupo);
		}
		
		if(listaQuantitativoProblemaPorOrigem!=null){
			relatorioQuantitativoProblemaDtoAux.setListaQuantidadeProblemaPorOrigem(listaQuantitativoProblemaPorOrigem);
		}
		
		if(listaQuantitativoProblemaPorSolicitante!=null){
			relatorioQuantitativoProblemaDtoAux.setListaQuantidadeProblemaPorSolicitante(listaQuantitativoProblemaPorSolicitante);
		}
		
		if(listaQuantitativoProblemaPorPrioridade!=null){
			relatorioQuantitativoProblemaDtoAux.setListaQuantidadeProblemaPorPrioridade(listaQuantitativoProblemaPorPrioridade);
		}
		if(listaQuantitativoProblemaPorCategoria!=null){
			relatorioQuantitativoProblemaDtoAux.setListaQuantidadeProblemaPorCategoriaProblema(listaQuantitativoProblemaPorCategoria);
		}
		
		if(listaQuantitativoProblemaPorProprietario!=null){
			relatorioQuantitativoProblemaDtoAux.setListaQuantidadeProblemaPorProprietario(listaQuantitativoProblemaPorProprietario);
		}
		if(listaQuantitativoProblemaPorImpacto!=null){
			relatorioQuantitativoProblemaDtoAux.setListaQuantidadeProblemaPorImpacto(listaQuantitativoProblemaPorImpacto);
			
		}
		if(listaQuantitativoProblemaPorUrgencia!=null){
			relatorioQuantitativoProblemaDtoAux.setListaQuantidadeProblemaPorUrgencia(listaQuantitativoProblemaPorUrgencia);
		}
		
		if (relatorioQuantitativoProblemaDtoAux.getListaQuantidadeProblemaPorSituacao() != null
				|| relatorioQuantitativoProblemaDtoAux.getListaQuantidadeProblemaPorGrupo() != null
				|| relatorioQuantitativoProblemaDtoAux.getListaQuantidadeProblemaPorOrigem() != null
				|| relatorioQuantitativoProblemaDtoAux.getListaQuantidadeProblemaPorSolicitante() != null
				|| relatorioQuantitativoProblemaDtoAux.getListaQuantidadeProblemaPorPrioridade() != null
				|| relatorioQuantitativoProblemaDtoAux.getListaQuantidadeProblemaPorCategoriaProblema() != null
				|| relatorioQuantitativoProblemaDtoAux.getListaQuantidadeProblemaPorProprietario() != null
				|| relatorioQuantitativoProblemaDtoAux.getListaQuantidadeProblemaPorImpacto() != null
				|| relatorioQuantitativoProblemaDtoAux.getListaQuantidadeProblemaPorUrgencia() != null) {
			listaDadosGeral.add(relatorioQuantitativoProblemaDtoAux);
		} else {
			listaDadosGeral = null;
		}
		
		if(listaDadosGeral != null){
			Date dt = new Date();
			
			String strCompl = "" + dt.getTime();
			String caminhoJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioQuantitativoProblema.jasper";
			String diretorioReceita = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
			String diretorioRelativoOS = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";

			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros = UtilRelatorio.trataInternacionalizacaoLocale(session, parametros);

			parametros.put("TITULO_RELATORIO", UtilI18N.internacionaliza(request, "relatorioQuantitativoProblema.relatorioQuantitativoProblema"));
			parametros.put("CIDADE", getCidadeParametrizada(request));
			parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
			parametros.put("NOME_USUARIO", usuario.getNomeUsuario());
			parametros.put("dataInicio", relatorioQuantitativoProblemaDto.getDataInicio());
			parametros.put("dataFim", relatorioQuantitativoProblemaDto.getDataFim());
			parametros.put("totalProblema", listaDadosGeral.size());
			parametros.put("Logo", LogoRel.getFile());
			
			if(relatorioQuantitativoProblemaDto.getIdContrato() != null && relatorioQuantitativoProblemaDto.getIdContrato().intValue() > 0){
				parametros.put("contrato", this.getContrato(relatorioQuantitativoProblemaDto.getIdContrato()));
			}else{
				parametros.put("contrato", UtilI18N.internacionaliza(request, "citcorpore.comum.todos"));
			}
			
			if (relatorioQuantitativoProblemaDto.getFormatoArquivoRelatorio().equalsIgnoreCase("pdf")) {

				this.gerarRelatorioFormatoPdf(listaDadosGeral, caminhoJasper, parametros, diretorioReceita, strCompl, document, diretorioRelativoOS, usuario);

			} else {

				this.gerarRelatorioFormatoXls(listaDadosGeral, parametros, diretorioReceita, strCompl, document, diretorioRelativoOS, usuario);

			}
		}else{
			document.executeScript("reportEmpty();");
		}
		
		document.executeScript("JANELA_AGUARDE_MENU.hide();");
	}
	
	public void gerarRelatorioFormatoPdf(Collection<RelatorioQuantitativoProblemaDTO> listaDadosGeral, String caminhoJasper, Map<String, Object> parametros, String diretorioReceita, String strCompl,
			DocumentHTML document, String diretorioRelativoOS, UsuarioDTO usuario) throws Exception {

		JRDataSource dataSource = new JRBeanCollectionDataSource(listaDadosGeral);

		JasperPrint print = JasperFillManager.fillReport(caminhoJasper, parametros, dataSource);

		JasperExportManager.exportReportToPdfFile(print, diretorioReceita + "/RelatorioQuantitativoProblema" + strCompl + "_" + usuario.getIdUsuario() + ".pdf");

		document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + diretorioRelativoOS
				+ "/RelatorioQuantitativoProblema" + strCompl + "_" + usuario.getIdUsuario() + ".pdf')");

	}

	
	public void gerarRelatorioFormatoXls(Collection<RelatorioQuantitativoProblemaDTO> listaDadosGeral, Map<String, Object> parametros, String diretorioReceita, String strCompl, DocumentHTML document,
			String diretorioRelativoOS, UsuarioDTO usuario) throws Exception {

		final JRDataSource dataSource = new JRBeanCollectionDataSource(listaDadosGeral);
		final JasperDesign desenho = JRXmlLoader.load(CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS") + "RelatorioQuantitativoProblemaXls.jrxml");
		desenho.setLanguage("java");
		final JasperReport relatorio = JasperCompileManager.compileReport(desenho);
		final JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, dataSource);
		final JRXlsxExporter exporter = new JRXlsxExporter();
		exporter.setExporterInput(new SimpleExporterInput(impressao));
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(diretorioReceita + "/RelatorioQuantitativoProblemaXls" + strCompl + "_" + usuario.getIdUsuario() + ".xlsx"));
        XlsxReportConfiguration xlsxReportConfiguration = new SimpleXlsxReportConfiguration();
        exporter.setConfiguration(xlsxReportConfiguration);
		exporter.exportReport();
		document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + diretorioRelativoOS
				+ "/RelatorioQuantitativoProblemaXls" + strCompl + "_" + usuario.getIdUsuario() + ".xlsx')");

	}
	
	public void preencherComboContrato(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		HTMLSelect comboContrato = document.getSelectById("idContrato");
		comboContrato.removeAllOptions();
		ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
		Collection colContrato = contratoService.listAtivos();
		comboContrato.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.todos"));
		comboContrato.addOptions(colContrato, "idContrato", "numero", null);
	}
	
	private String getContrato(Integer id) throws ServiceException, Exception{
		ContratoDTO contrato = new ContratoDTO();
		contrato.setIdContrato(id);
		ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
		contrato = (ContratoDTO) contratoService.restore(contrato);
		if(contrato != null){
			return contrato.getNumero();
		}
		return null;
	}

}
