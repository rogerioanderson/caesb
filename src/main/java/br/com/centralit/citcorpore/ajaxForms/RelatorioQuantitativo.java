/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.RelatorioQuantitativoSubRelatorioDTO;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.RelatorioQuantitativoSolicitacaoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.SolicitacaoServicoService;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.Enumerados.SituacaoSolicitacaoServico;
import br.com.centralit.citcorpore.util.LogoRel;
import br.com.centralit.citcorpore.util.UtilRelatorio;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.fill.JRAbstractLRUVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import net.sf.jasperreports.export.XlsxReportConfiguration;

@SuppressWarnings({ "rawtypes", "unused" })
public class RelatorioQuantitativo extends AjaxFormAction {
	UsuarioDTO usuario;
	private String localeSession = null;

	@Override
	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		usuario = WebUtil.getUsuario(request);

		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			return;
		}

		// Preenchendo a combobox de contratos.
		HTMLSelect comboContrato = document.getSelectById("idContrato");
		comboContrato.removeAllOptions();
		ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
		Collection colContrato = contratoService.list();
		comboContrato.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.todos"));
		comboContrato.addOptions(colContrato, "idContrato", "numero", null);

		// Preenchendo a combobox de situacoes.
		HTMLSelect comboSituacao = document.getSelectById("situacao");
		comboSituacao.removeAllOptions();
		comboSituacao.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.todas"));
		for (SituacaoSolicitacaoServico situacao : SituacaoSolicitacaoServico.values()) {
			comboSituacao.addOption(situacao.name(), UtilI18N.internacionaliza(request, "solicitacaoServico.situacao." + situacao.name()));
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public Class getBeanClass() {
		return SolicitacaoServicoDTO.class;
	}

	public Collection<RelatorioQuantitativoSolicitacaoDTO> makeDataSource(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		HttpSession session = ((HttpServletRequest) request).getSession();
		SolicitacaoServicoDTO solicitacaoServicoDto = (SolicitacaoServicoDTO) document.getBean();
		SolicitacaoServicoService solicitacaoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);
		ContratoDTO contratoDto = new ContratoDTO();
		ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
		usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return null;
		}

		if (solicitacaoServicoDto.getIdSolicitacaoServicoPesquisa() == null) {
			if (solicitacaoServicoDto.getDataInicio() == null) {
				document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.validacao.datainicio"));
				document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
				return null;
			}
			if (solicitacaoServicoDto.getDataFim() == null) {
				document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.validacao.datafim"));
				document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
				return null;
			}

		}

		JRDataSource listaPorGrupoDataSource = null;
		JRDataSource listaPorItemConfiguracaoDataSource = null;
		JRDataSource listaPorSituacaoDataSource = null;
		JRDataSource listaPorTipoDataSource = null;
		JRDataSource listaPorOrigemDataSource = null;
		JRDataSource listaPorPrioridadeDataSource = null;
		JRDataSource listaPorFaseDataSource = null;
		JRDataSource listaPorTipoServicoDataSource = null;
		JRDataSource listaPorResponsavelDataSource = null;
		JRDataSource listaPorHoraAberturaDataSource = null;
		JRDataSource listaPorSituacaoSLADataSource = null;
		JRDataSource listaPorPesquisaSatisfacaoDataSource = null;
		JRDataSource listaPorSolicitanteDataSource = null;

		Collection<RelatorioQuantitativoSolicitacaoDTO> listDadosRelatorio = new ArrayList<RelatorioQuantitativoSolicitacaoDTO>();
		RelatorioQuantitativoSolicitacaoDTO dadosRelatorio = new RelatorioQuantitativoSolicitacaoDTO();

		Collection<RelatorioQuantitativoSubRelatorioDTO> listPorGrupo = solicitacaoService.listaQuantidadeSolicitacaoPorGrupo(request, solicitacaoServicoDto);
		Collection<RelatorioQuantitativoSubRelatorioDTO> listPorItemConfiguracao = solicitacaoService.listaQuantidadeSolicitacaoPorItemConfiguracao(solicitacaoServicoDto);
		Collection<RelatorioQuantitativoSubRelatorioDTO> listPorSituacao = solicitacaoService.listaQuantidadeSolicitacaoPorSituacao(solicitacaoServicoDto);
		Collection<RelatorioQuantitativoSubRelatorioDTO> listPorTipo = solicitacaoService.listaQuantidadeSolicitacaoPorTipo(solicitacaoServicoDto);
		Collection<RelatorioQuantitativoSubRelatorioDTO> listPorOrigem = solicitacaoService.listaQuantidadeSolicitacaoPorOrigem(solicitacaoServicoDto);
		Collection<RelatorioQuantitativoSubRelatorioDTO> listPorPrioridade = solicitacaoService.listaQuantidadeSolicitacaoPorPrioridade(solicitacaoServicoDto);
		Collection<RelatorioQuantitativoSubRelatorioDTO> listPorFase = solicitacaoService.listaQuantidadeSolicitacaoPorFase(solicitacaoServicoDto);
		Collection<RelatorioQuantitativoSubRelatorioDTO> listPorTipoServico = solicitacaoService.listaQuantidadeSolicitacaoPorTipoServico(solicitacaoServicoDto);
		Collection<RelatorioQuantitativoSubRelatorioDTO> listPorResponsavel = solicitacaoService.listaQuantidadeSolicitacaoPorResponsavel(solicitacaoServicoDto);
		Collection<RelatorioQuantitativoSubRelatorioDTO> listPorHoraAbertura = solicitacaoService.listaQuantidadeSolicitacaoPorHoraAbertura(solicitacaoServicoDto);
		Collection<SolicitacaoServicoDTO> listPorSituacaoSLA = solicitacaoService.relatorioControleSla(solicitacaoServicoDto);
		Collection<RelatorioQuantitativoSubRelatorioDTO> listPorPesquisaSatisfacao = solicitacaoService.listaQuantidadeSolicitacaoPorPesquisaSatisfacao(request, solicitacaoServicoDto);
		Collection<RelatorioQuantitativoSubRelatorioDTO> listPorSolicitante = solicitacaoService.listaQuantidadeSolicitacaoPorSolicitante(solicitacaoServicoDto);

		if (listPorGrupo != null) {
			listaPorGrupoDataSource = new JRBeanCollectionDataSource(listPorGrupo);
		}
		if (listPorItemConfiguracao != null) {
			listaPorItemConfiguracaoDataSource = new JRBeanCollectionDataSource(listPorItemConfiguracao);
		}
		if (listPorSituacao != null) {
			listaPorSituacaoDataSource = new JRBeanCollectionDataSource(listPorSituacao);
		}
		if (listPorTipo != null) {
			listaPorTipoDataSource = new JRBeanCollectionDataSource(listPorTipo);
		}
		if (listPorOrigem != null) {
			listaPorOrigemDataSource = new JRBeanCollectionDataSource(listPorOrigem);
		}
		if (listPorPrioridade != null) {
			listaPorPrioridadeDataSource = new JRBeanCollectionDataSource(listPorPrioridade);
		}
		if (listPorFase != null) {
			listaPorFaseDataSource = new JRBeanCollectionDataSource(listPorFase);
		}
		if (listPorTipoServico != null) {
			listaPorTipoServicoDataSource = new JRBeanCollectionDataSource(listPorTipoServico);
		}
		if (listPorResponsavel != null) {
			listaPorResponsavelDataSource = new JRBeanCollectionDataSource(listPorResponsavel);
		}
		if (listPorHoraAbertura != null) {
			listaPorHoraAberturaDataSource = new JRBeanCollectionDataSource(listPorHoraAbertura);
		}
		if (listPorSituacaoSLA != null) {
			int qtdePrazo = 0;
			int qtdeForaPrazo = 0;
			if (listPorSituacaoSLA != null) {
				List<RelatorioQuantitativoSubRelatorioDTO> listSituacao = new ArrayList<RelatorioQuantitativoSubRelatorioDTO>();
				for (SolicitacaoServicoDTO relatorioQuantitativoSolicitacaoDTO2 : listPorSituacaoSLA) {
					if (relatorioQuantitativoSolicitacaoDTO2.getAtrasoSLAStr().equalsIgnoreCase("S")) {
						qtdePrazo++;
					} else {
						qtdeForaPrazo++;
					}
				}
				RelatorioQuantitativoSubRelatorioDTO relatorioComAtraso = new RelatorioQuantitativoSubRelatorioDTO();
				relatorioComAtraso.setItem(UtilI18N.internacionaliza(request, "citcorpore.comum.comAtraso"));
				relatorioComAtraso.setQuantidade(qtdePrazo);
				listSituacao.add(relatorioComAtraso);
				RelatorioQuantitativoSubRelatorioDTO relatorioSemAtraso = new RelatorioQuantitativoSubRelatorioDTO();
				relatorioSemAtraso.setItem(UtilI18N.internacionaliza(request, "citcorpore.comum.semAtraso"));
				relatorioSemAtraso.setQuantidade(qtdeForaPrazo);
				listSituacao.add(relatorioSemAtraso);
				listaPorSituacaoSLADataSource = new JRBeanCollectionDataSource(listSituacao);
			}
		}
		if (listPorPesquisaSatisfacao != null) {
			listaPorPesquisaSatisfacaoDataSource = new JRBeanCollectionDataSource(listPorPesquisaSatisfacao);
		}
		if (listPorSolicitante != null) {
			listaPorSolicitanteDataSource = new JRBeanCollectionDataSource(listPorSolicitante);
		}
		
		RelatorioQuantitativoSolicitacaoDTO relatorioQuantitativoSolictitacaoDto = new RelatorioQuantitativoSolicitacaoDTO();
		
		if (listaPorSolicitanteDataSource != null) {
			relatorioQuantitativoSolictitacaoDto.setListaPorSolicitante(listaPorSolicitanteDataSource);
		}
		if (listaPorGrupoDataSource != null) {
			relatorioQuantitativoSolictitacaoDto.setListaPorGrupo(listaPorGrupoDataSource);
		}
		if (listaPorItemConfiguracaoDataSource != null) {
			relatorioQuantitativoSolictitacaoDto.setListaPorItemConfiguracao(listaPorItemConfiguracaoDataSource);
		}
		if (listaPorSituacaoDataSource != null) {
			relatorioQuantitativoSolictitacaoDto.setListaPorSituacao(listaPorSituacaoDataSource);
		}
		if (listaPorTipoDataSource != null) {
			relatorioQuantitativoSolictitacaoDto.setListaPorTipo(listaPorTipoDataSource);
		}
		if (listaPorOrigemDataSource != null) {
			relatorioQuantitativoSolictitacaoDto.setListaPorOrigem(listaPorOrigemDataSource);
		}
		if (listaPorPrioridadeDataSource != null) {
			relatorioQuantitativoSolictitacaoDto.setListaPorPrioridade(listaPorPrioridadeDataSource);
		}
		if (listaPorFaseDataSource != null) {
			relatorioQuantitativoSolictitacaoDto.setListaPorFase(listaPorFaseDataSource);
		}
		if (listaPorTipoServicoDataSource != null) {
			relatorioQuantitativoSolictitacaoDto.setListaPorTipoServico(listaPorTipoServicoDataSource);
		}
		if (listaPorResponsavelDataSource != null) {
			relatorioQuantitativoSolictitacaoDto.setListaPorResponsavel(listaPorResponsavelDataSource);
		}
		if (listaPorHoraAberturaDataSource != null) {
			relatorioQuantitativoSolictitacaoDto.setListaPorHoraAbertura(listaPorHoraAberturaDataSource);
		}
		if (listaPorSituacaoSLADataSource != null) {
			relatorioQuantitativoSolictitacaoDto.setListaPorSituacaoSLA(listaPorSituacaoSLADataSource);
		}
		if (listaPorPesquisaSatisfacaoDataSource != null) {
			relatorioQuantitativoSolictitacaoDto.setListaPorPesquisaSatisfacao(listaPorPesquisaSatisfacaoDataSource);
		}
		listDadosRelatorio.add(relatorioQuantitativoSolictitacaoDto);
		return listDadosRelatorio;
	}
	
	public Map<String, Object> makeParameters(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception{
		HttpSession session = ((HttpServletRequest) request).getSession();
		SolicitacaoServicoDTO solicitacaoServicoDto = (SolicitacaoServicoDTO) document.getBean();
		SolicitacaoServicoService solicitacaoService = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);
		ContratoDTO contratoDto = new ContratoDTO();
		ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
		usuario = WebUtil.getUsuario(request);
		if (usuario == null) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.sessaoExpirada"));
			document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + request.getContextPath() + "'");
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return null;
		}
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros = UtilRelatorio.trataInternacionalizacaoLocale(session, parametros);
		
		parametros.put("TITULO_RELATORIO", UtilI18N.internacionaliza(request, "relatorioQuantitativo.relatorioQuantitativo"));
		parametros.put("CIDADE", getCidadeParametrizada(request));
		parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
		parametros.put("NOME_USUARIO", usuario.getNomeUsuario());
		parametros.put("dataInicio", solicitacaoServicoDto.getDataInicio());
		parametros.put("dataFim", solicitacaoServicoDto.getDataFim());
		parametros.put("Logo", LogoRel.getFile());
		if (solicitacaoServicoDto.getIdContrato() != null) {
			contratoDto.setIdContrato(solicitacaoServicoDto.getIdContrato());
			contratoDto = (ContratoDTO) contratoService.restore(contratoDto);
		}
		parametros.put("contrato", contratoDto.getNumero());
		if (solicitacaoServicoDto.getSituacao() != null && !solicitacaoServicoDto.getSituacao().isEmpty()) {
			parametros.put("situacao", UtilI18N.internacionaliza(request, "solicitacaoServico.situacao." + solicitacaoServicoDto.getSituacao()));
		}
		if (solicitacaoServicoDto.getTipoUsuario() != null && !solicitacaoServicoDto.getTipoUsuario().isEmpty()) {
			parametros.put("tipoUsuario", UtilI18N.internacionaliza(request, "citcorpore.comum." + solicitacaoServicoDto.getTipoUsuario()));
		}
		parametros.put("SUBREPORT_DIR", CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS"));
		
		return parametros;
	}
	/**
	 * Faz a impress�o do relat�rio no formato pdf.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author Thays.araujo
	 */
	public void imprimirRelatorioQuantitativo(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Collection<RelatorioQuantitativoSolicitacaoDTO> data = this.makeDataSource(document, request, response);
		Map<String, Object> parametros = this.makeParameters(document, request, response);
		
		//Configurando dados para gera��o do Relat�rio
		StringBuilder jasperArqRel = new StringBuilder();
		jasperArqRel.append("RelatorioQuantitativo");
		Date dt = new Date();
		String strMiliSegundos = Long.toString(dt.getTime());
		String caminhoJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS");
		String diretorioTemp = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
		String diretorioRelativo = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";
		String arquivoRelatorio = "/"+ jasperArqRel + strMiliSegundos + "_" + usuario.getIdUsuario();

		if (data.size() == 0) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		JRDataSource dataSource = new JRBeanCollectionDataSource(data);
		this.abreRelatorioPDF(dataSource, parametros, diretorioTemp, caminhoJasper, jasperArqRel.toString(), diretorioRelativo, arquivoRelatorio, document, request, response);

		document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
	}

	/**
	 * Faz a impress�o do relat�rio no formato xls.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author Thays.araujo
	 */
	public void imprimirRelatorioQuantitativoXls(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Collection<RelatorioQuantitativoSolicitacaoDTO> data = this.makeDataSource(document, request, response);
		Map<String, Object> parametros = this.makeParameters(document, request, response);
		
		//Configurando dados para gera��o do Relat�rio
		StringBuilder jasperArqRel = new StringBuilder();
		jasperArqRel.append("RelatorioQuantitativo");
		Date dt = new Date();
		String strMiliSegundos = Long.toString(dt.getTime());
		String caminhoJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS");
		String diretorioTemp = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
		String diretorioRelativo = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";
		String arquivoRelatorio = "/"+ jasperArqRel + strMiliSegundos + "_" + usuario.getIdUsuario();

		if (data.size() == 0) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioVazio"));
			document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			return;
		}

		JRDataSource dataSource = new JRBeanCollectionDataSource(data);
		this.abreRelatorioXLS(dataSource, parametros, diretorioTemp, caminhoJasper, jasperArqRel.toString(), diretorioRelativo, arquivoRelatorio, document, request, response);

		document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
	}

	public void abreRelatorioPDF(JRDataSource dataSource, Map<String, Object> parametros, String diretorioTemp, String caminhoJasper, String jasperArqRel, String diretorioRelativo, String arquivoRelatorio, DocumentHTML document, HttpServletRequest request, HttpServletResponse response){
		try
		{
			JRSwapFile arquivoSwap = new JRSwapFile(diretorioTemp, 4096, 25);
			JRAbstractLRUVirtualizer virtualizer = new JRSwapFileVirtualizer(25, arquivoSwap, true);
			parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
			JasperPrint print = JasperFillManager.fillReport(caminhoJasper+jasperArqRel+".jasper", parametros, dataSource);

			JasperExportManager.exportReportToPdfFile(print, diretorioTemp + arquivoRelatorio + ".pdf");

			document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url="
					+ diretorioRelativo + arquivoRelatorio + ".pdf')");
		} catch(OutOfMemoryError e) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.erro.erroServidor"));
		} catch (JRException e) {
			e.printStackTrace();
		}
	}

	public void abreRelatorioXLS(JRDataSource dataSource, Map<String, Object> parametros, String diretorioTemp, String caminhoJasper, String jasperArqRel, String diretorioRelativo, String arquivoRelatorio, DocumentHTML document, HttpServletRequest request, HttpServletResponse response){
		try
		{
			final JasperDesign desenho = JRXmlLoader.load(caminhoJasper + jasperArqRel +".jrxml");
			desenho.setLanguage("java");
			final JasperReport relatorio = JasperCompileManager.compileReport(desenho);
			final JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, dataSource);
			final JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(impressao));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(diretorioTemp + arquivoRelatorio + ".xlsx"));
            XlsxReportConfiguration xlsxReportConfiguration = new SimpleXlsxReportConfiguration();
            exporter.setConfiguration(xlsxReportConfiguration);
			exporter.exportReport();
			document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + diretorioRelativo + arquivoRelatorio + ".xlsx')");
		} catch(OutOfMemoryError | JRException e) {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.erro.erroServidor"));
		}
	}
}
