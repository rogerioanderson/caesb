/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLElement;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLTable;
import br.com.centralit.citcorpore.bean.CatalogoServicoDTO;
import br.com.centralit.citcorpore.bean.ClienteDTO;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.FornecedorDTO;
import br.com.centralit.citcorpore.bean.InfoCatalogoServicoDTO;
import br.com.centralit.citcorpore.bean.ServContratoCatalogoServDTO;
import br.com.centralit.citcorpore.bean.ServicoDTO;
import br.com.centralit.citcorpore.negocio.CatalogoServicoService;
import br.com.centralit.citcorpore.negocio.ClienteService;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.FornecedorService;
import br.com.centralit.citcorpore.negocio.InfoCatalogoServicoService;
import br.com.centralit.citcorpore.negocio.ServicoService;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;
import br.com.citframework.util.UtilStrings;

/**
 * 
 * @author pedro
 * 
 */
@SuppressWarnings({ "unused", "unchecked" })
public class CatalogoServico extends AjaxFormAction {

	/**
	 * Inicializa os dados ao carregar a tela.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@Override
	public void load(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final HTMLForm form = document.getForm("form");
		form.clear();
	}

	/**
	 * Inclui registro.
	 * 
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void save(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final CatalogoServicoDTO catalogoServicoDTO = (CatalogoServicoDTO) document.getBean();
		final CatalogoServicoService catalogoServicoService = (CatalogoServicoService) ServiceLocator.getInstance().getService(CatalogoServicoService.class,
				WebUtil.getUsuarioSistema(request));

		final Collection<InfoCatalogoServicoDTO> colinfoCatServico = br.com.citframework.util.WebUtil.deserializeCollectionFromRequest(InfoCatalogoServicoDTO.class,
				"infoCatalogoServicoSerialize", request);
		// aspas simples e "\n" da erro nos serializes, este la�o resolve o problema substituindo aspas
		// simples e "\n"por
		// aspas duplas e espa�o em branco, acontecia este problema na p�gina: pages/portal2/portal2.load.
		if (colinfoCatServico != null) {
			for (final InfoCatalogoServicoDTO infoCatServ : colinfoCatServico) {
				infoCatServ.setDescInfoCatalogoServico(infoCatServ.getDescInfoCatalogoServico().replaceAll("[\']", "\""));
				infoCatServ.setDescInfoCatalogoServico(infoCatServ.getDescInfoCatalogoServico().replaceAll("[\\n]", " "));
			}
		}
		if (colinfoCatServico != null) {
			catalogoServicoDTO.setColInfoCatalogoServico((List<InfoCatalogoServicoDTO>) colinfoCatServico);
		}

		if (catalogoServicoDTO.getIdCatalogoServico() == null || catalogoServicoDTO.getIdCatalogoServico().intValue() == 0) {
			catalogoServicoDTO.setDataInicio(UtilDatas.getDataAtual());

			if (catalogoServicoService.verificaSeCatalogoExiste(catalogoServicoDTO)) {
				document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.registroJaCadastrado"));
				return;
			}
			catalogoServicoService.create(catalogoServicoDTO);
			document.alert(UtilI18N.internacionaliza(request, "MSG05"));
		} else {
			if (catalogoServicoService.verificaSeCatalogoExiste(catalogoServicoDTO)) {
				document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.registroJaCadastrado"));
				return;
			}

			catalogoServicoService.update(catalogoServicoDTO);
			document.alert(UtilI18N.internacionaliza(request, "MSG06"));
		}
		final HTMLForm form = document.getForm("form");
		document.executeScript("limpar()");
		document.executeScript("limpar_LOOKUP_CATALOGOSERVICO()");
	}

	/**
	 * Restaura os dados ao clicar em um registro.
	 * 
	 * @author pedro
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void restore(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		CatalogoServicoDTO catalogoServicoDTO = (CatalogoServicoDTO) document.getBean();
		final CatalogoServicoService catalogoServicoService = (CatalogoServicoService) ServiceLocator.getInstance().getService(CatalogoServicoService.class,
				WebUtil.getUsuarioSistema(request));
		final ServicoService servicoService = (ServicoService) ServiceLocator.getInstance().getService(ServicoService.class, WebUtil.getUsuarioSistema(request));
		final ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, WebUtil.getUsuarioSistema(request));
		final FornecedorService fornService = (FornecedorService) ServiceLocator.getInstance().getService(FornecedorService.class, WebUtil.getUsuarioSistema(request));
		final ClienteService clienteService = (ClienteService) ServiceLocator.getInstance().getService(ClienteService.class, WebUtil.getUsuarioSistema(request));
		catalogoServicoDTO = (CatalogoServicoDTO) catalogoServicoService.restore(catalogoServicoDTO);

		// Recupera nome do contrato
		if (catalogoServicoDTO != null && catalogoServicoDTO.getIdContrato() != null) {
			ContratoDTO contratosDTO = new ContratoDTO();
			FornecedorDTO fornecedorDTO = new FornecedorDTO();

			ClienteDTO clienteDTO = new ClienteDTO();
			if (catalogoServicoDTO != null) {
				contratosDTO.setIdContrato(catalogoServicoDTO.getIdContrato());
				contratosDTO = (ContratoDTO) contratoService.restore(contratosDTO);
			}
			if (contratosDTO != null) {
				clienteDTO.setIdCliente(contratosDTO.getIdCliente());
				clienteDTO = (ClienteDTO) clienteService.restore(clienteDTO);
				fornecedorDTO.setIdFornecedor(contratosDTO.getIdFornecedor());
				fornecedorDTO = (FornecedorDTO) fornService.restore(fornecedorDTO);
				document.executeScript("document.form.idContrato.value= " + contratosDTO.getIdContrato() + "");
				catalogoServicoDTO.setNomeContrato(contratosDTO.getNumero() + " - " + clienteDTO.getNomeFantasia() + " - " + fornecedorDTO.getRazaoSocial());
			}
		}
		document.executeScript("deleteAllRows();");

		if (catalogoServicoDTO != null && catalogoServicoDTO.getColServicoContrato() != null) {

			for (final ServContratoCatalogoServDTO servicos : catalogoServicoDTO.getColServicoContrato()) {
				if (servicos != null) {
					final ServContratoCatalogoServDTO servContratoCatalogoServDTO = new ServContratoCatalogoServDTO();
					ServicoDTO servicoDTO = new ServicoDTO();
					servicoDTO.setIdServico(servicos.getIdServicoContrato());
					servicoDTO = (ServicoDTO) servicoService.restore(servicoDTO);
					servicos.setNomeServico(servicoDTO.getNomeServico());
					servicos.setIdServicoContrato(servicoDTO.getIdServico());
				}
			}
		}

		if (catalogoServicoDTO != null && catalogoServicoDTO.getColInfoCatalogoServico() != null) {
			HTMLTable table;
			table = document.getTableById("tblInfoCatalogoServico");
			table.deleteAllRows();
			table.addRowsByCollection(catalogoServicoDTO.getColInfoCatalogoServico(), new String[] { "", "idServicoCatalogo", "nomeServicoContrato", "nomeInfoCatalogoServico",
					"descInfoCatalogoServico" }, null, "", new String[] { "gerarButtonDelete2" }, "funcaoClickRow", null);

		}

		final HTMLForm form = document.getForm("form");
		form.clear();

		if (catalogoServicoDTO != null) {
			form.setValues(catalogoServicoDTO);
		}
	}

	public void delete(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final CatalogoServicoDTO catalogoServicoDTO = (CatalogoServicoDTO) document.getBean();
		final CatalogoServicoService catalogoServicoService = (CatalogoServicoService) ServiceLocator.getInstance().getService(CatalogoServicoService.class,
				WebUtil.getUsuarioSistema(request));

		if (catalogoServicoDTO.getIdCatalogoServico() != null) {
			catalogoServicoDTO.setDataFim(UtilDatas.getDataAtual());
			catalogoServicoService.update(catalogoServicoDTO);
			document.alert(UtilI18N.internacionaliza(request, "MSG07"));
		} else {
			document.alert(UtilI18N.internacionaliza(request, "citcorpore.comum.nome"));
		}
		document.executeScript("deleteAllRows();");
		final HTMLForm form = document.getForm("form");
		form.clear();
		document.executeScript("limpar_LOOKUP_CATALOGOSERVICO()");
	}

	@Override
	public Class<CatalogoServicoDTO> getBeanClass() {
		return CatalogoServicoDTO.class;
	}

	public void restoreInfo(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final CatalogoServicoDTO catalogoServicoDTO = (CatalogoServicoDTO) document.getBean();
		final InfoCatalogoServicoService infoService = (InfoCatalogoServicoService) ServiceLocator.getInstance().getService(InfoCatalogoServicoService.class,
				WebUtil.getUsuarioSistema(request));

		InfoCatalogoServicoDTO infoCatalogoServicoDTO = new InfoCatalogoServicoDTO();

		if (catalogoServicoDTO.getIdInfoCatalogoServico() != null) {
			infoCatalogoServicoDTO.setIdInfoCatalogoServico(catalogoServicoDTO.getIdInfoCatalogoServico());
			infoCatalogoServicoDTO.setIdCatalogoServico(catalogoServicoDTO.getIdCatalogoServico());
			infoCatalogoServicoDTO = (InfoCatalogoServicoDTO) infoService.restore(infoCatalogoServicoDTO);

			if (infoCatalogoServicoDTO != null) {

				final Integer id = infoCatalogoServicoDTO.getIdInfoCatalogoServico();
				final String text = UtilStrings.nullToVazio(infoCatalogoServicoDTO.getDescInfoCatalogoServico());
				final String nome = infoCatalogoServicoDTO.getNomeInfoCatalogoServico();
				final Integer index = catalogoServicoDTO.getRowIndex();

				document.executeScript("setaInfo('" + nome + "', '" + text + "', '" + index + "');");
			}
		}

	}

	/**
	 * Adiciona/atualiza grid servico.
	 * 
	 * @author pedro
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void adicionaGridServico(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final CatalogoServicoDTO catalogoServicoDTO = (CatalogoServicoDTO) document.getBean();
		final ServicoService servicoContratoService = (ServicoService) ServiceLocator.getInstance().getService(ServicoService.class, null);
		ServicoDTO bean = new ServicoDTO();
		final ServContratoCatalogoServDTO servContratoCatalogoServDTO = new ServContratoCatalogoServDTO();
		bean.setIdServico(catalogoServicoDTO.getIdServicoContrato());
		bean = (ServicoDTO) servicoContratoService.restore(bean);

		servContratoCatalogoServDTO.setIdServicoContrato(bean.getIdServico());
		servContratoCatalogoServDTO.setNomeServico(bean.getNomeServico());
	}

	/**
	 * Retorna o Conteudo de cat�logo de servi�o
	 * 
	 * @author pedro
	 * @param document
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void conteudoInfoCatalogoServico(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final CatalogoServicoDTO catalogoServicoDTO = (CatalogoServicoDTO) document.getBean();
		final InfoCatalogoServicoService infoService = (InfoCatalogoServicoService) ServiceLocator.getInstance().getService(InfoCatalogoServicoService.class,
				WebUtil.getUsuarioSistema(request));

		InfoCatalogoServicoDTO infoCatalogoServicoDTO = new InfoCatalogoServicoDTO();
		if (catalogoServicoDTO.getIdInfoCatalogoServico() != null) {
			infoCatalogoServicoDTO.setIdInfoCatalogoServico(catalogoServicoDTO.getIdInfoCatalogoServico());
			infoCatalogoServicoDTO = (InfoCatalogoServicoDTO) infoService.restore(infoCatalogoServicoDTO);
			document.executeScript("$('#tituloCatalogo').text('" + infoCatalogoServicoDTO.getNomeInfoCatalogoServico() + "');" + "$('#POPUP_CONTEUDOCATALOGO').dialog('open');");
			final HTMLElement m = document.getElementById("conteudoCatalogo");
			m.setInnerHTML(infoCatalogoServicoDTO.getDescInfoCatalogoServico());
		}
	}

	public void verificarContratoServico(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final CatalogoServicoDTO catalogoServicoDTO = (CatalogoServicoDTO) document.getBean();
		final InfoCatalogoServicoService infoService = (InfoCatalogoServicoService) ServiceLocator.getInstance().getService(InfoCatalogoServicoService.class,
				WebUtil.getUsuarioSistema(request));
		if (infoService.findByContratoServico(catalogoServicoDTO.getIdContrato())) {
			document.executeScript("$('#POPUP_DETALHES').dialog('open');");
		} else {
			document.executeScript("validarContratoServico();");
		}
	}
}
