/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citajax.html.HTMLForm;
import br.com.centralit.citajax.html.HTMLSelect;
import br.com.centralit.citcorpore.bean.ComandoDTO;
import br.com.centralit.citcorpore.bean.ComandoSistemaOperacionalDTO;
import br.com.centralit.citcorpore.bean.SistemaOperacionalDTO;
import br.com.centralit.citcorpore.negocio.ComandoService;
import br.com.centralit.citcorpore.negocio.ComandoSistemaOperacionalService;
import br.com.centralit.citcorpore.negocio.SistemaOperacionalService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilI18N;

/**
 * @author ygor.magalhaes
 *
 */

@SuppressWarnings("rawtypes")
public class ComandoSistemaOperacional extends AjaxFormAction {

	public void load(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {

		SistemaOperacionalService sistemaOperacionalService = (SistemaOperacionalService) ServiceLocator.getInstance().getService(SistemaOperacionalService.class, null);

		// Verificando a exist�ncia do servi�o.
		if (sistemaOperacionalService != null) {

			HTMLSelect selectSO = (HTMLSelect) document.getSelectById("idSistemaOperacional");

			Collection<SistemaOperacionalDTO> lista = sistemaOperacionalService.list();

			selectSO.removeAllOptions();
			selectSO.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione") );

			for (SistemaOperacionalDTO sis : lista) {
				selectSO.addOption(String.valueOf(sis.getId() ), StringEscapeUtils.escapeJavaScript(sis.getNome() ));
			}

			ComandoService comandoService = (ComandoService) ServiceLocator.getInstance().getService(ComandoService.class, null);

			HTMLSelect selectComando = (HTMLSelect) document.getSelectById("idComando");

			Collection<ComandoDTO> listaComando = comandoService.list();

			selectComando.removeAllOptions();

			selectComando.addOption("", UtilI18N.internacionaliza(request, "citcorpore.comum.selecione") );

			for (ComandoDTO sis : listaComando) {
				selectComando.addOption(String.valueOf(sis.getId() ), StringEscapeUtils.escapeJavaScript(sis.getDescricao() ));
			}
		}
	}

	public void save(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		// Obtendo os dados do formul�rio.
		ComandoSistemaOperacionalDTO comandoSODTO = (ComandoSistemaOperacionalDTO) document.getBean();
		// Criando servi�o (objeto da camada de servi�o).
		ComandoSistemaOperacionalService comandoSOService = (ComandoSistemaOperacionalService) ServiceLocator.getInstance()
				.getService(ComandoSistemaOperacionalService.class, null);

		// Verificando a exist�ncia do DTO e do servi�o.
		if (comandoSODTO != null && comandoSOService != null) {
			// Inserindo o comando de SO.
			if (comandoSODTO.getId() == null || comandoSODTO.getId().intValue() == 0) {
				Collection comandoSOJaCadastrado = comandoSOService.find(comandoSODTO);

				// boolean comandoSOjaExiste = comandoSOService.pesquisarExistenciaComandoSO(comandoSODTO);

				// Verificando se o comando de SO j� foi cadastrado.
				if (comandoSOJaCadastrado != null && !comandoSOJaCadastrado.isEmpty() ) {
					// Se verdadeiro ent�o alerta o usu�rio e pede para cadastrar outro comando de SO.
					document.alert(UtilI18N.internacionaliza(request, "MSE01") );
				} else {
					comandoSOService.create(comandoSODTO);
				    document.alert(UtilI18N.internacionaliza(request, "MSG05") );
				}
			} else { // Atualizando o comando de SO.
			    comandoSOService.update(comandoSODTO);
			    document.alert(UtilI18N.internacionaliza(request, "MSG06") );
			}

			// Limpando o formul�rio.
			HTMLForm form = document.getForm("form");
			form.clear();
		}
	}

	public void restore(DocumentHTML document, HttpServletRequest request, HttpServletResponse response) throws Exception {
		// Obtendo os dados do formul�rio de cadastro.
		ComandoSistemaOperacionalDTO comandoSODTO = (ComandoSistemaOperacionalDTO) document.getBean();
		// Obtendo o servi�o.
		ComandoSistemaOperacionalService comandoSOService = (ComandoSistemaOperacionalService) ServiceLocator.getInstance()
				.getService(ComandoSistemaOperacionalService.class, null);

		// Verificando a exist�ncia do DTO e do servi�o.
		if (comandoSODTO != null && comandoSOService != null) {
			// Recuperando o objeto.
			comandoSODTO = (ComandoSistemaOperacionalDTO) comandoSOService.restore(comandoSODTO);

			// Limpando o formul�rio.
			HTMLForm form = document.getForm("form");
			form.clear();

			// Preenchendo o formul�rio com os dados do objeto recuperado.
			form.setValues(comandoSODTO);
		}
	}

	public Class getBeanClass() {
		return ComandoSistemaOperacionalDTO.class;
	}
}
