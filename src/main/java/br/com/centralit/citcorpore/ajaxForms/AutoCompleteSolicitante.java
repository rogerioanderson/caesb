/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.ajaxForms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.AutoCompleteDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilStrings;

import com.google.gson.Gson;

public class AutoCompleteSolicitante extends AjaxFormAction {

	private final Gson gson = new Gson();

	@Override
	public Class<EmpregadoDTO> getBeanClass() {
		return EmpregadoDTO.class;
	}

	@Override
	public void load(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		if (request.getParameter("query") != null) {
			String consulta = UtilStrings.fixEncoding(request.getParameter("query"));

			final String idContratoStr = request.getParameter("contrato");
			Integer idContrato = null;
			if (StringUtils.isNotBlank(idContratoStr) && !idContratoStr.equals("-1")) {
				idContrato = Integer.parseInt(idContratoStr);
			}

			Collection<EmpregadoDTO> listEmpregadoDto = new ArrayList<>();

			String idUnidadeStr = request.getParameter("unidade");
			Integer idUnidade = null;
			if (StringUtils.isNotBlank(idUnidadeStr) && !idUnidadeStr.equals("-1")) {
				idUnidade = Integer.parseInt(idUnidadeStr);
			}

			if (idContrato != null) {
				listEmpregadoDto = this.getEmpregadoService().findSolicitanteByNomeAndIdContratoAndIdUnidade(consulta, idContrato, idUnidade);
			}

			final AutoCompleteDTO autoCompleteDTO = new AutoCompleteDTO();

			final List<String> listNome = new ArrayList<>();
			final List<Integer> listIdEmpregado = new ArrayList<>();

			if (listEmpregadoDto != null && !listEmpregadoDto.isEmpty()) {
				for (final EmpregadoDTO empregadoDto : listEmpregadoDto) {
					if (empregadoDto.getIdEmpregado() != null) {
						listNome.add(empregadoDto.getNome());
						listIdEmpregado.add(empregadoDto.getIdEmpregado());
					}
				}
			}
			autoCompleteDTO.setQuery(consulta);
			autoCompleteDTO.setSuggestions(listNome);
			autoCompleteDTO.setData(listIdEmpregado);

			String json = "";

			if (request.getParameter("colection") != null) {
				json = gson.toJson(listEmpregadoDto);
			} else {
				json = gson.toJson(autoCompleteDTO);
			}

			request.setAttribute("json_response", json);
		}
	}

	private EmpregadoService empregadoService;

	private EmpregadoService getEmpregadoService() throws Exception {
		if (empregadoService == null) {
			empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
		}
		return empregadoService;
	}

}
