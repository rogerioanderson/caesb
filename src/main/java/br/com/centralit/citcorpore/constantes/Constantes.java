/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.constantes;

/**
 * @author Central IT
 */
public class Constantes {

    public static boolean LOADED = false;
    // Definicoes para usuarios
    public static final String USUARIO_INDEFINIDO = "0";
    public static final String USUARIO_NORMAL = "1";
    public static final String USUARIO_PUBLICO = "2";

    public static final String FORM_NORMAL = "0";
    public static final String MULT_FORM_DATA = "1";

    public static final String VERSAO = "CITSmart V.1.0 Beta";

    public static final String SIMBOLO_MOEDA = "R$";
    public static final String SENHA_PADRAO = "mudar";

    public static final boolean IMPRIMIR_ERROS = true;

    public static String RAIZ_SITE = br.com.citframework.util.Constantes.getValue("SERVER_ADDRESS") + "/citcorpore";

    public static String PROTOCOLO = "http://";

    public static String ENDERECO_SITE = br.com.citframework.util.Constantes.getValue("SERVER_ADDRESS");

    public static boolean IMPRIMIR_FORMULARIO_POSTADO = false;

    // Controle de erros - redirecionamentos
    public static String PAGINA_LOGIN = RAIZ_SITE + "/login.jsp";
    public static String PAGINA_SEM_ACESSO = RAIZ_SITE + "/erro/semacesso.jsp";

    // Outras Constantes
    public static final String SPACE = " ";
    public static final String DESC = "desc";
    public static final String CARACTER_PESQUISA = "%";
    public static final String CARACTER_SEPARADOR = "\5";

    public static final String CARACTER_SEPARADOR_PIPE = "|";

    public static final String INSERT = "I";
    public static final String UPDATE = "U";

    public static final String EMAIL_DE = "local@portal.com.br";

    public static final int INTERVALO_PADRAO = 15; // 15 minutos

    public static final int CODIGO_CONVENIO_PARTICULAR = 1;

    /**
     * Defini��o de campos utilizados no sistema.
     */
    public static final int CAMPO_TEXTO = 1;
    public static final int CAMPO_NUMERO = 2;
    public static final int CAMPO_DATA = 3;
    public static final int CAMPO_LISTA = 4;
    public static final int CAMPO_TABELA = 5;

    public static final String ESTILO_LISTA = "L";
    public static final String ESTILO_RADIO = "R";
    public static final String ESTILO_CHECK = "C";

    public static final int SEQUENCIA_OUTROS = 1000;
    public static final String TEXTO_OUTROS = "Outro";

    public static final int TEMPLATE_APLICACAO_ANAMNESE = 1;

    public static String CAMINHO_GERAL_FOTO_INDV = "/empresas";
    public static String CAMINHO_GERAL_TMP_IMG = "/empresas";
    public static String CAMINHO_GERAL_BD_IMG = "/empresas";
    public static String CAMINHO_JASPER = "/citsmart/tempFiles/";

    public static final int CID_9 = 9;
    public static final int CID_10 = 10;

    public static final int CODIGO_CH = 9;

    public static final Integer OPER_AGENDAMENTO = 0x1;
    public static final Integer OPER_REMARCACAO = 0x2;
    public static final Integer OPER_DESMARCACAO = 0x3;

}
