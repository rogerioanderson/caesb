/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Created on 29/09/2005
 *
 * 
 * Window - Preferences - Java - Code Style - Code Templates
 */
package br.com.centralit.citcorpore.mail;

import java.util.Date;
import java.util.Properties;

import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.citframework.util.UtilStrings;

/**
 * @author rogerio
 * 
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class ControleEmail implements Runnable {

	private String username;
	private String password;
	private String auth;
	private String servidorSMTP;
	private String porta;
	private String starttls;
	private MensagemEmail mensagem;

	private static final Logger LOGGER = Logger.getLogger(ControleEmail.class);
	
	public ControleEmail(MensagemEmail mensagem) throws Exception {
		this.username = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_Usuario, "");
		this.password = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_Senha, "");
		if (ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_Autenticacao, "N").equalsIgnoreCase("S")) {
			this.auth = "true";
		} else {
			this.auth = "false";
		}
		this.servidorSMTP = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_Servidor, "");
		this.porta = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_Porta, "587");
		if (ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_StartTLS, "N").equalsIgnoreCase("S")) {
			this.starttls = "true";
		} else {
			this.starttls = "false";
		}
		this.mensagem = mensagem;
	}

	public void send(String to, String cc, String bcc, String from, String subject, String text) throws Exception {
		mensagem = new MensagemEmail(to, cc, bcc, from, subject, text);
		this.send();
		return;
	}

	public void send(String to, String cc, String bcc, String from, String subject, String text, boolean confirmarLeituraMail) throws Exception {
		mensagem = new MensagemEmail(to, cc, bcc, from, subject, text);
		mensagem.setConfirmarLeituraMail(confirmarLeituraMail);
		this.send();
		return;
	}

	public void send() throws Exception {
		try {
	    	Properties mailProps=new Properties();
	    	mailProps.put("mail.smtp.auth", this.auth);
	    	mailProps.put("mail.smtp.host", this.servidorSMTP);
	    	mailProps.put("mail.smtp.port", this.porta);
	    	mailProps.put("mail.smtp.starttls.enable", this.starttls);
            
	    	/**
			 * Motivo: Altera��o para resolu��o de incidente. Se n�o exige autentica��o pelo parametro ent�o o mesmo n�o ser� atribuido Autor: flavio.santana Data/Hora: 02/12/2013
			 */
	    	
	    	Session mailSession = null;
			if (ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_Autenticacao, "N").equalsIgnoreCase("S")) {
	    		mailSession = Session.getInstance(mailProps, new javax.mail.Authenticator() {
	    			@Override
					protected PasswordAuthentication getPasswordAuthentication() {
	    				return new PasswordAuthentication(username, password);
	    			}
	    		});
	    	} else {
	    		mailSession = Session.getInstance(mailProps);
	    	}
	    	
			mailSession.setDebug(false);
			Message email = new MimeMessage(mailSession);
			
			email.setFrom(new InternetAddress(mensagem.getFrom()));
			email.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mensagem.getTo()));
			if (mensagem.getCc() != null && mensagem.getCc().trim().length() > 0) {
				email.setRecipients(Message.RecipientType.CC, InternetAddress.parse(mensagem.getCc()));
			}
			if (mensagem.getCco() != null && mensagem.getCco().trim().length() > 0) {
				email.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(mensagem.getCco()));
			}
			
			email.setSubject(MimeUtility.encodeText(mensagem.getSubject(), "ISO-8859-1", "B"));
			
			try {
				email.setSentDate(new Date());
			} catch (Exception e) {
				System.out.println("ERRO AO SETAR A DATA EM Message email = new MimeMessage(mailSession)");
				e.printStackTrace();
			}
			
            MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
         	mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
         	mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
         	mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
         	mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
         	mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
         	CommandMap.setDefaultCommandMap(mc);

			email.setContent((StringEscapeUtils.unescapeHtml(mensagem.getText())), "text/html; charset=ISO-8859-1;");
			
			// Adicionar header para pedir confirmacao de leitura
			if (mensagem.isConfirmarLeituraMail()) { 
				email.addHeader("Disposition-Notification-To", mensagem.getFrom());
			}

			System.out.println(UtilStrings.fixEncoding("Envio de e-mail - Destinatario: " + mensagem.getTo() + " Assunto: " + email.getSubject()));
			Transport.send(email);
		} catch (Exception e) {
			LOGGER.error("PROBLEMAS AO ENVIAR EMAIL!", e);
		}
		return;
	}

	/*
	 * Codigo antigo public void send() throws Exception { if (USE_MAIL) { try { Properties mailProps = new Properties(); mailProps.put("mail.smtp.host",
	 * ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.EmailSMTP, null)); mailProps.put("mail.transport.protocol", "smtp"); Autenticador auth; Session mailSession; if (!NEED_AUTH) { //
	 * N�o ha necessidade de autenticacao mailProps.put("mail.smtp.auth", "false"); mailSession = Session.getInstance(mailProps); } else {
	 * ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.EmailAutenticacao, "N").equalsIgnoreCase("S"); // caso haja necessidade de autenticacao auth = new
	 * Autenticador(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.EmailUsuario, null), ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.EmailSenha, null));
	 * mailProps.put("mail.smtp.auth", "true"); mailProps.put("mail.smtp.submitter", auth.username); mailProps.put("mail.user", auth.username); mailProps.put("mail.pwd", auth.password);
	 * mailProps.put("mail.password", auth.password); mailProps.put("mail.from", mensagem.getFrom()); mailProps.put("mail.to", mensagem.getTo()); String parametroGmail =
	 * ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_GMAIL, "N"); if (parametroGmail != null && !StringUtils.isEmpty(parametroGmail) && StringUtils.contains(parametroGmail,
	 * "S")) { mailProps.put("mail.smtp.starttls.enable", "true"); mailProps.put("mail.smtp.socketFactory.port", (ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_LEITURA_Porta,
	 * "465"))); mailProps.put("mail.smtp.socketFactory.fallback", "false"); mailProps.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); } mailSession =
	 * Session.getInstance(mailProps, auth); } mailSession.setDebug(false); Message email = new MimeMessage(mailSession); email.setRecipients(Message.RecipientType.TO,
	 * InternetAddress.parse(mensagem.getTo())); if (mensagem.getCc() != null && mensagem.getCc().trim().length() > 0) { email.setRecipients(Message.RecipientType.CC,
	 * InternetAddress.parse(mensagem.getCc())); } if (mensagem.getCco() != null && mensagem.getCco().trim().length() > 0) { email.setRecipients(Message.RecipientType.BCC,
	 * InternetAddress.parse(mensagem.getCco())); } email.setFrom(new InternetAddress(mensagem.getFrom())); email.setSubject(MimeUtility.encodeText(mensagem.getSubject(), "ISO-8859-1", "B")); try {
	 * email.setSentDate(new Date()); } catch (Exception e) { System.out.println("ERRO AO SETAR A DATA EM Message email = new MimeMessage(mailSession)"); e.printStackTrace(); } //
	 * email.setContent(msg.getText(), "text/html; charset=" + // System.getProperty("file.encoding") + ";"); email.setContent(Util.encodeHTML(mensagem.getText()), "text/html; charset=ISO-8859-1;");
	 * if (mensagem.isConfirmarLeituraMail()) { // Adiciona header para // pedir confirmacao de // leitura email.addHeader("Disposition-Notification-To", mensagem.getFrom()); } // Transport transport
	 * = mailSession.getTransport(); Transport.send(email); //transport.connect(); transport.sendMessage(email, email.getRecipients(Message.RecipientType.TO)); transport.close(); } catch (Exception e)
	 * { System.out.println("PROBLEMAS AO ENVIAR EMAIL! "); System.out.println("[E]ERROR: " + e); e.printStackTrace(System.out); throw e; } // System.out.println(" [#] Email enviado"); return; } }
	 */	
	
	@Override
	public void run() {
		try {
			send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
