/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Created on 29/09/2005
 *
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
package br.com.centralit.citcorpore.mail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.centralit.citcorpore.bean.ModeloEmailDTO;
import br.com.centralit.citcorpore.negocio.ModeloEmailService;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Reflexao;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilStrings;

/**
 * @author rogerio
 *
 *
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class MensagemEmail {

	public String to;
	public String cc;
	public String cco;
	public String from;
	public String subject;
	public String text;
	public boolean confirmarLeituraMail;

	public MensagemEmail(String to, String cc, String cco, String from, String subject, String text) {
		this.setTo(to);
		this.setCc(cc);
		this.setCco(cco);
		this.setFrom(from);
		this.setSubject(subject);
		this.setText(text);
		this.setConfirmarLeituraMail(false);
	}

	public MensagemEmail(Integer idModelo, Map<String, String> map) throws Exception {
		criar(idModelo, map);
	}

	public MensagemEmail(Integer idModelo, IDto[] bean) throws Exception {
		Map<String, String> map = new HashMap<>();
		if (bean != null && bean.length > 0) {
			for (int x = 0; x < bean.length; x++) {
				try {
					List<?> lstGets = Reflexao.findGets(bean[x]);
					for (int i = 0; i < lstGets.size(); i++) {
						String propriedade = UtilStrings.convertePrimeiraLetra((String) lstGets.get(i), "L");
						Object value = Reflexao.getPropertyValue(bean[x], propriedade);
						if (value == null)
							continue;
						String id = UtilStrings.convertePrimeiraLetra(propriedade, "L");
						if (String.class.isInstance(value))
							map.put(id, (String) value);
						else if (Integer.class.isInstance(value))
							map.put(id, ((Integer) value).toString());
						else if (java.sql.Date.class.isInstance(value))
							map.put(id, UtilDatas.dateToSTR(((java.sql.Date) value)));
						else if (java.sql.Timestamp.class.isInstance(value))
							map.put(id, UtilDatas.convertDateToString(TipoDate.TIMESTAMP_DEFAULT, ((java.sql.Timestamp) value), null));

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		criar(idModelo, map);
	}

	public void criar(Integer idModelo, Map<String, String> map) throws Exception {

		if (idModelo == null || idModelo.intValue() < 0)
			return;

		ModeloEmailService modeloEmailService = (ModeloEmailService) ServiceLocator.getInstance().getService(ModeloEmailService.class, null);

		ModeloEmailDTO modeloEmailDto = new ModeloEmailDTO();

		modeloEmailDto.setIdModeloEmail(idModelo);
		modeloEmailDto = (ModeloEmailDTO) modeloEmailService.restore(modeloEmailDto);

		if (modeloEmailDto == null)
			throw new LogicException("Modelo de E-mail n�o parametrizado.");

		String texto = modeloEmailDto.getTexto();
		String titulo = modeloEmailDto.getTitulo();

		try {
			for (String key : map.keySet()) {
				if (texto != null) {
					texto = texto.replace(String.format("${%s}", key.toUpperCase()), map.get(key));
				}
				if (titulo != null) {
					titulo = titulo.replace(String.format("${%s}", key.toUpperCase()), map.get(key));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// remove poss�veis chaves que n�o encontraram valores no map
		if (texto != null) {
			texto = texto.replaceAll("\\$\\{[^}]*\\}", "");
		}

		this.setText(texto);
		this.setSubject(titulo);
		this.setConfirmarLeituraMail(false);
	}

	public void envia(String to, String cc, String from) throws Exception {
		this.setTo(to);
		this.setCc(cc);
		this.setFrom(from);
		new Thread(new ControleEmail(this)).start();
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getCco() {
		return cco;
	}

	public void setCco(String cco) {
		this.cco = cco;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isConfirmarLeituraMail() {
		return confirmarLeituraMail;
	}

	public void setConfirmarLeituraMail(boolean confirmarLeituraMail) {
		this.confirmarLeituraMail = confirmarLeituraMail;
	}
}
