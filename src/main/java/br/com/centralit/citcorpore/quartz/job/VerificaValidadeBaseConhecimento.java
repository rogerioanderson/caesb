/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.quartz.job;

import java.sql.Date;
import java.util.Collection;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.mail.MensagemEmail;
import br.com.centralit.citcorpore.negocio.BaseConhecimentoService;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.UsuarioService;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.citframework.dto.IDto;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;

public class VerificaValidadeBaseConhecimento implements Job {

    @Override
    public void execute(final JobExecutionContext arg0) throws JobExecutionException {
        Collection<BaseConhecimentoDTO> colBaseConhecimento = null;
        UsuarioDTO usuarioDtoAutor = new UsuarioDTO();
        UsuarioDTO usuarioDtoAvaliador = new UsuarioDTO();

        EmpregadoDTO empregadoDtoAutor = new EmpregadoDTO();
        EmpregadoDTO empregadoDtoAvaliador = new EmpregadoDTO();

        try {
            String AVISAR_DATAEXPIRACAO_BASECONHECIMENTO = "1";
            String ID_MODELO_EMAIL_EXPIRACAO_BASECONHECIMENTO = "7";
			final String remetente = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_RemetenteNotificacoesSolicitacao, null);

            if (AVISAR_DATAEXPIRACAO_BASECONHECIMENTO == null || AVISAR_DATAEXPIRACAO_BASECONHECIMENTO.isEmpty()) {
                AVISAR_DATAEXPIRACAO_BASECONHECIMENTO = "90";
            }

            if (ID_MODELO_EMAIL_EXPIRACAO_BASECONHECIMENTO == null || ID_MODELO_EMAIL_EXPIRACAO_BASECONHECIMENTO.isEmpty()) {
                ID_MODELO_EMAIL_EXPIRACAO_BASECONHECIMENTO = "6";
            }

            Date dataAtual = UtilDatas.getDataAtual();

            dataAtual = (Date) UtilDatas.incrementaDiasEmData(dataAtual, Integer.parseInt(AVISAR_DATAEXPIRACAO_BASECONHECIMENTO));

            final BaseConhecimentoService baseConhecimentoService = (BaseConhecimentoService) ServiceLocator.getInstance().getService(BaseConhecimentoService.class, null);
            final EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
            final UsuarioService usuarioService = (UsuarioService) ServiceLocator.getInstance().getService(UsuarioService.class, null);

            final BaseConhecimentoDTO baseConhecimento = new BaseConhecimentoDTO();
            baseConhecimento.setDataExpiracao(dataAtual);

            colBaseConhecimento = baseConhecimentoService.listaBaseConhecimentoUltimasVersoes(baseConhecimento);

            if (!colBaseConhecimento.isEmpty()) {
                for (final BaseConhecimentoDTO baseConhecimentoDTO : colBaseConhecimento) {
                    if (baseConhecimentoDTO.getIdUsuarioAutor() != null) {
                        usuarioDtoAutor = usuarioService.restoreByID(baseConhecimentoDTO.getIdUsuarioAutor());
                    }
                    if (baseConhecimentoDTO.getIdUsuarioAprovador() != null) {
                        usuarioDtoAvaliador = usuarioService.restoreByID(baseConhecimentoDTO.getIdUsuarioAprovador());
                    }
                    if (usuarioDtoAutor.getIdEmpregado() != null) {
                        empregadoDtoAutor = empregadoService.restoreByIdEmpregado(usuarioDtoAutor.getIdEmpregado());
                    }
                    if (usuarioDtoAvaliador.getIdEmpregado() != null) {
                        empregadoDtoAvaliador = empregadoService.restoreByIdEmpregado(usuarioDtoAvaliador.getIdEmpregado());
                    }
                    final MensagemEmail mensagem = new MensagemEmail(Integer.parseInt(ID_MODELO_EMAIL_EXPIRACAO_BASECONHECIMENTO), new IDto[] {baseConhecimentoDTO});
                    mensagem.envia(empregadoDtoAutor.getEmail(), "", remetente);
                    mensagem.envia(empregadoDtoAvaliador.getEmail(), "", remetente);
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

}
