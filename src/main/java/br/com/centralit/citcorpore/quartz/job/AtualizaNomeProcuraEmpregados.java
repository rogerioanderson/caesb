/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.quartz.job;

import java.text.Normalizer;
import java.util.Collection;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.citframework.service.ServiceLocator;

public class AtualizaNomeProcuraEmpregados implements Job {
	@SuppressWarnings("unchecked")
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);

			boolean aindaExistemRegistros = true;
			int intervalo = 200;
			int indice = 0;
			do {
				Collection<EmpregadoDTO> empregados = empregadoService.listarIdEmpregados(intervalo, indice);
				if(empregados != null){
					for (EmpregadoDTO empregado : empregados) {
						empregado = (EmpregadoDTO) empregadoService.restore(empregado);
						String nomeProcura = empregado.getNome();
						nomeProcura = nomeProcura.trim();
						nomeProcura = nomeProcura.toUpperCase();
						nomeProcura = Normalizer.normalize(nomeProcura, Normalizer.Form.NFD);
						nomeProcura = nomeProcura.replaceAll("[^\\p{ASCII}]", "");
						empregado.setNomeProcura(nomeProcura);
						empregadoService.update(empregado);
					}
				}

				indice += intervalo;
				if (empregados == null || empregados.size() < intervalo) {
					aindaExistemRegistros = false;
				}

			} while (aindaExistemRegistros);
			System.out.println("Atualiza��o de nomeProcura dos empregados conclu�da");
		} catch (Exception e) {
			System.out.println("ERRO -  " + e.getMessage());
		}
	}
}
