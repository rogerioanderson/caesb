/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.quartz.job;

import java.util.Collection;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.centralit.citcorpore.bean.RequisicaoProdutoDTO;
import br.com.centralit.citcorpore.bpm.negocio.ExecucaoRequisicaoProduto;
import br.com.centralit.citcorpore.integracao.RequisicaoProdutoDao;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;

public class VerificaRequisicoesCompra implements Job {
	@SuppressWarnings("unchecked")
	public void execute(JobExecutionContext context) throws JobExecutionException {
		RequisicaoProdutoDao requisicaoProdutoDao = new RequisicaoProdutoDao();
        TransactionControler tc = null;
        try{
        	Collection<RequisicaoProdutoDTO> requisicoes = requisicaoProdutoDao.consultaRequisicoesEmAndamento();
			if (requisicoes != null) {
				ExecucaoRequisicaoProduto execucaoRequisicao = new ExecucaoRequisicaoProduto();
				for (RequisicaoProdutoDTO requisicaoProdutoDto : requisicoes) {
			        tc = new TransactionControlerImpl(requisicaoProdutoDao.getAliasDB());
					try {
						tc.start();
				        execucaoRequisicao.setObjetoNegocioDto(requisicaoProdutoDto);
				        execucaoRequisicao.setTransacao(tc);
				        execucaoRequisicao.verificaExpiracao();
						tc.commit();
			        } catch (Exception ex) {
			            ex.printStackTrace();
			            try {
			                if (tc.isStarted()) { // Se estiver startada, entao faz roolback.
			                    tc.rollback();
			                }
			            } catch (final Exception e) {
			            	e.printStackTrace();
			            }
			        }finally{
						try {
							tc.close();
							tc = null;
						} catch (PersistenceException e) {
						}
			        }
				}
			}
		}catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
