/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.quartz.job;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.centralit.citcorpore.bean.PesquisaSatisfacaoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.integracao.PesquisaSatisfacaoDAO;
import br.com.centralit.citcorpore.integracao.SolicitacaoServicoDao;
import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citcorpore.util.Util;
import br.com.citframework.util.UtilDatas;

public class AvaliarSolicitacoesNaoRespondidas implements Job  {
	
	 private static final Logger LOGGER = Logger.getLogger(AvaliarSolicitacoesNaoRespondidas.class.getName());

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		String avaliacaoAutomatica = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.AVALIA��O_AUTOMATICA, "");
		
		if(avaliacaoAutomatica.equals("S")){
			try {
				LOGGER.log(Level.INFO, "Disparando Avalia��o autom�tica...");
				
				PesquisaSatisfacaoDTO pesquisaSatisfacaoDTO;
				PesquisaSatisfacaoDAO pesquisaSatisfacaoDAO = new PesquisaSatisfacaoDAO();
				SolicitacaoServicoDao solicitacaoServicoDao = new SolicitacaoServicoDao();
	
				// Nota padr�o para avalia��o autom�tica do atendimento de solicita��es
				Enumerados.Nota nota = null;
				String notaAvaliacaoAutomatica = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.NOTA_AVALIA��O_AUTOMATICA, "");
				if(notaAvaliacaoAutomatica.equals("BOM")){
					nota = Enumerados.Nota.BOM;
				}else if(notaAvaliacaoAutomatica.equals("OTIMO")){
					nota = Enumerados.Nota.OTIMO;
				}else if(notaAvaliacaoAutomatica.equals("REGULAR")){
					nota = Enumerados.Nota.REGULAR;
				}else if(notaAvaliacaoAutomatica.equals("RUIM")){
					nota = Enumerados.Nota.RUIM;
				}
				
				// Calculando a data limite para resposta do usu�rio
				Integer pQtdeDias = Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(br.com.centralit.citcorpore.util.Enumerados.ParametroSistema.QTDE_DIAS_RESP_PESQ_SASTISFACAO, "7"));
	
				java.sql.Date dataLimite = UtilDatas.getSqlDate(UtilDatas.incrementaDiasEmData(Util.getDataAtual(), -(new Integer(pQtdeDias))));
				
				String comentario = "Pesquisa respondida automaticamente pelo sistema citsmart.";
	
				// As Solicita��es n�o respondidas e que foram conclu�das at� a data limite ser�o respondidas automaticamente pelo sistema
				// Trazer as Solicita��es n�o respondidas
				Collection<SolicitacaoServicoDTO> listaIdSolicitacoesNResp = solicitacaoServicoDao.listaIDSolicitacaoNaoRespondida(dataLimite);
				for (SolicitacaoServicoDTO solicitacaoServicoDTO : listaIdSolicitacoesNResp) {
					// Gravar pesquisa para Cada Solicita��o de Servi�os n�o avaliada
					pesquisaSatisfacaoDTO = new PesquisaSatisfacaoDTO();
					pesquisaSatisfacaoDTO.setIdSolicitacaoServico(solicitacaoServicoDTO.getIdSolicitacaoServico());
					pesquisaSatisfacaoDTO.setNota(nota.getNota());
					pesquisaSatisfacaoDTO.setComentario(comentario);
					pesquisaSatisfacaoDAO.create(pesquisaSatisfacaoDTO);
				}
				
				LOGGER.log(Level.INFO, "Finalizando Avalia��o autom�tica...");
				
			} catch (Exception e) {
				LOGGER.log(Level.WARNING, "Erro na Avalia��o autom�tica de solicita��es: " + e.getMessage(), e);
			}
		}
	}
}
