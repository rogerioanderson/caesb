/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.quartz.job;

import java.util.Collection;

import net.htmlparser.jericho.Source;

import org.apache.commons.lang.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.negocio.SolicitacaoServicoService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;

public class EventoPopulaDescricaoSolicitacao implements Job {

    @Override
    public void execute(final JobExecutionContext arg0) throws JobExecutionException {
        try {
            final Collection<SolicitacaoServicoDTO> lista = this.getService().list();
            for (final SolicitacaoServicoDTO solicitacaoServicoDTO : lista) {

                if (solicitacaoServicoDTO.getDescricao() != null && !StringUtils.isBlank(solicitacaoServicoDTO.getDescricao())) {
                    final Source source = new Source(solicitacaoServicoDTO.getDescricao());
                    solicitacaoServicoDTO.setDescricaoSemFormatacao(source.getTextExtractor().toString());
                }
                if (solicitacaoServicoDTO.getDescricaoSemFormatacao() != null && !StringUtils.isBlank(solicitacaoServicoDTO.getDescricaoSemFormatacao())) {
                    this.getService().updateNotNull(solicitacaoServicoDTO);
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    private SolicitacaoServicoService service;

    private SolicitacaoServicoService getService() throws ServiceException {
        if (service == null) {
            service = (SolicitacaoServicoService) ServiceLocator.getInstance().getService(SolicitacaoServicoService.class, null);
        }
        return service;
    }

}
