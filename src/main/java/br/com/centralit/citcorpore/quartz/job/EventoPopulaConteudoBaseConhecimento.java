/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.quartz.job;

import java.util.Collection;

import net.htmlparser.jericho.Source;

import org.apache.commons.lang.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;
import br.com.centralit.citcorpore.negocio.BaseConhecimentoService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;

public class EventoPopulaConteudoBaseConhecimento implements Job {

    @Override
    public void execute(final JobExecutionContext arg0) throws JobExecutionException {
        try {
            final Collection<BaseConhecimentoDTO> lista = this.getBaseConhecimentoService().list();
            if (lista != null) {
                for (final BaseConhecimentoDTO baseConhecimentoDto : lista) {
                    if (baseConhecimentoDto.getConteudo() != null && !StringUtils.isBlank(baseConhecimentoDto.getConteudo())) {
                        final Source source = new Source(baseConhecimentoDto.getConteudo());
                        baseConhecimentoDto.setConteudoSemFormatacao(source.getTextExtractor().toString());
                    }
                    if (baseConhecimentoDto.getConteudoSemFormatacao() != null && !StringUtils.isBlank(baseConhecimentoDto.getConteudoSemFormatacao())) {
                        this.getBaseConhecimentoService().updateNotNull(baseConhecimentoDto);
                    }
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    private BaseConhecimentoService baseConhecimentoService;

    private BaseConhecimentoService getBaseConhecimentoService() throws ServiceException {
        if (baseConhecimentoService == null) {
            baseConhecimentoService = (BaseConhecimentoService) ServiceLocator.getInstance().getService(BaseConhecimentoService.class, null);
        }
        return baseConhecimentoService;
    }

}
