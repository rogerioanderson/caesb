/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.quartz.job;

import java.util.Collection;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.ItemConfiguracaoDTO;
import br.com.centralit.citcorpore.mail.MensagemEmail;
import br.com.centralit.citcorpore.negocio.EmpregadoService;
import br.com.centralit.citcorpore.negocio.ItemConfiguracaoService;
import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.citframework.dto.IDto;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;

public class VerificaValidadeLicenca implements Job {

    @Override
    public void execute(final JobExecutionContext arg0) throws JobExecutionException {
        Collection<ItemConfiguracaoDTO> colItemConfiguracao = null;
        EmpregadoDTO empregadoDTO = new EmpregadoDTO();
        try {
            String AVISAR_DATAEXPIRACAO_LICENCA = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.AVISAR_DATAEXPIRACAO_LICENCA, "90");
            String ENVIAR_EMAIL_DATAEXPIRACAO = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.ENVIAR_EMAIL_DATAEXPIRACAO, "2");
            String ID_MODELO_EMAIL_EXPIRACAO_LICENCA = ParametroUtil.getValorParametroCitSmartHashMap(Enumerados.ParametroSistema.ID_MODELO_EMAIL_EXPIRACAO_LICENCA, "6");
			final String remetente = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SMTP_ENVIO_RemetenteNotificacoesSolicitacao, null);

            if (AVISAR_DATAEXPIRACAO_LICENCA == null || AVISAR_DATAEXPIRACAO_LICENCA.isEmpty()) {
                AVISAR_DATAEXPIRACAO_LICENCA = "90";
            }

            if (ID_MODELO_EMAIL_EXPIRACAO_LICENCA == null || ID_MODELO_EMAIL_EXPIRACAO_LICENCA.isEmpty()) {
                ID_MODELO_EMAIL_EXPIRACAO_LICENCA = "6";
            }

            Date dataAtual = UtilDatas.getDataAtual();

            dataAtual = UtilDatas.incrementaDiasEmData(dataAtual, Integer.parseInt(AVISAR_DATAEXPIRACAO_LICENCA));

            final ItemConfiguracaoService itemConfiguracaoService = (ItemConfiguracaoService) ServiceLocator.getInstance().getService(ItemConfiguracaoService.class, null);
            colItemConfiguracao = itemConfiguracaoService.pesquisaDataExpiracao(dataAtual);

            if (!colItemConfiguracao.isEmpty()) {
                if (ENVIAR_EMAIL_DATAEXPIRACAO == null || ENVIAR_EMAIL_DATAEXPIRACAO.isEmpty()) {
                    ENVIAR_EMAIL_DATAEXPIRACAO = "2";
                }
                if (ENVIAR_EMAIL_DATAEXPIRACAO.trim().equals("2")) {
                    final EmpregadoService empregadoService = (EmpregadoService) ServiceLocator.getInstance().getService(EmpregadoService.class, null);
                    for (final ItemConfiguracaoDTO itemConfiguracao : colItemConfiguracao) {
                        empregadoDTO = empregadoService.restoreByIdEmpregado(itemConfiguracao.getIdProprietario());
                        final MensagemEmail mensagem = new MensagemEmail(Integer.parseInt(ID_MODELO_EMAIL_EXPIRACAO_LICENCA.trim()), new IDto[] {itemConfiguracao});
                        mensagem.envia(empregadoDTO.getEmail(), "", remetente);
                    }
                } else {
                    for (final ItemConfiguracaoDTO itemConfiguracao : colItemConfiguracao) {
                        final MensagemEmail mensagem = new MensagemEmail(Integer.parseInt(ID_MODELO_EMAIL_EXPIRACAO_LICENCA.trim()), new IDto[] {itemConfiguracao});
                        mensagem.envia(itemConfiguracao.getEmailGrupoItemConfiguracao(), "", remetente);
                    }
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

}
