/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.quartz.job;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.centralit.citcorpore.bean.EventoGrupoDTO;
import br.com.centralit.citcorpore.bean.EventoItemConfigRelDTO;
import br.com.centralit.citcorpore.bean.GrupoItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.ItemConfigEventoDTO;
import br.com.centralit.citcorpore.bean.ItemConfiguracaoDTO;
import br.com.centralit.citcorpore.componenteMaquina.ThreadDisparaEvento;
import br.com.centralit.citcorpore.integracao.ItemConfiguracaoDao;
import br.com.centralit.citcorpore.negocio.EventoGrupoService;
import br.com.centralit.citcorpore.negocio.EventoItemConfigRelService;
import br.com.centralit.citcorpore.negocio.ItemConfigEventoService;
import br.com.citframework.service.ServiceLocator;

public class DisparaEvento implements Job {

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
        try {
            final EventoGrupoService eventoGrupoService = (EventoGrupoService) ServiceLocator.getInstance().getService(EventoGrupoService.class, null);
            final EventoItemConfigRelService eventoItemConfigRelService = (EventoItemConfigRelService) ServiceLocator.getInstance().getService(EventoItemConfigRelService.class,
                    null);
            final ItemConfigEventoService itemConfigEventoService = (ItemConfigEventoService) ServiceLocator.getInstance().getService(ItemConfigEventoService.class, null);
            final Collection<ItemConfigEventoDTO> colEventos = itemConfigEventoService.verificarDataHoraEvento();
            for (final ItemConfigEventoDTO itemCfgEventoDto : colEventos) {

                final Collection<EventoGrupoDTO> lstGrupo = eventoGrupoService.listByEvento(itemCfgEventoDto.getIdEvento());
                itemCfgEventoDto.getIdEvento();
                List<EventoItemConfigRelDTO> listItemConfiguracao = (List<EventoItemConfigRelDTO>) eventoItemConfigRelService.listByEvento(itemCfgEventoDto.getIdEvento());

                if (listItemConfiguracao == null) {
                    listItemConfiguracao = new ArrayList<EventoItemConfigRelDTO>();
                }

                // Busca Itens de Configura��o relacionados ao grupo
                final GrupoItemConfiguracaoDTO grupoItemConfiguracaoDTO = new GrupoItemConfiguracaoDTO();
                for (final EventoGrupoDTO eventoGrupoDTO : lstGrupo) {
                    final Integer idGrupo = eventoGrupoDTO.getIdGrupo();
                    final ItemConfiguracaoDao itemConfiguracaoDao = new ItemConfiguracaoDao();
                    grupoItemConfiguracaoDTO.setIdGrupoItemConfiguracao(idGrupo);
                    final Collection<ItemConfiguracaoDTO> lstItemConfigGrupo = itemConfiguracaoDao.listByGrupo(grupoItemConfiguracaoDTO, null, null);
                    for (final ItemConfiguracaoDTO itemConfiguracaoDTO : lstItemConfigGrupo) {
                        final EventoItemConfigRelDTO configRelDTO = new EventoItemConfigRelDTO();
                        configRelDTO.setIdItemConfiguracao(itemConfiguracaoDTO.getIdItemConfiguracao());
                        // Verifica se o Item de Configura��o consta na lista
                        if (!listItemConfiguracao.contains(configRelDTO)) {
                            listItemConfiguracao.add(configRelDTO);
                        }
                    }
                }

                for (final EventoItemConfigRelDTO eventoItemConfigRel : listItemConfiguracao) {
                    new Thread(new ThreadDisparaEvento(eventoItemConfigRel.getIdItemConfiguracao(), itemCfgEventoDto.getIdBaseItemConfiguracao(), itemCfgEventoDto.getIdEvento(),
                            itemCfgEventoDto.getTipoExecucao(), itemCfgEventoDto.getLinhaComando(), itemCfgEventoDto.getLinhaComandoLinux())).start();
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

}
