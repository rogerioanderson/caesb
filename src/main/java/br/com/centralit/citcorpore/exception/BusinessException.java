/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Created on 20/03/2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package br.com.centralit.citcorpore.exception;

/**
 * @author Tellus SA
 *
 *         To change the template for this generated type comment go to
 *         Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BusinessException extends GeneralException {

    private static final long serialVersionUID = -1595760355392522066L;

    public BusinessException() {
        super();
    }

    public BusinessException(final Throwable cause) {
        super(cause);
    }

    /**
     * @param code
     */
    public BusinessException(final String code) {
        super(code);
    }

    /**
     * @param code
     * @param arg1
     */
    public BusinessException(final String code, final String arg1) {
        super(code, arg1);
    }

    /**
     * @param code
     * @param arg1
     * @param arg2
     */
    public BusinessException(final String code, final String arg1, final String arg2) {
        super(code, arg1, arg2);
    }

    /**
     * @param code
     * @param arg1
     * @param arg2
     * @param arg3
     */
    public BusinessException(final String code, final String arg1, final String arg2, final String arg3) {
        super(code, arg1, arg2, arg3);
    }

    /**
     * @param code
     * @param cause
     */
    public BusinessException(final String code, final Throwable cause) {
        super(code, cause);
    }

    /**
     * @param code
     * @param arg1
     * @param cause
     */
    public BusinessException(final String code, final String arg1, final Throwable cause) {
        super(code, arg1, cause);
    }

    /**
     * @param code
     * @param arg1
     * @param arg2
     * @param cause
     */
    public BusinessException(final String code, final String arg1, final String arg2, final Throwable cause) {
        super(code, arg1, arg2, cause);
    }

    /**
     * @param code
     * @param arg1
     * @param arg2
     * @param arg3
     * @param cause
     */
    public BusinessException(final String code, final String arg1, final String arg2, final String arg3, final Throwable cause) {
        super(code, arg1, arg2, arg3, cause);
    }

}
