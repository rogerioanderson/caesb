/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Created on 17/06/2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package br.com.centralit.citcorpore.exception;

import java.util.ResourceBundle;

/**
 * @author Tellus SA
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TradutorExcecao {
	private ResourceBundle tradutor;
	private static TradutorExcecao instance = null; 
	private static String 
	   TRADUTOR_FILE    = 
				  "Excecoes";

	/**
	 * Construtor
	 */
    public TradutorExcecao(String arquivo){
		try {
		  tradutor = ResourceBundle.getBundle(arquivo);
		} catch(Exception exc) {
		  exc.printStackTrace();
		}
    }

	/**
	 * Classe singleton
	 * @return CfgXml
	 */
	public static TradutorExcecao getInstance(){
		if (instance == null){
			instance = new TradutorExcecao(TRADUTOR_FILE);
		}
		return instance;
	}
	
	public String getMensagem(String code){
		String result = "";
		try{
			result = tradutor.getString(code);	
		}catch (Exception e) {
			result = "Erro inesperado.";
		}
		return result;
	}
}
