/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.versao;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import br.com.centralit.citcorpore.util.CITCorporeUtil;

public class Versao {
//    public static String VERSAO_MAIOR = "2";
//    public static String VERSAO_MENOR = "0";
//    public static String VERSAO_REVISAO_CORRECOES = "7";
    
    public static String VERSAO_DATA_GERACAO = "";
    
    public static String getVersao(){
    	
    	return lerXmlDeVersoes();
		
    	//return Versao.VERSAO_MAIOR + "." + Versao.VERSAO_MENOR + "." + Versao.VERSAO_REVISAO_CORRECOES;
    }
    
    public static String getDataAndVersao(){
    	return Versao.VERSAO_DATA_GERACAO + " " + lerXmlDeVersoes();
    }
    
    private static String lerXmlDeVersoes(){
    	// LEITURA PROGRESSIVA DO XML
    	
    	String versaoStr = "2.0.7";
    	
		String separator = System.getProperty("file.separator");
		String diretorio = CITCorporeUtil.CAMINHO_REAL_APP + "XMLs" + separator;
		File file = new File(diretorio + "historicoDeVersoes.xml");
		SAXBuilder sb = new SAXBuilder();
		Document doc = new Document();
		
		try {
			doc = sb.build(file);
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Element historicoDeVersoes = doc.getRootElement();
		
		List<Element> versoes = historicoDeVersoes.getChildren();
		
		if(versoes != null && versoes.size() > 0){
			return versaoStr = versoes.get(versoes.size() -1).getText();
		}else{
			return versaoStr;
		}
    }
}
