/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.tld;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

@SuppressWarnings("serial")
public class UploadControlList extends BodyTagSupport {

	/**
	 * 
	 */

	private String id;
	private String style;
	private String title;
	private String form;
	private String action;
	private String disabled;
	
	public int doStartTag() throws JspException {
		try {
			//String urlIframe = "../../include/vazio.jsp";
			String urlIframe;
			try {
				urlIframe = br.com.citframework.util.Constantes.getValue("SERVER_ADDRESS") + 
						((HttpServletRequest) pageContext.getRequest()).getContextPath() +
						"/pages/refresh" + getId() + "List/refresh" + getId() + "List.load";
			} catch (Exception e1) {
				throw new JspException(e1);
			}
			
			JspWriter out = pageContext.getOut();
			out.println("<div style='border:1px solid black;' id='divUpload_" + getId() + "'>\n");			
			out.println("<div style='display:none;background:#E3F0FD;' id='divMostraResultadoUpload_" + getId() + "'></div>\n");
			out.println("<iframe name='fraUpload_" + getId() + "' id='fraUpload_" + getId() + "' style='" + getStyle() + ";width:100%; border: none;' src='" + urlIframe + "'></iframe>\n");
			out.println("</div>\n");
			
			
		} catch (IOException e) {
			throw new JspException(e);
		}
		
		return SKIP_BODY;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDisabled() {
		return disabled;
	}

	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}
}
