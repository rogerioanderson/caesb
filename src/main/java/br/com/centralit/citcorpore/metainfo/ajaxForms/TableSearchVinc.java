/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.metainfo.ajaxForms;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.centralit.citajax.framework.ParserRequest;
import br.com.centralit.citajax.html.AjaxFormAction;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.metainfo.bean.TableSearchDTO;
import br.com.centralit.citcorpore.metainfo.negocio.TableSearchService;
import br.com.citframework.service.ServiceLocator;

public class TableSearchVinc extends AjaxFormAction {

    private static boolean DEBUG = true;

    @Override
    public Class<TableSearchDTO> getBeanClass() {
        return TableSearchDTO.class;
    }

    @Override
    public void load(final DocumentHTML document, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        final ParserRequest parser = new ParserRequest();
        final Map<String, Object> hashValores = parser.getFormFields(request);
        if (DEBUG) {
            this.debugValuesFromRequest(hashValores);
        }

        final TableSearchService tableSearchService = (TableSearchService) ServiceLocator.getInstance().getService(TableSearchService.class, null);

        final TableSearchDTO tableSearchDTO = (TableSearchDTO) document.getBean();
        String retorno = "";
        if (tableSearchDTO.getLoad() == null || !tableSearchDTO.getLoad().equalsIgnoreCase("false")) {
            if (tableSearchDTO.getJsonData() != null) {
                if (tableSearchDTO.getMatriz() == null || !tableSearchDTO.getMatriz().equalsIgnoreCase("true")) {
                    retorno = tableSearchService.findItens(tableSearchDTO, true, hashValores, request);
                } else {
                    retorno = tableSearchService.getInfoMatriz(tableSearchDTO, true, hashValores, request);
                    retorno = "[" + retorno + "]";
                }
            } else {
                if (tableSearchDTO.getMatriz() != null && tableSearchDTO.getMatriz().equalsIgnoreCase("true")) {
                    retorno = tableSearchService.getInfoMatriz(tableSearchDTO, true, hashValores, request);
                    retorno = "[" + retorno + "]";
                }
            }
        }

        if (retorno.trim().equalsIgnoreCase("")) {
            retorno = "{\"total\": \"0\",\"rows\":[]}";
        }

        request.setAttribute("json_response", "" + retorno + "");
    }

}
