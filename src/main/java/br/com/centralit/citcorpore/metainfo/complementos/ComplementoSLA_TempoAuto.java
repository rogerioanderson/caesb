/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.metainfo.complementos;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;

import br.com.centralit.citcorpore.bean.AcordoNivelServicoDTO;
import br.com.centralit.citcorpore.integracao.AcordoNivelServicoDao;
import br.com.centralit.citcorpore.negocio.PrioridadeService;
import br.com.centralit.citcorpore.negocio.UnidadeService;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilFormatacao;

public class ComplementoSLA_TempoAuto {
	public String execute(JspWriter out, HttpServletRequest request, HttpServletResponse response) throws ServiceException, Exception {
		UnidadeService unidadeService = (UnidadeService) ServiceLocator.getInstance().getService(UnidadeService.class, null);
		Collection colUnidades = unidadeService.list();
		String retorno = "";
		
		String idServicoContratoStr = (String)request.getParameter("saveInfo");
		Integer idServicoContrato = null;
		if (idServicoContratoStr != null && !idServicoContratoStr.trim().equalsIgnoreCase("")){
		    idServicoContrato = new Integer(idServicoContratoStr);
		}
		String idAcordoNivelServicoSTR = (String)request.getParameter("id");
		Integer idAcordoNivelServico = null;
		if (idAcordoNivelServicoSTR != null && !idAcordoNivelServicoSTR.trim().equalsIgnoreCase("")){
		    idAcordoNivelServico = new Integer(idAcordoNivelServicoSTR);
		}	
		AcordoNivelServicoDao acordoNivelServicoDao = new AcordoNivelServicoDao();
		AcordoNivelServicoDTO acordoNivelServicoDTO = new AcordoNivelServicoDTO();
		acordoNivelServicoDTO.setIdAcordoNivelServico(idAcordoNivelServico);
		if (idAcordoNivelServico != null && idAcordoNivelServico.intValue() > 0){
			acordoNivelServicoDTO = (AcordoNivelServicoDTO) acordoNivelServicoDao.restore(acordoNivelServicoDTO);
		}
		PrioridadeService prioridadeService = (PrioridadeService) ServiceLocator.getInstance().getService(PrioridadeService.class, null);
		Collection col = prioridadeService.list();
		if (col != null){
			String tempo = "";
			if (acordoNivelServicoDTO != null && acordoNivelServicoDTO.getTempoAuto() != null){
				tempo = UtilFormatacao.formatDouble(acordoNivelServicoDTO.getTempoAuto(), 2);
			}
			retorno = retorno + "<input type='text' name='TEMPOAUTO' id='TEMPOAUTO' size='12' maxlength='8' class='Format[Money]' value='" + tempo + "'/>min";
		}
		
		return retorno;
	}
}
