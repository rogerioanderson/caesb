/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.metainfo.bean;

import br.com.citframework.dto.IDto;

public class GrupoVisaoCamposNegocioLigacaoDTO implements IDto {
	public static String PRESENTATION = "A";
	public static String VALUE = "V";
	public static String FILTER = "F";
	public static String ORDER = "O";
	
	private Integer idGrupoVisaoCamposNegocioLigacao;
	private Integer idGrupoVisao;
	private Integer idCamposObjetoNegocio;
	private Integer idCamposObjetoNegocioLigacao;
	private String tipoLigacao;
	private String filtro;
	private String descricao;

	public Integer getIdGrupoVisaoCamposNegocioLigacao(){
		return this.idGrupoVisaoCamposNegocioLigacao;
	}
	public void setIdGrupoVisaoCamposNegocioLigacao(Integer parm){
		this.idGrupoVisaoCamposNegocioLigacao = parm;
	}

	public Integer getIdGrupoVisao(){
		return this.idGrupoVisao;
	}
	public void setIdGrupoVisao(Integer parm){
		this.idGrupoVisao = parm;
	}

	public Integer getIdCamposObjetoNegocio(){
		return this.idCamposObjetoNegocio;
	}
	public void setIdCamposObjetoNegocio(Integer parm){
		this.idCamposObjetoNegocio = parm;
	}

	public Integer getIdCamposObjetoNegocioLigacao(){
		return this.idCamposObjetoNegocioLigacao;
	}
	public void setIdCamposObjetoNegocioLigacao(Integer parm){
		this.idCamposObjetoNegocioLigacao = parm;
	}

	public String getTipoLigacao(){
		return this.tipoLigacao;
	}
	public void setTipoLigacao(String parm){
		this.tipoLigacao = parm;
	}

	public String getFiltro(){
		return this.filtro;
	}
	public void setFiltro(String parm){
		this.filtro = parm;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
