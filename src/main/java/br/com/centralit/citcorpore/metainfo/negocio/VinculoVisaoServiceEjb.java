/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.metainfo.negocio;

import java.util.Collection;

import br.com.centralit.citcorpore.metainfo.integracao.VinculoVisaoDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class VinculoVisaoServiceEjb extends CrudServiceImpl implements VinculoVisaoService {

    private VinculoVisaoDao dao;

    @Override
    protected VinculoVisaoDao getDao() {
        if (dao == null) {
            dao = new VinculoVisaoDao();
        }
        return dao;
    }

    @Override
    public Collection findByIdVisaoRelacionada(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdVisaoRelacionada(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdGrupoVisaoPai(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdGrupoVisaoPai(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdGrupoVisaoPai(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdGrupoVisaoPai(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdCamposObjetoNegocioPai(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdCamposObjetoNegocioPai(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdCamposObjetoNegocioPai(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdCamposObjetoNegocioPai(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdGrupoVisaoFilho(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdGrupoVisaoFilho(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdGrupoVisaoFilho(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdGrupoVisaoFilho(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdCamposObjetoNegocioFilho(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdCamposObjetoNegocioFilho(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdCamposObjetoNegocioFilho(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdCamposObjetoNegocioFilho(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdCamposObjetoNegocioPaiNN(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdCamposObjetoNegocioPaiNN(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdCamposObjetoNegocioPaiNN(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdCamposObjetoNegocioPaiNN(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Collection findByIdCamposObjetoNegocioFilhoNN(final Integer parm) throws Exception {
        try {
            return this.getDao().findByIdCamposObjetoNegocioFilhoNN(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdCamposObjetoNegocioFilhoNN(final Integer parm) throws Exception {
        try {
            this.getDao().deleteByIdCamposObjetoNegocioFilhoNN(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

}
