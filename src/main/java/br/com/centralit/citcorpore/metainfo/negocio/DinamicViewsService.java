/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.metainfo.negocio;

import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.metainfo.bean.DinamicViewsDTO;
import br.com.citframework.service.CrudService;

public interface DinamicViewsService extends CrudService {

    void save(final UsuarioDTO usuarioDto, final DinamicViewsDTO dinamicViewDto, final Map map, final HttpServletRequest request) throws Exception;

    void saveMatriz(final UsuarioDTO usuarioDto, final DinamicViewsDTO dinamicViewDto, final HttpServletRequest request) throws Exception;

    Collection restoreVisao(final Integer idVisao, final Collection colFilter) throws Exception;

    String internacionalizaScript(final String script, final Locale locale) throws Exception;

    String internacionalizaScript(final String script, final String locale) throws Exception;

    void setInfoSave(final Integer idVisao, final Collection colCamposPK, final Collection colCamposTodos) throws Exception;

    boolean isPKExists(final Collection colCamposPK, final Map hashValores) throws Exception;

}
