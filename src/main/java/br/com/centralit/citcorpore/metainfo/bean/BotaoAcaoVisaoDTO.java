/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.metainfo.bean;

import br.com.citframework.dto.IDto;

public class BotaoAcaoVisaoDTO implements IDto {
	public static String ACAO_GRAVAR = "1";
	public static String ACAO_LIMPAR = "2";
	public static String ACAO_EXCLUIR = "3";
	public static String ACAO_SCRIPT = "4";
	
	private Integer idBotaoAcaoVisao;
	private Integer idVisao;
	private String texto;
	private String acao;
	private String script;
	private String hint;
	private String icone;
	private Integer ordem;

	public Integer getIdBotaoAcaoVisao(){
		return this.idBotaoAcaoVisao;
	}
	public void setIdBotaoAcaoVisao(Integer parm){
		this.idBotaoAcaoVisao = parm;
	}

	public Integer getIdVisao(){
		return this.idVisao;
	}
	public void setIdVisao(Integer parm){
		this.idVisao = parm;
	}

	public String getTexto(){
		return this.texto;
	}
	public void setTexto(String parm){
		this.texto = parm;
	}

	public String getAcao(){
		return this.acao;
	}
	public void setAcao(String parm){
		this.acao = parm;
	}

	public String getScript(){
		return this.script;
	}
	public void setScript(String parm){
		this.script = parm;
	}

	public String getHint(){
		return this.hint;
	}
	public void setHint(String parm){
		this.hint = parm;
	}

	public String getIcone(){
		return this.icone;
	}
	public void setIcone(String parm){
		this.icone = parm;
	}

	public Integer getOrdem(){
		return this.ordem;
	}
	public void setOrdem(Integer parm){
		this.ordem = parm;
	}

}
