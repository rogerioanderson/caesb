/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.metainfo.bean;

import java.util.ArrayList;
import java.util.Collection;

import br.com.citframework.dto.IDto;

public class HtmlCodeVisaoDTO implements IDto {
	public static HtmlCodePartDTO HTMLCODE_INIT = new HtmlCodePartDTO("INIT", "visaoAdm.inicioAreaCentral");
	public static HtmlCodePartDTO HTMLCODE_END  = new HtmlCodePartDTO("END", "visaoAdm.finalAreaCentral");
	public static HtmlCodePartDTO HTMLCODE_INIT_FORM = new HtmlCodePartDTO("INIT_FORM", "visaoAdm.inicioFormulario");
	public static HtmlCodePartDTO HTMLCODE_END_FORM  = new HtmlCodePartDTO("END_FORM", "visaoAdm.finalFormulario");
	public static HtmlCodePartDTO HTMLCODE_INIT_BUTTONS = new HtmlCodePartDTO("INIT_BUTTONS", "visaoAdm.inicioAreaBotao");
	public static HtmlCodePartDTO HTMLCODE_END_BUTTONS  = new HtmlCodePartDTO("END_BUTTONS", "visaoAdm.finalAreaBotao");	
	public static HtmlCodePartDTO HTMLCODE_SUPERIOR  = new HtmlCodePartDTO("SUPERIOR", "visaoAdm.areaSuperior");	
	
	private Integer idHtmlCodeVisao;
	private Integer idVisao;
	private String htmlCodeType;
	private String htmlCode;

	public static Collection<HtmlCodePartDTO> colHtmlCodeParts = new ArrayList<HtmlCodePartDTO>();
	
	static {
		colHtmlCodeParts.add(HTMLCODE_INIT);
		colHtmlCodeParts.add(HTMLCODE_END);
		colHtmlCodeParts.add(HTMLCODE_INIT_FORM);
		colHtmlCodeParts.add(HTMLCODE_END_FORM);
		colHtmlCodeParts.add(HTMLCODE_INIT_BUTTONS);
		colHtmlCodeParts.add(HTMLCODE_END_BUTTONS);
		colHtmlCodeParts.add(HTMLCODE_SUPERIOR);
	}
	
	public Integer getIdHtmlCodeVisao(){
		return this.idHtmlCodeVisao;
	}
	public void setIdHtmlCodeVisao(Integer parm){
		this.idHtmlCodeVisao = parm;
	}

	public Integer getIdVisao(){
		return this.idVisao;
	}
	public void setIdVisao(Integer parm){
		this.idVisao = parm;
	}

	public String getHtmlCodeType(){
		return this.htmlCodeType;
	}
	public void setHtmlCodeType(String parm){
		this.htmlCodeType = parm;
	}

	public String getHtmlCode(){
		return this.htmlCode;
	}
	public void setHtmlCode(String parm){
		this.htmlCode = parm;
	}
}
