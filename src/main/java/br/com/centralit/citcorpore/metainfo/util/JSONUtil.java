/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citcorpore.metainfo.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JSONUtil {

    public static Map<String, Object> convertJsonToMap(final String strData, final boolean convKeyToUpperCase) {
        if (strData == null) {
            return null;
        }
        final JsonParser parserJson = new JsonParser();
        final JsonElement element = parserJson.parse(strData);
        if (JsonObject.class.isInstance(element)) {
            final JsonObject object = (JsonObject) element;
            if (object != null) {
                final Set<Map.Entry<String, JsonElement>> set = object.entrySet();
                final Iterator<Map.Entry<String, JsonElement>> iterator = set.iterator();
                final Map<String, Object> map = new HashMap<>();
                while (iterator.hasNext()) {
                    final Map.Entry<String, JsonElement> entry = iterator.next();
                    String key = entry.getKey();
                    final JsonElement value = entry.getValue();
                    if (convKeyToUpperCase) {
                        if (key != null) {
                            key = key.toUpperCase();
                        }
                    }
                    if (!value.isJsonPrimitive()) {
                        if (value.isJsonArray()) {
                            map.put(key, JSONUtil.convertJsonArrayToCollection((JsonArray) value, convKeyToUpperCase));
                        } else {
                            map.put(key, JSONUtil.convertJsonToMap(value.toString(), convKeyToUpperCase));
                        }
                    } else {
                        map.put(key, value.getAsString());
                    }
                }
                return map;
            }
        }
        if (JsonArray.class.isInstance(element)) {
            final JsonArray array = (JsonArray) element;
            final Map<String, Object> map = new HashMap<>();
            map.put("ARRAY", JSONUtil.convertJsonArrayToCollection(array, convKeyToUpperCase));
            return map;
        }
        return null;
    }

    public static Collection<Map<String, Object>> convertJsonArrayToCollection(final JsonArray array, final boolean convUpperCase) {
        if (array != null) {
            final Iterator<JsonElement> iterator = array.iterator();
            final Collection<Map<String, Object>> col = new ArrayList<>();
            while (iterator.hasNext()) {
                final Object obj = iterator.next();
                col.add(JSONUtil.convertJsonToMap(obj.toString(), convUpperCase));
            }
            return col;
        }
        return null;
    }

}
