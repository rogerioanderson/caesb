/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.bean;

import java.sql.Timestamp;

import br.com.citframework.dto.IDto;

public class RestExecutionDTO implements IDto {

    private static final long serialVersionUID = 4360912318625452051L;

    private Integer idRestExecution;
    private Integer idRestOperation;
    private Timestamp inputDateTime;
    private Integer idUser;
    private String inputClass;
    private String inputData;
    private String status;
    
    private String platform;
    private String uuid;    

    private RestOperationDTO restOperationDto;
    private Object input;

    public Integer getIdRestExecution() {
        return idRestExecution;
    }

    public void setIdRestExecution(final Integer parm) {
        idRestExecution = parm;
    }

    public Integer getIdRestOperation() {
        return idRestOperation;
    }

    public void setIdRestOperation(final Integer parm) {
        idRestOperation = parm;
    }

    public Timestamp getInputDateTime() {
        return inputDateTime;
    }

    public void setInputDateTime(final Timestamp parm) {
        inputDateTime = parm;
    }

    public String getInputData() {
        return inputData;
    }

    public void setInputData(final String parm) {
        inputData = parm;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String parm) {
        status = parm;
    }

    public RestOperationDTO getRestOperationDto() {
        return restOperationDto;
    }

    public void setRestOperationDto(final RestOperationDTO restOperationDto) {
        this.restOperationDto = restOperationDto;
    }

    public String getInputClass() {
        return inputClass;
    }

    public void setInputClass(final String inputClass) {
        this.inputClass = inputClass;
    }

    public Object getInput() {
        return input;
    }

    public void setInput(final Object input) {
        this.input = input;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(final Integer idUser) {
        this.idUser = idUser;
    }

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
