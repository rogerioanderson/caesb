/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * 2013 - CentralIT - Citsmart - BI Citsmart
 * 
 */
package br.com.centralit.citsmart.rest.resource;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import br.com.centralit.citsmart.rest.schema.BICitsmart;
import br.com.centralit.citsmart.rest.util.RestBICitsmartOperationUtil;

/**
 * Classe Resource respons�vel pelas Requisi��es do BICitsmart.
 * 
 * @author valdoilo.damasceno
 * @since 06.12.2013
 */
@Path("/services")
public class RestBICitsmartResource {

	/**
	 * Trata Requisi��o de Recuperara��o da Tabela Contratos.
	 * 
	 * @param input
	 *            - BICitsmart
	 * @return Response
	 * @author valdoilo.damasceno
	 * @since 06.12.2013
	 */
	
	 @POST
	 @Path("/bicitsmart/recuperarTabelas")
	public Response recuperarTabelas(BICitsmart input) {
		 input.setMessageID("bicitsmart_recuperarTabelas");
		 return RestBICitsmartOperationUtil.execute(input);
	}
	
}
