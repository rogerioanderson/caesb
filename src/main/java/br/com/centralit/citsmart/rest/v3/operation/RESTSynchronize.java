/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.v3.operation;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.OcorrenciaSolicitacaoDTO;
import br.com.centralit.citcorpore.bean.PesquisaSolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.ServicoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.TabFederacaoDadosDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.util.Enumerados.CategoriaOcorrencia;
import br.com.centralit.citcorpore.util.Enumerados.SituacaoSolicitacaoServico;
import br.com.centralit.citsmart.rest.bean.RestOperationDTO;
import br.com.centralit.citsmart.rest.bean.RestSessionDTO;
import br.com.centralit.citsmart.rest.util.RestEnum;
import br.com.centralit.citsmart.rest.util.RestOperationUtil;
import br.com.centralit.citsmart.rest.util.RestUtil;
import br.com.centralit.citsmart.rest.v3.schema.CtOccurrenceGetNotSynchronized;
import br.com.centralit.citsmart.rest.v3.schema.CtOccurrenceGetNotSynchronizedResp;
import br.com.centralit.citsmart.rest.v3.schema.CtRequestGetList;
import br.com.centralit.citsmart.rest.v3.schema.CtRequestGetNotSynchronized;
import br.com.centralit.citsmart.rest.v3.schema.CtRequestGetNotSynchronizedResp;
import br.com.centralit.citsmart.rest.v3.schema.CtSynchronize;
import br.com.centralit.citsmart.rest.v3.schema.CtSynchronizeGetList;
import br.com.centralit.citsmart.rest.v3.schema.CtSynchronizeGetListResp;
import br.com.centralit.citsmart.rest.v3.schema.CtSynchronizeResp;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.integracao.TransactionControlerImpl;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilStrings;

/**
 * Implementa��o das opera��os que respondem em {@code /service} da vers�o V3 REST
 *
 * @author carlos.alberto - <a href="mailto:carlos.alberto@centrait.com.br">carlos.alberto@centrait.com.br</a>
 * @since 07/09/2015
 *
 */
public class RESTSynchronize extends RESTOperation {

    private static final Logger LOGGER = Logger.getLogger(RESTSynchronize.class.getName());
    
    protected CtRequestGetNotSynchronizedResp getNotSynchronizedRequests(final RestSessionDTO restSession, final CtRequestGetNotSynchronized message, final RestOperationDTO restOperation) {
        final CtRequestGetNotSynchronizedResp resp = new CtRequestGetNotSynchronizedResp();
        
	    if (message == null) {
            resp.setError(RestOperationUtil.buildError(RestEnum.INPUT_ERROR, "Par�metros da consulta n�o informados"));
            return resp;
        }
        
        ServicoDTO servico = null;
        try {
        	servico = this.findService(restSession, message.getService(), null, null, false, false);
        } catch (final Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return resp;
        }
        
        EmpregadoDTO empregado = null;
        UsuarioDTO usuario = null;
	    if (!UtilStrings.isNullOrEmpty(message.getUserID())) {
	        try {
	    		usuario = this.getUsuarioService().restoreByLogin(message.getUserID());
	    		if (usuario != null) {
	    			empregado = this.getEmpregadoService().restoreByIdEmpregado(usuario.getIdEmpregado());
	    		}
			} catch (Exception e) {
	            LOGGER.log(Level.SEVERE, e.getMessage(), e);
	            resp.setError(RestOperationUtil.buildSimpleError(e));
	            return resp;
			}
        }
	    
       	List<SolicitacaoServicoDTO> solicitacoes = new ArrayList<SolicitacaoServicoDTO>();

        List<Integer> groupList = this.buildGroupList(message);
       	List<Integer> contractList = this.buildContractList(message);
        List<SituacaoSolicitacaoServico> statusList = this.buildStatusList(message);
        
        for (Integer contractID : contractList) {
        	
            ContratoDTO contrato = null;
            try {
    			contrato = this.findContract(restSession, contractID.toString(), null, false, false);
    		} catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
                resp.setError(RestOperationUtil.buildSimpleError(e));
                return resp;
    		}
            if (contrato == null) {
                resp.setError(RestOperationUtil.buildError(RestEnum.PARAM_ERROR, RestUtil.i18nMessage(restSession, "rest.service.mobile.contract.default.not.parametrized")));
                return resp;
            }        

            HashMap<String, IDto> map = new HashMap<String, IDto>();
            for (Integer groupID : groupList) { 
            	
            for (SituacaoSolicitacaoServico status : statusList) {
        	    PesquisaSolicitacaoServicoDTO pesquisaSolicitacao = new PesquisaSolicitacaoServicoDTO();
                pesquisaSolicitacao.setIdSolicitante(empregado != null ? empregado.getIdEmpregado() : null);
               	pesquisaSolicitacao.setDataInicio(message.getStartDate() != null ? new Date(message.getStartDate().getTimeInMillis()) : null);
               	pesquisaSolicitacao.setDataFim(message.getEndDate() != null ? new Date(message.getEndDate().getTimeInMillis()) : null);
               	pesquisaSolicitacao.setIdContrato(contrato.getIdContrato());
               	pesquisaSolicitacao.setIdServico(servico != null ? servico.getIdServico() : null);
	               	pesquisaSolicitacao.setIdGrupoAtual(groupID);
               	pesquisaSolicitacao.setSituacao(status != null ? status.name() : null);
               	pesquisaSolicitacao.setPalavraChave(message.getDescription());
               	pesquisaSolicitacao.setOrigemSincronizacao(UtilStrings.isNullOrEmpty(restSession.getPlatform()) ? restSession.getUser().getLogin() : restSession.getPlatform());
               	pesquisaSolicitacao.setSomenteNaoSincronizadas(true);
               	
               	try {
               		List<SolicitacaoServicoDTO> list = this.getSolicitacaoServicoService().listPesquisaAvancadaRelatorio(pesquisaSolicitacao);
        			if (list != null) {
        				for (SolicitacaoServicoDTO solicitacaoServico : list) {
	        					if (map.get(solicitacaoServico.getIdSolicitacaoServico().toString()) == null) {
        					solicitacaoServico = this.getSolicitacaoServicoService().restoreAll(solicitacaoServico.getIdSolicitacaoServico());
        					solicitacoes.add(solicitacaoServico);
        					
        					resp.getRequests().add(this.buildOutputRequest(solicitacaoServico, true, null));
		        					
		        					map.put(solicitacaoServico.getIdSolicitacaoServico().toString(), solicitacaoServico);
        				}
	        				}
        				resp.setAmount(list.size());
        			}
        		} catch (Exception e) {
                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
                    resp.setError(RestOperationUtil.buildSimpleError(e));
                    return resp;
        		}
            }
        }
        }
        

       	if (message.getSynchronize() != null && message.getSynchronize()) {
			TransactionControler tc = new TransactionControlerImpl(Constantes.getValue("DATABASE_ALIAS"));
			try {
				tc.start();
	
				for (SolicitacaoServicoDTO solicitacaoServico : solicitacoes) {
					Integer toKey = solicitacaoServico.getIdSolicitacaoServico() * -1;
					this.buildCorrelation(restSession, SOLICITACAOSERVICO, solicitacaoServico.getIdSolicitacaoServico().toString(), toKey.toString(), tc);
				}

				tc.commit();
			} catch (Exception e) {
				this.rollbackTransaction(tc);
	
				LOGGER.log(Level.SEVERE, e.getMessage(), e);
	            resp.setError(RestOperationUtil.buildSimpleError(e));
	            return resp;
			} finally {
				try {
					tc.close();
				} catch (PersistenceException e) {
					e.printStackTrace();
				}
			}
       	}
       	
        return resp;
    }
    
    protected CtOccurrenceGetNotSynchronizedResp getNotSynchronizedOccurrences(final RestSessionDTO restSession, final CtOccurrenceGetNotSynchronized message, final RestOperationDTO restOperation) {
        final CtOccurrenceGetNotSynchronizedResp resp = new CtOccurrenceGetNotSynchronizedResp();
        
	    if (message == null) {
            resp.setError(RestOperationUtil.buildError(RestEnum.INPUT_ERROR, "Par�metros da consulta n�o informados"));
            return resp;
        }

	    CtRequestGetList requestGetList = message.getRequestGetList();
	    if (requestGetList == null) {
	    	requestGetList = new CtRequestGetList();
	    }
	    
        ServicoDTO servico = null;
        if (requestGetList.getService() != null) {
	        try {
	        	servico = this.findService(restSession, requestGetList.getService(), null, null, false, false);
	        } catch (final Exception e) {
	            LOGGER.log(Level.SEVERE, e.getMessage(), e);
	            return resp;
	        }
        }
        
        EmpregadoDTO empregado = null;
        UsuarioDTO usuario = null;
        if (requestGetList.getUserID() != null) {
	        try {
	    		usuario = this.getUsuarioService().restoreByLogin(requestGetList.getUserID());
	    		if (usuario != null) {
	    			empregado = this.getEmpregadoService().restoreByIdEmpregado(usuario.getIdEmpregado());
	    		}
			} catch (Exception e) {
	            LOGGER.log(Level.SEVERE, e.getMessage(), e);
	            resp.setError(RestOperationUtil.buildSimpleError(e));
	            return resp;
			}
        }
        
       	List<OcorrenciaSolicitacaoDTO> todasOcorrencias = new ArrayList<OcorrenciaSolicitacaoDTO>();

       	List<Integer> contractList = this.buildContractList(requestGetList);
        List<SituacaoSolicitacaoServico> statusList = this.buildStatusList(requestGetList);
        
        String origemSincronizacao = UtilStrings.isNullOrEmpty(restSession.getPlatform()) ? restSession.getUser().getLogin() : restSession.getPlatform();

        HashMap<String, IDto> map = new HashMap<String, IDto>();
        
        for (Integer contractID : contractList) {
            for (SituacaoSolicitacaoServico status : statusList) {

                PesquisaSolicitacaoServicoDTO pesquisaSolicitacao = new PesquisaSolicitacaoServicoDTO();
                pesquisaSolicitacao.setIdSolicitante(empregado != null ? empregado.getIdEmpregado() : null);
               	pesquisaSolicitacao.setDataInicio(requestGetList.getStartDate() != null ? new Date(requestGetList.getStartDate().getTimeInMillis()) : null);
               	pesquisaSolicitacao.setDataFim(requestGetList.getEndDate() != null ? new Date(requestGetList.getEndDate().getTimeInMillis()) : null);
               	pesquisaSolicitacao.setIdContrato(contractID);
               	pesquisaSolicitacao.setIdServico(servico != null ? servico.getIdServico() : null);
               	pesquisaSolicitacao.setSituacao(status != null ? status.name() : null);
               	pesquisaSolicitacao.setPalavraChave(requestGetList.getDescription());
               	pesquisaSolicitacao.setOrigemSincronizacao(UtilStrings.isNullOrEmpty(restSession.getPlatform()) ? restSession.getUser().getLogin() : restSession.getPlatform());
               	pesquisaSolicitacao.setSomenteSincronizadas(true);
               	
               	try {  
        			List<SolicitacaoServicoDTO> list = this.getSolicitacaoServicoService().listPesquisaAvancadaRelatorio(pesquisaSolicitacao);
        			if (list != null) {
        				for (SolicitacaoServicoDTO solicitacaoServico : list) {
        		        	Collection<OcorrenciaSolicitacaoDTO> ocorrencias = this.getOcorrenciaSolicitacaoService().findNaoSincronizadas(solicitacaoServico.getIdSolicitacaoServico(), origemSincronizacao, message.getCategoryList());
        		        	if (ocorrencias != null && !ocorrencias.isEmpty()) {
        		        		String requestNumberOrigin = solicitacaoServico.getCodigoExterno();
        		        		if (requestNumberOrigin == null) {
	    		        			TabFederacaoDadosDTO federacaoDados = this.findCorrelation(restSession, SOLICITACAOSERVICO, solicitacaoServico.getIdSolicitacaoServico().toString());
	    		        			requestNumberOrigin = federacaoDados != null ? federacaoDados.getChaveOriginal() : null;
        		        		}
        		        		
        		        		boolean hasTask = false;
        		        		for (OcorrenciaSolicitacaoDTO ocorrencia : ocorrencias) {
    	        					if (map.get(ocorrencia.getIdOcorrencia().toString()) == null) {
        		        			todasOcorrencias.add(ocorrencia);
        		        			TabFederacaoDadosDTO federacaoDados = this.findCorrelation(restSession, OCORRENCIASOLICITACAO, ocorrencia.getIdOcorrencia().toString());
        		        			String numberOrigin = federacaoDados != null ? federacaoDados.getChaveOriginal() : ocorrencia.getIdOcorrencia().toString();
        		        			resp.getOccurrences().add(this.buildOccurrence(ocorrencia, numberOrigin, requestNumberOrigin, true));
	        		        			
	        		        			map.put(ocorrencia.getIdOcorrencia().toString(), ocorrencia);
	        		        			
	        		        			hasTask = ocorrencia.getIdItemTrabalho() != null && ocorrencia.getCategoria() != null && ocorrencia.getCategoria().equals(CategoriaOcorrencia.Execucao.name());
	        		        			if (hasTask) {
	        		        				break;
        						}
        		        	}
        				}
        			}
        				}
        			}
        		} catch (Exception e) {
                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
                    resp.setError(RestOperationUtil.buildSimpleError(e));
                    return resp;
        		}
    		}
		}
       	
       	if (message.getSynchronize() != null && message.getSynchronize()) {
			TransactionControler tc = new TransactionControlerImpl(Constantes.getValue("DATABASE_ALIAS"));
			try {
				tc.start();
	
				for (OcorrenciaSolicitacaoDTO ocorrencia : todasOcorrencias) {
					Integer toKey = ocorrencia.getIdOcorrencia() * -1;
					this.buildCorrelation(restSession, OCORRENCIASOLICITACAO, ocorrencia.getIdOcorrencia().toString(), toKey.toString(), tc);
				}

				tc.commit();
			} catch (Exception e) {
				this.rollbackTransaction(tc);
	
				LOGGER.log(Level.SEVERE, e.getMessage(), e);
	            resp.setError(RestOperationUtil.buildSimpleError(e));
	            return resp;
			} finally {
				try {
					tc.close();
				} catch (PersistenceException e) {
					e.printStackTrace();
				}
			}
       	}

       	return resp;
    }
    
    protected CtSynchronizeResp synchronize(final RestSessionDTO restSession, final CtSynchronize message, final RestOperationDTO restOperation) {
        final CtSynchronizeResp resp = new CtSynchronizeResp();
        
	    if (message == null) {
            resp.setError(RestOperationUtil.buildError(RestEnum.INPUT_ERROR, "Par�metros da consulta n�o informados"));
            return resp;
        }
        if (UtilStrings.isNullOrEmpty(message.getObject())) {
            resp.setError(RestOperationUtil.buildError(RestEnum.INPUT_ERROR, "Tipo de objeto n�o informado"));
            return resp;
        }	    
        if (UtilStrings.isNullOrEmpty(message.getFromKey())) {
            resp.setError(RestOperationUtil.buildError(RestEnum.INPUT_ERROR, "Chave 'DE' n�o informada"));
            return resp;
        }
        if (UtilStrings.isNullOrEmpty(message.getToKey())) {
            resp.setError(RestOperationUtil.buildError(RestEnum.INPUT_ERROR, "Chave 'PARA' n�o informada"));
            return resp;
        }
                
        try{
        	this.buildCorrelation(restSession, message.getObject(), message.getFromKey(), message.getToKey(), null);
        } catch (final Exception e) {
        	e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            resp.setError(RestOperationUtil.buildSimpleError(message.getObject()+" "+message.getFromKey()+" -> ",e));
            return resp;
        }
 
        return resp;
    }    

    protected CtSynchronizeResp deleteSynchronize(final RestSessionDTO restSession, final CtSynchronize message, final RestOperationDTO restOperation) {
        final CtSynchronizeResp resp = new CtSynchronizeResp();
        
	    if (message == null) {
            resp.setError(RestOperationUtil.buildError(RestEnum.INPUT_ERROR, "Par�metros da consulta n�o informados"));
            return resp;
        }
        if (UtilStrings.isNullOrEmpty(message.getObject())) {
            resp.setError(RestOperationUtil.buildError(RestEnum.INPUT_ERROR, "Tipo de objeto n�o informado"));
            return resp;
        }	    
        if (UtilStrings.isNullOrEmpty(message.getFromKey())) {
            resp.setError(RestOperationUtil.buildError(RestEnum.INPUT_ERROR, "Chave 'DE' n�o informada"));
            return resp;
        }
                
        try{
        	TabFederacaoDadosDTO federacaoDados = this.findCorrelation(restSession, message.getObject(), message.getFromKey());
        	if (federacaoDados != null) {
        		this.getFederacaoDadosService().delete(federacaoDados);
        	}
        } catch (final Exception e) {
        	e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            resp.setError(RestOperationUtil.buildSimpleError(e));
            return resp;
        }
 
        return resp;
    }    

    @SuppressWarnings("unchecked")
	protected CtSynchronizeGetListResp find(final RestSessionDTO restSession, final CtSynchronizeGetList message, final RestOperationDTO restOperation) {
        final CtSynchronizeGetListResp resp = new CtSynchronizeGetListResp();
        
	    if (message == null) {
            resp.setError(RestOperationUtil.buildError(RestEnum.INPUT_ERROR, "Par�metros da consulta n�o informados"));
            return resp;
        }
                
    	String origem = UtilStrings.isNullOrEmpty(restSession.getPlatform()) ? restSession.getUser().getLogin() : restSession.getPlatform();
        try{
        	Collection<TabFederacaoDadosDTO> list = this.getFederacaoDadosService().find(origem
        																			  , message.getObject()
        																			  , message.getFromKey()
        																			  , message.getToKey());
        	if (list != null) {
        		for (TabFederacaoDadosDTO federacaoDados : list) {
        			CtSynchronize object = new CtSynchronize();
        			object.setObject(federacaoDados.getNomeTabela());
        			object.setFromKey(federacaoDados.getChaveOriginal());
        			object.setToKey(federacaoDados.getChaveFinal());
        			
        			resp.getObjects().add(object);
				}
        	}
        } catch (final Exception e) {
        	e.printStackTrace();
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            resp.setError(RestOperationUtil.buildSimpleError(e));
            return resp;
        }
 
        return resp;
    }    
}
