/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import br.com.centralit.citsmart.rest.bean.RestLogDTO;
import br.com.centralit.citsmart.rest.util.RestEnum.ExecutionStatus;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilStrings;

public class RestLogDao extends CrudDaoDefaultImpl {

    public RestLogDao() {
        super(Constantes.getValue("DATABASE_ALIAS"), null);
    }

    @Override
    public Collection<Field> getFields() {
        final Collection<Field> listFields = new ArrayList<>();
        listFields.add(new Field("idRestLog", "idRestLog", true, true, false, false));
        listFields.add(new Field("idRestExecution", "idRestExecution", false, false, false, false));
        listFields.add(new Field("dateTime", "dateTime", false, false, false, false));
        listFields.add(new Field("status", "status", false, false, false, false));
        listFields.add(new Field("resultData", "resultData", false, false, false, false));
        listFields.add(new Field("resultClass", "resultClass", false, false, false, false));
        return listFields;
    }

    @Override
    public String getTableName() {
        return this.getOwner() + "Rest_Log";
    }

    @Override
    public Collection<RestLogDTO> list() throws PersistenceException {
        return null;
    }

    @Override
    public Class<RestLogDTO> getBean() {
        return RestLogDTO.class;
    }

    @Override
    public Collection<RestLogDTO> find(final IDto arg0) throws PersistenceException {
        return null;
    }

    public Collection<RestLogDTO> findByIdRestExecution(final Integer parm) throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordenacao = new ArrayList<>();
        condicao.add(new Condition("idRestExecution", "=", parm));
        ordenacao.add(new Order("idRestLog"));
        return super.findByCondition(condicao, ordenacao);
    }

    public void deleteByIdRestExecution(final Integer parm) throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        condicao.add(new Condition("idRestExecution", "=", parm));
        super.deleteByCondition(condicao);
    }

	public Collection<RestLogDTO> findErrorsByOperationAndDate(final String origin, final Date startDate, final Date endDate, final String... operations) throws PersistenceException {
		List parametro = new ArrayList();
		List listRetorno = new ArrayList();
		List list = new ArrayList();

		StringBuilder sql = new StringBuilder();
		sql.append("select l.idrestlog, l.idrestexecution, l.datetime, o.name, o.description, l.resultdata, e.platform, e.uuid");
		sql.append("  from rest_log l inner join rest_execution e ON e.idrestexecution = l.idrestexecution ");
		sql.append("                  inner join rest_operation o ON o.idrestoperation = e.idrestoperation ");
		sql.append(" where l.status = ? ");
		sql.append("   and l.datetime between ? and ?");                                                                  

		parametro.add(ExecutionStatus.Error.name());		
		parametro.add(startDate);
		parametro.add(endDate);
		
		if (operations != null && operations.length > 0) {
			sql.append("  and o.name in (");
			String str = "";
			int i = 0;
			for (String operation : operations) {
				if (i > 0) {
					str += ",";
				}
				str += "'"+operation+"'";
				i++;
			}
			sql.append(str+") ");
		}
		
		if (!UtilStrings.isNullOrEmpty(origin)) {
			sql.append("   and e.platform = ? ");
			parametro.add(origin);
		}
		
		sql.append(" order by l.datetime , l.idrestlog");
		

		list = this.execSQL(sql.toString(), parametro.toArray());

		listRetorno.add("idRestLog");
		listRetorno.add("idRestExecution");
		listRetorno.add("dateTime");
		listRetorno.add("name");
		listRetorno.add("description");
		listRetorno.add("resultData");
		listRetorno.add("origin");
		listRetorno.add("uuid");
		
		return (Collection<RestLogDTO>) this.listConvertion(getBean(), list, listRetorno);
	}
}
