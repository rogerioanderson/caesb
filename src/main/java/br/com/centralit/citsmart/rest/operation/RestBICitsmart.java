/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.operation;

import javax.xml.bind.JAXBException;

import br.com.centralit.citcorpore.bi.utils.BICitsmartUtils;
import br.com.centralit.citsmart.rest.bean.RestOperationDTO;
import br.com.centralit.citsmart.rest.bean.RestSessionDTO;
import br.com.centralit.citsmart.rest.schema.BICitsmart;
import br.com.centralit.citsmart.rest.schema.BICitsmartResp;
import br.com.centralit.citsmart.rest.util.RestBICitsmartOperationUtil;
import br.com.centralit.citsmart.rest.util.RestEnum;

/**
 * Respons�vel por Executar as Opera��es da Classe RestBICitsmartResource.
 *
 * @author valdoilo.damasceno
 * @since 06.12.2013
 */
public class RestBICitsmart implements IRestOperation<BICitsmart, BICitsmartResp> {

    /**
     * Busca o xml das tabelas utilizadas no BI do Citsmart, seta e retorna o objeto BI.
     *
     * @param session
     * @param operation
     * @param message
     * @return BICitsmartResp
     * @throws JAXBException
     * @author rodrigo.acorse
     */
    protected BICitsmartResp getTabelas(final RestSessionDTO session, final RestOperationDTO operation, final BICitsmart message) throws JAXBException {
        final BICitsmartResp resp = new BICitsmartResp();

        if (session.getUser() == null || session.getUser().getLogin() == null || session.getUser().getLogin().trim().equals("")) {
            resp.setError(RestBICitsmartOperationUtil.buildError(RestEnum.INPUT_ERROR, "Login do usu�rio n�o identificado"));
            return resp;
        }

        try {
            final String xml = BICitsmartUtils.recuperaXmlTabelasBICitsmart(false);
            resp.setXml(xml);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    /*
     * (non-Javadoc)
     * @see br.com.centralit.citsmart.rest.operation.IRestOperation#execute(br.com.centralit.citsmart.rest.bean.RestSessionDTO,
     * br.com.centralit.citsmart.rest.bean.RestExecutionDTO,
     * br.com.centralit.citsmart.rest.bean.RestOperationDTO, br.com.centralit.citsmart.rest.schema.CtMessage)
     */
    @Override
    public BICitsmartResp execute(final RestSessionDTO session, final RestOperationDTO operation, final BICitsmart message) throws JAXBException {
        return this.getTabelas(session, operation, message);
    }

}
