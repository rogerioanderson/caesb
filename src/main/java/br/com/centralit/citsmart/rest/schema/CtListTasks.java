/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CtListTasks", propOrder = {"listarIncidentes", "listarRequisicoes", "listarCompras", "listarViagens", "listarRH"})
public class CtListTasks extends CtMessage {

    @XmlElement(name = "ListarIncidentes", required = true)
    protected String listarIncidentes;

    @XmlElement(name = "ListarRequisicoes", required = true)
    protected String listarRequisicoes;

    @XmlElement(name = "ListarCompras", required = true)
    protected String listarCompras;

    @XmlElement(name = "ListarViagens", required = true)
    protected String listarViagens;

    @XmlElement(name = "ListarRH", required = true)
    protected String listarRH;

    public String getListarIncidentes() {
        return listarIncidentes;
    }

    public void setListarIncidentes(final String listarIncidentes) {
        this.listarIncidentes = listarIncidentes;
    }

    public String getListarRequisicoes() {
        return listarRequisicoes;
    }

    public void setListarRequisicoes(final String listarRequisicoes) {
        this.listarRequisicoes = listarRequisicoes;
    }

    public String getListarCompras() {
        return listarCompras;
    }

    public void setListarCompras(final String listarCompras) {
        this.listarCompras = listarCompras;
    }

    public String getListarViagens() {
        return listarViagens;
    }

    public void setListarViagens(final String listarViagens) {
        this.listarViagens = listarViagens;
    }

    public String getListarRH() {
        return listarRH;
    }

    public void setListarRH(final String listarRH) {
        this.listarRH = listarRH;
    }

}
