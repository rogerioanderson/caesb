/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.v3.schema;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import br.com.centralit.citsmart.rest.schema.CtMessage;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CtOccurrenceGetList")
public class CtOccurrenceGetList extends CtMessage {
	
    @XmlElement(name = "RequestNumber", required = false, nillable = true)
    protected String requestNumber;
    
    @XmlElement(name = "RequestNumberOrigin", required = false, nillable = true)
    protected String requestNumberOrigin;
    
    @XmlElement(name = "CategoryList", required = false, nillable=true)
    protected List<String> categoryList;

    @XmlElement(name = "RequestGetList", required = false, nillable = true)
    protected CtRequestGetList requestGetList;

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getRequestNumberOrigin() {
		return requestNumberOrigin;
	}

	public void setRequestNumberOrigin(String requestNumberOrigin) {
		this.requestNumberOrigin = requestNumberOrigin;
	}

	public CtRequestGetList getRequestGetList() {
		return requestGetList;
	}

	public void setRequestGetList(CtRequestGetList requestGetList) {
		this.requestGetList = requestGetList;
	}

	public List<String> getCategoryList() {
        if (categoryList == null) {
        	categoryList = new ArrayList<String>();
        }
		return categoryList;
	}

	public void setCategoryList(List<String> categoryList) {
		this.categoryList = categoryList;
	}
    
    
}
