/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.v2.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import br.com.centralit.citsmart.rest.schema.CtNotification;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"latitude", "longitude", "timeSLA", "personal", "inService", "inChekin", "typeRequest", "contract", "unit", "priorityorder"})
public class CTNotification extends CtNotification {

    @XmlElement(name = "Latitude", required = true)
    protected Double latitude;

    @XmlElement(name = "Longitude", required = true)
    protected Double longitude;

    @XmlElement(name = "TimeSLA", required = true)
    private Integer timeSLA;

    @XmlElement(name = "Personal", required = true, defaultValue = "false")
    private boolean personal;

    @XmlElement(name = "InService", required = true, defaultValue = "false")
    private boolean inService;

    @XmlElement(name = "Unit", required = true)
    private Integer unit;

    @XmlElement(name = "Contract", required = true)
    private Integer contract;

    @XmlElement(name = "TypeRequest", defaultValue = "0", required = true)
    private Integer typeRequest;

    @XmlElement(name = "PriorityOrder", required = true)
    private Integer priorityorder;

    @XmlElement(name = "InChekin", defaultValue = "0", required = true)
    private Integer inChekin;

    @XmlElement(name = "UnidadeDes", defaultValue = "", required = false)
    private String unidadeDes;
    
    @XmlElement(name = "NomeContrato", defaultValue = "", required = false)
    private String nomeContrato;
    
    @XmlElement(name = "NomeResponsavel", defaultValue = "", required = false)
    private String nomeResponsavel;
    
    @XmlElement(name = "Situacao", defaultValue = "", required = false)
    private String situacao;
    
    @XmlElement(name = "NotificarSomenteSemResponsavel", defaultValue = "", required = false)
    private boolean notificarSomenteSemResponsavel;
    
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

    public Integer getTimeSLA() {
        return timeSLA;
    }

    public void setTimeSLA(final Integer timeSLA) {
        this.timeSLA = timeSLA;
    }

    public boolean isPersonal() {
        return personal;
    }

    public void setPersonal(final boolean personal) {
        this.personal = personal;
    }

    public boolean isInService() {
        return inService;
    }

    public void setInService(final boolean inService) {
        this.inService = inService;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(final Integer unidade) {
        unit = unidade;
    }

    public Integer getContract() {
        return contract;
    }

    public void setContract(final Integer contrato) {
        contract = contrato;
    }

    public Integer getTypeRequest() {
        return typeRequest;
    }

    public void setTypeRequest(final Integer typeRequest) {
        this.typeRequest = typeRequest;
    }

    public Integer getPriorityorder() {
        return priorityorder;
    }

    public void setPriorityorder(final Integer priorityorder) {
        this.priorityorder = priorityorder;
    }

    public Integer getInChekin() {
        return inChekin;
    }

    public void setInChekin(final Integer inChekin) {
        this.inChekin = inChekin;
    }

	public String getUnidadeDes() {
		return unidadeDes;
	}

	public void setUnidadeDes(String unidadeDes) {
		this.unidadeDes = unidadeDes;
	}

	public String getNomeContrato() {
		return nomeContrato;
	}

	public void setNomeContrato(String nomeContrato) {
		this.nomeContrato = nomeContrato;
	}

	public String getNomeResponsavel() {
		return nomeResponsavel;
	}

	public void setNomeResponsavel(String nomeResponsavel) {
		this.nomeResponsavel = nomeResponsavel;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public boolean isNotificarSomenteSemResponsavel() {
		return notificarSomenteSemResponsavel;
	}

	public void setNotificarSomenteSemResponsavel(boolean notificarSomenteSemResponsavel) {
		this.notificarSomenteSemResponsavel = notificarSomenteSemResponsavel;
	}

}
