/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.v3.schema;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CtOccurrence")
public class CtOccurrence {

    @XmlElement(name = "NumberOrigin", required = false, nillable = true)
    protected String numberOrigin;
    
    @XmlElement(name = "Number", required = false, nillable = true)
    protected String number;
    
    @XmlElement(name = "Description", required = false, nillable = true)
    protected String description;

    @XmlSchemaType(name = "dateTime")
    @XmlElement(name = "Data", required = false, nillable = false)
    protected Calendar Date;
    
    @XmlElement(name = "Hour", required = false, nillable = true)
    protected String hour;
    
    @XmlElement(name = "Origin", required = false, nillable = true)
    protected CtOrigin origin;

    @XmlElement(name = "Category", required = false, nillable = true)
    protected CtCategory category;

    @XmlElement(name = "UserID", required = false, nillable = true)
    protected String userID;

    @XmlElement(name = "ElapsedTime", required = false, nillable = true)
    protected Long elapsedTime;

    @XmlElement(name = "Reason", required = false, nillable = true)
    protected String reason;
    
    @XmlElement(name = "Task", required = false, nillable=true)
    protected CtTask task;
    
    @XmlElement(name = "RequestNumber", required = false, nillable = true)
    protected String requestNumber;    

    @XmlElement(name = "RequestNumberOrigin", required = false, nillable = true)
    protected String requestNumberOrigin;    

    @XmlElement(name = "Request", required = false, nillable = true)
    protected CtRequest request;  
    
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Calendar getDate() {
		return Date;
	}

	public void setDate(Calendar date) {
		Date = date;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public Long getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(Long elapsedTime) {
		this.elapsedTime = elapsedTime;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public CtTask getTask() {
		return task;
	}

	public void setTask(CtTask task) {
		this.task = task;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public CtOrigin getOrigin() {
		return origin;
	}

	public void setOrigin(CtOrigin origin) {
		this.origin = origin;
	}

	public CtCategory getCategory() {
		return category;
	}

	public void setCategory(CtCategory category) {
		this.category = category;
	}

	public String getNumberOrigin() {
		return numberOrigin;
	}

	public void setNumberOrigin(String numberOrigin) {
		this.numberOrigin = numberOrigin;
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public CtRequest getRequest() {
		return request;
	}

	public void setRequest(CtRequest request) {
		this.request = request;
	}

	public String getRequestNumberOrigin() {
		return requestNumberOrigin;
	}

	public void setRequestNumberOrigin(String requestNumberOrigin) {
		this.requestNumberOrigin = requestNumberOrigin;
	}
    
}
