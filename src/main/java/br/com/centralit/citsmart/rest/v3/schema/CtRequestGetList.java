/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.v3.schema;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import br.com.centralit.citsmart.rest.schema.CtMessage;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CtRequestGetList")
public class CtRequestGetList extends CtMessage {
	
    @XmlElement(name = "UserID", required = false, nillable = false)
    protected String userID;

    @XmlElement(name = "Description", required = false, nillable = true)
    protected String description;

    @XmlSchemaType(name = "dateTime")
    @XmlElement(name = "StartDate", required = false, nillable = true)
    protected Calendar startDate;
    
    @XmlSchemaType(name = "dateTime")
    @XmlElement(name = "EndDate", required = false, nillable = true)
    protected Calendar endDate;

    @XmlElement(name = "Service", required = false, nillable = true)
    protected CtService service;
    
    @XmlElement(name = "ContractID", required = false, nillable=true)
    protected String contractID;
    
    @XmlElement(name = "Status", required = false, nillable=true)
    protected CtStatus status;
    
    @XmlElement(name = "ContractList", required = false, nillable=true)
    protected List<String> contractList;
    
    @XmlElement(name = "StatusList", required = false, nillable=true)
    protected List<String> statusList;
    
    @XmlElement(name = "GroupList", required = false, nillable=true)
    protected List<String> groupList;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Calendar getStartDate() {
		return startDate;
	}

	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	public CtService getService() {
		return service;
	}

	public void setService(CtService service) {
		this.service = service;
	}

	public String getContractID() {
		return contractID;
	}

	public void setContractID(String contractID) {
		this.contractID = contractID;
	}

	public CtStatus getStatus() {
		return status;
	}

	public void setStatus(CtStatus status) {
		this.status = status;
	}

    public CtRequestGetList() {
        this.setMessageID("requestGetByUser");
    }

	public List<String> getContractList() {
        if (contractList == null) {
        	contractList = new ArrayList<String>();
        }
		return contractList;
	}

	public void setContractList(List<String> contractList) {
		this.contractList = contractList;
	}

	public List<String> getStatusList() {
        if (statusList == null) {
        	statusList = new ArrayList<String>();
        }
		return statusList;
	}

	public void setStatusList(List<String> statusList) {
		this.statusList = statusList;
	}

	public List<String> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<String> groupList) {
		this.groupList = groupList;
	}
    
    
}
