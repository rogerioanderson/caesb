/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.resource;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import br.com.centralit.citsmart.rest.schema.CtNotificationFeedback;
import br.com.centralit.citsmart.rest.schema.CtNotificationGetById;
import br.com.centralit.citsmart.rest.schema.CtNotificationGetByUser;
import br.com.centralit.citsmart.rest.schema.CtNotificationGetReasons;
import br.com.centralit.citsmart.rest.schema.CtNotificationNew;
import br.com.centralit.citsmart.rest.util.RestOperationUtil;

@Path("/mobile/notification")
public class RestMobileResources {

    protected static final String GETBYUSER = "/getByUser";
    protected static final String GETBYID = "/getById";

    @POST
    @Path(GETBYUSER)
    public Response getNotificationByUser(final CtNotificationGetByUser input) {
        input.setMessageID("notification_getByUser");
        return RestOperationUtil.execute(input);
    }

    @POST
    @Path(GETBYID)
    public Response getNotificationById(final CtNotificationGetById input) {
        input.setMessageID("notification_getById");
        return RestOperationUtil.execute(input);
    }

    @POST
    @Path("/feedback")
    public Response feedback(final CtNotificationFeedback input) {
        input.setMessageID("notification_feedback");
        return RestOperationUtil.execute(input);
    }

    @POST
    @Path("/new")
    public Response newNotification(final CtNotificationNew input) {
        input.setMessageID("notification_new");
        return RestOperationUtil.execute(input);
    }

    @POST
    @Path("/getReasons")
    public Response getReasons(final CtNotificationGetReasons input) {
        input.setMessageID("notification_getReasons");
        return RestOperationUtil.execute(input);
    }

}
