/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.v3.schema;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CtRequest")
public class CtRequest {

    @XmlElement(name = "Number", required = false)
    protected String number;

    @XmlElement(name = "NumberOrigin", required = false)
    protected String numberOrigin;

    @XmlElement(name = "Type", required = false, nillable = true)
    protected StRequestType type;

    @XmlElement(name = "Description", required = false, nillable = true)
    protected String description;

    @XmlSchemaType(name = "dateTime")
    @XmlElement(name = "StartDateTime", required = false, nillable = true)
    protected Calendar startDateTime;

    @XmlSchemaType(name = "dateTime")
    @XmlElement(name = "StartSLA", required = false, nillable = true)
    protected Calendar startSLA;

    @XmlSchemaType(name = "dateTime")
    @XmlElement(name = "EndSLA", required = false, nillable = true)
    protected Calendar endSLA;
    
    @XmlElement(name = "Deadline", required = false, nillable = true)
    protected long deadline;

    @XmlElement(name = "Urgency", required = false, nillable = true)
    protected StRequestPriority urgency;

    @XmlElement(name = "Impact", required = false, nillable = true)
    protected StRequestPriority impact;

    @XmlElement(name = "GroupId", required = false, nillable = true)
    protected String groupId;

    @XmlElement(name = "UserID", required = false, nillable = true)
    protected String userID;

    @XmlElement(name = "Contact", required = false, nillable = true)
    protected CtContact contact;

    @XmlElement(name = "Service", required = false, nillable = true)
    protected CtService service;
    
    @XmlElement(name = "ContractID", required = false, nillable=true)
    protected String contractID;
    
    @XmlElement(name = "Status", required = false, nillable=true)
    protected CtStatus status;

    @XmlElement(name = "CurrentTask", required = false, nillable=true)
    protected CtTask currentTask;
    
    @XmlElement(name = "DefinedSLA", required = false, nillable = true)
    protected boolean definedSLA;
    
    @XmlElement(name = "LinkedNumber", required = false)
    protected String linkedNumber;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getNumberOrigin() {
		return numberOrigin;
	}

	public void setNumberOrigin(String numberOrigin) {
		this.numberOrigin = numberOrigin;
	}

	public StRequestType getType() {
		return type;
	}

	public void setType(StRequestType type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Calendar getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(Calendar startDateTime) {
		this.startDateTime = startDateTime;
	}

	public Calendar getStartSLA() {
		return startSLA;
	}

	public void setStartSLA(Calendar startSLA) {
		this.startSLA = startSLA;
	}

	public Calendar getEndSLA() {
		return endSLA;
	}

	public void setEndSLA(Calendar endSLA) {
		this.endSLA = endSLA;
	}

	public StRequestPriority getUrgency() {
		return urgency;
	}

	public void setUrgency(StRequestPriority urgency) {
		this.urgency = urgency;
	}

	public StRequestPriority getImpact() {
		return impact;
	}

	public void setImpact(StRequestPriority impact) {
		this.impact = impact;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public CtContact getContact() {
		return contact;
	}

	public void setContact(CtContact contact) {
		this.contact = contact;
	}

	public CtService getService() {
		return service;
	}

	public void setService(CtService service) {
		this.service = service;
	}

	public String getContractID() {
		return contractID;
	}

	public void setContractID(String contractID) {
		this.contractID = contractID;
	}

	public CtStatus getStatus() {
		return status;
	}

	public void setStatus(CtStatus status) {
		this.status = status;
	}

	public CtTask getCurrentTask() {
		return currentTask;
	}

	public void setCurrentTask(CtTask currentTask) {
		this.currentTask = currentTask;
	}

	public long getDeadline() {
		return deadline;
	}

	public void setDeadline(long deadline) {
		this.deadline = deadline;
	}

	public boolean isDefinedSLA() {
		return definedSLA;
	}

	public void setDefinedSLA(boolean definedSLA) {
		this.definedSLA = definedSLA;
	}

	public String getLinkedNumber() {
		return linkedNumber;
	}

	public void setLinkedNumber(String linkedNumber) {
		this.linkedNumber = linkedNumber;
	}

}
