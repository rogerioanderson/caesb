/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.schema.old;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import br.com.centralit.citsmart.rest.schema.CtError;
import br.com.centralit.citsmart.rest.schema.CtServiceRequest;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CtAddServiceRequestResp", propOrder = {"dateTime", "operationID", "error", "serviceRequestDest"})
public class CtAddServiceRequestResp {

    @XmlSchemaType(name = "dateTime")
    @XmlElement(name = "DateTime", required = true)
    protected XMLGregorianCalendar dateTime;

    @XmlElement(name = "OperationID", required = true)
    protected BigInteger operationID;

    @XmlElement(name = "Error")
    protected CtError error;

    @XmlElement(name = "ServiceRequestDest")
    protected CtServiceRequest serviceRequestDest;

    public XMLGregorianCalendar getDateTime() {
        return dateTime;
    }

    public void setDateTime(final XMLGregorianCalendar dateTime) {
        this.dateTime = dateTime;
    }

    public BigInteger getOperationID() {
        return operationID;
    }

    public void setOperationID(final BigInteger operationID) {
        this.operationID = operationID;
    }

    public CtError getError() {
        return error;
    }

    public void setError(final CtError error) {
        this.error = error;
    }

    public CtServiceRequest getServiceRequestDest() {
        return serviceRequestDest;
    }

    public void setServiceRequestDest(final CtServiceRequest serviceRequestDest) {
        this.serviceRequestDest = serviceRequestDest;
    }

}
