/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.operation;

import javax.xml.bind.JAXBException;

import br.com.centralit.citsmart.rest.bean.RestOperationDTO;
import br.com.centralit.citsmart.rest.bean.RestSessionDTO;
import br.com.centralit.citsmart.rest.schema.CtMessage;
import br.com.centralit.citsmart.rest.schema.CtMessageResp;

/**
 * Interface para execu��o de uma opera��o REST cadastrada no sistema {@link RestOperationDTO}
 *
 * @param <T>
 *            objeto JAXB da request
 * @param <U>
 *            objeto JAXB da response
 */
public interface IRestOperation<T extends CtMessage, U extends CtMessageResp> {

    /**
     * Executa uma opera��o REST previamente cadastrada no sistema
     *
     * @param session
     *            sess�o do usu�rio que realizou a chamada
     * @param operation
     *            opera��o REST
     * @param message
     *            {@code T extends CtMessage}, request a ser processada
     * @return {@code U extends CtMessageResp}, response a ser serializada
     * @throws JAXBException
     */
    U execute(final RestSessionDTO session, final RestOperationDTO operation, final T message) throws JAXBException;

}
