/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.v3.resource;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import br.com.centralit.citsmart.rest.resource.RestOperationResources;
import br.com.centralit.citsmart.rest.util.RestOperationUtil;
import br.com.centralit.citsmart.rest.v2.util.RESTOperations;
import br.com.centralit.citsmart.rest.v3.schema.CtErrorGetList;
import br.com.centralit.citsmart.rest.v3.schema.CtOccurrenceCreate;
import br.com.centralit.citsmart.rest.v3.schema.CtOccurrenceGetList;
import br.com.centralit.citsmart.rest.v3.schema.CtOccurrenceGetNotSynchronized;
import br.com.centralit.citsmart.rest.v3.schema.CtRequestCreate;
import br.com.centralit.citsmart.rest.v3.schema.CtRequestGetById;
import br.com.centralit.citsmart.rest.v3.schema.CtRequestGetList;
import br.com.centralit.citsmart.rest.v3.schema.CtRequestGetNotSynchronized;
import br.com.centralit.citsmart.rest.v3.schema.CtRequestUpdate;
import br.com.centralit.citsmart.rest.v3.schema.CtRequestUpdateStatus;
import br.com.centralit.citsmart.rest.v3.schema.CtSynchronize;
import br.com.centralit.citsmart.rest.v3.schema.CtSynchronizeGetList;

/**
 * Endpoints REST da vers�o V2 dos servi�os oferecidos ao Mobile
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 22/09/2014
 *
 */
@Path("/services")
public class RESTOperationResources extends RestOperationResources {

    private static final Logger LOGGER = Logger.getLogger(RESTOperationResources.class.getName());

    private final String CALL_MESSAGE = "Service handling at '%s' called with parameters '%s'";

    private static final String CREATE = "/request/create";
    private static final String UPDATE = "/request/update";
    private static final String UPDATESTATUS = "/request/updateStatus";
    private static final String GETBYUSER = "/request/getByUser";
    private static final String GETBYID = "/request/getById";
    private static final String CREATEOCCURRENCE = "/request/createOccurrence";
    private static final String LISTOCCURRENCES = "/request/listOccurrences";
    
    private static final String NOTSYNCHRONIZEDREQUESTS = "/request/getNotSynchronized";
    private static final String NOTSYNCHRONIZEDOCCURRENCES = "/occurrence/getNotSynchronized";
    private static final String SYNCHRONIZE = "/synchronize"; 
    private static final String FINDSYNCHRONIZE = "/synchronize/find"; 
    private static final String DELETESYNCHRONIZE = "/synchronize/delete"; 
    
    private static final String ERRORGETLIST = "/error/getList";
    
    @Context
    private HttpServletRequest request;

    @POST
    @Path(CREATE)
    public Response createRequest(final CtRequestCreate input) {
        LOGGER.log(Level.FINE, String.format(CALL_MESSAGE, CREATE, ReflectionToStringBuilder.toString(input)));
        input.setMessageID(RESTOperations.REQUEST_CREATE.getMessageID());
        return RestOperationUtil.execute(input);
    }

    @POST
    @Path(UPDATE)
    public Response updateRequest(final CtRequestUpdate input) {
        LOGGER.log(Level.FINE, String.format(CALL_MESSAGE, UPDATE, ReflectionToStringBuilder.toString(input)));
        input.setMessageID(RESTOperations.REQUEST_UPDATE.getMessageID());
        return RestOperationUtil.execute(input);
    }
    
    @POST
    @Path(UPDATESTATUS)
    public Response createRequest(final CtRequestUpdateStatus input) {
        LOGGER.log(Level.FINE, String.format(CALL_MESSAGE, UPDATESTATUS, ReflectionToStringBuilder.toString(input)));
        input.setMessageID(RESTOperations.REQUEST_UPDATE_STATUS.getMessageID());
        return RestOperationUtil.execute(input);
    }

    @POST
    @Path(GETBYUSER)
    public Response getByUser(final CtRequestGetList input) {
        LOGGER.log(Level.FINE, String.format(CALL_MESSAGE, GETBYUSER, ReflectionToStringBuilder.toString(input)));
        input.setMessageID(RESTOperations.REQUEST_GET_BY_USER.getMessageID());
        return RestOperationUtil.execute(input);
    }
    
    @POST
    @Path(GETBYID)
    public Response getById(final CtRequestGetById input) {
        LOGGER.log(Level.FINE, String.format(CALL_MESSAGE, GETBYID, ReflectionToStringBuilder.toString(input)));
        input.setMessageID(RESTOperations.REQUEST_GET_BY_ID.getMessageID());
        return RestOperationUtil.execute(input);
    }
    
    @POST
    @Path(CREATEOCCURRENCE)
    public Response createOccurrence(final CtOccurrenceCreate input) {
        LOGGER.log(Level.FINE, String.format(CALL_MESSAGE, CREATEOCCURRENCE, ReflectionToStringBuilder.toString(input)));
        input.setMessageID(RESTOperations.REQUEST_CREATE_OCCURRENCE.getMessageID());
        return RestOperationUtil.execute(input);
    }
    
    @POST
    @Path(LISTOCCURRENCES)
    public Response listOccurrence(final CtOccurrenceGetList input) {
        LOGGER.log(Level.FINE, String.format(CALL_MESSAGE, LISTOCCURRENCES, ReflectionToStringBuilder.toString(input)));
        input.setMessageID(RESTOperations.REQUEST_LIST_OCCURRENCES.getMessageID());
        return RestOperationUtil.execute(input);
    }
    
    @POST
    @Path(NOTSYNCHRONIZEDREQUESTS)
    public Response getNotSynchronizedRequests(final CtRequestGetNotSynchronized input) {
        LOGGER.log(Level.FINE, String.format(CALL_MESSAGE, NOTSYNCHRONIZEDREQUESTS, ReflectionToStringBuilder.toString(input)));
        input.setMessageID(RESTOperations.REQUEST_GET_NOT_SYNCHRONIZED.getMessageID());
        return RestOperationUtil.execute(input);
    }    

    @POST
    @Path(NOTSYNCHRONIZEDOCCURRENCES)
    public Response getNotSynchronizedOccurrences(final CtOccurrenceGetNotSynchronized input) {
        LOGGER.log(Level.FINE, String.format(CALL_MESSAGE, NOTSYNCHRONIZEDOCCURRENCES, ReflectionToStringBuilder.toString(input)));
        input.setMessageID(RESTOperations.OCCURRENCE_GET_NOT_SYNCHRONIZED.getMessageID());
        return RestOperationUtil.execute(input);
    }  
    
    @POST
    @Path(SYNCHRONIZE)
    public Response synchronize(final CtSynchronize input) {
        LOGGER.log(Level.FINE, String.format(CALL_MESSAGE, SYNCHRONIZE, ReflectionToStringBuilder.toString(input)));
        input.setMessageID(RESTOperations.SYNCHRONIZE.getMessageID());
        return RestOperationUtil.execute(input);
    } 

    @POST
    @Path(FINDSYNCHRONIZE)
    public Response synchronizeFind(final CtSynchronizeGetList input) {
        LOGGER.log(Level.FINE, String.format(CALL_MESSAGE, FINDSYNCHRONIZE, ReflectionToStringBuilder.toString(input)));
        input.setMessageID(RESTOperations.SYNCHRONIZE_FIND.getMessageID());
        return RestOperationUtil.execute(input);
    } 

    @POST
    @Path(DELETESYNCHRONIZE)
    public Response deleteSynchronize(final CtSynchronize input) {
        LOGGER.log(Level.FINE, String.format(CALL_MESSAGE, DELETESYNCHRONIZE, ReflectionToStringBuilder.toString(input)));
        input.setMessageID(RESTOperations.SYNCHRONIZE_DELETE.getMessageID());
        return RestOperationUtil.execute(input);
    } 
    
    @POST
    @Path(ERRORGETLIST)
    public Response errorGetList(final CtErrorGetList input) {
        LOGGER.log(Level.FINE, String.format(CALL_MESSAGE, ERRORGETLIST, ReflectionToStringBuilder.toString(input)));
        input.setMessageID(RESTOperations.ERROR_GET_LIST.getMessageID());
        return RestOperationUtil.execute(input);
    } 
}
