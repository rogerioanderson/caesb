/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.bean;

import java.sql.Timestamp;

import javax.servlet.http.HttpSession;

import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.citframework.dto.IDto;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;

public class RestSessionDTO implements IDto {

    private static final long serialVersionUID = -4117354478284577409L;

    private HttpSession httpSession;
    private Timestamp creation;
    private long maxTime;
    
    private String platform;
    private String uuid;    

    public HttpSession getHttpSession() {
        return httpSession;
    }

    public void setHttpSession(final HttpSession httpSession) {
        this.httpSession = httpSession;
    }

    public Timestamp getCreation() {
        return creation;
    }

    public void setCreation(final Timestamp creation) {
        this.creation = creation;
    }

    public long getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(final long maxTime) {
        this.maxTime = maxTime;
    }

    public String getSessionID() {
        if (httpSession != null) {
            return httpSession.getId();
        } else {
            return null;
        }
    }

    public boolean isValid() {
        if (httpSession == null) {
            return false;
        }
        long tempo;
        try {
            tempo = UtilDatas.calculaDiferencaTempoEmMilisegundos(UtilDatas.getDataHoraAtual(), creation) / 1000;
        } catch (final Exception e) {
            e.printStackTrace();
            return false;
        }
        return tempo <= maxTime;
    }

    public UsuarioDTO getUser() {
        if (!this.isValid()) {
            return null;
        }
        return (UsuarioDTO) this.getHttpSession().getAttribute(Constantes.getValue("USUARIO_SESSAO") + "_CITCORPORE");
    }

    public Integer getUserId() {
        final UsuarioDTO userDto = this.getUser();
        if (userDto == null) {
            return null;
        }
        return userDto.getIdUsuario();
    }

    public Integer getDptoId() {
        final UsuarioDTO userDto = this.getUser();
        if (userDto == null) {
            return null;
        }
        return userDto.getIdUnidade();
    }

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
