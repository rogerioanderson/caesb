/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.service;

import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.centralit.citsmart.rest.bean.RestExecutionDTO;
import br.com.centralit.citsmart.rest.bean.RestLogDTO;
import br.com.centralit.citsmart.rest.dao.RestLogDao;
import br.com.centralit.citsmart.rest.schema.CtError;
import br.com.centralit.citsmart.rest.util.RestEnum;
import br.com.centralit.citsmart.rest.util.RestEnum.ExecutionStatus;
import br.com.centralit.citsmart.rest.util.RestUtil;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;
import br.com.citframework.util.UtilDatas;

import com.google.gson.Gson;

public class RestLogServiceEjb extends CrudServiceImpl implements RestLogService {

    private static final Gson GSON = new Gson();

    private static final Logger LOGGER = Logger.getLogger(RestLogServiceEjb.class.getName());

    private RestLogDao dao;

    @Override
    protected RestLogDao getDao() {
        if (dao == null) {
            dao = new RestLogDao();
        }
        return dao;
    }

    @Override
    public Collection<RestLogDTO> findByIdRestExecution(final Integer parm) throws ServiceException {
        try {
            return this.getDao().findByIdRestExecution(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteByIdRestExecution(final Integer parm) throws ServiceException {
        try {
            this.getDao().deleteByIdRestExecution(parm);
        } catch (final Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public CtError create(final RestExecutionDTO restExecution, final Exception e) {
        final CtError error = new CtError();
        error.setCode(RestEnum.INTERNAL_ERROR);
        error.setDescription(RestUtil.stackToString(e));
        this.create(restExecution, error, ExecutionStatus.Error);
        return error;
    }

    @Override
    public CtError create(final RestExecutionDTO restExecution, final String code, final String description) {
        final CtError error = new CtError();
        error.setCode(code);
        error.setDescription(description);
        this.create(restExecution, error, ExecutionStatus.Error);
        return error;
    }

    @Override
    public RestLogDTO create(final RestExecutionDTO restExecution, final Object result, final ExecutionStatus status) {
        RestLogDTO restLog = new RestLogDTO();
        restLog.setIdRestExecution(restExecution.getIdRestExecution());
        restLog.setDateTime(UtilDatas.getDataHoraAtual());
        restLog.setStatus(status.name());
        restLog.setResultClass(result.getClass().getName());
        restLog.setResultData(GSON.toJson(result));

        try {
            restLog = (RestLogDTO) this.create(restLog);
        } catch (final Exception e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }

        return restLog;
    }
    
    @Override
    public Collection<RestLogDTO> findErrorsByOperationAndDate(final String origin, final Date startDate, final Date endDate, final String... operations) throws PersistenceException {
    	return this.getDao().findErrorsByOperationAndDate(origin, startDate, endDate, operations);
    }

}
