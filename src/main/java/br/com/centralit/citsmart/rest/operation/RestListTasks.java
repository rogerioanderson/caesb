/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citsmart.rest.operation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import br.com.centralit.citcorpore.util.Enumerados.TipoSolicitacaoServico;
import br.com.centralit.citsmart.rest.bean.RestOperationDTO;
import br.com.centralit.citsmart.rest.bean.RestSessionDTO;
import br.com.centralit.citsmart.rest.schema.CtListTasks;
import br.com.centralit.citsmart.rest.schema.CtListTasksResp;
import br.com.centralit.citsmart.rest.util.RestEnum;
import br.com.centralit.citsmart.rest.util.RestOperationUtil;
import br.com.centralit.citsmart.rest.util.RestUtil;
import br.com.citframework.util.UtilStrings;

public class RestListTasks implements IRestOperation<CtListTasks, CtListTasksResp> {

    @Override
    public CtListTasksResp execute(final RestSessionDTO restSession, final RestOperationDTO restOperation, final CtListTasks message) throws JAXBException {
        final CtListTasksResp resp = new CtListTasksResp();
        if (restSession.getUser() == null || restSession.getUser().getLogin() == null || restSession.getUser().getLogin().trim().equals("")) {
            resp.setError(RestOperationUtil.buildError(RestEnum.INPUT_ERROR, "Login do usu�rio n�o identificado"));
            return resp;
        }

        final List<TipoSolicitacaoServico> tipos = new ArrayList<>();
        if (UtilStrings.nullToVazio(message.getListarCompras()).equalsIgnoreCase("S")) {
            tipos.add(TipoSolicitacaoServico.COMPRA);
        }
        if (UtilStrings.nullToVazio(message.getListarIncidentes()).equalsIgnoreCase("S")) {
            tipos.add(TipoSolicitacaoServico.INCIDENTE);
        }
        if (UtilStrings.nullToVazio(message.getListarRequisicoes()).equalsIgnoreCase("S")) {
            tipos.add(TipoSolicitacaoServico.REQUISICAO);
        }
        if (UtilStrings.nullToVazio(message.getListarRH()).equalsIgnoreCase("S")) {
            tipos.add(TipoSolicitacaoServico.RH);
        }
        if (UtilStrings.nullToVazio(message.getListarViagens()).equalsIgnoreCase("S")) {
            tipos.add(TipoSolicitacaoServico.VIAGEM);
        }

        if (tipos.size() == 0) {
            resp.setError(RestOperationUtil.buildError(RestEnum.INPUT_ERROR, "Pelo menos um tipo de solicita��o deve ser selecionado"));
            return resp;
        }

        try {
            resp.setTarefas(RestUtil.getExecucaoSolicitacaoService(restSession).recuperaTarefas(restSession.getUser().getLogin(),
                    tipos.toArray(new TipoSolicitacaoServico[tipos.size()]), "N"));
            resp.setQtdeTarefas(resp.getTarefas().size());
        } catch (final Exception e) {
            e.printStackTrace();
            resp.setError(RestOperationUtil.buildError(e));
        }

        return resp;
    }

}
