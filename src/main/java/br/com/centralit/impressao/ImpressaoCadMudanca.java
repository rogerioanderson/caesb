/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.impressao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.fill.JRAbstractLRUVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.AprovacaoMudancaDTO;
import br.com.centralit.citcorpore.bean.AprovacaoPropostaDTO;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.GrupoRequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.LiberacaoMudancaDTO;
import br.com.centralit.citcorpore.bean.ProblemaMudancaDTO;
import br.com.centralit.citcorpore.bean.ReqMudancaJasperDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaResponsavelDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaRiscoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaServicoDTO;
import br.com.centralit.citcorpore.bean.SolicitacaoServicoDTO;
import br.com.centralit.citcorpore.bean.UploadDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.GrupoRequisicaoMudancaService;
import br.com.centralit.citcorpore.negocio.LiberacaoMudancaService;
import br.com.centralit.citcorpore.negocio.ProblemaMudancaService;
import br.com.centralit.citcorpore.negocio.RequisicaoMudancaResponsavelService;
import br.com.centralit.citcorpore.negocio.RequisicaoMudancaRiscoService;
import br.com.centralit.citcorpore.negocio.RequisicaoMudancaService;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.LogoRel;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citcorpore.util.UtilRelatorio;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citged.bean.ControleGEDDTO;
import br.com.centralit.citged.negocio.ControleGEDService;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

/**
 * @author euler.ramos
 * Classe que gera a impress�o do cadastro da Requisi��o de Mudan�a
 */
public class ImpressaoCadMudanca {

	private DocumentHTML document;
	private HttpServletRequest request;
	private ReqMudancaJasperDTO reqMudancaJasperDTO;
	private RequisicaoMudancaDTO requisicaoMudancaDTO;
	private UsuarioDTO usuarioDTO;

	private RequisicaoMudancaService requisicaoMudancaService;

	private JRDataSource dataSource;
	private Map<String, Object> parametros;

	private String arquivoRelatorio;
	private String caminhoJasper;
	private String diretorioRelativo;
	private String diretorioTemp;
	private String jasperArqRel;

	private String contrato;

	public ImpressaoCadMudanca(DocumentHTML document, HttpServletRequest request) throws Exception {
		super();
		this.document = document;
		this.request = request;
		this.usuarioDTO = WebUtil.getUsuario(this.request);
		this.requisicaoMudancaDTO = (RequisicaoMudancaDTO) this.document.getBean();
		this.requisicaoMudancaService = (RequisicaoMudancaService) ServiceLocator.getInstance().getService(RequisicaoMudancaService.class, WebUtil.getUsuarioSistema(this.request));
	}

	public void gerarRelatorio() {
		if (this.usuarioDTO == null) {
			this.document.alert(UtilI18N.internacionaliza(this.request, "citcorpore.comum.sessaoExpirada"));
			this.document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + this.request.getContextPath() + "'");
		} else {
			try {
				if ((this.requisicaoMudancaDTO != null) && (this.requisicaoMudancaDTO.getIdRequisicaoMudanca() != null) && (this.requisicaoMudancaDTO.getIdRequisicaoMudanca().intValue() > 0)) {
					this.geraImpressaoCadMudanca();
				} else {
					this.document.alert(UtilI18N.internacionaliza(this.request, "requisicaoMudanca.naoEncontrada"));
				}
				this.document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void abreRelatorioPDF() {
		try {
			JRSwapFile arquivoSwap = new JRSwapFile(this.diretorioTemp, 4096, 25);
			JRAbstractLRUVirtualizer virtualizer = new JRSwapFileVirtualizer(25, arquivoSwap, true);
			this.parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
			JasperPrint print = JasperFillManager.fillReport(this.caminhoJasper + this.jasperArqRel + ".jasper", this.parametros, this.dataSource);

			JasperExportManager.exportReportToPdfFile(print, this.diretorioTemp + this.arquivoRelatorio + ".pdf");

			this.document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + this.diretorioRelativo
					+ this.arquivoRelatorio + ".pdf')");
		} catch (OutOfMemoryError e) {
			this.document.alert(UtilI18N.internacionaliza(this.request, "citcorpore.erro.erroServidor"));
		} catch (JRException e) {
			e.printStackTrace();
		}
	}

	private void buscarVotacoesProposta() throws Exception {
		Collection<AprovacaoPropostaDTO> listaVotacoesProposta = this.requisicaoMudancaService.retornaAprovacoesProposta(this.requisicaoMudancaDTO, this.usuarioDTO, this.request);
		
		//euler.ramos
		//Requisito: A ITIL determina que, quando a mudan�a for padr�o e emergencial, n�o h� a necessidade de vota��o; entretanto, a Central IT convencionou que se poderia votar uma mudan�a padr�o.
		//Por isso, estamos tratando a impress�o para n�o imprimir os registros de vota��o, quando o usu�rio n�o realizar a vota��o.
		if (this.requisicaoMudancaDTO.getTipo().equalsIgnoreCase(UtilI18N.internacionaliza(this.request, "citcorpore.comum.padrao"))
				|| this.requisicaoMudancaDTO.getTipo().equalsIgnoreCase(UtilI18N.internacionaliza(this.request, "citcorpore.comum.emergencial"))) {
			if (this.naoHouveVotacaoProposta(listaVotacoesProposta)){
				listaVotacoesProposta = null;
			}
		}
		
		JRDataSource dsVotacoesProposta;
		if (listaVotacoesProposta!=null && listaVotacoesProposta.size()>0){
			dsVotacoesProposta = new JRBeanCollectionDataSource(listaVotacoesProposta);
		} else {
			dsVotacoesProposta = null;
		}
		this.reqMudancaJasperDTO.setListaVotacoesProposta(dsVotacoesProposta);
	}

	private boolean naoHouveVotacaoProposta(Collection<AprovacaoPropostaDTO> listaVotacoesProposta) {
		boolean resultado = true;
		for (AprovacaoPropostaDTO aprovacaoPropostaDTO : listaVotacoesProposta) {
			if ((aprovacaoPropostaDTO.getVoto()!=null)&&(aprovacaoPropostaDTO.getVoto().length()>0)){
				resultado = false;
				break;
			}
		}
		return resultado;
	}

	private void buscarVotacoesReqMudanca() throws Exception {
		Collection<AprovacaoMudancaDTO> listaVotacoesReqMudanca = this.requisicaoMudancaService.retornaAprovacoesMudanca(this.requisicaoMudancaDTO, this.usuarioDTO, this.request);
		
		//euler.ramos
		//Requisito: A ITIL determina que, quando a mudan�a for padr�o e emergencial, n�o h� a necessidade de vota��o; entretanto, a Central IT convencionou que se poderia votar uma mudan�a padr�o.
		//Por isso, estamos tratando a impress�o para n�o imprimir os registros de vota��o, quando o usu�rio n�o realizar a vota��o.
		if (this.requisicaoMudancaDTO.getTipo().equalsIgnoreCase(UtilI18N.internacionaliza(this.request, "citcorpore.comum.padrao"))
				|| this.requisicaoMudancaDTO.getTipo().equalsIgnoreCase(UtilI18N.internacionaliza(this.request, "citcorpore.comum.emergencial"))) {
			if (this.naoHouveVotacaoReqMudanca(listaVotacoesReqMudanca)){
				listaVotacoesReqMudanca = null;
			}
		}

		JRDataSource dsVotacoesReqMudanca;
		if (listaVotacoesReqMudanca!=null && listaVotacoesReqMudanca.size()>0){
			dsVotacoesReqMudanca = new JRBeanCollectionDataSource(listaVotacoesReqMudanca);
		} else {
			dsVotacoesReqMudanca = null;
		}
		this.reqMudancaJasperDTO.setListaVotacoesReqMudanca(dsVotacoesReqMudanca);
	}

	private boolean naoHouveVotacaoReqMudanca(Collection<AprovacaoMudancaDTO> listaVotacoesReqMudanca) {
		boolean resultado = true;
		for (AprovacaoMudancaDTO aprovacaoMudancaDTO : listaVotacoesReqMudanca) {
			if ((aprovacaoMudancaDTO.getVoto()!=null)&&(aprovacaoMudancaDTO.getVoto().length()>0)){
				resultado = false;
				break;
			}
		}
		return resultado;
	}

	private void buscarICsRelacionados() throws ServiceException, Exception {
		Collection<RequisicaoMudancaItemConfiguracaoDTO> listaICs = this.requisicaoMudancaService.listItensRelacionadosRequisicaoMudanca(this.requisicaoMudancaDTO);
		
		JRDataSource dsICs;
		if(listaICs!=null && listaICs.size()>0){
			dsICs = new JRBeanCollectionDataSource(listaICs);
		} else {
			dsICs = null;
		}
		this.reqMudancaJasperDTO.setListaICs(dsICs);
	}

	private void buscarServicosRelacionados() {
		Collection<RequisicaoMudancaServicoDTO> listaServicos = this.requisicaoMudancaService.listServicosRequisicaoMudanca(this.requisicaoMudancaDTO);
		
		JRDataSource dsServicos;
		if(listaServicos!=null && listaServicos.size()>0){
			dsServicos = new JRBeanCollectionDataSource(listaServicos);
		} else {
			dsServicos = null;
		}
		this.reqMudancaJasperDTO.setListaServicos(dsServicos);
	}

	private void buscarProblemasRelacionados() throws ServiceException, Exception {
		ProblemaMudancaService problemaMudancaService = (ProblemaMudancaService) ServiceLocator.getInstance().getService(ProblemaMudancaService.class, null);
		@SuppressWarnings("unchecked")
		Collection<ProblemaMudancaDTO> listaProblemas = problemaMudancaService.findByIdRequisicaoMudanca(this.requisicaoMudancaDTO.getIdRequisicaoMudanca());
		
		JRDataSource dsProblemas;
		if(listaProblemas!=null && listaProblemas.size()>0){
			dsProblemas = new JRBeanCollectionDataSource(listaProblemas);
		} else {
			dsProblemas = null;
		}
		this.reqMudancaJasperDTO.setListaProblemas(dsProblemas);
	}

	private void buscarSolicitacoesRelacionadas() {
		Collection<SolicitacaoServicoDTO> listaSolicitacoes = this.requisicaoMudancaService.listaSolicitacoesRequisicaoMudanca(this.requisicaoMudancaDTO.getIdRequisicaoMudanca());
		
		JRDataSource dsSolicitacoes;
		if(listaSolicitacoes!=null && listaSolicitacoes.size()>0){
			dsSolicitacoes = new JRBeanCollectionDataSource(listaSolicitacoes);
		} else {
			dsSolicitacoes = null;
		}
		this.reqMudancaJasperDTO.setListaSolicitacoes(dsSolicitacoes);
	}

	private void buscarRiscosRelacionados() throws Exception {
		RequisicaoMudancaRiscoService requisicaoMudancaRiscoService = (RequisicaoMudancaRiscoService) ServiceLocator.getInstance().getService(RequisicaoMudancaRiscoService.class, null);
		@SuppressWarnings("unchecked")
		Collection<RequisicaoMudancaRiscoDTO> listaRiscos = requisicaoMudancaRiscoService.findByIdRequisicaoMudanca(this.requisicaoMudancaDTO.getIdRequisicaoMudanca());
		
		JRDataSource dsListaRiscos;
		if(listaRiscos!=null && listaRiscos.size()>0){
			dsListaRiscos = new JRBeanCollectionDataSource(listaRiscos);
		} else {
			dsListaRiscos = null;
		}
		this.reqMudancaJasperDTO.setListaRiscos(dsListaRiscos);
	}

	private void buscarLiberacoesRelacionadas() throws Exception {
		LiberacaoMudancaService liberacaoMudancaService = (LiberacaoMudancaService) ServiceLocator.getInstance().getService(LiberacaoMudancaService.class, null);
		@SuppressWarnings("unchecked")
		Collection<LiberacaoMudancaDTO> listaLiberacoes = liberacaoMudancaService.findByIdRequisicaoMudanca(this.requisicaoMudancaDTO.getIdLiberacao(),
				this.requisicaoMudancaDTO.getIdRequisicaoMudanca());
		
		JRDataSource dsListaLiberacoes;
		if(listaLiberacoes!=null && listaLiberacoes.size()>0){
			dsListaLiberacoes = new JRBeanCollectionDataSource(listaLiberacoes);
		} else {
			dsListaLiberacoes = null;
		}
		this.reqMudancaJasperDTO.setListaLiberacoes(dsListaLiberacoes);
	}

	private void buscarPapeisRelacionados() throws Exception {
		RequisicaoMudancaResponsavelService mudancaResponsavelService = (RequisicaoMudancaResponsavelService) ServiceLocator.getInstance().getService(RequisicaoMudancaResponsavelService.class,
				WebUtil.getUsuarioSistema(request));
		@SuppressWarnings("unchecked")
		Collection<RequisicaoMudancaResponsavelDTO> listaPapeis = mudancaResponsavelService.findByIdMudancaEDataFim(this.requisicaoMudancaDTO.getIdRequisicaoMudanca());
		
		JRDataSource dsListaPapeis;
		if(listaPapeis!=null && listaPapeis.size()>0){
			dsListaPapeis = new JRBeanCollectionDataSource(listaPapeis);
		} else {
			dsListaPapeis = null;
		}
		this.reqMudancaJasperDTO.setListaPapeis(dsListaPapeis);
	}

	private void buscarAnexosRelacionados() throws Exception {
        ControleGEDService controleGedService = (ControleGEDService) ServiceLocator.getInstance().getService(ControleGEDService.class, null);
        @SuppressWarnings("rawtypes")
		Collection colAnexos = controleGedService.listByIdTabelaAndIdLiberacaoAndLigacao(ControleGEDDTO.TABELA_PLANO_REVERSAO_MUDANCA, this.requisicaoMudancaDTO.getIdRequisicaoMudanca());
        @SuppressWarnings("unchecked")
		Collection<UploadDTO> colAnexosUploadDTO = controleGedService.convertListControleGEDToUploadDTO(colAnexos);
		
        JRDataSource dsListaAnexos;
        if (colAnexosUploadDTO!=null && colAnexosUploadDTO.size()>0){
        	dsListaAnexos = new JRBeanCollectionDataSource(colAnexosUploadDTO);
        } else {
        	dsListaAnexos = null;
        }
		this.reqMudancaJasperDTO.setListaAnexosPlReversao(dsListaAnexos);
	}

	private void buscarGruposRelacionados() throws Exception {
        GrupoRequisicaoMudancaService grupoRequisicaoMudancaService = (GrupoRequisicaoMudancaService) ServiceLocator.getInstance().getService(GrupoRequisicaoMudancaService.class, null);
        @SuppressWarnings("unchecked")
		Collection<GrupoRequisicaoMudancaDTO> listaGrupos = grupoRequisicaoMudancaService.findByIdRequisicaoMudanca(this.requisicaoMudancaDTO.getIdRequisicaoMudanca());
        
        JRDataSource dsListaGrupos;
        if (listaGrupos!=null && listaGrupos.size()>0){
        	dsListaGrupos = new JRBeanCollectionDataSource(listaGrupos);
        } else {
        	dsListaGrupos = null;
        }
		this.reqMudancaJasperDTO.setListaGrupos(dsListaGrupos);
	}

	private void geraImpressaoCadMudanca() throws ServiceException, Exception {
		// Obtendo informa��es da mudan�a
		this.requisicaoMudancaDTO = this.requisicaoMudancaService.restoreAll(this.requisicaoMudancaDTO.getIdRequisicaoMudanca());

		if ((this.requisicaoMudancaDTO != null) && (this.requisicaoMudancaDTO.getIdRequisicaoMudanca() != null) && (this.requisicaoMudancaDTO.getIdRequisicaoMudanca() >= 0)) {

			// Buscando Informa��es de contrato
			if ((this.requisicaoMudancaDTO.getIdContrato() != null) && (this.requisicaoMudancaDTO.getIdContrato().intValue() > 0)) {
				try {
					ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
					ContratoDTO contratoDTO = new ContratoDTO();
					contratoDTO.setIdContrato(this.requisicaoMudancaDTO.getIdContrato());
					contratoDTO = (ContratoDTO) contratoService.restore(contratoDTO);
					this.contrato = contratoDTO.getNumero();
				} catch (LogicException | ServiceException e) {
					e.printStackTrace();
					this.contrato = null;
				}
			}

			// O DTO de requisi��o de mudan�a n�o tem a capacidade de passar os JRDataSource necess�rios para a impress�o das informa��es complementares, ent�o,
			// Foi criado este objeto reqMudancaJasperDTO para passar os objetos cole��o: JRDataSource.
			this.reqMudancaJasperDTO = new ReqMudancaJasperDTO();
			this.reqMudancaJasperDTO.setIdRequisicaoMudanca(this.requisicaoMudancaDTO.getIdRequisicaoMudanca());
			this.reqMudancaJasperDTO.setTitulo(this.requisicaoMudancaDTO.getTitulo());
			this.reqMudancaJasperDTO.setDataHoraInicio(this.requisicaoMudancaDTO.getDataHoraInicio());
			this.reqMudancaJasperDTO.setDataHoraTermino(this.requisicaoMudancaDTO.getDataHoraTermino());
			this.reqMudancaJasperDTO.setDataHoraConclusao(this.requisicaoMudancaDTO.getDataHoraConclusao());
			this.reqMudancaJasperDTO.setNomeSolicitante(this.requisicaoMudancaDTO.getNomeSolicitante());
			this.reqMudancaJasperDTO.setDescrSituacao(this.requisicaoMudancaDTO.getDescrSituacao());
			this.reqMudancaJasperDTO.setNomeProprietario(this.requisicaoMudancaDTO.getNomeProprietario());
			this.reqMudancaJasperDTO.setTipo(this.requisicaoMudancaDTO.getTipo());
			this.reqMudancaJasperDTO.setNomeGrupoAtual(this.requisicaoMudancaDTO.getNomeGrupoAtual());
			this.reqMudancaJasperDTO.setDescricao(this.requisicaoMudancaDTO.getDescricao());
			this.reqMudancaJasperDTO.setFechamento(this.requisicaoMudancaDTO.getFechamento());
			this.reqMudancaJasperDTO.setRazaoMudanca(this.requisicaoMudancaDTO.getRazaoMudanca());
			this.reqMudancaJasperDTO.setAnaliseImpacto(this.requisicaoMudancaDTO.getAnaliseImpacto());
			this.reqMudancaJasperDTO.setRisco(this.requisicaoMudancaDTO.getRisco());

			// Alimentando as Informa��es Complementares da Mudan�a
			this.buscarVotacoesProposta();
			this.buscarVotacoesReqMudanca();
			this.buscarICsRelacionados();
			this.buscarServicosRelacionados();
			this.buscarProblemasRelacionados();
			this.buscarSolicitacoesRelacionadas();
			this.buscarRiscosRelacionados();
			this.buscarLiberacoesRelacionadas();
			this.buscarPapeisRelacionados();
			this.buscarAnexosRelacionados();
			this.buscarGruposRelacionados();

			ArrayList<ReqMudancaJasperDTO> listaMudanca = new ArrayList<ReqMudancaJasperDTO>();
			listaMudanca.add(this.reqMudancaJasperDTO);
			this.dataSource = new JRBeanCollectionDataSource(listaMudanca);

			// Alimentando os par�metros com informa��es adicionais sobre a impress�o para serem mostradas no relat�rio
			this.alimentaParametros(UtilI18N.internacionaliza(this.request, "requisicaoMudanca.requisicaoMudancaNumero"));

			// Configurando dados para gera��o do Relat�rio
			Date dt = new Date();
			String strMiliSegundos = Long.toString(dt.getTime());
			this.jasperArqRel = "cadastroMudanca";
			this.caminhoJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS");
			this.diretorioTemp = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
			this.diretorioRelativo = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";
			this.arquivoRelatorio = "/" + this.jasperArqRel + strMiliSegundos + "_" + this.usuarioDTO.getIdUsuario();

			// Chamando o relat�rio
			this.abreRelatorioPDF();
		} else {
			this.document.alert(UtilI18N.internacionaliza(this.request, "citcorpore.comum.relatorioVazio"));
		}
	}

	private void alimentaParametros(String titulo) throws ServiceException, Exception {
		HttpSession session = ((HttpServletRequest) this.request).getSession();
		this.parametros = new HashMap<String, Object>();
		this.parametros = UtilRelatorio.trataInternacionalizacaoLocale(session, this.parametros);
		this.parametros.put("TITULO_RELATORIO", titulo);
		this.parametros.put("CIDADE", getCidadeParametrizada(request));
		this.parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
		this.parametros.put("Logo", LogoRel.getFile());
		this.parametros.put("NOME_USUARIO", this.usuarioDTO.getNomeUsuario());
		this.parametros.put("SUBREPORT_DIR", CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS"));
		this.parametros.put("contrato", this.contrato);
	}

    /**
     * M�todo retorna a cidade onde o sistema foi instalado, � um parametro que dece ser configurado. Caso n�o seja informado
     * o mesmo retorna a cidade de Brasilia.
	 * defeito-2287 - AjaxFormAction.java_(#getCidadeParametrizada).
	 *
	 * @since 21/12/2015
	 * @author ibimon.morais
     */
    public String getCidadeParametrizada(final HttpServletRequest request) throws Exception{
    	String cidadeRelatorio = ParametroUtil.getValor(ParametroSistema.CIDADE_LOCALIDADE);
    	if(cidadeRelatorio == null || "".equals(cidadeRelatorio)){
    		cidadeRelatorio = UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioCidade");
    	}
		return cidadeRelatorio;
    }
	
}
