/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.impressao;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.fill.JRAbstractLRUVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import br.com.centralit.citajax.html.DocumentHTML;
import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.LiberacaoMudancaDTO;
import br.com.centralit.citcorpore.bean.LiberacaoProblemaDTO;
import br.com.centralit.citcorpore.bean.PrioridadeDTO;
import br.com.centralit.citcorpore.bean.ReqLiberacaoJasperDTO;
import br.com.centralit.citcorpore.bean.RequisicaoLiberacaoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoLiberacaoItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoLiberacaoMidiaDTO;
import br.com.centralit.citcorpore.bean.RequisicaoLiberacaoRequisicaoComprasDTO;
import br.com.centralit.citcorpore.bean.RequisicaoLiberacaoResponsavelDTO;
import br.com.centralit.citcorpore.bean.UploadDTO;
import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.LiberacaoMudancaService;
import br.com.centralit.citcorpore.negocio.LiberacaoProblemaService;
import br.com.centralit.citcorpore.negocio.PrioridadeService;
import br.com.centralit.citcorpore.negocio.RequisicaoLiberacaoItemConfiguracaoService;
import br.com.centralit.citcorpore.negocio.RequisicaoLiberacaoMidiaService;
import br.com.centralit.citcorpore.negocio.RequisicaoLiberacaoRequisicaoComprasService;
import br.com.centralit.citcorpore.negocio.RequisicaoLiberacaoResponsavelService;
import br.com.centralit.citcorpore.negocio.RequisicaoLiberacaoService;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.LogoRel;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.centralit.citcorpore.util.UtilRelatorio;
import br.com.centralit.citcorpore.util.WebUtil;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citged.bean.ControleGEDDTO;
import br.com.centralit.citged.negocio.ControleGEDService;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilI18N;

/**
 * @author euler.ramos Classe que gera a impress�o do cadastro da Requisi��o de Libera��o
 */
public class ImpressaoCadLiberacao {

	private DocumentHTML document;
	private HttpServletRequest request;
	private ReqLiberacaoJasperDTO reqLiberacaoJasperDTO;
	private RequisicaoLiberacaoDTO requisicaoLiberacaoDTO;
	private UsuarioDTO usuarioDTO;

	private RequisicaoLiberacaoService requisicaoLiberacaoService;

	private JRDataSource dataSource;
	private Map<String, Object> parametros;

	private String arquivoRelatorio;
	private String caminhoJasper;
	private String diretorioRelativo;
	private String diretorioTemp;
	private String jasperArqRel;

	public ImpressaoCadLiberacao(DocumentHTML document, HttpServletRequest request) throws Exception {
		super();
		this.document = document;
		this.request = request;
		this.usuarioDTO = WebUtil.getUsuario(this.request);
		this.requisicaoLiberacaoDTO = (RequisicaoLiberacaoDTO) this.document.getBean();
		this.requisicaoLiberacaoService = (RequisicaoLiberacaoService) ServiceLocator.getInstance().getService(RequisicaoLiberacaoService.class, WebUtil.getUsuarioSistema(this.request));
	}

	public void gerarRelatorio() {
		if (this.usuarioDTO == null) {
			this.document.alert(UtilI18N.internacionaliza(this.request, "citcorpore.comum.sessaoExpirada"));
			this.document.executeScript("window.location = '" + Constantes.getValue("SERVER_ADDRESS") + this.request.getContextPath() + "'");
		} else {
			try {
				if ((this.requisicaoLiberacaoDTO != null) && (this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao() != null)
						&& (this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao().intValue() > 0)) {
					this.geraImpressaoCadLiberacao();
				} else {
					this.document.alert(UtilI18N.internacionaliza(this.request, "requisicaoliberacao.validacao.requisicaoliberacao"));
				}
				this.document.getJanelaPopupById("JANELA_AGUARDE_MENU").hide();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void abreRelatorioPDF() {
		try {
			JRSwapFile arquivoSwap = new JRSwapFile(this.diretorioTemp, 4096, 25);
			JRAbstractLRUVirtualizer virtualizer = new JRSwapFileVirtualizer(25, arquivoSwap, true);
			this.parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
			JasperPrint print = JasperFillManager.fillReport(this.caminhoJasper + this.jasperArqRel + ".jasper", this.parametros, this.dataSource);

			JasperExportManager.exportReportToPdfFile(print, this.diretorioTemp + this.arquivoRelatorio + ".pdf");

			this.document.executeScript("window.open('" + Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/printPDF/printPDF.jsp?url=" + this.diretorioRelativo
					+ this.arquivoRelatorio + ".pdf')");
		} catch (OutOfMemoryError e) {
			this.document.alert(UtilI18N.internacionaliza(this.request, "citcorpore.erro.erroServidor"));
		} catch (JRException e) {
			e.printStackTrace();
		}
	}

	private String buscarNumeroContrato() {
		if ((this.requisicaoLiberacaoDTO.getIdContrato() != null) && (this.requisicaoLiberacaoDTO.getIdContrato().intValue() > 0)) {
			try {
				ContratoService contratoService = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
				ContratoDTO contratoDTO = new ContratoDTO();
				contratoDTO.setIdContrato(this.requisicaoLiberacaoDTO.getIdContrato());
				contratoDTO = (ContratoDTO) contratoService.restore(contratoDTO);
				return contratoDTO.getNumero();
			} catch (LogicException | ServiceException e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	private String buscarNomePrioridade() {

		if ((this.requisicaoLiberacaoDTO.getPrioridade() != null) && (this.requisicaoLiberacaoDTO.getPrioridade().intValue() > 0)) {
			try {
				PrioridadeService prioridadeService = (PrioridadeService) ServiceLocator.getInstance().getService(PrioridadeService.class, null);
				PrioridadeDTO prioridadeDTO = new PrioridadeDTO();
				prioridadeDTO.setIdPrioridade(this.requisicaoLiberacaoDTO.getPrioridade());
				prioridadeDTO = (PrioridadeDTO) prioridadeService.restore(prioridadeDTO);
				return prioridadeDTO.getNomePrioridade();
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			} 
		}
		return null;
	}

	private void buscarMudancasRelacionadas() throws Exception {
		LiberacaoMudancaService liberacaoMudancaService = (LiberacaoMudancaService) ServiceLocator.getInstance().getService(LiberacaoMudancaService.class, null);
		@SuppressWarnings("unchecked")
		Collection<LiberacaoMudancaDTO> listaMudancas = liberacaoMudancaService.findByIdRequisicaoMudanca(this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao(), null);
		JRDataSource dsMudancas = new JRBeanCollectionDataSource(listaMudancas);
		this.reqLiberacaoJasperDTO.setListaMudancas(dsMudancas);
	}

	private void buscarICsRelacionados() throws ServiceException, Exception {
		RequisicaoLiberacaoItemConfiguracaoService requisicaoLiberacaoItemConfiguracaoService = (RequisicaoLiberacaoItemConfiguracaoService) ServiceLocator.getInstance().getService(
				RequisicaoLiberacaoItemConfiguracaoService.class, null);
		RequisicaoLiberacaoItemConfiguracaoDTO requisicaoLiberacaoICDTO = new RequisicaoLiberacaoItemConfiguracaoDTO();
		requisicaoLiberacaoICDTO.setIdRequisicaoLiberacao(this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao());
		@SuppressWarnings("unchecked")
		Collection<RequisicaoLiberacaoItemConfiguracaoDTO> listaICs = requisicaoLiberacaoItemConfiguracaoService.findByIdRequisicaoLiberacao(requisicaoLiberacaoICDTO.getIdRequisicaoLiberacao());
		JRDataSource dsICs = new JRBeanCollectionDataSource(listaICs);
		this.reqLiberacaoJasperDTO.setListaICs(dsICs);
	}

	private void buscarProblemasRelacionados() throws ServiceException, Exception {
		LiberacaoProblemaService liberacaoProblemaService = (LiberacaoProblemaService) ServiceLocator.getInstance().getService(LiberacaoProblemaService.class, WebUtil.getUsuarioSistema(request));
		@SuppressWarnings("unchecked")
		Collection<LiberacaoProblemaDTO> listaProblemas = liberacaoProblemaService.findByIdLiberacao(this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao());
		JRDataSource dsProblemas = new JRBeanCollectionDataSource(listaProblemas);
		this.reqLiberacaoJasperDTO.setListaProblemas(dsProblemas);
	}

	private void buscarDocumentosLegaisRelacionados() throws Exception {
		ControleGEDService controleGedService = (ControleGEDService) ServiceLocator.getInstance().getService(ControleGEDService.class, null);
		@SuppressWarnings("rawtypes")
		Collection listaDocumentosLegaisCript = controleGedService.listByIdTabelaAndIdLiberacaoAndLigacao(ControleGEDDTO.TABELA_DOCSLEGAIS_REQUISICAOLIBERACAO,
				this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao());
		@SuppressWarnings("unchecked")
		Collection<UploadDTO> listaDocumentosLegais = controleGedService.convertListControleGEDToUploadDTO(listaDocumentosLegaisCript);
		JRDataSource dsDocumentosLegais = new JRBeanCollectionDataSource(listaDocumentosLegais);
		this.reqLiberacaoJasperDTO.setListaDocumentosLegais(dsDocumentosLegais);
	}

	private void buscarMidiasDefinitivasRelacionadas() throws Exception {
		RequisicaoLiberacaoMidiaService requisicaoLiberacaoMidiaService = (RequisicaoLiberacaoMidiaService) ServiceLocator.getInstance().getService(RequisicaoLiberacaoMidiaService.class,
				WebUtil.getUsuarioSistema(request));
		@SuppressWarnings("unchecked")
		Collection<RequisicaoLiberacaoMidiaDTO> listaRequisicaoLiberacaoMidia = requisicaoLiberacaoMidiaService.findByIdLiberacaoEDataFim(this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao());
		JRDataSource dsMidiasDefinitivas = new JRBeanCollectionDataSource(listaRequisicaoLiberacaoMidia);
		this.reqLiberacaoJasperDTO.setListaMidiasDefinitivas(dsMidiasDefinitivas);
	}

	private void buscarDocumentosGeraisRelacionados() throws Exception {
		ControleGEDService controleGedService = (ControleGEDService) ServiceLocator.getInstance().getService(ControleGEDService.class, null);
		@SuppressWarnings("rawtypes")
		Collection listaDocumentosGeraisCript = controleGedService.listByIdTabelaAndIdLiberacaoAndLigacao(ControleGEDDTO.TABELA_DOCSGERAIS_REQUISICAOLIBERACAO,
				this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao());
		@SuppressWarnings("unchecked")
		Collection<UploadDTO> listaDocumentosGerais = controleGedService.convertListControleGEDToUploadDTO(listaDocumentosGeraisCript);
		JRDataSource dsDocumentosGerais = new JRBeanCollectionDataSource(listaDocumentosGerais);
		this.reqLiberacaoJasperDTO.setListaDocumentosGerais(dsDocumentosGerais);
	}

	private void buscarPapeisRelacionados() throws Exception {
		RequisicaoLiberacaoResponsavelService liberacaoResponsavelService = (RequisicaoLiberacaoResponsavelService) ServiceLocator.getInstance().getService(
				RequisicaoLiberacaoResponsavelService.class, WebUtil.getUsuarioSistema(request));
		@SuppressWarnings("unchecked")
		Collection<RequisicaoLiberacaoResponsavelDTO> listaPapeis = liberacaoResponsavelService.findByIdLiberacaoEDataFim(this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao());
		JRDataSource dsPapeis = new JRBeanCollectionDataSource(listaPapeis);
		this.reqLiberacaoJasperDTO.setListaPapeis(dsPapeis);
	}

	private void buscarRequisicoesComprasRelacionadas() throws Exception {
		RequisicaoLiberacaoRequisicaoComprasService liberacaoPedidoComprasService = (RequisicaoLiberacaoRequisicaoComprasService) ServiceLocator.getInstance().getService(
				RequisicaoLiberacaoRequisicaoComprasService.class, WebUtil.getUsuarioSistema(request));
		@SuppressWarnings("unchecked")
		Collection<RequisicaoLiberacaoRequisicaoComprasDTO> listaCompras = liberacaoPedidoComprasService.findByIdLiberacaoAndDataFim(this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao());
		JRDataSource dsCompras = new JRBeanCollectionDataSource(listaCompras);
		this.reqLiberacaoJasperDTO.setListaCompras(dsCompras);
	}

	private void geraImpressaoCadLiberacao() throws ServiceException, Exception {
		// Obtendo informa��es da mudan�a
		this.requisicaoLiberacaoDTO = this.requisicaoLiberacaoService.restoreAll(this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao());

		if ((this.requisicaoLiberacaoDTO != null) && (this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao() != null) && (this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao() >= 0)) {

			// O DTO de requisi��o de libera��o n�o tem a capacidade de passar os JRDataSource necess�rios para a impress�o das informa��es complementares, ent�o,
			// Foi criado este objeto reqLiberacaoJasperDTO para passar os objetos cole��o: JRDataSource.
			this.reqLiberacaoJasperDTO = new ReqLiberacaoJasperDTO();
			this.reqLiberacaoJasperDTO.setIdRequisicaoLiberacao(this.requisicaoLiberacaoDTO.getIdRequisicaoLiberacao());
			this.reqLiberacaoJasperDTO.setTitulo(this.requisicaoLiberacaoDTO.getTitulo());
			this.reqLiberacaoJasperDTO.setNumeroContrato(this.buscarNumeroContrato());
			this.reqLiberacaoJasperDTO.setDataHoraInicio(this.requisicaoLiberacaoDTO.getDataHoraInicio());
			this.reqLiberacaoJasperDTO.setDataHoraTermino(this.requisicaoLiberacaoDTO.getDataHoraTermino());
			this.reqLiberacaoJasperDTO.setDataHoraInicioAgendada(this.requisicaoLiberacaoDTO.getDataHoraInicioAgendada());
			this.reqLiberacaoJasperDTO.setDataHoraTerminoAgendada(this.requisicaoLiberacaoDTO.getDataHoraTerminoAgendada());
			this.reqLiberacaoJasperDTO.setDataHoraCaptura(this.requisicaoLiberacaoDTO.getDataHoraCaptura());
			this.reqLiberacaoJasperDTO.setDataHoraConclusao(this.requisicaoLiberacaoDTO.getDataHoraConclusao());
			this.reqLiberacaoJasperDTO.setNomeSolicitante(this.requisicaoLiberacaoDTO.getNomeSolicitante());
			this.reqLiberacaoJasperDTO.setDescrSituacao(this.requisicaoLiberacaoDTO.getDescrSituacao());
			this.reqLiberacaoJasperDTO.setNomeProprietario(this.requisicaoLiberacaoDTO.getNomeProprietario());
			this.reqLiberacaoJasperDTO.setTipo(this.requisicaoLiberacaoDTO.getTipo());
			this.reqLiberacaoJasperDTO.setNomeGrupoAtual(this.requisicaoLiberacaoDTO.getNomeGrupoAtual());
			this.reqLiberacaoJasperDTO.setDescricao(this.requisicaoLiberacaoDTO.getDescricao());
			this.reqLiberacaoJasperDTO.setFechamento(this.requisicaoLiberacaoDTO.getFechamento());
			this.reqLiberacaoJasperDTO.setRisco(this.requisicaoLiberacaoDTO.getRisco());
			this.reqLiberacaoJasperDTO.setNomePrioridade(this.buscarNomePrioridade());

			// Alimentando as Informa��es Complementares da Liberacao
			this.buscarMudancasRelacionadas();
			this.buscarICsRelacionados();
			this.buscarProblemasRelacionados();
			this.buscarDocumentosLegaisRelacionados();
			this.buscarMidiasDefinitivasRelacionadas();
			this.buscarDocumentosGeraisRelacionados();
			this.buscarPapeisRelacionados();
			this.buscarRequisicoesComprasRelacionadas();

			ArrayList<ReqLiberacaoJasperDTO> listaLiberacao = new ArrayList<ReqLiberacaoJasperDTO>();
			listaLiberacao.add(this.reqLiberacaoJasperDTO);
			this.dataSource = new JRBeanCollectionDataSource(listaLiberacao);

			// Alimentando os par�metros com informa��es adicionais sobre a impress�o para serem mostradas no relat�rio
			this.alimentaParametros(UtilI18N.internacionaliza(this.request, "requisicaoLiberacao.requisicaoLiberacaoNumero"));

			// Configurando dados para gera��o do Relat�rio
			Date dt = new Date();
			String strMiliSegundos = Long.toString(dt.getTime());
			this.jasperArqRel = "cadastroLiberacao";
			this.caminhoJasper = CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS");
			this.diretorioTemp = CITCorporeUtil.CAMINHO_REAL_APP + "/tempFiles";
			this.diretorioRelativo = Constantes.getValue("SERVER_ADDRESS") + Constantes.getValue("CONTEXTO_APLICACAO") + "/tempFiles";
			this.arquivoRelatorio = "/" + this.jasperArqRel + strMiliSegundos + "_" + this.usuarioDTO.getIdUsuario();

			// Chamando o relat�rio
			this.abreRelatorioPDF();
		} else {
			this.document.alert(UtilI18N.internacionaliza(this.request, "citcorpore.comum.relatorioVazio"));
		}
	}

	private void alimentaParametros(String titulo) throws ServiceException, Exception {
		HttpSession session = ((HttpServletRequest) this.request).getSession();
		this.parametros = new HashMap<String, Object>();
		this.parametros = UtilRelatorio.trataInternacionalizacaoLocale(session, this.parametros);
		this.parametros.put("TITULO_RELATORIO", titulo);
		this.parametros.put("CIDADE", getCidadeParametrizada(this.request));
		this.parametros.put("DATA_HORA", UtilDatas.getDataHoraAtual());
		this.parametros.put("Logo", LogoRel.getFile());
		this.parametros.put("NOME_USUARIO", this.usuarioDTO.getNomeUsuario());
		this.parametros.put("SUBREPORT_DIR", CITCorporeUtil.CAMINHO_REAL_APP + Constantes.getValue("CAMINHO_RELATORIOS"));
	}

    /**
     * M�todo retorna a cidade onde o sistema foi instalado, � um parametro que dece ser configurado. Caso n�o seja informado
     * o mesmo retorna a cidade de Brasilia.
	 * defeito-2287 - AjaxFormAction.java_(#getCidadeParametrizada).
	 *
	 * @since 21/12/2015
	 * @author ibimon.morais
     */
    public String getCidadeParametrizada(final HttpServletRequest request) throws Exception{
    	String cidadeRelatorio = ParametroUtil.getValor(ParametroSistema.CIDADE_LOCALIDADE);
    	if(cidadeRelatorio == null || "".equals(cidadeRelatorio)){
    		cidadeRelatorio = UtilI18N.internacionaliza(request, "citcorpore.comum.relatorioCidade");
    	}
		return cidadeRelatorio;
    }
	
}
