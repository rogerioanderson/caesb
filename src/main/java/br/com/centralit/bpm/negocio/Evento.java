/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.negocio;

import java.util.List;
import java.util.Map;

import br.com.centralit.bpm.dto.ItemTrabalhoFluxoDTO;
import br.com.centralit.bpm.integracao.ItemTrabalhoFluxoDao;
import br.com.centralit.bpm.util.Enumerados;
import br.com.citframework.util.UtilDatas;

public class Evento extends ItemTrabalho {

	@Override
	public List<ItemTrabalho> resolve() throws Exception {
		registra();
		return null;
	}
	
	@Override
	public void executa(String loginUsuario, Map<String, Object> objetos) throws Exception {
		ItemTrabalhoFluxoDao itemTrabalhoFluxoDao = new ItemTrabalhoFluxoDao();
		setTransacaoDao(itemTrabalhoFluxoDao);
		
		itemTrabalhoDto.setDataHoraExecucao(UtilDatas.getDataHoraAtual());
        itemTrabalhoFluxoDao.update(itemTrabalhoDto);
	}
	
	@Override
	public void registra() throws Exception {
		if (existe())
			return;
		
		ItemTrabalhoFluxoDao itemTrabalhoDao = new ItemTrabalhoFluxoDao();
		setTransacaoDao(itemTrabalhoDao);
		
		itemTrabalhoDto = new ItemTrabalhoFluxoDTO();
		itemTrabalhoDto.setIdElemento(elementoFluxoDto.getIdElemento());
		itemTrabalhoDto.setIdInstancia(instanciaFluxo.getIdInstancia());
		itemTrabalhoDto.setDataHoraCriacao(UtilDatas.getDataHoraAtual());
		itemTrabalhoDto.setSituacao(Enumerados.SituacaoItemTrabalho.Criado.name());
		itemTrabalhoDto = (ItemTrabalhoFluxoDTO) itemTrabalhoDao.create(itemTrabalhoDto);
	}

	
	@Override
	public boolean executavel() {
		return true;
	}	
	
	@Override
	public boolean resolvido() {
	    return itemTrabalhoDto.getDataHoraExecucao() != null;
	}
}
