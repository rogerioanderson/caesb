/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.negocio;

import br.com.centralit.bpm.config.Config;
import br.com.centralit.bpm.dto.FluxoDTO;
import br.com.centralit.bpm.dto.GrupoBpmDTO;
import br.com.centralit.bpm.dto.PermissoesFluxoDTO;


public class PermissoesFluxo implements IPermissoesFluxo {
	
	private IPermissoesFluxo singleton;

	private IPermissoesFluxo getInstance() throws Exception {
		if (singleton == null) {
			String classe = Config.getPropriedade("classe.permissoes.fluxo");
			try {
				singleton = (IPermissoesFluxo) Class.forName(classe).newInstance();
			} catch (Exception e) {
				throw new Exception("Classe de configura��o de permiss�es de fluxo encontrada");
			}
		}
		return singleton;
	}

	@Override
	public PermissoesFluxoDTO getPermissoesFluxo(GrupoBpmDTO grupoDto, FluxoDTO fluxoDto) throws Exception {
		return getInstance().getPermissoesFluxo(grupoDto, fluxoDto);
	}
	
}
