/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.negocio;

import br.com.centralit.bpm.dto.AtribuicaoFluxoDTO;
import br.com.centralit.bpm.dto.GrupoBpmDTO;
import br.com.centralit.bpm.dto.UsuarioBpmDTO;
import br.com.centralit.bpm.integracao.AtribuicaoFluxoDao;
import br.com.centralit.bpm.util.Enumerados;
import br.com.centralit.bpm.util.Enumerados.TipoAtribuicao;
import br.com.citframework.util.UtilDatas;


public class AtribuicaoFluxo extends NegocioBpm {
	
	protected ItemTrabalho itemTrabalho;

	public AtribuicaoFluxo (ItemTrabalho itemTrabalho) {
		this.itemTrabalho = itemTrabalho;
		this.setTransacao(itemTrabalho.getInstanciaFluxo().getTransacao());
	}
	
	public AtribuicaoFluxoDTO registraAtribuicao(UsuarioBpmDTO usuario, GrupoBpmDTO grupo, TipoAtribuicao tipoAtribuicao) throws Exception {
		AtribuicaoFluxoDao atribuicaoFluxoDao = new AtribuicaoFluxoDao();
		setTransacaoDao(atribuicaoFluxoDao);
		AtribuicaoFluxoDTO atribuicaoFluxoDto = new AtribuicaoFluxoDTO();
		atribuicaoFluxoDto.setIdItemTrabalho(itemTrabalho.getIdItemTrabalho());
		atribuicaoFluxoDto.setTipo(tipoAtribuicao.name());
		atribuicaoFluxoDto.setDataHora(UtilDatas.getDataHoraAtual());
		if (grupo != null)
			atribuicaoFluxoDto.setIdGrupo(grupo.getIdGrupo());
		if (usuario != null)
			atribuicaoFluxoDto.setIdUsuario(usuario.getIdUsuario());
		return (AtribuicaoFluxoDTO) atribuicaoFluxoDao.create(atribuicaoFluxoDto);		
	}

	public AtribuicaoFluxoDTO registraDelegacao(UsuarioBpmDTO responsavel, UsuarioBpmDTO usuario, GrupoBpmDTO grupo) throws Exception {
		AtribuicaoFluxoDao atribuicaoFluxoDao = new AtribuicaoFluxoDao();
		setTransacaoDao(atribuicaoFluxoDao);
		atribuicaoFluxoDao.deleteDelegacao(itemTrabalho.getIdItemTrabalho());
		
		new HistoricoItemTrabalho(itemTrabalho).registraDelegacao(responsavel, usuario, grupo);
		
		AtribuicaoFluxoDTO atribuicaoFluxoDto = new AtribuicaoFluxoDTO();
		atribuicaoFluxoDto.setIdItemTrabalho(itemTrabalho.getIdItemTrabalho());
		atribuicaoFluxoDto.setTipo(Enumerados.TipoAtribuicao.Delegacao.name());
		atribuicaoFluxoDto.setDataHora(UtilDatas.getDataHoraAtual());
		if (grupo != null)
			atribuicaoFluxoDto.setIdGrupo(grupo.getIdGrupo());
		if (usuario != null)
			atribuicaoFluxoDto.setIdUsuario(usuario.getIdUsuario());
		return (AtribuicaoFluxoDTO) atribuicaoFluxoDao.create(atribuicaoFluxoDto);		
	}
}
