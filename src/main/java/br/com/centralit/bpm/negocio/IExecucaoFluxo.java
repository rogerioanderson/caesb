/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.negocio;

import java.util.HashMap;
import java.util.Map;

import br.com.centralit.bpm.dto.EventoFluxoDTO;
import br.com.centralit.bpm.dto.FluxoDTO;
import br.com.centralit.bpm.dto.ObjetoNegocioFluxoDTO;

public interface IExecucaoFluxo {
	
	public InstanciaFluxo inicia() throws Exception;
	public InstanciaFluxo inicia(String nomeFluxo, Integer idFase) throws Exception;
	public InstanciaFluxo inicia(FluxoDTO fluxoDto, Integer idFase) throws Exception;
	public void executa(String loginUsuario, ObjetoNegocioFluxoDTO objetoNegocioDto, Integer idItemTrabalho, String acao, HashMap<String, Object> map) throws Exception;
	public void mapObjetoNegocio(Map<String, Object> map) throws Exception;

	public void encerra() throws Exception;
	public void reabre(String loginUsuario) throws Exception;
	public void suspende(String loginUsuario) throws Exception;
	public void reativa(String loginUsuario) throws Exception;
	
	public void enviaEmail(Integer idModeloEmail) throws Exception;
	public void enviaEmail(String identificador) throws Exception;
	public void enviaEmail(String identificador, String[] destinatarios) throws Exception;
	
	public void delega(String loginUsuario, ObjetoNegocioFluxoDTO objetoNegocioDto, Integer idItemTrabalho, String usuarioDestino, String grupoDestino) throws Exception;
	public void direcionaAtendimento(String loginUsuario, ObjetoNegocioFluxoDTO objetoNegocioDto, String grupoAtendimento) throws Exception;
    public void executaEvento(EventoFluxoDTO eventoFluxoDto) throws Exception;
    
    public void validaEncerramento() throws Exception;
    public void atribuiAcompanhamento(ItemTrabalho itemTrabalho, String usuario, String grupo) throws Exception;
    public void verificaSLA(ItemTrabalho itemTrabalho) throws Exception;
}
