/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.negocio;

import java.util.List;

import br.com.centralit.bpm.dto.GrupoBpmDTO;
import br.com.centralit.bpm.dto.UsuarioBpmDTO;
import br.com.citframework.integracao.TransactionControler;

public interface IUsuarioGrupo {
	
	public UsuarioBpmDTO recuperaUsuario(Integer idUsuario) throws Exception;
	public UsuarioBpmDTO recuperaUsuario(String login) throws Exception;
	public GrupoBpmDTO recuperaGrupo(Integer idGrupo) throws Exception;
	public GrupoBpmDTO recuperaGrupo(String siglaGrupo) throws Exception;

	public boolean existeUsuario(String login) throws Exception;
	public boolean existeGrupo(String siglaGrupo) throws Exception;
	public List<GrupoBpmDTO> getGruposDoUsuario(String login) throws Exception;
	
	public boolean pertenceAoGrupo(String login, String siglaGrupo) throws Exception;
	public void setTransacao(TransactionControler transacao) throws Exception;
	
}
