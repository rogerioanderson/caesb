/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import br.com.centralit.citajax.util.CitAjaxUtil;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.UtilTratamentoArquivos;

import com.google.gson.Gson;


public class Config {
	
	private static Properties props = null;
	private static final String fileName = "bpm.properties";
	private static final String designJson = "bpm_design.json";
	
	private static final String prefixoClasseDto = "br.com.centralit.bpm.dto.ElementoFluxo";
	private static final String prefixoClasseNegocio = "br.com.centralit.bpm.negocio.";
	
	public static InputStream inputStreamSettedInLoad = null;
	
	private static Design design = null;
	
	static{
		
		props = new Properties();
		ClassLoader load = Constantes.class.getClassLoader();

		InputStream is = load.getResourceAsStream(fileName);		
		if (is == null){
			is = ClassLoader.getSystemResourceAsStream(fileName);
		}
		if (is == null){
			is = ClassLoader.getSystemClassLoader().getResourceAsStream(fileName);
		}	
		
		//InputStream is = ClassLoader.getSystemResourceAsStream(fileName);
		try {
			if (is == null){
				is = inputStreamSettedInLoad;
			}
			if (is == null){
				throw new Exception("Arquivo de recursos nao encontrado: " + fileName);
			}
			props.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static Properties getProps() {
//		if (props == null){
//			InputStream is = inputStreamSettedInLoad;
//			if (is != null){
//				try {
//					props.load(is);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}	
//			if (props == null){ //Se ainda continuar nulo.
//				return null;
//			}
//		}
		
		if(props == null){
			return null;
		}
		
		return props;
	}
	
	public static String getPropriedade(String chave) {
		return getProps().getProperty(chave);
	}
	
	public static String getClasseDtoElemento(String tipoElemento) {
		String result = getProps().getProperty("classe.dto.elemento."+tipoElemento.toLowerCase());
		if (result == null)
			result = prefixoClasseDto + tipoElemento + "DTO";
		return result;
	}

	public static String getClasseNegocioElemento(String tipoElemento) {
		String result = getProps().getProperty("classe.negocio.elemento."+tipoElemento.toLowerCase());
		if (result == null)
			result = prefixoClasseNegocio + tipoElemento;
		return result;
	}

	public static Design getRender() {
		try {
			String propriedades = UtilTratamentoArquivos.getStringTextFromFileTxt(CitAjaxUtil.CAMINHO_REAL_APP + "/WEB-INF/" + designJson);		
			design = new Gson().fromJson(propriedades, Design.class);
			design.configuraElementos();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return design;
	}

}
