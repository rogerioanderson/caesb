/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.centralit.bpm.dto.ElementoFluxoDTO;
import br.com.centralit.bpm.dto.PropriedadeElementoDTO;
import br.com.citframework.util.Reflexao;

public class Design {

    private List<PropriedadeElementoDTO> propriedades;
    private List<ElementoFluxoDTO> elementos;

    public List<PropriedadeElementoDTO> getPropriedades() {
        return propriedades;
    }

    public void setPropriedades(final List<PropriedadeElementoDTO> propriedades) {
        this.propriedades = propriedades;
    }

    public PropriedadeElementoDTO getPropriedade(final String id) {
        if (propriedades == null) {
            return null;
        }

        PropriedadeElementoDTO result = null;
        for (final PropriedadeElementoDTO propriedadeDto : propriedades) {
            if (propriedadeDto.getId().equalsIgnoreCase(id)) {
                result = propriedadeDto;
                break;
            }
        }
        return result;
    }

    public ElementoFluxoDTO getElemento(final String tipo) {
        if (elementos == null) {
            return null;
        }

        ElementoFluxoDTO result = null;
        for (final ElementoFluxoDTO elementoDto : elementos) {
            if (elementoDto.getTipoElemento().equalsIgnoreCase(tipo)) {
                result = elementoDto;
                break;
            }
        }
        return result;
    }

    public List<ElementoFluxoDTO> getElementos() {
        return elementos;
    }

    public void setElementos(final List<ElementoFluxoDTO> elementos) {
        this.elementos = elementos;
    }

    public void configuraElementos() {
        if (propriedades == null || elementos == null) {
            return;
        }

        final HashMap<String, PropriedadeElementoDTO> mapProp = new HashMap<>();
        for (final PropriedadeElementoDTO propriedade : propriedades) {
            if (propriedade != null) {
                if (propriedade.getValorDefault() == null) {
                    propriedade.setValorDefault("");
                }
                mapProp.put(propriedade.getId(), propriedade);
            }
        }

        for (final ElementoFluxoDTO elemento : elementos) {
            if (elemento != null && elemento.getLstPropriedades() != null) {
                final List<PropriedadeElementoDTO> lst = new ArrayList<>();
                for (int i = 0; i < elemento.getLstPropriedades().length; i++) {
                    if (mapProp.get(elemento.getLstPropriedades()[i]) != null) {
                        final PropriedadeElementoDTO propriedade = mapProp.get(elemento.getLstPropriedades()[i]);
                        lst.add(propriedade);
                        try {
                            Reflexao.setPropertyValue(elemento, elemento.getLstPropriedades()[i], propriedade.getValorDefault());
                        } catch (final Exception e) {}
                    }
                }
                elemento.setPropriedades(lst);
            }
        }
    }

}
