/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.servico;

import java.util.Collection;

import br.com.centralit.bpm.dto.TipoFluxoDTO;
import br.com.centralit.bpm.integracao.TipoFluxoDao;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudServiceImpl;

public class TipoFluxoServiceEjb extends CrudServiceImpl implements TipoFluxoService {

    private TipoFluxoDao tipoFluxoDao;

    @Override
    protected TipoFluxoDao getDao() {
        if (tipoFluxoDao == null) {
            tipoFluxoDao = new TipoFluxoDao();
        }
        return tipoFluxoDao;
    }

    @Override
    protected void validaCreate(final Object arg0) throws Exception {
        final TipoFluxoDTO tipoFluxoDto = (TipoFluxoDTO) arg0;
        final TipoFluxoDTO tipoFluxoAuxDto = this.getDao().findByNome(tipoFluxoDto.getNomeFluxo());
        if (tipoFluxoAuxDto != null) {
            throw new ServiceException("J� existe um tipo de fluxo com esse nome.");
        }
    }

    @Override
    protected void validaUpdate(final Object arg0) throws Exception {
        final TipoFluxoDTO tipoFluxoDto = (TipoFluxoDTO) arg0;
        final TipoFluxoDTO tipoFluxoAuxDto = this.getDao().findByNome(tipoFluxoDto.getNomeFluxo());

        if (tipoFluxoAuxDto != null && tipoFluxoAuxDto.getIdTipoFluxo().intValue() != tipoFluxoDto.getIdTipoFluxo().intValue()) {
            throw new ServiceException("J� existe um tipo de fluxo com esse nome.");
        }
    }

    @Override
    public Collection findByIdProcessoNegocio(final Integer idProcessoNegocio) throws Exception {
        return this.getDao().findByIdProcessoNegocio(idProcessoNegocio);
    }
}
