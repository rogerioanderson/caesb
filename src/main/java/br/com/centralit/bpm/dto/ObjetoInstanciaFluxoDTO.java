/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.dto;

import br.com.citframework.dto.IDto;

public class ObjetoInstanciaFluxoDTO implements IDto {

    private static final long serialVersionUID = -3401382998327232131L;

    private Integer idObjetoInstancia;
    private Integer idInstancia;
    private Integer idItemTrabalho;
    private Integer idObjetoNegocio;
    private String nomeObjeto;
    private String nomeClasse;
    private String tipoAssociacao;
    private String campoChave;
    private String objetoPrincipal;
    private String nomeTabelaBD;
    private String nomeCampoBD;
    private String tipoCampoBD;
    private String valor;

    public Integer getIdObjetoInstancia() {
        return idObjetoInstancia;
    }

    public void setIdObjetoInstancia(final Integer idObjetoInstancia) {
        this.idObjetoInstancia = idObjetoInstancia;
    }

    public Integer getIdInstancia() {
        return idInstancia;
    }

    public void setIdInstancia(final Integer idInstancia) {
        this.idInstancia = idInstancia;
    }

    public Integer getIdItemTrabalho() {
        return idItemTrabalho;
    }

    public void setIdItemTrabalho(final Integer idItemTrabalho) {
        this.idItemTrabalho = idItemTrabalho;
    }

    public Integer getIdObjetoNegocio() {
        return idObjetoNegocio;
    }

    public void setIdObjetoNegocio(final Integer idObjetoNegocio) {
        this.idObjetoNegocio = idObjetoNegocio;
    }

    public String getTipoAssociacao() {
        return tipoAssociacao;
    }

    public void setTipoAssociacao(final String tipoAssociacao) {
        this.tipoAssociacao = tipoAssociacao;
    }

    public String getNomeClasse() {
        return nomeClasse;
    }

    public void setNomeClasse(final String nomeClasse) {
        this.nomeClasse = nomeClasse;
    }

    public String getCampoChave() {
        return campoChave;
    }

    public void setCampoChave(final String campoChave) {
        this.campoChave = campoChave;
    }

    public String getObjetoPrincipal() {
        return objetoPrincipal;
    }

    public void setObjetoPrincipal(final String objetoPrincipal) {
        this.objetoPrincipal = objetoPrincipal;
    }

    public String getNomeTabelaBD() {
        return nomeTabelaBD;
    }

    public void setNomeTabelaBD(final String nomeTabelaBD) {
        this.nomeTabelaBD = nomeTabelaBD;
    }

    public String getNomeObjeto() {
        return nomeObjeto;
    }

    public void setNomeObjeto(final String nomeObjeto) {
        this.nomeObjeto = nomeObjeto;
    }

    public String getNomeCampoBD() {
        return nomeCampoBD;
    }

    public void setNomeCampoBD(final String nomeCampoBD) {
        this.nomeCampoBD = nomeCampoBD;
    }

    public String getTipoCampoBD() {
        return tipoCampoBD;
    }

    public void setTipoCampoBD(final String tipoCampoBD) {
        this.tipoCampoBD = tipoCampoBD;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(final String valor) {
        this.valor = valor;
    }

}
