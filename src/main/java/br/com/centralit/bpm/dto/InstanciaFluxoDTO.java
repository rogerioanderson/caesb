/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.dto;

import java.sql.Timestamp;
import java.util.List;

import br.com.citframework.dto.IDto;

public class InstanciaFluxoDTO implements IDto {

    private static final long serialVersionUID = -7198923723510246056L;

    private Integer idInstancia;
    private Integer idFluxo;
    private Timestamp dataHoraCriacao;
    private String situacao;
    private Timestamp dataHoraFinalizacao;
    private List<ObjetoInstanciaFluxoDTO> colObjetos;
    private FluxoDTO fluxoDto;

    public Integer getIdInstancia() {
        return idInstancia;
    }

    public void setIdInstancia(final Integer idInstancia) {
        this.idInstancia = idInstancia;
    }

    public Integer getIdFluxo() {
        return idFluxo;
    }

    public void setIdFluxo(final Integer parm) {
        idFluxo = parm;
    }

    public Timestamp getDataHoraCriacao() {
        return dataHoraCriacao;
    }

    public void setDataHoraCriacao(final Timestamp parm) {
        dataHoraCriacao = parm;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(final String parm) {
        situacao = parm;
    }

    public Timestamp getDataHoraFinalizacao() {
        return dataHoraFinalizacao;
    }

    public void setDataHoraFinalizacao(final Timestamp parm) {
        dataHoraFinalizacao = parm;
    }

    public FluxoDTO getFluxoDto() {
        return fluxoDto;
    }

    public void setFluxoDto(final FluxoDTO fluxoDto) {
        this.fluxoDto = fluxoDto;
    }

    public List<ObjetoInstanciaFluxoDTO> getColObjetos() {
        return colObjetos;
    }

    public void setColObjetos(final List<ObjetoInstanciaFluxoDTO> colObjetos) {
        this.colObjetos = colObjetos;
    }
}
