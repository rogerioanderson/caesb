/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.dto;

public class ElementoFluxoEmailDTO extends ElementoFluxoDTO {

    private static final long serialVersionUID = 7072776670004568782L;

    private String[] colGrupos;
    private String[] colUsuarios;
    private String[] colDestinatarios;

    @Override
    public void setGrupos(final String grupos) {
        if (grupos != null) {
            colGrupos = grupos.split(";");
        } else {
            colGrupos = new String[] {};
        }
        this.grupos = grupos;
    }

    @Override
    public void setUsuarios(final String usuarios) {
        if (usuarios != null) {
            colUsuarios = usuarios.split(";");
        } else {
            colUsuarios = new String[] {};
        }
        this.usuarios = usuarios;
    }

    public void setColGrupos(final String[] colGrupos) {
        grupos = "";
        if (colGrupos != null && colGrupos.length > 0) {
            for (int i = 0; i < colGrupos.length; i++) {
                if (i > 0) {
                    grupos += ";";
                }
                grupos += colGrupos[i];
            }
        }
        this.colGrupos = colGrupos;
    }

    public void setColUsuarios(final String[] colUsuarios) {
        usuarios = "";
        if (colUsuarios != null && colUsuarios.length > 0) {
            for (int i = 0; i < colUsuarios.length; i++) {
                if (i > 0) {
                    usuarios += ";";
                }
                usuarios += colUsuarios[i];
            }
        }
        this.colUsuarios = colUsuarios;
    }

    public String[] getColGrupos() {
        return colGrupos;
    }

    public String[] getColUsuarios() {
        return colUsuarios;
    }

    @Override
    public void setDestinatariosEmail(final String destinatariosEmail) {
        if (destinatariosEmail != null) {
            colDestinatarios = destinatariosEmail.split(";");
        } else {
            colDestinatarios = new String[] {};
        }
        this.destinatariosEmail = destinatariosEmail;
    }

    public String[] getColDestinatarios() {
        return colDestinatarios;
    }

    public void setColDestinatarios(final String[] colDestinatarios) {
        destinatariosEmail = "";
        if (colDestinatarios != null && colDestinatarios.length > 0) {
            for (int i = 0; i < colDestinatarios.length; i++) {
                if (i > 0) {
                    destinatariosEmail += ";";
                }
                destinatariosEmail += colDestinatarios[i];
            }
        }
        this.colDestinatarios = colDestinatarios;
    }

}
