/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import br.com.citframework.dto.IDto;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PermissoesFluxo")
public class PermissoesFluxoDTO implements IDto {

    private static final long serialVersionUID = -8206459839120172951L;

    private Integer idTipoFluxo;
    private Integer idGrupo;
    private String criar;
    private String executar;
    private String delegar;
    private String suspender;
    private String reativar;
    private String alterarSLA;
    private String reabrir;
    private String cancelar;

    public Integer getIdTipoFluxo() {
        return idTipoFluxo;
    }

    public void setIdTipoFluxo(final Integer idTipoFluxo) {
        this.idTipoFluxo = idTipoFluxo;
    }

    public Integer getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(final Integer idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getCriar() {
        return criar;
    }

    public void setCriar(final String criar) {
        this.criar = criar;
    }

    public String getExecutar() {
        return executar;
    }

    public void setExecutar(final String executar) {
        this.executar = executar;
    }

    public String getDelegar() {
        return delegar;
    }

    public void setDelegar(final String delegar) {
        this.delegar = delegar;
    }

    public String getSuspender() {
        return suspender;
    }

    public void setSuspender(final String suspender) {
        this.suspender = suspender;
    }

    public String getReativar() {
        return reativar;
    }

    public void setReativar(final String reativar) {
        this.reativar = reativar;
    }

    public String getAlterarSLA() {
        return alterarSLA;
    }

    public void setAlterarSLA(final String alterarSLA) {
        this.alterarSLA = alterarSLA;
    }

    public String getReabrir() {
        return reabrir;
    }

    public void setReabrir(final String reabrir) {
        this.reabrir = reabrir;
    }

    public String getCancelar() {
        return cancelar;
    }

    public void setCancelar(final String cancelar) {
        this.cancelar = cancelar;
    }

}
