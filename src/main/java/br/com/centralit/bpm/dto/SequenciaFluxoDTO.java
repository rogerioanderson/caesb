/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.dto;

import br.com.citframework.dto.IDto;

public class SequenciaFluxoDTO implements IDto {

    private static final long serialVersionUID = 8226164140444325947L;

    private Integer idFluxo;
    private Integer idElementoOrigem;
    private Integer idElementoDestino;
    private Integer idConexaoOrigem;
    private Integer idConexaoDestino;
    private String nome;
    private String condicao;

    private Integer idElemento;
    private String idDesenhoOrigem;
    private String idDesenhoDestino;
    private Double bordaX;
    private Double bordaY;
    private String posicaoAlterada;

    public Integer getIdFluxo() {
        return idFluxo;
    }

    public void setIdFluxo(final Integer idFluxo) {
        this.idFluxo = idFluxo;
    }

    public String getCondicao() {
        return condicao;
    }

    public void setCondicao(final String condicao) {
        this.condicao = condicao;
    }

    public Integer getIdElementoOrigem() {
        return idElementoOrigem;
    }

    public void setIdElementoOrigem(final Integer idElementoOrigem) {
        this.idElementoOrigem = idElementoOrigem;
    }

    public Integer getIdElementoDestino() {
        return idElementoDestino;
    }

    public void setIdElementoDestino(final Integer idElementoDestino) {
        this.idElementoDestino = idElementoDestino;
    }

    public String getIdDesenhoOrigem() {
        return idDesenhoOrigem;
    }

    public void setIdDesenhoOrigem(final String idDesenhoOrigem) {
        this.idDesenhoOrigem = idDesenhoOrigem;
    }

    public String getIdDesenhoDestino() {
        return idDesenhoDestino;
    }

    public void setIdDesenhoDestino(final String idDesenhoDestino) {
        this.idDesenhoDestino = idDesenhoDestino;
    }

    public Integer getIdConexaoOrigem() {
        return idConexaoOrigem;
    }

    public void setIdConexaoOrigem(final Integer idConexaoOrigem) {
        this.idConexaoOrigem = idConexaoOrigem;
    }

    public Integer getIdConexaoDestino() {
        return idConexaoDestino;
    }

    public void setIdConexaoDestino(final Integer idConexaoDestino) {
        this.idConexaoDestino = idConexaoDestino;
    }

    public Integer getIdElemento() {
        return idElemento;
    }

    public void setIdElemento(final Integer idElemento) {
        this.idElemento = idElemento;
    }

    public Double getBordaX() {
        return bordaX;
    }

    public void setBordaX(final Double bordaX) {
        this.bordaX = bordaX;
    }

    public Double getBordaY() {
        return bordaY;
    }

    public void setBordaY(final Double bordaY) {
        this.bordaY = bordaY;
    }

    public String getPosicaoAlterada() {
        return posicaoAlterada;
    }

    public void setPosicaoAlterada(final String posicaoAlterada) {
        this.posicaoAlterada = posicaoAlterada;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

}
