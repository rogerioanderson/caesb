/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import br.com.citframework.util.Reflexao;
import br.com.citframework.util.UtilStrings;

public class UtilScript {

    public static Object executaScript(final String nome, String script, final Map<String, Object> objetos) throws Exception {
        Object object = null;
        final Map<String, Object> params = getParams(objetos);
        if (script != null) {
            script = substituiParametros(script, params);
            Context cx = Context.enter();
            final Scriptable scope = cx.initStandardObjects();
            scope.put("params", scope, params);
            try {
                object = cx.evaluateString(scope, script, "script", 1, null);
            } catch (final Exception e) {
                final String msg = e.getLocalizedMessage(); // + "\n" + script;
                throw new Exception(msg);
            } finally {
                Context.exit();
                cx = null;
            }
            cx = null;
        }
        return object;
    }

    public static Map<String, Object> getParams(final Map<String, Object> objetos) throws Exception {
        final Map<String, Object> params = new HashMap<>();
        for (final String key : objetos.keySet()) {
            final Object objeto = objetos.get(key);
            params.put(key, objeto);
            try {
                final List lstGets = Reflexao.findGets(objeto);
                for (int i = 0; i < lstGets.size(); i++) {
                    final String propriedade = UtilStrings.convertePrimeiraLetra((String) lstGets.get(i), "L");
                    final Object value = Reflexao.getPropertyValue(objeto, propriedade);
                    if (value != null) {
                        final String id = key + "." + UtilStrings.convertePrimeiraLetra(propriedade, "L");
                        params.put(id, value);
                    }
                }
            } catch (final Exception e) {

            }
        }
        return params;
    }

    public static String substituiParametros(String str, final Map<String, Object> params) throws Exception {
        if (str != null) {
            for (final String key : params.keySet()) {
                str = str.replaceAll("\\#\\{" + key + "\\}", "params.get(\"" + key + "\")");
            }
        }
        return str;
    }

}
