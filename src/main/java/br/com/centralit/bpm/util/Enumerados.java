/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.util;

public class Enumerados {

    public static final String ACAO_DELEGAR = "D";
    public static final String ACAO_INICIAR = "I";
    public static final String ACAO_EXECUTAR = "E";
    public static final String ACAO_VISUALIZAR = "V";

    public static final String INTERACAO_VISAO = "V";
    public static final String INTERACAO_URL = "U";

    public static final String TIPO_ASSOCIACAO_INSTANCIA = "I";
    public static final String TIPO_ASSOCIACAO_TAREFA = "T";

    public static final String INSTANCIA_ABERTA = SituacaoInstanciaFluxo.Aberta.name();
    public static final String INSTANCIA_ENCERRADA = SituacaoInstanciaFluxo.Encerrada.name();

    public enum SituacaoItemTrabalho {
        Criado("Criado"),
        Disponivel("Dispon�vel"),
        EmAndamento("Em andamento"),
        Suspenso("Suspenso"),
        Cancelado("Cancelado"),
        Encerrado("Suspenso"),
        Executado("Executado");

        private final String descricao;

        SituacaoItemTrabalho(final String descricao) {
            this.descricao = descricao;
        }

        public String getDescricao() {
            return descricao;
        }
    }

    public enum SituacaoInstanciaFluxo {
        Aberta("Aberta"),
        Suspensa("Suspensa"),
        Cancelada("Cancelada"),
        Encerrada("Encerrada");

        private final String descricao;

        SituacaoInstanciaFluxo(final String descricao) {
            this.descricao = descricao;
        }

        public String getDescricao() {
            return descricao;
        }
    }

    public enum TipoAtribuicao {
        Automatica("Autom�tica"),
        Acompanhamento("Acompanhamento"),
        Delegacao("Delega��o");

        private final String descricao;

        TipoAtribuicao(final String descricao) {
            this.descricao = descricao;
        }

        public String getDescricao() {
            return descricao;
        }
    }

    public enum TipoElementoFluxo {
        Inicio("In�cio"),
        Tarefa("Tarefa"),
        Script("Script"),
        Email("Email"),
        Porta("Porta"),
        Evento("Evento"),
        Finalizacao("Finaliza��o");

        private final String descricao;

        TipoElementoFluxo(final String descricao) {
            this.descricao = descricao;
        }

        public String getDescricao() {
            return descricao;
        }
    }

    public enum TipoPorta {
        Decisao("Decis�o"),
        Paralela("Paralela"),
        Convergente("Convergente");

        private final String descricao;

        TipoPorta(final String descricao) {
            this.descricao = descricao;
        }

        public String getDescricao() {
            return descricao;
        }
    }

    public enum TipoFinalizacao {
        Encadeada("Encadeada");

        private final String descricao;

        TipoFinalizacao(final String descricao) {
            this.descricao = descricao;
        }

        public String getDescricao() {
            return descricao;
        }
    }

    public enum AcaoItemTrabalho {
        Iniciar("Iniciar"),
        Executar("Executar"),
        Delegar("Delegar"),
        Suspender("Suspender"),
        Cancelar("Cancelar");

        private final String descricao;

        AcaoItemTrabalho(final String descricao) {
            this.descricao = descricao;
        }

        public String getDescricao() {
            return descricao;
        }
    }

}
