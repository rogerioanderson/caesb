/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.integracao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.bpm.dto.ItemTrabalhoFluxoDTO;
import br.com.centralit.bpm.util.Enumerados;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.SQLConfig;

public class ItemTrabalhoFluxoDao extends CrudDaoDefaultImpl {

    private static final String TABLE_NAME = "bpm_itemtrabalhofluxo";

    public ItemTrabalhoFluxoDao() {
        super(Constantes.getValue("DATABASE_ALIAS"), null);
    }

    @Override
    public Collection<Field> getFields() {
        final Collection<Field> listFields = new ArrayList<>();
        listFields.add(new Field("idItemTrabalho", "idItemTrabalho", true, true, false, false));
        listFields.add(new Field("idInstancia", "idInstancia", false, false, false, false));
        listFields.add(new Field("idElemento", "idElemento", false, false, false, false));
        listFields.add(new Field("idResponsavelAtual", "idResponsavelAtual", false, false, false, false));
        listFields.add(new Field("dataHoraCriacao", "dataHoraCriacao", false, false, false, false));
        listFields.add(new Field("dataHoraInicio", "dataHoraInicio", false, false, false, false));
        listFields.add(new Field("dataHoraFinalizacao", "dataHoraFinalizacao", false, false, false, false));
        listFields.add(new Field("dataHoraExecucao", "dataHoraExecucao", false, false, false, false));
        listFields.add(new Field("situacao", "situacao", false, false, false, false));
        return listFields;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public Class<?> getBean() {
        return ItemTrabalhoFluxoDTO.class;
    }

    public Collection<ItemTrabalhoFluxoDTO> findByIdInstanciaAndIdElemento(final Integer idInstancia, final Integer idElemento) throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        condicao.add(new Condition("idInstancia", "=", idInstancia));
        condicao.add(new Condition("idElemento", "=", idElemento));
        return super.findByCondition(condicao, null);
    }

    public Collection<ItemTrabalhoFluxoDTO> findDisponiveisByIdInstancia(final Integer idInstancia) throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        condicao.add(new Condition("idInstancia", "=", idInstancia));
        condicao.add(new Condition("situacao", "<>", Enumerados.SituacaoItemTrabalho.Executado.name()));
        condicao.add(new Condition("situacao", "<>", Enumerados.SituacaoItemTrabalho.Cancelado.name()));
        return super.findByCondition(condicao, null);
    }

    public ItemTrabalhoFluxoDTO lastByIdInstanciaAndIdElemento(final Integer idInstancia, final Integer idElemento) throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordem = new ArrayList<>();
        condicao.add(new Condition("idInstancia", "=", idInstancia));
        condicao.add(new Condition("idElemento", "=", idElemento));
        ordem.add(new Order("idItemTrabalho", Order.DESC));

        final Collection col = super.findByCondition(condicao, null);
        if (col == null || col.size() == 0) {
            return null;
        }
        return (ItemTrabalhoFluxoDTO) ((List) col).get(0);
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Integer buscaIdUsuarioResponsavelFechamentoSolicitacao(final Long idTarefaEncerramento) throws Exception {
		try {
			List resp = new ArrayList();
            final List parametro = new ArrayList();
            
            final StringBuilder sql = new StringBuilder();
			sql.append("select ");
			sql.append("idresponsavelatual ");
			sql.append("from bpm_itemtrabalhofluxo ");
			
            if (idTarefaEncerramento != null && idTarefaEncerramento > 0) {
				sql.append("where iditemtrabalho=?");
				parametro.add(idTarefaEncerramento);
			}
            
            resp = this.execSQL(sql.toString(), parametro.toArray());
            
            if (resp != null && !resp.isEmpty() && resp.get(0) != null) {
                if (CITCorporeUtil.SGBD_PRINCIPAL.trim().toUpperCase().equalsIgnoreCase(SQLConfig.SQLSERVER)) {
            		return ((Integer) ((Object[]) resp.get(0))[0]).intValue();
                } else if (CITCorporeUtil.SGBD_PRINCIPAL.trim().toUpperCase().equalsIgnoreCase(SQLConfig.ORACLE)) {
            		return ((BigDecimal) ((Object[]) resp.get(0))[0]).intValue();
                } else {
            	return (Integer) ((Object[]) resp.get(0))[0];
                }
            } else {
            	return 0;
            }
        } catch (final PersistenceException e) {
			e.printStackTrace();
			return 0;
        } catch (final Exception e) {
			e.printStackTrace();
			return 0;
		}
    }

}
