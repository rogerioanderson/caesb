/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.integracao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.centralit.bpm.config.Config;
import br.com.centralit.bpm.dto.ElementoFluxoDTO;
import br.com.centralit.bpm.util.Enumerados.TipoElementoFluxo;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.Condition;
import br.com.citframework.integracao.CrudDaoDefaultImpl;
import br.com.citframework.integracao.Field;
import br.com.citframework.integracao.Order;
import br.com.citframework.util.Constantes;
import br.com.citframework.util.Reflexao;
import br.com.citframework.util.SQLConfig;

public class ElementoFluxoDao extends CrudDaoDefaultImpl {

    private static final String TABLE_NAME = "bpm_elementofluxo";

    public ElementoFluxoDao() {
        super(Constantes.getValue("DATABASE_ALIAS"), null);
    }

    @Override
    public Collection<Field> getFields() {
        final Collection<Field> listFields = new ArrayList<>();
        listFields.add(new Field("idElemento", "idElemento", true, true, false, false));
        listFields.add(new Field("idFluxo", "idFluxo", false, false, false, false));
        listFields.add(new Field("tipoElemento", "tipoElemento", false, false, false, false));
        listFields.add(new Field("subTipo", "subTipo", false, false, false, false));
        listFields.add(new Field("nome", "nome", false, false, false, false));
        listFields.add(new Field("documentacao", "documentacao", false, false, false, false));
        listFields.add(new Field("acaoEntrada", "acaoEntrada", false, false, false, false));
        listFields.add(new Field("acaoSaida", "acaoSaida", false, false, false, false));
        listFields.add(new Field("tipoInteracao", "tipoInteracao", false, false, false, false));
        listFields.add(new Field("visao", "visao", false, false, false, false));
        listFields.add(new Field("url", "url", false, false, false, false));
        listFields.add(new Field("grupos", "grupos", false, false, false, false));
        listFields.add(new Field("usuarios", "usuarios", false, false, false, false));
        listFields.add(new Field("script", "script", false, false, false, false));
        listFields.add(new Field("textoEmail", "textoEmail", false, false, false, false));
        listFields.add(new Field("nomeFluxoEncadeado", "nomeFluxoEncadeado", false, false, false, false));
        listFields.add(new Field("modeloEmail", "modeloEmail", false, false, false, false));
        listFields.add(new Field("intervalo", "intervalo", false, false, false, false));
        listFields.add(new Field("condicaoDisparo", "condicaoDisparo", false, false, false, false));
        listFields.add(new Field("multiplasInstancias", "multiplasInstancias", false, false, false, false));
        listFields.add(new Field("contabilizaSLA", "contabilizaSLA", false, false, false, false));
        listFields.add(new Field("percExecucao", "percExecucao", false, false, false, false));
        listFields.add(new Field("destinatariosEmail", "destinatariosEmail", false, false, false, false));
        listFields.add(new Field("posX", "posX", false, false, false, false));
        listFields.add(new Field("posY", "posY", false, false, false, false));
        listFields.add(new Field("largura", "largura", false, false, false, false));
        listFields.add(new Field("altura", "altura", false, false, false, false));
        listFields.add(new Field("template", "template", false, false, false, false));
        return listFields;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public Collection<ElementoFluxoDTO> list() throws PersistenceException {
        return null;
    }

    @Override
    public Class<?> getBean() {
        return ElementoFluxoDTO.class;
    }

    @Override
    public Collection<ElementoFluxoDTO> find(final IDto arg0) throws PersistenceException {
        return null;
    }

    public List<ElementoFluxoDTO> findAllByIdFluxo(final Integer parm) throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordenacao = new ArrayList<>();
        condicao.add(new Condition("idFluxo", "=", parm));
        ordenacao.add(new Order("idElemento"));
        return (List<ElementoFluxoDTO>) super.findByCondition(condicao, ordenacao);
    }

    public List<ElementoFluxoDTO> findByIdFluxo(final Integer parm) throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        final List<Order> ordenacao = new ArrayList<>();
        condicao.add(new Condition("idFluxo", "=", parm));
        condicao.add(new Condition("tipoElemento", "=", this.getTipoElemento().name()));
        ordenacao.add(new Order("idElemento"));
        return (List<ElementoFluxoDTO>) super.findByCondition(condicao, ordenacao);
    }

    public void deleteByIdFluxo(final Integer parm) throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        condicao.add(new Condition("idFluxo", "=", parm));
        condicao.add(new Condition("tipoElemento", "=", this.getTipoElemento().name()));
        super.deleteByCondition(condicao);
    }

    public void deleteAllByIdFluxo(final Integer parm) throws PersistenceException {
        final List<Condition> condicao = new ArrayList<>();
        condicao.add(new Condition("idFluxo", "=", parm));
        super.deleteByCondition(condicao);
    }

    @Override
    public IDto create(final IDto obj) throws PersistenceException {
        ((ElementoFluxoDTO) obj).setTipoElemento(this.getTipoElemento().name());
        return super.create(obj);
    }

    @Override
    public IDto restore(IDto obj) throws PersistenceException {
        obj = super.restore(obj);
        if (obj == null) {
            return null;
        }
        final String nomeClasse = Config.getClasseDtoElemento(((ElementoFluxoDTO) obj).getTipoElemento());
        ElementoFluxoDTO elementoDto = null;
        try {
            elementoDto = (ElementoFluxoDTO) Class.forName(nomeClasse).newInstance();
            Reflexao.copyPropertyValues(obj, elementoDto);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return elementoDto;
    }

    public ElementoFluxoDTO restore(final Integer idElementoFluxo) throws PersistenceException {
        final ElementoFluxoDTO elementoDto = new ElementoFluxoDTO();
        elementoDto.setIdElemento(idElementoFluxo);
        return (ElementoFluxoDTO) this.restore(elementoDto);
    }

    protected TipoElementoFluxo getTipoElemento() {
        return null;
    }

    public List<ElementoFluxoDTO> listaElementoFluxo(final String documentacao) throws PersistenceException {
        final List<String> parametro = new ArrayList<>();
        final List<String> listRetorno = new ArrayList<>();
        List<?> lista = new ArrayList<>();

        String sql = "";
        if (CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.SQLSERVER)) {
            sql = "select distinct(trim(documentacao)) documentacao from bpm_elementofluxo where trim(documentacao) ";
        } else {
            sql = "select distinct(trim(documentacao)) documentacao from bpm_elementofluxo where trim(documentacao) ";
        }
        
        if (CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.POSTGRESQL)) {
            sql += " ilike ('%" + documentacao + "%')";
        } else {
            sql += " like ('%" + documentacao + "%')";
        }

        sql += " ORDER BY documentacao ";

        if (CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.ORACLE) || CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.SQLSERVER)) {
            String sqlOracleSqlServer = "";
            if (CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.ORACLE)) {
                sqlOracleSqlServer = "select distinct cast(documentacao as varchar2(4000)) documentacao  from bpm_elementofluxo where UPPER(documentacao) ";
            }
            if (CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.SQLSERVER)) {
                sqlOracleSqlServer = "select distinct cast(documentacao as varchar(4000)) documentacao  from bpm_elementofluxo where documentacao ";
            }
            sqlOracleSqlServer += " like UPPER('%" + documentacao + "%')";
            sqlOracleSqlServer += " and documentacao is not null ";
            sqlOracleSqlServer += " order by documentacao ";

            listRetorno.add("documentacao");

            lista = this.execSQL(sqlOracleSqlServer.toString(), parametro.toArray());

            return engine.listConvertion(this.getBean(), lista, listRetorno);
        }

        if (!CITCorporeUtil.SGBD_PRINCIPAL.toUpperCase().equals(SQLConfig.ORACLE)) {
            lista = this.execSQL(sql, parametro.toArray());
        }

        listRetorno.add("documentacao");

        return engine.listConvertion(this.getBean(), lista, listRetorno);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Integer buscaIdElementoFinalizacao(Integer idFluxo) {
		try {
			List resp = new ArrayList();
            final List parametro = new ArrayList();
            
            final StringBuilder sql = new StringBuilder();
			sql.append("select ");
			sql.append("idelemento ");
			sql.append("from bpm_elementofluxo where ");
			
            if (idFluxo != null && idFluxo > 0) {
				sql.append("idfluxo=? ");
				parametro.add(idFluxo);
			}
            
            sql.append("and tipoelemento = ?");
            parametro.add(TipoElementoFluxo.Finalizacao.name());
            
            resp = this.execSQL(sql.toString(), parametro.toArray());
            
            if (resp != null && !resp.isEmpty() && resp.get(0) != null) {
                if (CITCorporeUtil.SGBD_PRINCIPAL.trim().toUpperCase().equalsIgnoreCase(SQLConfig.SQLSERVER)) {
            		return Integer.valueOf(((Long) ((Object[]) resp.get(0))[0]).intValue());
                } else if (CITCorporeUtil.SGBD_PRINCIPAL.trim().toUpperCase().equalsIgnoreCase(SQLConfig.ORACLE)) {
            		return ((BigDecimal) ((Object[]) resp.get(0))[0]).intValue();
                } else {
                	return Integer.valueOf(((Long) ((Object[]) resp.get(0))[0]).intValue());
                }
            } else {
            	return 0;
            }
        } catch (final PersistenceException e) {
			e.printStackTrace();
			return 0;
        } catch (final Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

}
