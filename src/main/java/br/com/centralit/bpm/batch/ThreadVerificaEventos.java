/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.bpm.batch;

import java.sql.Timestamp;
import java.util.Collection;

import br.com.centralit.bpm.dto.EventoFluxoDTO;
import br.com.centralit.bpm.integracao.EventoFluxoDao;
import br.com.citframework.integracao.TransactionControler;
import br.com.citframework.util.UtilDatas;

public class ThreadVerificaEventos extends Thread {

    private EventoFluxoDao eventoFluxoDao;
    private Collection<EventoFluxoDTO> eventosDisponiveis;

    @Override
    public void run() {
        while (true) {
            try {
                sleep(5000);
                this.carregaEventosDisponiveis();
                if (eventosDisponiveis != null) {
                    for (final EventoFluxoDTO eventoFluxoDto : eventosDisponiveis) {
                        sleep(500);
                        boolean bExecuta = true;
                        if (eventoFluxoDto.getDataHoraExecucao() != null && eventoFluxoDto.getIntervalo() != null) {
                            final Timestamp ts = UtilDatas.somaSegundos(eventoFluxoDto.getDataHoraExecucao(), eventoFluxoDto.getIntervalo());
                            bExecuta = ts.compareTo(UtilDatas.getDataHoraAtual()) <= 0;
                        }
                        if (bExecuta) {
                            sleep(1000);

                            // Forma Antiga, sem usar executor para controlar as threads
                            final ThreadExecutaEvento thread = new ThreadExecutaEvento(eventoFluxoDto);
                            thread.start();
                        }
                    }
                }
            } catch (final Exception e) {
                System.out.println("#########################################");
                System.out.println("Problemas na execu��o dos eventos bpm");
                System.out.println("#########################################");
                e.printStackTrace();
            }
        }
    }

    private void carregaEventosDisponiveis() {
        final EventoFluxoDao eventoFluxoDao = this.getEventoFluxoDao();
        try {
            eventosDisponiveis = eventoFluxoDao.findDisponiveis();
        } catch (final Exception e) {
            eventosDisponiveis = null;
            System.out.println("#########################################");
            System.out.println("Problemas na carga dos eventos bpm       ");
            System.out.println("#########################################");
            e.printStackTrace();
        } finally {
            this.closeConnectionOnTransaction();
        }
    }

    private EventoFluxoDao getEventoFluxoDao() {
        if (eventoFluxoDao == null) {
            eventoFluxoDao = new EventoFluxoDao();
        }
        return eventoFluxoDao;
    }

    private void closeConnectionOnTransaction() {
        try {
            final TransactionControler tc = this.getEventoFluxoDao().getTransactionControler();
            if (tc != null) {
                tc.closeQuietly();
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

}
