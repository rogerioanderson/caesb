/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citgerencial.processparameters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import br.com.centralit.citcorpore.bean.ContratoDTO;
import br.com.centralit.citcorpore.bean.OrigemAtendimentoDTO;
import br.com.centralit.citcorpore.bean.PrioridadeDTO;
import br.com.centralit.citcorpore.bean.ServicoDTO;
import br.com.centralit.citcorpore.bean.UnidadeDTO;
import br.com.centralit.citcorpore.negocio.ContratoService;
import br.com.centralit.citcorpore.negocio.OrigemAtendimentoService;
import br.com.centralit.citcorpore.negocio.PrioridadeService;
import br.com.centralit.citcorpore.negocio.ServicoService;
import br.com.centralit.citcorpore.negocio.UnidadeService;
import br.com.centralit.citgerencial.bean.GerencialOptionDTO;
import br.com.centralit.citgerencial.bean.GerencialParameterDTO;
import br.com.centralit.citgerencial.bean.GerencialProcessParameters;
import br.com.citframework.comparacao.ObjectSimpleComparator;
import br.com.citframework.excecao.CompareException;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilStrings;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class ProcessParametersGeral extends GerencialProcessParameters {

	public String processParameters(final HashMap hsmParms, final Collection colParmsUtilizadosNoSQL, final Collection colDefinicaoParametros) {
		if (colDefinicaoParametros == null || colDefinicaoParametros.isEmpty())
			return "";
		StringBuilder valor = new StringBuilder();
		StringBuilder strRetorno = new StringBuilder();
		String nameParam = null;
		List nomeParams = new ArrayList();
		GerencialParameterDTO param = null;
		for (Iterator it = colDefinicaoParametros.iterator(); it.hasNext();) {
			param = (GerencialParameterDTO) it.next();
			nameParam = "PARAM." + param.getName();
			if (nomeParams.indexOf(nameParam) < 0) {
				nomeParams.add(nameParam);
				if (String[].class.isInstance(hsmParms.get(nameParam))) {
					valor.setLength(0);
					String[] val = (String[]) hsmParms.get(nameParam);
					for (int i = 0; i < ((String[]) val).length; i++) {
						if (valor.length() > 0) {
							valor.append(",");
						}
						valor.append(val[i]);
					}
				} else if (String.class.isInstance(hsmParms.get(nameParam))) {
				        valor.setLength(0);
					valor.append((String) hsmParms.get(nameParam));
				} else {
					valor = null;
				}
				
				/**
				 * rcs - Analista Desenvolvedor
				 * 
				 * o if abaixo foi inserido, para se buscar corretamente o valor do campo "numero" na tabela "contratos".
				 * Pois antes quando na gera��o de relat�rios, o valor que aparecia para contrato era o 'id' do contrato na tabela, e n�o o texto "numero", que carrega o 'nome'(por assim dizer) do contrato.
				 * 
				 *  data: 28/07/2015
				 *  email: rafael.soyer@centralit.com.br
				 */
				if (nameParam.equalsIgnoreCase("PARAM.idContrato")) {
                                    if (StringUtils.isNotBlank(valor) && !valor.toString().equalsIgnoreCase("-1")) {
                                        ContratoDTO contratoDto = new ContratoDTO();
                                        contratoDto.setIdContrato(Integer.valueOf((valor.toString())));
                
                                        try {
                                            ContratoService contratoServ = (ContratoService) ServiceLocator.getInstance().getService(ContratoService.class, null);
                                            contratoDto = (ContratoDTO) contratoServ.restore(contratoDto);
                                        } catch (ServiceException servExc) {
                                            servExc.printStackTrace();
                                            contratoDto = null;
                                        } catch (LogicException logicExc) {
                                            logicExc.printStackTrace();
                                            contratoDto = null;
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            contratoDto = null;
                                        }
                
                                        String campo_numeroContrato;
                
                                        if ((ObjectUtils.notEqual(contratoDto, null)) && (StringUtils.isNotBlank(contratoDto.getNumero()))) {
                                            campo_numeroContrato = contratoDto.getNumero();
                                        } else {
                                            campo_numeroContrato = (String) hsmParms.get("citcorpore.comum.naoInformado");
                                        }
                
                                        strRetorno.append(param.getDescription());
                                        strRetorno.append(": ");
                                        strRetorno.append(campo_numeroContrato);
                                    } else {
                                        strRetorno.append(getDescricaoParametro(colDefinicaoParametros, nameParam));
                                        strRetorno.append(": ");
                                        strRetorno.append((String) hsmParms.get("citcorpore.comum.todos"));
                                    }
                                } else if (nameParam.equalsIgnoreCase("PARAM.idPrioridade")) {
					if (StringUtils.isNotBlank(valor) && !valor.toString().equalsIgnoreCase("-1")) {
						PrioridadeDTO prioridadeDto = new PrioridadeDTO();
						prioridadeDto.setIdPrioridade(Integer.valueOf(valor.toString()));
						try {
							PrioridadeService serv = (PrioridadeService) ServiceLocator.getInstance().getService(PrioridadeService.class, null);
							prioridadeDto = (PrioridadeDTO) serv.restore(prioridadeDto);
						} catch (Exception e) {
							e.printStackTrace();
							prioridadeDto = null;
						}
						String nomePrioridade = "";
						if (prioridadeDto != null)
							nomePrioridade = prioridadeDto.getNomePrioridade();
						else{
							nomePrioridade = (String) hsmParms.get("citcorpore.comum.naoInformado");
						}
						
						strRetorno.append(param.getDescription());
						strRetorno.append(": ");
						strRetorno.append(nomePrioridade);
					} else{
						strRetorno.append(getDescricaoParametro(colDefinicaoParametros, nameParam));
					        strRetorno.append(": ");
					        strRetorno.append((String) hsmParms.get("citcorpore.comum.todos"));
					}
				} else if (nameParam.equalsIgnoreCase("PARAM.idServico")) {
					if (StringUtils.isNotBlank(valor) && !valor.toString().equalsIgnoreCase("-1")) {
						ServicoDTO servicoDto = new ServicoDTO();
						servicoDto.setIdServico(Integer.valueOf(valor.toString()));
						try {
							ServicoService serv = (ServicoService) ServiceLocator.getInstance().getService(ServicoService.class, null);
							servicoDto = (ServicoDTO) serv.restore(servicoDto);
						} catch (Exception e) {
							e.printStackTrace();
							servicoDto = null;
						}
						String nomeServico = "";
						if (servicoDto != null)
							nomeServico = servicoDto.getNomeServico();
						else{
							nomeServico = (String) hsmParms.get("citcorpore.comum.naoInformado");
						}
						
						strRetorno.append(param.getDescription());
						strRetorno.append(": ");
						strRetorno.append(nomeServico);
					} else{
						strRetorno.append(getDescricaoParametro(colDefinicaoParametros, nameParam));
					        strRetorno.append(": ");
					        strRetorno.append((String) hsmParms.get("citcorpore.comum.todos"));
					}
				} else if (nameParam.equalsIgnoreCase("PARAM.idUnidade")) {
					if (StringUtils.isNotBlank(valor) && !valor.toString().equalsIgnoreCase("-1")) {
						UnidadeDTO unidadeDto = new UnidadeDTO();
						unidadeDto.setIdUnidade(Integer.valueOf(valor.toString()));
						try {
							UnidadeService serv = (UnidadeService) ServiceLocator.getInstance().getService(UnidadeService.class, null);
							unidadeDto = (UnidadeDTO) serv.restore(unidadeDto);
						} catch (Exception e) {
							e.printStackTrace();
							unidadeDto = null;
						}
						String nomeUnidade = "";
						if (unidadeDto != null)
							nomeUnidade = unidadeDto.getNome();
						else{
							nomeUnidade = (String) hsmParms.get("citcorpore.comum.naoInformado");
						}
						
						strRetorno.append(param.getDescription());
						strRetorno.append(": ");
						strRetorno.append(nomeUnidade);
					} else{
						strRetorno.append(getDescricaoParametro(colDefinicaoParametros, nameParam));
						strRetorno.append(": ");
						strRetorno.append((String) hsmParms.get("citcorpore.comum.todos"));
					}
				} else if (nameParam.equalsIgnoreCase("PARAM.idOrigem")) {
					if (StringUtils.isNotBlank(valor) && !valor.toString().equalsIgnoreCase("-1")) {
						OrigemAtendimentoDTO origemDto = new OrigemAtendimentoDTO();
						origemDto.setIdOrigem(Integer.valueOf(valor.toString()));
						try {
							OrigemAtendimentoService origemAtendimentoService = (OrigemAtendimentoService) ServiceLocator.getInstance().getService(OrigemAtendimentoService.class, null);
							origemDto = (OrigemAtendimentoDTO) origemAtendimentoService.restore(origemDto);
						} catch (Exception e) {
							e.printStackTrace();
							origemDto = null;
						}
						String nomeOrigemAtendimento = "";
						if (origemDto != null)
							nomeOrigemAtendimento = origemDto.getDescricao();
						else{
							nomeOrigemAtendimento = (String) hsmParms.get("citcorpore.comum.naoInformado");
						}
						
						strRetorno.append(param.getDescription());
						strRetorno.append(": ");
						strRetorno.append(nomeOrigemAtendimento);
					} else{
						strRetorno.append(getDescricaoParametro(colDefinicaoParametros, nameParam));
						strRetorno.append(": ");
						strRetorno.append((String) hsmParms.get("citcorpore.comum.todos"));
					}
				} else {
					if (UtilStrings.isNotVazio(param.getTypeHTML()) && "select".equalsIgnoreCase(param.getTypeHTML()) && param.getColOptions() != null && !param.getColOptions().isEmpty()) {
						try {
							ObjectSimpleComparator osc = new ObjectSimpleComparator("getValue", ObjectSimpleComparator.ASC);

							List options = new ArrayList(param.getColOptions());
							GerencialOptionDTO aux = new GerencialOptionDTO();
							aux.setValue(valor.toString());
							if (options != null && osc != null) {
								try {
									Collections.sort(options, osc);
									Integer index = Collections.binarySearch(options, aux, osc);
									if (index != null && index >= 0) {
										try {
										        valor.setLength(0);
											valor.append(((GerencialOptionDTO) options.get(index)).getText());
										} catch (Exception e) {
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						} catch (CompareException ce) {
							ce.printStackTrace();
						} catch (NullPointerException e) {
							e.printStackTrace();
						}
					}
					try {
						if (StringUtils.isNotBlank(valor)) {
						        valor.setLength(0);
						        
							if (valor.toString().equalsIgnoreCase((String) hsmParms.get("citcorpore.comum.selecione"))) {
								valor.append((String) hsmParms.get("citcorpore.comum.todos"));
							} else if (valor.toString().equalsIgnoreCase("-1")){
	                                                        valor.append("--");
							}
						} else {
						        valor = new StringBuilder();
							valor.append("--");
						}
						
						strRetorno.append(getDescricaoParametro(colDefinicaoParametros, nameParam));
						strRetorno.append(": ");
						strRetorno.append(valor.toString());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			strRetorno.append("   ");
		}
		
		return strRetorno.toString();
	}

	private String getDescricaoParametro(final Collection colDefinicaoParametros, final String nameParm) {
		if (colDefinicaoParametros == null)
			return "";
		for (Iterator it = colDefinicaoParametros.iterator(); it.hasNext();) {
			GerencialParameterDTO gerencialParameterDTO = (GerencialParameterDTO) it.next();
			String nomeParmAux = "PARAM." + gerencialParameterDTO.getName().trim();
			if (nomeParmAux.equalsIgnoreCase(nameParm)) {
				String desc = gerencialParameterDTO.getDescription();
				int p = desc.indexOf(" (");
				if (p > 0)
					desc = desc.substring(0, p);
				return desc;
			}
		}
		return "";
	}

}
