/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citgerencial.negocio;

import java.util.Collection;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citgerencial.bean.GerencialInfoGenerateDTO;
import br.com.centralit.citgerencial.bean.GerencialItemInformationDTO;
import br.com.centralit.citgerencial.bean.GerencialItemPainelDTO;
import br.com.centralit.citgerencial.bean.GerencialOptionsDTO;
import br.com.centralit.citgerencial.bean.GerencialPainelDTO;
import br.com.citframework.dto.Usuario;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.CrudService;

public interface GerencialGenerate extends CrudService {

    Object generate(final GerencialItemInformationDTO gerencialItemDto, final Usuario usuario, final GerencialInfoGenerateDTO infoGenerate,
            final GerencialItemPainelDTO gerencialItemPainelAuxDto, final GerencialPainelDTO gerencialPainelDto, final HttpServletRequest request) throws ServiceException;

    Collection executaSQLOptions(final GerencialOptionsDTO options, final GerencialPainelDTO gerencialPainelDto, final HashMap hashParametros, final Usuario user)
            throws ServiceException;

    Collection executaSQLOptions(final GerencialOptionsDTO options, final Collection listParameters, final HashMap hashParametros, final Usuario user) throws ServiceException;

    Object geraTabelaVazia(final GerencialInfoGenerateDTO infoGenerate, final HttpServletRequest request);

}
