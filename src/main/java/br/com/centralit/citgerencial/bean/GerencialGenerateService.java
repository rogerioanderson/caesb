/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citgerencial.bean;

import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import br.com.citframework.util.UtilI18N;

@SuppressWarnings("rawtypes")
public abstract class GerencialGenerateService {

    /**
     * O retorno deste metodo deve ser uma Lista onde cada linha da lista � uma array de Objetos. Exemplo: Object[] row
     *
     * @param parametersValues
     * @param paramtersDefinition
     * @return
     * @throws ParseException
     */
    public abstract List execute(final HashMap parametersValues, final Collection paramtersDefinition) throws ParseException;

    /**
     * Retorna a linguagem que foi passada no request que est� em paramtersDefinition;
     *
     * @param paramtersDefinition
     * @return String - Language
     * @author valdoilo.damasceno
     * @since 06.02.2014
     */
    public String getLanguage(final Collection paramtersDefinition) {
        String language = UtilI18N.PORTUGUESE_SIGLA;

        for (final Iterator iterator = paramtersDefinition.iterator(); iterator.hasNext();) {
            final Object parametro = iterator.next();

            if (parametro != null && "org.apache.catalina.connector.RequestFacade".equals(parametro.getClass().getName())) {
                final HttpServletRequest request = (HttpServletRequest) parametro;

                final String aux = (String) request.getSession().getAttribute("locale");

                if (aux != null && StringUtils.isNotBlank(aux)) {
                    language = aux;
                }

                break;
            }
        }

        return language;
    }

}
