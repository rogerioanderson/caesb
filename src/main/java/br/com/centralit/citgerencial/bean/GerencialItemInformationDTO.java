/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citgerencial.bean;

import java.util.Collection;

import br.com.citframework.dto.IDto;

public class GerencialItemInformationDTO implements IDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1996048245518604610L;
	private String type;
	private String description;
	private String hDetailSpacing;
	private String title;
	private boolean report;
	private boolean graph;
	private String reportPageLayout;
	private String defaultVisualization; /* T - Tabela; G:Type - Gr�fico + Tipo do Grafico */
	
	private GerencialSQLDTO gerencialSQL = null;
	private GerencialServiceInformationDTO gerencialService = null;
	private GerencialSummaryInformationDTO gerencialSummary = null;
	private Collection listFields;
	private Collection listGroups;
	private Collection listGraphs;
	
	private String classExecute;
	private boolean porcentagem;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isGraph() {
		return graph;
	}
	public void setGraph(boolean graph) {
		this.graph = graph;
	}
	public String getHDetailSpacing() {
		return hDetailSpacing;
	}
	public void setHDetailSpacing(String detailSpacing) {
		hDetailSpacing = detailSpacing;
	}
	public boolean isReport() {
		return report;
	}
	public void setReport(boolean report) {
		this.report = report;
	}
	public String getReportPageLayout() {
		return reportPageLayout;
	}
	public void setReportPageLayout(String reportPageLayout) {
		this.reportPageLayout = reportPageLayout;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public GerencialServiceInformationDTO getGerencialService() {
		return gerencialService;
	}
	public void setGerencialService(GerencialServiceInformationDTO gerencialService) {
		this.gerencialService = gerencialService;
	}
	public GerencialSQLDTO getGerencialSQL() {
		return gerencialSQL;
	}
	public void setGerencialSQL(GerencialSQLDTO gerencialSQL) {
		this.gerencialSQL = gerencialSQL;
	}
	public Collection getListFields() {
		return listFields;
	}
	public void setListFields(Collection listFields) {
		this.listFields = listFields;
	}
	public Collection getListGraphs() {
		return listGraphs;
	}
	public void setListGraphs(Collection listGraphs) {
		this.listGraphs = listGraphs;
	}
	public Collection getListGroups() {
		return listGroups;
	}
	public void setListGroups(Collection listGroups) {
		this.listGroups = listGroups;
	}
	public String getDefaultVisualization() {
		return defaultVisualization;
	}
	public void setDefaultVisualization(String defaultVisualization) {
		this.defaultVisualization = defaultVisualization;
	}
	public GerencialSummaryInformationDTO getGerencialSummary() {
		return gerencialSummary;
	}
	public void setGerencialSummary(GerencialSummaryInformationDTO gerencialSummary) {
		this.gerencialSummary = gerencialSummary;
	}
	public String getClassExecute() {
		return classExecute;
	}
	public void setClassExecute(String classExecute) {
		this.classExecute = classExecute;
	}
	public boolean isPorcentagem() {
		return porcentagem;
	}
	public void setPorcentagem(boolean porcentagem) {
		this.porcentagem = porcentagem;
	}
}
