/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citgerencial.generateservices.mudancas;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import br.com.centralit.citcorpore.bean.GrupoItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.ItemConfiguracaoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaDTO;
import br.com.centralit.citcorpore.bean.RequisicaoMudancaItemConfiguracaoDTO;
import br.com.centralit.citcorpore.integracao.ItemConfiguracaoDao;
import br.com.centralit.citcorpore.integracao.RequisicaoMudancaDao;
import br.com.centralit.citcorpore.integracao.RequisicaoMudancaItemConfiguracaoDao;
import br.com.centralit.citcorpore.negocio.GrupoItemConfiguracaoService;
import br.com.centralit.citcorpore.negocio.ImagemItemConfiguracaoServiceEjb;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.Enumerados.StatusIC;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.centralit.citgerencial.bean.GerencialGenerateService;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilStrings;

/**
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class GenerateServiceImpactoMudanca extends GerencialGenerateService {

	private RequisicaoMudancaDao requisicaoMudancaDao;
	private RequisicaoMudancaDao requisicaoMudancaDaoDataBaseAlias;
	
	private RequisicaoMudancaDao getRequisicaoMudancaDao() {
		if(requisicaoMudancaDao == null) {
			requisicaoMudancaDao = new RequisicaoMudancaDao();
		}
		return requisicaoMudancaDao;
	}
	
	/**
	 * Desenvolvedor: Fabio Amorim - Data: 13/08/2015 - Hor�rio: 14:48 - ID Citsmart: 176361 -
	 * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
	 * @param databaseAlias
	 * @return dao com o databaseAlias especificado, caso esteja preenchido, sen�o dao com o datasource principal
	 */
	private RequisicaoMudancaDao getRequisicaoMudancaDao(String databaseAlias) {
		if (UtilStrings.isNotVazio(databaseAlias)) {
			if (requisicaoMudancaDaoDataBaseAlias == null) {
				requisicaoMudancaDaoDataBaseAlias = new RequisicaoMudancaDao(databaseAlias);
			}
			return requisicaoMudancaDaoDataBaseAlias;
		}
		return this.getRequisicaoMudancaDao();
	}
	
	public List execute(HashMap parametersValues, Collection paramtersDefinition) {

		String datainicial = (String) parametersValues.get("PARAM.dataInicial");
		String datafinal = (String) parametersValues.get("PARAM.dataFinal");
		String idGrupoItemConfiguracaoPaiStr = (String) parametersValues.get("PARAM.idGrupoItemConfiguracaoPai");
		String idCriticidadeStr = (String) parametersValues.get("PARAM.idCriticidade");
		String idTipoItemConfiguracaoStr = (String) parametersValues.get("PARAM.idTipoItemConfiguracao");
		String situacaoMudanca = (String) parametersValues.get("PARAM.situacaoMudanca");
		String situacaoStr = (String) parametersValues.get("PARAM.situacao");

		Date datafim = null;
		Date datainicio = null;
		Integer idGrupoItemConfiguracaoPai = null;
		Integer situacao = null;
		Integer idCriticidade = null;
		Integer idTipoItemConfiguracao = null;
		List<RequisicaoMudancaDTO> mudancas = null;
		GrupoItemConfiguracaoService grupoItemConfiguracaoService = null;
		Collection<GrupoItemConfiguracaoDTO> colGrupos = null;
		List retorno = null;

		try {
			datainicio = UtilDatas.convertStringToSQLDate(TipoDate.DATE_DEFAULT, datainicial, super.getLanguage(paramtersDefinition));
			datafim = UtilDatas.convertStringToSQLDate(TipoDate.DATE_DEFAULT, datafinal, super.getLanguage(paramtersDefinition));
			
			idGrupoItemConfiguracaoPai = new Integer(idGrupoItemConfiguracaoPaiStr);
			situacao = new Integer(situacaoStr);
			idCriticidade = new Integer(idCriticidadeStr);
			idTipoItemConfiguracao = new Integer(idTipoItemConfiguracaoStr);
			
			grupoItemConfiguracaoService = (GrupoItemConfiguracaoService) ServiceLocator.getInstance().getService(GrupoItemConfiguracaoService.class, null);
			ImagemItemConfiguracaoServiceEjb imagemItemConfiguracaoService = new ImagemItemConfiguracaoServiceEjb();
			
			ItemConfiguracaoDao itemConfiguracaoDao = new ItemConfiguracaoDao();
			RequisicaoMudancaItemConfiguracaoDao requisicaoMudancaItemConfiguracaoDao = new RequisicaoMudancaItemConfiguracaoDao();

			/* Realiza o filtro de mudan�as */
			mudancas = getRequisicaoMudancaDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).findByPeriodoAndSituacao(datainicio, datafim, situacaoMudanca);

			/* Filtra os grupos relacionados */
			colGrupos = grupoItemConfiguracaoService.listHierarquiaGruposByIdGrupo(idGrupoItemConfiguracaoPai, null);

			/*
			 * Removido, filtra apenas os grupos relacionados a desenvolvimento if (idGrupoItemConfiguracaoPai != null && idGrupoItemConfiguracaoPai.intValue() == 997){ if (idGrupoItemConfiguracaoPai
			 * != null){ Collection colGrupos2 = null; colGrupos2 = grupoItemConfiguracaoService.listHierarquiaGrupoPaiNull(); if (colGrupos2 != null && !colGrupos2.isEmpty()){
			 * colGrupos.addAll(colGrupos2); } }
			 */

			retorno = new ArrayList();
			
			if (mudancas != null && mudancas.size() > 0) {
				for (RequisicaoMudancaDTO requisicaoMudancaDto : mudancas) {
					Collection<ItemConfiguracaoDTO> itensImpactados = new ArrayList<ItemConfiguracaoDTO>();
					Collection<RequisicaoMudancaItemConfiguracaoDTO> itensMudanca = null;
	
					itensMudanca = requisicaoMudancaItemConfiguracaoDao.findByIdRequisicaoMudanca(requisicaoMudancaDto.getIdRequisicaoMudanca());
	
					if (itensMudanca != null) {
						for (RequisicaoMudancaItemConfiguracaoDTO requisicaoItemDto : itensMudanca) {
							ItemConfiguracaoDTO itemConfiguracaoDto = new ItemConfiguracaoDTO();
							itemConfiguracaoDto.setIdItemConfiguracao(requisicaoItemDto.getIdItemConfiguracao());
							itemConfiguracaoDto = (ItemConfiguracaoDTO) itemConfiguracaoDao.restore(itemConfiguracaoDto);
	
							if (!itemValido(itemConfiguracaoDto, situacao, idTipoItemConfiguracao, idCriticidade, colGrupos)) {
								continue;
							}
	
							HashMap<String, ItemConfiguracaoDTO> mapItens = new HashMap();
							itensImpactados.add(itemConfiguracaoDto);
							mapItens.put("" + itemConfiguracaoDto.getIdItemConfiguracao(), itemConfiguracaoDto);
							
							try {
								Collection<ItemConfiguracaoDTO> colHierarq = imagemItemConfiguracaoService.findItensRelacionadosHierarquia(itemConfiguracaoDto.getIdItemConfiguracao());
								if (colHierarq != null) {
									for (ItemConfiguracaoDTO itemDto : colHierarq) {
										if (mapItens.get("" + itemDto.getIdItemConfiguracao()) != null)
											continue;
	
										if (!itemValido(itemDto, situacao, idTipoItemConfiguracao, idCriticidade, colGrupos))
											continue;
	
										mapItens.put("" + itemDto.getIdItemConfiguracao(), itemDto);
										itensImpactados.add(itemDto);
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
	
					if (itensImpactados != null && itensImpactados.size() > 0) {
						requisicaoMudancaDto.setDataHoraInicio(requisicaoMudancaDto.getDataHoraInicio());
						requisicaoMudancaDto.setStatus(requisicaoMudancaDto.getStatus());
	
						List<Object> linha = new ArrayList<Object>();
						linha.add("" + requisicaoMudancaDto.getIdRequisicaoMudanca());
						linha.add(requisicaoMudancaDto.getTitulo());
						linha.add(UtilDatas.convertDateToString(TipoDate.TIMESTAMP_WITH_SECONDS, requisicaoMudancaDto.getDataHoraSolicitacao(), super.getLanguage(paramtersDefinition)));
						linha.add(requisicaoMudancaDto.getDescrSituacao());
						if (requisicaoMudancaDto.getDataHoraConclusao() != null)
							linha.add(UtilDatas.convertDateToString(TipoDate.TIMESTAMP_WITH_SECONDS, requisicaoMudancaDto.getDataHoraConclusao(), super.getLanguage(paramtersDefinition)));
						else
							linha.add("");
	
						int i = 0;
						for (ItemConfiguracaoDTO itemDto : itensImpactados) {
							if (i > 0) {
								linha = new ArrayList();
								linha.add("" + requisicaoMudancaDto.getIdRequisicaoMudanca());
								linha.add("");
								linha.add("");
								linha.add("");
								linha.add("");
							}
							linha.add(itemDto.getIdentificacao());
							StatusIC status = StatusIC.getStatus(itemDto.getStatus());
							if (status != null)
								linha.add(status.getDescricao());
							else
								linha.add("");
							i++;
							retorno.add(linha.toArray());
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return retorno;
	}

	private boolean itemValido(ItemConfiguracaoDTO itemConfiguracaoDto, Integer situacao, Integer idTipoItemConfiguracao, Integer idCriticidade, Collection<GrupoItemConfiguracaoDTO> colGrupos) {
		if (situacao != null && situacao.intValue() != -1) {
			if (itemConfiguracaoDto.getStatus() == null && situacao.intValue() == 1)
				return true;
			if (itemConfiguracaoDto.getStatus() == null || itemConfiguracaoDto.getStatus().intValue() != situacao)
				return false;
		}

		if (idTipoItemConfiguracao != null && idTipoItemConfiguracao.intValue() != -1) {
			if (itemConfiguracaoDto.getIdTipoItemConfiguracao() == null || itemConfiguracaoDto.getIdTipoItemConfiguracao().intValue() != idTipoItemConfiguracao.intValue())
				return false;
		}

		if (idCriticidade != null && idCriticidade.intValue() != -1) {
			if (itemConfiguracaoDto.getCriticidade() == null || itemConfiguracaoDto.getCriticidade().intValue() != idCriticidade.intValue())
				return false;
		}

		if (colGrupos != null && colGrupos.size() > 0) {
			boolean bAdicionar = false;
			for (GrupoItemConfiguracaoDTO grupoDto : colGrupos) {
				if (grupoDto.getIdGrupoItemConfiguracao().intValue() == itemConfiguracaoDto.getIdGrupoItemConfiguracao()) {
					bAdicionar = true;
					break;
				}
			}
			if (!bAdicionar)
				return false;
		}
		return true;
	}

}
