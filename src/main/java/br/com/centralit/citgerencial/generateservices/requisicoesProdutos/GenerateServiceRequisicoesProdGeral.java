/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citgerencial.generateservices.requisicoesProdutos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citcorpore.bean.EmpregadoDTO;
import br.com.centralit.citcorpore.bean.HistoricoItemRequisicaoDTO;
import br.com.centralit.citcorpore.bean.ItemRequisicaoProdutoDTO;
import br.com.centralit.citcorpore.bean.RequisicaoProdutoDTO;
import br.com.centralit.citcorpore.integracao.EmpregadoDao;
import br.com.centralit.citcorpore.integracao.HistoricoItemRequisicaoDao;
import br.com.centralit.citcorpore.integracao.ItemRequisicaoProdutoDao;
import br.com.centralit.citcorpore.integracao.RequisicaoProdutoDao;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.util.Enumerados.AcaoItemRequisicaoProduto;
import br.com.centralit.citcorpore.util.Enumerados.SituacaoItemRequisicaoProduto;
import br.com.centralit.citcorpore.util.Enumerados.TipoDate;
import br.com.centralit.citgerencial.bean.GerencialGenerateServiceBuffer;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilFormatacao;
import br.com.citframework.util.UtilI18N;
import br.com.citframework.util.UtilStrings;

@SuppressWarnings({"rawtypes","unchecked", "unused"})
public class GenerateServiceRequisicoesProdGeral extends GerencialGenerateServiceBuffer {

	private RequisicaoProdutoDao requisicaoDao;
	private RequisicaoProdutoDao requisicaoDaoDataBaseAlias;
	
	private EmpregadoDao empregadoDao;
	private EmpregadoDao empregadoDaoDataBaseAlias;
	
    private ItemRequisicaoProdutoDao itemRequisicaoProdutoDao;
    private ItemRequisicaoProdutoDao itemRequisicaoProdutoDaoDataBaseAlias;
    
    private HistoricoItemRequisicaoDao historicoItemRequisicaoDao;
    private HistoricoItemRequisicaoDao historicoItemRequisicaoDaoDataBaseAlias;
	
    private HashMap novoParametro = new HashMap();
    
	private RequisicaoProdutoDao getDao() {
		if(requisicaoDao == null) {
			requisicaoDao = new RequisicaoProdutoDao();
		}
		return requisicaoDao;
	}
	
	/**
	 * Desenvolvedor: Fabio Amorim - Data: 13/08/2015 - Hor�rio: 14:48 - ID Citsmart: 176361 -
	 * Motivo/Coment�rio: Possibilitara gera��o dos relat�rios do Citsmart atrav�s do dataSsource passado por par�metro para o DAO, citsmart_reports, por exemplo.
	 * @param databaseAlias
	 * @return dao com o databaseAlias especificado, caso esteja preenchido, sen�o dao com o datasource principal
	 */
	private RequisicaoProdutoDao getDao(String databaseAlias) {
		if (UtilStrings.isNotVazio(databaseAlias)) {
			if (requisicaoDaoDataBaseAlias == null) {
				requisicaoDaoDataBaseAlias = new RequisicaoProdutoDao(databaseAlias);
			}
			return requisicaoDaoDataBaseAlias;
		}
		return this.getDao();
	}
	
	private EmpregadoDao getEmpregadoDao() {
		if(empregadoDao == null) {
			empregadoDao = new EmpregadoDao();
		}
		return empregadoDao;
	}
	
	private EmpregadoDao getEmpregadoDao(String databaseAlias) {
		if (UtilStrings.isNotVazio(databaseAlias)) {
			if (empregadoDaoDataBaseAlias == null) {
				empregadoDaoDataBaseAlias = new EmpregadoDao(databaseAlias);
			}
			return empregadoDaoDataBaseAlias;
		}
		return this.getEmpregadoDao();
	}
	
	private ItemRequisicaoProdutoDao getItemRequisicaoProdutoDao() {
		if(itemRequisicaoProdutoDao == null) {
			itemRequisicaoProdutoDao = new ItemRequisicaoProdutoDao();
		}
		return itemRequisicaoProdutoDao;
	}
	
	private ItemRequisicaoProdutoDao getItemRequisicaoProdutoDao(String databaseAlias) {
		if (UtilStrings.isNotVazio(databaseAlias)) {
			if (itemRequisicaoProdutoDaoDataBaseAlias == null) {
				itemRequisicaoProdutoDaoDataBaseAlias = new ItemRequisicaoProdutoDao(databaseAlias);
			}
			return itemRequisicaoProdutoDaoDataBaseAlias;
		}
		return this.getItemRequisicaoProdutoDao();
	}
	
	private HistoricoItemRequisicaoDao getHistoricoItemRequisicaoDao() {
		if(historicoItemRequisicaoDao == null) {
			historicoItemRequisicaoDao = new HistoricoItemRequisicaoDao();
		}
		return historicoItemRequisicaoDao;
	}
	
	private HistoricoItemRequisicaoDao getHistoricoItemRequisicaoDao(String databaseAlias) {
		if (UtilStrings.isNotVazio(databaseAlias)) {
			if (historicoItemRequisicaoDaoDataBaseAlias == null) {
				historicoItemRequisicaoDaoDataBaseAlias = new HistoricoItemRequisicaoDao(databaseAlias);
			}
			return historicoItemRequisicaoDaoDataBaseAlias;
		}
		return this.getHistoricoItemRequisicaoDao();
	}
	
	public String execute(HashMap parametersValues, Collection paramtersDefinition) throws Exception {
		
	    Set set = parametersValues.entrySet();
	    Iterator i = set.iterator();

	    while(i.hasNext()){
	      Map.Entry entrada = (Map.Entry)i.next();
	      getNovoParametro().put(entrada.getKey(), entrada.getValue());
	    }
	    
		String datainicial = (String) getNovoParametro().get("PARAM.dataInicial");
		String datafinal = (String) getNovoParametro().get("PARAM.dataFinal");
		
		Date datafim = null;
		Date datainicio = null;
		SimpleDateFormat formatoBanco = new SimpleDateFormat("yyyy-MM-dd");
		
		if (datainicial != null && !"".equals(datainicial)) {
    		try {
    			datainicio = new SimpleDateFormat("dd/MM/yyyy").parse(datainicial);
    		} catch (ParseException e) {
    			e.printStackTrace();
    		}
		}

        if (datafinal != null && !"".equals(datafinal)) {
    		try {
                datafim = new SimpleDateFormat("dd/MM/yyyy").parse(datafinal); 
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
		
        String numero = (String) getNovoParametro().get("PARAM.numero");
        if ((numero == null || numero.trim().length() == 0) && (datainicio == null || datafim == null))
            throw new LogicException("Per�odo e/ou N�mero devem ser informados");
        
        String produto = (String) getNovoParametro().get("PARAM.produto");
        String situacaoItem = (String) getNovoParametro().get("PARAM.situacaoItem");

/*        Calendar calendar = Calendar.getInstance();  
		calendar.setTime(datafim);  
		calendar.add(GregorianCalendar.DATE, 1);  */
		
        if (datainicio != null) {
    		getNovoParametro().put("PARAM.dataInicial", new java.sql.Date(datainicio.getTime()));
        }
        
        if (datafim != null) {
    		getNovoParametro().put("PARAM.dataFinal", new java.sql.Date(datafim.getTime()));
        }
		
        getNovoParametro().put("PARAM.topList", parametersValues.get("PARAM.topList"));
        
        HttpServletRequest request = null;
        // Pega o request dos par�metros
			for (Iterator iterator = paramtersDefinition.iterator(); iterator.hasNext();) {
				Object parametro = (Object) iterator.next();
				if (parametro != null && "org.apache.catalina.connector.RequestFacade".equals(parametro.getClass().getName())) {
					request = (HttpServletRequest) parametro;
					break;
				}
			}
			
		Collection col = new ArrayList();
		List listaRetorno = null;
	    String strTexto = "";
	    
		Collection<RequisicaoProdutoDTO> colRequisicoes = getDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).consultaRequisicoesPorCCusto(getNovoParametro());
		
		if (colRequisicoes != null && colRequisicoes.size() > 0) {
            strTexto += "<table border='1' width='100%' cellpadding='0' cellspacing='0'>";
		    
            int qtd = 0;
		    for (RequisicaoProdutoDTO requisicaoDto : colRequisicoes) {
		    	boolean bCabecalho = true;
		        Collection<ItemRequisicaoProdutoDTO> colItens = getItemRequisicaoProdutoDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).findTodosByIdSolicitacaoServico(requisicaoDto.getIdSolicitacaoServico());
		        if (colItens == null)
		            continue;
		        
                requisicaoDto.setDataHoraSolicitacao(requisicaoDto.getDataHoraSolicitacao());
                for (ItemRequisicaoProdutoDTO itemDto : colItens) {
                    if (produto != null && produto.trim().length() > 0) {
                        if (itemDto.getDescricaoItem().toUpperCase().indexOf(produto.toUpperCase().trim()) < 0)
                            continue;
                    }
                    if (situacaoItem != null && !situacaoItem.trim().equals("*")) {
                        if (!itemDto.getSituacao().equalsIgnoreCase(situacaoItem))
                            continue;
                    }
                    if (bCabecalho) {
                    	if (qtd > 0) {
                        strTexto += "  <tr>";
                            strTexto += "    <td colspan='8' style='border:none'>&nbsp;</td>";
                            strTexto += "</tr>";
                    	}

                    	strTexto += "  <tr>";
                        strTexto += "    <th width='5%' style='background:#eee;font-size:11px'><b>" + UtilI18N.internacionaliza(request, "citcorpore.comum.numero") + "</b></th>";
                        strTexto += "    <th width='15%' style='background:#eee;font-size:11px'><b>" + UtilI18N.internacionaliza(request, "citcorpore.comum.centroCusto") + "</b></th>";
                        strTexto += "    <th width='15%' style='background:#eee;font-size:11px'><b>" + UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.projeto") + "</b></th>";
                        strTexto += "    <th width='15%' style='background:#eee;font-size:11px'><b>" + UtilI18N.internacionaliza(request, "citcorporeRelatorio.comum.solicitante") + "</b></th>";
                        strTexto += "    <th width='10%' style='background:#eee;font-size:11px'><b>" + UtilI18N.internacionaliza(request, "citcorpore.comum.datahora") + "</b></th>";
                        strTexto += "    <th style='background:#eee;font-size:11px'><b>" + UtilI18N.internacionaliza(request, "coletaPreco.item") + "</b></th>";
                        strTexto += "    <th width='5%' style='background:#eee;font-size:11px'><b>" + UtilI18N.internacionaliza(request, "itemRequisicaoProduto.quantidade") + "</b></th>";
                        strTexto += "    <th width='10%' style='background:#eee;font-size:11px'><b>" + UtilI18N.internacionaliza(request, "lookup.status") + "</b></th>";
                        strTexto += "  </tr>";
                        bCabecalho = false;
                    }
                    
                    strTexto += "<tr>";
                    strTexto += "   <td style='padding:2px 5px;font-size:11px'>"+requisicaoDto.getIdSolicitacaoServico()+"</td>";
                    strTexto += "   <td style='padding:2px 5px;font-size:11px'>"+requisicaoDto.getCentroCusto()+"</td>";
                    strTexto += "   <td style='padding:2px 5px;font-size:11px'>"+requisicaoDto.getProjeto()+"</td>";
                    strTexto += "   <td style='padding:2px 5px;font-size:11px'>"+requisicaoDto.getSolicitante()+"</td>";
                    strTexto += "   <td style='padding:2px 5px;font-size:11px'>"+UtilDatas.convertDateToString(TipoDate.TIMESTAMP_WITH_SECONDS, requisicaoDto.getDataHoraSolicitacao(), null)+"</td>";
                    strTexto += "   <td style='padding:2px 5px;font-size:11px'>"+itemDto.getDescricaoItem()+"</td>";
                    strTexto += "   <td style='padding:2px 5px;font-size:11px'>"+UtilFormatacao.formatDouble(itemDto.getQtdeAprovada(),2)+"</td>";
                    strTexto += "   <td style='padding:2px 5px;font-size:11px'>"+itemDto.getDescrSituacao()+"</td>";
                    strTexto += "</tr>";

                    qtd ++;
                    Collection<HistoricoItemRequisicaoDTO> colHistorico = getHistoricoItemRequisicaoDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).findByIdItemRequisicao(itemDto.getIdItemRequisicaoProduto());
                    if (colHistorico == null)
                        continue;

                    strTexto += "<tr>";
                    strTexto += "    <td colspan='1'>&nbsp</td>";
                    strTexto += "    <td colspan='7'>";
                    strTexto += "       <table border='1' width='100%' cellpadding='0' cellspacing='0'>";
                    strTexto += "           <tr><td></td></tr>";
                    strTexto += "           <tr>";
                    strTexto += "               <th width='15%' style='background:#eee;font-size:11px'><b>" + UtilI18N.internacionaliza(request, "citcorpore.comum.datahora") + "</b></th>";
                    strTexto += "               <th width='20%' style='background:#eee;font-size:11px'><b>" + UtilI18N.internacionaliza(request, "planoMelhoria.monitoramento.responsavel") + "</b></th>";
                    strTexto += "               <th width='20%' style='background:#eee;font-size:11px'><b>" + UtilI18N.internacionaliza(request, "rh.acao") + "</b></th>";                    
                    strTexto += "               <th width='20%' style='background:#eee;font-size:11px'><b>" + UtilI18N.internacionaliza(request, "lookup.status") + "</b></th>";
                    strTexto += "               <th style='background:#eee;font-size:11px'><b>" + UtilI18N.internacionaliza(request, "produto.detalhes") + "</b></th>";
                    strTexto += "           </tr>";
                    
                    for (HistoricoItemRequisicaoDTO historicoDto : colHistorico) {
                    	EmpregadoDTO empregadoDto = new EmpregadoDTO();
                    	
                    	if(historicoDto.getIdResponsavel()!=null){
                    		empregadoDto = getEmpregadoDao(CITCorporeUtil.JDBC_ALIAS_REPORTS).restoreByIdEmpregado(historicoDto.getIdResponsavel());
                    	}
                    	
                        String nome = "";
                        
                        if (empregadoDto.getNome() != null) {
                            nome = empregadoDto.getNome();
                        }
                        
                        strTexto += "<tr>";
                        strTexto += "   <td style='padding:2px 5px;font-size:11px'>"+UtilDatas.convertDateToString(TipoDate.TIMESTAMP_WITH_SECONDS, historicoDto.getDataHora(), null)+"</td>";
                        strTexto += "   <td style='padding:2px 5px;font-size:11px'>"+nome+"</td>";
                        
                        /* Desenvolvedor: Thiago Matias - Data: 31/10/2013 - Hor�rio: 19:30 - ID Citsmart: 122665 - 
                		* Motivo/Coment�rio: Condi��o para adicionar a a��o somente se for diferente de null  */
                        if (historicoDto.getAcao() != null){
	                        strTexto += "   <td style='padding:2px 5px;font-size:11px'>"+AcaoItemRequisicaoProduto.valueOf(historicoDto.getAcao()).getDescricao()+"</td>";
                        } else {
	                        strTexto += "   <td style='padding:2px 5px;font-size:11px'></td>";
                        }
                        
	                    strTexto += "   <td style='padding:2px 5px;font-size:11px'>"+SituacaoItemRequisicaoProduto.valueOf(historicoDto.getSituacao()).getDescricao()+"</td>";
                        strTexto += "   <td style='padding:2px 5px;font-size:11px'>"+UtilStrings.nullToVazio(historicoDto.getComplemento())+"</td>";
                        strTexto += "</tr>";
                       
                    }
                    strTexto += "       </table>";
                    strTexto += "    </td>";
                    strTexto += "</tr>";
                }
            }
            strTexto += "</table>";
		}else{
			strTexto = "<table border='1' width='100%' cellpadding='0' cellspacing='0'>";
			strTexto += "	<tr>";
			strTexto += "   	<td style='padding:2px 5px;font-size:11px;text-align: center;'>" + UtilI18N.internacionaliza(request, "citcorpore.comum.resultado") + "</td>";
			strTexto += "	</tr>";
			strTexto += "</table>";
		}
        return strTexto;
        //return UtilHTML.encodeHTML(strTexto);
	}
	
	public HashMap getNovoParametro() {
		return novoParametro;
	}

	public void setNovoParametro(HashMap novoParametro) {
		this.novoParametro = novoParametro;
	}

}
