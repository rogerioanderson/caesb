/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citgerencial.util;

import javax.servlet.http.HttpServletRequest;

import br.com.citframework.dto.Usuario;
import br.com.citframework.util.Constantes;

public class WebUtilGerencial {

    public static void setUsuario(final Usuario usuario, final HttpServletRequest req) {
        String USUARIO_SESSAO = Constantes.getValue("USUARIO_SESSAO");
        if (USUARIO_SESSAO == null) {
            USUARIO_SESSAO = "USUARIO_SESSAO";
        }
        req.getSession().setAttribute(USUARIO_SESSAO, usuario);

    }

    public static Usuario getUsuario(final HttpServletRequest req) {
        String USUARIO_SESSAO = Constantes.getValue("USUARIO_SESSAO");
        if (USUARIO_SESSAO == null) {
            USUARIO_SESSAO = "USUARIO_SESSAO";
        }
        Usuario user = (Usuario) req.getSession().getAttribute(USUARIO_SESSAO);
        if (user == null) {
            final Usuario usr = new Usuario();
            usr.setIdUsuario("1");
            usr.setNomeUsuario("EMAURI GOMES GASPAR JUNIOR");
            usr.setMatricula("1");
            usr.setIdProfissional(new Integer(1));
            usr.setIdEmpresa(new Integer(1));

            final String[] grupos = new String[] {"grupoteste"};
            usr.setGrupos(grupos);

            setUsuario(usr, req);
            user = usr;
        }
        return user;
    }

}
