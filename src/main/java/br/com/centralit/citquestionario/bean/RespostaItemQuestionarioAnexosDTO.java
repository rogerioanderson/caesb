/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citquestionario.bean;

import br.com.citframework.dto.IDto;

public class RespostaItemQuestionarioAnexosDTO implements IDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2088779287936858372L;
	private Integer idRespostaItmQuestionarioAnexo;
	private Integer idRespostaItemQuestionario;
	private String caminhoAnexo;
	private String observacao;
	private Integer idQuestaoQuestionario;
	private String nomeArquivo;
	private Integer idControleGED;
	public Integer getIdRespostaItmQuestionarioAnexo() {
		return idRespostaItmQuestionarioAnexo;
	}
	public void setIdRespostaItmQuestionarioAnexo(
			Integer idRespostaItmQuestionarioAnexo) {
		this.idRespostaItmQuestionarioAnexo = idRespostaItmQuestionarioAnexo;
	}
	public Integer getIdRespostaItemQuestionario() {
		return idRespostaItemQuestionario;
	}
	public void setIdRespostaItemQuestionario(Integer idRespostaItemQuestionario) {
		this.idRespostaItemQuestionario = idRespostaItemQuestionario;
	}
	public String getCaminhoAnexo() {
		return caminhoAnexo;
	}
	public void setCaminhoAnexo(String caminhoAnexo) {
		this.caminhoAnexo = caminhoAnexo;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Integer getIdQuestaoQuestionario() {
		return idQuestaoQuestionario;
	}
	public void setIdQuestaoQuestionario(Integer idQuestaoQuestionario) {
		this.idQuestaoQuestionario = idQuestaoQuestionario;
	}
	public String getNomeArquivo() {
		return nomeArquivo;
	}
	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	public Integer getIdControleGED() {
		return idControleGED;
	}
	public void setIdControleGED(Integer idControleGED) {
		this.idControleGED = idControleGED;
	}
}
