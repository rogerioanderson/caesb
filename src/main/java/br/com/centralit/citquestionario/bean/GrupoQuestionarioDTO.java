/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citquestionario.bean;

import java.util.Collection;

import br.com.citframework.dto.IDto;

public class GrupoQuestionarioDTO implements IDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8473113003183923972L;
	private Integer idGrupoQuestionario;
	private Integer idQuestionario;
	private String nomeGrupoQuestionario;
	private Integer ordem;
	
	private String infoGrupo;
	
	private String serializeQuestoesGrupo;
	private Collection colQuestoes;
	private Integer idControleTabela;
	
	public Integer getIdGrupoQuestionario() {
		return idGrupoQuestionario;
	}
	public void setIdGrupoQuestionario(Integer idGrupoQuestionario) {
		this.idGrupoQuestionario = idGrupoQuestionario;
	}
	public Integer getIdQuestionario() {
		return idQuestionario;
	}
	public void setIdQuestionario(Integer idQuestionario) {
		this.idQuestionario = idQuestionario;
	}
	public String getNomeGrupoQuestionario() {
		return nomeGrupoQuestionario;
	}
	public void setNomeGrupoQuestionario(String nomeGrupoQuestionario) {
		this.nomeGrupoQuestionario = nomeGrupoQuestionario;
	}
	public String getInfoGrupo() {
		return infoGrupo;
	}
	public void setInfoGrupo(String infoGrupo) {
		this.infoGrupo = infoGrupo;
	}
	public String getSerializeQuestoesGrupo() {
		return serializeQuestoesGrupo;
	}
	public void setSerializeQuestoesGrupo(String serializeQuestoesGrupo) {
		this.serializeQuestoesGrupo = serializeQuestoesGrupo;
	}
	public Collection getColQuestoes() {
		return colQuestoes;
	}
	public void setColQuestoes(Collection colQuestoes) {
		this.colQuestoes = colQuestoes;
	}
	public Integer getIdControleTabela() {
		return idControleTabela;
	}
	public void setIdControleTabela(Integer idControleTabela) {
		this.idControleTabela = idControleTabela;
	}
	public Integer getOrdem() {
		return ordem;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
}
