/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citquestionario.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import br.com.centralit.citquestionario.bean.RespostaItemAuxiliarDTO;
import br.com.citframework.util.Constantes;

public class WebValuesQuestionario {
	/**
	 * Atribui para a Collection os valores da requisicao.
	 * @return
	 */	
	public static Collection getFormValues(HttpServletRequest req){
		Collection colRespostas = new ArrayList();
		Enumeration en = req.getParameterNames();
		String[] strValores;
		String aux;
		int i;
		if ("S".equalsIgnoreCase(Constantes.getValue("IMPRIMIR_FORMULARIO_POSTADO_QUEST"))){
			System.out.println("****** >INFORMACOES DO FORMULARIO POSTADO ******");
		}
		while(en.hasMoreElements()) {
			String nomeCampo  = (String)en.nextElement();
			strValores = req.getParameterValues(nomeCampo);
			
			RespostaItemAuxiliarDTO respostaItem = new RespostaItemAuxiliarDTO();
			if (strValores.length == 0){
				respostaItem.setFieldName(nomeCampo.toUpperCase());
				respostaItem.setFieldValue(req.getParameter(nomeCampo));
				respostaItem.setMultiple(false);
				
				colRespostas.add(respostaItem);
				if ("S".equalsIgnoreCase(Constantes.getValue("IMPRIMIR_FORMULARIO_POSTADO_QUEST"))){
					System.out.println("===> " + nomeCampo + " : " + req.getParameter(nomeCampo));
				}
			} else {
				aux = "";
				for (i = 0; i < strValores.length; i++){
					if (!aux.equals("")){
						aux = aux + ConstantesQuestionario.CARACTER_SEPARADOR;
					}
					aux = aux + strValores[i]; 			
				}
				respostaItem.setFieldName(nomeCampo.toUpperCase());
				respostaItem.setFieldValue(aux);
				respostaItem.setMultiple(true);
				
				colRespostas.add(respostaItem);
				if ("S".equalsIgnoreCase(Constantes.getValue("IMPRIMIR_FORMULARIO_POSTADO_QUEST"))){
					System.out.println("===> " + nomeCampo + " : " + aux);
				}
			}
		}
		if ("S".equalsIgnoreCase(Constantes.getValue("IMPRIMIR_FORMULARIO_POSTADO_QUEST"))){
			System.out.println("***********************************************");
		}
		return colRespostas;
	}

}
