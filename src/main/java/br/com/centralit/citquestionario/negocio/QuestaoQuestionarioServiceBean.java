/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citquestionario.negocio;

import java.util.Collection;

import br.com.centralit.citquestionario.bean.QuestaoQuestionarioDTO;
import br.com.centralit.citquestionario.integracao.QuestaoQuestionarioDao;
import br.com.citframework.service.CrudServiceImpl;

public class QuestaoQuestionarioServiceBean extends CrudServiceImpl implements QuestaoQuestionarioService {

    private QuestaoQuestionarioDao dao;

    @Override
    protected QuestaoQuestionarioDao getDao() {
        if (dao == null) {
            dao = new QuestaoQuestionarioDao();
        }
        return dao;
    }

    @Override
    public Collection listByIdGrupoQuestionario(final Integer idGrupoQuestionario) throws Exception {
        return this.getDao().listByIdGrupoQuestionario(idGrupoQuestionario);
    }

    @Override
    public Collection listByIdGrupoQuestionarioComAgrupadoras(final Integer idGrupoQuestionario) throws Exception {
        return dao.listByIdGrupoQuestionarioComAgrupadoras(idGrupoQuestionario);
    }

    @Override
    public Collection listByIdQuestaoAgrupadora(final Integer idQuestaoAgrupadora) throws Exception {
        return this.getDao().listByIdQuestaoAgrupadora(idQuestaoAgrupadora);
    }

    public Collection listCabecalhosLinha(final Integer idQuestaoAgrupadora) throws Exception {
        return this.getDao().listCabecalhosLinha(idQuestaoAgrupadora);
    }

    public Collection listCabecalhosColuna(final Integer idQuestaoAgrupadora) throws Exception {
        return this.getDao().listCabecalhosColuna(idQuestaoAgrupadora);
    }

    @Override
    public QuestaoQuestionarioDTO findBySiglaAndIdQuestionario(final String sigla, final Integer idQuestionario) throws Exception {
        return this.getDao().findBySiglaAndIdQuestionario(sigla, idQuestionario);
    }

    @Override
    public Collection listByTipoQuestaoAndIdQuestionario(final String tipoQuestao, final Integer idQuestionario) throws Exception {
        return dao.listByTipoQuestaoAndIdQuestionario(tipoQuestao, idQuestionario);
    }

    @Override
    public Collection listByTipoAndIdQuestionario(final String tipo, final Integer idQuestionario) throws Exception {
        return this.getDao().listByTipoAndIdQuestionario(tipo, idQuestionario);
    }

    @Override
    public Collection listByIdQuestaoAndContrato(final Integer idQuestao, final Integer idContrato) throws Exception {
        return this.getDao().listByIdQuestaoAndContrato(idQuestao, idContrato);
    }

    @Override
    public Collection listByIdQuestaoAndContratoOrderDataASC(final Integer idQuestao, final Integer idContrato) throws Exception {
        return this.getDao().listByIdQuestaoAndContratoOrderDataASC(idQuestao, idContrato);
    }

}
