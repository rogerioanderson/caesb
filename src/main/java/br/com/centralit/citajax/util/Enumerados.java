/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/

package br.com.centralit.citajax.util;

import java.io.Serializable;

public class Enumerados implements Serializable {

	private static final long serialVersionUID = 2466679077707063321L;

	/**
	 * Enumerado para armazenar os Tipos de Data.
	 * 
	 * DATE_DEFAULT: dd/MM/yyyy ou MM/dd/yyyy, TIMESTAMP_DEFAULT: dd/MM/yyyy HH:mm ou MM/dd/yyyy HH:mm, TIMESTAMP_WITH_SECONDS: dd/MM/yyyy HH:mm:ss ou MM/dd/yyyy HH:mm:ss, FORMAT_DATABASE: yyyy-MM-dd
	 * 
	 * @author valdoilo.damasceno
	 * @since 04.02.2013
	 */
	public enum TipoDate {
		DATE_DEFAULT("DATE_DEFAULT"), TIMESTAMP_DEFAULT("TIMESTAMP_DEFAULT"), TIMESTAMP_WITH_SECONDS("TIMESTAMP_WITH_SECONDS"), FORMAT_DATABASE("FORMAT_DATABASE");

		private String tipoData;

		TipoDate(String tipoDate) {
			this.tipoData = tipoDate;
		}

		public String getTipoDate() {
			return tipoData;
		}
	}

}
