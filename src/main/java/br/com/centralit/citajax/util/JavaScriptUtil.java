/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citajax.util;

public class JavaScriptUtil {
    public static String escapeJavaScript(String str) {
        if (str == null) {
            return null;
        }
        StringBuilder writer = new StringBuilder();
        int sz;
        sz = str.length();
        for (int i = 0; i < sz; i++) {
            char ch = str.charAt(i);
            switch (ch) {
	            case '\b':
	                writer.append('\\');
	                writer.append('b');
	                break;
	            case '\n':
	                writer.append('\\');
	                writer.append('n');
	                break;
	            case '\t':
	                writer.append('\\');
	                writer.append('t');
	                break;
	            case '\f':
	                writer.append('\\');
	                writer.append('f');
	                break;
	            case '\r':
	                writer.append('\\');
	                writer.append('r');
	                break;
	            case '\'':
	                writer.append('\\');
	                writer.append('\'');
	                break;
	            case '"':
	                writer.append('\\');
	                writer.append('"');
	                break;
	            case '\\':
	                writer.append('\\');
	                writer.append('\\');
	                break;
                default :
                    writer.append(ch);
                    break;
            }
        }
        return writer.toString();
    }
}
