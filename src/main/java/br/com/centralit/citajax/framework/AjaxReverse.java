/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * 
 */
package br.com.centralit.citajax.framework;

import org.directwebremoting.Browser;
import org.directwebremoting.ScriptSessions;

/**
 * @author valdoilo.damasceno
 * 
 */
public class AjaxReverse extends Browser {

	/**
	 * Executa o Ajax Reverso chamando uma fun��o JavaScript com seus respectivos par�metros.
	 * 
	 * @param nomeDaFuncaoJavaScript
	 *            - Nome da Fun��o JavaScript.
	 * @param parametrosDaFuncao
	 *            - Par�metros da Fun��o JavaScript.
	 * @since 21.11.2013
	 * @author valdoilo.damasceno
	 */
	public static void executarAjaxReverseWithCurrentPage(final String nomeDaFuncaoJavaScript, final Object... parametrosDaFuncao) {

		Browser.withCurrentPage(new Runnable() {

			public void run() {

				ScriptSessions.addFunctionCall(nomeDaFuncaoJavaScript, parametrosDaFuncao);

			}

		});
	}

	/**
	 * Executa o Ajax Reverso chamando uma fun��o JavaScript com seus respectivos par�metros.
	 * 
	 * @param nomeDaFuncaoJavaScript
	 *            - Nome da Fun��o JavaScript.
	 * @param parametrosDaFuncao
	 *            - Par�metros da Fun��o JavaScript.
	 * @since 28.11.2013
	 * @author valdoilo.damasceno
	 */
	public static void executarAjaxReverseWithAllSessions(final String nomeDaFuncaoJavaScript, final Object... parametrosDaFuncao) {

		Browser.withAllSessions(new Runnable() {

			public void run() {

				ScriptSessions.addFunctionCall(nomeDaFuncaoJavaScript, parametrosDaFuncao);

			}

		});
	}
}
