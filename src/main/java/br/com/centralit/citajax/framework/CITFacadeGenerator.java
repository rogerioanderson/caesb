/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.citajax.framework;

import java.util.List;

import javax.servlet.ServletContext;

import br.com.centralit.citajax.reflexao.CitAjaxReflexao;
import br.com.centralit.citajax.util.CitAjaxUtil;
import br.com.citframework.util.Constantes;

public class CITFacadeGenerator {

    /**
     * Processa o objeto passado como parametro e retorna uma string javascript
     *
     * @param path
     * @return
     * @throws Exception
     */
    public String process(final String path, final ServletContext ctx) throws Exception {
        String strAux = "";
        final String javaScriptObject = this.getObjectName(path);
        if (javaScriptObject == null) {
            return null;
        }
        final Class<?> classe = Class.forName(Constantes.getValue("BEAN_LOCATION_PACKAGE") + "." + javaScriptObject);
        if (classe != null) {
            final Object objeto = classe.newInstance();
            final List<String> listaGets = CitAjaxReflexao.findGets(objeto);

            strAux = "function " + javaScriptObject + "(";
            String strAux2 = "";
            for (int i = 0; i < listaGets.size(); i++) {
                if (listaGets.get(i).equalsIgnoreCase("class")) {
                    continue;
                }
                strAux2 += "\tthis." + CitAjaxUtil.convertePrimeiraLetra(listaGets.get(i), "L") + " = function(){;\n";
                strAux2 += "\t\t";
                strAux2 += "\t}";
            }
            strAux2 += "\tthis.idControleCITFramework = null;\n";

            strAux += "){\n";
            strAux += strAux2;
            strAux += "} ";
        }

        return strAux;
    }

    public String getObjectName(final String path) {
        if (path.length() - 3 <= 0) {
            return "";
        }
        String strResult = "";
        for (int i = path.length() - 4; i >= 0; i--) {
            if (path.charAt(i) == '/') {
                return strResult;
            } else {
                strResult = path.charAt(i) + strResult;
            }
        }
        return strResult;
    }

}
