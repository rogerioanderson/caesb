/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.testeselenium.helper;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;

public abstract class CitTeste
{
	public static WebDriver driver;
	public static String baseUrl;
	public static String usuario;
	public static String senha;
	public static String navegador;
	
	public CitTeste()	{
		
		try {
			
			usuario = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SELENIUM_USUARIO_TESTE, "consultor");
			senha = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SELENIUM_SENHA_TEST, "1");
			baseUrl = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SELENIUM_URL_TESTE, "localhost:8080/citsmart");
			navegador = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SELENIUM_NAVEGADOR_TESTE, "Google Chrome");
			
			if(!baseUrl.contains("http://"))
				baseUrl = "http://" + baseUrl; 

			String ipCliente = ParametroUtil.getValor(ParametroSistema.SELENIUM_URL_CLIENTE_TESTE);
				
			// Execu��o automatica sem a necidade da tela
			// ipCliente == null
			
			if(ipCliente != null && ipCliente.trim().length() > 0){
				
				DesiredCapabilities capability = new DesiredCapabilities();
				
				if(navegador.equalsIgnoreCase("chrome") || navegador.equalsIgnoreCase("Google Chrome")){

					capability = DesiredCapabilities.chrome();
					
				} else if(navegador.equalsIgnoreCase("ie") || navegador.equalsIgnoreCase("internetexplorer") || navegador.equalsIgnoreCase("Internet Explorer")){
					
					capability = DesiredCapabilities.internetExplorer();
					
				} else {
					
					capability = DesiredCapabilities.firefox();
					
				}
				
				ipCliente = "http://" + ipCliente + ":4444/wd/hub";
				
				driver = new RemoteWebDriver(new URL(ipCliente), capability);
				
			} else {
				
				if(navegador.equalsIgnoreCase("chrome") || navegador.equalsIgnoreCase("Google Chrome")){

	                System.setProperty("webdriver.chrome.driver", ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SELENIUM_CAMINHO_COMPLETO_DRIVER_CHROME, "Google Chrome"));
					
					driver = new ChromeDriver();
					
				} else if(navegador.equalsIgnoreCase("ie") || navegador.equalsIgnoreCase("internetexplorer") || navegador.equalsIgnoreCase("Internet Explorer")){

		            System.setProperty("webdriver.ie.driver", ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.SELENIUM_CAMINHO_COMPLETO_DRIVER_IE, "Internet Explorer"));
					
					driver = new InternetExplorerDriver();
					
				} else {
					
					driver = new FirefoxDriver();
						
				}
				
			}
			
		} catch (Exception e) {
			System.out.println("Erro nas configura��es de teste:");
			System.out.println(e.toString());
		}
		
		tempoDeEsperaMaximo(30);
		
		maximizarTela();
		
	}
	
	public void maximizarTela(){
		driver.manage().window().maximize();
	}
	
	public static void minimizarTela(){
		
		driver.manage().window().setPosition(new Point(-2000, 0));
	}
	
	public static void minimizarTela(Integer x, Integer y){
		
		if(x == null)
			x = -2000;
		
		if(y == null)
			y = 0;
		
		driver.manage().window().setPosition(new Point(x, y));
	}
	
	public void tempoDeEsperaMaximo(int tempoEmSegundos){
		
		driver.manage().timeouts().implicitlyWait(tempoEmSegundos, TimeUnit.SECONDS);
		
	}

}
