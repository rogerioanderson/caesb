/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.testeselenium.helper;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public abstract class TesteCitsmart extends CitTeste {

    private static String page;

    public static void login(WebDriver driver, String baseUrl, String usuario, String senha) {
        setPage("/pages/login/login.load");

        driver.get(baseUrl + getPage());
        driver.findElement(By.id("user")).clear();
        driver.findElement(By.id("user")).sendKeys(usuario);
        driver.findElement(By.id("senha")).clear();
        driver.findElement(By.id("senha")).sendKeys(senha);

        driver.findElement(By.xpath("//button[@id='']")).click();

        UtilWebSelenium.waitForPageToLoad(driver);
    }

    public static void logout(WebDriver driver) {
        driver.findElement(By.xpath("//form[@id='formInternacionaliza']/ul/li[3]/a/span")).click();

        driver.findElement(By.linkText("Sair")).click();
    }

    public static Boolean pesquisarRegistro(String nomeDoBotaoDeLimpar, String nomeDoLinkDaAbaDePesquisa, String nomeDoCampoDePesquisa, String nomeDoBotaoDePesquisa,
            String xPathDoPrimeiroItemDaLista, String valorObjeto) {
        try {

            driver.findElement(By.linkText(nomeDoLinkDaAbaDePesquisa)).click();

            UtilWebSelenium.aguardar(1000);

            if (nomeDoBotaoDeLimpar != null && !nomeDoBotaoDeLimpar.trim().equals(""))
                driver.findElement(By.name(nomeDoBotaoDeLimpar)).click();

            driver.findElement(By.id(nomeDoCampoDePesquisa)).clear();
            driver.findElement(By.id(nomeDoCampoDePesquisa)).sendKeys(valorObjeto);
            driver.findElement(By.id(nomeDoBotaoDePesquisa)).click();

            UtilWebSelenium.aguardar(3000);

            driver.findElement(By.xpath(xPathDoPrimeiroItemDaLista)).click();

        } catch (Exception e) {
            return false;
        }

        UtilWebSelenium.waitForPageToLoad(driver);

        return true;
    }

    public static String getPage() {
        return page;
    }

    public static void setPage(String pagina) {
        page = pagina;
    }

}
