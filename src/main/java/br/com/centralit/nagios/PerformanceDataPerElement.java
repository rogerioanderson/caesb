/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/* Released under the GPL2. See license.txt for details. */
package br.com.centralit.nagios;

import java.util.ArrayList;
import java.util.List;

public class PerformanceDataPerElement
{
	String lastCheckTime = "";
	final protected List<DataSource> dataSources = new ArrayList<DataSource>();

	public DataSource add(String source)
	{
		for(DataSource current : dataSources)
		{
			if (current.getDataSourceName().equals(source))
				return current;
		}

		DataSource newDataSource = new DataSource(source);
		dataSources.add(newDataSource);

		return newDataSource;
	}

	public void setDataSourceUnit(String name, String unit)
	{
		getDataSource(name).setUnit(unit);
	}

	public void setCheckTime(String checkTime)
	{
		lastCheckTime = checkTime;
	}

	public DataSource add(String source, double value)
	{
		DataSource newDataSource = add(source);

		newDataSource.add(value);

		return newDataSource;
	}

	public String getCheckTime()
	{
		return lastCheckTime;
	}

	public DataSource getDataSource(String name)
	{
		for(DataSource current : dataSources)
		{
			if (current.getDataSourceName().equals(name))
				return current;
		}

		return null;
	}

	public List<DataSource> getAllDataSources()
	{
		return dataSources;
	}
}
