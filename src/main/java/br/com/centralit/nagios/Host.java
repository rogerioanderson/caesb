/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/* Released under the GPL2. See license.txt for details. */
package br.com.centralit.nagios;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Host contains all host- and servicechecks results for one host.
 * 
 * @author Folkert van Heusden
 * @version %I%, %G%
 * @since 0.1
 */
public class Host {

	String hostName;

	String nagiosSource;

	List<ParameterEntry> hostEntries = new ArrayList<ParameterEntry>();

	List<Service> services = new ArrayList<Service>();

	/**
	 * @param hostName
	 *            Name of the host this object describes.
	 */
	public Host(String nagiosSource, String hostName) {
		this.nagiosSource = nagiosSource;
		this.hostName = hostName;
	}

	public String getNagiosSource() {
		return nagiosSource;
	}

	/**
	 * @return The hostname set in the constructor.
	 */
	public String getHostName() {
		return hostName;
	}

	/**
	 * Adiciona um novo servi�o ao host.
	 * 
	 * @param serviceName
	 *            Nome do servi�o.
	 * @return Service - Servi�o.
	 */
	Service addAndOrFindService(String serviceName) {
		for (Service currentService : services) {
			if (currentService.getServiceName().equals(serviceName))
				return currentService;
		}

		Service newService = new Service(serviceName);

		services.add(newService);

		return newService;
	}

	/**
	 * @return Returns a list of all services checked for this host.
	 */
	public List<Service> getServices() {
		return services;
	}

	/**
	 * Each host has a couple of parameters as well. These can be obtained using this method.
	 * 
	 * @return A List of 'ParameterEntry'-objects defining each parameter of this host
	 */
	public List<ParameterEntry> getParameters() {
		return hostEntries;
	}

	/**
	 * Get the value of a host-parameter.
	 * 
	 * @return A string with the value. Indeed, also values are returned as a string.
	 */
	public String getParameter(String parameter) {
		for (ParameterEntry parameterEntry : hostEntries) {
			if (parameterEntry.getParameterName().equals(parameter))
				return parameterEntry.getParameterValue();
		}

		return null;
	}

	/**
	 * Adiciona um novo ParameterEntry (Parametro/Valor) ao Host.
	 * 
	 * @param hostParameterName
	 *            Nome do par�metro do host.
	 * @param hostParameterValue
	 *            Valor do par�metro do host.
	 * @return ParameterEntry - Parametro/Valor.
	 */
	public ParameterEntry addParameter(String hostParameterName, String hostParameterValue) {

		for (ParameterEntry currentHostEntry : hostEntries) {
			if (currentHostEntry.getParameterName().equals(hostParameterName))
				return currentHostEntry;
		}

		ParameterEntry currentHostEntry = new ParameterEntry(hostParameterName, hostParameterValue);

		hostEntries.add(currentHostEntry);

		return currentHostEntry;
	}

	/**
	 * Add a parameter/value pair to a service of this host.
	 * 
	 * @param serviceName
	 *            Service to which this parameter/value pair applies.
	 * @param serviceParameterName
	 *            Name of the parameter.
	 * @param serviceParameterValue
	 *            Parameter value.
	 * @return Altered service
	 */
	public Service addServiceEntry(String serviceName, String serviceParameterName, String serviceParameterValue) {
		ParameterEntry parameterEntry = new ParameterEntry(serviceParameterName, serviceParameterValue);

		for (Service curService : services) {
			if (curService.getServiceName().equals(serviceName)) {
				curService.addParameter(parameterEntry);
				return curService;
			}
		}

		Service newService = new Service(serviceName);
		newService.addParameter(parameterEntry);
		services.add(newService);

		return newService;
	}

	public Service getService(String serviceName) {
		for (Service currentService : services) {
			if (currentService.getServiceName().equals(serviceName))
				return currentService;
		}

		return null;
	}
}
