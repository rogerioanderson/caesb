/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/* Released under the GPL2. See license.txt for details. */
package br.com.centralit.nagios;

/**
 * Class Totals is only used to return counts.
 *
 * @author	Folkert van Heusden
 * @version	%I%, %G%
 * @since	0.1 
 */
public class Totals {
	int nCritical, nWarning, nOk;
	int nUp, nDown, nUnreachable, nPending;
	int nHosts, nServices;
	int nStateUnknownHost, nStateUnknownService;
	int nAcked, nFlapping;

	public Totals(int nCritical, int nWarning, int nOk, int nUp, int nDown, int nUnreachable, int nPending, int nHosts, int nServices, int nStateUnknownHost, int nStateUnknownService, int nAcked, int nFlapping) {
		this.nCritical = nCritical;
		this.nWarning = nWarning;
		this.nOk = nOk;
		this.nUp = nUp;
		this.nDown = nDown;
		this.nUnreachable = nUnreachable;
		this.nPending = nPending;
		this.nHosts = nHosts;
		this.nServices = nServices;
		this.nStateUnknownHost = nStateUnknownHost;
		this.nStateUnknownService = nStateUnknownService;
		this.nAcked = nAcked;
		this.nFlapping = nFlapping;
	}

	public int getNCritical() {
		return nCritical;
	}

	public int getNWarning() {
		return nWarning;
	}

	public int getNOk() {
		return nOk;
	}

	public int getNUp() {
		return nUp;
	}

	public int getNDown() {
		return nDown;
	}

	public int getNUnreachable() {
		return nUnreachable;
	}

	public int getNPending() {
		return nPending;
	}

	public int getNHosts() {
		return nHosts;
	}

	public int getNServices() {
		return nServices;
	}

	public int getNStateUnknownHost() {
		return nStateUnknownHost;
	}

	public int getNStateUnknownService() {
		return nStateUnknownService;
	}

	public int getNAcked() {
		return nAcked;
	}

	public int getNFlapping() {
		return nFlapping;
	}
}
