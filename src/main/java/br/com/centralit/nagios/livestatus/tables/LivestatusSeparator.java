/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.centralit.nagios.livestatus.tables;

/**
 * 
 * @author adenir
 */
public class LivestatusSeparator {

	protected static final int P_SEP = '\n';
	protected static final int S_SEP = '|';
	protected static final int T_SEP = '>';
	protected static final int Q_SEP = '}';

	public static String getSeparators() {
		return String.format("%d %d %d %d", P_SEP, S_SEP, T_SEP, Q_SEP);
	}

	/**
	 * Get the value of SEP1 (\n)
	 * 
	 * @return the value of P_SEP
	 */
	public static String SEP1() {
		return String.format("%c", P_SEP); // \n
	}

	/**
	 * Get the value of SEP2 (|)
	 * 
	 * @return the value of S_SEP
	 */
	public static String SEP2() {
		return String.format("\\%c", S_SEP); // |
	}

	/**
	 * Get the value of SEP3 (>)
	 * 
	 * @return the value of T_SEP
	 */
	public static String SEP3() {
		return String.format("%c", T_SEP); // >
	}

	/**
	 * Get the value of SEP4 (})
	 * 
	 * @return the value of Q_SEP
	 */
	public static String SEP4() {
		return String.format("%c", Q_SEP); // }
	}
}
