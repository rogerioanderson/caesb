/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.centralit.nagios.livestatus.tables;

/**
 * 
 * @author adenir
 */
public class SevicesBase extends LiveStatusBase {
	private final String TABLE = "services";

	public SevicesBase(String pPath) {
		super(pPath);
	}

	public String[] get_hosts() throws Exception {
		return execute_query(TABLE, "name", "").asArrayString();
	}

	public String[] hosts() throws Exception {
		return get_hosts();
	}

	public int state(String host) throws Exception {
		return getAsInt(execute_query(TABLE, "state", "name = " + host).asString());
	}

	// os vários serviço do host
	public String[] services(String host) throws Exception {
		return execute_query(TABLE, "services", "name = " + host).asString().split(LivestatusSeparator.SEP3());
	}

	// os vários serviço do host com informações
	public String[] services_with_info(String host) throws Exception {
		return execute_query(TABLE, "services_with_info", "name = " + host).asArrayString();
	}

	// um serviço do host com informações
	public String service_with_info(String host, String service) throws Exception {
		for (String ss : execute_query(TABLE, "services_with_info", "name = " + host).asArrayList()) {
			String[] st = ss.split(LivestatusSeparator.SEP2());
			if (service.equals(st[0]))
				return ss;
		}

		return "";
	}

	public int service_state(String host, String service) throws Exception {
		return getAsInt(service_with_info(host, service));
	}

}
