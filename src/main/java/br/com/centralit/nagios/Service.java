/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/* Released under the GPL2. See license.txt for details. */
package br.com.centralit.nagios;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Service
 * Contains all parameters/values for a service.
 *
 * @author	Folkert van Heusden
 * @version	%I%, %G%
 * @since	0.1 
 */
public class Service
{
	List<ParameterEntry> parameterEntries = new ArrayList<ParameterEntry>();

	String serviceName;

	/**
	 * Sets the service name.
	 *
	 * @param serviceName	Name of the service.
	 */
	public Service(String serviceName)
	{
		this.serviceName = serviceName;
	}

	/**
	 * Returns the service name.
	 */
	public String getServiceName()
	{
		return serviceName;
	}

	/**
	 * Adds a parameter+value.
	 */
	public void addParameter(ParameterEntry parameterEntry)
	{
		parameterEntries.add(parameterEntry);
	}

	/**
	 * Returns all parameters (and their values) for this service.
	 *
	 * @return	All parameters.
	 */
	public List<ParameterEntry> getParameters()
	{
		return parameterEntries;
	}

        /**
         * Get the value of a service-parameter.
         *
         * @return      A string with the value. Indeed, also values are returned as a string.
         */
        public String getParameter(String parameter)
        {
                for(ParameterEntry parameterEntry : parameterEntries)
                {
                        if (parameterEntry.getParameterName().equals(parameter))
                                return parameterEntry.getParameterValue();
                }

                return null;
        }
}
