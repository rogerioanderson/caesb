/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.centralit.lucene;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.lucene.queryParser.ParseException;

import br.com.centralit.citcorpore.bean.BaseConhecimentoDTO;
import br.com.citframework.excecao.LogicException;

public class TesteLucene {

	/**
	 * @param args
	 */
	private static Date formataData(String data) throws Exception {
		if (data == null || data.equals(""))
			return null;

		Date date = null;
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		date = (java.util.Date) formatter.parse(data);
		return date;
	}

	public static void main(String[] args) throws Exception {
		Lucene lucene = new Lucene();
		try {
			BaseConhecimentoDTO baseConhecimentoDTO = new BaseConhecimentoDTO();
			ArrayList<BaseConhecimentoDTO> pesquisa;
			try {
				
				//baseConhecimentoDTO.setIdBaseConhecimento(775); 
				//lucene.excluirBaseConhecimento(baseConhecimentoDTO);
				
				//baseConhecimentoDTO.setTermoPesquisa("Vincular Servi�o");
				//baseConhecimentoDTO.setTermoPesquisa("SGRSI Vincular Trabalhador");
				//baseConhecimentoDTO.setTermoPesquisa("Ajuste servidor Web");
				//baseConhecimentoDTO.setTermoPesquisa("SGF SAPF Atualiza��o Dados Empresa");
				//baseConhecimentoDTO.setTermoPesquisa("Outlook n�o funciona");
				//baseConhecimentoDTO.setTermoPesquisa("este som desenvolveu-se");
				//baseConhecimentoDTO.setTermoPesquisa("teste3333");
				//baseConhecimentoDTO.setTermoPesquisa("teste3333 + v1.2");
				//baseConhecimentoDTO.setTermoPesquisa("Proxy configura��o CentralIT");
				//baseConhecimentoDTO.setTermoPesquisa("E-DOC- N�o Consta a regional que deseja peticionar - v1.5");
				//baseConhecimentoDTO.setTermoPesquisa("@#$%�?*?KMNJBHJB?_???`?? >< `?_?_??����??�?*???? �������?=���????");
				//baseConhecimentoDTO.setTermoPesquisa("N�o Consta a regional que deseja peticionar");
				//baseConhecimentoDTO.setTermoPesquisa("erro87");
				//baseConhecimentoDTO.setTermoPesquisa("o usuario informa que nao esta conseguindo realizar a assinatura e nem o envio do peticionamento ele preenche todos os campos do documento do edoc mas quando ele clica em assinar documento o sistema fica com o botao pressionado e nao sai desta tela pelo o internet explorer, foi realizado a limpeza do estado ssl, redefini��o do internet explorer, a desinstala��o da versao mais atual do java e assim mesmo nao foi possivel realizar a assinatura do documento, foi tamb�m tentado fazer o petcionamento e mesmo assim o assinador trava, a versao utilizada foi a 7 update 13 \"!@#$%�&*()_+{}`^?:><,,.;/;^}`{_+_)(����~]�^*-/-+.+�������-=���\\\\\\\\");
				//baseConhecimentoDTO.setTermoPesquisa("plitixaENAP_");
				//baseConhecimentoDTO.setTermoPesquisa("plitica");
				//baseConhecimentoDTO.setTermoPesquisa("pol�tica");
				//baseConhecimentoDTO.setTermoPesquisa("Professional");
				//baseConhecimentoDTO.setTermoPesquisa("Instal Profissional adobe");
				//baseConhecimentoDTO.setTermoPesquisa("Mindjet");
				//baseConhecimentoDTO.setTermoPesquisa("aroqnd");
				//baseConhecimentoDTO.setTermoPesquisa("Configura��o do Calend�rio/Agenda de outro usu�rio");
				//baseConhecimentoDTO.setTermoPesquisa("VIAN");
				//baseConhecimentoDTO.setTermoPesquisa("turma");
				//baseConhecimentoDTO.setTermoPesquisa("Abertura de Chamado - Backup do sistema ASI para a empresa");
				//baseConhecimentoDTO.setTermoPesquisa("ENAP");
				//baseConhecimentoDTO.setTermoPesquisa("1� N�vel - ROTxIRO PAxdfgR�O DE ATENDIMENTO ENAP - v1.8");
				//baseConhecimentoDTO.setTermoPesquisa("GlobIP");
				//baseConhecimentoDTO.setTermoPesquisa("Maindijet");
				//baseConhecimentoDTO.setTermoPesquisa("Instalar Mindjet 2012 Profissional");
				//baseConhecimentoDTO.setTermoPesquisa("bacula");
				//baseConhecimentoDTO.setTermoPesquisa("poliENAP_");
				//baseConhecimentoDTO.setTermoPesquisa("CorelDRAW - v1.1");
				//baseConhecimentoDTO.setTermoPesquisa("Computador");
				//baseConhecimentoDTO.setTermoPesquisa("Hardw");
				//baseConhecimentoDTO.setTermoPesquisa("teste_");
				baseConhecimentoDTO.setTermoPesquisa("Work around Mensagem de erro no IE - Interno");
				 
				
				//baseConhecimentoDTO.setMedia("0");
				//baseConhecimentoDTO.setGerenciamentoDisponibilidade("N");
				//baseConhecimentoDTO.setIdUsuarioAutorPesquisa(20);
				//baseConhecimentoDTO.setIdUsuarioAprovadorPesquisa(0);
				//baseConhecimentoDTO.setDataInicioPesquisa(new java.sql.Date(formataData("07/01/2013").getTime()));
				//baseConhecimentoDTO.setDataFimPesquisa(new java.sql.Date(formataData("08/01/2013").getTime()));
				//baseConhecimentoDTO.setDataInicioPublicacao(new java.sql.Date(formataData("30/12/1969").getTime()));
				//baseConhecimentoDTO.setDataFimPublicacao(new java.sql.Date(formataData("01/01/1970").getTime()));
				//baseConhecimentoDTO.setDataInicioExpiracao(new java.sql.Date(formataData("31/05/2013").getTime()));
				//baseConhecimentoDTO.setDataFimExpiracao(new java.sql.Date(formataData("01/06/2013").getTime()));
				//baseConhecimentoDTO.setIdPasta(81);
		
				pesquisa = lucene.pesquisaBaseConhecimento(baseConhecimentoDTO);
				
				System.out.println("INICIO");
				for (BaseConhecimentoDTO baseConhecimento : pesquisa) {
					System.out.println("------------------------------------------------------------------------------------------");
					System.out.println("id: " + baseConhecimento.getIdBaseConhecimento());
					System.out.println("titulo: " + baseConhecimento.getTitulo());
					System.out.println("conteudo: " + baseConhecimento.getConteudo());
					System.out.println("avaliacao: " + baseConhecimento.getMedia());
					System.out.println("gerdisponibilidade: " + baseConhecimento.getGerenciamentoDisponibilidade());
					System.out.println("idusuarioautor: " + baseConhecimento.getIdUsuarioAutor());
					System.out.println("idusuarioaprovador: " + baseConhecimento.getIdUsuarioAprovador());
					System.out.println("datainicio: " + baseConhecimento.getDataInicio());
					System.out.println("datapublicacao: " + baseConhecimento.getDataPublicacao());
					System.out.println("dataexpiracao: " + baseConhecimento.getDataExpiracao());
					System.out.println("idpasta: " + baseConhecimento.getIdPasta());
					System.out.println("cliques: " + baseConhecimento.getContadorCliques());
				}
				System.out.println("FIM.");
				/*
				 * ArrayList<String> pesquisa = lucene.pesquisaPalavrasGemeas("backup"); for (String palavra : pesquisa) { System.out.println(palavra); }
				 */
			} catch (LogicException e) {
				e.printStackTrace();
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
