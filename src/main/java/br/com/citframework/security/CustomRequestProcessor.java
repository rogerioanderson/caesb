/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.tiles.TilesRequestProcessor;

public class CustomRequestProcessor extends TilesRequestProcessor {

	/*private static final Logger LOGGER = Logger.getLogger(CustomRequestProcessor.class);
	private static boolean cargaEmpregadoIniciada = false;
	
	private class CargaEmpregadoTask implements Runnable {
		
		DataLoadService service;
		
		public CargaEmpregadoTask(DataLoadService service) {
			this.service = service;
		}
		
		public void run() {
			try {
				service.loadEmpregados("resource/empregados.txt");
			} catch (Exception e) {
				LOGGER.error("Erro na carga de empregados.", e);
			}
		}
	}*/
	
	protected boolean processPreprocess(HttpServletRequest arg0, HttpServletResponse arg1) {
		/*if (!cargaEmpregadoIniciada) {
			cargaEmpregadoIniciada = true;
			try {
				DataLoadService service = ServiceLocator.getInstance().getDataLoadService();
				Thread thread = new Thread(new CargaEmpregadoTask(service));
				thread.start();
			} catch (Exception e) {
				LOGGER.error("Erro na execucao da thread da carga de empregados.", e);
			}
		}*/
		return super.processPreprocess(arg0, arg1);
	}
}
