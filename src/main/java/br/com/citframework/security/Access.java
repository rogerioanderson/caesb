/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.security;

import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;

public class Access {

	private String path;
	private List accessingTransactionList;
	private static final String CERQUILHA = "#";
	static final String PUBLIC = "PUBLIC";

	public String getPath() {
		return path;
	}

	void setPath(String path) {
		this.path = path;
	}

	void setAccessingTransactionList(List accessingTransactionList) {
		this.accessingTransactionList = accessingTransactionList;
	}

	public boolean hasAccess(String userTransactions) {
		if (accessingTransactionList.indexOf(PUBLIC) != -1)
			return true;
		if (StringUtils.isBlank(userTransactions))
			return false;
		StringTokenizer tokenizer = new StringTokenizer(userTransactions, CERQUILHA);
		while (tokenizer.hasMoreTokens()) {
			String userTransaction = tokenizer.nextToken().toUpperCase();
			if (accessingTransactionList.indexOf(userTransaction) != -1)
				return true;
		}
		return false;
	}

	public String toString() {
		return path == null ? "'null' path" : path;
	}

	/*
	public boolean equals(Object obj) {
		return path != null && obj != null && obj instanceof Access && path.equals(((Access) obj).path);
	}
	 */
	
	
	public List getAccessingTransactionList() {
		return accessingTransactionList;
	}

	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((path == null) ? 0 : path.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Access other = (Access) obj;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		return true;
	}

}
