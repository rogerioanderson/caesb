/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.service;

import java.util.Collection;

import br.com.citframework.dto.IDto;
import br.com.citframework.excecao.LogicException;
import br.com.citframework.excecao.ServiceException;

public interface CrudService extends IService {

    /**
     * Grava o IModel passado como parametro em meio persistente.
     *
     * @param entity
     * @return
     * @throws LogicException
     * @throws ServiceException
     *
     */
    <E extends IDto> E create(final E entity) throws LogicException, ServiceException;

    /**
     * Atualiza o Object passado como parametro em meio persistente.
     *
     * @param entity
     * @return
     * @throws LogicException
     * @throws ServiceException
     *
     */
    <E extends IDto> void update(final E entity) throws LogicException, ServiceException;

    /**
     * Exclui o Object passado como parametro do meio persistente.
     *
     * @param entity
     * @return
     * @throws LogicException
     * @throws ServiceException
     *
     */
    <E extends IDto> void delete(final E entity) throws LogicException, ServiceException;

    /**
     * Recebe um Object com seus atributos chave preenchidos, recupera todos os atributos do meio persistente e retorna o Object Preenchido.
     *
     * @param entity
     * @return
     * @throws LogicException
     * @throws ServiceException
     *
     */
    <E extends IDto> E restore(final IDto entity) throws LogicException, ServiceException;

    <E extends IDto> Collection<E> find(final E entity) throws LogicException, ServiceException;

    <E extends IDto> Collection<E> list() throws LogicException, ServiceException;

}
