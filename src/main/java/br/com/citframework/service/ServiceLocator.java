/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.citframework.dto.Usuario;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.excecao.ServiceNotFoundException;
import br.com.citframework.util.Constantes;

public class ServiceLocator {

    private static final Logger LOGGER = Logger.getLogger(ServiceLocator.class.getName());

    private static ServiceLocator singleton;

    private static final String SUFIXO_SERVICE = Constantes.getValue("SUFIXO_SERVICE");

    private ServiceLocator() {}

    public static ServiceLocator getInstance() {
        if (singleton == null) {
            synchronized (ServiceLocator.class) {
                if (singleton == null) {
                    singleton = new ServiceLocator();
                }
            }
        }
        return singleton;
    }

    public Object getService(final Class<?> iservice, final Usuario usr) throws ServiceException {
        final String nome = iservice.getName();
        try {
            final Object obj = Class.forName(nome + SUFIXO_SERVICE).newInstance();
            ((IService) obj).setUsuario(usr);
            return obj;
        } catch (final Exception e) {
            try {
                // Se nao conseguiu com o sufixo indicado nos parametros, faz com Bean pra ter certeza de que nao existe.
                final Object obj = Class.forName(nome + "Bean").newInstance();
                ((IService) obj).setUsuario(usr);
                return obj;
            } catch (final Exception e2) {
                final String message = String.format("Classe %s%s n�o existe", nome, SUFIXO_SERVICE);
                LOGGER.log(Level.SEVERE, message + e.getMessage(), e);
                throw new ServiceNotFoundException(message);
            }
        }
    }

}
