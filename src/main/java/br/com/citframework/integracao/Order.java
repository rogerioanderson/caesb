/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao;

import java.io.Serializable;

public class Order implements Serializable {

    private static final long serialVersionUID = -3708708164549646409L;

    public static final String ASC = "ASC";
    public static final String DESC = "DESC";

    private String field;
    private String typeOrder = ASC;

    public Order(final String field, final String typeOrder) {
        super();
        this.field = field;
        this.typeOrder = typeOrder;
    }

    public Order(final String field) {
        super();
        this.field = field;

    }

    public String getField() {
        return field;
    }

    public void setField(final String campo) {
        field = campo;
    }

    public String getTypeOrder() {
        return typeOrder;
    }

    public void setTypeOrder(final String tipoOrdenacao) {
        typeOrder = tipoOrdenacao;
    }

}
