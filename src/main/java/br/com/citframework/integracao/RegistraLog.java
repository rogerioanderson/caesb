/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao;

import java.util.List;

import org.apache.log4j.Logger;

import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.citframework.log.DbLog;
import br.com.citframework.log.Log;
import br.com.citframework.log.LogFactory;
import br.com.citframework.util.Reflexao;

public class RegistraLog {

    private static final Logger LOGGER = Logger.getLogger(RegistraLog.class);

	public static void registrar(final Object obj, final String operacao, final PersistenceUtil persistenceUtil, final UsuarioDTO usuarioSessao, final String tableName) {
		final Log log = LogFactory.getLog();

		if (log instanceof DbLog) {
			registraLog(log, obj, operacao, persistenceUtil, usuarioSessao, tableName);
		} else {
			Thread thread = new Thread() {
				@Override
				public void run() {
					try {
						registraLog(log, obj, operacao, persistenceUtil, usuarioSessao, tableName);
					} catch (Exception e) {
						LOGGER.debug(String.format("Nao foi possivel registrar o arquivo log. Mensagem de erro: %s.", (e.getMessage() != null ? e.getMessage() : "Nao informada")), e);
					}
				}
			};
			thread.start();
		}
	}

	private static void registraLog(final Log log, final Object obj, final String operacao, final PersistenceUtil persistenceUtil, final UsuarioDTO usuarioSessao, final String tableName) {
        try {
            if (usuarioSessao != null) {
                final List<String> lista = Reflexao.getAllProperties(obj);
                final StringBuilder dados = new StringBuilder();
                for (int i = 0; i < lista.size(); i++) {
                    String campo = lista.get(i).toString();
                    final Object valor = Reflexao.getPropertyValue(obj, campo);
                    if (valor != null) {
                        campo = persistenceUtil.getCampoDB(campo);
                        if (dados.length() > 0) {
                            dados.append(";");
                        }
                        dados.append(campo);
                        dados.append(" = ");
                        dados.append(valor);
                    }
                }
                if (dados.length() > 0) {
                    final StringBuilder message = new StringBuilder();
                    message.append(tableName);
                    message.append(Log.SEPARADOR);
                    message.append(operacao);
                    message.append(Log.SEPARADOR);
                    message.append(dados);
                    message.append(Log.SEPARADOR);
                    message.append(usuarioSessao.getIdUsuario());
                    message.append(Log.SEPARADOR);
                    message.append(usuarioSessao.getNomeUsuario());
                    message.append(Log.SEPARADOR);
                    message.append(usuarioSessao.getStatus());

					log.registraLog(message.toString(), RegistraLog.class, Log.INFO);
                }
            }
        } catch (final Exception e) {
			LOGGER.debug(String.format("Nao foi possivel registrar o log. Mensagem de erro: %s.", (e.getMessage() != null ? e.getMessage() : "Nao informada")), e);
        }
    }

}
