/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao;

import java.io.Serializable;
import java.util.List;

public class SqlConfiguration implements Serializable {

    private static final long serialVersionUID = -5645039604033764091L;

    private String sql;
    private Object[] parametros;
    private List camposRetorno;

    public SqlConfiguration(final String sql, final Object[] parametros, final List camposRetorno) {
        this.sql = sql;
        this.parametros = parametros;
        this.camposRetorno = camposRetorno;
    }

    public SqlConfiguration(final String sql, final Object[] parametros) {
        this.sql = sql;
        this.parametros = parametros;
    }

    public List getCamposRetorno() {
        return camposRetorno;
    }

    public void setCamposRetorno(final List camposRetorno) {
        this.camposRetorno = camposRetorno;
    }

    public Object[] getParametros() {
        return parametros;
    }

    public void setParametros(final Object[] parametros) {
        this.parametros = parametros;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(final String sql) {
        this.sql = sql;
    }

}
