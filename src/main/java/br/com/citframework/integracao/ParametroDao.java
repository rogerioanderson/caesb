/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao;

import java.util.ArrayList;
import java.util.Collection;

import br.com.citframework.dto.IDto;
import br.com.citframework.dto.ParametroDTO;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.util.Constantes;

public class ParametroDao extends CrudDaoDefaultImpl {

    public ParametroDao() {
        super(Constantes.getValue("DATABASE_ALIAS"), null);
    }

    @Override
    public Collection find(final IDto arg0) throws PersistenceException {
        return null;
    }

    @Override
    public Collection<Field> getFields() {
        final Collection<Field> listFields = new ArrayList<>();
        listFields.add(new Field("modulo", "modulo", true, false, false, false));
        listFields.add(new Field("nomeParametro", "nomeParametro", true, false, false, false));
        listFields.add(new Field("idEmpresa", "idEmpresa", true, false, false, false));
        listFields.add(new Field("valor", "valor", false, false, false, false));
        listFields.add(new Field("detalhamento", "detalhamento", false, false, false, false));
        return listFields;
    }

    @Override
    public String getTableName() {
        return "Parametros";
    }

    @Override
    public Collection list() throws PersistenceException {
        return null;
    }

    @Override
    public Class<ParametroDTO> getBean() {
        return ParametroDTO.class;
    }

    public ParametroDTO getValue(final String moduloParm, final String nomeParametroParm, final Integer idEmpresaParm) throws Exception {
        final ParametroDTO parmDto = new ParametroDTO();
        parmDto.setModulo(moduloParm);
        parmDto.setNomeParametro(nomeParametroParm);
        parmDto.setIdEmpresa(idEmpresaParm);
        return (ParametroDTO) this.restore(parmDto);
    }

    public void setValue(final String moduloParm, final String nomeParametroParm, final Integer idEmpresaParm, final String valorParm, final String detalhamentoParm)
            throws Exception {
        final ParametroDTO parmDto = new ParametroDTO();
        parmDto.setModulo(moduloParm);
        parmDto.setNomeParametro(nomeParametroParm);
        parmDto.setIdEmpresa(idEmpresaParm);
        parmDto.setValor(valorParm);
        parmDto.setDetalhamento(detalhamentoParm);

        final ParametroDTO parmDtoAux = this.getValue(moduloParm, nomeParametroParm, idEmpresaParm);
        if (parmDtoAux != null) { // Se ja existe, atualiza.
            this.update(parmDto);
        } else {
            this.create(parmDto);
        }
    }

}
