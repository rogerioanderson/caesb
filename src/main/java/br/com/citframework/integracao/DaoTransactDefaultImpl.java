/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao;

import java.util.List;

import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.citframework.dto.Usuario;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.integracao.core.DataBase;

public abstract class DaoTransactDefaultImpl implements IDaoTransact {

    protected String aliasDB;
    protected Usuario usuario;
    protected PersistenceEngine engine;

    protected static final DataBase MAIN_SGBD = DataBase.fromStringId(CITCorporeUtil.SGBD_PRINCIPAL.trim());

    public DaoTransactDefaultImpl(final String aliasDB, final Usuario usuario) {
        this.aliasDB = aliasDB;
        this.usuario = usuario;
        engine = new PersistenceEngine(aliasDB, this.getTableName(), this.getBean(), this.getFields(), this.usuario);
    }

    public DaoTransactDefaultImpl(final TransactionControler tc, final Usuario usuario) {
        this.usuario = usuario;
        aliasDB = tc.getDataBaseAlias();
        engine = new PersistenceEngine(tc, this.getTableName(), this.getBean(), this.getFields(), this.usuario);
    }

    protected List execSQL(final String sql, final Object[] parametros) throws PersistenceException {
        return engine.execSQL(sql, parametros);
    }

    protected int execUpdate(final String sql, final Object[] parametros) throws PersistenceException {
        return engine.execUpdate(sql, parametros);
    }

    protected List listConvertion(final Class<?> classe, final List dados, final List fields) throws PersistenceException {
        return engine.listConvertion(classe, dados, fields);
    }

    @Override
    public String getAliasDB() {
        return aliasDB;
    }

    @Override
    public TransactionControler getTransactionControler() {
        return engine.getTransactionControler();
    }

    @Override
    public void setTransactionControler(final TransactionControler tc) {
        engine.setTransactionControler(tc);
    }

    public abstract Class<?> getBean();

    @Override
    public Usuario getUsuario() {
        return usuario;
    }

}
