/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao.core;

/**
 * Interface para informa��o de pagina��o
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 01/10/2014
 * @see <a href="http://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/domain/Pageable.html">org.springframework.data.domain.Pageable</a>
 */
public interface Pageable {

    /**
     * Retorna a p�gina a ser retornada
     *
     * @return {@code int}
     */
    int getPageNumber();

    /**
     * Retorna a n�mero de itens da p�gina
     *
     * @return {@code int} n�mero de itens da p�gina
     */
    int getPageSize();

    /**
     * Retorna the offset to be taken according to the underlying page and page size
     *
     * @return {@code int} offset a ser mantido
     */
    int getOffset();

    /**
     * Retorna a {@link Pageable} pedindo a pr�xima {@link Page}
     *
     * @return {@link Pageable}
     */
    Pageable next();

    Pageable previous();

    /**
     * Retorna a {@link Pageable} anterior ou a primeira {@link Pageable} se a atual � a primeira
     *
     * @return {@link Pageable}
     */
    Pageable previousOrFirst();

    /**
     * Retorna {@link Pageable} pedindo a primeira {@link Page}
     *
     * @return {@link Pageable}
     */
    Pageable first();

    /**
     * Returns whether there's a previous {@link Pageable} we can access from the current one. Will return {@literal false} in case the current {@link Pageable} already refers to
     * the first page.
     *
     * @return {@code true}, caso tenha uma {@link Pageable} anterior. {@code false}, caso seja a {@link Pageable} atual
     */
    boolean hasPrevious();

}
