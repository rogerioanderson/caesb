/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao.core;

import java.util.List;

/**
 * Representa��o de p�gina, que � uma sublista de uma lista de objetos.
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 01/10/2014
 * @see <a href="http://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/domain/Page.html">org.springframework.data.domain.Page</a>
 */
public interface Page<T> extends Iterable<T> {

    /**
     * Retorna o n�mero total de p�ginas
     *
     * @return {@code int}: n�mero total de p�ginas
     */
    int getTotalPages();

    /**
     * Retorna a quantidade total de elementos
     *
     * @return {@code long}: quantidade total de elementos
     */
    long getTotalElements();

    /**
     * Retorna o n�mero da {@link Page} atual. � sempre n�o negativa
     *
     * @return {@code int}: o n�mero da {@link Page} atual
     */
    int getNumber();

    /**
     * Retorna o tamanho da {@link Page}
     *
     * @return the size of the {@link Page}
     */
    int getSize();

    /**
     * Retorna o n�mero de elementos da {@link Page}
     *
     * @return {@code int} n�mero de elementos da {@link Page}
     */
    int getNumberOfElements();

    /**
     * Recupera, como uma {@link List}, o conte�do de uma p�gina
     *
     * @return {@link List}: conte�do da {@link Page}
     */
    List<T> getContent();

    /**
     * Verifica se a {@link Page} possui conte�do
     *
     * @return {@code true}, caso possua conte�do {@link Page}. {@code false}, caso contr�rio
     */
    boolean hasContent();

    /**
     * Retorna se a {@link Page} � a primeira
     *
     * @return {@code true}, caso seja a primeira {@link Page}. {@code false}, caso contr�rio
     */
    boolean isFirst();

    /**
     * Retorna se a {@link Page} � a �ltima
     *
     * @return {@code true}, caso seja a �ltima {@link Page}. {@code false}, caso contr�rio
     */
    boolean isLast();

    /**
     * Retorna exist�ncia de pr�xima p�gina
     *
     * @return {@code true}, caso tenha pr�xima {@link Page}. {@code false}, caso contr�rio
     */
    boolean hasNext();

    /**
     * Retorna exist�ncia de p�gina anterior
     *
     * @return {@code true}, caso tenha uma {@link Page} anterior. {@code false}, caso contr�rio
     */
    boolean hasPrevious();

    /**
     * Recupera a {@link Pageable} para solicita a pr�xima {@link Page}. Pode ser {@code null} caso a {@link Page} atual seja a �ltima. Usu�rios devem checar, chamando
     * {@link #hasNext()} antes de chamar este m�todo para ter certeza que o valor retornado n�o ser� {@code null}
     *
     * @return {@link Pageable}
     */
    Pageable nextPageable();

    /**
     * Recupera a {@link Pageable} para solicita a {@link Page} anterior. Pode ser {@code null} caso a {@link Page} atual seja a primeira. Usu�rios devem checar, chamando
     * {@link #hasPrevious()} antes de chamar este m�todo para ter certeza que o valor retornado n�o ser� {@code null}
     *
     * @return {@link Pageable}
     */
    Pageable previousPageable();

}
