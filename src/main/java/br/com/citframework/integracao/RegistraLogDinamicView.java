/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao;

import org.apache.log4j.Logger;

import br.com.centralit.citcorpore.bean.UsuarioDTO;
import br.com.centralit.citcorpore.negocio.LogDadosService;
import br.com.centralit.citcorpore.negocio.LogDadosServiceEjb;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.citframework.dto.LogDados;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilStrings;

public class RegistraLogDinamicView implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(RegistraLogDinamicView.class);

    private final Object[] obj;
    private final String operacao;
    private final String sqlExec;
    private final UsuarioDTO usuarioSessao;
    private final String tableName;

    public RegistraLogDinamicView(final Object[] obj, final String operacao, final String sqlExec, final UsuarioDTO usuarioSessao, final String tableName) {
        this.obj = obj;
        this.operacao = operacao;
        this.sqlExec = sqlExec;
        this.usuarioSessao = usuarioSessao;
        this.tableName = tableName;
    }

    @Override
    public void run() {
        if (UtilStrings.nullToVazio(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.USE_LOG, "false")).equalsIgnoreCase("true")) {
            if (usuarioSessao != null) {
                String dados = "";
                if (obj != null) {
                    for (final Object element : obj) {
                        if (!dados.trim().equalsIgnoreCase("")) {
                            dados += ", ";
                        }
                        if(element != null){
                        	dados += "[" + element.toString() + "]";
                        }
                    }
                }
                LogDados logDados = new LogDados();
                logDados.setDtAtualizacao(UtilDatas.getDataHoraAtual());
                logDados.setIdUsuario(usuarioSessao.getIdUsuario());
                logDados.setDataInicio(UtilDatas.getDataAtual());
                logDados.setDataLog(UtilDatas.getDataHoraAtual());
                logDados.setNomeTabela(tableName);
                logDados.setOperacao(operacao);
                logDados.setLocalOrigem(usuarioSessao.getNomeUsuario());
                logDados.setDados("Execute ... {" + sqlExec + "} Data: {" + dados + "}");

                final LogDadosService lds = new LogDadosServiceEjb();

                try {
					logDados = (LogDados) lds.create(logDados);
                } catch (final Exception e) {
                    LOGGER.debug(e.getMessage(), e);
                }
            }
        }
    }

}
