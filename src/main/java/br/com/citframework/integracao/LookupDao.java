/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.citframework.dto.LookupDTO;
import br.com.citframework.dto.Usuario;

public abstract class LookupDao extends DaoTransactDefaultImpl {

    public LookupDao(final String alias, final Usuario usuario) {
        super(alias, usuario);
    }

    public abstract List processLookup(final LookupDTO lookupObject, final HttpServletRequest request) throws Exception;

    public String getValueParmLookup(final LookupDTO lookupObject, final int number) {
        if (number == 1) {
            return lookupObject.getParm1();
        } else if (number == 2) {
            return lookupObject.getParm2();
        } else if (number == 3) {
            return lookupObject.getParm3();
        } else if (number == 4) {
            return lookupObject.getParm4();
        } else if (number == 5) {
            return lookupObject.getParm5();
        } else if (number == 6) {
            return lookupObject.getParm6();
        } else if (number == 7) {
            return lookupObject.getParm7();
        } else if (number == 8) {
            return lookupObject.getParm8();
        } else if (number == 9) {
            return lookupObject.getParm9();
        } else if (number == 10) {
            return lookupObject.getParm10();
        }
        return null;
    }

}
