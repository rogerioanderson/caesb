/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao;

import java.sql.Connection;

import br.com.citframework.excecao.PersistenceException;

/**
 * Controlador de comportamento de conex�es que n�o posuem transa��o
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 25/08/2014
 *
 */
public interface ConnectionControler extends AutoCloseable {

    /**
     * Coloca uma conex�o em estado {@code read-only}, n�o pode ser transacional, n�o executa DML de altera��o
     *
     * @param readOnly
     * @throws PersistenceException
     * @since 01/09/2014
     * @see Connection#setReadOnly(boolean)
     */
    void setReadOnly(final boolean readOnly) throws PersistenceException;

    /**
     * Verifica se a conex�o � ou n�o apenas leitura
     *
     * @return {@code true}, caso a conex�o seja read only. {@code false}, caso contr�rio
     * @throws PersistenceException
     * @since 01/09/2014
     */
    boolean isReadOnly() throws PersistenceException;;

    /**
     * Recupera o alias da base de dados. Normalmente, o resource JNDI do pool
     *
     * @return {@link String}
     */
    String getDataBaseAlias();

    /**
     * Seta o alias da base de dados. Normalmente, o resource JNDI do pool
     *
     * @param dataBaseAlias
     *            alias da base de dados
     */
    void setDataBaseAlias(final String dataBaseAlias);

    /**
     * Retorna a conex�o relacionada ao Transaction Controler
     *
     * <p>
     * {@code IMPORTANTE}: esta trasa��o pode n�o estar com a transa��o iniciada ({@code {@link Connection#getAutoCommit()} == false}). Voc� deve chamar {@link #start()} caso
     * queira comportamento transacional
     * <p>
     *
     * @return {@link Connection}
     */
    Connection getConnection() throws PersistenceException;

    /**
     * Commita e fecha a transa��o
     *
     * @throws PersistenceException
     *             caso algum problema ao fechar a transa��o aconte�a, como {@link Connection} j� fechada
     * @see {@link Connection#close()}
     */
    @Override
    void close() throws PersistenceException;

    /**
     * Commit e fecha a conex�o sem levantar excec�es
     *
     * @return {@code true}, caso obtenha sucesso em fechar a conex�o. {@code false}, caso contr�rio
     * @see {@link #close()}
     */
    boolean closeQuietly();

}
