/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao;

import java.io.Serializable;

/**
 * Classe de campos da base de dados e com os atributos do DTO.
 *
 * @author Administrador
 *
 */
public class Field implements Serializable {

    private static final long serialVersionUID = 5945592714921506681L;

    private String fieldDB;
    private String fieldClass;
    private boolean pk = false;// caso seja chave prim�ria
    private boolean sequence = false;// preenchido com sequence
    private boolean auto = false;// valor default ou auto incremento
    private boolean unique = false;// se pode ou n�o possuir valor duplicado
    private String msgReturn = ""; // mensagem de retorno para campo �nico

    /**
     * Construtor Field sem o campo de descri��o para unique
     *
     * @param fieldDB
     *            Nome do campo no banco de dados.
     * @param fieldClass
     *            Nome do atributo do DTO
     * @param pk
     *            Ccso seja chave prim�ria
     * @param sequence
     *            Preenchido com sequence
     * @param auto
     *            Valor default ou auto incremento
     * @param unique
     *            Se pode ou n�o possuir valor duplicado
     */
    public Field(final String fieldDB, final String fieldClass, final boolean pk, final boolean sequence, final boolean auto, final boolean unique) {
        this.fieldDB = fieldDB;
        this.fieldClass = fieldClass;
        this.pk = pk;
        this.sequence = sequence;
        this.auto = auto;
        this.unique = unique;
    }

    /**
     * Construtor Field com o campo de descri��o para unique
     *
     * @param fieldDB
     *            Nome do campo no banco de dados.
     * @param fieldClass
     *            Nome do atributo do DTO
     * @param pk
     *            Ccso seja chave prim�ria
     * @param sequence
     *            Preenchido com sequence
     * @param auto
     *            Valor default ou auto incremento
     * @param unique
     *            Se pode ou n�o possuir valor duplicado
     * @param msgReturn
     *            de retorno caso aconte�a duplica��o de campos acionado pelo
     *            par�metro 'unique'
     */
    public Field(final String fieldDB, final String fieldClass, final boolean pk, final boolean sequence, final boolean auto, final boolean unique, final String msgReturn) {
        this.fieldDB = fieldDB;
        this.fieldClass = fieldClass;
        this.pk = pk;
        this.sequence = sequence;
        this.auto = auto;
        this.unique = unique;
        this.msgReturn = msgReturn;
    }

    public boolean isAuto() {
        return auto;
    }

    public void setAuto(final boolean auto) {
        this.auto = auto;
    }

    public String getFieldClass() {
        return fieldClass;
    }

    public void setFieldClass(final String campoClasse) {
        fieldClass = campoClasse;
    }

    public String getFieldDB() {
        return fieldDB;
    }

    public void setFieldDB(final String campoDB) {
        fieldDB = campoDB;
    }

    public boolean isPk() {
        return pk;
    }

    public void setPk(final boolean pk) {
        this.pk = pk;
    }

    public boolean isSequence() {
        return sequence;
    }

    public void setSequence(final boolean sequence) {
        this.sequence = sequence;
    }

    public boolean isUnique() {
        return unique;
    }

    public void setUnique(final boolean unique) {
        this.unique = unique;
    }

    public void setMsgReturn(final String msgReturn) {
        this.msgReturn = msgReturn;
    }

    public String getMsgReturn() {
        return msgReturn;
    }

}
