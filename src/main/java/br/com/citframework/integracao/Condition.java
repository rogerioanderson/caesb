/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao;

import java.io.Serializable;

public class Condition implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -94317735780450660L;
	public static final int AND =1;
	public static final int OR =2;
	
	
	private String filedClass;
	private String comparator;
	private Object value;
	private int operator = AND;
	
	
	
	public Condition(int operator, String filedClass, String comparator, Object value) {
		super();
		this.operator = operator;
		this.filedClass = filedClass;
		this.comparator = comparator;
		this.value = value;
	}
	
	
	
	public Condition(String filedClass, String comparator, Object value) {
		super();
		this.filedClass = filedClass;
		this.comparator = comparator;
		this.value = value;
	}


	public String getFiledClass() {
		return filedClass;
	}
	public void setFiledClass(String campo) {
		this.filedClass = campo;
	}
	public String getComparator() {
		return comparator;
	}
	public void setComparator(String comparacao) {
		this.comparator = comparacao;
	}
	public int getOperator() {
		return operator;
	}
	public void setOperator(int operador) {
		this.operator = operador;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object valor) {
		this.value = valor;
	}
	
	
	
	
	
	
	
	
	

}
