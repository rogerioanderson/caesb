/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 *
 */
package br.com.citframework.integracao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.citframework.dto.IDto;
import br.com.citframework.dto.LogTabela;
import br.com.citframework.dto.Usuario;
import br.com.citframework.excecao.PersistenceException;
import br.com.citframework.util.Constantes;

/**
 * @author karem.ricarte
 *
 */
public class LogTabelaDao extends CrudDaoDefaultImpl {

    public LogTabelaDao(final Usuario usuario) {
        super(Constantes.getValue("DATABASE_ALIAS"), usuario);
    }

    @Override
    public Collection<LogTabela> find(final IDto dto) throws PersistenceException {
        return null;
    }

    @Override
    public Collection<Field> getFields() {
        final List<Field> lista = new ArrayList<>();
        lista.add(new Field("idLog", "idLog", true, true, false, false));
        lista.add(new Field("nomeTabela", "nomeTabela", false, false, false, false));
        return lista;
    }

    @Override
    public String getTableName() {
        return "logtabela";
    }

    @Override
    public Collection list() throws PersistenceException {
        final List<Order> ordenacao = new ArrayList<>();
        ordenacao.add(new Order("idLog"));
        return this.list(ordenacao);
    }

    @Override
    public Class<LogTabela> getBean() {
        return LogTabela.class;
    }

    public LogTabela getLogByTabela(final String nomeTabela) throws Exception {
        final List<Condition> condicoes = new ArrayList<>();
        condicoes.add(new Condition("nomeTabela", "=", nomeTabela));
        final Collection resultado = this.findByCondition(condicoes, null);
        if (resultado == null || resultado.size() == 0) {
            return null;
        }
        return (LogTabela) ((List) resultado).get(0);

    }

}
