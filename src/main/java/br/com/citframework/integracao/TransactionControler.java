/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.integracao;

import java.sql.Connection;
import java.sql.Savepoint;

import br.com.citframework.excecao.PersistenceException;

public interface TransactionControler extends ConnectionControler {

    /**
     * Verifica se a tras��o est� iniciada
     *
     * @return {@code true}, caso trasa��o esteja iniciada. {@code false}, caso contr�rio
     */
    boolean isStarted();

    /**
     * Inicia a transa��o
     *
     * @throws PersistenceException
     *             caso algum problema ao iniciar a transa��o aconte�a, como {@link Connection} j� fechada
     */
    void start() throws PersistenceException;

    /**
     * Commita as altera��es na transa��o
     *
     * @throws PersistenceException
     *             caso algum problema ao iniciar a transa��o aconte�a, como {@link Connection} j� fechada
     */
    void commit() throws PersistenceException;

    /**
     * Realiza��o rollback de todas as altera��es ainda n�o commitadas na transa��o
     *
     * @throws PersistenceException
     *             caso algum problema ao iniciar a transa��o aconte�a, como {@link Connection} j� fechada
     */
    void rollback() throws PersistenceException;

    /**
     * Realiza rollback na transa��o at� um ponto de marca��o
     *
     * @param savepoint
     *            ponto at� o qual deve ser feito o rollback
     * @throws PersistenceException
     *             caso algum problema ao dar rollback na transa��o aconte�a, como {@link Connection} j� fechada
     */
    void rollback(final Savepoint savepoint) throws PersistenceException;

    /**
     * Cria um {@link Savepoint} na transa��o
     *
     * @return {@link Savepoint}
     * @throws PersistenceException
     */
    Savepoint savepoint() throws PersistenceException;

    /**
     * Cria um {@link Savepoint} nomeado na transa��o
     *
     * @param name
     *            nome para identifica��o do ponto de marca��o
     * @return {@link Savepoint}
     * @throws PersistenceException
     */
    Savepoint savepoint(final String name) throws PersistenceException;

    /**
     * Libera um {@link Savepoint} previamente marcado na linha de transa��es
     *
     * @param savepoint
     *            savepoint a ser liberado na transa��o
     * @throws PersistenceException
     */
    void releaseSavepoint(final Savepoint savepoint) throws PersistenceException;

}
