/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.build;

import java.io.IOException;
import java.util.concurrent.ConcurrentMap;

import org.apache.log4j.Logger;

import br.com.citframework.util.Assert;
import br.com.citframework.util.FileUtil;
import br.com.citframework.util.UtilI18N;

import com.google.gson.Gson;

/**
 * Gera, no momento do build, os arquivos JavaScript contendo as chaves da internacionaliza��o
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 09/02/2015
 *
 */
public final class GenerateI18NJSFiles {

    private static final Logger LOGGER = Logger.getLogger(GenerateI18NJSFiles.class);

    private static final Gson GSON = new Gson();

    private static final String PATH_PATTERN = "%s/js/i18n/";

    private static final String FILE_NAME_PATTERN = "messages_%s.js";

    /**
     * Gera os arquivos JavaScript I18N
     *
     * @param buildDirectory
     *            diret�rio em que os arquivos da constru��o ser�o gerados
     * @param encode
     *            no qual o conte�do do arquivo ser� gerado
     */
    public void generateJavaScriptFiles(final String buildDirectory, final String encoding) throws IOException {
        Assert.notNullAndNotEmpty(buildDirectory, "Build directory name must not be null or empty.");

        this.generateEnglishFile(buildDirectory, encoding);
        this.generatePortugueseFile(buildDirectory, encoding);
        this.generateSpanishFile(buildDirectory, encoding);
    }

    private void generateEnglishFile(final String buildDirectory, final String encoding) {
        LOGGER.debug("Generating English I18N JavaScript File");

        final String content = this.getContent(UtilI18N.ENGLISH_SIGLA);

        this.persistsFile(content, String.format(PATH_PATTERN, buildDirectory), String.format(FILE_NAME_PATTERN, UtilI18N.ENGLISH_SIGLA), encoding);
    }

    private void generatePortugueseFile(final String buildDirectory, final String encoding) {
        LOGGER.debug("Generating English I18N Portuguese File");

        final String content = this.getContent(UtilI18N.PORTUGUESE_SIGLA);

        this.persistsFile(content, String.format(PATH_PATTERN, buildDirectory), String.format(FILE_NAME_PATTERN, UtilI18N.PORTUGUESE_SIGLA), encoding);
    }

    private void generateSpanishFile(final String buildDirectory, final String encoding) {
        LOGGER.debug("Generating Spanish I18N JavaScript File");

        final String content = this.getContent(UtilI18N.SPANISH_SIGLA);

        this.persistsFile(content, String.format(PATH_PATTERN, buildDirectory), String.format(FILE_NAME_PATTERN, UtilI18N.SPANISH_SIGLA), encoding);
    }

    private void persistsFile(final String content, final String path, final String fileName, final String encoding) {
        try {
            FileUtil.salvarArquivo(content, fileName, path, false, encoding);
        } catch (final IOException e) {
            LOGGER.error(String.format("Error while creating file %s: ", fileName) + e.getMessage(), e);
        }
    }

    private String getContent(final String sigla) {
        final ConcurrentMap<String, String> keysMap = UtilI18N.getMapLanguage(sigla);

        final StringBuilder jsonTemplate = new StringBuilder();
        jsonTemplate.append("var bundle={key:");
        jsonTemplate.append(GSON.toJson(keysMap));
        jsonTemplate.append("};");
        return jsonTemplate.toString();
    }

    public static void main(final String[] args) {
        LOGGER.info("Generating I18N JavaScript files.");
        try {
            new GenerateI18NJSFiles().generateJavaScriptFiles(args[0], args[2]);
			new GenerateI18NJSFiles().generateJavaScriptFiles(args[1], args[2]);
        } catch (final IOException e) {
            LOGGER.error("Error while creating files: " + e.getMessage(), e);
        }
    }

}
