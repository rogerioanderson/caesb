/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.tld;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import br.com.centralit.citcorpore.ajaxForms.GerenciamentoProcessos;
import br.com.centralit.citcorpore.util.Enumerados;
import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;

public class GerenciamentoField extends BodyTagSupport {

	private static final long serialVersionUID = 259097494202966188L;
	private boolean paginacao;
	private String classeExecutora;
	private Integer tipoLista;

	public int doStartTag() throws JspException {
		GerenciamentoProcessos gp = null;
		Integer paginaSelecionada = 1;
		@SuppressWarnings("unused")
		Integer itensPorPagina = Integer.parseInt(ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.QUANT_RETORNO_PESQUISA, "5"));
		StringBuilder strBuff = new StringBuilder();
		try {

			gp = (GerenciamentoProcessos) Class.forName(this.getClasseExecutora()).newInstance();
			gp.iniciar(strBuff, (HttpServletRequest) pageContext.getRequest(), Enumerados.ItensPorPagina.CINCO.getValor(), paginaSelecionada, this.getTipoLista());
			pageContext.getOut().println(strBuff.toString());

		} catch (ClassNotFoundException ex) {
			throw new RuntimeException("Classe executora n�o encontrada");
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SKIP_BODY;
	}

	public boolean isPaginacao() {
		return paginacao;
	}

	public void setPaginacao(boolean paginacao) {
		this.paginacao = paginacao;
	}

	public String getClasseExecutora() {
		return classeExecutora;
	}

	public void setClasseExecutora(String classeExecutora) {
		this.classeExecutora = classeExecutora;
	}

	public Integer getTipoLista() {
		return tipoLista;
	}

	public void setTipoLista(Integer tipoLista) {
		this.tipoLista = tipoLista;
	}

}
