/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Created on 11/01/2005
 *

 * Window - Preferences - Java - Code Style - Code Templates
 */
package br.com.citframework.tld;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.bean.WriteTag;

/**
 * @author ney
 *

 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FormatDate extends WriteTag{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 254679665210265708L;
	private String mask;
	
	public FormatDate(){
		
		setFormatKey("DATA_FORMAT");
	}
	
	
	public String getMask() {
		return mask;
	}
	public void setMask(String mask) {
		this.mask = mask;
	}
	
	
	protected String formatValue(Object value) throws JspException {
		
		if(value==null || value.toString().trim().length()==0){
			return "";
		}
		
		if(value.toString().indexOf("/")>-1){
		    return value.toString();
		}
		Date data = null;
		SimpleDateFormat spd =null;
		if(value instanceof String){
		    spd = new SimpleDateFormat("yyyy-MM-dd");
		    try {
                data = spd.parse(value.toString());
            } catch (ParseException e1) {
                
                throw new JspException("Mascara:"+getMask()+", Valor:"+value+", campo:"+property,e1);
            } 
		}else if(value instanceof Date){
		    data = (Date)value;
		    
		}else{
		    throw new JspException("Tipo do atributo "+getProperty()+" inv�lido pata convers�o de Data");
		}
		    
		    
		    
		 spd = new SimpleDateFormat(getMask().toString());
		
		
		try {
            return spd.format(data);
        } catch (Exception e) {
            
           throw new JspException("Mascara:"+getMask()+", Valor:"+value+", campo:"+property,e);
        }
	}
}
