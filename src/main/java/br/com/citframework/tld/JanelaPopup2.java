/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.tld;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 * @author danillo.lisboa
 * 
 */
public class JanelaPopup2 extends BodyTagSupport {

	private static final long serialVersionUID = 1L;

	private String id;
	private String titulo;

	public int doAfterBody() throws JspException {
		try {
			BodyContent bc = getBodyContent();
			String body = bc.getString();
			body = this.tratarLinhas(body);
			body = this.tratarAspas(body);
			body = this.getDialog(body);
			body = this.getDiv(body);
			JspWriter out = bc.getEnclosingWriter();
			out.print(body);

		} catch (IOException e) {
			throw new JspException("erro: " + e.getMessage());
		}
		return SKIP_BODY;

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String tratarLinhas(String menssagem) {
		menssagem = menssagem.replace("\r\n\t\t\t", "");
		menssagem = menssagem.replace("\n", "");
		menssagem = menssagem.replace("\r", "");
		menssagem = menssagem.replace("\t", "");
		return menssagem;
	}

	public String tratarAspas(String menssagem) {
		menssagem = org.apache.commons.lang.StringEscapeUtils
				.escapeJava(menssagem);
		return menssagem;
	}

	public String getDiv(String body) {

		return "<div id=\"" + this.getId() + "\" title=\" " + this.getTitulo()
				+ "\">\n" + body + "\n</div>";

	}

	public String getDialog(String body) {
		String dialog = null;

		dialog = "<script>" + "$(\'#btn_" + getId() + "\').click(function(){"
				+ "bootbox.dialog(\"" + body + "\", [{"
				+ "\"label\" : \"Sair\"," + "\"class\" : \"btn-default\","
				+ "\"callback\": function() {" + "$.gritter.add({"
				+ "title: \'" + this.getTitulo() + "\',text: \"Cancelado\""
				+ "});" + "}}])});" + "</script>";

		return dialog;
	}

}
