/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.tld;

import java.io.FileNotFoundException;
import java.util.Collection;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import br.com.centralit.citcorpore.bean.ReleaseDTO;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.centralit.citcorpore.versao.Versao;
import br.com.centralit.citgerencial.util.Util;
import br.com.citframework.util.HistoricoAtualizacoesCitsmart;
import br.com.citframework.util.UtilI18N;

/**
 * Imprime as informa��es relacionadas a vers�o
 *
 * @author flavio.santana
 * @since 23/10/2013
 */
public class SobreCitsmart extends BodyTagSupport {

    private static final long serialVersionUID = 1L;

    @Override
    public int doStartTag() throws JspException {

        /*
         * Recupera a propriedade do locale
         */
        final HttpSession session = ((HttpServletRequest) pageContext.getRequest()).getSession();
        final StringBuilder stringBuilder = new StringBuilder();
        
        String locale = UtilI18N.PORTUGUESE_SIGLA;
        
        if (session.getAttribute("locale") != null) {
            locale = session.getAttribute("locale").toString();
        } 

        try {
        	
            final String separator = System.getProperty("file.separator");
            final String path = CITCorporeUtil.CAMINHO_REAL_APP + "XMLs" + separator + "release_" + locale + ".xml";
            HistoricoAtualizacoesCitsmart hc = null;
            Collection<ReleaseDTO> listRelease = null;

            try {
                hc = new HistoricoAtualizacoesCitsmart(
                		);
                listRelease = hc.lerXML(path);
            } catch (final FileNotFoundException e) {
                e.printStackTrace();
            }

            final String passoInstalacao = (String) session.getAttribute("passoInstalacao");
            final ServletContext context = ((HttpServletRequest) pageContext.getRequest()).getSession().getServletContext();

            if (passoInstalacao != null || context.getAttribute("instalacao") != null) {
                stringBuilder.append("<form  name='formSobre' id='formSobre'  action='" + CITCorporeUtil.CAMINHO_SERVIDOR
                        + ((HttpServletRequest) pageContext.getRequest()).getContextPath() + "/pages/start/start'>");
            } else {
                stringBuilder.append("<form  name='formSobre' id='formSobre'  action='" + CITCorporeUtil.CAMINHO_SERVIDOR
                        + ((HttpServletRequest) pageContext.getRequest()).getContextPath() + "/pages/index/index'>");
            }
            stringBuilder.append("	<div id='sobre-container'>");
            stringBuilder.append("	     <img src='" + CITCorporeUtil.CAMINHO_SERVIDOR + ((HttpServletRequest) pageContext.getRequest()).getContextPath()
                    + "/imagens/logo/iconeLogo.png'>");
            stringBuilder.append("	     <div id='produto-descricao'>");
            stringBuilder.append("	       <h2>Citsmart</h2>");
            stringBuilder.append("	       <span>" + UtilI18N.internacionaliza(locale, "sobre.citsmart") + "</span>");
            stringBuilder.append("	  	 </div>");
            stringBuilder.append("	</div>");
            stringBuilder.append("  <div id='versao-container'>");
            stringBuilder.append("		<div>" + UtilI18N.internacionaliza(locale, "login.versao") + "<b>" + Versao.getDataAndVersao() + "</b></div>");
            stringBuilder.append("		<div><a href='javascript:;' class='openHistorico'>" + UtilI18N.internacionaliza(locale, "sobre.oQueHaDeNovoNestaVersao") + "</a></b></div>");
            stringBuilder.append("		<div id='historico'>");
            stringBuilder.append("			<h2>" + UtilI18N.internacionaliza(locale, "release.historicoAtualizacoes") + "</h2>");
            stringBuilder.append("			<select name='versao' id='versao' onchange='buscaHistoricoPorVersao()'>");
            if (listRelease != null && !listRelease.isEmpty()) {
                for (final ReleaseDTO releaseDto : listRelease) {
                    stringBuilder.append("				<option value='" + releaseDto.getVersao() + "'>" + releaseDto.getVersao() + "</option>");
                }
            }
            stringBuilder.append("			</select>");

            stringBuilder.append("		<div class='slim-scroll box-generic'>");
            stringBuilder.append("			<div  id='divRelease'>");

            if (listRelease != null && !listRelease.isEmpty()) {

                stringBuilder.append("			<div id='historicoRelease' style='overflow: auto; text-align: justify;'>");

                int countRelease = 0;
                for (final ReleaseDTO releaseDto : listRelease) {

                    stringBuilder.append("			<div id='release" + countRelease + "'>");
                    stringBuilder.append("				<div>");
                    stringBuilder.append("					<br>");

                    if (releaseDto.getConteudo() != null && !releaseDto.getConteudo().isEmpty()) {
                        int i = 0;
                        for (final String item : releaseDto.getConteudo()) {
                            ++i;
                            stringBuilder.append("			<div>");
                            stringBuilder.append("				<span  style='font-weight:bold;'>");
                            stringBuilder.append(i);
                            stringBuilder.append(" ");
                            stringBuilder.append("				</span>");
                            stringBuilder.append(Util.encodeHTML(item));
                            stringBuilder.append("			</div>");
                            stringBuilder.append("			<br>");
                        }
                    }
                    stringBuilder.append("					</div>");
                    stringBuilder.append("				</div>");
                    ++countRelease;
                    break;
                }

                stringBuilder.append("				</div>");

            }

            stringBuilder.append("				</div>");
            stringBuilder.append("			</div>");
            stringBuilder.append("		</div>");
            stringBuilder.append("	</div>");

            stringBuilder.append("	<div id='produto-container'>");
            stringBuilder.append("		<div><a target='_blank' href='http://www.citsmart.com.br'>Citsmart</a></div>");
            stringBuilder.append("		<div>� 2014 -  " + UtilI18N.internacionaliza((HttpServletRequest) pageContext.getRequest(), "citcorpore.todosDireitosReservados") + "</div>");
            stringBuilder.append("	</div>");
            stringBuilder.append("</form>");

            pageContext.getOut().println(stringBuilder.toString());

        } catch (final Exception e) {
            e.printStackTrace();
        }

        return SKIP_BODY;
    }


}
