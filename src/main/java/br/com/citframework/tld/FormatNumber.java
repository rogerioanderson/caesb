/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Created on 11/01/2005
 *

 * Window - Preferences - Java - Code Style - Code Templates
 */
package br.com.citframework.tld;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.bean.WriteTag;

import br.com.citframework.util.converter.ConverterUtils;

/**
 * @author ney
 *

 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FormatNumber extends WriteTag{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6823703333271138561L;
	private String decimal;
	private String digitos;
	
	
	
	public String getDigitos() {
		return digitos;
	}


	public void setDigitos(String digitos) {
		this.digitos = digitos;
	}


	public FormatNumber(){
		
		setFormatKey("DOUBLE_KEY");
	}
	

	protected String formatValue(Object value) throws JspException {
		
		if(value==null || value.toString().trim().length()==0){
			return null;
		}
		
		if(ConverterUtils.possuiMascara(value.toString())){
			   value =  ConverterUtils.retiraMascara(value.toString());
			}
		
		Integer dec = new Integer(getDecimal());
		NumberFormat nf = NumberFormat.getInstance(new Locale("pt","BR"));
		nf.setMaximumFractionDigits(dec.intValue());
		nf.setMinimumFractionDigits(dec.intValue());
		if(getDigitos()!=null && getDigitos().trim().length()>0){
			nf.setMinimumIntegerDigits(new Integer(getDigitos()).intValue());
		}
		value = nf.format(new Double(value.toString().trim()));
		
		if(getDecimal().equals("0")){
				//System.out.print("Atrb_ant: "+getProperty()+" Valor:"+tmp+" ind "+toString().indexOf("."));
		    if( value.toString().indexOf(".")>-1){
						StringTokenizer tok = new StringTokenizer(value.toString(),".");
						String tmp2 ="";
						while(tok.hasMoreTokens())
							tmp2+=tok.nextToken();
						if(tmp2.length()>0){
						    value = tmp2;
						}		        
		    }

				if(getProperty().toUpperCase().indexOf("CNPJ")>-1){
						String  mascaraCnpj = "##.###.###/####-##";
						value = ConverterUtils.aplicaMascara(value.toString(),mascaraCnpj);
						
					}else if(getProperty().toUpperCase().indexOf("CPF")>-1){
						String  mascaraCpf = "###.###.###-##";
						value = ConverterUtils.aplicaMascara(value.toString(),mascaraCpf);
					}
				
				return value.toString();
				
		}
				
	

		
		
		try {
            return value.toString();
        } catch (Exception e) {
            
            throw new JspException("Erro ao converter propriedade "+getProperty()+" = "+value+". "+e.getMessage());
        }
	}
	
	
	public String getDecimal() {
		return decimal;
	}
	public void setDecimal(String decimal) {
		this.decimal = decimal;
	}

}
