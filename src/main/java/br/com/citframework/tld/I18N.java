/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.tld;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.com.centralit.citcorpore.util.WebUtil;
import br.com.citframework.util.UtilI18N;

/**
 *
 * @author Cleison Ferreira de Melo
 * @since 21/03/2012
 *
 */
public class I18N extends BodyTagSupport {

    private static final long serialVersionUID = 2714552161457953214L;

    private static final Logger LOGGER = Logger.getLogger(I18N.class);

    private String key;
    private String locale;

    @Override
    public int doStartTag() throws JspException {
        try {
            final HttpSession session = pageContext.getSession();
            setLocale(WebUtil.getLanguage((HttpServletRequest) pageContext.getRequest()));
            String siglaLocale = this.getLocale();
            if(pageContext.getRequest() != null && !((HttpServletRequest) pageContext.getRequest()).isRequestedSessionIdValid()){
            	/*Sess�o inv�lida, usar o locale padr�o*/
           }else{
	            if (session != null && session.getAttribute("locale") != null) {
	                final String sessionLocale = session.getAttribute("locale").toString().trim();
	                if (StringUtils.isNotBlank(sessionLocale)) {
	                    siglaLocale = sessionLocale;
	                }
	            }
            }

            final String value = UtilI18N.internacionaliza(siglaLocale, this.getKey());
            pageContext.getOut().print(value);
        } catch (final Exception e) {
            LOGGER.warn("Erro nas taglibs: " + e.getMessage(), e);
        }

        return SKIP_BODY;
    }

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(final String locale) {
        this.locale = locale;
    }

}
