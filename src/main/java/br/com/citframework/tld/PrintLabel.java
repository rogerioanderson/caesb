/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.tld;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import br.com.citframework.util.Label;

public class PrintLabel extends TagSupport{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2748514436529792606L;
	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int doStartTag() throws JspException {
		
		String valor =  Label.getValue(key);
		if(valor==null || valor.trim().length()==0){
			InputStream is = this.pageContext.getServletContext().getResourceAsStream("/WEB-INF/classes/" + Label.fileName);
			
			Label.setProp(is);
			valor =  Label.getValue(key);
			
			if(valor==null || valor.trim().length()==0){
				throw new JspException("N�o foi encontrada a chave "+key+" no arquivo Label.properties" );
			}
		}
		
		try {
			pageContext.getOut().print(valor.trim());
		} catch (IOException e) {
             throw new JspException(e);
		}
		return SKIP_BODY;
	}
	
	
	
	
	

}
