/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.push.google;

import java.util.List;

import br.com.citframework.push.MessageResponse;

/**
 * Padr�o de mensagem de response que o <a href="http://developer.android.com/google/gcm/gs.html">Google Cloud Message</a> retorna
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 15/11/2014
 */
public class GoogleCloudMessageResponse extends MessageResponse {

    private int httpCode;

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(final int httpCode) {
        this.httpCode = httpCode;
    }

    private Long multicast_id;
    private Integer success;
    private Integer failure;
    private Long canonical_ids;
    private List<GoogleResultsMessage> results;

    public Long getMulticastId() {
        return multicast_id;
    }

    public void setMulticastId(final Long multicast_id) {
        this.multicast_id = multicast_id;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(final Integer success) {
        this.success = success;
    }

    public Integer getFailure() {
        return failure;
    }

    public void setFailure(final Integer failure) {
        this.failure = failure;
    }

    public Long getCanonicalIds() {
        return canonical_ids;
    }

    public void setCanonicalIds(final Long canonical_ids) {
        this.canonical_ids = canonical_ids;
    }

    public List<GoogleResultsMessage> getResults() {
        return results;
    }

    public void setResults(final List<GoogleResultsMessage> results) {
        this.results = results;
    }

}
