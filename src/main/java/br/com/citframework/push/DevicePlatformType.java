/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.push;

import org.apache.commons.lang.StringUtils;

import br.com.citframework.util.Assert;

/**
 * Enumerado dos tipos de SO de device para os quais podem ser enviadas push messages
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 15/11/2014
 */
public enum DevicePlatformType {

    ANDROID(1, "Android"),
    IOS(2, "iOS");

    private final Integer id;
    private final String description;

    private DevicePlatformType(final Integer id, final String description) {
        this.id = id;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    /**
     * Recupera uma {@link DevicePlatformType} de acordo com seu identificador
     *
     * @param id
     *            identificador a ser verificada se h� uma {@link DevicePlatformType}
     * @return {@link DevicePlatformType} caso encontre. {@link IllegalArgumentException}, caso contr�rio
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @since 15/11/2014
     */
    public static DevicePlatformType fromId(final Integer id) {
        Assert.notNull(id, "Id must not be null");
        final DevicePlatformType[] values = DevicePlatformType.values();
        for (final DevicePlatformType type : values) {
            if (id.equals(type.getId())) {
                return type;
            }
        }
        throw new IllegalArgumentException(String.format("DevicePlatformType not found for identifier '%s'", id));
    }

    /**
     * Recupera uma {@link DevicePlatformType} de acordo com sua descri��o
     *
     * @param description
     *            descric�o a ser verificada se h� uma {@link DevicePlatformType}
     * @return {@link DevicePlatformType} caso encontre. {@link IllegalArgumentException}, caso contr�rio
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @since 15/11/2014
     */
    public static DevicePlatformType fromDescription(final String description) {
        Assert.notNullAndNotEmpty(description, "Description must not be null or empty");
        final DevicePlatformType[] values = DevicePlatformType.values();
        for (final DevicePlatformType type : values) {
            if (StringUtils.equalsIgnoreCase(description, type.getDescription())) {
                return type;
            }
        }
        throw new IllegalArgumentException(String.format("DevicePlatformType not found for description '%s'", description));
    }

}
