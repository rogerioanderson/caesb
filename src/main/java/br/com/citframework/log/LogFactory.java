/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.log;

import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;

public class LogFactory {

    public static String CIT_LOG = "CIT_LOG";
    public static String LOG4J_LOG = "LOG4J_LOG";
    public static String DB_LOG = "DB_LOG";

    public static Log getLog() {
        String tipoLog = null;
        try {
            tipoLog = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.TIPO_LOG, "DB_LOG");
        } catch (final Exception e) {
            e.printStackTrace();
        }

        if (tipoLog == null) {
            tipoLog = LOG4J_LOG;
        }

        if (tipoLog.equals(CIT_LOG)) {
            return new CitLog();
        }
        if (tipoLog.equals(LOG4J_LOG)) {
            return new Log4jLog();
        }
        if (tipoLog.equals(DB_LOG)) {
            return new DbLog();
        }
        return null;
    }

}
