/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.log;

import java.sql.Timestamp;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import br.com.centralit.citcorpore.negocio.LogDadosService;
import br.com.citframework.dto.LogDados;
import br.com.citframework.excecao.ServiceException;
import br.com.citframework.service.ServiceLocator;
import br.com.citframework.util.UtilDatas;

public class DbLog implements Log {

    private static final Logger LOGGER = Logger.getLogger(DbLog.class);

	@Override
    public void registraLog(final String mensagem, final Class<?> classe, final String tipoMensagem) throws Exception {
		if (mensagem.indexOf(Log.SEPARADOR) > 0) {
			final StringTokenizer stok = new StringTokenizer(mensagem, SEPARADOR);
			int i = 0;
			String nomeTabela = "";
			String operacao = "";
			String dados = "";
			String idUsuario = "";
			String login = "";
			String ipOrigem = "";

			while (stok.hasMoreTokens()) {
				final String tok = stok.nextToken();
				if (i == 0) {
					nomeTabela = tok;
				} else if (i == 1) {
					operacao = tok;
				} else if (i == 2) {
					dados = tok;
				} else if (i == 3) {
					idUsuario = tok;
				} else if (i == 4) {
					login = tok;
				} else if (i == 5) {
					ipOrigem = tok;
                }
				i++;
			}

			if (nomeTabela.equalsIgnoreCase("logdados")) {
				return;
			}
			final LogDados ld = new LogDados();
			ld.setDados(dados);
			final Timestamp dataAtual = UtilDatas.getDataHoraAtual();
			ld.setDtAtualizacao(dataAtual);
			ld.setDataLog(dataAtual);
			ld.setIdUsuario(new Integer(idUsuario));
			ld.setLocalOrigem(ipOrigem);
			ld.setOperacao(operacao);
			ld.setNomeTabela(nomeTabela);
			ld.setNomeUsuario(login);

			this.getLogDadosService().create(ld);

			final DbLogArquivo dbLogArquivo = new DbLogArquivo();
			final String ldString = dbLogArquivo.formatarStringLogDados(ld);
			dbLogArquivo.registraLog(ldString, classe, tipoMensagem);
		}
    }

    @Override
    public void registraLog(final Exception e, final Class<?> classe, final String tipoMensagem) throws Exception {}

    private LogDadosService logDadosService;

    private LogDadosService getLogDadosService() throws ServiceException {
        if (logDadosService == null) {
        	logDadosService = (LogDadosService) ServiceLocator.getInstance().getService(LogDadosService.class, null);
        }
        return logDadosService;
    }

}
