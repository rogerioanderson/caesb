/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.log;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.centralit.citcorpore.util.Enumerados.ParametroSistema;
import br.com.centralit.citcorpore.util.ParametroUtil;
import br.com.citframework.util.UtilDatas;
import br.com.citframework.util.UtilTratamentoArquivos;

@SuppressWarnings({"rawtypes", "unchecked"})
public class CitLog implements Log {

    private String pathLog;
    private String fileLog;
    private String extLog;

    /**
     * @author breno.guimaraes
     */
    public CitLog() {
        this.inicializarParametros();
    }

    private void inicializarParametros() {
        pathLog = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.PATH_LOG, "C:/Program Files/jboss/server/default/deploy/CitCorpore.war/tempFiles");
        fileLog = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.FILE_LOG, "/log");
        extLog = ParametroUtil.getValorParametroCitSmartHashMap(ParametroSistema.EXT_LOG, "txt");
    }

    @Override
    public void registraLog(final String mensagem, final Class classe, final String tipoMensagem) throws Exception {
        final Date dataAtual = UtilDatas.getDataAtual();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        final String nomeArquivo = pathLog + fileLog + "_" + sdf.format(dataAtual) + "." + extLog;
        synchronized (nomeArquivo) {
            final List lista = new ArrayList();
            sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            lista.add("[" + tipoMensagem + "] - " + sdf.format(dataAtual) + " - " + classe.getName() + " - " + mensagem);
            UtilTratamentoArquivos.geraFileTXT(nomeArquivo, lista, true);
        }
    }

    @Override
    public void registraLog(final Exception e, final Class classe, final String tipoMensagem) throws Exception {
        final Date dataAtual = UtilDatas.getDataAtual();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        final String nomeArquivo = pathLog + fileLog + "_" + sdf.format(dataAtual) + "." + extLog;
        final List lista = new ArrayList();
        sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        lista.add("[" + tipoMensagem + "] - " + sdf.format(dataAtual) + " - " + classe.getName() + " - Exception:");
        synchronized (nomeArquivo) {
            try (final FileOutputStream fos = new FileOutputStream(nomeArquivo, true)) {
                final PrintStream out = new PrintStream(fos);
                UtilTratamentoArquivos.geraFileTXT(nomeArquivo, lista, out);
                e.printStackTrace(out);
            }
        }
    }

}
