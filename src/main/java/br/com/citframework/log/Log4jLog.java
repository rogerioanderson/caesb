/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.log;

import org.apache.log4j.Logger;

public class Log4jLog implements Log {

    @Override
    public void registraLog(final String mensagem, final Class<?> classe, final String tipoMensagem) throws Exception {
        final Logger log = Logger.getLogger(classe);
        if (tipoMensagem.equals(DEBUG)) {
            log.debug(mensagem);
        } else if (tipoMensagem.equals(ERROR)) {
            log.error(mensagem);
        } else if (tipoMensagem.equals(FATAL)) {
            log.fatal(mensagem);
        } else if (tipoMensagem.equals(INFO)) {
            log.info(mensagem);
        } else if (tipoMensagem.equals(WARN)) {
            log.warn(mensagem);
        }
    }

    @Override
    public void registraLog(final Exception e, final Class<?> classe, final String tipoMensagem) throws Exception {
        final Logger log = Logger.getLogger(classe);
        if (tipoMensagem.equals(DEBUG)) {
            log.debug(e);
        } else if (tipoMensagem.equals(ERROR)) {
            log.error(e);
        } else if (tipoMensagem.equals(FATAL)) {
            log.fatal(e);
        } else if (tipoMensagem.equals(INFO)) {
            log.info(e);
        } else if (tipoMensagem.equals(WARN)) {
            log.warn(e);
        }
    }

}
