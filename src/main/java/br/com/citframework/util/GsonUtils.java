/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import java.io.Reader;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.Date;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

public final class GsonUtils {

    private static final Gson GSON = createGson(true, null);

    private static final Gson GSON_NO_NULLS = createGson(false,null);
    
    private static final Gson GSON_NO_DATES = createGsonWithoutDates(true);
    
    private static final Gson GSON_NO_DATES_NO_NULLS = createGsonWithoutDates(false);
    
    private static final String DATE_FORMAT = "MMM dd, yyyy";

    /**
     * Create the standard {@link Gson} configuration
     *
     * @return created gson, never null
     */
    public static final Gson createGson() {
        return createGson(true,null);
    }

    /**
     * Create the standard {@link Gson} configuration
     *
     * @param serializeNulls whether nulls should be serialized
     * @return created gson, never null
     */
    public static final Gson createGson(final boolean serializeNulls, String dateFormat) {
        final GsonBuilder builder = new GsonBuilder();
        if (serializeNulls) {
            builder.serializeNulls();
        }      
        builder.setDateFormat( dateFormat != null ? dateFormat : DATE_FORMAT);
        builder.setExclusionStrategies(new ExclusionStrategy() {
        	
    		public boolean shouldSkipClass(Class<?> arg0) {
                return false;
            }

            public boolean shouldSkipField(FieldAttributes f) {
            	 final Expose expose = f.getAnnotation(Expose.class);
                 return expose != null;
            }

        });
        return builder.create();
    }
    
    public static final Gson createGsonWithoutDates(final boolean serializeNulls) {
    	final GsonBuilder builder = new GsonBuilder();
        if (serializeNulls) {
            builder.serializeNulls();
        }      
        builder.setExclusionStrategies(new ExclusionStrategy() {
    		public boolean shouldSkipClass(Class<?> arg0) {
                return false;
            }

            public boolean shouldSkipField(FieldAttributes f) {
            	Class<?> czz = f.getDeclaredClass();
            	if (czz == Timestamp.class
            			|| czz == Date.class
            			|| czz == java.sql.Date.class) {
            		return true;
            	}
            	
                return false;
            }
        });
        return builder.create();
    }

    /**
     * Get reusable pre-configured {@link Gson} instance
     *
     * @return Gson instance
     */
    public static final Gson getGson() {
        return GSON;
    }

    /**
     * Get reusable pre-configured {@link Gson} instance
     *
     * @return Gson instance
     */
    public static final Gson getGson(final boolean serializeNulls) {
        return serializeNulls ? GSON : GSON_NO_NULLS;
    }
    
    public static final Gson getGsonWithoutDates() {
    	return GSON_NO_DATES;
    }
    
    public static final Gson getGsonWithoutDates(final boolean serializeNulls) {
    	return serializeNulls ? GSON_NO_DATES : GSON_NO_DATES_NO_NULLS;
    }

    /**
     * Convert object to json
     *
     * @return json string
     */
    public static final String toJson(final Object object) {
        return toJson(object, true);
    }

    /**
     * Convert object to json
     *
     * @return json string
     */
    public static final String toJson(final Object object, final boolean includeNulls) {
        return includeNulls ? GSON.toJson(object) : GSON_NO_NULLS.toJson(object);
    }

    /**
     * Convert string to given type
     *
     * @return instance of type
     */
    public static final <V> V fromJson(String json, Class<V> type) {
        return GSON.fromJson(json, type);
    }

    /**
     * Convert string to given type
     *
     * @return instance of type
     */
    public static final <V> V fromJson(String json, Type type) {
        return GSON.fromJson(json, type);
    }

    /**
     * Convert content of reader to given type
     *
     * @return instance of type
     */
    public static final <V> V fromJson(Reader reader, Class<V> type) {
        return GSON.fromJson(reader, type);
    }

    /**
     * Convert content of reader to given type
     *
     * @return instance of type
     */
    public static final <V> V fromJson(Reader reader, Type type) {
        return GSON.fromJson(reader, type);
    }
    
}


