/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.citframework.comparacao.ObjectSimpleComparator;
import br.com.citframework.excecao.CompareException;

@SuppressWarnings({"unchecked", "rawtypes"})
public class UtilListas {
	/**
	 * Ordena uma lista de beans (nao importa o tipo), atraves de uma propriedade (get).
	 * @param lst
	 * @param getProperty (exemplo: "getDataInicio") - Pode ser data, Numero, String, ...
	 * @param typeOrder ("ASC" ou "DESC") - pode ser utilizado: ObjectSimpleComparator.ASC, ObjectSimpleComparator.DESC
	 * @return A colecao ordenada pela propriedade indicada
	 * @throws CompareException
	 */
	public static Collection sortListOfBensByProperty(List lst, String getProperty, String typeOrder) throws CompareException{
		if (lst == null) return new ArrayList();
		Collections.sort(lst, new ObjectSimpleComparator(getProperty, typeOrder));
		return lst;
	}
	
	public static Map<String, Integer> agrupaDuplicadosComQtde(List<String> lista) {
		Map<String, Integer> listaAgrupada = new HashMap<String, Integer>();
		Set<String> set = new HashSet<>(lista);
		for (String item : set) {
			listaAgrupada.put(item, Collections.frequency(lista, item));
		}
		return listaAgrupada;
	}
}
