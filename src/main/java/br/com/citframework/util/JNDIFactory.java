/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.resource.ResourceException;

import org.apache.commons.lang.StringUtils;

/**
 * Recupera recurso publicados na JNDI
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 19/08/2014
 *
 */
public final class JNDIFactory implements IContextFactory<Object> {

    private static final Logger LOGGER = Logger.getLogger(JNDIFactory.class.getName());

    private Context context;

    /**
     * Constr�i um {@link JNDIFactory} com propriedades padr�o do contexto
     *
     */
    public JNDIFactory() {
        try {
            context = new InitialContext();
        } catch (final NamingException e) {
            final String mensagem = "Ocorreu um erro ao inicializar o contexto JNDI: " + e.getMessage();
            LOGGER.log(Level.SEVERE, mensagem, e);
        }
    }

    /**
     * Constr�i um {@link JNDIFactory} informado propriedades do contexto
     *
     * @param properties
     *            propriedades a serem utilizadas no contexto
     */
    public JNDIFactory(final Properties properties) {
        try {
            context = new InitialContext(properties);
        } catch (final NamingException e) {
            final String mensagem = "Ocorreu um erro ao inicializar o contexto JNDI: " + e.getMessage();
            LOGGER.log(Level.SEVERE, mensagem, e);
        }
    }

    @Override
    public Object getResource(final String resourceName) throws ResourceException {
        try {
            return context.lookup(this.normalizeJNDIResourceName(resourceName));
        } catch (final NamingException e) {
            final String mensagem = "Ocorreu um erro ao tentar localizar objeto na refer�ncia " + resourceName + " no servidor: " + e.getMessage();
            LOGGER.log(Level.SEVERE, mensagem, e);
            throw new ResourceException(mensagem, e);
        }
    }

    @Override
    public Boolean putResource(final Context context, final String name, final Object object) throws ResourceException {
        try {
            context.bind(name, object);
            return true;
        } catch (final NamingException e) {
            LOGGER.log(Level.WARNING, "N�o foi poss�vel fazer bind do objeto: " + e.getMessage(), e);
        }
        return false;
    }

    private static final String javaStart = "java:/";
    private static final String jbossStart = "jboss:/";
    private static final String contextoConexao = Constantes.getValue("CONTEXTO_CONEXAO");

    private String normalizeJNDIResourceName(final String jndiName) {
        final String[] jndiNameStarts = new String[] {javaStart, jbossStart};
        String completeJNDIName = jndiName;
        if (!StringUtils.startsWithAny(jndiName, jndiNameStarts)) {
            if (StringUtils.isNotBlank(contextoConexao)) {
                completeJNDIName = contextoConexao.concat(jndiName);
            } else {
                completeJNDIName = javaStart.concat(jndiName);
            }
        }
        return completeJNDIName;
    }

}
