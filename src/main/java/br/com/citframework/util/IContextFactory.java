/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import javax.naming.Context;
import javax.resource.ResourceException;

/**
 * Interface para recupera��o de recursos.
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 19/08/2014
 *
 */
public interface IContextFactory<E> {

    /**
     * Recupera um recurso de acordo com seu nome
     *
     * @param name
     *            nome do recurso a ser recuperado
     * @return {@link Object}
     * @throws ResourceException
     *             caso ocorra algum problema ao recuperar o recurso
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @since 19/08/2014
     */
    E getResource(final String name) throws ResourceException;

    /**
     * Faz bind de um recurso em um contexto
     *
     * @param object
     *            objeto a ser feito bind
     * @param name
     *            nome de refer�ncia do objeto no contexto
     * @param context
     *            contexto em que o objeto ser� armazenado
     * @return {@code true}, caso tenha tido sucesso ao fazer o bind. {@code false}, caso contr�rio
     * @throws ResourceException
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @since 19/08/2014
     */
    Boolean putResource(final Context context, final String name, final E object) throws ResourceException;

}
