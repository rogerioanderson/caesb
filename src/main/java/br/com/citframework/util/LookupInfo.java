/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import java.util.ArrayList;
import java.util.Collection;

public class LookupInfo {
	private String nome;
	private String tabela;
	private Collection colCamposPesquisa;
	private Collection colCamposRetorno;
	private Collection colCamposChave;
	private Collection colCamposOrdenacao;
	private String daoProcessor;
	private String where;
	private String scriptRef;
	private Collection colScriptRef;
	private String script;
	private String separaCampos;
	public Collection getColCamposChave() {
		return colCamposChave;
	}
	public void setColCamposChave(Collection colCamposChave) {
		this.colCamposChave = colCamposChave;
	}
	public Collection getColCamposOrdenacao() {
		return colCamposOrdenacao;
	}
	public void setColCamposOrdenacao(Collection colCamposOrdenacao) {
		this.colCamposOrdenacao = colCamposOrdenacao;
	}
	public Collection getColCamposPesquisa() {
		return colCamposPesquisa;
	}
	public void setColCamposPesquisa(Collection colCamposPesquisa) {
		this.colCamposPesquisa = colCamposPesquisa;
	}
	public Collection getColCamposRetorno() {
		return colCamposRetorno;
	}
	public void setColCamposRetorno(Collection colCamposRetorno) {
		this.colCamposRetorno = colCamposRetorno;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTabela() {
		return tabela;
	}
	public void setTabela(String tabela) {
		this.tabela = tabela;
	}
	public String getDaoProcessor() {
		return daoProcessor;
	}
	public void setDaoProcessor(String daoProcessor) {
		this.daoProcessor = daoProcessor;
	}
	public String getWhere() {
		return where;
	}
	public void setWhere(String where) {
		this.where = where;
	}
	public String getScriptRef() {
		return scriptRef;
	}
	public void setScriptRef(String scriptRef) {
		this.scriptRef = scriptRef;
		if (scriptRef == null || scriptRef.trim().equalsIgnoreCase("")){
			this.colScriptRef = null;
			return;
		}
		String scriptRefAux = scriptRef;
		scriptRefAux += ";";
		String str[] = scriptRefAux.split(";");
		this.colScriptRef = new ArrayList();
		if (str != null){
			for(int i=0; i < str.length; i++){
				this.colScriptRef.add(str[i]);
			}
		}
	}
	public Collection getColScriptRef() {
		return colScriptRef;
	}
	public void setColScriptRef(Collection colScriptRef) {
		this.colScriptRef = colScriptRef;
	}
	public String getScript() {
		return script;
	}
	public void setScript(String script) {
		this.script = script;
	}
	public String getSeparaCampos() {
		if (this.separaCampos == null) return "N";
		return separaCampos;
	}
	public void setSeparaCampos(String separaCampos) {
		this.separaCampos = separaCampos;
	}
}
