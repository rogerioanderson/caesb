/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import org.w3c.dom.Document;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;
import com.lowagie.text.html.HtmlParser;

public class Html2Pdf {   
	  
    public static void convert(String input, OutputStream out) throws DocumentException, UnsupportedEncodingException{   
        convert(new ByteArrayInputStream(input.getBytes("ISO-8859-1")), out);   
    }   
    
    /**
     * O campo style, define tamanhos da pagina e margens:
     * 				Exemplo: <style>@page {size: 4.18in 6.88in; margin: 30px 20px 15px 35px; size:landscape;}</style>
     * @param input
     * @param out
     * @param style
     * @throws DocumentException
     */
    public static void convert(String input, OutputStream out, String style) throws DocumentException{
    	String str = input;
    	if (str == null){
    		str = "";
    	}
    	str = style + str;
    	convert(new ByteArrayInputStream(str.getBytes()), out);   
    }   
       
    public static void convert(InputStream input, OutputStream out) throws DocumentException{   
        Tidy tidy = new Tidy();  
        Document doc = tidy.parseDOM(input, null);   
        ITextRenderer renderer = new ITextRenderer();  
        renderer.setDocument(doc, null);   
        renderer.layout();     
        renderer.createPDF(out);                   
    }   
    
    public static void convert(com.lowagie.text.Document document, String strNameFileInDisk) throws DocumentException{   
    	HtmlParser.parse(document, strNameFileInDisk);
    }
  
} 
