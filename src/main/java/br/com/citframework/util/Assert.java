/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import org.apache.commons.lang.StringUtils;

/**
 * Utilit�rio pra facilitar asser��es e valida��es
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 02/10/2014
 *
 */
public final class Assert {

    private Assert() {}

    private static final String DEFAULT_ASSERT_NULL_MESSAGE = "[ASSERTION FAILED] - The object must be null";
    private static final String DEFAULT_ASSERT_NOT_NUL_MESSAGE = "[ASSERTION FAILED] - The object must not be null";
    private static final String DEFATUL_ASSERT_TRUE_MESSAGE = "[ASSERTION FAILED] - This expression must be true";

    /**
     * Valida uma express�o booleana
     *
     * @param expression
     *            express�o a ser avaliada
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @since 02/10/2014
     * @throws IllegalArgumentException
     *             se a express�o � {@code false}
     */
    public static void isTrue(final boolean expression) {
        isTrue(expression, DEFATUL_ASSERT_TRUE_MESSAGE);
    }

    /**
     * Valida uma express�o booleana
     *
     * @param expression
     *            express�o a ser avaliada
     * @param message
     *            message de erro a ser exibida
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @since 02/10/2014
     * @throws IllegalArgumentException
     *             se a express�o � {@code false}
     */
    public static void isTrue(final boolean expression, final String message) {
        if (!expression) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Valida se um objeto � nulo
     *
     * @param object
     *            objeto para valida��o
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @since 02/10/2014
     * @throws IllegalArgumentException
     *             se o objeto n�o � nulo
     */
    public static void isNull(final Object object) {
        isNull(object, DEFAULT_ASSERT_NULL_MESSAGE);
    }

    /**
     * Valida se um objeto � nulo
     *
     * @param object
     *            objeto para valida��o
     * @param message
     *            message de erro a ser exibida
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @since 02/10/2014
     * @throws IllegalArgumentException
     *             se o objeto n�o � nulo
     */
    public static void isNull(final Object object, final String message) {
        if (object != null) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Valida se um objeto � n�o nulo
     *
     * @param object
     *            objeto para valida��o
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @since 02/10/2014
     * @throws IllegalArgumentException
     *             se o objeto � nulo
     */
    public static void notNull(final Object object) {
        notNull(object, DEFAULT_ASSERT_NOT_NUL_MESSAGE);
    }

    /**
     * Valida se um objeto � n�o nulo
     *
     * @param object
     *            objeto para valida��o
     * @param message
     *            message de erro a ser exibida
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @since 02/10/2014
     * @throws IllegalArgumentException
     *             se o objeto � nulo
     */
    public static void notNull(final Object object, final String message) {
        if (object == null) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Valida se uma {@link String} � n�o nula e n�o vazia
     *
     * @param object
     *            objeto para valida��o
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @since 23/10/2014
     */
    public static void notNullAndNotEmpty(final Object object) {
        notNullAndNotEmpty(object, DEFAULT_ASSERT_NOT_NUL_MESSAGE);
    }

    /**
     * Valida se uma {@link String} � n�o nula e n�o vazia
     *
     * @param object
     *            objeto para valida��o
     * @param message
     *            message de erro a ser exibida
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     * @since 23/10/2014
     * @throws IllegalArgumentException
     *             se o objeto � nulo e, se {@link String}, tamb�m vazio
     */
    public static void notNullAndNotEmpty(final Object object, final String message) {
        notNull(object, message);
        if (object instanceof String) {
            final String strObject = (String) object;
            if (StringUtils.isBlank(strObject)) {
                throw new IllegalArgumentException(message);
            }
        }
    }
}
