/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Mensagens {
	
	
	private static Properties props = null;
	private static final String fileName = "Mensagens.properties";
	
	public static InputStream inputStreamSettedInLoad = null;
	
	static{
		
		props = new Properties();
		ClassLoader load = Mensagens.class.getClassLoader();
		InputStream is = load.getResourceAsStream(fileName);
		if (is == null){
			is = ClassLoader.getSystemResourceAsStream(fileName);
		}
		if (is == null){
			is = ClassLoader.getSystemClassLoader().getResourceAsStream(fileName);
		}		
		try {
			if (is == null){
				is = inputStreamSettedInLoad;
			}	
			if (is != null){
				props.load(is);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String getValue(String value) {
//		if (props == null){
//			InputStream is = inputStreamSettedInLoad;
//			if (is != null){
//				try {
//					props.load(is);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}	
//			if (props == null){ //Se ainda continuar nulo.
//				return null;
//			}
//		}
		
		if(props == null){
			return null;
		}
		return props.getProperty(value);
	}

}
