/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.com.centralit.citcorpore.util.AdaptacaoBD;
import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.citframework.dto.ParametroDTO;
import br.com.citframework.integracao.ParametroDao;

public final class SQLConfig {

    private static final Logger LOGGER = Logger.getLogger(SQLConfig.class);

    private SQLConfig() {}

    public static String SGBD_PRINCIPAL = null;
    public static final String MYSQL = "MYSQL";
    public static final String ORACLE = "ORACLE";
    public static final String DB2 = "DB2";
    public static final String SQLSERVER = "SQLSERVER";
    public static final String POSTGRESQL = "POSTGRES";

    public static String traduzSQL(final String databaseAlias, final String sql) {
        String sqlAux = sql;

        if (databaseAlias.equalsIgnoreCase("jdbc/bi_citsmart")) {
            // Se o executor da query for o database do BI Citsmart, realiza somente os tratamentos para SQL Server (BI Corpore � somente utilizado no SQL Server).
            sqlAux = sqlAux.replaceAll("LENGTH", "LEN");
            sqlAux = sqlAux.replaceAll("UCASE", "UPPER");
        } else {
            // Se o executor da query for o database principal, realiza os tratamentos para o banco que estiver sendo utilizado.
            if (SGBD_PRINCIPAL == null) {
                if (CITCorporeUtil.SGBD_PRINCIPAL == null || CITCorporeUtil.SGBD_PRINCIPAL.trim().equalsIgnoreCase("")) {
                    AdaptacaoBD.getBancoUtilizado(); // este metodo atualizada o valor de CITCorporeUtil.SGBD_PRINCIPAL
                }
                SGBD_PRINCIPAL = CITCorporeUtil.SGBD_PRINCIPAL;
                if (SGBD_PRINCIPAL != null) {
                    SGBD_PRINCIPAL = SGBD_PRINCIPAL.toUpperCase();
                }
                SGBD_PRINCIPAL = UtilStrings.nullToVazio(SGBD_PRINCIPAL).trim();
                LOGGER.info("CIT Framework - Identificando o SGBD: " + SGBD_PRINCIPAL);
            }
            if (StringUtils.isBlank(SGBD_PRINCIPAL)) {
                SGBD_PRINCIPAL = ORACLE;
                LOGGER.info("CIT Framework - Identificando o SGBD: " + SGBD_PRINCIPAL);
            }
            // --- INFORMACOES PARA DB2 ----
            if (SGBD_PRINCIPAL.equalsIgnoreCase(DB2)) {
                sqlAux = sqlAux.replaceAll("UPPER", "UCASE");
                sqlAux = sqlAux.replaceAll("\\{DATAATUAL\\}", "CURRENT_DATE");
                sqlAux = sqlAux.replaceAll(" \\_AS\\_ ", " AS "); // TIRA A PALAVRA AS DOS SQLs
                sqlAux = sqlAux.replaceAll(" \\_as\\_ ", " AS "); // TIRA A PALAVRA AS DOS SQLs
                sqlAux = sqlAux.replaceAll(" \\_As\\_ ", " AS "); // TIRA A PALAVRA AS DOS SQLs
                sqlAux = sqlAux.replaceAll(" \\_aS\\_ ", " AS "); // TIRA A PALAVRA AS DOS SQLs
            }

            // --- INFORMACOES PARA ORACLE ----
            if (SGBD_PRINCIPAL.equalsIgnoreCase(ORACLE)) {
                sqlAux = sqlAux.toUpperCase();
                sqlAux = sqlAux.replaceAll("UCASE", "UPPER");
                sqlAux = sqlAux.replaceAll("\\{DATAATUAL\\}", "CURRENT_DATE");
                sqlAux = sqlAux.replaceAll("'9999-12-31'", "TO_DATE('31/12/9999')");
                if (sqlAux.contains(" AS VARCHAR2(4000)")) {
                    sqlAux = sqlAux.replaceAll(" AS ", " AS "); // TIRA A PALAVRA AS DOS SQLs
                } else {
                    sqlAux = sqlAux.replaceAll(" AS ", " ");
                }
                sqlAux = sqlAux.replaceAll(" \\_AS\\_ ", " AS "); // TIRA A PALAVRA AS DOS SQLs
                sqlAux = sqlAux.replaceAll("\"", "'");
                sqlAux = sqlAux.replaceAll("SUBSTRING", "SUBSTR");
                sqlAux = sqlAux.replaceAll("!=", "<>"); // TRATA A SINTAXE DE DIFERENTE
                if (sqlAux.indexOf("FETCH FIRST 1 ROWS ONLY") > -1) {
                    sqlAux = sqlAux.replaceAll(" FETCH FIRST 1 ROWS ONLY ", " "); // TIRA A PALAVRA AS DOS SQLs
                    if (sqlAux.indexOf(" ORDER ") > -1) {
                        if (sqlAux.indexOf(" WHERE ") > -1) {
                            sqlAux = sqlAux.replaceAll(" ORDER ", " AND ROWNUM <= 1 ORDER "); // TIRA A PALAVRA AS DOS SQLs
                        } else {
                            sqlAux = sqlAux.replaceAll(" ORDER ", " WHERE ROWNUM <= 1 ORDER "); // TIRA A PALAVRA AS DOS SQLs
                        }
                    } else {
                        if (sqlAux.indexOf(" WHERE ") > -1) {
                            sqlAux = sqlAux + " AND ROWNUM <= 1";
                        } else {
                            sqlAux = sqlAux + " WHERE ROWNUM <= 1";
                        }
                    }
                }

                if (!StringUtils.contains(sqlAux, "'DELETED'")) {
                    final Pattern pat = Pattern.compile("[\\w*\\.]*DELETED");
                    final Matcher mat = pat.matcher(sqlAux);
                    if (mat.find() && StringUtils.contains(sqlAux, "SELECT")) {
                        sqlAux = sqlAux.replaceAll("[\\w*\\.]*DELETED", "UPPER(" + mat.group() + ")");
                    }
                }
                sqlAux = sqlAux.toUpperCase();
            }

            // --- INFORMACOES PARA POSTGRESQL ----
            if (SGBD_PRINCIPAL.equalsIgnoreCase(POSTGRESQL)) {
                sqlAux = sqlAux.replaceAll("SYSDATE", "now()");
                sqlAux = sqlAux.replaceAll(" \\_AS\\_ ", " AS "); // TIRA A PALAVRA AS DOS SQLs
                sqlAux = sqlAux.replaceAll(" \\_as\\_ ", " AS "); // TIRA A PALAVRA AS DOS SQLs
                sqlAux = sqlAux.replaceAll(" \\_As\\_ ", " AS "); // TIRA A PALAVRA AS DOS SQLs
                sqlAux = sqlAux.replaceAll(" \\_aS\\_ ", " AS "); // TIRA A PALAVRA AS DOS SQLs
                sqlAux = sqlAux.replaceAll("\\{DATAATUAL\\}", "now()");
                sqlAux = sqlAux.replaceAll("TRUNC", "");
                //sqlAux = sqlAux.replaceAll("\"", "'");
                sqlAux = sqlAux.replaceAll("UCASE", "UPPER");
                sqlAux = sqlAux.replaceAll("'9999-12-31'", "TO_DATE('31/12/9999')");

                if (!StringUtils.contains(sqlAux, "'DELETED'")) {
                    final Pattern pat = Pattern.compile("[\\w*\\.]*DELETED");
                    final Matcher mat = pat.matcher(sqlAux);
                    if (mat.find() && !StringUtils.contains(sqlAux, "UPDATE") && !StringUtils.contains(sqlAux, "INSERT")) {
                        sqlAux = sqlAux.replaceAll("[\\w*\\.]*DELETED", "UPPER(" + mat.group() + ")");
                    }
                }
            }

            // --- INFORMACOES PARA SQLSERVER ----
            if (SGBD_PRINCIPAL.equalsIgnoreCase(SQLSERVER)) {
                sqlAux = sqlAux.replaceAll("LENGTH", "LEN");
                sqlAux = sqlAux.replaceAll("UCASE", "UPPER");
            }

            if (SGBD_PRINCIPAL.equalsIgnoreCase(MYSQL)) {
				// Identifica as refer�ncias que precisam de upper case
				sqlAux = sqlAux.replaceAll("'%d/%m/%Y ", "'%d/%m/%yy ");
				sqlAux = sqlAux.replaceAll(" %H:%i:%s'", " %hh:%i:%s'");

                sqlAux = sqlAux.toLowerCase();

				// Agora volta para a mascara correta apenas nas ocorr�ncias identificadas anteriormente
				sqlAux = sqlAux.replaceAll("'%d/%m/%yy ", "'%d/%m/%Y ");
				sqlAux = sqlAux.replaceAll(" %hh:%i:%s'", " %H:%i:%s'");
            }

            // Converte Elementos configurados conforme o banco de dados.
            final XmlReadDBItemConvertion xmlReadDB = XmlReadDBItemConvertion.getInstance(SGBD_PRINCIPAL);
            if (xmlReadDB != null) {
                final Collection<DBItemConvertion> col = xmlReadDB.getItens();
                if (col != null) {
                    for (final DBItemConvertion dbItemConvertion : col) {
                        sqlAux = sqlAux.replaceAll(dbItemConvertion.getNameToBeConverted().toUpperCase(), dbItemConvertion.getNameAfterConversion());
                    }
                }
            }

            final ParametroDao parametroDAO = new ParametroDao();

            while (sqlAux.toUpperCase().indexOf("{GETPARAMETER") > -1) {
                String valorParametro = "NULL";
                final int pos = sqlAux.toUpperCase().indexOf("{GETPARAMETER");
                final int ini = sqlAux.indexOf("(", pos);
                final int fim = sqlAux.indexOf(")", ini);
                final int sep = sqlAux.indexOf(",", ini);
                final String nomeModulo = sqlAux.substring(ini + 1, sep);
                final String nomeParametro = sqlAux.substring(sep + 1, fim);
                try {
                    final ParametroDTO parametro = parametroDAO.getValue(nomeModulo, nomeParametro, new Integer(Constantes.getValue("ID_EMPRESA_PROC_BATCH")));
                    if (StringUtils.isNotBlank(parametro.getValor())) {
                        valorParametro = parametro.getValor();
                        valorParametro = valorParametro.replaceAll("\n", "");
                        valorParametro = valorParametro.replaceAll("\r", "");
                        valorParametro = valorParametro.replaceAll("\\\n", "");
                        valorParametro = valorParametro.replaceAll("\\\r", "");
                    }
                } catch (final Exception e) {
                    LOGGER.warn(e.getMessage(), e);
                }
                sqlAux = sqlAux.substring(0, pos) + valorParametro + sqlAux.substring(fim + 2, sqlAux.length());
            }

            sqlAux = sqlAux.replaceAll("\\{OWNER\\}", Constantes.getValue("OWNER_DB"));
            sqlAux = sqlAux.replaceAll("\\{OWNER_BD\\}", Constantes.getValue("OWNER_DB"));
        }

        return sqlAux;
    }

	public static void verificaSGBDprincipal() {
		if (SGBD_PRINCIPAL == null) {
		    if (CITCorporeUtil.SGBD_PRINCIPAL == null || CITCorporeUtil.SGBD_PRINCIPAL.trim().equalsIgnoreCase("")) {
		        AdaptacaoBD.getBancoUtilizado(); // este metodo atualiza o valor de CITCorporeUtil.SGBD_PRINCIPAL
		    }
		    SGBD_PRINCIPAL = CITCorporeUtil.SGBD_PRINCIPAL;
		    if (SGBD_PRINCIPAL != null) {
		        SGBD_PRINCIPAL = SGBD_PRINCIPAL.toUpperCase();
		    }
		    SGBD_PRINCIPAL = UtilStrings.nullToVazio(SGBD_PRINCIPAL).trim();
		    LOGGER.info("CIT Framework - Identificando o SGBD: " + SGBD_PRINCIPAL);
		}
		if (StringUtils.isBlank(SGBD_PRINCIPAL)) {
		    SGBD_PRINCIPAL = ORACLE;
		    LOGGER.info("CIT Framework - Identificando o SGBD: " + SGBD_PRINCIPAL);
		}
	}
	
	public static Boolean isMySqlSGBDPrincipal() {
		return SGBD_PRINCIPAL.equalsIgnoreCase(MYSQL);
	}
	
	public static Boolean isDB2SGBDPrincipal() {
		return SGBD_PRINCIPAL.equalsIgnoreCase(DB2);
	}
	
	public static Boolean isPostgresSGBDPrincipal() {
		return SGBD_PRINCIPAL.equalsIgnoreCase(POSTGRESQL);
	}
	
	public static Boolean isOracleSGBDPrincipal() {
		return SGBD_PRINCIPAL.equalsIgnoreCase(ORACLE);
	}
	
	public static Boolean isSqlServerSGBDPrincipal() {
		return SGBD_PRINCIPAL.equalsIgnoreCase(SQLSERVER);
	}
	
	public static String getFuncaoRemoveAcento() {
		String funcaoRemoveAcento = "";
		
		if (isSqlServerSGBDPrincipal()) {
			funcaoRemoveAcento = "dbo.REMOVE_ACENTO(";			
		} else if (isOracleSGBDPrincipal() || isPostgresSGBDPrincipal()) {
			funcaoRemoveAcento = "REMOVE_ACENTO(";
		}
		
		return funcaoRemoveAcento;
	}
	
	public static String getFechamentoFuncaoRemoveAcento() {
		String fechamentoFuncaoRemoveAcento = ")";
		
		if (isMySqlSGBDPrincipal()) {
			fechamentoFuncaoRemoveAcento = "";			
		}
		
		return fechamentoFuncaoRemoveAcento;
	}

	/**
     * Desenvolvedor: Douglas Japiassu | Data: 29/05/2015 | Hora: 14:00 | ID CITSmart: 155819 | Motivo: Reaproveitando c�digo
     * e adicionando tratamento para fun��o TRIM do SQLSERVER.
     */
	private static String tratamentoSqlServer(String sqlAux) {
		sqlAux = sqlAux.replaceAll("LENGTH", "LEN");
        sqlAux = sqlAux.replaceAll("UCASE", "UPPER");
        
        if (sqlAux.contains("TRIM")){
        	sqlAux = tratamentoTrimSqlServer(sqlAux);
        }
		
		return sqlAux;
	}

	/**
	 * Faz a remontagem da SQL, trocando apenas as express�es TRIM(texto) por LTRIM(RTRIM(texto))
	 * pois o banco de dados SQLSERVER n�o reconhece essa fun��o.
	 * @param sqlAux
	 * @return sql tratado
	 * @author douglas.japiassu
	 */
	private static String tratamentoTrimSqlServer(String sqlAux) {
		String expressaoTrim = "TRIM\\(";
		int TAMANHO_EXPRESSAO_TRIM = 4;
		int indiceOcorrenciaTrim = 0;
		String sql = "";
		
		String[] sqlAuxSplit = sqlAux.split(expressaoTrim);
		
		for (int i = 0; i < sqlAuxSplit.length; i++) {
			// A primeira ocorr�ncia n�o cont�m a express�o
			if (i == 0) {
				sql = sqlAux.substring(0, sqlAuxSplit[i].length());
			} else {
				// verifica em qual posi��o da SQL original estava a express�o
				indiceOcorrenciaTrim += sqlAuxSplit[i - 1].length() + TAMANHO_EXPRESSAO_TRIM;
				int indiceFinal = sqlAuxSplit[i].length() + 1;
				
				String sqlAuxTrim = sqlAux.substring(indiceOcorrenciaTrim);
				
				String conteudo = getConteudoDaExpressaoPorDelimitador(sqlAuxTrim, "(", ")");
				
				sql += "LTRIM(RTRIM".concat(conteudo).concat(")");
				// Concatena com o restante da SQL que n�o fazia parte do TRIM, 
				// a partir do indice de ocorr�ncia somando com o tamanho do conte�do
				sql += sqlAux.substring(indiceOcorrenciaTrim + conteudo.length(), indiceOcorrenciaTrim + indiceFinal);
				indiceOcorrenciaTrim++;
			}
		}
		
		return sql;
	}
	
	/**
	 * Pega o conte�do entre os 2 delimitadores passados por par�metro.
	 * ATEN��O: 1. Delimitadores s� podem ter 1 caractere;
	 * 			2. delimitadorInicial DEVE ser DIFERENTE do delimitadorFinal
	 * @param texto
	 * @param delimitadorInicial Ex: (, {, <
	 * @param delimitadorFinal Ex: ), }, >
	 * @return conteudo O conte�do entre os delimitadores
	 * @author douglas.japiassu
	 */
	private static String getConteudoDaExpressaoPorDelimitador(String texto, String delimitadorInicial, String delimitadorFinal) {
		int indiceInicial = texto.indexOf(delimitadorInicial);
		int posicaoAtual = indiceInicial;
		int delimitadorAberto = 0;
		Boolean continuarBuscando = true;

		while (continuarBuscando && posicaoAtual < texto.length()) {
			String caractereAtual = Character.toString(texto.charAt(posicaoAtual));

			if (caractereAtual.equals(delimitadorInicial)) {
				delimitadorAberto++;
			} else if (caractereAtual.equals(delimitadorFinal)) {
				delimitadorAberto--;
			}

			posicaoAtual++;
			if (delimitadorAberto == 0) {
				continuarBuscando = false;
			}
		}

		String conteudo = texto.substring(indiceInicial, posicaoAtual);
		
		return conteudo;
	}
}
