/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util.rest;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.citframework.util.UtilStrings;


public class RestOutput implements Serializable {
	
	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 1L;

	private String response;
	
	private Integer httpStatus;
	
	private String error;

	public RestOutput(String response, Integer httpStatus) {
		this.response = response;
		this.httpStatus = httpStatus;
		this.error = "";
		if (httpStatus != 200 && UtilStrings.isNotVazio(response) && response.charAt(0) == '{') {
			try {
				JSONObject resp;
				resp = new JSONObject(response);
				if (resp.has("ex")) {
					JSONObject ex = resp.getJSONObject("ex");
					error = ex.getString("message");
				}
			} catch (JSONException e) {
			}
		}		
	}
	
	/**
	 * Retorna o valor do atributo <code>response</code>
	 *
	 * @return <code>String</code>
	 */
	public String getResponse() {
	
		return response;
	}

	
	/**
	 * Define o valor do atributo <code>response</code>.
	 *
	 * @param response 
	 */
	public void setResponse(String response) {
	
		this.response = response;
	}

	
	/**
	 * Retorna o valor do atributo <code>httpStatus</code>
	 *
	 * @return <code>Integer</code>
	 */
	public Integer getHttpStatus() {
	
		return httpStatus;
	}

	
	/**
	 * Define o valor do atributo <code>httpStatus</code>.
	 *
	 * @param httpStatus 
	 */
	public void setHttpStatus(Integer httpStatus) {
	
		this.httpStatus = httpStatus;
	}

	
	/**
	 * Retorna o valor do atributo <code>exception</code>
	 *
	 * @return <code>String</code>
	 */
	public String getError() {
	
		return error;
	}

	
	/**
	 * Define o valor do atributo <code>exception</code>.
	 *
	 * @param exception 
	 */
	public void setError(String error) {
	
		this.error = error;
	}
	
}
