/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util.rest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import br.com.centralit.citcorpore.util.Enumerados.HttpMethodEnum;
import br.com.centralit.citcorpore.util.Enumerados.RestParameterTypeEnum;
import br.com.citframework.util.UtilStrings;

public class RestExecution {
	public static RestOutput execute(HttpMethodEnum httpMethod, String url, RestParameterTypeEnum parameterType, String parms, String encode, Integer timeout) {

		HttpClient client = new HttpClient();
        client.getHttpConnectionManager().getParams().setConnectionTimeout(timeout);
        if (httpMethod.equals(HttpMethodEnum.GET)){
        	GetMethod getMethod = new GetMethod(url);
        	getMethod.addRequestHeader("encoding", encode); 
        	getMethod.addRequestHeader("charset", encode); 
        	if (parameterType.equals(RestParameterTypeEnum.JSON)) {
        		getMethod.addRequestHeader("Content-type", "application/json");
        		getMethod.addRequestHeader("Accept", "application/xml");
        	}else{
        		getMethod.addRequestHeader("Content-type", "application/xml");
        		getMethod.addRequestHeader("Accept", "application/xml");
        	}
        	getMethod.setFollowRedirects(true);
        	try{
        		if (UtilStrings.isNotVazio(parms)) {
        	        String[] linesParms = parms.split("\n");
        	        NameValuePair[] params = new NameValuePair[linesParms.length];

        	        for(int i = 0; i < linesParms.length; i++){
    	        		if (linesParms[i] != null && !linesParms[i].trim().equalsIgnoreCase("")){
    	        			int p = linesParms[i].indexOf("=");
    	        			if (p >= 0) {
    	        				String prop = linesParms[i].substring(0, p);
    	        				String value = linesParms[i].substring(p+1);
    	        				params[i] = new NameValuePair(prop,value);
    	        			}
    	        		}
    	        	}
        	        getMethod.setQueryString(params);
        		}
        		client.executeMethod(getMethod);
        		String retorno = getMethod.getResponseBodyAsString();
        		
        		if (!getMethod.getResponseCharSet().equalsIgnoreCase(encode)) {
	        		byte p[] = retorno.getBytes(getMethod.getResponseCharSet()); 
	        		retorno = new String(p, 0, p.length, encode); 
        		}
        		
        		return new RestOutput(retorno, getMethod.getStatusCode());
        	}catch(Exception ex){
        		ex.printStackTrace();
        		throw new RuntimeException(ex);
        	}finally{
        		getMethod.releaseConnection();
        	}
        }else{
        	PostMethod postMethod = new PostMethod(url);
        	postMethod.addRequestHeader("encoding", encode); 
        	postMethod.addRequestHeader("charset", encode); 
        	if (parameterType.equals(RestParameterTypeEnum.JSON)) {
        		postMethod.addRequestHeader("Content-type", "application/json");
        		postMethod.addRequestHeader("Accept", "application/json");
        	}else{
        		postMethod.addRequestHeader("Content-type", "application/xml");
        		postMethod.addRequestHeader("Accept", "application/xml");
        	}
        	try{
        		if (parms != null && !parms.trim().equals("")){
        			StringRequestEntity requestEntity = new StringRequestEntity(
        					parms,
        					parameterType.equals(RestParameterTypeEnum.JSON) ? "application/json" : "application/xml",
        					encode);
        			postMethod.setRequestEntity(requestEntity);
        		}
        		client.executeMethod(postMethod);
        		String retorno = postMethod.getResponseBodyAsString();

        		if (!postMethod.getResponseCharSet().equalsIgnoreCase(encode)) {
	        		byte p[] = retorno.getBytes(postMethod.getResponseCharSet()); 
	        		retorno = new String(p, 0, p.length, encode); 
        		}
        		
        		return new RestOutput(retorno, postMethod.getStatusCode());
        	}catch(Exception ex){
        		ex.printStackTrace();
        		throw new RuntimeException(ex);
        	}finally{
        		postMethod.releaseConnection();
        	}        	
        }
	}
}
