/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import java.io.File;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import br.com.centralit.citcorpore.util.CITCorporeUtil;
import br.com.citframework.dto.IDto;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class DtoAdapter extends XmlAdapter<String, IDto> {
	
	private String getPath() {
		String strTempUpload = CITCorporeUtil.CAMINHO_REAL_APP
				+ "tempUpload";

		File fileDir = new File(strTempUpload);
		if (!fileDir.exists()) {
			fileDir.mkdir();
		}
		fileDir = new File(strTempUpload);
		if (!fileDir.exists()) {
			fileDir.mkdir();
		}
		
		String fileName = strTempUpload + "/temp_"+UtilDatas.getDataHoraAtual().getTime()+".xml";
		return fileName;
	}

    @Override
    public String marshal(IDto obj) throws Exception {
    	//String fileName = getPath();
		//OutputStream os = new FileOutputStream(fileName);
		//JAXB.marshal(obj, os);
		//return UtilTratamentoArquivos.getStringTextFromFileTxt(fileName,"UTF-8");
		
		/*String result = UtilTratamentoArquivos.getStringTextFromFileTxt(fileName,"UTF-8");
		if (result.indexOf("<?xml") >= 0) {
			result = result.substring(result.indexOf(">")+1);
		}
		result = result.substring(result.indexOf(">")+1);
		int p = result.length() - 1;
		for (int i = result.length() - 1; i >= 0; i--) {
			if (result.charAt(i) == '<') {
				p = i-1;
				break;
			}
		}
		result = result.substring(0,p);
		return result;*/

    	XStream x = new XStream(new DomDriver("UTF-8"));
		return x.toXML(obj);
    }

    @Override
    public IDto unmarshal(String str) throws Exception {
    	//String fileName = getPath();
    	//UtilTratamentoArquivos.geraFileTxtFromString(fileName, str);
    	//return JAXB.unmarshal(fileName, IDto.class);
		XStream x = new XStream(new DomDriver("UTF-8"));
		return (IDto) x.fromXML(str);
	}

}
