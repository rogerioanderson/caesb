/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;

import br.com.centralit.citcorpore.bean.ReleaseDTO;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
/**
 * Classe utilitaria para leitura do arquivo de release do Citsmart
 * @author flavio.santana
 * @since 23/10/2013
 */
public class HistoricoAtualizacoesCitsmart {

	private static final String DEFAULT_CHARSET = "ISO-8859-1";
	private String charset  = DEFAULT_CHARSET;
	 
	public HistoricoAtualizacoesCitsmart() {
	}
	
	@SuppressWarnings("unchecked")
	public Collection<ReleaseDTO> lerXML(String path) throws IOException {
		
		Reader reader = null;
		Collection<ReleaseDTO> listRelease = null;
		XStream x = null;
		try {
			reader = (Reader) new InputStreamReader(new FileInputStream(path), getCharset());
			 x = new XStream(new DomDriver(getCharset()));
			listRelease = (Collection<ReleaseDTO>) x.fromXML(reader);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if(reader!= null){
				reader.close();
			}
		}
		return listRelease;
	}
	
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}	 
}

