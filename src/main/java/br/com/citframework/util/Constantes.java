/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
package br.com.citframework.util;

import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

/**
 * Classe que encapsula a recupera��o de valores do {@link Properties} {@code Constantes.properties}
 */
public final class Constantes {

    private static final Logger LOGGER = Logger.getLogger(Constantes.class.getName());

    private static final Properties props = new Properties();
    private static final String CONTANTES_FILE_NAME = "Constantes.properties";

    public static String SERVER_ADDRESS = "";

    static {
        final ClassLoader load = Constantes.class.getClassLoader();

        try (InputStream is = load.getResourceAsStream(CONTANTES_FILE_NAME)) {
            if (is == null) {
                throw new RuntimeException("Arquivo de recursos nao encontrado: " + CONTANTES_FILE_NAME);
            }
            props.load(is);
        } catch (final Exception e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * Recupera, diretamente do arquivo de propriedades, valor associado � chave passada como argumento
     *
     * @param key
     *            chave a ser pesquisada no arquivo de propriedades
     * @return valor associado � key, caso encontrado, {@code null}, caso contr�rio
     */
    public static String getValue(final String key) {
        String result = null;
        if (key.equalsIgnoreCase("SERVER_ADDRESS") && StringUtils.isNotBlank(SERVER_ADDRESS)) {
            result = SERVER_ADDRESS;
        }

        result = getValueFromProperties(key);
        return result;
    }

    /**
     * Recupera, diretamente do arquivo de propriedades, valor associado � chave passada como argumento
     *
     * @param key
     *            chave a ser pesquisada no arquivo de propriedades
     * @param defaultValue
     *            valor padr�o de uma chave, caso a chave n�o seja encontrada, seja nula ou vazia
     * @return valor associado � key, caso encontrado, {@code null}, caso contr�rio
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     */
    public static String getValue(final String key, final String defaultValue) {
        final String keyValue = getValue(key);
        return StringUtils.isBlank(keyValue) ? defaultValue : keyValue;
    }

    /**
     * Recupera, diretamente do arquivo de propriedades, valor associado � chave passada como argumento
     *
     * @param key
     *            chave a ser pesquisada no arquivo de propriedades
     * @return valor associado � key, caso encontrado, {@code null}, caso contr�rio
     */
    public static String getValueFromProperties(final String key) {
        if (props == null) {
            return null;
        }
        return props.getProperty(key);
    }

    /**
     * Recupera, diretamente do arquivo de propriedades, valor associado � chave passada como argumento
     *
     * @param key
     *            chave a ser pesquisada no arquivo de propriedades
     * @param defaultValue
     *            valor padr�o de uma chave, caso a chave n�o seja encontrada, seja nula ou vazia
     * @return valor associado � key, caso encontrado, {@code null}, caso contr�rio
     * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
     */
    public static String getValueFromProperties(final String key, final String defaultValue) {
        final String keyValue = getValueFromProperties(key);
        return StringUtils.isBlank(keyValue) ? defaultValue : keyValue;
    }

}
