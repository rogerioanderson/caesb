/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * 
 */
package br.com.citframework.dto;

import java.sql.Timestamp;


/**
 * @author karem.ricarte
 *
 */
public class LogAcessoUsuario implements IDto {

	
	private static final long serialVersionUID = 1L;
	
	private Timestamp dtAcessoUsuario;
	private Integer HistAtualUsuario_idUsuario;
	private String login;
	
	
	public Timestamp getDtAcessoUsuario() {
		return dtAcessoUsuario;
	}
	public void setDtAcessoUsuario(Timestamp dtAcessoUsuario) {
		this.dtAcessoUsuario = dtAcessoUsuario;
	}
	public Integer getHistAtualUsuario_idUsuario() {
		return HistAtualUsuario_idUsuario;
	}
	public void setHistAtualUsuario_idUsuario(Integer histAtualUsuario_idUsuario) {
		HistAtualUsuario_idUsuario = histAtualUsuario_idUsuario;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	
}
