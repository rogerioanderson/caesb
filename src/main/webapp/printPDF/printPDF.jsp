<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ include file="/WEB-INF/templates/taglibs.jsp"%>

<%
	String fileURL = request.getParameter("url");
%>

<%@page import="br.com.citframework.util.Constantes"%>
<html>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
</head>

<body>
	<script>
		function recarrega(){
			document.getElementById('frameVIEW').src = '<%=fileURL%>';
		}
		function fechar(){
			window.close();
		}
	</script>
	<table width="100%" height="100%">
		<tr>
			<td width="100%">
				<input type='button' style="background-color: #ffff00" name='btnRecarregar' value='Caso n�o seja apresentado o documento, clique aqui para recarregar o documento'
						onclick='recarrega();'/>
				<input type='button' style='color: #444; font-weight: bold; background: url("${ctx}/imagens/fecharLookup.gif") no-repeat scroll top left' name='btnFechar' value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fechar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						onclick='fechar();'/>
			</td>
		</tr>
		<tr>
			<td width="100%" height="98%">
				<iframe id='frameVIEW' width="100%" height="100%" src="<%=fileURL%>"></iframe>
			</td>
		</tr>
	</table>
</body>
	<script>
		//window.moveTo(0,0);
		//window.resizeTo(screen.width,screen.height);
	</script>
</html>
