<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.ParametroCorporeDTO"%>
<%@page import="br.com.centralit.citcorpore.negocio.ParametroCorporeService"%>
<%@page import="br.com.centralit.citcorpore.negocio.UsuarioService"%>
<%@page import="br.com.citframework.service.ServiceLocator"%>
<%@page import="br.com.centralit.citcorpore.util.Enumerados.ParametroSistema" %>
<%@page import="br.com.centralit.citcorpore.integracao.ad.LDAPUtils" %>
<%@page import="br.com.centralit.citcorpore.bean.LoginDTO" %>

<%@ include file="/WEB-INF/templates/taglibs.jsp"%>

<link rel="stylesheet" type="text/css" href="${ctx}/js/themes/gray/easyui.css" >
<link rel="stylesheet" type="text/css" href="${ctx}/js/themes/icon.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/css/jquery-easy.css" />
<script type="text/javascript">
$(document).ready(function() {
	$('.abalinguas').click(function() {

		if ($("#lang").is('.hide')) {
			$("#lang").removeClass('hide').addClass('show');
		} else {
			$("#lang").removeClass('show').addClass('hide');
		}
	});

	$('#gbg4').click(function() {
		if ($("#gbd4").is('.visibilityFalse')) {
			$('#gbd4').removeClass('visibilityFalse').addClass('visibilityTrue');
		} else {
			$('#gbd4').removeClass('visibilityTrue').addClass('visibilityFalse');
		}
	});
	function hidden(){
		$('#gbd4').removeClass('visibilityTrue').addClass('visibilityFalse')
	}

	$('body').click(function(e){
		if(!$(e.target).hasClass('TRUE')) {
			$('#gbd4').removeClass('visibilityTrue').addClass('visibilityFalse');
		}
	});


	var altura = $(window).height()-140;
	$("#main_container").css("height", altura);
});

function internacionalizar(parametro){
	document.getElementById('locale').value = parametro;
	document.formInternacionaliza.fireEvent('internacionaliza');
}
</script>

<script type="text/javascript" src="${ctx}/template_new/js/lookup/jquery.ui.lookup.js"></script>

<div id='menusT' style="width: 100%; display: block; float: left; letter-spacing: 0px; margin: 0px 0px 10px;">
	<div class="navbar main hidden-print">
		<%@include file="/novoLayout/common/include/cabecalho.jsp" %>
		<%@include file="/novoLayout/common/include/menuPadrao.jsp" %>
	</div>

	<div class="grid_16">

	</div>
</div>
