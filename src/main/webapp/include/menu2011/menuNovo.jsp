<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
    <ul class="topnav">  
        <li>
        	<a href="#">Cadastros</a>
        	<ul class="subnav">
                <li><a href="../../pages/empregado/empregado.load">Colaborador</a></li>
                <li><a href="../../pages/cliente/cliente.load">Cliente</a></li>
                <li><a href="../../pages/contratos/contratos.load">Contrato</a></li>
                <li><a href="../../pages/funcionalidade/funcionalidade.load">Funcionalidade</a></li>
                <li><a href="../../pages/grupoAcesso/grupoAcesso.load">Grupo de Acesso</a></li>
                <li><a href="../../pages/atividades/atividades.load">Atividades</a></li>
                <li><a href="../../pages/painelAtividades/painelAtividades.load">Painel de Atividades</a></li>
                <li><a href="../../pages/projeto/projeto.load">Projeto</a></li>
                <li><a href="../../pages/usuario/usuario.load">Usu�rio</a></li>
        	</ul>
        </li>
        <li><a href="#">&nbsp;&nbsp;O.S.&nbsp;&nbsp;</a></li>
        <li><a href="#">Fluxo de Trabalho</a></li>
        <li><a href="#">Financeiro</a></li>
        <li><a href="#">Ger�ncia do Projeto</a></li>
        <li><a href="#">Consultas</a></li>
        <li>
        	<a href="#">Relat�rios</a>
        	<ul class="subnav">
        		<li><a href="../../pages/relatorioAtividades/relatorioAtividades.load">Relat�rio de Atividades</a></li>		
        	</ul>
        </li>
    </ul>
<link type="text/css" rel="stylesheet" href="../../include/menu2011/menuNovo.css" />
 <script type="text/javascript">
	$(document).ready(function() {

		$("ul.topnav li a").hover(function() {
			$(this).parent().find("ul.subnav").slideDown('fast').show();
			$(this).parent().hover(function() {
			}, function() {
				$(this).parent().find("ul.subnav").slideUp('slow');
			});

		}).hover(function() {
			$(this).addClass("subhover");
		}, function() {
			$(this).removeClass("subhover");
		});

	});
</script>
