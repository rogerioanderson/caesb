<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<script type="text/javascript" src="${ctx}/js/MenuGenerico.js"></script>
<link rel="alternate stylesheet" type="text/css" id="listmenu-o" href="${ctx}/css/menu/listmenu_o.css" title="Vertical 'Office'" />
<link rel="alternate stylesheet" type="text/css" id="listmenu-v" href="${ctx}/css/menu/listmenu_v.css" title="Vertical 'Earth'" />
<link rel="stylesheet" type="text/css" id="listmenu-h" href="${ctx}/css/menu/listmenu_h.css" title="Horizontal 'Earth'" />
<link rel="stylesheet" type="text/css" id="fsmenu-fallback" href="${ctx}/css/menu/listmenu_fallback.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/css/menu/divmenu.css" />
