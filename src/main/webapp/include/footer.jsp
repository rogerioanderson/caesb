<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@ page import="br.com.centralit.citcorpore.util.ParametroUtil" %>
<%@ page import="br.com.centralit.citcorpore.util.FiltroSegurancaCITSmart" %>

<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/main.css">
<link href="${ctx}/novoLayout/common/bootstrap/css/bootstrap.css" rel="stylesheet" />
<link href="${ctx}/novoLayout/common/theme/css/atualiza-antigo.css" rel="stylesheet" />

<script type="text/javascript" src="${ctx}/template_new/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="${ctx}/template_new/js/jqueryTreeview/jquery.treeview.js"></script>
<script type="text/javascript" src="${ctx}/template_new/js/jqueryTreeview/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctx}/template_new/js/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="${ctx}/template_new/js/jquery/jquery.maskedinput.js"></script>
<script type="text/javascript" src="${ctx}/template_new/js/touchPunch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="${ctx}/template_new/js/tiptip/jquery.tipTip.minified.js"></script>
<script type="text/javascript" src="${ctx}/template_new/js/colorpicker/js/colorpicker.js"></script>

<script type="text/javascript" src="${ctx}/template_new/js/isotope/jquery.isotope.min.js"></script>
<script type="text/javascript" src="${ctx}/template_new/js/fancybox/jquery.fancybox-1.3.4.js"></script>
<script type="text/javascript" src="${ctx}/template_new/js/custom/gallery.js"></script>
<script type="text/javascript" src="${ctx}/template_new/js/jQueryGantt/js/jquery.fn.gantt.js"></script>

<% if(!FiltroSegurancaCITSmart.getHaVersoesSemValidacao()) { %>
<script type="text/javascript" src="${ctx}/dwr/engine.js"></script>
<%} %>

<script type="text/javascript" src="${ctx}/dwr/util.js"></script>
<script type="text/javascript">
	function tratarEnter (field, event) {
		var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
		if (keyCode == 13) {
			var i;
			for (i = 0; i < field.form.elements.length; i++)
				if (field == field.form.elements[i])
					break;
			i = (i + 1) % field.form.elements.length;
			field.form.elements[i].focus();
			return false;
		}
		return true;
	}
	/*
	* Motivo: Corrigindo erros de scripts
	* Autor: flavio.santana
	* Data/Hora: 04/11/2013 16:19
	*/
	function resize_iframe(){}

	if (window.matchMedia("screen and (-ms-high-contrast: active), (-ms-high-contrast: none)").matches) {
		 document.documentElement.className += " " + "ie10";
	}
</script>

<div id="loading_overlay">
	<div class="loading_message round_bottom">
		<img src="${ctx}/template_new/images/loading.gif" alt="aguarde..." />
	</div>
</div>

<script type="text/javascript" src="${ctx}/novoLayout/common/bootstrap/js/bootstrap.min.js"></script>

<%
if(request.getSession(true).getAttribute("permissaoBotao") != null) {
	out.print(request.getSession(true).getAttribute("permissaoBotao").toString());
}
%>

<%@ include file="/pages/ctrlAsterisk/ctrlAsterisk.jsp" %>
