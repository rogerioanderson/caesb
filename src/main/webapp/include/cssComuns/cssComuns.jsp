<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@ include file="/WEB-INF/templates/taglibs.jsp"%>

<link rel="stylesheet" type="text/css" href="${ctx}/css/pesquisa.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/reset.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/fonts.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/fancybox/jquery.fancybox-1.3.4.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/tinyeditor/style.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/slidernav/slidernav.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/syntax_highlighter/styles/shCore.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/syntax_highlighter/styles/shThemeDefault.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/uitotop/css/ui.totop.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/fullcalendar/fullcalendar.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/isotope/isotope.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/elfinder/css/elfinder.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/tiptip/tipTip.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/uniform/css/uniform.aristo.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/multiselect/css/ui.multiselect.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/selectbox/jquery.selectBox.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/colorpicker/css/colorpicker.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/themeroller/Aristo.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/fonts.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/text.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/grid.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/main.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/theme_base.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/buttons.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/ie.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/definicoesParaUsoComum.css" />
<link rel="stylesheet" href="${ctx}/template_new/js/jQueryGantt/css/style.css" />	
<link rel="stylesheet" type="text/css" href="${ctx}/js/themes/icon.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/css/jquery-easy.css" />		
<style type="text/css" >.tabber{display:none;}</style>
