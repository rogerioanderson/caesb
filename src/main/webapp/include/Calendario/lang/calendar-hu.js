/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// ** I18N
Calendar._DN = new Array
("Vas�rnap",
 "H�tf�",
 "Kedd",
 "Szerda",
 "Cs�t�rt�k",
 "P�ntek",
 "Szombat",
 "Vas�rnap");
Calendar._MN = new Array
("janu�r",
 "febru�r",
 "m�rcius",
 "�prilis",
 "m�jus",
 "j�nius",
 "j�lius",
 "augusztus",
 "szeptember",
 "okt�ber",
 "november",
 "december");

// tooltips
Calendar._TT = {};
Calendar._TT["TOGGLE"] = "Toggle first day of week";
Calendar._TT["PREV_YEAR"] = "El�z� �v (hold for menu)";
Calendar._TT["PREV_MONTH"] = "El�z� h�nap (hold for menu)";
Calendar._TT["GO_TODAY"] = "Mai napra ugr�s";
Calendar._TT["NEXT_MONTH"] = "K�v. h�nap (hold for menu)";
Calendar._TT["NEXT_YEAR"] = "K�v. �v (hold for menu)";
Calendar._TT["SEL_DATE"] = "V�lasszon d�tumot";
Calendar._TT["DRAG_TO_MOVE"] = "Drag to move";
Calendar._TT["PART_TODAY"] = " (ma)";
Calendar._TT["MON_FIRST"] = "H�tf� legyen a h�t els� napja";
Calendar._TT["SUN_FIRST"] = "Vas�rnap legyen a h�t els� napja";
Calendar._TT["CLOSE"] = "Bez�r";
Calendar._TT["TODAY"] = "Ma";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "y-mm-dd";
Calendar._TT["TT_DATE_FORMAT"] = "M d, D";

Calendar._TT["WK"] = "h�t";
