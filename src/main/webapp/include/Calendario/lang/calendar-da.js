/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// ** I18N
Calendar._DN = new Array
("S�ndag",
 "Mandag",
 "Tirsdag",
 "Onsdag",
 "Torsdag",
 "Fredag",
 "L�rdag",
 "S�ndag");
Calendar._MN = new Array
("January",
 "Februar",
 "Marts",
 "April",
 "Maj",
 "Juni",
 "Juli",
 "August",
 "September",
 "Oktober",
 "November",
 "December");

// tooltips
Calendar._TT = {};
Calendar._TT["TOGGLE"] = "Skift f�rste ugedag";
Calendar._TT["PREV_YEAR"] = "�t �r tilbage (hold for menu)";
Calendar._TT["PREV_MONTH"] = "�n m�ned tilbage (hold for menu)";
Calendar._TT["GO_TODAY"] = "G� til i dag";
Calendar._TT["NEXT_MONTH"] = "�n m�ned frem (hold for menu)";
Calendar._TT["NEXT_YEAR"] = "�t �r frem (hold for menu)";
Calendar._TT["SEL_DATE"] = "V�lg dag";
Calendar._TT["DRAG_TO_MOVE"] = "Tr�k vinduet";
Calendar._TT["PART_TODAY"] = " (i dag)";
Calendar._TT["MON_FIRST"] = "Vis mandag f�rst";
Calendar._TT["SUN_FIRST"] = "Vis s�ndag f�rst";
Calendar._TT["CLOSE"] = "Luk vinduet";
Calendar._TT["TODAY"] = "I dag";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "y-mm-dd";
Calendar._TT["TT_DATE_FORMAT"] = "D d. M, y";

Calendar._TT["WK"] = "wk";
