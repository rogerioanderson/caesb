/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// ** I18N
Calendar._DN = new Array
("Domingo",
 "Segunda",
 "Ter�a",
 "Quarta",
 "Quinta",
 "Sexta",
 "S�bado",
 "Domingo");
Calendar._MN = new Array
("Janeiro",
 "Fevereiro",
 "Mar�o",
 "Abril",
 "Maio",
 "Junho",
 "Julho",
 "Agosto",
 "Setembro",
 "Outubro",
 "Novembro",
 "Dezembro");

// tooltips
Calendar._TT = {};
Calendar._TT["TOGGLE"] = "Altera primeiro dia da semana";
Calendar._TT["PREV_YEAR"] = "Ano anterior(hold for menu)";
Calendar._TT["PREV_MONTH"] = "M�s anterior (hold for menu)";
Calendar._TT["GO_TODAY"] = "Hoje";
Calendar._TT["NEXT_MONTH"] = "M�s seguinte (hold for menu)";
Calendar._TT["NEXT_YEAR"] = "ano seguinte (hold for menu)";
Calendar._TT["SEL_DATE"] = "Seleciona uma data";
Calendar._TT["DRAG_TO_MOVE"] = "Arrasta calend�rio";
Calendar._TT["PART_TODAY"] = " (hoje)";
Calendar._TT["MON_FIRST"] = "Inicia na segunda-feira";
Calendar._TT["SUN_FIRST"] = "Inicia no domingo";
Calendar._TT["CLOSE"] = "Fechar";
Calendar._TT["TODAY"] = "Hoje";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "dd-mm-y";
Calendar._TT["TT_DATE_FORMAT"] = "DD, dd de MM de y";

Calendar._TT["WK"] = "sem";
