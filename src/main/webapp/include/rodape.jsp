<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<script type="text/javascript">
	// inicio ajuste altura
	var ini = new Date().getTime();
	if(!document.all){
		var divs = document.getElementsByTagName('div');
		for(var i = 0; i < divs.length; i++){
			if(divs[i].id.indexOf('menu') >= 0){
				var child = null;
				for(var n = 0; n < divs[i].childNodes.length; n++){
					if(divs[i].childNodes[n].nodeName == 'TABLE'){
						child = divs[i].childNodes[n];
						break;
					}
				}
				if(child){
					var h = child.rows[0].cells[0].offsetHeight;
					//if(divs[i].offsetHeight < h)
						divs[i].style.height = h + 9;
				}
			}
		}
	}
	//alert(new Date().getTime() - ini);
	// fim ajuste altura

	if(document.all){
		var inputs = document.getElementsByTagName('input');
		for(var i = 0; i < inputs.length; i++){
			if(inputs[i].type.toLowerCase() == 'button'){
				if(inputs[i].className == ''){
					inputs[i].className = 'btBB';
				}
			}
		}
	}

</script>
