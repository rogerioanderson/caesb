/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
﻿////////////////////////////////////////////////////
// controlWindow object
////////////////////////////////////////////////////
function controlWindow( controlForm ) {
	// private properties
	this._form = controlForm;

	// public properties
	this.windowType = "controlWindow";
//	this.noSuggestionSelection = "- No suggestions -";	// by FredCK
	this.noSuggestionSelection = FCKLang.DlgSpellNoSuggestions ;
	// set up the properties for elements of the given control form
	this.suggestionList  = this._form.sugg;
	this.evaluatedText   = this._form.misword;
	this.replacementText = this._form.txtsugg;
	this.undoButton      = this._form.btnUndo;

	// public methods
	this.addSuggestion = addSuggestion;
	this.clearSuggestions = clearSuggestions;
	this.selectDefaultSuggestion = selectDefaultSuggestion;
	this.resetForm = resetForm;
	this.setSuggestedText = setSuggestedText;
	this.enableUndo = enableUndo;
	this.disableUndo = disableUndo;
}

function resetForm() {
	if( this._form ) {
		this._form.reset();
	}
}

function setSuggestedText() {
	var slct = this.suggestionList;
	var txt = this.replacementText;
	var str = "";
	if( (slct.options[0].text) && slct.options[0].text != this.noSuggestionSelection ) {
		str = slct.options[slct.selectedIndex].text;
	}
	txt.value = str;
}

function selectDefaultSuggestion() {
	var slct = this.suggestionList;
	var txt = this.replacementText;
	if( slct.options.length == 0 ) {
		this.addSuggestion( this.noSuggestionSelection );
	} else {
		slct.options[0].selected = true;
	}
	this.setSuggestedText();
}

function addSuggestion( sugg_text ) {
	var slct = this.suggestionList;
	if( sugg_text ) {
		var i = slct.options.length;
		var newOption = new Option( sugg_text, 'sugg_text'+i );
		slct.options[i] = newOption;
	 }
}

function clearSuggestions() {
	var slct = this.suggestionList;
	for( var j = slct.length - 1; j > -1; j-- ) {
		if( slct.options[j] ) {
			slct.options[j] = null;
		}
	}
}

function enableUndo() {
	if( this.undoButton ) {
		if( this.undoButton.disabled == true ) {
			this.undoButton.disabled = false;
		}
	}
}

function disableUndo() {
	if( this.undoButton ) {
		if( this.undoButton.disabled == false ) {
			this.undoButton.disabled = true;
		}
	}
}
