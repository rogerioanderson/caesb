/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// wizard

	$('.wizard_steps ul li:not(".current") a').live('click',function(){
	
	
		$('.wizard_steps ul li').removeClass('current');
		$(this).parent('li').addClass('current');
		
		var step = $(this).attr('href');
		var step_num = $(this).attr('href').replace('#step_','');
		var step_multiplyby = (100 / $(".wizard_steps > ul > li").size());
		var prog_val = (step_num*step_multiplyby);
		
		$( ".wizard_progressbar").progressbar({ value: prog_val });
		
		$('.wizard_content').children().hide();
		$('.wizard_content').children(step).fadeIn(1000);
		
		return false;
	});
	
	var initialProg = (100 / $(".wizard_steps > ul > li").size());
	$( ".wizard_progressbar").progressbar({ value : initialProg});
	
	
	$('.button_bar button.move').live('click', function(){
		
		var goTo = $(this).attr("data-goto").replace('step_','');;
	
		$('.wizard_steps ul li:nth-child('+goTo+') a').trigger('click');

	});
