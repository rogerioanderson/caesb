/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * This is a CommonJS compatibility test. You can run this file with node.
 */
require.paths.unshift(__dirname + '/../scripts');

var sys = require('sys'),
	shSyntaxHighlighter = require('shCore').SyntaxHighlighter,
	code = 'test',
	brushes = [
		'AS3',
		'AppleScript',
		'Bash',
		'CSharp',
		'ColdFusion',
		'Cpp',
		'Css',
		'Delphi',
		'Diff',
		'Erlang',
		'Groovy',
		'JScript',
		'Java',
		'JavaFX',
		'Perl',
		'Php',
		'Plain',
		'PowerShell',
		'Python',
		'Ruby',
		'Sass',
		'Scala',
		'Sql',
		'Vb',
		'Xml'
	]
	;

brushes.sort();

for (var i = 0; i < brushes.length; i++)
{
	var name = brushes[i],
		brush = require('shBrush' + name).Brush
		;
		
	brush = new brush();
	brush.init({ toolbar: false });
	
	var result = brush.getHtml(code);
	
	sys.puts(name + (result != null ? ': ok' : ': NOT OK'));
}
