/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/

	$(window).load( function() {
		
		document.form.afterRestore = function() {
			$('.tabs').tabs('select', 0);
		}
		
		
	});


	function tree(id) {

		$(id).treeview();

	}

	function excluir() {
		if (document.getElementById("idPerfilAcesso").value != "") {
			if (confirm(i18n_message("citcorpore.comum.deleta"))){
				document.form.acessoMenuSerializados.value = adicionaMenus();
				document.form.fireEvent("delete");
			}
		}
	}

	function acessarSistemaCitsmart(){
		if (confirm(i18n_message("perfilAcesso.desablitarAcessoSistema"))){

			desmarcarCheckbox();

			if ($("#idPerfilAcesso").val() != null && $("#idPerfilAcesso").val() != ""){
				document.form.fireEvent("verificarGruposPerfilAcesso");
			}
		}

	}

	function desmarcarCheckbox(){

		$("#corpoInf").find("input[type='checkbox']").each(function(i, el){
			$(el).attr('checked',false)
		});

	}

	function LOOKUP_PERFILACESSO_select(id, desc) {
		JANELA_AGUARDE_MENU.show();
		document.form.restore({idPerfilAcesso : id});
	}

	function marcarTodosCheckbox(id) {

		var classe = $(id).attr("class");
		var x = classe.split(" ");
		if(x[1] != null){
			classe = x[x.length - 2];
		}

		var valor = "";
		if(!$(id).is(':checked')){

			$("." + classe).each(function() {
				$(this).attr("checked", false);
			});
		}else{
			$("." + classe).each(function() {
					$(this).attr("checked", true);
			});
		}
	}

		function checkboxPesquisar(id) {
			var idPesquisa = "pesq_" + id;
			$("#" + idPesquisa).attr("checked", true);
		}

		function checkboxIncluir(id) {
			var idInclui = "inc_" + id;
			$("#" + idInclui).attr("checked", true);
		}

		function checkboxGravar(id) {
			var idGravar = "gravar_" + id;
			$("#" + idGravar).attr("checked", true);
		}

		function checkboxDeletar(id) {
			var idDeleta = "del_" + id;
			$("#" + idDeleta).attr("checked", true);
		}

		adicionaMenus = function() {
			var id = "";
			var check = "";
			var lista = "";
			var tipo = "";
			var i = 0;
			var x = 0;
			var array = new Array();
			$("input[name='menu']").each(function() {
				id = $(this).val();
				if ($(this).is(':checked')) {
					tipo = "S";
				} else {
					tipo = "N";
				}
				check += tipo + "-";
				if (i == 2) {
					lista += id + "@" + check + ";";
					i = -1;
					check = "";
				}
				i++;
				x++;
			});
			return lista;
		}

		function gravar() {

			if (document.getElementById('acessoSistemaCitsmartN').checked){
				desmarcarCheckbox();
			}
			document.form.acessoMenuSerializados.value = adicionaMenus();
			document.form.save();
		}
