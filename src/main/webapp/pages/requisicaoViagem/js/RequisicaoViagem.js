/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
var idResponsavel;
var responsavel;
var idEmpregadoAux;
var nomeImpregadoAux;
var indexAux;

addEvent(window, "load", load, false);
function load() {
	
	document.form.afterLoad = function() {
		parent.escondeJanelaAguarde();
	}
	
}

//Masks
$('#horaInicio').mask('99:99');
$('#horaFim').mask('99:99');
$("#cpf").mask("999.999.999-99");

function decodificaTextarea() {
	var infoNaoFuncionario = document.getElementById("infoNaoFuncionario").value;
	var infoNaoFuncionarioAux = document.getElementById("infoNaoFuncionarioAux").value;
	
	document.getElementById("infoNaoFuncionario").value = ObjectUtils.decodificaEnter(infoNaoFuncionario);
	document.getElementById("infoNaoFuncionarioAux").value = ObjectUtils.decodificaEnter(infoNaoFuncionarioAux);
}

function tooltip() {
	$(document).ready(function() {
		$('.titulo').tooltip();
	});
}

function gerarButtonEditDelete(row, obj){
	
	row.cells[3].innerHTML += '<a href="javascript:;" class="btn-action glyphicons icon-eye-open" onclick="abrirModalIntegrante(this.parentNode.parentNode.rowIndex)" title="'+i18n_message("citcorpore.comum.visualizacaoAvancada")+'" style="background-position: -91px -114px;"><i></i></a>  ';
	row.cells[3].innerHTML += '<a href="javascript:;" class="btn-action btn-success glyphicons edit" onclick="editarLinhaTabela(this.parentNode.parentNode.rowIndex)"><i></i></a>  ';
	row.cells[3].innerHTML += '<a href="javascript:;" class="btn-action glyphicons remove_2 btn-danger titulo" onclick="removerLinhaTabela(this.parentNode.parentNode.rowIndex)"><i></i></a>';

	if(obj.integranteFuncionario == 'N') {
		var html = $('#' + obj.idControleCITFramework + ' td:eq(0)').html();
		html = '<div>' + html + '</div>'
		$('#' + obj.idControleCITFramework + ' td:eq(0)').empty().append(html);
		
		var tooltipText = "<strong>" + i18n_message("dinamicview.informacoescomplementares") + "</strong><br />" + (obj.infoNaoFuncionario).replace(/\n/g, "<br />");
		$('#' + obj.idControleCITFramework + ' td:eq(0) div').tooltip({
			title: tooltipText,
			html: true
		});
	}
}

function deleteLinha(table, index) {
	HTMLUtils.deleteRow(table, index);
}

function getObjetoSerializado() {
	var obj = new CIT_RequisicaoViagemDTO();
	HTMLUtils.setValuesObject(document.form, obj);
	var itegranteViagem = HTMLUtils.getObjectsByTableId('tblIntegranteViagem');
	obj.integranteViagemSerialize = ObjectUtils.serializeObjects(itegranteViagem);
	return ObjectUtils.serializeObject(obj);
}

//function calcularQuantidadeDias(obj) {
//	
//	if(document.getElementById("dataInicioViagem").value != ""){
//		fctValidaData(document.getElementById("dataInicioViagem"));
//	}
//	if(document.getElementById("dataFimViagem").value != ""){
//		fctValidaData(document.getElementById("dataFimViagem"));
//	}
//	
//	
//	var dataInicio = document.getElementById("dataInicioViagem").value;
//	var dataFim = document.getElementById("dataFimViagem").value;
//
//	var dtInicio = new Date();
//	var dtFim = new Date();
//	
//	if(dataInicio != "" && dataFim == ""){
//		document.form.qtdeDias.value = 1;
//	}
//
//	if (dataInicio != "" & dataFim != "") {
//		
//		if (validaData(dataInicio, dataFim, obj)) {
//			dtInicio.setTime(Date.parse(dataInicio.split("/").reverse().join("/"))).setFullYear;
//			dtFim.setTime(Date.parse(dataFim.split("/").reverse().join("/"))).setFullYear;
//			var dias = DateTimeUtil.diferencaEmDias(dtInicio, dtFim);
//
//			document.form.qtdeDias.value = dias + 1;
//		}
//		
//	}
//}

function fctValidaData(obj){
	
	if(obj == null){
		return false;
	}
	
    var data = obj.value;
    var dia = data.substring(0,2);
    var mes = data.substring(3,5);
    var ano = data.substring(6,10);
 
    //Criando um objeto Date usando os valores ano, mes e dia.
    var novaData = new Date(ano,(mes-1),dia);
 
    var mesmoDia = parseInt(dia,10) == parseInt(novaData.getDate());
    var mesmoMes = parseInt(mes,10) == parseInt(novaData.getMonth())+1;
    var mesmoAno = parseInt(ano) == parseInt(novaData.getFullYear());
 
    if (!((mesmoDia) && (mesmoMes) && (mesmoAno))){
        alert(i18n_message("si.comum.dataDe")+" "+obj.descricao+" "+i18n_message("si.comum.eInvalida"));  
        obj.value = "";
        obj.focus();   
        return false;
    } 
    return true;
}

function utilValidaData(value, id){
	
	if(!ValidacaoUtils.validaData(value, '')){
		document.getElementById(id).value = '';
		id.focus();
	}
}

/**
 * @author rodrigo.oliveira
 */
function validaData(dataInicio, dataFim, obj) {
	var dtInicio = new Date();
	var dtFim = new Date();

	dtInicio.setTime(Date.parse(dataInicio.split("/").reverse().join("/"))).setFullYear;
	dtFim.setTime(Date.parse(dataFim.split("/").reverse().join("/"))).setFullYear;

	if (dtInicio > dtFim) {
		alert(i18n_message("solicitacaoservico.validacao.datainiciomenorfinal"));
		$("#"+obj.id+"").val("");
		
		return false;
	} else
		return true;
}

function atribuiResponsavel(index, idEmp, nomeEmp) {
	idEmpregadoAux = idEmp;
	nomeImpregadoAux = nomeEmp;
	indexAux = index;

	$('#idIntegranteAux').val(idEmpregadoAux);

	document.form.fireEvent("validaAtribuicao");
}

function removerLinhaTabela(indice) {
	controlarDelecaoLinhaIntegranteViagem(indice);
	HTMLUtils.deleteRow('tblIntegranteViagem', indice);
	$(".tooltip").hide();
	limpaCamposIntegrante();
}
//FIXME controle de exclusão
controlarDelecaoLinhaIntegranteViagem = function(index) {
	var lista = HTMLUtils.getObjectByTableIndex('tblIntegranteViagem', index);
	if (lista != null && lista != '') {
		document.getElementById('colItens_Delecao_IntegranteViagem').value += lista.idIntegranteViagem + ",";
	}
};

$("#POPUP_DADOSINTEGRANTE").dialog({
	autoOpen : false,
	width : "500px",
	modal : true
});

function abrirModalIntegrante(rowIndex){
	obj = HTMLUtils.getObjectByTableIndex('tblIntegranteViagem', rowIndex);
	
	var dados = '<p><b>Integrante:</b> '+obj.nome+'</p>'+
				'<p><b>'+i18n_message("candidato.funcionario")+':</b> '+obj.integranteFuncionario+'</p>'+
				'<p><b>'+i18n_message("citcorpore.comum.responsavel")+':</b> '+obj.respPrestacaoContas+'</p>'+
				
				"<div class='widget'>"+
					"<div class=widget-head>"+
						"<h2 class='heading' id='titulo'>"+i18n_message("requisicaoViagem.itinerario")+"</h2>"+
					"</div><!-- .widget-head -->"+
						
					"<div class='widget-body'>"+
					
						'<p><b>Origem:</b> '+obj.nomeOrigem+'</p>'+
						'<p><b>Aeroporto origem:</b> '+obj.aeroportoOrigem+'</p>'+
						'<p><b>Destino:</b> '+obj.nomeDestino+'</p>'+
						'<p><b>Aeroporto destino:</b> '+obj.aeroportoDestino+'</p>'+
						'<p><b>Data da ida:</b> '+obj.dataInicio+'</p>'+
						'<p><b>Hora da ida:</b> '+obj.horaInicio+'</p>'+
						'<p><b>Data da volta:</b> '+obj.volta+'</p>'+
						'<p><b>Hora da volta:</b> '+obj.horaFim+'</p>'+
						'<p><b>'+i18n_message("requisicaoViagem.hoteisPreferencia")+':</b> '+obj.hoteisPreferenciais+'</p>'+
					
					"</div><!-- .widget-body -->"+
				"</div><!-- .widget -->"+
				
				"<div class='widget'>"+
					"<div class=widget-head>"+
						"<h2 class='heading' id='titulo'>"+i18n_message("requisicaoViagem.dadosBancarios")+"</h2>"+
					"</div><!-- .widget-head -->"+
							
					"<div class='widget-body'>"+
					
						'<p><b>Banco:</b> '+obj.banco+'</p>'+
						'<p><b>'+i18n_message("colaborador.agencia")+':</b> '+obj.agencia+'</p>'+
						'<p><b>Conta:</b> '+obj.conta+'</p>'+
						'<p><b>'+i18n_message("logs.operacao")+':</b> '+obj.operacao+'</p>'+
						'<p><b>CPF:</b> '+obj.cpf+'</p>'+
						
					"</div><!-- .widget-body -->"+
				"</div><!-- .widget -->";
	
	$('#dadosIntegrante').empty().append(dados);
	
	$("#POPUP_DADOSINTEGRANTE").dialog("open");
}

function editarLinhaTabela(rowIndex) {
	
	obj = HTMLUtils.getObjectByTableIndex('tblIntegranteViagem', rowIndex);	
	
	if (obj.integranteFuncionario == 'S'){
		limpaCamposIntegrante();
		$('#integranteFuncionario').click();
		$('#idEmpregado').val(obj.idEmpregado);
		$('#idRespPrestacaoContas').val(obj.idRespPrestacaoContas);
		$('#nomeEmpregado').val(obj.nome);
		$('#respPrestacaoContas').val(obj.respPrestacaoContas);
		$('#divNaoFuncionario').hide();
		
		adicionarOuRemoverObrigatoriedadeDosCampos('S');
		
		document.form.fireEvent("validaAtribuicao");
		
	}else{
		
		limpaCamposIntegrante();
		$('#integranteNaoFuncionario').attr('checked', 'checked');
		$('#divEmpregado').hide();
		$('#divNaoFuncionario').show();
		$('#divResponsavelEmpregado').show();
		$('#divInfoNaoFuncionario').show();
		
		$('#idEmpregado').val(obj.idEmpregado);
		$('#idRespPrestacaoContas').val(obj.idRespPrestacaoContas);
		$('#nomeNaoFuncionario').val(obj.nomeNaoFuncionario);
		$('#respPrestacaoContas').val(obj.respPrestacaoContas);
		$('#infoNaoFuncionario').val(obj.infoNaoFuncionario);
		
		adicionarOuRemoverObrigatoriedadeDosCampos('N');
	}
	
	$('#idCidadeOrigem').val(obj.origem);
	$('#idCidadeDestino').val(obj.destino);
	$('#nomeCidadeEUfOrigem').val(obj.nomeOrigem);
	$('#nomeCidadeEUfDestino').val(obj.nomeDestino);
	$('#aeroportoDestino').val(obj.aeroportoDestino);
	$('#aeroportoOrigem').val(obj.aeroportoOrigem);
	$('#dataInicioViagem').val(obj.dataInicio);
	$('#dataFimViagem').val(obj.volta);
	$('#horaInicio').val(obj.horaInicio);
	$('#horaFim').val(obj.horaFim);
	$('#hoteisPreferenciais').val(obj.hoteisPreferenciais);
	$('#banco').val(obj.banco);
	$('#agencia').val(obj.agencia);
	$('#conta').val(obj.conta);
	$('#operacao').val(obj.operacao);
	$('#cpf').val(obj.cpf);
	
	document.getElementById("rowIndexIntegrante").value = rowIndex;
}

function startLoading() {
	
	document.getElementById('tdResultadoSLAPrevisto').style.display = 'none';
	document.getElementById('divMini_loading').style.display = 'block';
		
}


/** Autocompletes * */
var completeCidadeOrigem;
var completeCidadeDestino;
var completeEmpregado;
var completeNaoEmpregado;
var completeResponsavelEmpregado;


/** Inicializa��o da tela * */
$(document).ready(function() {
	
	completeCidadeOrigem = $('#nomeCidadeEUfOrigem').autocomplete({ 
		serviceUrl:'pages/autoCompleteCidade/autoCompleteCidade.load',
		noCache: true,
		onSelect: function(value, data){
			$('#idCidadeOrigem').val(data);
			$('#nomeCidadeEUfOrigem').val(value);
		}
	});
	
	completeCidadeDestino = $('#nomeCidadeEUfDestino').autocomplete({ 
		serviceUrl:'pages/autoCompleteCidade/autoCompleteCidade.load',
		noCache: true,
		onSelect: function(value, data){
			$('#idCidadeDestino').val(data);
			$('#nomeCidadeEUfDestino').val(value);
		}
	});
	
	completeEmpregado = $('#nomeEmpregado').autocomplete({ 
		serviceUrl:'pages/autoCompleteFuncionario/autoCompleteFuncionario.load',
		noCache: true,
		onSelect: function(value, data){
			$('#idEmpregado').val(data);
			$('#nomeEmpregado').val(value);
			document.form.fireEvent("validaAtribuicao");
			document.form.fireEvent("setaDadosBancarios");
		}
	});
	
	completeResponsavelEmpregado = $('#respPrestacaoContas').autocomplete({ 
		serviceUrl:'pages/autoCompleteFuncionario/autoCompleteFuncionario.load',
		noCache: true,
		onSelect: function(value, data){
			$('#idRespPrestacaoContas').val(data);
			$('#respPrestacaoContas').val(value);
		}
	});
	
	completeNaoEmpregado = $('#nomeNaoFuncionario').autocomplete({ 
		serviceUrl:'pages/autoCompleteNaoEmpregado/autoCompleteNaoEmpregado.load',
		noCache: true,
		onSelect: function(value, data){
				$('#idEmpregado').val(data);
				$('#nomeNaoFuncionario').val(value);
				$('#nomeNaoFuncionarioAux').val(value);
				document.form.fireEvent("restoreInfNaoFuncionario");
		}
	});

	
	// Esconde o campo de responsavel
	$('#divResponsavelEmpregado').hide();
	$('#divInfoNaoFuncionario').hide();
	
// $('#divEmpregadoOuNao').hide('slow');
	
	$('#divNaoFuncionario').hide();
	
	$('#integranteFuncionario').change(function() {

		//Esconder campos para o nao funcionario e exibir do funcionario
		$('#divEmpregado').show();
		$('#divNaoFuncionario').hide();
		$('#divResponsavelEmpregado').hide();
		$('#divInfoNaoFuncionario').hide();
		
		adicionarOuRemoverObrigatoriedadeDosCampos('S');		

	});
	
	$('#integranteNaoFuncionario').change(function() {
		
		//Esconder campos para o funcionario e exibir os do nao funcionario
		$('#divEmpregado').hide();
		$('#divNaoFuncionario').show();
		$('#divResponsavelEmpregado').show();
		$('#divInfoNaoFuncionario').show();
		
		adicionarOuRemoverObrigatoriedadeDosCampos('N');
		
	});
	
});


function adicionarOuRemoverObrigatoriedadeDosCampos(integranteFuncionario){

	if(integranteFuncionario == 'S'){
	
		if(!$('#LblBanco').hasClass("campoObrigatorio")){
			
			$('#LblBanco').addClass("campoObrigatorio");
			$('#LblAgencia').addClass("campoObrigatorio");
			$('#LblConta').addClass("campoObrigatorio");
			$('#LblCPF').addClass("campoObrigatorio");
			
		}
	} else {
		
		$('#LblBanco').removeClass("campoObrigatorio");
		$('#LblAgencia').removeClass("campoObrigatorio");
		$('#LblConta').removeClass("campoObrigatorio");
		$('#LblCPF').removeClass("campoObrigatorio");
		
	}
	
}

function adicionarEmpregado() {
	
	if(validaData()){
		var obj = new CIT_IntegranteViagemDTO();
		
		obj.idEmpregado = $("#idEmpregado").val();
		obj.nome = $('#nomeEmpregado').val();
		
		var rowIndex = document.getElementById("rowIndexIntegrante").value;
		
		var eFunc = document.form.integranteFuncionario[0].checked;
		
		if(eFunc == true){
			
			var idResp = $('#idRespPrestacaoContas').val();
			if(idResp){
				obj.idRespPrestacaoContas = idResp;
				obj.respPrestacaoContas = $('#respPrestacaoContas').val();
			}else{
				obj.respPrestacaoContas = $('#nomeEmpregado').val();
			}
			
			if((obj.idEmpregado == null || obj.nome == null) || (obj.idEmpregado == "" || obj.nome == "")){
				alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("categoriaProblema.nome")+"] "+i18n_message("rh.alertEObrigatorio")+"!");			
				return;
			}
			if((obj.remarcacao == null) || (obj.remarcacao == "" )){
				obj.remarcacao = "N";
			}
			obj.integranteFuncionario = 'S';
		} else {
			var naoFunc = $('#nomeNaoFuncionario').val();
			var idNaoFunc = $('#idEmpregado').val();
			
			if(idNaoFunc == null || idNaoFunc == "" && naoFunc != null && naoFunc != ""){
				document.form.fireEvent("createNaoFuncionario");
				return;
			}
			
			if(($('#nomeNaoFuncionario').val() != $('#nomeNaoFuncionarioAux').val()) || ($('#infoNaoFuncionario').val() != $('#infoNaoFuncionarioAux').val())){
				document.form.fireEvent("createNaoFuncionario");
				return;
			}
			
			if(naoFunc != null && naoFunc != "" && idNaoFunc != null && idNaoFunc != ""){
				obj.nome = naoFunc;
				obj.nomeNaoFuncionario = naoFunc;
				obj.idEmpregado = idNaoFunc;
			} else {
				alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("categoriaProblema.nome")+"] "+i18n_message("rh.alertEObrigatorio")+"!");		
				return;
			}
			
			var idResp = $('#idRespPrestacaoContas').val();
			if(idResp){
				obj.idRespPrestacaoContas = idResp;
				obj.respPrestacaoContas = $('#respPrestacaoContas').val();
			} else {
				alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("requisicaoViagem.responsavelPrestacaoDeContas")+"] "+i18n_message("rh.alertEObrigatorio")+"!");		
				return;
			}
			
			obj.infoNaoFuncionario = $("#infoNaoFuncionario").val();
			obj.integranteFuncionario = 'N';
		}
		
		if($('#idCidadeOrigem').val() === null || $('#idCidadeOrigem').val() === ""){
			alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("citcorpore.comum.origem")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
			return;
		}
		
		if($('#idCidadeDestino').val() === null || $('#idCidadeDestino').val() === ""){
			alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("importmanager.destino")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
			return;
		}
		
		if($('#dataInicioViagem').val() === null || $('#dataInicioViagem').val() === ""){
			alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("requisicaoViagem.dataIda")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
			return;
		}
		
		if($('#dataFimViagem').val() === null || $('#dataFimViagem').val() === ""){
			alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("requisicaoViagem.dataVolta")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
			return;
		}
		
		if($('#banco').val() === null || trim($('#banco').val()) === "" && eFunc){
			alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("externalconnection.banco")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
			return;
		}
		
		if($('#agencia').val() === null || trim($('#agencia').val()) === "" && eFunc){
			alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("colaborador.agencia")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
			return;
		}
		
		if($('#conta').val() === null || trim($('#conta').val()) === "" && eFunc){
			alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("colaborador.conta")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
			return;
		}
		
		if($('#cpf').val() === null || trim($('#cpf').val()) === "" && eFunc){
			alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("lookup.cpf")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
			return;
		}
		
		obj.origem = $('#idCidadeOrigem').val();
		obj.destino = $('#idCidadeDestino').val();
		obj.nomeOrigem = $('#nomeCidadeEUfOrigem').val();
		obj.nomeDestino = $('#nomeCidadeEUfDestino').val();
		obj.aeroportoDestino = $('#aeroportoDestino').val();
		obj.aeroportoOrigem = $('#aeroportoOrigem').val();
		obj.dataInicio = $('#dataInicioViagem').val();
		obj.volta = $('#dataFimViagem').val();
		obj.horaInicio = $('#horaInicio').val();
		obj.horaFim = $('#horaFim').val();
		obj.hoteisPreferenciais = $('#hoteisPreferenciais').val();
		obj.banco = $('#banco').val();
		obj.agencia = $('#agencia').val();
		obj.conta = $('#conta').val();
		obj.operacao = $('#operacao').val();
		obj.cpf = $('#cpf').val();
		
		if(StringUtils.isBlank(StringUtils.trim(rowIndex))) {
			if(eFunc == true) {
				HTMLUtils.addRow('tblIntegranteViagem', null, '', obj, [ 'nome', 'integranteFuncionario', 'respPrestacaoContas', '' ], ['idEmpregado'], i18n_message("citcorpore.comum.registroJaAdicionado"), [ gerarButtonEditDelete ], null, null, false);
			} else {
				HTMLUtils.addRow('tblIntegranteViagem', null, '', obj, [ 'nome', 'integranteFuncionario', 'respPrestacaoContas', '' ], ['nome'], i18n_message("citcorpore.comum.registroJaAdicionado"), [ gerarButtonEditDelete ], null, null, false);
			}
		} else {
			var integranteViagem = HTMLUtils.getObjectByTableIndex('tblIntegranteViagem', rowIndex);
			
			if(obj.idEmpregado == integranteViagem.idEmpregado) {
				HTMLUtils.updateRow('tblIntegranteViagem', null, '', obj, [ 'nome', 'integranteFuncionario', 'respPrestacaoContas', '' ], null, '', [ gerarButtonEditDelete ], null, null, rowIndex, false);
			} else {
				HTMLUtils.updateRow('tblIntegranteViagem', null, '', obj, [ 'nome', 'integranteFuncionario', 'respPrestacaoContas', '' ], ['idEmpregado'], i18n_message("citcorpore.comum.registroJaAdicionado"), [ gerarButtonEditDelete ], null, null, rowIndex, false);
			}
		}
		
		if(eFunc)
			$('#divResponsavelEmpregado').hide();
		
		limpaCamposIntegrante();
		
	}
}

function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

/*
 * Oculta a coluna de responsaveis por prestaca de contas na
 * tblIntegrantesViagem
 */
function ocultaColunaResp(obj){
	if(obj == true){
		$('td:nth-child(2),th:nth-child(2)').hide();
	} else {
		$('td:nth-child(2),th:nth-child(2)').show();
	}
}

function limpaCamposIntegrante(){
	$('#nomeCidadeEUfOrigem').val("");
	$('#nomeCidadeEUfDestino').val("");
	$('#idCidadeOrigem').val("");
	$('#idCidadeDestino').val("");
	$('#aeroportoOrigem').val("");
	$('#aeroportoDestino').val("");
	$('#dataInicioViagem').val("");
	$('#dataFimViagem').val("");
	$('#horaInicio').val("");
	$('#horaFim').val("");
	$('#hoteisPreferenciais').val("");
	$('#nomeCidade').val("");
	
	$('#banco').val("");
	$('#agencia').val("");
	$('#conta').val("");
	$('#operacao').val("");
	$('#cpf').val("");
	
	$('#idEmpregado').val("");
	$('#nomeEmpregado').val("");
	$('#idRespPrestacaoContas').val("");
	$('#respPrestacaoContas').val("");
	$('#nomeNaoFuncionario').val("");
	$('#infoNaoFuncionario').val("");
	$('#nomeNaoFuncionarioAux').val("");
	$('#infoNaoFuncionarioAux').val("");
	document.getElementById("rowIndexIntegrante").value = "";
	
	$('#integranteFuncionario').click();
}

function validaCampos(){
	if($('#nomeNaoFuncionario').val() == null || $('#nomeNaoFuncionario').val() == ""){
		limpaCamposIntegrante();
	}
}

function validaCpf(cpf) {
	
	if(StringUtils.isBlank(cpf) || cpf === "___.___.___-__"){
	     return true;
	}
	     
	cpf = cpf.replace(".","");
	cpf = cpf.replace(".","");
	cpf = cpf.replace("-","");                 
	var erro = new String;
	if (cpf.length < 11) erro += i18n_message("rh.saoNecessariosDigitosVerificacaoCPF")+"\n\n"; 
	var nonNumbers = /\D/;
	if (nonNumbers.test(cpf)) erro += i18n_message("rh.verificacaoCPFSuportaApenasNumeros")+" \n\n"; 
	if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999"){
		erro += i18n_message('citcorpore.validacao.numeroCPFInvalido');
		$('#cpf').val("");
	}
	var a = [];
	var b = new Number;
	var c = 11;
	for (i=0; i<11; i++){
		a[i] = cpf.charAt(i);
	    if (i < 9) b += (a[i] * --c);
	}
	if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
	b = 0;
	c = 11;
	for (y=0; y<10; y++) b += (a[y] * c--); 
	if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
	if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])){
		erro +=i18n_message("citcorpore.validacao.numeroCPFInvalido");
		$('#cpf').val("");
	}
	if (erro.length > 0){
		alert(erro);
		field.focus();
		return false;
	}
	return true;
};

function validaData(){
	
	var dataAtual = new Date();
	dataAtual.setHours(00);
	dataAtual.setMinutes(00);
	dataAtual.setSeconds(00);
	dataAtual.setMilliseconds(00);
	
	var d1 = DateTimeUtil.converteData($('#dataInicioViagem').val());
	var d2 = DateTimeUtil.converteData($('#dataFimViagem').val());
	var h1 = $('#horaInicio').val();
	var h2 = $('#horaFim').val();
	
	if(d1 == null || d2 == null){
		return true;
	}
	
	if(d1.getTime() < dataAtual.getTime()){
		alert(i18n_message("requisicaoViagem.validacaoDataInicio"));
		return false;
	}
	
	if(d1.getTime() > d2.getTime()){
		
		$('#dataInicioViagem').val("");
		$('#dataFimViagem').val("");
		alert(i18n_message("requisicaoViagem.validacaoDataInicioFim"));
		return false;
		
	}else if(d1.getTime() == d2.getTime()){
		
		if(h1 == null || h2 == null){
			return true;
		}else if(DateTimeUtil.comparaHora(h1, h2)){
			return true;
		}else{
			$('#horaInicio').val("");
			$('#horaFim').val("");
			alert(i18n_message("requisicaoViagem.validacaoHoraInicioFim"));
			
			return false;
		}
		
		return false;
	}else{
		return true;
	}
}

function atualizarListaIntegrantesNovo(){
	
	var objsCriterios = HTMLUtils.getObjectsByTableId('tblIntegranteViagem');
	document.form.integranteViagemSerialize.value = ObjectUtils.serializeObjects(objsCriterios);
	
	if(document.form.integranteViagemSerialize == null || document.form.integranteViagemSerialize.value == null || document.form.integranteViagemSerialize.value == ""){
		alert(i18n_message("requisicaoViagem.integranteViagemCampoObrigatorio"));
		return false;
	}
	
	document.form.fireEvent("atualizarNovosIntegrantesViagem");
}

function esconderDadosGerais(){
	document.getElementById('idPaiDadosGerais').style.display = 'none'; 
	return true;
}

function fecharFrame(){
	document.form.fireEvent("fecharAlteracaoIntegranteViagem");
	document.form.clear();
	parent.fecharFrameAddIntegrantes();
}
