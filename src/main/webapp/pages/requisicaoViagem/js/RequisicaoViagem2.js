/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * FUNCTIONS
 */
/* Criação Viagem */
function habilitaInfoNaoFuncionario() {
	if($("#nao_funcionario:checked").length) {
		$("#info-complementares-nao-funcionario-container").show();
	} else {
		$("#info-complementares-nao-funcionario-container").hide();
	}
}

function addIntegrante() {
	if(StringUtils.trim($("#nome_funcionario").val()) == "") {
		alert("Informe o nome do funcionário!");
		return;
	}
	if($("#nao_funcionario:checked").length) {
		if(StringUtils.trim($("#nome_responsavel_prest_contas").val()) == "") {
			alert("Informe o nome do responsável pela prestação de contas!");
			return;
		} 
		if(StringUtils.trim($("#info_complementares_nao_funcionario").val()) == "") {
			alert("Informe as informações complementares do não funcionário!");
			return;
		}
	}
}

/* Execução Viagem */
function abrirPopup(){
	$("#POPUP_ITEMCONTROLEFINANCEIRO").dialog("open");
} 

function fecharFrameItemControleFinanceiro(){
	limparTabelaDeItensCadastradosDaPopup('tblItemControleFinaceiro');
	$("#POPUP_ITEMCONTROLEFINANCEIRO").dialog("close");
}

/* Autorização Viagem */
function habilitaJustificativaNaoAutorizacao() {
	if($("#nao_autorizado:checked").length) {
		$("#autorizacao-justificativa-container").show();
	} else {
		$("#autorizacao-justificativa-container").hide();
	}
}

/**
 * LOAD
 */
$(window).load(function() {
	/* Criação Viagem */
	habilitaInfoNaoFuncionario();
	
	/* Autorização Viagem */
	habilitaJustificativaNaoAutorizacao();
	
	/* Confer�ncia Viagem */
	habilitaJustificativaNaoAutorizacaoConferencia();
});

/**
 * ACTIONS
 */
/* Criação Viagem */
$(".radio-integrante").on("click", function() {
	habilitaInfoNaoFuncionario();
});

$("#btn-add-integrante").click(function() {
	addIntegrante();
});

/* Execução Viagem */
$(".browser").treeview();

$("#POPUP_ITEMCONTROLEFINANCEIRO").dialog({
	autoOpen : false,
	width : "80%",
	height : $(window).height()-200,
	modal : true
});

/* Autorização Viagem */
$(".radio-autorizacao").on("click", function() {
	habilitaJustificativaNaoAutorizacao();
});

/* Confer�ncia Viagem */
$(".radio-autorizacao-conferencia").on("click", function() {
	habilitaJustificativaNaoAutorizacaoConferencia();
});

/* Confer�ncia Viagem */
function habilitaJustificativaNaoAutorizacaoConferencia() {
	if($("#nao_autorizado_conferencia:checked").length) {
		$("#autorizacao-justificativa-conferencia-container").show();
	} else {
		$("#autorizacao-justificativa-conferencia-container").hide();
	}
}
