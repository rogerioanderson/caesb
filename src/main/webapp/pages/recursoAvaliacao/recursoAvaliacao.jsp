<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>

<!doctype html>
<html>
<head>
<%
	//identifica se a p�gina foi aberta a partir de um iframe (popup de cadastro r�pido)
	String iframe = "";
	iframe = request.getParameter("iframe");

%>
<%@include file="/include/header.jsp"%>

<%@include file="/include/security/security.jsp" %>
<title><fmt:message key="citcorpore.comum.title"/></title>

<%@include file="/include/javaScriptsComuns/javaScriptsComuns.jsp"%>
<script type="text/javascript" src="./js/recursoAvaliacao.js"></script>

<%
//se for chamado por iframe deixa apenas a parte de cadastro da p�gina
if(iframe != null){%>
<link rel="stylesheet" type="text/css" href="./css/recursoAvaliacao.css" />
<%}%>

</head>

<body>
	<div id="wrapper">
	<%if(iframe == null){%>
		<%@include file="/include/menu_vertical.jsp"%>
	<%}%>
<!-- Conteudo -->
	<div id="main_container">
	<%if(iframe == null){%>
		<%@include file="/include/menu_horizontal.jsp"%>
	<%}%>

			<form name='form' action='${ctx}/pages/recursoAvaliacao/recursoAvaliacao'>
				<div>
					<fieldset>
						<legend><fmt:message key='citcorpore.comum.filtros'/></legend>
						<table>
							<tr>
								<td>
									<label  class="campoObrigatorio"><fmt:message key="citcorpore.comum.periodo" /></label>
								</td>
								<td>
									<input type='text' name='dataInicio' size="10" maxlength="10" class="Valid[Required,Date] Description[visao.dataDeInicio] Format[Date] datepicker" />
								</td>
								<td>
									<input type='text' name='dataFim' size="10" maxlength="10" class="Valid[Required,Date] Description[avaliacao.fornecedor.dataFim] Format[Date] datepicker" />
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td style='vertical-align: top;'>
									<label><fmt:message key="controle.grupo"/>:</label>
								</td>
								<td colspan="2" style='vertical-align: top;'>
									<select name='idGrupoRecurso' class="Description[Grupo]">
									</select>
								</td>
								<td style='vertical-align: top;'>
									<button type="button" onclick='gerarInformacoes()'>
										<fmt:message key="citcorpore.comum.gerarInformacoes"/>
									</button>
								</td>
							</tr>
						</table>
					</fieldset>
				</div>
				<div id='divInfo'>
				</div>
			</form>

		</div>
		<!-- Fim da Pagina de Conteudo -->
	</div>
	<%@include file="/include/footer.jsp"%>
</body>
</html>

