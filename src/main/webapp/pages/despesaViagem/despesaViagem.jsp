<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.citframework.util.Constantes"%>
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>

<!DOCTYPE html public "">
<html>
	<head>
		<%@include file="/novoLayout/common/include/libCabecalho.jsp"%>
		<%@include file="/novoLayout/common/include/titulo.jsp"%>
		
		<link type="text/css" rel="stylesheet" href="../../novoLayout/common/include/css/template.css"/>
		<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/jqueryTreeview/jquery.treeview.css"/>
		<link type="text/css" rel="stylesheet" href="../../novoLayout/common/include/css/jqueryautocomplete.css"/>
		<link type="text/css" rel="stylesheet" href="css/DespesaViagem.css" />
		
		<%
	        response.setHeader("Cache-Control", "no-cache"); 
	        response.setHeader("Pragma", "no-cache");
	        response.setDateHeader("Expires", -1);
    	%>
	       
	    <script type="text/javascript" src="${ctx}/js/ValidacaoUtils.js"></script>
	    <script type="text/javascript" src="${ctx}/js/PopupManager.js"></script>
	    <script type="text/javascript" src="${ctx}/cit/objects/EmpregadoDTO.js"></script>
	    <script type="text/javascript" src="${ctx}/cit/objects/RequisicaoViagemDTO.js"></script>
	    <script type="text/javascript" src="${ctx}/cit/objects/DespesaViagemDTO.js"></script>
	    <script type="text/javascript" src="${ctx}/cit/objects/IntegranteViagemDTO.js"></script>
	</head>
	
	<cit:janelaAguarde id="JANELA_AGUARDE_MENU"  title="" style="display:none;top:325px;width:300px;left:500px;height:50px;position:absolute;"></cit:janelaAguarde>
	
	<body>
		<div class="nowrapper">
			<div id="content">
				<form name='form' action='${ctx}/pages/despesaViagem/despesaViagem'>
					<input type='hidden' name='idSolicitacaoServico' id='idSolicitacaoServico' />
					<input type='hidden' name='idContrato' id='idContrato' />
					<input type='hidden' name='idTarefa' id='idTarefa' />
					<input type='hidden' name='idIntegranteAux' id='idIntegranteAux' />
					<input type='hidden' name='estado'/>
					<input type='hidden' name='idCidadeOrigem'/>
					<input type='hidden' name='idCidadeDestino'/>
					
					<div class="widget">
                    	<div class="widget-head">
                        	<h2 class="heading" id="titulo"></h2>
                        </div><!-- .widget-head -->
                        
                        <div class="widget-body">
							<div id="divNaoAprovada" class="widget row-fluid" data-toggle="collapse-widget" data-collapse-closed="false">
								<div class="widget-head">
									<h3 class="heading"><fmt:message key="requisicaoViagem.naoAprovada"/></h3>
								</div><!-- .widget-head -->
								
								<div class="widget-body">
									<div id="naoAprovada" class="span12">
									</div>
								</div>
							</div>
                        	
                        	<div style="margin-bottom: 15px;">
                        		<a href="javascript:;" class="btn btn-default btn-primary" onclick="visualizarResponsaveisPopup();">
                        			<i class="icon-white icon-eye-open"></i> <fmt:message key="viagem.visualizarResponsaveis"/>
                        		</a>
                        	</div>
                        	
							<div id="infoViagem" class="widget row-fluid" data-toggle="collapse-widget" data-collapse-closed="true">
								<div class="widget-head">
									<h3 class="heading"><fmt:message key="requisicaoViagem.dadosGerais"/></h3>
								</div><!-- .widget-head -->
								
								<div class="widget-body collapse">
									<div class="widget-body">                 
			                            <div class="row-fluid">
			                         		<div class="span3">
                                            	<label for='finalidade' class="strong campoObrigatorio"><fmt:message key="requisicaoProduto.finalidade" /></label>
                                          		<select name='finalidade' id='finalidade' class="Valid[Required] Description[requisicaoProduto.finalidade] span12"></select>
                                            </div>
			                            	<div class="span3">
			                            		<label for="idCentroCusto" class="strong campoObrigatorio"><fmt:message key="centroResultado"/></label>
			                            	 	<select id='idCentroCusto' name='idCentroCusto' class="Valid[Required] Description[centroResultado.custo] span12"></select>
			                            	</div><!-- .span3 -->
			                            	<div class="span3">
			                            		<label for="idProjeto" class="strong campoObrigatorio"><fmt:message key="requisicaoProduto.projeto"/></label>
			                            	 	<select name='idProjeto' class="Valid[Required] Description[requisicaoProduto.projeto] span12"></select>
			                            	</div><!-- .span3 -->
			                            	<div class="span3">
			                            		<label for="idMotivoViagem" class="strong campoObrigatorio"><fmt:message key="requisicaoViagem.justificativa"/></label>
			                            	 	<select name='idMotivoViagem' class="Valid[Required] Description[requisicaoViagem.justificativa] span12"></select>
			                            	</div><!-- .span3 -->
			                            </div><!-- .row-fluid -->
			                           
			                            <div class="row-fluid">
			                            	<div class="span12">
			                            		<label for="descricaoMotivo" class="strong campoObrigatorio"><fmt:message key="requisicaoViagem.motivo"/></label>
			                            		<textarea name="descricaoMotivo" id="descricaoMotivo" cols='200' rows='5' maxlength = "2000" class="span12"></textarea>
			                            	</div><!-- .span12 -->
			                            </div><!-- .row-fluid -->
				                    </div><!-- widget-body -->
								</div><!-- widget-body -->
							</div><!-- #infoViagem -->
							
							<div id="infoCtrlViagem" class="widget" data-toggle="collapse-widget">
								<div class="widget-head">
									<h3 class="heading"><fmt:message key="requisicaoViagem.integrantes" /></h3>
								</div><!-- .widget-head -->
								
								<div class="widget-body">
									<button type='button' name='btnAdicionarItem' class="rFloat btn btn-icon btn-primary despesa-viagem-buttom-confirma" onclick='adicionarIntegranteViagem();'>
										<i></i> <fmt:message key="requisicaoViagem.adicionarIntegranteViagem" />
									</button>
								</div>
						
								<div class="widget-body">
									<div class="row-fluid">
										<div class="widget widget-hide-button-collapse">
										
											<div class="widget-head">
												<h3 class="heading"><fmt:message key="despesaViagem.viagensIntegrante" /></h3>
<%-- 												<span onclick="abrirPopupItemControleFinanceiro();" id="add_itens" name='add_itens' class="lFloat btn btn-icon btn-primary"><fmt:message key="requisicaoViagem.adicionarItens" /></span> --%>
											</div><!-- .widget-head -->
											
											<div class="widget-body">
												<div class="row-fluid">
													<div id="divBotoesAcao" class="span6">
														<a href="javascript:;" onclick="expandirItemDespesa();">+ Expandir todos</a> | <a href="javascript:;" onclick="retrairItemDespesa();">- Retrair todos</a>
													</div>
												</div>
												
												<div class="row-fluid">
													<div id="despesa-viagem-items-container"></div>
												</div>
											</div><!-- .widget-body -->
										</div><!-- .widget -->
									</div><!-- .row-fluid -->
								</div><!-- .widget-body -->
							</div><!-- #infoCtrlViagem -->
							
							<%--
							<div class="row-fluid">
								<div class="span12 strong">
									<fmt:message key="requisicaoViagem.cancelarRequisicao"/>&nbsp;
									
									<label for="cancelarRequisicao" class="checkbox inline strong">
										<input type="checkbox" class="checkbox" name="cancelarRequisicao" value="S" id="cancelarRequisicao"/> <fmt:message key="citcorpore.comum.sim" />
									</label>
								</div><!-- .span12 -->
							</div><!-- .row-fluid --> 
							 --%>
						</div><!-- .widget-body -->
					</div><!-- .widget -->
					
					<div id="POPUP_DADOSINTEGRANTE" name="POPUP_DADOSINTEGRANTE" title="<fmt:message key="requisicaoViagem.dadosIntegrante"/>">
						<div class='row-fluid'>
							<div class="span12">
								<div id="dadosIntegrante"></div>
							</div>
						</div>		
					</div><!-- #POPUP_ITEMCONTROLEFINANCEIRO -->	
					
				</form>
			</div><!-- #content -->
		</div><!-- #nowrapper -->
		
		<div id="POPUP_ITEMCONTROLEFINANCEIRO" name="POPUP_ITEMCONTROLEFINANCEIRO" title="<fmt:message key="requisicaoViagem.despesaViagem"/>">
			<form name='formItem' id='formItem' action='${ctx}/pages/despesaViagem/despesaViagem'>
				<input type='hidden' name='idDespesaViagem' id='idDespesaViagem' />
				<input type='hidden' name='idSolicitacaoServicoAux' id='idSolicitacaoServicoAux' />
				<input type='hidden' name='idIntegrante' id='idIntegrante' />
				<input type='hidden' name='idFornecedor' id='idFornecedor' />
				<input type='hidden' name='idTipo' id='idTipo' />
				<input type='hidden' name='dataInicio' id='dataInicio' />
				<input type='hidden' name='idRoteiro' id='idRoteiro' />
				<input type='hidden' name='original' id='original' />
				<input type='hidden' name='idMoeda' id='idMoeda' />
				
				<div id="form-itens-container">
					<div class='row-fluid'>
						<div class="span4">
							<label class="campoObrigatorio strong"><fmt:message key="requisicaoViagem.tipoDespesa" /></label>
							<select name='tipoDespesa' id='tipoDespesa' class='span12' onchange="tratarValoresTipoMovimentacao();"></select>
						</div><!-- .span3 -->
						
						<div class="span8">
							<label class="campoObrigatorio strong"><fmt:message key="itemControleFinanceiroViagem.fornecedor" /></label>
							<input type='text' id="nomeFornecedor" name="nomeFornecedor" maxlength="100" class='span12' />
						</div><!-- .span6 -->
					</div><!-- .row-fluid -->
					
					<div class='row-fluid'>
						<div class="span3">
							<label for="idMoedaAux" class="strong campoObrigatorio"><fmt:message key="moeda.moeda"/></label>
							<select id="idMoedaAux" name='idMoedaAux' class="Valid[Required] Description[moeda.moeda] span12"></select>
						</div><!-- span3 -->
						
						<div class="span5">
							<label class="campoObrigatorio strong"><fmt:message key="itemControleFinanceiroViagem.formaPagamento" /></label>
							<select name='idFormaPagamento' id='idFormaPagamento' class='span12'></select>
						</div><!-- .span5 -->
						
					</div><!-- .row-fluid -->
					
					<div class='row-fluid'>
						<div class="span4">
							<label class="campoObrigatorio strong"><fmt:message key="itemControleFinanceiroViagem.valorUnitario" /></label>
							<input type='text' class="format-money span12" id="valor" name="valor" maxlength="8" onblur="document.formItem.fireEvent('calcularTotal');" />
						</div><!-- .span4 -->
						
						<div class="span4">
							<label class="campoObrigatorio strong"><fmt:message key="itemControleFinanceiroViagem.quantidade" /></label> 
							<input type='text' id="quantidade" name="quantidade" class="Format[Numero] span12" maxlength="8" onblur="document.formItem.fireEvent('calcularTotal');" />
						</div><!-- .span4 -->
						
						<div class="span4">
							<label class="strong"><fmt:message key="despesaViagem.totalDespesa" /></label>
							<input type='text' id="valorAdiantamento" name="valorAdiantamento" class="span12" maxlength="20" readonly="readonly" />
						</div><!-- .span4 -->
					</div><!-- .row-fluid -->
					
					<div class='row-fluid'>
						<div class='span12'>
							<label class="strong"><fmt:message key="avaliacaoFonecedor.observacao" /></label>
							<textarea id="observacoes" name="observacoes" class="span12" float: left;"></textarea>
						</div><!-- .span12 -->
					</div><!-- .row-fluid -->
				</div><!-- #form-itens-container -->
				
<!-- 				<div id="integrantes-viagem-container" class="widget"> -->
<!-- 					<div class="widget-head"> -->
<%-- 						<h3 id="integrantes-viagem-heading" class="heading"><fmt:message key="requisicaoViagem.atribuirIntegrante" /></h3> --%>
<!-- 					</div>.widget-head -->
<!-- 					<div class="widget-body"> -->
<!-- 						<ul id="integrantes-itens" class="unstyled"> -->
<!-- 						</ul> -->
<!-- 					</div>.widget-body -->
<!-- 				</div>.widget -->
				
				<div class='row-fluid'>
					<div class="span12">
						<button type='button' name='btnFechar' class="rFloat btn btn-icon" onclick='fecharPopupItemControleFinanceiro()'>
							<i></i> <fmt:message key="Fechar" />
						</button>
						<button type='button' name='btnAdicionarItem' class="rFloat btn btn-icon btn-primary despesa-viagem-buttom-confirma" onclick='adicionarDespesaViagem();'>
							<i></i> <fmt:message key="citcorpore.comum.confirmar" />
						</button>
					</div><!-- .span12 -->
				</div><!-- .row-fluid -->
			</form>
		</div><!-- #POPUP_ITEMCONTROLEFINANCEIRO -->
		
		<div id="POPUP_VISUALIZARRESPONSAVEIS" name="POPUP_VISUALIZARRESPONSAVEIS" title="<fmt:message key="viagem.visualizarResponsaveis"/>"></div><!-- #POPUP_VISUALIZARRESPONSAVEIS -->
		
		<div id="POPUP_NOVOINTEGRANTE_V" title="<fmt:message key="requisicaoViagem.integranteViagem" />" class="dialogViagemResponsavel section hide">
			<iframe  src="" width="100%" height="90%" id="iframeAdicionarNovoIntegrante" style="border: 0px !important;"></iframe>
		</div>
		
		<%@include file="/novoLayout/common/include/libRodape.jsp" %>
		<script src="${ctx}/template_new/js/jqueryTreeview/jquery.treeview.js"></script>
		<script src="js/jquery.maskMoney.js"></script>
		<script src="js/DespesaViagem.js"></script>
	</body>
</html>
