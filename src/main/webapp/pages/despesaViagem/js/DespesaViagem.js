/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
$("#POPUP_ITEMCONTROLEFINANCEIRO").dialog({
	autoOpen : false,
	width : "98%",
	modal : true
});

$("#POPUP_DADOSINTEGRANTE").dialog({
	autoOpen : false,
	width : "500px",
	modal : true
});

$("#POPUP_VISUALIZARRESPONSAVEIS").dialog({
	autoOpen : false,
	width : "500px",
	modal : true
});

$("#POPUP_NOVOINTEGRANTE_V").dialog({
	autoOpen : false,
	width : "80%",
	modal : true
});

function visualizarResponsaveisPopup() {
	document.form.fireEvent("visualizarResponsaveisPopup");
}

function abrirModalIntegrante(idIntegrante){
	document.getElementById("idIntegranteAux").value = idIntegrante;

	document.form.fireEvent('carregaIntegrantePopup');

	$("#POPUP_DADOSINTEGRANTE").dialog({ closeOnEscape: false, open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).show(); } });
	$("#POPUP_DADOSINTEGRANTE").dialog("open");
}

function expandirItemDespesa() {
	$('.browser ul').show(0);
	$('.expandable').addClass('collapsable').removeClass('expandable');
	$('.lastExpandable').addClass('lastCollapsable').removeClass('lastExpandable');
}
function retrairItemDespesa() {
	$('.browser ul').hide(0);
	$('.collapsable').addClass('expandable').removeClass('collapsable');
	$('.lastCollapsable').addClass('lastExpandable').removeClass('lastCollapsable');
}

function abrirPopupItemControleFinanceiro(idIntegrante) {
	document.getElementById("idIntegrante").value = idIntegrante;
	document.getElementById("idDespesaViagem").value = "";

	$("#POPUP_ITEMCONTROLEFINANCEIRO").dialog({ closeOnEscape: false, open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).show(); } });
	$("#POPUP_ITEMCONTROLEFINANCEIRO").dialog("open");
	
	document.form.fireEvent('carregarPopup');
}

function fecharPopupItemControleFinanceiro() {
	document.formItem.fireEvent("limparCamposDosItensDeDespesaEFecharPopup");
}

function tratarValoresTipoMovimentacao() {
	var selectTipo = document.getElementById("tipoDespesa");
	document.getElementById("idTipo").value = selectTipo.options[selectTipo.selectedIndex].value;
	document.formItem.fireEvent("tratarValoresTipoMovimentacao");
}

function adicionarDespesaViagem() {
	if(!validarCamposObrigatorios()) {
		return false;
	}
	document.formItem.fireEvent("adicionarDespesaViagem");

}

function editarDespesaViagem(idDespesaViagem) {
	document.getElementById("idDespesaViagem").value = idDespesaViagem;
	
	$("#POPUP_ITEMCONTROLEFINANCEIRO").dialog({ closeOnEscape: false, open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).show(); } });
	$("#POPUP_ITEMCONTROLEFINANCEIRO").dialog("open");
	
	document.formItem.fireEvent("carregarPopup");
}

function excluirDespesaViagem(idDespesaViagem) {
	if(window.confirm(i18n_message("despesaViagem.excluirDespesa")+"?")) {
		document.getElementById("idDespesaViagem").value = idDespesaViagem;
		document.getElementById("idSolicitacaoServicoAux").value = document.getElementById("idSolicitacaoServico").value;
		document.formItem.fireEvent("excluirDespesaViagem");
	}
}

function calculaMinutos(data, minutos) {
	var hours = data.getHours();
	var min = data.getMinutes()+minutos;

	if(min < 0) {
		while(diffMin < 0) {
			hours--;
			min += 60;
		}
	} else {
		while(min > 60) {
			hours++;
			min -= 60;
		}
	}

	return new Date(data.getFullYear(), data.getMonth(), data.getDay(), hours, min, data.getSeconds(), data.getMilliseconds());
};

//Valida se os campos obrigatorios da Popup item estao preenchidos (o Valid[Required] nao funciona nesse caso)
//False + mensagem caso algum campo obrigatorio nao esteja preenchido
//True caso todos os campos obrigatorios estejam preenchidos
function validarCamposObrigatorios(){
	if(document.getElementById("tipoDespesa").value == ""){
		alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("requisicaoViagem.tipoDespesa")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
		return false;
	}

	if(StringUtils.isBlank(StringUtils.trim(document.getElementById("nomeFornecedor").value)) || StringUtils.isBlank(StringUtils.trim(document.getElementById("idFornecedor").value))){
		alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("itemControleFinanceiroViagem.fornecedor")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
		return false;
	}

	if(StringUtils.isBlank(StringUtils.trim(document.getElementById("idMoeda").value))){
		alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("contrato.moeda")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
		return false;
	}

	if(StringUtils.isBlank(StringUtils.trim(document.getElementById("idFormaPagamento").value))){
		alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("itemControleFinanceiroViagem.formaPagamento")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
		return false;
	}

	if(StringUtils.isBlank(StringUtils.trim(document.getElementById("valor").value))){
		alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("itemControleFinanceiroViagem.valorUnitario")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
		return false;
	}

	if(StringUtils.isBlank(StringUtils.trim(document.getElementById("quantidade").value))){
		alert(i18n_message("rh.alertOCampo")+" ["+i18n_message("carrinho.quantidade")+"] "+i18n_message("rh.alertEObrigatorio")+"!");
		return false;
	}

	return true;
}

//Masks
$('.format-money').maskMoney({
	thousands: '',
	decimal: ',',
	allowNegative: true
});

// Auto Complete Fornecedor
var completeFornecedor = $('#nomeFornecedor').autocomplete({
	serviceUrl:'pages/autoCompleteParceiro/autoCompleteParceiro.load',
	noCache: true,
	onSelect: function(value, data){
		$('#idFornecedor').val(data);
		$('#nomeFornecedor').val(value);
	}
});

// Marcar todos
$('body').on('click', '#idIntegranteMarcaTodos', function() {
	if(this.checked) {
		$('.idIntegrate').attr('checked', 'checked');
	} else { 
		$('.idIntegrate').removeAttr('checked');
	}
});

$('body').on('click', '.idIntegrate', function() {
	var checkedCount = $('.idIntegrate:checked').length;
	var checkCount = $('.idIntegrate').length;
	if((document.getElementById('idIntegranteMarcaTodos').checked == false) && (checkedCount == checkCount)) {
		document.getElementById('idIntegranteMarcaTodos').checked = true;
	} else {
		document.getElementById('idIntegranteMarcaTodos').checked = false;
	}
});

function adicionarIntegranteViagem(){
	document.form.fireEvent("adicionarIntegranteViagem");
}

function exibirModalIntegrantes(){
	criaDialogHere('P', 'dialogViagemResponsavel');
	document.getElementById('iframeAdicionarNovoIntegrante').src = URL_SISTEMA+'pages/requisicaoViagem/requisicaoViagem.load?iframe=true';
	$("#POPUP_NOVOINTEGRANTE_V").dialog({ closeOnEscape: false, open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); } });
	$("#POPUP_NOVOINTEGRANTE_V").dialog("open");
}

function fecharPopupNovoIntegrante(){
	$("#POPUP_NOVOINTEGRANTE_VJ").dialog("hide");
}

function criaDialogHere(tamanho, classDialog, functionBeforeClose, adicionarInt){
	var docElement = document.documentElement;

	var porcentagem = 90;

	switch(tamanho){
		case 'G':
			porcentagem = 90;
			break;
		case 'M':
			porcentagem = 75;
			break;
		case 'P':
			porcentagem = 60;
			break;
		case 'PP':
			porcentagem = 45;
			break;
		case 'SP':
			porcentagem = 25;
			break;
	}

	$("."+classDialog).dialog({
		dialogClass: "no-close",
		width : porcentagem+"%",
		height : porcentagem*(docElement.clientHeight)/100,
		modal : true,
		autoOpen : false,
		resizable : true,
		show : "fade",
		hide : "fade"
	});
}

function fecharFrameAddIntegrantes(){
	document.getElementById('iframeAdicionarNovoIntegrante').src = '';''
	$("#POPUP_NOVOINTEGRANTE_V").dialog("close");
	document.form.fireEvent("removerAddIntegranteViagemDaSessao");
}
