<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->

<%@page import="br.com.centralit.citcorpore.util.ParametroUtil"%>
<%@page import="br.com.centralit.citcorpore.util.Enumerados"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>
<%@page import="br.com.centralit.citcorpore.util.Enumerados.SituacaoRequisicaoMudanca"%>
<%@page import="br.com.centralit.bpm.util.Enumerados.SituacaoItemTrabalho"%>

<!-- Thiago Fernandes - 23/10/2013 16:50 - Sol. 121468 - Retirar barras de rolagens desnecesarias. Linhas 652, 653, 792, 1221. -->
<%
    response.setCharacterEncoding("ISO-8859-1");
    response.setHeader( "Cache-Control", "no-cache");
    response.setHeader( "Pragma", "no-cache");
    response.setDateHeader ( "Expires", -1);
    
    String PAGE_CADASTRO = "/pages/requisicaoMudanca/requisicaoMudanca.load";
    
    String login = "";
	UsuarioDTO usuario = WebUtil.getUsuario(request);
	if (usuario != null)
		login = usuario.getLogin();
    pageContext.setAttribute("situacaoSuspensa", SituacaoRequisicaoMudanca.Suspensa.name());
    pageContext.setAttribute("login", login);
    pageContext.setAttribute("PAGE_CADASTRO",PAGE_CADASTRO);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<%@include file="/include/security/security.jsp" %>
	<%@include file="/include/header.jsp"%>
    <%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>
<%@include file="/novoLayout/common/include/libCabecalho.jsp" %>

<link href="${ctx}/novoLayout/common/theme/css/atualiza-antigo.css" rel="stylesheet" />
		<title><fmt:message key="citcorpore.comum.title" /></title>
        
    <title><fmt:message key="citcorpore.comum.title"/></title>
	<%@include file="/include/javaScriptsComuns/javaScriptsComuns.jsp" %>
	
    <link type="text/css" rel="stylesheet" href="${ctx}/css/layout-default-latest.css"/>
    <link type="text/css" rel="stylesheet" href="${ctx}/css/jquery.ui.all.css"/>
	<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/themeroller/Aristo.css"/>
	<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/main.css"/>
	<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/theme_base.css"/>
	<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/buttons.css"/>
	<link rel="stylesheet" type="text/css" href="${ctx}/template_new/css/ie.css"/>
	<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/jqueryTreeview/jquery.treeview.css"/>
	<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/jQueryGantt/css/style.css" />
    <link rel="stylesheet" type="text/css" href="${ctx}/css/slick.grid.css"/>	
	<link type="text/css" rel="stylesheet" class="include" href="${ctx}/pages/graficos/jquery.jqplot.min.css" />
    <link rel="stylesheet" type="text/css" href="css/gerenciamentoMudancas.css">

    <script type="text/javascript" src="${ctx}/js/jquery-latest.js"></script>
    <script type="text/javascript" src="${ctx}/js/jquery-ui-latest.js"></script>
    <script type="text/javascript" src="${ctx}/js/jquery.layout-latest.js"></script>
    <script type="text/javascript" src="${ctx}/js/debug.js"></script>

	<script type="text/javascript" src="${ctx}/template_new/js/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="${ctx}/template_new/js/autogrow/jquery.autogrowtextarea.js"></script>
	<script type="text/javascript" src="${ctx}/template_new/js/multiselect/js/ui.multiselect.js"></script>
	<script type="text/javascript" src="${ctx}/template_new/js/selectbox/jquery.selectBox.min.js"></script>
	<script type="text/javascript" src="${ctx}/template_new/js/timepicker/jquery.timepicker.js"></script>
	<script type="text/javascript" src="${ctx}/template_new/js/colorpicker/js/colorpicker.js"></script>
	<script type="text/javascript" src="${ctx}/template_new/js/tiptip/jquery.tipTip.minified.js"></script>
	<script type="text/javascript" src="${ctx}/template_new/js/validation/jquery.validate.min.js"></script>		
	<script type="text/javascript" src="${ctx}/template_new/js/uitotop/js/jquery.ui.totop.js"></script>
	<script type="text/javascript" src="${ctx}/template_new/js/custom/ui.js"></script>
	<script type="text/javascript" src="${ctx}/template_new/js/custom/forms.js"></script>
	<script type="text/javascript" src="${ctx}/js/PopupManager.js"></script>
	<script class="include" type="text/javascript" src="${ctx}/pages/graficos/jquery.jqplot.min.js"></script>
	<script class="include" type="text/javascript" src="${ctx}/pages/graficos/plugins/jqplot.pieRenderer.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/json2.js"></script>
    <script type="text/javascript" src="${ctx}/js/jquery.rule-1.0.1.1-min.js"></script>
    <script type="text/javascript" src="${ctx}/js/jquery.event.drag.custom.js"></script>
    <script type="text/javascript" src="${ctx}/js/slick.core.js"></script>
    <script type="text/javascript" src="${ctx}/js/slick.editors.js"></script>
    <script type="text/javascript" src="${ctx}/js/slick.grid.js"></script>
    <script type="text/javascript" src="${ctx}/js/CollectionUtils.js"></script>
     <script type="text/javascript" src="${ctx}/js/highcharts.js"></script>
    <script type="text/javascript" src="${ctx}/js/exporting.js"></script>
    <script type="text/javascript">
    	var situacaoSuspensa = "${situacaoSuspensa}";
    	var login = "${login}";
    	var idRequisicao = "${idRequisicao}";
    	var PAGE_CADASTRO = "${PAGE_CADASTRO}";
    </script>
    <script type="text/javascript" src="js/gerenciamentoMudancas.js"></script>

</head>
<cit:janelaAguarde id="JANELA_AGUARDE_MENU" title="<fmt:message key='citcorpore.comum.aguardeProcessando'/>" style="display:none;top:100px;width:300px;left:200px;height:50px;position:absolute;">
</cit:janelaAguarde>
<body>

<%@include file="/include/menu_horizontal_gerenciamento.jsp"%>

	<h2 class="loading"><fmt:message key="citcorpore.comum.aguardecarregando"/></h2>
	
	<div id="botao_voltar">
	<a href="#" onclick="voltar()">
						<fmt:message key="citcorpore.comum.voltar" />
					</a>
	</div>			

	<div class="ui-layout-center hidden ">
	    <table width='100%' height='100%' style='width: 100%; height: 100%; '>
	         <tr>
	         	<td width='100%' style='width: 100%; vertical-align: top !important; '>
					<iframe id='fraRequisicaoMudanca' src='about:blank' width="100%" style='width: 100%; border:none;' onload="resize_iframe()"></iframe>
				</td>
			</tr>
		</table>		
	</div>	

	<!-- <div class="ui-layout-east hidden"></div> -->
	<div  class="ui-layout-south hidden ">
	
	<div class="flat_area grid_16">
					<h2><fmt:message key='gerenciamentoMudanca.gerenciamentoMudancas'/></h2>						
	</div>
	
	<div class="box grid_16 tabs">

	
	
	<ul class="tab_header clearfix">
		<li>
			<a href="#tabs-1"><fmt:message key='gerenciamentoMudanca.gerenciamentoMudancas'/></a>
		</li>
		<li>
			<a href="#tabs-2" class="round_top"><fmt:message key='citcorpore.comum.graficos'/></a>
		</li>
	</ul>	
	

	<div class="toggle_container">					
<!---------- In�cio Gerenciamento de Mudan�a ---------->					
					
		<div id="tabs-1" class="block">
	    	<div class="clearfix ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" style="min-height: 75px; ">
	    	<form name='formPesquisa' id='formPesquisa' action='${ctx}/pages/gerenciamentoMudancas/gerenciamentoMudancas'>
	    	 <div class="space" >
				<table width="100%"  cellspacing="3" >
					<tr>
						<td style='vertical-align: top;'>
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td style='vertical-align: middle;'>
											<fmt:message key="requisicaoMudanca.NumeroMudanca" />:&nbsp;
										</td>
										<td>
											<input type="text" onkeypress="return disableEnterKey(event)"  name="idRequisicaoSel" id="idRequisicaoSel" class="Format[Numero]"   />
										</td>
										<td style='vertical-align: middle;'>		 
											<%-- <img border="0" title="Pesquisar" style="cursor:pointer" src="${ctx}/imagens/search.png" onclick="atualizarListaTarefas()"/>&nbsp; --%>
											<a href='#' class='btn-action glyphicons search btn-default titulo'  title="<fmt:message key="citcorpore.comum.pesquisar"/>" onclick="atualizarListaTarefas()"><i></i></a>
										</td>
										<td style='vertical-align: middle;'>&nbsp;&nbsp;
											<fmt:message key="gerenciaservico.atribuidacompartilhada"/>&nbsp;
										</td>
										<td style='vertical-align: middle;'>
											<select name='atribuidaCompartilhada' id='atribuidaCompartilhada' onchange="atualizarListaTarefas()"></select>
										</td>																				
										<td style='vertical-align: middle;'>
											<%-- <img border="0" title="<fmt:message key="citcorpore.comum.limpar" />" 	  style="cursor:pointer" src="${ctx}/imagens/clear.png" onclick="document.formPesquisa.clear();atualizarListaTarefas()"/> --%>
											<span style='cursor:pointer;' class='btn btn-mini btn-primary titulo' title="<fmt:message key="citcorpore.comum.limpar" />"  onclick="document.formPesquisa.clear();atualizarListaTarefas()"><fmt:message key='citcorpore.ui.botao.rotulo.Limpar' /></span>
										</td>
									</tr>
								</table>
						</td>
						<td>
							&nbsp;
						</td>
					<%-- 	<td>
							<button type='button' title="<fmt:message key='gerenciarequisicao.pesquisaMudanca'/>" class="light img_icon has_text" onclick="abrePopupPesquisa()">
								<div class="asa">
									<span class=""><fmt:message key="gerenciarequisicao.pesquisaMudanca"/></span>
									<div class="G-asx2 T-I-J3 J-J5-Ji"></div>
								</div>
							</button>
						</td>						
						<td style='vertical-align: top;' width="20%">							
							<button  type="button" class="light img_icon has_text" onclick="abrirSolicitacao()" style='width: 150px'>
								<img border="0" title="<fmt:message key="requisicaoMudanca.cadastrarMudanca" />" src="${ctx}/imagens/add-icon-medium.png" />
								<span ><fmt:message key="requisicaoMudanca.novaMudanca" /></span>
							</button>
						</td>
						<td style='vertical-align: top; text-align: right;float: right;'>
							<button style="" type="button" class="light img_icon has_text" onclick="atualizarListaTarefas()" >
								<img title="<fmt:message key="citcorpore.comum.atualizar" />" src="${ctx}/imagens/refresh.png" /><span><fmt:message key="citcorpore.comum.atualizar" /></span>
							</button>								
						</td> --%>
					<td>
					<span class="btn btn-small btn-primary" title="<fmt:message key='requisicaoMudanca.cadastrarMudanca'/>"  onclick="abrirSolicitacao()">
						<fmt:message key="requisicaoMudanca.cadastrarMudanca"/>
					</span>
					
					<span  title="<fmt:message key='gerenciarequisicao.pesquisaMudanca'/>"  class="btn btn-small btn-primary" onclick="abrePopupPesquisa()">
						<fmt:message key="gerenciarequisicao.pesquisaMudanca"/>
					</span>
					
					<%-- <button type='button' title="<fmt:message key="citcorpore.comum.atualizar" />" class="T-I J-J5-Ji nu T-I-ax7 L3 T-I-JO T-I-hvr" onclick="atualizarListaTarefas()">
						<div class="asa">
							<span class="J-J5-Ji ask">&nbsp;</span>
							<div class="asf T-I-J3 J-J5-Ji"></div>
						</div>
					</button> --%>
					<span class="btn btn-default btn-primary" type="button" onclick="atualizarListaTarefas();" id=""><i class="icon-white icon-refresh"></i></span>
					
					<div class="T-I J-J5-Ji ar7 nf L3 T-I-JO ">
						<label>
							<input type='checkbox' id='chkAtualiza' name='chkAtualiza' value='X'/>
							<fmt:message key="citcorpore.comum.atualizar" />&nbsp;<fmt:message key="citcorpore.comum.automaticamente" />
						</label>
					</div>
			</td>
					</tr>
				</table>
				</div>
			</form>				
	    	</div>	
	  
		
		<div id='divConteudoLista' class="ui-layout-content" style="height: 380px;">
	    	<div id='divConteudoListaInterior' style='height: 100%; width: 100%'></div>
	    </div>
	</div>
<!---------- Fim Gerenciamento de Mundan�a ---------->	
	
	
<!---------- In�cio Gr�ficos ---------->	
	<div id="tabs-2" class="block">	
		<table cellpadding='0' cellspacing='0'>		
			<tr>
				<td id='tdAvisosSol' style='vertical-align: top; '>
				</td>				
			</tr>
		</table>								
	</div>
<!---------- Fim Gr�ficos ---------->		
	
	
	
	<form name='form' id='form' action='${ctx}/pages/gerenciamentoMudancas/gerenciamentoMudancas'>
		<input type='hidden' name='idFluxo'/>
		<input type='hidden' name='idVisao'/>
		<input type='hidden' name='idTarefa'/>
		<input type='hidden' name='acaoFluxo'/>
		<input type='hidden' name='idUsuarioDestino'/>
		<input type='hidden' name='numeroContratoSel'/>
		<input type='hidden' name='idRequisicaoSel'/>
		<input type='hidden' name='atribuidaCompartilhada'/>
		<input type='hidden' name='idRequisicao' id='idRequisicaoForm'/>		
		<input type='hidden' name='erroGrid' id='erroGrid'/>	
	</form>
	
	<div id="POPUP_VISAO">
		<iframe id='fraVisao' src='about:blank' width="98%" height="99%"></iframe>
	</div>
	<div id="POPUP_REUNIAO">
		<iframe id='fraReuniao' src='about:blank' width="100%" height="100%"></iframe>
	</div>
		
	<div id="popupCadastroRapido">
          <iframe id="frameCadastroRapido" name="frameCadastroRapido" width="100%" height="100%"></iframe>
	</div>
</div></div></div>
</body>
</html>

