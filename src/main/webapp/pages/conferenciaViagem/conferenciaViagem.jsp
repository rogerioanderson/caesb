<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.citframework.util.Constantes"%>
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>

<!DOCTYPE html public "">
<html>
	<head>
		<%@include file="/novoLayout/common/include/libCabecalho.jsp"%>
		<%@include file="/novoLayout/common/include/titulo.jsp"%>
		
		<link type="text/css" rel="stylesheet" href="../../novoLayout/common/include/css/template.css"/>
		<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/jqueryTreeview/jquery.treeview.css"/>
		<link type="text/css" rel="stylesheet" href="../../novoLayout/common/include/css/jqueryautocomplete.css"/>
		<link type="text/css" rel="stylesheet" href="css/conferenciaViagem.css" />
		
		<%
	        response.setHeader("Cache-Control", "no-cache"); 
	        response.setHeader("Pragma", "no-cache");
	        response.setDateHeader("Expires", -1);
    	%>
	       
	    <script type="text/javascript" src="${ctx}/js/ValidacaoUtils.js"></script>
	    <script type="text/javascript" src="${ctx}/js/PopupManager.js"></script>
	    <script type="text/javascript" src="${ctx}/cit/objects/EmpregadoDTO.js"></script>
	    <script type="text/javascript" src="${ctx}/cit/objects/RequisicaoViagemDTO.js"></script>
	    <script type="text/javascript" src="${ctx}/cit/objects/DespesaViagemDTO.js"></script>
	    <script type="text/javascript" src="${ctx}/cit/objects/PrestacaoContasViagemDTO.js"></script>
	</head>
	
	<cit:janelaAguarde id="JANELA_AGUARDE_MENU"  title="" style="display:none;top:325px;width:300px;left:500px;height:50px;position:absolute;"></cit:janelaAguarde>
	
	<body>
		<div class="nowrapper">
			<!-- Inicio conteudo -->
			<div id="content">
					<form id="form" name="form" action='${ctx}/pages/conferenciaViagem/conferenciaViagem'>
					<input type='hidden' name='idSolicitacaoServico' id='idSolicitacaoServico' /> 
					<input type='hidden' name='idEmpregado' id='idEmpregado' /> 
					<input type='hidden' name='idPrestacaoContasViagem' id='idPrestacaoContasViagem' /> 
					<input type='hidden' name='itensPrestacaoContasViagemSerialize' id="itensPrestacaoContasViagemSerialize"/>
					<input type='hidden' name='idEmpregado' id="idEmpregado"/>
					<input type='hidden' name='situacao' id='situacao'/>
					<input type='hidden' name='listItens' id='listItens'/>
					<input type='hidden' name='idResponsavel' id='idResponsavel'/>
					<input type='hidden' name='idAprovacao' id='idAprovacao'/>
					<input type='hidden' name='idContrato' id='idContrato'/>
					<input type='hidden' name='idTarefa' id='idTarefa'/>
					
					<div class="widget">
						<div class="widget-head">
							<h4 class="heading"><fmt:message key="prestacaoContas.aprovarPrestarConta"/></h4>
						</div><!-- .widget-head -->
						
					<div class="widget-body">
						<div style="margin-bottom: 15px;">
                      			<a href="javascript:;" class="btn btn-default btn-primary" onclick="visualizarResponsaveisPopup();">
                       			<i class="icon-white icon-eye-open"></i> <fmt:message key="viagem.visualizarResponsaveis"/>
                       		</a>
                       	</div>
	                        	
					<div id="infoViagem" class="widget row-fluid" data-toggle="collapse-widget" data-collapse-closed="true">
						<div class="widget-head">
							<h4 class="heading"><fmt:message key="requisicaoViagem.dadosGerais"/></h4>
						</div><!-- .widget-head -->
						
						<div class="widget-body collapse">
							<div class="widget-body">
								<div class="row-fluid">
	                         		<div class="span3">
                                          	<label for='finalidade' class="strong campoObrigatorio"><fmt:message key="requisicaoProduto.finalidade" /></label>
                                        		<select name='finalidade' disabled="disabled" id='finalidade' class="Valid[Required] Description[requisicaoProduto.finalidade] span12"></select>
                                          </div>
	                            	<div class="span3">
	                            		<label for="idCentroCusto" class="strong campoObrigatorio"><fmt:message key="centroResultado"/></label>
	                            	 	<select id='idCentroCusto' disabled="disabled" name='idCentroCusto' class="Valid[Required] Description[centroResultado.custo] span12"></select>
	                            	</div><!-- .span3 -->
	                            	<div class="span3">
	                            		<label for="idProjeto" class="strong campoObrigatorio"><fmt:message key="requisicaoProduto.projeto"/></label>
	                            	 	<select name='idProjeto' disabled="disabled" class="Valid[Required] Description[requisicaoProduto.projeto] span12"></select>
	                            	</div><!-- .span3 -->
	                            	<div class="span3">
	                            		<label for="idMotivoViagem" class="strong campoObrigatorio"><fmt:message key="requisicaoViagem.justificativa"/></label>
	                            	 	<select name='idMotivoViagem' disabled="disabled" class="Valid[Required] Description[requisicaoViagem.justificativa] span12"></select>
	                            	</div><!-- .span3 -->
	                            </div><!-- .row-fluid -->
	                           
	                            <div class="row-fluid">
	                            	<div class="span12">
	                            		<label for="descricaoMotivo" class="strong campoObrigatorio"><fmt:message key="requisicaoViagem.motivo"/></label>
	                            		<textarea name="descricaoMotivo" disabled="disabled" id="descricaoMotivo" cols='200' rows='5' maxlength = "2000" class="span12"></textarea>
	                            	</div><!-- .span12 -->
	                            </div><!-- .row-fluid -->
		                    </div><!-- widget-body -->
						</div><!-- widget-body -->	
					</div><!-- #infoViagem -->
					
					<div id="infoPrestacaoContas" class="widget" data-toggle="collapse-widget">
						<div class="widget-head">
							<h4 class="heading" id="nomeIntegranteViagem"></h4>
						</div><!-- .widget-head -->
						
						<div class="widget-body">
						
							<div class="row-fluid">
								<div id="divCustoTotal" class="span12">
									<label id="valorTotalViagem" style="float: right; margin-right: 18px;"></label>
								</div>
							</div>	
								
                            <div class="row-fluid">
                                   <div class="span4">
                                         <label class="strong">Valor a prestar contas</label>
                                         <input type="text" name="totalPrestacaoContas" id="totalPrestacaoContas" readonly="true" class="Format[Moeda] span12" />
                                   </div><!-- .span4 -->
                                   
                                   <div class="span4">
                                         <label class="strong">Total dos lan�amento</label>
                                         <input type="text" name="totalLancamentos" id="totalLancamentos" readonly="true" class="Format[Moeda] span12" />
                                   </div><!-- .span4 -->
                                   
                                   <div class="span4">
                                         <label class="strong">Diferen�a</label>
                                         <input type="text" name="valorDiferenca" id="valorDiferenca" readonly="true" class="Format[Moeda] span12" />
                                   </div><!-- .span4 -->
                            </div><!-- .row-fluid -->
                                  
                            
                            <div id="itensPrestacaoContas" class="widget widget-hide-button-collapse">   
                               <div class="widget-head">
                                     <h3 class="heading">Itens</h3>
                               </div><!-- .widget-head -->
                               
                               <div class="widget-body">
                               	  <div class="row-fluid">
                               	  	<div class="span12">
	                                  <table class="table table-hover table-bordered table-striped" id="tabelaItemPrestacaoContasViagem">
	                                    <tr>
	                                       <th style="width: 9%;"><fmt:message  key="itemPrestacaoContasViagem.numeroNota" /></th>
	                                       <th style="width: 7%;"><fmt:message  key="itemPrestacaoContasViagem.data" /></th>
	                                       <th style="width: 10%; text-align: center;"><fmt:message  key="itemPrestacaoContasViagem.nomeFornecedor" /></th>
	                                       <th style="width: 7%;"><fmt:message  key="citcorpore.comum.valor" /></th>
	                                       <th style="width: 31%;"><fmt:message  key="citcorpore.comum.descricao" /></th>
	                                    </tr>
	                                  </table>
                                  	</div>
                                  </div>
                              </div><!-- .widget-body -->
							</div><!-- #itensPrestacaoContas -->
							
						</div><!-- .widget-body -->
					</div><!-- #infoPrestacaoContas -->
					
					<div id="divAprovacao" class="widget" data-toggle="collapse-widget">
						<div class="widget-head">
							<h4 class="heading" id="integrante"><fmt:message key="prestacaoContas.aprovarPrestacaoConta" /></h4>
						</div><!-- .widget-head -->
						
						<div class="widget-body">
							<div class="row-fluid">
				               <div class="span2" >
			                       <label style='cursor:pointer' class='radio strong'>
	                       	   	      <input type='radio' name="aprovado" value="S" onclick='configuraJustificativa("S");'  checked="checked"><fmt:message key="citcorpore.comum.aprovada"/>
	                       	   	   </label>
			                       <label style='cursor:pointer' class='radio strong'>
			                       	  <input type='radio' name="aprovado" value="N"  onclick='configuraJustificativa("N");' ><fmt:message key="citcorpore.comum.naoAprovada"/>
			                       </label>
				               </div>
				               <div id="divJustificativa">
				                   <div class="span4">
				                        <fieldset>
				                            <label class="campoObrigatorio"><fmt:message key="itemRequisicaoProduto.justificativa" /></label>
				                            <div>
				                                <select id='idJustificativaAutorizacao' class="span12" name='idJustificativaAutorizacao'></select>
				                            </div>
				                        </fieldset>
				                   </div>
				                   <div class="span6">
				                       <fieldset>
				                           <label class="campoObrigatorio"><fmt:message key="itemRequisicaoProduto.complementoJustificativa" /></label>
				                           <div>
				                                <textarea id="complemJustificativaAutorizacao" name="complemJustificativaAutorizacao" class="span12"></textarea>                               
				                           </div>
				                       </fieldset>
				                   </div>
				               </div>
				           </div> 
						</div><!-- .widget-body -->
					</div><!-- #divAprovacao -->
					
					</div><!-- #widget-body -->
				</div><!-- #widget -->
				<div id="POPUP_VISUALIZARRESPONSAVEIS" name="POPUP_VISUALIZARRESPONSAVEIS" title="<fmt:message key="viagem.visualizarResponsaveis"/>"></div><!-- #POPUP_VISUALIZARRESPONSAVEIS -->
				</form>
	   		</div><!-- #content -->
	    </div><!-- #nowrapper -->
	    
	    <%@include file="/novoLayout/common/include/libRodape.jsp" %>
		<script src="${ctx}/template_new/js/jqueryTreeview/jquery.treeview.js"></script>
		<script src="js/conferenciaViagem.js"></script>
	
	</body>
</html>
