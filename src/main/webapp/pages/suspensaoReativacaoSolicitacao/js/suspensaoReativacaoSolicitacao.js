/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * 
 */

		function suspenderSolicitacoes(){
			document.getElementById("tipoAcao").value = "suspender";
			if(validarCampos(document.getElementById("tipoAcao").value)!=true){
				var confimacao = confirm(i18n_message("suspensaoReativacaoSolicitacao.MensagemConfirmacaoSuspensao"));
				if(confimacao==true){
					parent.janelaAguarde();
					document.form.fireEvent("processarSolicitacoes");
					document.form.clear();
				}
			}

		}

		function reativarSolicitacoes(){
			document.getElementById("tipoAcao").value = "reativar";
			if(validarCampos(document.getElementById("tipoAcao").value)!=true){
				var confimacao = confirm(i18n_message("suspensaoReativacaoSolicitacao.MensagemConfirmacaoReativacao"));
				if(confimacao==true){
					parent.janelaAguarde();
					document.form.fireEvent("processarSolicitacoes");
					document.form.clear();
				}

			}
		}

		function validarCampos(tipoAcao){

			if(document.getElementById("idContrato").value==""){
				alert(i18n_message("suspensaoReativacaoSolicitacao.alertaCampoVazioContrato"));
				return true;
			}
			if(document.getElementById("solicitante").value==""){
				alert(i18n_message("suspensaoReativacaoSolicitacao.alertaCampoVazioSolicitante"));
				return true;
			}
			if(document.getElementById("idGrupo").value==""){
				alert(i18n_message("suspensaoReativacaoSolicitacao.alertaCampoVazioidGrupo"));
				return true;
			}
			if(tipoAcao=='suspender'){
				if(document.getElementById("justificativa").value==""){
					alert(i18n_message("suspensaoReativacaoSolicitacao.alertaCampoVaziojustificativa"));
					return true;
				}
				if(document.getElementById("idJustificativa").value==""){
					alert(i18n_message("suspensaoReativacaoSolicitacao.alertaCampoVazioidJustificativa"));
					return true;
				}

			}
			return false;

		}

		var completeSolicitante;
		$(function(){
			$('#idSelectTipo').on('change', function() {
				  if(this.value=='Suspender'){
					  $("#idReativacao").hide();
					  $("#idSuspensao").show();
					  $("#solicitante").attr("disabled", "disabled");
					  document.getElementById("solicitanteSuspensao").style.display = 'block';
					  document.getElementById("solicitanteReativacao").style.display = 'none';
				  }
				  else{
					  $("#idSuspensao").hide();
					  $("#idReativacao").show();
					  $("#solicitante").attr("disabled", "disabled");
					  document.getElementById("solicitanteReativacao").style.display = 'block';
					  document.getElementById("solicitanteSuspensao").style.display = 'none';
				  }
				  document.form.reset();
			});

			$('#idContrato').on('change', function() {
				if(this.value!=""){
					$("#solicitante").removeAttr("disabled");
					$("#btnPesqAvancada").removeAttr("disabled");
				}else{
					$("#solicitante").attr("disabled", "disabled");
					$("#btnPesqAvancada").attr("disabled", "disabled");
				}
				$("#solicitante").val("");
			});

			completeSolicitante = $('#solicitante').autocomplete({
				serviceUrl:'pages/autoCompleteSolicitante/autoCompleteSolicitante.load',
				noCache: true,
				onSelect: function(value, data){
					$('#idSolicitante').val(data);
					$('#solicitante').val(value);
					$('#nomecontato').val(value);
				}
			});
		});


		/**Monta os parametros para a buscas do autocomplete**/
		function montaParametrosAutocompleteServico(){
		 	contrato = $("#idContrato").val();
		 	completeSolicitante.setOptions({params: {contrato: contrato} });
		}

		function LOOKUP_SOLICITANTE_select(id, desc){
			document.form.solicitante.value = desc;
			$('#modal_lookupSolicitante').modal('hide');
		}
