/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
	var objTab = null;

	addEvent(window, "load", load, false);
	function load() {
		document.form.afterRestore = function() {
			$('.tabs').tabs('select', 0);
		}
	}

	function LOOKUP_PRODUTO_select(id, desc) {
		document.form.restore({
			idProduto : id
		});
	}

	function excluir() {
		if (document.getElementById("idProduto").value != "") {
			if (confirm(i18n_message("citcorpore.comum.deleta"))) {
				document.form.fireEvent("delete");
			}
		}
	}

	 var popup;

	$(function() {

		if (iframe == 'true') {
			popup = new PopupManager(580, 680, ctx+"/pages/");
		} else {
			popup = new PopupManager(1100, 800, ctx+"/pages/");
		}


		$("#POPUP_MARCA").dialog({
			autoOpen : false,
			width : 500,
			height : 400,
			modal : true
		});

		$("#POPUP_TIPOPRODUTO").dialog({
			autoOpen : false,
			width : 500,
			height : 400,
			modal : true
		});


		$("#nomeMarca").click(function() {
			$("#POPUP_MARCA").dialog("open");
		});

		$("#nomeProduto").click(function() {
			$("#POPUP_TIPOPRODUTO").dialog("open");
		});

	});


	function LOOKUP_MARCA_select(id, desc){
		document.form.idMarca.value = id;
		document.form.nomeMarca.value  = desc;
		fecharNomeMarca();
	}

	function LOOKUP_TIPOPRODUTO_select(id, desc){
		document.form.idTipoProduto.value = id;
		document.form.nomeProduto.value  = desc;
		fecharTipoProduto();
	}


	function fecharNomeMarca(){
		$("#POPUP_MARCA").dialog('close');
	}

	function fecharTipoProduto(){
		$("#POPUP_TIPOPRODUTO").dialog('close');
	}

	function limparForm(){
		document.getElementById('file_uploadAnexos').value = '';
		document.form.fireEvent("clear");
	}

	function chamaPopupCadastroTipoProduto(){
		popup.abrePopupParms('tipoProduto', '', '');
	}
	
	// função chamada no caso de uma popup ser chamada dentro de outra popup. O width é definido por porcentagem e não por 'px'
	function redimencionaPopupCadastroRapido(){
		$('#popupCadastroRapido').dialog('option', 'width', '100%');
		$('#popupCadastroRapido').dialog('option', 'left', '0px');
	}
