<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.centralit.citcorpore.bean.EmpregadoDTO"%>
<%@page import="br.com.citframework.util.UtilStrings"%>

<!doctype html public "">
<html>
<head>
<%
	String idSolicitacaoServico = UtilStrings.nullToVazio((String)request.getParameter("idSolicitacaoServico"));
	String dataHoraSolicitacao = UtilStrings.nullToVazio((String)request.getAttribute("dataHoraSolicitacao"));

%>
	<%@include file="/include/header.jsp"%>
    <%@include file="/include/security/security.jsp" %>
	<title><fmt:message key="citcorpore.comum.title"/></title>
	<%@include file="/include/menu/menuConfig.jsp" %>
	<%@include file="/include/javaScriptsComuns/javaScriptsComuns.jsp" %>

<style>
div#main_container {
	margin: 10px 10px 10px 10px;
}
</style>

</head>

<body>
<div id="wrapper">
<!-- Conteudo -->
 <div id="main_container" class="main_container container_16 clearfix">

		<div class="flat_area grid_16">
				<h2><fmt:message key="solicitacaoServico.solicitacao"/>&nbsp;N&deg; &nbsp;<%=idSolicitacaoServico%>&nbsp;-&nbsp;<fmt:message key="solicitacaoServico.dataHoraCriacao"/>&nbsp;<%=dataHoraSolicitacao%></h2>
		</div>
  <div class="box grid_16 tabs">

	 <div class="toggle_container">
						<div class="">
							<form name='form' action='${ctx}/pages/consultaSolicitacaoServico/consultaSolicitacaoServico'>
								<div class="columns clearfix">
									<input type='hidden' name='idSolicitacaoServico' id='idSolicitacaoServico'/>

									<div class="col_66">
										<div class="col_50" >
											<fieldset>
												<label style="cursor: pointer;"><fmt:message key="origemAtendimento.origem"/></label>
												<div>
													<input type='text' name="origem" readonly="readonly"/>
												</div>
											</fieldset>
										</div>
										<div class="col_50" >
											<fieldset>
												<label style="cursor: pointer;"><fmt:message key="solicitacaoServico.solicitante"/></label>
												<div>
													<input type='text' name="solicitante"  readonly="readonly"/>
												</div>
											</fieldset>
										</div>
									</div>

									<div class="col_66">
										<div class="col_50" >
											<fieldset>
												<label style="cursor: pointer;"><fmt:message key="solicitacaoServico.tipo"/></label>
												<div>
													<input type='text' name="demanda" readonly="readonly"/>
												</div>
												<label style="cursor: pointer;"><fmt:message key="servico.servico"/></label>
												<div>
													<input type='text' name="servico"  readonly="readonly"/>
												</div>
											</fieldset>
										</div>
										<div class="col_50" >
											<fieldset>
												<label><fmt:message key="solicitacaoServico.descricao"/></label>
													<div>
									       				<textarea name="descricao" cols='70' rows='5' readonly="readonly"></textarea>
													</div>
											</fieldset>
										</div>
									</div>
									<div class="col_66">
										<div class="col_50" >
											<fieldset>
												<label><fmt:message key="contrato.contrato" /></label>
													<div>
														<input type='text' name="contrato" size='50' readonly="readonly"/>
													</div>
												<label><fmt:message key="solicitacaoServico.situacao" /></label>
													<div>
														<input type='text' name="situacao" size='50' readonly="readonly"/>
													</div>
											</fieldset>
										</div>
										<div class="col_50" >
											<fieldset>
												<label><fmt:message key="solicitacaoServico.prioridade" /></label>
													<div>
														<input type='text' name="prioridade" size='10' readonly="readonly"/>
													</div>
												<label><fmt:message key="solicitacaoServico.prazoLimite" /></label>
													<div>
														<input type='text' name="dataHoraLimiteStr" size='30' readonly="readonly"/>
													</div>
											</fieldset>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- ## FIM - AREA DA APLICACAO ## -->
	 </div>
  </div>
 </div>
<!-- Fim da Pagina de Conteudo -->

<script type="text/javascript" src="js/consultaSolicitacaoServico.js"></script>

</body>

</html>
