/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/


			var objTab = null;

			addEvent(window, "load", load, false);

			function load() {
				document.form.afterRestore = function() {
					$('.tabs').tabs('select', 0);
				}
			}

			/*alterado por
			 desenvolvedor: rcs (Rafael C�sar Soyer)
			 data: 24/12/2014
			*/
			function filtrar(){
				var dataInicio = document.getElementById("dataInicio").value;
				var dataFim = document.getElementById("dataFim").value;

                if (dataInicio.trim() == ""){
                    alert(i18n_message("citcorpore.comum.validacao.datainicio"));
                    return false;
                }
                else if(DateTimeUtil.isValidDate(dataInicio) == false){
                    alert(i18n_message("citcorpore.comum.datainvalida"));
                    document.getElementById("dataInicio").value = '';
                    return false;
                }

                if (dataFim.trim() == ""){
                    alert(i18n_message("citcorpore.comum.validacao.datafim"));
                    return false;
                }
                else if(DateTimeUtil.isValidDate(dataFim) == false){
                    alert(i18n_message("citcorpore.comum.dataFinalInvalida"));
                     document.getElementById("dataFim").value = '';
                    return false;
                }

				if(validaData(dataInicio,dataFim)){
					setGantt(i18n_message("citcorpore.comum.aguardecarregando"));
					document.form.fireEvent("filtrarGantt");
				}
			}

			function setGantt(txt) {
				$('.gantt').html(txt);
			}

			function ocultarInformacao(){
				$('#informacao').hide();
			}

			/**
			* @author rodrigo.oliveira
			*/
			function validaData(dataInicio, dataFim) {
				if (typeof(locale) === "undefined") locale = '';

				var dtInicio = new Date();
				var dtFim = new Date();

				var dtInicioConvert = '';
				var dtFimConvert = '';
				var dtInicioSplit = dataInicio.split("/");
				var dtFimSplit = dataFim.split("/");

				if (locale == 'en') {
					dtInicioConvert = dtInicioSplit[2] + "/" + dtInicioSplit[0] + "/" + dtInicioSplit[1];
					dtFimConvert = dtFimSplit[2] + "/" + dtFimSplit[0] + "/" + dtFimSplit[1];
				} else {
					dtInicioConvert = dtInicioSplit[2] + "/" + dtInicioSplit[1] + "/" + dtInicioSplit[0];
					dtFimConvert = dtFimSplit[2] + "/" + dtFimSplit[1] + "/" + dtFimSplit[0];
				}

				dtInicio.setTime(Date.parse(dtInicioConvert)).setFullYear;
				dtFim.setTime(Date.parse(dtFimConvert)).setFullYear;

				if (dtInicio > dtFim){
					alert(i18n_message("citcorpore.comum.dataInicioMenorFinal"));
					return false;
				}else
					return true;
			}

			/* desenvolvedor: rcs (Rafael C�sar Soyer)
		      Limpa tudo na tela, campos preenchidos pelo usu�rio e tamb�m o que foi filtrado na tabela "gantt".
		      Chama o m�todo limpar que v�m via include do "javaScriptsComuns.jsp".
		      data: 26/12/2014
		    */
			function meulimpar(){
				limpar();
				$('.gantt').empty();
		    }

		
