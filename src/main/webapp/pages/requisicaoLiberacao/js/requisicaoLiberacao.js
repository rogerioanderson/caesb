/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
var acaoBotao = false;
var tabelaRelacionamentoICs;
var popup3;

	addEvent(window, "load", load, false);
	function load() {
		popup3 = new PopupManager(900, 450, ctx + "/pages/");
		popupMidia = new PopupManager("98%" , $(window).height()-100, ctx + "/pages/");
		document.form.afterRestore = function() {
			$('.tabs').tabs('select', 0);
			initTabelasRelacionamentos();
		}
	}
	
	   function mostraMensagemInsercao(msg){
			document.getElementById('divMensagemInsercao').innerHTML = msg;
			$("#POPUP_INFO_INSERCAO").dialog("open");
		}
	   function mostraMensagemRestaurarBaseline(msg){
			document.getElementById('divMensagemInsercaoBaseline').innerHTML = msg;
			$("#POPUP_INFO_BASELINE").dialog("open");
		}
	
	//Adiciona os dados vindos da lookup, o ultimo par�metro ele pega do fluxo , no caso o status de requisicaoliberacao
    function LOOKUP_MUDANCA_CONCLUIDA_SITUACAO_select(id, desc){
    	var n = desc.lastIndexOf(" - ");    
    	var titulo = desc.substring(0, n);
    	var status = desc.substring(n+3, n.length);

        addMudanca(id, titulo,status, $("#status option:selected").text());
        addICMudanca(id);
    }
    
    function LOOKUP_PROBLEMA_select(id, desc){
        var str = desc.split('-');
        addProblema(id, str[1], str[2]);
    }
    

    function LOOKUP_REQUISICAOCOMPRAS_select(id, desc){
    	document.getElementById("idItemRequisicaProduto").value = id;
    	document.form.fireEvent("addDadosItemProdutos");
    } 

    function LOOKUP_EMPREGADO_select(id, desc){
    	document.form.idSolicitante.value = id;
        document.form.nomeSolicitante.value = desc;
        $("#POPUP_EMPREGADO").dialog("close");
        document.form.fireEvent("carregaDadosContato");
        document.form.fireEvent("preencherComboGrupoAprovador");

	}
    
    function escondeJanelaAguarde() {
        JANELA_AGUARDE_MENU.hide();
    }
	
	function LOOKUP_LIBERACAO_select(id, desc) {
		document.form.restore({idLiberacao : id});
	}
	
	function gravar(){
		var dataInicio = document.getElementById("dataInicial").value;
		var dataFim = document.getElementById("dataFinal").value;
		var dataLiberacao = document.getElementById("dataLiberacao").value;
		var mudancas_serialize = document.getElementById('mudancas_serialize').value;
		if (dataInicio != ""){
			if(DateTimeUtil.isValidDate(dataInicio) == false){
				alert(i18n_message("citcorpore.comum.validacao.datainicio"));
			 	document.getElementById("dataInicial").value = '';
				return false;	
			}
		}
		
		if (dataFim != "") {
			if (DateTimeUtil.isValidDate(dataFim) == false) {
				alert(i18n_message("citcorpore.comum.validacao.datafim"));
				document.getElementById("dataFinal").value = '';
				return false;
			}
		}
		if (dataLiberacao != ""){
			if(DateTimeUtil.isValidDate(dataLiberacao) == false){
				alert(i18n_message("MSG03"));
			 	document.getElementById("dataLiberacao").value = '';
				return false;	
			}
		}
		
		if (document.getElementById('dataInicial').value != '' && document.getElementById('horaAgendamentoInicial').value == '') {
			alert(i18n_message("requisicaoLiberacao.informacaoHoraInicialPlanejada"));
			document.getElementById('horaAgendamentoInicial').focus();
			return;
		}
		
		if (document.getElementById('dataFinal').value != '' && document.getElementById('horaAgendamentoFinal').value == '') {
			alert(i18n_message("requisicaoLiberacao.informacaoHoraFinalPlanejada"));
			document.getElementById('horaAgendamentoFinal').focus();
			return;
		}
		
		serializa();
		
		var informacoesComplementares_serialize = '';
		try{
			informacoesComplementares_serialize = window.frames["fraInformacoesComplementares"].getObjetoSerializado();
		}catch(e){}
		document.form.informacoesComplementares_serialize.value = informacoesComplementares_serialize;
		
		
		if(validaDataParametrizado(dataInicio,dataFim)){
			serializa(); 
 			var mudancas = HTMLUtils.getObjectsByTableId('tblMudancas');
 			 if(mudancas == ""){
  				alert(i18n_message("requisicaoLiberacao.vincularMudanca"));
 				$('.tabs').tabs('select', 0);
 				document.getElementById("relacionarMudancas").focus();
 				return; 
			}else{
				parent.JANELA_AGUARDE_MENU.show();
				document.form.save();
 			 }  
		}
		

	}
	
	$(function() {
		   $('.datepicker').datepicker();
		   $('#telefoneContato').mask('(999) 9999-9999');
	  });
	
	fechar = function(){
			parent.fecharVisao();
		}	
	
	//Atividades  do Fluxo
    function gravarEFinalizar() {
		document.form.acaoFluxo.value = acaoExecutar;
		gravar();
    }
	//Atividades  do Fluxo
 	function gravarEContinuar() {
		document.form.acaoFluxo.value = acaoIniciar;
		gravar();
    }
	
	function validaData(dataInicio, dataFim) {
		if (typeof(locale) === "undefined") locale = '';
		
		var dtInicio = new Date();
		var dtFim = new Date();
		
		var dtInicioConvert = '';
		var dtFimConvert = '';
		var dtInicioSplit = dataInicio.split("/");
		var dtFimSplit = dataFim.split("/");

		if (locale == 'en') {
			dtInicioConvert = dtInicioSplit[2] + "/" + dtInicioSplit[0] + "/" + dtInicioSplit[1];
			dtFimConvert = dtFimSplit[2] + "/" + dtFimSplit[0] + "/" + dtFimSplit[1];
		} else {
			dtInicioConvert = dtInicioSplit[2] + "/" + dtInicioSplit[1] + "/" + dtInicioSplit[0];
			dtFimConvert = dtFimSplit[2] + "/" + dtFimSplit[1] + "/" + dtFimSplit[0];
		}
		
		dtInicio.setTime(Date.parse(dtInicioConvert)).setFullYear;
		dtFim.setTime(Date.parse(dtFimConvert)).setFullYear;
		
		if (dtInicio > dtFim){
			alert(i18n_message("citcorpore.comum.dataInicioMenorFinal"));
			return false;
		}else
			return true;
	}

	function addProblema(id, desc, stat){
        var obj = new CIT_ProblemaDTO();
        document.getElementById('problema#idProblema').value = id;
        document.getElementById('problema#titulo').value = desc;
        document.getElementById('problema#status').value = stat;
        HTMLUtils.addRow('tblProblema', document.form, 'problema', obj,
                ["","idProblema","titulo","status"], ["idProblema"], "Problema j� adicionado", [gerarImgDelProblema], null, null, false);
        
        
		$("#POPUP_PROBLEMA").dialog("close");
	}
	
	//geber.costa
	var teste = '';
	function alterarFechamentoMudanca(id){
		$("#POPUP_MUDANCA_FECHAMENTO_CATEGORIA").dialog("open");
		if(acaoBotao == false){
			 //document.form.fireEvent("divFinalizacaoCategoria");
			 $('#linha').val(id);		
	        $("#POPUP_MUDANCA").dialog("close");
		}
	}
	
	/* function deleteLinha(table, index){
		if (confirm(i18n_message("citcorpore.comum.confirmaExclusao"))) {
		teste = 'true';
		HTMLUtils.deleteRow(table, index);
		return;
		}
		teste = '';
	} */
	function deleteLinha(table, index){
		acaoBotao = true;
		if (confirm(i18n_message("citcorpore.comum.confirmaExclusao"))) {
			teste = 'true';
			HTMLUtils.deleteRow(table, index);
			return;
		}
		teste = '';
	}
	
	 function gerarImgDelProblema(row, obj){
	        row.cells[0].innerHTML = '<img src="' + ctx + '/imagens/delete.png" style="cursor: pointer;" onclick="excluirProblema(\''+row.id+'\')"/>';
	    };
	    
    function gerarImgDel(row, obj){
        row.cells[0].innerHTML = '<img src="' + ctx + '/imagens/delete.png" style="cursor: pointer;" onclick="deleteLinha(\'tblMudancas\', this.parentNode.parentNode.rowIndex);"/>';
    };
    
    function gerarImgDelIc(row, obj){
        row.cells[0].innerHTML = '<img src="' + ctx + '/imagens/delete.png" style="cursor: pointer;" onclick="deleteLinha(\'tblICs\', this.parentNode.parentNode.rowIndex);"/>';
    };
    
    function gerarImgDelResponsavel(row, obj){
        row.cells[0].innerHTML = '<img src="<' + ctx + '/imagens/delete.png" style="cursor: pointer;" onclick="deleteLinha(\'tblResponsavel\', this.parentNode.parentNode.rowIndex);"/>';
    };
    
    function excluirMudanca(linha) {
        if (linha > 0 && confirm(i18n_message("citcorpore.comum.confirmaExclusao"))) {
            HTMLUtils.deleteRow('tblMudancas', linha);
        }
    }
    
    function excluirProblema(idLinhaProblema) {
        if (idLinhaProblema && confirm(i18n_message("citcorpore.comum.confirmaExclusao"))) {
            var row = document.getElementById(idLinhaProblema);
            row.parentNode.removeChild(row);
        }
    }
  
 	function serializa(){
        var mudancas = HTMLUtils.getObjectsByTableId('tblMudancas');
		document.form.mudancas_serialize.value =  ObjectUtils.serializeObjects(mudancas);
		var problemas = HTMLUtils.getObjectsByTableId('tblProblema');
		document.form.problemas_serialize.value =  ObjectUtils.serializeObjects(problemas);
		
		var midias = HTMLUtils.getObjectsByTableId('tblMidia');
		document.form.midias_serialize.value =  ObjectUtils.serializeObjects(midias);
		
		var responsavel = HTMLUtils.getObjectsByTableId('tblResponsavel');
		document.form.responsavel_serialize.value =  ObjectUtils.serializeObjects(responsavel);
		
		var requisicaoCompra = HTMLUtils.getObjectsByTableId('tblRequisicaoCompra');
		document.form.requisicaoCompras_serialize.value =  ObjectUtils.serializeObjects(requisicaoCompra);
	
		var objIc = HTMLUtils.getObjectsByTableId('tblICs');
		document.form.itensConfiguracaoRelacionadosSerializado.value = ObjectUtils.serializeObjects(objIc);
 	}

 	$(function() {
		$("#POPUP_EMPREGADO").dialog({
			autoOpen : false,
			width : 600,
			height : 530,
			modal : true,
			show: "fade",
			hide: "fade"
		});
	});
	
	$(function() {
		$("#POPUP_MUDANCA").dialog({
			autoOpen : false,
			width : 600,
			height : 400,
			modal : true,
			show: "fade",
			hide: "fade"
		});
		


		$("#POPUP_PROBLEMA").dialog({
			autoOpen : false,
			width : 600,
			height : 400,
			modal : true,
			show: "fade",
			hide: "fade"
		});		
		
		$("#POPUPITEMCONFIGURACAO").dialog({
			autoOpen : false,
			width : 600,
			height : 400,
			modal : true,
			show: "fade",
			hide: "fade"
		});
		
		$("#POPUP_MIDIA").dialog({
			autoOpen : false,
			width : 600,
			height : 400,
			modal : true,
			show: "fade",
			hide: "fade"
		});	
		
		//geber.costa
		$("#POPUP_MUDANCA_FECHAMENTO_CATEGORIA").dialog({
			autoOpen : false,
			width : 320,
			height : 200,
			modal : true,
			show: "fade",
			hide: "fade"
		});	
		
		//geber.costa
		$("#POPUP_MUDANCA_CADASTRAR_SITUACAO").dialog({
			autoOpen : false,
			width : 600,
			height : 400,
			modal : true,
			show: "fade",
			hide: "fade"
		});
		
			
		$("#POPUP_RESPONSAVEL").dialog({
			autoOpen : false,
			width : 850,
			height : 500,
			modal : true,
			show: "fade",
			hide: "fade"
		});	
		
		$("#POPUP_INFO_INSERCAO").dialog({
			autoOpen : false,
			width : 400,
			height : 280,
			modal : true,
			close: function(event, ui) {
				fechar();
			}
		});	
		$("#POPUP_INFO_BASELINE").dialog({
			autoOpen : false,
			width : 400,
			height : 280,
			modal : true,
			close: function(event, ui) {
			}
		});	
		
			$("#POPUP_REQUISICAOCOMPRAS").dialog({
			autoOpen : false,
			width : 910,
			height : 600,
			modal : true,
			show: "fade",
			hide: "fade"
		}); 
		    
	    $("#POPUP_PLANOSREVERSAO").dialog({
			autoOpen : false,
			width : 910,
			height : 600,
			modal : true,
			show: "fade",
			hide: "fade",
			open: function() {
				  	  $('#POPUP_PLANOSREVERSAO').find('#fraUpload_uploadPlanoDeReversaoLiberacao').attr("src", ctx + "/pages/uploadPlanoDeReversaoLiberacao/uploadPlanoDeReversaoLiberacao.load?editar=" + editar);
				  },
			buttons: [{
				 text: i18n_message('citcorpore.comum.gravar'),
				 click: function() {
		          gravarAnexosPlanosReversao();
		          $( this ).dialog( "close" );
			        }
				}]
		});
			
		$("#POPUP_PESQUISAITEMCONFIGURACAO").dialog({
				autoOpen : false,
				width : 1200,
				height : 650,
				modal : true
		});

		$("#nomeSolicitante").click(function() {
			if (document.form.idContrato.value == ''){
				alert('Informe o contrato!');
				return;
			}
			
			$("#POPUP_EMPREGADO").dialog("open");
		});
		initTabelasRelacionamentos();
	});

function adicionarMudanca() {
		$("#POPUP_MUDANCA").dialog("open");
	};
	
	function limpar() {
        HTMLUtils.deleteAllRows('tblMudancas');
		document.form.clear();
		//fechar();
	}

	function adicionarProblema(){
		$("#POPUP_PROBLEMA").dialog("open");		
	}

    function excluir() {
        if (confirm(i18n_message("citcorpore.comum.confirmaExclusao"))) 
            document.form.fireEvent('delete');
    }
    function mostrarEscondeRegExec(){
		if (document.getElementById('divMostraRegistroExecucao').style.display == 'none'){
			document.getElementById('divMostraRegistroExecucao').style.display = 'block';
			document.getElementById('lblMsgregistroexecucao').style.display = 'block';
			document.getElementById('btnAddRegExec').innerHTML = i18n_message("solicitacaoServico.addregistroexecucao_menos");
		}else{
			document.getElementById('divMostraRegistroExecucao').style.display = 'none';
			document.getElementById('lblMsgregistroexecucao').style.display = 'none';
			document.getElementById('btnAddRegExec').innerHTML = i18n_message("solicitacaoServico.addregistroexecucao_mais");
		}
	}
    
	/** INFLUENCIA PRIORIDADE */
	function atualizaPrioridade(){
		
		var impacto = document.getElementById('nivelImpacto').value;
		var urgencia = document.getElementById('nivelUrgencia').value;
		
		if (urgencia == "B"){
			if (impacto == "B"){
				document.form.prioridade.value = 5;
			}else if (impacto == "M"){
				document.form.prioridade.value = 4;
			}else if (impacto == "A"){
				document.form.prioridade.value = 3;
			}
		}
		
		
		if (urgencia == "M"){
			if (impacto == "B"){
				document.form.prioridade.value = 4;
			}else if (impacto == "M"){
				document.form.prioridade.value = 3;
			}else if (impacto == "A"){
				document.form.prioridade.value = 2;
			}
		}
		
		if (urgencia == "A"){
			if (impacto == "B"){
				document.form.prioridade.value = 3;
			}else if (impacto == "M"){
				document.form.prioridade.value = 2;
			}else if (impacto == "A"){
				document.form.prioridade.value = 1;
			}
		}
	}
    
	/** INFLUENCIA PRIORIDADE */
	function atualizaPrioridade(){
		
		var impacto = document.getElementById('nivelImpacto').value;
		var urgencia = document.getElementById('nivelUrgencia').value;
		
		if (urgencia == "B"){
			if (impacto == "B"){
				document.form.prioridade.value = 5;
			}else if (impacto == "M"){
				document.form.prioridade.value = 4;
			}else if (impacto == "A"){
				document.form.prioridade.value = 3;
			}
		}
		
		
		if (urgencia == "M"){
			if (impacto == "B"){
				document.form.prioridade.value = 4;
			}else if (impacto == "M"){
				document.form.prioridade.value = 3;
			}else if (impacto == "A"){
				document.form.prioridade.value = 2;
			}
		}
		
		if (urgencia == "A"){
			if (impacto == "B"){
				document.form.prioridade.value = 3;
			}else if (impacto == "M"){
				document.form.prioridade.value = 2;
			}else if (impacto == "A"){
				document.form.prioridade.value = 1;
			}
		}
	}
    
    function adicionarIC(){			
		abrePopupIcs();
	}
    
    function abrePopupIcs(){
		$("#POPUPITEMCONFIGURACAO").dialog("open");
	}
    
	function initTabelasRelacionamentos(){
		
		//Solicitacaoservico
		tabelaRelacionamentoSolicitacaoServico = new CITTable("tblSolicitacaoServico",["idSolicitacaoServico", "nomeServico"],[]);
		tabelaRelacionamentoSolicitacaoServico.setInsereBotaoExcluir(true, ctx + "/imagens/delete.png");

		//servicos
		tabelaRelacionamentoServicos = new CITTable("tblServicos",["idServico", "Nome", "Mapa", "Descri��o"],[]);
		tabelaProblema = new CITTable("tblProblema",["idProblema", "titulo", "status"],[]);
		tabelaRelacionamentoServicos.setInsereBotaoExcluir(true, ctx + "/imagens/delete.png");
		tabelaProblema.setInsereBotaoExcluir(true, ctx + "/imagens/delete.png");
		
		//Papeis e Responsabilidades
		tabelaRelacionamentoServicos = new CITTable("tblResponsavel", ["idResponsavel", "nomeResponsavel",  "papelResponsavel", "telResponsavel", "emailResponsavel"], []);
		tabelaRelacionamentoServicos.setInsereBotaoExcluir(true, ctx + "/imagens/delete.png");

		
		
		//Requisicao de Compras
		tabelaRelacionamentoServicos = new CITTable("tblRequisicaoCompra", ["idSolicitacaoServico",  "nomeServico", "situacaoServicos"], []);
		tabelaRelacionamentoSolicitacaoServico.setInsereBotaoExcluir(true, ctx + "/imagens/delete.png");

	}
	
	function limpaListasRelacionamentos(){
		tabelaRelacionamentoICs.limpaLista();
		tabelaRelacionamentoServicos.limpaLista();
		tabelaProblema.limpaLista();
		tabelaRelacionamentoSolicitacaoServico.limpaLista();
		
	}
	
	 function CITTable(_idCITTable, _fields, _tableObjects){
			var self = this;
			var idCITTable = _idCITTable;
			var fields = _fields;
			var tableObjects = _tableObjects;
			var tabela = null;
			
			var insereBtExcluir = true;
			var imgBotaoExcluir;

			this.onDeleteRow = function(deletedItem){};

			this.getTableList = function(){
				return tableObjects;
			}

			/**
			 * Transforma a lista da tabela em uma lista de objetos
			 * de acordo com o 'fields' passado.
			 */
			this.getTableObjects = function(){
				var objects = [];
				var object = {};

				for(var j = 0; j < tableObjects.length; j++){
					for(var i = 0 ; i < fields.length; i++){
						eval("object." + fields[i] + " = '" + tableObjects[j][i] + "'");
					}
					objects.push(object);
					object = {};
				}
				
				return objects;
			}

			this.setTableObjects = function(objects){
				tableObjects = objects;
				this.montaTabela();
			}
			
			this.addObject = function(object){
				tableObjects.push(object);
				this.montaTabela();
			}

			this.limpaLista = function(){
				tableObjects.length = 0;
				tableObjects = null;
				tableObjects = [];
				limpaTabela();
			}
			
			var limpaTabela = function(){
				while (getTabela().rows.length > 1){
					getTabela().deleteRow(1); 
				}
			}
			
			this.montaTabela = function(){
				var linha;
				var celula;
				
				limpaTabela();

				for(var i = tableObjects.length - 1; i >= 0; i--){

					var j = 0;					
					linha = getTabela().insertRow(1);

					for(j = 0; j < fields.length; j++){
						celula = linha.insertCell(j);

						//tratamento caso seja um componente ao inv�s de texto
						try{
							celula.appendChild(tableObjects[i][j]);
						}catch(e){
							celula.innerHTML = tableObjects[i][j];
						}							
					}					

					if(insereBtExcluir){
						var btAux = getCopiaBotaoExcluir();
						var celExcluir = linha.insertCell(j);
						
						btAux.setAttribute("id", i);
						btAux.addEventListener("click", function(evt){
							//ao disparar o evento, considerar� o id do bot�o
							self.removeObject(this.id);
							this.onDeleteRow(this);
							
						}, false);							
						celExcluir.appendChild(btAux);
					}
				}
			}

			this.removeObject = function(indice){
				removeObjectDaLista(indice);
				this.montaTabela();
			}
		
			/**
			 * Remove item e organiza lista
			 */
			var removeObjectDaLista = function(indice){
				tableObjects[indice] = null;
				var novaLista = [];
				for(var i = 0 ; i < tableObjects.length; i++){
					if(tableObjects[i] != null){
						novaLista.push(tableObjects[i]);
					}
				}
				tableObjects = novaLista;
			}

			var getCopiaBotaoExcluir = function(){
				var novoBotao = new Image();
				novoBotao.setAttribute("style", "cursor: pointer;");
				novoBotao.src = imgBotaoExcluir;
				return novoBotao;
			}

			var setImgPathBotaoExcluir = function(src){
				imgBotaoExcluir = src;
			}

			var getTabela = function(){
				if(tabela == null){
					tabela = document.getElementById(idCITTable);
				}
				return tabela;
			}

			this.setInsereBotaoExcluir = function(bool, imgSrc){
				insereBtExcluir = bool;
				setImgPathBotaoExcluir(imgSrc);
			}
		}

		/*Gravar baseline*/
		function gravarBaseline() {
			var tabela = document.getElementById('tblBaselines');
			var count = tabela.rows.length;
			var contadorAux = 0;
			var baselines = new Array();

			for ( var i = 1; i <= count; i++) {
				var trObj = document.getElementById('idHistoricoMudanca' + i);	
				if (!trObj) {
					continue;
				}	
				baselines[contadorAux] = getbaseline(i);
				contadorAux = contadorAux + 1;
			}
			serializaBaseline();
			document.form.fireEvent("saveBaseline");
		}
		
		var seqBaseline = '';
		var aux = '';
		serializaBaseline = function() {
			var tabela = document.getElementById('tblBaselines');
			var count = tabela.rows.length;
			var contadorAux = 0;
			var baselines = new Array();
			for ( var i = 1; i <= count; i++) {
				var trObj = document.getElementById('idHistoricoMudanca' + i);
				if (!trObj) {
					continue;
				}else if(trObj.checked){
					baselines[contadorAux] = getbaseline(i);
					contadorAux = contadorAux + 1;
					continue;
				}	
				
			}
		var baselinesSerializadas = ObjectUtils.serializeObjects(baselines);
			document.form.baselinesSerializadas.value = baselinesSerializadas;
			return true;
		}

		getbaseline = function(seq) {
			var HistoricoMudancaDTO = new CIT_HistoricoMudancaDTO();
			HistoricoMudancaDTO.sequencia = seq;
			HistoricoMudancaDTO.idHistoricoMudanca = eval('document.form.idHistoricoMudanca' + seq + '.value');
			return HistoricoMudancaDTO;
		}
		function marcarCheckbox(elementos){
			var arrayIds = new Array();
			arrayIds = elementos;
			for ( var i = 0; i <= arrayIds.length; i++) {
				var posicao = arrayIds[i];
				$("#posicao").attr("checked",true);
			}
			
		}
		
		function restaurarHistorico(id){
			document.form.idHistoricoMudanca.value = id;
			if(confirm(i18n_message("itemConfiguracaoTree.restaurarVersao")))
				document.form.fireEvent("restaurarBaseline");
		}
		
		function LOOKUP_ITEMCONFIGURACAO_select(id, desc) {
			document.form.nomeItemConfig.value = desc;
			document.form.idItemConfig.value = id;
			document.form.idItemConfig.disabled=false;
			document.form.fireEvent('addICs');
		}
		
		exibeIconesIC = function(row, obj) {
			var id = obj.idItemConfiguracao;
			//if(obj.idItemConfiguracaoPai == ""){
				row.cells[4].innerHTML = '<img src="'+ctx+'/template_new/images/icons/small/grey/graph.png" border="0" onclick="popupAtivos( '+ id + ')" style="cursor:pointer"/>';
			//}
		}
		
		function inventario() {
			document.getElementById('iframeAtivos').src = ctx + '/pages/inventarioNew/inventarioNew.load?iframe=' + true;
			$("#POPUP_ATIVOS").dialog("open");
		}
		
		function popupAtivos(idItem){
			document.getElementById('iframeAtivos').src =ctx + '/pages/informacaoItemConfiguracao/informacaoItemConfiguracao.load?id=' + idItem;
			$("#POPUP_ATIVOS").dialog("open");
		}
		
		function fechaPopupAtivos(){
			
			setTimeout($('#POPUP_ATIVOS').dialog('close'),1000);
		}
		
		$(function() {
			$("#POPUP_ATIVOS").dialog({
				autoOpen : false,
				width : 1005,
				height : 565,
				modal : true
			});
		});
		
		function limpaListasRelacionamentos(){
			tabelaRelacionamentoICs.limpaLista();
		}
		
		function setaValorLookup(obj){
			document.getElementById('pesqLockupLOOKUP_EMPREGADO_IDCONTRATO').value = '';
			document.getElementById('pesqLockupLOOKUP_EMPREGADO_IDCONTRATO').value = obj.value; 
		}
		
		//MIDIA SOFTWARE
		function adicionarMidia(){
			$("#POPUP_MIDIA").dialog("open");		
		}
		
	    function LOOKUP_MIDIASOFTWARE_select(id, desc){
	    	  document.getElementById('midia#idMidiaSoftware').value = id;
	    	  document.form.fireEvent("restaurarMidiaSoftware");
	    }

		exibeIconesMidia = function(row, obj){
			var id = obj.idMidiaSoftware;
	        obj.sequenciaLiberacao = row.rowIndex; 
		    row.cells[0].innerHTML = '<img src="' + ctx + '/imagens/delete.png" border="0" onclick="excluiMidia('+ row.rowIndex + ',this)" style="cursor:pointer"/>';
		}
		
		excluiMidia = function(indice) {
			if (indice > 0 && confirm('Confirma exclus�o?')) {
				HTMLUtils.deleteRow('tblMidia', indice);
			}
		}
		
		function fecharMidia(){
			$("#POPUP_MIDIA").dialog("close");
		}
	    
		
	    //Questionario
		    function carregaQuestionario(idTipoAba){
		    	document.form.idTipoAba.value = idTipoAba; 
		    	document.form.fireEvent("carregaQuestionario");
		    }
	    
	    	/**
			 *Alterado pois a rotina usando citTable nao estava funcionando.
			 * 
			 * @author maycon.fernandes
			 * 31/10/2013 18:47
			 */	
	    
	     function LOOKUP_RESPONSAVEL_select(id, desc){
	    	 document.getElementById('responsavel#idResponsavel').value = id;
	    	 addResponsavel();
	     }
	    
	    function adicionarResponsavel(){
			$("#POPUP_RESPONSAVEL").dialog("open");		
		}

		exibeIconesResponsavel = function(row, obj){
			var id = obj.idResponsavel;
	        obj.sequenciaLiberacao = row.rowIndex; 
		    row.cells[0].innerHTML = '<img src="' + ctx + '/imagens/delete.png" border="0" onclick="excluiResponsavel('+ row.rowIndex + ',this)" style="cursor:pointer"/>';
		}
		
		excluiResponsavel = function(indice) {
			if (indice > 0 && confirm('Confirma exclus�o?')) {
				HTMLUtils.deleteRow('tblResponsavel', indice);
			}
		}
		
		function fecharResponsavel(){
			$("#POPUP_RESPONSAVEL").dialog("close");
		} 
	     	    
	    function addResponsavel(){
	        var papel =  prompt(i18n_message("citcorpore.comum.papel"),"");
	        if(papel.length <= 180){
	        	document.getElementById('responsavel#papelResponsavel').value = papel;
	        }else{
	        	alert(i18n_message("requisicaoLiberacao.limiteCaracter"));
	        	return;
	        } 
	        
	        document.form.fireEvent('restaurarResponsavel');
		}
	    
	    function excluirResponsavel(linha) {
	        if (linha > 0 && confirm(i18n_message("citcorpore.comum.confirmaExclusao"))) {
	            HTMLUtils.deleteRow('tblResponsavel', linha);
	        }
	    }
	    
	    function adicionarResponsavel(){
			$("#POPUP_RESPONSAVEL").dialog("open");		
		}
	    

	    
	    //add Requisicao de Compras
	   function addRequisicaoCompras(id, nomeservico, situacaoservico ){
	        var obj = new CIT_RequisicaoLiberacaoRequisicaoComprasDTO();
      		
	        
	        document.getElementById('requisicaoCompras#idSolicitacaoServico').value = id;
	        document.getElementById('requisicaoCompras#nomeServico').value = nomeservico;
	        document.getElementById('requisicaoCompras#situacaoServicos').value = situacaoservico;  
	        
	        HTMLUtils.addRow('tblRequisicaoCompra', document.form, 'requisicaoCompras', obj, ["","idSolicitacaoServico", "nomeServico", "situacaoServicos"], ["idSolicitacaoServico"], null, [gerarImgDelRequisicaoCompras], null, null, false);
	    	$("#POPUP_REQUISICAOCOMPRAS").dialog("close");
		}
	    
	    function adicionarRequisicaoCompras(){
	    	$("#POPUP_REQUISICAOCOMPRAS").dialog("open");
	    }
	    
	    function excluirRequisicaoCompras(linha) {
	        if (linha > 0 && confirm(i18n_message("citcorpore.comum.confirmaExclusao"))) {
	            HTMLUtils.deleteRow('tblRequisicaoCompra', linha);
	        }
	    };
	    
	    function gerarImgDelRequisicaoCompras(row, obj){
	        row.cells[0].innerHTML = '<img src="' + ctx + '/imagens/delete.png" style="cursor: pointer;"  onclick="deleteLinha(\'tblRequisicaoCompra\', this.parentNode.parentNode.rowIndex);"/>';
	    };
	    
	  //geber.costa
	    function gerarImgpopup(row, obj){
	    row.cells[5].innerHTML = '<img src="' + ctx + '/imagens/edit.png" style="cursor: pointer;" onclick="alterarFechamentoMudanca('+row.rowIndex+')"/>';
	    };
	  //murilo.pacheco
	    function geraImgPlanoReversao(row, obj){
	    row.cells[6].innerHTML = '<img src="' + ctx + '/template_new/images/icons/small/grey/paperclip.png" style="cursor: pointer;" onclick="listarPlanosDeReversao('+obj.idRequisicaoMudanca+')"/>';
	    };
	    
	    
	    /**
	    	INFORMA��ES COMPLEMENTARES (TEMPLATE/QUESTIONARIO)
	    **/
	    function exibirInformacoesComplementares(url) {
            if (url != '') {
                /* JANELA_AGUARDE_MENU.show(); */
                document.getElementById('fraInformacoesComplementares').src = ctx+url;
                
            }else{
                try{
                	escondeJanelaAguarde();
                }catch (e) {
                }       
                document.getElementById('divInformacoesComplementares').style.display = 'none';
            } 
        }
	    
	    function addICMudanca(id){
	    	document.form.idICMudanca.value = id;
	    	document.form.fireEvent("relacionaICMudanca");
	    }
	    
	    //geber.costa
	    var fechamento = "";
	    function setarFechamentoCategoria(){
	    	fechamento = document.getElementById('idFechamentoCategoria').value;
	    	if(fechamento==""){
	    		alert(i18n_message("situacaoLiberacaoMudanca.selecione"));
	    		return;
	    	}
	    	 var obj = HTMLUtils.getObjectByTableIndex('tblMudancas', document.getElementById('rowIndex').value);
		        document.getElementById('mudanca#idRequisicaoMudanca').value = obj.idRequisicaoMudanca;
	              document.getElementById('mudanca#titulo').value = obj.titulo;
	              document.getElementById('mudanca#status').value =  obj.status;
	              document.getElementById('mudanca#situacaoLiberacao').value =  fechamento;
		        HTMLUtils.updateRow('tblMudancas', document.form, 'mudanca', obj, ["", "idRequisicaoMudanca", "titulo","status","situacaoLiberacao","",""], null, '', [gerarImgDel, gerarImgpopup, geraImgPlanoReversao], funcaoClickRowMudanca, null, document.getElementById('rowIndex').value, false);

	    	$("#POPUP_MUDANCA_FECHAMENTO_CATEGORIA").dialog("close");

	    }

	     //geber.costa
	     //adiciona uma linha na tabela de mudan�as
	     function funcaoClickRowMudanca(row, obj){
		    	if(row == null){
		            document.getElementById('rowIndex').value = null;
		            document.form.clear();
		        }else{
		        	document.getElementById('rowIndex').value = row.rowIndex;
		        	
		        	document.getElementById('idFechamentoCategoria').value =  obj.situacaoLiberacao;
		        	/* if(teste == ""){
		            $("#POPUP_MUDANCA_FECHAMENTO_CATEGORIA").dialog("open");
		        	}
		        	teste = '';
 */
		        }
		    }
	     
	     addMudanca = function(id, desc, stat, fecha) {

	        	 var obj = new CIT_RequisicaoMudancaDTO();
	        	  document.getElementById('mudanca#idRequisicaoMudanca').value = id;
	              document.getElementById('mudanca#titulo').value = desc;
	              document.getElementById('mudanca#status').value = stat;
	              document.getElementById('mudanca#situacaoLiberacao').value = fecha;
	            HTMLUtils.addRow('tblMudancas', document.form, 'mudanca', obj,["", "idRequisicaoMudanca", "titulo","status","situacaoLiberacao","", ""], ["idRequisicaoMudanca"], "Mudan�a j� adicionada", [gerarImgDel, gerarImgpopup, geraImgPlanoReversao], funcaoClickRowMudanca, null, false);
	            $("#POPUP_MUDANCA").dialog("close");
	             novoItem(); 
	        HTMLUtils.applyStyleClassInAllCells('tblMudancas', 'celulaGrid');
		}
	     
	   
	     
	     function novoItem(){
		 		document.form.rowIndex.value = "";
		    }
	     function informaNumeroSolicitacao(numero){
				document.getElementById('divTituloLiberacao').innerHTML = 
					'<h2>' + i18n_message("requisicaoLiberacao.requisicaoLiberacaoNumero") + '&nbsp;' + numero + '</h2>';   
			}
	     function gravarAnexosPlanosReversao(){
		    document.form.fireEvent("gravarAnexosPlanosReversao");
		 }

	     
	     function listarPlanosDeReversao(idMudanca){
	    	acaoBotao = true;
	    	document.form.idMudanca.value = idMudanca;
	    	document.formUploadReversaoLiberacao.idMudanca.value = idMudanca;
	    	document.form.fireEvent("listarPlanos"); 
	     }
	     
		// Manipulador de evento para o Campo dataLiberacao.
		// Se A requisi��o for macada como resolvida o campo dataLiberacao passa a ser obrigatorio. 	     
	     function verificaDataLiberacao(status){
	    	 var teste = status.value;
				if( teste == "Resolvida"){
					$('#dtLiberacao').addClass('campoObrigatorio');
					$('#dataLiberacao').addClass("Format[Data] datepicker Valid[Required] Description[liberacao.dataLiberacao]");
				}else{
					$('#dtLiberacao').removeClass('campoObrigatorio');
					$('#dataLiberacao').removeClass("Format[Data] datepicker Valid[Required] Description[liberacao.dataLiberacao]");
					
				}
	     }
			/* Desenvolvedor: Thiago Matias - Data: 27/11/2013 - Hor�rio: 16:00 - ID Citsmart: 125329 - 
				* Motivo/Coment�rio: foi alterado o modo de pesquisa dos itens de configura��o para vincula��o*/
	     function abrirModalPesquisaItemConfiguracao(){

				var h;
				var w;
				w = parseInt($(window).width() * 0.75);
				h = parseInt($(window).height() * 0.85);
				
				document.getElementById('framePesquisaItemConfiguracao').src = ctx + "/pages/pesquisaItemConfiguracao/pesquisaItemConfiguracao.load?iframe=true";
			
				$("#POPUP_PESQUISAITEMCONFIGURACAO").dialog("open");
			}
	     
	     function selectedItemConfiguracao(id){
				idItemConfiguracao = id;
				document.getElementById('hiddenIdItemConfiguracao').value = id;
				var tbl = document.getElementById('tblICs');
				tbl.style.display = 'block';
				var lastRow = tbl.rows.length;
				var resultado = true;
				resultado = validaAddLinhaTabelaItemConfiguracao(lastRow, id);
				
				document.form.idItemConfig.value = id;
				document.form.idItemConfig.disabled=false;
				document.form.fireEvent('addICs');
				
			}

	 	function validaAddLinhaTabelaItemConfiguracao(lastRow, id){
			var listaICs = ObjectUtils.deserializeCollectionFromString(document.form.itensConfiguracaoRelacionadosSerializado.value);
			
			if (lastRow > 1){
				for(var i = 0; i < listaICs.length; i++){
					if (listaICs[i].idItemConfiguracao == id){
						alert(i18n_message("citcorpore.comum.registroJaAdicionado"));
						return false;
					}
				}
			} 
			return true;
		}

		function imprimirCadastroPdf(){
			JANELA_AGUARDE_MENU.show();
			document.form.fireEvent('imprimirCadastroPdf');
		}
