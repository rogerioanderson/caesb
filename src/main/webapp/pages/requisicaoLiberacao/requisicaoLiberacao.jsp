<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citajax.html.DocumentHTML"%>
<%@page import="br.com.centralit.citcorpore.bean.RequisicaoLiberacaoDTO"%>
<%@page import="br.com.centralit.bpm.util.Enumerados"%>
<%@page import="br.com.centralit.citcorpore.util.ParametroUtil"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>
<%@page import="br.com.centralit.citcorpore.util.Enumerados.SituacaoSolicitacaoServico"%>
<%@page import="br.com.centralit.bpm.util.Enumerados.SituacaoItemTrabalho"%>
<%@page import="br.com.centralit.citcorpore.util.Enumerados.ParametroSistema"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="br.com.centralit.citcorpore.bean.HistoricoLiberacaoDTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collection"%>
<%@page import="br.com.citframework.util.UtilHTML"%>
<%@page import="br.com.centralit.citcorpore.util.Enumerados.Aba"%>

<% 
/**
* Motivo: Verifica se a tela � de visualiza��o
* Autor: luiz.borges	
* Data/Hora: 10/12/2013 15:41
*/
	String display = "";
	String editar = "false";
	if(request.getParameter("editar") != null && request.getParameter("editar").toString().equalsIgnoreCase("N")){
		display = "display:none";
		editar = "true";
		request.getSession().setAttribute("disable", "true");
	}else if (request.getParameter("editar") != null && request.getParameter("editar").toString().equalsIgnoreCase("RO")){
		display = "display:none";
        editar = "false";
        request.getSession().setAttribute("disable", "true");
	}else{
		request.getSession().setAttribute("disable", "false");
	}
	
	pageContext.setAttribute("display", display);
	pageContext.setAttribute("editar", editar);
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);
    String iframe = "";
    iframe = request.getParameter("iframe");
    
    String descStatus;
    String tarefaAssociada = (String)request.getAttribute("tarefaAssociada");
    if (tarefaAssociada == null){
    	tarefaAssociada = "N";
    }
    pageContext.setAttribute("acaoExecutar", Enumerados.ACAO_EXECUTAR);
    pageContext.setAttribute("acaoIniciar", Enumerados.ACAO_INICIAR);
    
%>
<%@include file="/include/security/security.jsp"%>
<%@include file="/include/menu/menuConfig.jsp"%>
<%@include file="/include/header.jsp"%>
<%@include  file="/include/javaScriptsComuns/javaScriptsComuns.jsp"%>
<script charset="ISO-8859-1"  type="text/javascript" src="${ctx}/js/PopupManager.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/RequisicaoMudancaDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/HistoricoLiberacaoDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/ProblemaDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/RequisicaoLiberacaoResponsavelDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/RequisicaoLiberacaoRequisicaoComprasDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/RequisicaoLiberacaoMidiaDTO.js"></script>
<script  charset="ISO-8859-1" type="text/javascript" src="${ctx}/js/PopupManager.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/HistoricoMudancaDTO.js"></script> 
<title><fmt:message key="citcorpore.comum.title" /></title>
<link rel="stylesheet" type="text/css" href="${ctx}/pages/requisicaoLiberacao/css/requisicaoLiberacao.css">

<script type="text/javascript">
		var editar = "${editar}";
		var display = "${display}";
        var acaoIniciar = "${acaoIniciar}";
        var acaoExecutar = "${acaoExecutar}";
</script>

<script type="text/javascript" src="${ctx}/pages/requisicaoLiberacao/js/requisicaoLiberacao.js"></script>

<%  //se for chamado por iframe deixa apenas a parte de cadastro da página

if (iframe != null) {
	
%>

<style>
div#main_container {
	margin: 10px 10px 10px 10px;
}
</style>

<%  } %>
   
</head>
<cit:janelaAguarde id="JANELA_AGUARDE_MENU"  title="" style="display:none;top:325px;width:300px;left:500px;height:50px;position:absolute;">
</cit:janelaAguarde>
<div id="divBarraFerramentas" style='position:absolute; top: 0px; left: 500px; z-index: 1000'>
		<jsp:include page="/pages/barraFerramentasLiberacoes/barraFerramentasLiberacoes.jsp"></jsp:include>
	</div>
<body>
						
					<div 	 id="wrapper" class="wrapper">
					<div id="main_container" class="main_container container_16 clearfix" style='margin: 10px 10px 10px 10px'>
					<div id='divTituloLiberacao' class="flat_area grid_16" style="text-align:right;">
									<h2><fmt:message key="requisicaoLiberacao.requisicaoLiberacao" /></h2>
					</div>
					<div class="box grid_16 tabs">
						<div class="toggle_container">
							<div class="section">
							<form class='formReqLiberacao' name='form' action='${ctx}/pages/requisicaoLiberacao/requisicaoLiberacao'>
								<div class="section">
                                    <input type='hidden' name='idSolicitante' id='idSolicitante' />
                                    <input type='hidden' name='idItemRequisicaProduto' id='idItemRequisicaProduto' />
                                    <input type='hidden' name='idContatoRequisicaoLiberacao' id='idContatoRequisicaoLiberacao' />
                                    <input type='hidden' name='mudanca#idRequisicaoMudanca' id='mudanca#idRequisicaoMudanca' />
                                    <input type='hidden' name='mudanca#titulo' id='mudanca#titulo' />
                                    <input type='hidden' name='mudanca#status' id='mudanca#status' /> 
                                    <input type='hidden' name='mudanca#situacaoLiberacao' id='mudanca#situacaoLiberacao' /> 
									<input type="hidden" name="mudancas_serialize" id="mudancas_serialize" />
									<input type="hidden" name="idMudanca"  id="idMudanca" />
									<input type='hidden' name='problema#idProblema' id='problema#idProblema' />
                                    <input type='hidden' name='problema#titulo' id='problema#titulo' />
                                    <input type='hidden' name='problema#status' id='problema#status' />
									<input type="hidden" name="problemas_serialize" id="problemas_serialize" />
									
									<input type='hidden' name='acaoFluxo' id='acaoFluxo' />
									<input type='hidden' name='idRequisicaoLiberacao' id='idRequisicaoLiberacao' />
									<input type='hidden' name='alterarSituacao' id='alterarSituacao' />
									<input type='hidden' name='idTarefa' id='idTarefa' />
									<input type='hidden' name='escalar' id='escalar' />
									<input type='hidden' name='fase' id='fase' />
									<input type="hidden" name="idProprietario" id="idProprietario" />	
									<input type="hidden" id="itensConfiguracaoRelacionadosSerializado" name="itensConfiguracaoRelacionadosSerializado" />
									<input type="hidden" id="hiddenIdItemConfiguracao" name="hiddenIdItemConfiguracao" />	
									
									<!-- MIDIA SOFTWARE -->
									<input type='hidden' name='midia#idMidiaSoftware' id='midia#idMidiaSoftware' />
                                    <input type='hidden' name='midia#nomeMidia' id='midia#nomeMidia' />
                                  	<input type="hidden" name="midias_serialize" id="midias_serialize" />
                                  	
                                  	<input type='hidden' name='responsavel#idResponsavel' id='responsavel#idResponsavel' />
                                    <input type='hidden' name='responsavel#nomeResponsavel' id='responsavel#nomeResponsavel' />
                                    <input type='hidden' name='responsavel#papelResponsavel' id='responsavel#papelResponsavel' />
                                    <input type='hidden' name='responsavel#telResponsavel' id='responsavel#telResponsavel' />
                                    <input type='hidden' name='responsavel#nomeCargo' id='responsavel#nomeCargo' />
									<input type='hidden' name='responsavel#emailResponsavel' id='responsavel#emailResponsavel' />
									<input type="hidden" name="responsavel_serialize" id="responsavel_serialize" />
									<!-- FIM RESPONS�VEL-->
									<!-- PEDIDO DE COMPRAS -->
									
									<input type='hidden' name='requisicaoCompras#idSolicitacaoServico' id='requisicaoCompras#idSolicitacaoServico' />
                                    <input type='hidden' name='requisicaoCompras#nomeServico' id='requisicaoCompras#nomeServico' />
                                    <input type='hidden' name='requisicaoCompras#situacaoServicos' id='requisicaoCompras#situacaoServicos' />
									<input type="hidden" name="requisicaoCompras_serialize" id="requisicaoCompras_serialize" />
									<!-- FIM PEDIDO COMPRAS -->
                                  	<input type="hidden" name="informacoesComplementares_serialize" id="informacoesComplementares_serialize" />
                                  	<input type="hidden" id="rowIndex" name="rowIndex"/>
                                  	
                                  	<input type="hidden" name="idTipoAba" id="idTipoAba" />	
                                  	<input type="hidden" name="idTipoRequisicao" id="idTipoRequisicao" />	
                                  	
                                  	<input type="hidden" name="idItemConfig" id="idItemConfig" />	
                                  	<input type="hidden" name="nomeItemConfig" id="nomeItemConfig" />	
                                  	
                                  	<input type="hidden" name="idICMudanca" id="idICMudanca" />	
                                  	<input type="hidden" name="numeroLiberacao" id="numeroLiberacao" />	
                                  	<input type="hidden" name="rowIndex" id="rowIndex" />	
                                  	<input type="hidden" name="SGBD" id="SGBD" />
                                  		<div class="col_100">
											<fieldset>
												<label class="" style="font-family: Arial; font-weight: bold;">&nbsp;</label>
											
											</fieldset>
										</div>	
									<div class="col_100">
										<fieldset>
											<label class="campoObrigatorio" style="font-family: Arial; font-weight: bold;"><fmt:message key="contrato.contrato" /></label>
											<div>
											<select  id="idContrato" onchange="setaValorLookup(this);" onclick= "document.form.fireEvent('carregaUnidade');"  name='idContrato' class=" Valid[Required] Description[<fmt:message key='contrato.contrato' />]" >
											</select>
											</div>
										</fieldset>
									</div>
									
									<div class="col_100">
                                        <div class="col_60" >
                                             <fieldset style="height: 60px;">
                                                 <label class="campoObrigatorio" style="cursor: pointer; " >
                                                     <fmt:message key="liberacao.solicitadaPor" /> 
                                                 </label>
                                                 <div>
                                                     <input id="nomeSolicitante" type='text' name="nomeSolicitante" readonly="readonly" class="Valid[Required] Description[liberacao.solicitadaPor]" />
                                                 </div>
                                            </fieldset>
                                        </div>   
									</div>
									
								<div class="col_100">
									<div class="col_15">
											<fieldset style="height: 60px;">
												<label class="campoObrigatorio"><fmt:message key="citcorpore.comum.situacao"/>
												</label>
												<div>
	                                                <select id='status' onchange="verificaDataLiberacao(this);" name='status' class="Valid[Required] Description[citcorpore.comum.situacao]"></select>
												</div>
											</fieldset>
										</div>

										<div class="col_15">
		                                    <fieldset style="height: 60px;">
		                                        <label class="campoObrigatorio"><fmt:message key="liberacao.dataInicial" />
		                                        </label>
		                                            <div>
		                                                <input type='text' name="dataInicial" id="dataInicial" maxlength="10" size="10" 
		                                                    class="Valid[Required] Description[liberacao.dataInicial] Format[Data] datepicker" />
		                                            </div>
		                                    </fieldset>
										</div>
										<div class="col_15">
											<fieldset>
												<label class="campoObrigatorio"><fmt:message key="requisicaoMudanca.horaAgendamentoInicial"/></label>
												<div>
													<input type='text' name="horaAgendamentoInicial" id="horaAgendamentoInicial" maxlength="5" size="5" maxlength="5"  class="Valid[Hora] Format[Hora]" />
												</div>
											</fieldset>
										</div>
										<div class="col_15">
	                                        <fieldset style="height: 60px;">
	                                            <label class="campoObrigatorio"><fmt:message key="liberacao.dataFinal" />
	                                            </label>
	                                                <div>
	                                                    <input type='text' name="dataFinal" id="dataFinal" maxlength="10" size="10" 
	                                                        class="Valid[Required] Description[liberacao.dataFinal] Format[Data] datepicker" />
	                                                </div>
	                                        </fieldset>
										</div>
										<div class="col_15">
											<fieldset>
												<label class="campoObrigatorio"><fmt:message key="requisicaoLiberacao.horaFinal"/></label>
												<div>
													<input type='text' name="horaAgendamentoFinal" id="horaAgendamentoFinal" maxlength="5" size="5" maxlength="5"  class="Valid[Hora] Format[Hora]" />
												</div>
											</fieldset>
										</div>
										<div class="col_25">
											<fieldset>
												<label class="campoObrigatorio" style="cursor: pointer;"><fmt:message key='gerenciaservico.agendaratividade.crupoatividades' /></label>
											<div> 
												<select name='idGrupoAtvPeriodica' id='idGrupoAtvPeriodica'>
												</select>					
											</div>
											</fieldset>
										</div>
									</div>
									
							        <div class="col_100">
                                        <div class="col_40">
                                            <fieldset style="height: 120px;">
                                                <label class="campoObrigatorio"><fmt:message key="liberacao.titulo" />
                                                </label>
                                                    <div>
                                                        <input type='text' name="titulo" id="titulo" maxlength="100" 
                                                            class="Valid[Required] Description[liberacao.titulo]" />
                                                    </div>
                                            </fieldset>
                                        </div>
					                    <div class="col_60">
					                         <fieldset style="height: 120px;">
					                             <label class="campoObrigatorio"><fmt:message key="liberacao.descricao" />
					                             </label>
					                             <div>
					                                 <textarea name="descricao" id="descricao" cols='200' rows='4' maxlength="65000"  class="Valid[Required] Description[liberacao.descricao]" ></textarea>
					                             </div>
					                         </fieldset>
					                     </div>
                                    </div>
                                    <div class="col_100">
									<div>
										<h2 class="section">
											<fmt:message key="solicitacaoServico.informacaoContato" />
										</h2>
									</div>
 
									<div class="col_100">
										<div class="col_50">
											<div class="col_50">
												<fieldset>
													<label class="campoObrigatorio"> <fmt:message
															key="solicitacaoServico.nomeDoContato" /></label>
													<div>
														<input id="nomeContato2" type='text' name="nomeContato2"
															maxlength="70"
															class="Valid[Required] Description[<fmt:message key='solicitacaoServico.nomeDoContato' />]" />
													</div>
												</fieldset>
											</div>

											<div class="col_50">
												<fieldset>
													<label class="campoObrigatorio"><fmt:message
															key="requisicaoMudanca.email" /></label>
													<div>
														<input id="emailContato" type='text' name="emailContato"
															maxlength="120"
															class="Valid[Required, Email] Description[requisicaoMudanca.email]" />
													</div>
												</fieldset>
											</div>

											<div class="col_25">
												<fieldset>
													<label><fmt:message
															key="requisicaoMudanca.telefone" /></label>
													<div>
														<input id="telefoneContato" type='text'
															name="telefoneContato" maxlength="20" class="" />
													</div>
												</fieldset>
											</div>

											<div class="col_25">
												<fieldset>
													<label><fmt:message key="citcorpore.comum.ramal" /></label>
													<div>
														<input id="ramal" type='text' name="ramal" maxlength="4"
															class="Format[Numero]" />
													</div>
												</fieldset>
											</div>

											<div class="col_25">
												<fieldset style="height: 55px">
													<label class="tooltip_bottom campoObrigatorio"
														title="<fmt:message key="colaborador.cadastroUnidade"/>">
														<fmt:message key="unidade.unidade" />
													</label>
													<div>
														<select name='idUnidade' id='idUnidade'
															onchange="document.form.fireEvent('preencherComboLocalidade')"
															class="Valid[Required] Description[colaborador.cadastroUnidade]"></select>
													</div>
												</fieldset>
											</div>

											<div class="col_25">
												<fieldset style="height: 55px">
													<label class="tooltip_bottom "
														title="<fmt:message key="colaborador.cadastroUnidade"/>">
														<fmt:message key="solicitacaoServico.localidadeFisica" />
													</label>
													<div>
														<select name='idLocalidade' id='idLocalidade'></select>
													</div>
												</fieldset>
											</div>
										</div>

										<div class="col_50">
											<fieldset style="height: 112px">
												<label><fmt:message
														key="requisicaoMudanca.observacao" /></label>
												<div>
													<textarea id="observacao" class="col_98" name="observacao"
														maxlength="2000" style="height: 90px; float: right;"></textarea>
												</div>
											</fieldset>
										</div>
									</div>
									 
								</div>
									<div>
										<h2 class="section">
											<fmt:message key="requisicaoLiberacao.informacaoRequisicao" />
										</h2>
									</div>
                                    <div class="col_100">
							        <div class="col_50">
							        <div class="col_100">
				                       <div class= "col_25">
											<fieldset>
												<label class="campoObrigatorio"><fmt:message key="citcorpore.comum.tipo"/></label>
												<div>
													<select class="Valid[Required] Description[citcorpore.comum.tipo]" id="idTipoLiberacao" name='idTipoLiberacao' ></select>
												</div>
											</fieldset>
										</div>
                                        <div class="col_25">
                                            <fieldset>
                                                <label id="dtLiberacao" ><fmt:message key="liberacao.dataLiberacao" />
                                                </label>
                                                    <div>
                                                        <input type='text' name="dataLiberacao" id="dataLiberacao" maxlength="10" size="10" 
                                                            class="Format[Data] datepicker" />
                                                    </div>
                                            </fieldset>
                                        </div>
                                        <div class="col_25">
                                            <fieldset>
                                                <label ><fmt:message key="liberacao.versao" />
                                                </label>
                                                    <div>
                                                        <input type='text' name="versao" id="versao" maxlength="25"  />
                                                    </div>
                                            </fieldset>
                                        </div>
                                        <div class="col_25">
                                            <fieldset>
                                                <label class="campoObrigatorio"><fmt:message key="liberacao.risco"/>
                                                </label>
                                                <div>
                                                    <select id='risco' name='risco' class="Valid[Required] Description[liberacao.risco]" >
                                                        <option value=''><fmt:message key="citcorpore.comum.selecione"/></option>
                                                        <option value='B'><fmt:message key="liberacao.riscoBaixo"/></option>
                                                        <option value='M'><fmt:message key="liberacao.riscoMedio"/></option>
                                                        <option value='A'><fmt:message key="liberacao.riscoAlto"/></option>
                                                    </select>
                                                </div>
                                            </fieldset>
                                        </div>
							        </div>
							        
							        	<div class="col_100">
					        				<div class="col_25">
												<fieldset>
													<label class="campoObrigatorio"> <fmt:message key="citcorpore.comum.grupoExecutor" /></label>
													<div>
														<select name='idGrupoAtual' id='idGrupoAtual' class="Valid[Required] Description[citcorpore.comum.grupoExecutor]" >								
														</select>			
													</div>
												</fieldset>
											</div>
											<div class="col_25">
											<fieldset>
												<label class="campoObrigatorio"><fmt:message key="solicitacaoServico.impacto" /></label>
												<div>
													<select onchange="atualizaPrioridade()" id="nivelImpacto" name="nivelImpacto">
														<option value="B"><fmt:message key="citcorpore.comum.baixa"/></option>
														<option value="M"><fmt:message key="citcorpore.comum.media"/></option>
														<option value="A"><fmt:message key="citcorpore.comum.alta"/></option>
													</select>
												</div>
											</fieldset>
										</div>
										
										<div class="col_25" >
											<fieldset>
												<label class="campoObrigatorio"><fmt:message key="solicitacaoServico.urgencia"/></label>
												<div>
													<select onchange="atualizaPrioridade()" id="nivelUrgencia" name="nivelUrgencia">
														<option value="B"><fmt:message key="citcorpore.comum.baixa"/></option>
														<option value="M"><fmt:message key="citcorpore.comum.media"/></option>
														<option value="A"><fmt:message key="citcorpore.comum.alta"/></option>
													</select>
												</div>
											</fieldset>
										</div>
										
										<div class="col_25">
											<fieldset style="height: 52px">
												<label><fmt:message key="prioridade.prioridade" /></label>
												<div>
													<input id="prioridade" name="prioridade" type="text" readonly="readonly" value="5" />
												</div>
											</fieldset>
										</div>
										</div>
										
									</div>
									<div id="divNotificacaoEmail" class="col_50" >
										<fieldset>
											<label><fmt:message key="requisicaoMudanca.notificacaoporEmail"/></label>								
											<label style='cursor:pointer'><input type='checkbox' value='S' name='enviaEmailCriacao' checked="checked"/><fmt:message key="requisicaoLiberacao.enviaEmailCriacao"/></label><br>
											<label style='cursor:pointer'><input type='checkbox' value='S' name='enviaEmailFinalizacao' checked="checked"/><fmt:message key="requisicaoLiberacao.enviaEmailFinalizacao"/></label><br>
											<label style='cursor:pointer'><input type='checkbox' value='S' name='enviaEmailAcoes'/><fmt:message key="requisicaoLiberacao.enviaEmailAcoes"/></label>
											
										</fieldset>
									</div>			
	                                </div>
									
								<div class="col_100">
									<fieldset>
										<label><fmt:message key="citcorpore.comum.fechamento" /></label>
										<div>
											<textarea id="fechamento" name="fechamento" maxlength="4000" cols='70' rows='5' class="Description[citcorpore.comum.fechamento]"></textarea>
										</div>
									</fieldset>									
								</div>
								<div class="col_50"> 
										<fieldset style="FONT-SIZE: xx-small;">
											<label style="cursor: pointer;"><fmt:message key="citcorpore.comum.categoriaSolucao"/></label>
											<div>
												<select name='idCategoriaSolucao' ></select>
											</div>
										</fieldset>
								</div>
                                     <div>
										<div class="col_100" id='divBotaoAddRegExecucao'>
											<fieldset style="FONT-SIZE: xx-small;">
												<button type='button' name='btnAddRegExec' id='btnAddRegExec' onclick='mostrarEscondeRegExec()'><fmt:message key="solicitacaoServico.addregistroexecucao_mais" /></button>
											</fieldset>									
										</div>	
										<div class="col_100">
											<fieldset style="FONT-SIZE: xx-small;">
												<label id='lblMsgregistroexecucao' style='display:none'><font color='red'><b><fmt:message key="solicitacaoServico.msgregistroexecucao" /></b></font></label>
													<div id='divMostraRegistroExecucao' style='display:none'>
														<textarea id="registroexecucao" name="registroexecucao" cols='70' rows='5' class="Description[citcorpore.comum.resposta]"></textarea>
													</div>
											</fieldset>									
										</div>			
										<div class="col_100" style="overflow : auto;max-height: 400px">
											<fieldset>
												<div  id="tblOcorrencias" ></div>
											</fieldset>									
										</div>									
									</div>	
								</div>

							<div id="btRelatorio">
								<button id="btnImprimir" type="button" name="btnImprimir"
									class="light" onclick="imprimirCadastroPdf();"
									style="margin: 10px !important; text-align: right; float: left;">
									<img
										src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/template_new/images/icons/small/util/file_pdf.png">
									<span><fmt:message key="cronograma.imprimir" /></span>
								</button>
							</div>

							<div  id="abas" class="formRelacionamentos">
									<div id="tabs" class="block">
										<ul class="tab_header clearfix">
											<li><a href="#relacionarMudancas"><fmt:message key="requisicaoMudanca.relacionarMudancas"/></a></li>
											<li><a href="#relacionaIcs"><fmt:message key="requisicaoMudanca.relacionarICs"/></a></li>
											<li><a href="#relacionarProblemas"><fmt:message key="requisicaoMudanca.relacionarProblemas"/></a></li>
											<%-- <li><a href="#relacionarLiberacoes"><fmt:message key="requisicaoMudanca.relacionarLiberacoes"/></a></li> --%>
											<li><a href="#relacionarDocsLegais"><fmt:message key="requisicaoMudanca.documentosLegais"/></a></li>
											<li><a href="#midiaDefinitiva"><fmt:message key="requisicaoMudanca.midiaDefinitiva"/></a></li>
											<li><a href="#documentosGerais"><fmt:message key="requisicaoLiberacao.documentosGerais"/></a></li>
											<li><a href="#responsavel"><fmt:message key="requisicaoLiberacao.papeisResponsabilidades"/></a></li>
											<li><a href="#requisicaoCompras"><fmt:message key="requisicaoLiberacao.requisicaoComprasVinculadas"/></a></li>
											<%if (!tarefaAssociada.equalsIgnoreCase("N")) {%>
												<li><a href="#checklist" onclick="carregaQuestionario('<%=br.com.centralit.citcorpore.util.Enumerados.Aba.LIBERCAOTESTE.getId()%>')"><fmt:message key="requisicaoLiberacao.checklist"/></a></li>
											<%}%>																				
											
										</ul>
										<div id="relacionarMudancas">
											<div id='btAdicionarMudanca' style="width: 15%; float: left;" align="center" >
												<label style="cursor: pointer;" onclick='adicionarMudanca();'>
												<fmt:message key="liberacao.adicionarMudanca" />
												<img  src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/imagens/add.png" />
												</label>
											</div>

											
											<div id='btCadastrarSituacao' style="width: 15%; float: left;" align="center" >
												<label style="cursor: pointer;" onclick="popup3.abrePopup('situacaoLiberacaoMudanca', 'tabelaMudancaSituacaoLiberacao')">
												<fmt:message key="situacaoLiberacaoMudanca.cadastroSituacao" />
												<img  src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/imagens/add.png" />
												</label>
											</div>
											
				                             <div class="col_100">
				                                 	<table id="tblMudancas" class="table table-bordered table-striped" style="width: 100%;">
				                                       <tr>
			                                          	   <th width="1%">&nbsp;</th> 
				                                           <th width="5%"><fmt:message key="citcorpore.comum.numero" /></th>
				                                           <th width="20%"><fmt:message key="liberacao.titulo" /></th>
				                                           <th width="10%"><fmt:message key="situacaoLiberacaoMudanca.status"/></th>
				                                           <th width="10%"><fmt:message key="situacaoLiberacaoMudanca.situacaoDaLiberacao"/></th>
				                                           <th id="altSituacaoMudanca" width="5%"><fmt:message key="situacaoLiberacaoMudanca.alterar"/></th>
				                                           <th width="6%"><fmt:message key="citcorpore.comum.anexosPlanoRevisao"/></th>
				                                           
				                                   </table>
				                             </div>
				                             <div class="col_100">
				                                  <label>&nbsp;</label>
				                             </div>
										</div>
										<div id="relacionaIcs">
										<%-- 	<div  style="width: 15%; float: left;" align="center" >
												<label style="cursor: pointer;" onclick='adicionarIC();'>
												<fmt:message key="requisicaoMudanca.adicionarItemConfiguracao"/>
													<img  src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/imagens/add.png" />
												</label>
											</div> --%>
												<div id="adicionaIC" style="width: 15%; float: left;" align="center" >
													<label style="cursor: pointer;" onclick='abrirModalPesquisaItemConfiguracao();'>
													<fmt:message key="requisicaoMudanca.adicionarItemConfiguracao"/>
														<img  src="<%=br.com.citframework.util.Constantes
											.getValue("CONTEXTO_APLICACAO")%>/template_new/images/icons/small/grey/magnifying_glass.png" />
													</label>
												</div>
											<div id="consultaIC" style="width: 15%; float: left;" align="center" >
												<label style="cursor: pointer;" onclick='inventario();'>
												<fmt:message key="requisicaoMudanca.inventario"/>
													<img  src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/imagens/add.png" />
												</label>
											</div>
											
											<div style="width: 99%; height : 30px; float: left;"></div>
											
											<div class="formBody">
												<table id="tblICs" class="table table-bordered table-striped" style="width: 99%">
													<tr>
														<th width="2%">&nbsp;</th>
														<th width="5%"><fmt:message key="parametroCorpore.id"/></th>
														<th width="60%"><fmt:message key="citcorpore.comum.nome"/></th>
														<th width="30%"><fmt:message key="situacaoLiberacaoMudanca.status"/></th>
														<th width="2%">&nbsp;</th>
													</tr>
												</table>
											</div>
										</div>
										<div id="relacionarProblemas">
											<div id="adicionaProblema" style="width: 15%; float: left;" align="center" >
												<label  style="cursor: pointer;" onclick='adicionarProblema();'>
													<fmt:message key="requisicaoMudanca.adicionarProblema"/>
													<img  src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/imagens/add.png" />
												</label>
											</div>						
											<div style="width: 99%; height : 30px; float: left;"></div>						
											<div class="formBody">
												<table id="tblProblema" class="table table-bordered table-striped">
													<tr>
														<th height="10px" width="5%"></th>
														<th height="10px" width="15%"><fmt:message key="parametroCorpore.id"/></th>
														<th height="10px" width="35%"><fmt:message key="problema.titulo"/></th>
														<th height="10px"width="15%"><fmt:message key="problema.status"/></th>
														<th height="10px"width="10%"></th>
													</tr>
												</table>
											</div>				
										</div>
											<!-- 	MIDIA DEFINITIVA -->
											<div id="midiaDefinitiva">
											<div id="adicionarMidia" style="width: 15%; float: left;" align="center" >
												<label  style="cursor: pointer;" >
													<fmt:message key="liberacao.adicionarMidia"/>
													<img onclick='adicionarMidia();' style="vertical-align: middle; cursor: pointer;"  src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/template_new/images/icons/small/grey/magnifying_glass.png" />
													<img  onclick="popupMidia.abrePopup('midiaSoftware', '');" style="vertical-align: middle; cursor: pointer;" src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/imagens/add.png" />
												</label>
											</div>						
											<div style="width: 99%; height : 30px; float: left;"></div>						
											<div class="formBody">
												<table id="tblMidia" class="table table-bordered table-striped">
													<tr>
														<th height="10px" width="5%"></th>
														<th height="10px" width="15%"><fmt:message key="parametroCorpore.id"/></th>
														<th height="10px" width="35%"><fmt:message key="problema.titulo"/></th>
													</tr>
												</table>
											</div>				
										</div>
										<!--  INICIO DIV DOCUMENTOS LEGAIS -->
										
										<div id="relacionarDocsLegais" style="border:none !important;">
											<div class="formDocsLegais">
												<input type='hidden' id='idItemConfiguracao' name='idItemConfiguracao'/> 
												 <div id="contentDocsLegais" style="border:none !important;">
													<cit:uploadControl id="uploadAnexosdocsLegais" title="Anexos" style="height: 100px; width: 98%; border:0px solid black;" form="document.form" action="/pages/uploadDocsLegais/uploadDocsLegais.load" disabled="false" />				
												</div>	
											</div>				
										</div>
									
										<!-- DOCUMENTOSGERAIS -->
										<div id="documentosGerais">
												<cit:uploadControl id="uploadAnexosDocsGerais" title="Anexos"	style="height: 100px; width: 100%; border: 0px solid black;" form="form" action="/pages/uploadDocsGerais/uploadDocsGerais.load" disabled="false" />
										</div>
										<!-- inicio documentosGerais -->
										
										
										<!--  INICIO DOCUMENTOS GERAIS -->

										<!--  INICIO CHECKLIST -->
										<%if (!tarefaAssociada.equalsIgnoreCase("N")) {%>
											<div id="checklist">
												<div class="col_100" id='divInformacoesComplementares' style='display:block; height: 850px'>
	                                				<iframe id='fraInformacoesComplementares' name='fraInformacoesComplementares' src='about:blank' width="100%" height="100%" style='width: 100%; height: 100%; border:none;'></iframe>
	                            				</div>
											</div>
										<%}%>
										<!-- IN�CIO PAP�IS E RESPONSABILIDADES -->
										<div id="responsavel">
											<div id="adicionarResponsavel" style="width: 20%; float: left;" align="center" >
												<label  style="cursor: pointer;" onclick='adicionarResponsavel();'>
													<fmt:message key="liberacao.adicionarPapeisResponsabilidades"/>
													<img  src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/imagens/add.png" />
												</label>
											</div>						
											<div style="width: 99%; height : 30px; float: left;"></div>						
											<div class="formBody">
												<table id="tblResponsavel" class="table table-bordered table-striped">
													<tr>
														<th height="10px" width="1%"></th>
														<th height="10px" width="5%"><fmt:message key="parametroCorpore.id"/></th>
														<th height="10px" width="25%"><fmt:message key="citcorpore.comum.nome"/></th>
														<th height="10px"width="15%"><fmt:message key="citcorpore.comum.cargo"/></th>
														<th height="10px" width="20%"><fmt:message key="citcorpore.comum.telefone"/></th>
														<th height="10px" width="20%"><fmt:message key="citcorpore.comum.email"/></th>
														<th height="10px" width="20%"><fmt:message key="citcorpore.comum.papel"/></th>
													</tr>
												</table>
											</div>
										</div>				

										<!-- IN�CIO PEDIDO DE COMPRAS -->
										<div id="requisicaoCompras">
											<div id="adicionarRequisicaoCompras" style="width: 8%; float: left;" align="center" >
												<label  style="cursor: pointer;" onclick='adicionarRequisicaoCompras();'>
													<fmt:message key="citcorpore.comum.adicionar"/>
													<img  src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/imagens/add.png" />
												</label>
											</div>						
											<div class="formBody">
												<table id="tblRequisicaoCompra" class="table table-bordered table-striped">
													<tr>
														<th height="10px" width="1%"></th>
														<th height="10px" width="5%"><fmt:message key="requisicaMudanca.numero_solicitacao"/></th>
														<th height="10px" width="40%"><fmt:message key="itemRequisicaoProduto.descrProduto"/></th>
														<th height="10px" width="20%"><fmt:message key="lookup.nomeCentroResultado"/></th>
														<th height="10px" width="20%"><fmt:message key="lookup.codigoCentroResultado"/></th>
														<th height="10px" width="20%"><fmt:message key="lookup.nomeProjeto"/></th>
														<th height="10px" width="20%"><fmt:message key="citcorpore.comum.situacao"/></th>
													</tr>
												</table>
											</div>	
									</div>
									 <div id="btnGravar" style="margin-top: 10px; margin-bottom: 50px;" class="col_100">
                                    
			                             <%if (tarefaAssociada.equalsIgnoreCase("N")) {%>
		                                    <button type='button' name='btnGravarEContinuar' class="light" onclick='gravarEContinuar();'>
												<img src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/template_new/images/icons/small/grey/pencil.png">
												<span><fmt:message key="citcorpore.comum.gravar" /></span>
											</button>
			                                <button type="button" name='btnLimpar' class="light" onclick='limpar();'>
			                                    <img src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/template_new/images/icons/small/grey/clear.png">
			                                    <span><fmt:message key="citcorpore.comum.limpar"/></span>
			                                </button>   
										<%}else{%>
		                                    <button type='button' name='btnGravarEContinuar' class="light" onclick='gravarEContinuar();'>
												<img src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/template_new/images/icons/small/grey/pencil.png">
												<span><fmt:message key="citcorpore.comum.gravarEContinuar" /></span>
											</button>
											<button type='button' name='btnGravarEFinalizar' class="light" onclick='gravarEFinalizar();'>
												<img src="<%=br.com.citframework.util.Constantes.getValue("CONTEXTO_APLICACAO")%>/template_new/images/icons/small/grey/cog_2.png">
												<span><fmt:message key="citcorpore.comum.gravarEFinalizar" /></span>
											</button>                                 
										<%}%>
                                                               
                                  
                                   </div>
									</div><!-- FIM_tabs -->
								</div><!-- FIM_formRelaciomentos -->
								<div id="POPUP_MUDANCA_FECHAMENTO_CATEGORIA" title=<fmt:message key="situacaoLiberacaoMudanca.alterar" />>
      								 <div class="box grid_16 tabs">
       								 <div class="toggle_container">
       								 <div id="tabs-2" class="block">
        							 <div class="col_100">
										<fieldset>
										<label><fmt:message key="situacaoLiberacaoMudanca.situacao" /></label>
												<div>
												<input type="hidden" id="linha" name="linha"/>
											<select name="idFechamentoCategoria" id="idFechamentoCategoria"
													class="Valid[Required] Description[liberacaomudanca.status]">
											</select>
											<button type="button" class="light" onclick="setarFechamentoCategoria()">OK</button>
												</div>
										</fieldset>
								  </div>
     							  </div>
  								  </div>
      							  </div>
      							  </div>
   								 </div>
							</form>
						</div>
					</div>
			</div>
		</div>
		</div>
		<!-- Fim da Pagina de Conteudo -->
	<%@include file="/include/footer.jsp"%>
</body>

	<div id="POPUP_EMPREGADO" title="<fmt:message  key="citcorpore.comum.pesquisa" />">
		<div class="box grid_16 tabs">
		<div class="toggle_container">
		<div id="tabs-2" class="block">
		<div class="section">
			<form name='formPesquisaEmp' style="width: 540px">
<!-- lookup_empregado -->
				<cit:findField formName='formPesquisaEmp' lockupName='LOOKUP_SOLICITANTE_CONTRATO' id='LOOKUP_EMPREGADO' top='0' left='0' len='550' heigth='400' javascriptCode='true' htmlCode='true' />
			</form>
		</div>
		</div>
		</div>
		</div>
	</div>
	
    <div id="POPUP_MUDANCA" title="<fmt:message  key="citcorpore.comum.pesquisa" />">
        <div class="box grid_16 tabs">
        <div class="toggle_container">
        <div id="tabs-2" class="block">
        <div class="section">
            <form name='formPesquisaMudanca' style="width: 540px">
                <cit:findField formName='formPesquisaMudanca' lockupName='LOOKUP_MUDANCA_CONCLUIDA_SITUACAO' id='LOOKUP_MUDANCA_CONCLUIDA_SITUACAO' top='0' left='0' len='550' heigth='400' javascriptCode='true' htmlCode='true' />
            </form>
        </div>
        </div>
        </div>
        </div>
    </div>
    
    <div id="POPUP_PROBLEMA" title="<fmt:message  key="citcorpore.comum.pesquisa" />">
        <div class="box grid_16 tabs">
        <div class="toggle_container">
        <div id="tabs-2" class="block">
        <div class="section">
            <form name='formPesquisaProblema' style="width: 540px">
                <cit:findField formName='formPesquisaProblema' lockupName='LOOKUP_PROBLEMA_CONCLUIDO' id='LOOKUP_PROBLEMA' top='0' left='0' len='550' heigth='400' javascriptCode='true' htmlCode='true' />
            </form>
        </div>
        </div>
        </div>
        </div>
    </div>
    
    <div id="POPUP_INFO_INSERCAO"
		title=""
		style="overflow: hidden;">
		<div class="toggle_container">
			<div id='divMensagemInsercao' class="section" style="overflow: hidden; font-size: 24px;">
				
			</div>
			<button type="button" onclick='$("#POPUP_INFO_INSERCAO").dialog("close");'>
				<fmt:message key="citcorpore.comum.fechar" />
			</button>
		</div>
	</div>	
    <div id="POPUP_INFO_BASELINE"
		title=""
		style="overflow: hidden;">
		<div class="toggle_container">
			<div id='divMensagemInsercaoBaseline' class="section" style="overflow: hidden; font-size: 24px;">
				
			</div>
			<button type="button" onclick='$("#POPUP_INFO_BASELINE").dialog("close");'>
				<fmt:message key="citcorpore.comum.fechar" />
			</button>
		</div>
	</div>	
    
    
   <div id="POPUPITEMCONFIGURACAO" style=""  title="<fmt:message key="citcorpore.comum.pesquisa" />">
 		<table >
			<tr>
				<td>
					<h3><fmt:message key="eventoItemConfiguracao.itensConfiguracao" /></h3>
				</td>
			</tr>
		</table>				
		<form name='formPesquisaItem' style="width: 95%; margin:auto;">
			<cit:findField formName='formPesquisaItem' 
						   lockupName='LOOKUP_ITEMCONFIGURACAO_ATIVO_ALL' 
						   id='LOOKUP_ITEMCONFIGURACAO' 
						   top='0' left='0' len='550' 
						   heigth='400' 	
						   javascriptCode='true' htmlCode='true' />
		</form>
	</div> 
		<div id="POPUP_MIDIA" title="<fmt:message  key="citcorpore.comum.pesquisa" />">
        <div class="box grid_16 tabs">
        <div class="toggle_container">
        <div id="tabs-2" class="block">
        <div class="section">
            <form name='formPesquisaMidia' style="width: 540px">
                <cit:findField formName='formPesquisaMidia' lockupName='LOOKUP_MIDIASOFTWARE' id='LOOKUP_MIDIASOFTWARE' top='0' left='0' len='550' heigth='400' javascriptCode='true' htmlCode='true' />
            </form>
        </div>
        </div>
        </div>
        </div>
		</div>
    </div>
    <div id="POPUP_RESPONSAVEL" title="<fmt:message  key="citcorpore.comum.pesquisa" />">
        <div class="box grid_16 tabs">
        <div class="toggle_container">
        <div id="tabs-2" class="block">
       <div class="section">
            <form name='formPesquisaResponsavel' style="width: 790px">
                <cit:findField formName='formPesquisaResponsavel' lockupName='LOOKUP_RESPONSAVEL' id='LOOKUP_RESPONSAVEL' top='0' left='0' len='800' heigth='500' javascriptCode='true' htmlCode='true' />
            </form>
        </div>
        </div>
        </div>
        </div>
    </div> 
    <div id="POPUP_REQUISICAOCOMPRAS" title="<fmt:message  key="citcorpore.comum.pesquisa" />">
        <div class="box grid_16 tabs">
        <div class="toggle_container">
        <div id="tabs-2" class="block">
       <div class="section">
            <form name='formPesquisaRequisicaoCompras' style="width: 800px">
                <cit:findField formName='formPesquisaRequisicaoCompras' lockupName='LOOKUP_REQUISICAOCOMPRAS' id='LOOKUP_REQUISICAOCOMPRAS' top='0' left='0' len='800' heigth='600' javascriptCode='true' htmlCode='true' />
            </form>
        </div>
        </div>
        </div>
        </div>
    </div>
	<div id="POPUP_ATIVOS" title="<fmt:message key="pesquisa.listaAtivosDaMaquina" />" style="overflow: hidden;">
		<div class="box grid_16 tabs" >
			<div class="toggle_container" >
				<div id="tabs-2" class="block" style="overflow: hidden;">
					<div class="section" style="overflow: hidden;">
						<iframe id="iframeAtivos" style="display: block; margin-left: -20px;" name="iframeAtivos" width="970" height="480" >
						</iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="POPUP_PLANOSREVERSAO" title="<fmt:message key="requisicaoLiberacao.planosReversao" />" style="overflow: hidden;">
		<form name='formUploadReversaoLiberacao' action='${ctx}/pages/uploadPlanoDeReversaoLiberacao/uploadPlanoDeReversaoLiberacao' enctype="multipart/form-data">
				<input type="hidden" name="idMudanca"  id="idMudanca" />
				<cit:uploadPlanoDeReversaoControl id="uploadPlanoDeReversaoLiberacao" title="Anexos" style="height: 220px; width: 98%; border: 1px solid black;" form="formUploadReversaoLiberacao" action="/pages/uploadPlanoDeReversaoLiberacao/uploadPlanoDeReversaoLiberacao.load" disabled="false" />
		</form>  
	</div>
	<div id="POPUP_PESQUISAITEMCONFIGURACAO" style="" title="<fmt:message key="itemConfiguracao.pesquisa" />" class="POPUP_LOOKUP_SERVICO">
		<iframe id='framePesquisaItemConfiguracao' src='about:blank' width="100%" height="99%" onload="resize_iframe()">
		</iframe>
	</div>
</html>
