<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>

<%
	String iframe = "";
	iframe = request.getParameter("iframe");
%>
<!doctype html public "">
<html>
	<head>
		<%@include file="/novoLayout/common/include/libCabecalho.jsp" %>
		<%@include file="/novoLayout/common/include/titulo.jsp" %>
		<link type="text/css" rel="stylesheet" href="../../novoLayout/common/include/css/template.css"/>
		<link type="text/css" rel="stylesheet" href="css/relatorioPrestacoesContas.css"/>
		<link type="text/css" rel="stylesheet" href="../../novoLayout/common/include/css/jqueryautocomplete.css"/>
	</head>
	<cit:janelaAguarde id="JANELA_AGUARDE_MENU"  title="" style="display:none;top:325px;width:300px;left:500px;height:50px;position:absolute;"></cit:janelaAguarde>
	<body>
		<div class="<%=(iframe == null) ? "container-fluid fixed" : "" %>">
			
			<!-- Top navbar (note: add class "navbar-hidden" to close the navbar by default) -->
			<div class="navbar <%=(iframe == null) ? "main" : "nomain" %> hidden-print">
			
				<% if(iframe == null) { %>
					<%@include file="/novoLayout/common/include/cabecalho.jsp" %>
					<%@include file="/novoLayout/common/include/menuPadrao.jsp" %>
				<% } %>
				
			</div>
	
			<div id="wrapper" class="<%=(iframe == null) ? "" : "nowrapper" %>">
					
				<!-- Inicio conteudo -->
				<div id="content">			
					<form name='form' id='form' action='${ctx}/pages/relatorioPrestacoesContas/relatorioPrestacoesContas'>
					<input type="hidden" id="idIntegranteViagem" name="idIntegranteViagem" />
					
					<div class="widget">
					
						<div class="widget-head">
							<h3 class="heading strong"><fmt:message key="menu.acompanhamentoPrestacaoContas"/></h3>
						</div>
						
						<div class="widget-body">	
							<div class="widget">
					
								<div class="widget-head">
									<h4 class="heading strong"><fmt:message key="citcorpore.comum.filtros"/></h4>
								</div>
								
								<div class="widget-body">	
									<div class='row-fluid'>
						             	<div class='span3'>
								            <label class='strong'><fmt:message key="gerenciamentoservico.NSolicitacao" /></label>
								            <input type='text' name="idSolicitacaoServico" id="idSolicitacaoServico" maxlength="9" class="span12 Format[Numero]" />
						                </div>
					                     	
						                <div class='span6'>
								            <label class='strong'><fmt:message key="itemControleFinanceiroViagem.nome" /></label>
								            <input type='text' name="nomeEmpregado" id="nomeEmpregado" class='span12' onchange="limpaIntegrante()"/>
					                    </div>
					                    
					                    <div class='span3'>
								            <label class='strong'><fmt:message key="citcorpore.comum.status" /></label>
								            <select id="comboIntegranteEstado" type='text' name="estado" id="estado" class='span12'></select>
					                    </div>
				                    </div>
							                    
				                    <div class='row-fluid'>
										<div class='span12'>
										 
											<div class="widget">
												<div class="widget-head">
													<h4 class="heading strong"><fmt:message key='requisicaoViagem.periodoDaViagem'/></h4>
												</div><!-- .widget-head -->
												
												 <div class="widget-body">
													<div class='row-fluid'>
														<div class='span4'>
															<label class='strong'><fmt:message key="itemControleFinanceiroViagem.iniciandoEntre" /></label>
															<div class='row-fluid'>
																<div class='span5'>
																	<input type='text' name="dataInicio" id="dataInicio" maxlength="10" class="span12 Format[Data] datepicker" onblur="fctValidaData(this)"/>
																</div>
																<div class='span2' style="text-align: center !important;">
																	<fmt:message key="citcorpore.comum.a" />
																</div>
																<div class='span5'>
																	<input type='text' name="dataInicioAux" id="dataInicioAux" maxlength="10" class="span12 Format[Data] datepicker" onblur="fctValidaData(this)"/>
																</div>
															</div>
														</div>
														   
														<div class='span4' style="text-transform: lowercase; margin-top: 26px; text-align: center;">
															<label class="radio inline strong">
																<input type="radio" id="e" name="eOu" value="and" checked="checked"/><fmt:message key="citSmart.comum.e" />
															</label>
															<label class="radio inline strong">	
																<input type="radio" id="ou" name="eOu" value="or" /> <fmt:message key="citSmart.comum.ou" />
															</label>	
														</div>
													 
														<div class='span4'>
															<label class='strong'><fmt:message key="itemControleFinanceiroViagem.terminandoEntre" /></label>
															<div class='row-fluid'>
																<div class='span5'>
																	<input type='text' name="dataFim" id="dataFim" maxlength="10" class="span12 Format[Data] datepicker" onblur="fctValidaData(this)"/>
																</div>
																<div class='span2' style="text-align: center !important;">
																	<fmt:message key="citcorpore.comum.a" />
																</div>
																<div class='span5'>
																	<input type='text' name="dataFimAux" id="dataFimAux" maxlength="10" class="span12 Format[Data] datepicker" onblur="fctValidaData(this)"/>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
						             	</div>
					             	</div>
					         	</div><!-- .widget-body -->
							</div><!-- .widget -->
							
							<div class='row-fluid'>
								<div class='span12'>
									<button type='button' name='btnGerarRelatorioRequisicao' class="lFloat btn btn-icon btn-primary" onclick='pesquisarRequisicoes();'>
										<i></i><fmt:message key="citcorpore.comum.gerarelatorio" />
									</button>
									
									<button type='button' name='btnPesquisarRequisicao' class="lFloat btn btn-icon btn-primary" onclick="pesquisaPrestacoesContasPendentes();">
										<i></i><fmt:message key="citcorpore.comum.pesquisar" />
									</button>
									
									<button type='button' name='btnLimpar' class="lFloat btn btn-icon btn-primary" onclick='limparFormulario();'>
										<i></i><fmt:message key="citcorpore.comum.limpar" />
									</button>
								</div>
					 		</div>	
					 		
					 		<div class="row-fluid" style="margin-top: 15px;">
								<div class="widget">
									<div class="widget-head" style="height: height: 30px!important">
										<h4 class="heading strong"><fmt:message key='menu.acompanhamentoPrestacaoContas'/></h4>
									</div><!-- .widget-head -->
									
									<div class="widget-body">							
										<!-- Table  -->
										<div role="grid" class="dataTables_wrapper form-inline" id="DataTables_Table_0_wrapper">
											<div class="row-fluid"></div>
											
											<table class="dynamicTable table table-striped table-bordered table-condensed dataTable" id="tblPrestacoesContasPendentes" aria-describedby="DataTables_Table_0_info">
												<thead>
													<tr>
							                           <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" 
														style="width: 10%;" aria-label="Browser: activate to sort column ascending"><fmt:message key="requisicaoProduto.requisicao" /></th>
														
							                           <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" 
														style="width: 10%;" aria-label="Browser: activate to sort column ascending"><fmt:message key="relatorioPrestacoesContas.dataTermino" /></th>
														
							                           <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" 
														style="width: 20%;" aria-label="Browser: activate to sort column ascending"><fmt:message key="relatorioPrestacoesContas.nomeResponsavel" /></th>
														
							                           <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" 
														style="width: 20%;" aria-label="Browser: activate to sort column ascending"><fmt:message key="citcorpore.comum.status" /></th>
														
							                           <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" 
														style="width: 10%;" aria-label="Browser: activate to sort column ascending"><fmt:message key="relatorioPrestacoesContas.valorAdiantado" /></th>
														
													   <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" 
														style="width: 10%;" aria-label="Browser: activate to sort column ascending"><fmt:message key="relatorioPrestacoesContas.valorPrestacao" /></th>
														
							                           <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" 
														style="width: 20%;" aria-label="Browser: activate to sort column ascending"><fmt:message key="citcorpore.comum.acoes" /></th>
							                       </tr>
												</thead>
												
												<tbody role="alert" aria-live="polite" aria-relevant="all"></tbody>
											</table>
										</div><!-- .dataTables_wrapper -->
									</div><!-- .widget-body -->
								</div><!-- .widget -->
							</div><!-- .row-fluid -->
	             		</div><!-- .widget-body -->
					</div><!-- .widget -->
           		</form>	
				</div>                     
				<!--  Fim conteudo-->
				<%@include file="/novoLayout/common/include/rodape.jsp" %>
			</div>
		</div>
		<script type="text/javascript" src="js/relatorioPrestacoesContas.js"></script>
		<script src="js/jquery.maskMoney.js"></script>
		<script src="${ctx}/novoLayout/common/include/js/jquery.autocomplete.js"></script>
	</body>
</html>
