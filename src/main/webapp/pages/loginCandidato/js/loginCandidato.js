/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
  $(function(){
	  if(LOCALE_SISTEMA=="en"){	
			$("#cpf").unmask();
		}else{
			$("#cpf").mask("999.999.999-99");
		}
		
  });

function validar() {
	if(!StringUtils.isBlank(StringUtils.trim($('#metodoAutenticacao').val()))) {
		if(StringUtils.trim($('#user').val()) == "") {
			alert(i18n_message("login.digite_login"));
			$('#user').focus();
			return false;
		}
		if(StringUtils.trim($('#senha').val()) == "") {
			alert(i18n_message("login.digite_senha"));
			$('#user').focus();
			return false;
		}
		
		mostrar_aguarde();
		document.form.fireEvent("autenticarCandidato");
	} else {
		// Valida��o login
		if(!$('#cpf').val()) {
			alert(i18n_message("informe.cpf"));
			$('#cpf').focus();
			return false;
		} else if ($('#cpf').val().length < 14) {
			alert(i18n_message("cpf.invalido"));
			$('#cpf').focus();
			return false;
		} else if(!$('#senha').val()) {
			alert(i18n_message("informe.senha"));
			$('#senha').focus();
			return false;
		} else {
			mostrar_aguarde();
			document.form.fireEvent("autenticarCandidato");
		}
	}
	return true;
}

mostrar_aguarde = function() {
	JANELA_AGUARDE_MENU.show();
}

fechar_aguarde = function() {
	JANELA_AGUARDE_MENU.hide();
}
