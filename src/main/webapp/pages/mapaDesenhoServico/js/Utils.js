/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * @author breno.guimaraes
 */

function Utils() {
}
/**
 * Elimina espa�os em branco em qualquer lugar da string
 * 
 * @param str
 * String a ser modificada.
 */
Utils.trim = function(str) {
	var novaStr = "";
	for ( var i = 0; i < str.length; i++) {
		novaStr += str.charAt(i) == ' ' ? '' : str.charAt(i);
	}
	return novaStr;
};

/**
 * Mostra uma mensagem definida em um componente definido por um tempo definido.
 * 
 * @param componenteId
 * Id do componente onde a mensagem ser� colocada.
 * @param segundos
 * Tempo em segundos que a mensagem ficar� na tela.
 * @param texto
 * Texto que dever� aparecer no componente pelo tempo determinado.
 */
this.mostrarMsgTemporaria = function(componenteId, segundos, texto) {
	var t;
	var componente = document.getElementById(componenteId);

	componente.innerText = texto;

	t = setTimeout(function() {
		componente.innerText = '';
	}, segundos * 1000);
};
