/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * @author breno.guimaraes
 */

function GerenciadorImagem() {

	/**
	 * Mostra uma imagem na tela.
	 * 
	 * @param myImage
	 * Objeto da classe MyImage com as propriedades da imagem.
	 */
	this.attachImage = function(myImage) {
		var imageObj = new Image();
		// ao carregar, torna vis�vel
		imageObj.onload = function() {
			if (myImage.getHeight() != null && myImage.getWidth() != null) {
				myImage.getContext().drawImage(imageObj, myImage.getXPos(), myImage.getYPos(), myImage.getWidth(), myImage.getHeight());
			} else {
				myImage.getContext().drawImage(imageObj, myImage.getXPos(), myImage.getYPos());

			}
		};

		// carrega imagem
		imageObj.src = myImage.getCaminho();

		return imageObj;
	};
	
	
	/**
	 * M�todo privado apenas para tornar as imagens vis�veis ap�s
	 * o carregamento.
	 * @myImages
	 * Lista de imagens (MyImages) que foram carregadas
	 */
	this.drawImages = function(myImages) {
		for ( var j = 0; j < myImages.length; j++ ) {
			if(myImages[j].getHeight() != null && myImages[j].getWidth() != null){
				myImages[j].getContext().drawImage(myImages[j].getImageObj(),
						  					  	   myImages[j].getXPos(), 
						  					  	   myImages[j].getYPos(),
						  					  	   myImages[j].getWidth(), 
						  					  	   myImages[j].getHeight());
			} else {				
				myImages[j].getContext().drawImage(myImages[j].getImgObj(),
											  	   myImages[j].getXPos(), 
											  	   myImages[j].getYPos());
			}
		}
	};
}
