/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 *  @author breno.guimaraes
 */

function ItemConfiguracao(_nome, _id, imagemItemConfiguracao){
	var idItemConfiguracao = _id;
	var identificacao = _nome;
	var imagem = imagemItemConfiguracao;
	var itemPai;
	
	//GETTERS SETTERS
	this.getTipo = function(){
		return tipo;
	};
	
	this.getIdItemConfiguracao = function(){
		return idItemConfiguracao;
	};
	
	this.setIdItemConfiguracao = function(value){
		idItemConfiguracao = value;
	};
	
	this.getIdentificacao = function(){
		return identificacao;
	};
	
	this.setIdentificacao = function(value){
		identificacao = value;
	};
	
	this.getImagem = function(){
		return imagem;
	};
	
	this.setImagem = function(imgItemConfig){
		imagem = imgItemConfig;
	};
	
	this.getItemPai = function(){
		return itemPai;
	};
	
	this.setItemPai = function(itemConfigPai){
		itemPai = itemConfigPai;
	};
}
