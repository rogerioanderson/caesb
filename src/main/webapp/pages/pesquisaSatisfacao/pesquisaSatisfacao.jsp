<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>

<!doctype html public "">
<html>
	<head>
		<%
			String descricao = (String)request.getAttribute("descricao");
			String resposta = (String)request.getAttribute("resposta");
			if (descricao == null){
			    descricao= "";
			}
			descricao = descricao.replaceAll("\n", "<br>");
			if (resposta == null){
			    resposta= "";
			}
			resposta = resposta.replaceAll("\n", "<br>");
		%>
		<%String locale = UtilStrings.nullToVazio((String)request.getSession().getAttribute("locale")); %>
		<%@include file="/include/header.jsp"%>

		<%@include file="/include/security/security.jsp" %>
		<title><fmt:message key="citcorpore.comum.title"/></title>

		<%@include file="/include/javaScriptsComuns/javaScriptsComuns.jsp"%>

		<link type="text/css" rel="stylesheet" href="${ctx}/pages/pesquisaSatisfacao/css/pesquisaSatisfacao.css"></link>
		<script type="text/javascript" src="${ctx}/pages/pesquisaSatisfacao/js/pesquisaSatisfacao.js"></script>
	</head>

	<cit:janelaAguarde id="JANELA_AGUARDE_MENU"  title="" style="display:none;top:325px;width:300px;left:500px;height:50px;position:absolute;">
</cit:janelaAguarde>

	<body>
	<div id="corpo" >
		<div id="page" class="hidden">
			<form id="form" name='form' action='${ctx}/pages/pesquisaSatisfacao/pesquisaSatisfacao'>
				<input type="hidden" id="idSolicitacaoServico" name="idSolicitacaoServico">
				<input type="hidden" id="frame" name="frame">
				<input type="hidden" name="locale" id="locale"/>

				<ul class="menu">
		    		<li <% if (locale.equalsIgnoreCase("")) { %> class="active" <% } %> onclick="internacionalizar('pt');return false;"><a href="" title="Portugues" ><img onclick="internacionalizar('pt');return false;" src="../../novoLayout/common/theme/images/lang/br.png" alt="Portugues"> Portugu�s BR</a></li>
		      		<li <% if (locale.equalsIgnoreCase("en")) { %> class="active" <% } %> onclick="internacionalizar('en');return false;"><a href="" title="English"><img onclick="internacionalizar('en');return false;" src="../../novoLayout/common/theme/images/lang/us.png" alt="English"> English</a></li>
		      		<li <% if (locale.equalsIgnoreCase("es")) { %> class="active" <% } %> onclick="internacionalizar('es');return false;"><a href="" title="Espa�ol"><img onclick="internacionalizar('es');return false;" src="../../novoLayout/common/theme/images/lang/es.png" alt="Espanhol"> Espa�ol</a></li>
	      		</ul>

	      		<div class="clear"></div>

				<div class='titulo'>
					<h2>CITSMart - <fmt:message key="pesquisasatisfacao.pesquisasatisfacao"/></h2>
				</div>
				
				<div id="pagemsg" class="hidden">
					<h1 class="textoPesquisaSucesso"><fmt:message key="pesquisasatisfacao.sucesso"/></h1>
				</div>
				
				<div id="conteudo">
					<div class="camp">
						<label class="titLab">
							<fmt:message key="pesquisa.codigodataabertura"/>:
						</label>
						<label id="divId" class="result"></label>
					</div>
					<div class="camp">
						<label class="titLab">
							<fmt:message key="pesquisa.descricao"/>:
						</label>
						<label id="" class="result">
							<%=descricao%>
						</label>
					</div>
					<div class="camp">
						<label class="titLab">
							<fmt:message key="pesquisa.resposta"/>:
						</label>
						<label id="" class="result">
						<%=resposta%>
						</label>
					</div>
					<div class="camp">
						<label class="titLab">
							<fmt:message key="comentarios.avaliacao"/>:
						</label>
						<label id="" class="result">
							<select id="comboNotas" name="nota" ></select>
						</label>
					</div>
					<div class="camp">
						<label class="titLab">
							<fmt:message key="pesquisa.melhoria"/>:
						</label>
						<label id="" class="result">
							<textarea id="comentario" name="comentario" rows="10" cols="50" style="display: block;"></textarea>
						</label>
					</div>
					<div class="camp">
	
						<button type='button' name='btnEnviar' class="light"  onclick='enviar();' >
							<img src="${ctx}/template_new/images/icons/small/grey/user_comment.png">
							<span><fmt:message key="citSmart.comum.enviar"/></span>
						</button>
					</div>				
				</div>
			</form>
		</div>
	</div>
<%@include file="/include/header.jsp" %>
<script type="text/javascript" src="${ctx}/template_new/js/isotope/jquery.isotope.min.js"></script>
<script type="text/javascript" src="${ctx}/template_new/js/fancybox/jquery.fancybox-1.3.4.js"></script>

<script type="text/javascript" src="${ctx}/template_new/js/custom/gallery.js"></script>

		<div id="loading_overlay">
			<div class="loading_message round_bottom">
				<img src="${ctx}/template_new/images/loading.gif" alt="aguarde..." />
			</div>
		</div>
	</body>
</html>
