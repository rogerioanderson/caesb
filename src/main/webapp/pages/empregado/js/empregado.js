/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
var popup;
var popup2;
var popup3;

var listaUnidades = [];
var listaIdUnidades = [];

var validarComboUnidade;
var unidadeAutoComplete;

addEvent(window, "load", load, false);
function load() {
    popup = new PopupManager(1000, 600, ctx + "/pages/");
    popup2 = new PopupManager(1000, 600, ctx + "/pages/");
    popup3 = new PopupManager(1000, 600, ctx + "/pages/");
    document.form.afterRestore = function () {
        $('.tabs').tabs('select', 0);
        
        var valor_id_unidade = document.getElementById("idUnidade").value;
        if (valor_id_unidade >= 0) {
            var ix = listaIdUnidades.indexOf(valor_id_unidade);
            document.getElementById("unidadeDes").value = listaUnidades[ix];
        }
    }

}

function verificaValor(obj) {
    if (obj.options[obj.selectedIndex].value == 'N') {
        $("#labelUnidade").removeClass("campoObrigatorio");
        $("#labelCargos").removeClass("campoObrigatorio");
    } else {
        $("#labelUnidade").addClass("campoObrigatorio");
        $("#labelCargos").addClass("campoObrigatorio");
    }

    if (obj.options[obj.selectedIndex].value == 'E') {
        $('valorSalario').innerHTML = 'Valor Sal&atilde;rio CLT:';
    } else if (obj.options[obj.selectedIndex].value == 'S') {
        $('valorSalario').innerHTML = 'Valor Est&atilde;gio:';
    } else if (obj.options[obj.selectedIndex].value == 'P') {
        $('valorSalario').innerHTML = 'Valor Contratado Mensal:';
    } else if (obj.options[obj.selectedIndex].value == 'X') {
        $('valorSalario').innerHTML = 'Valor do Prolabore:';
    } else {
        $('valorSalario').innerHTML = 'Valor Mensal:';
    }
}

function LOOKUP_EMPREGADO_select(id, desc) {
    document.form.restore({idEmpregado: id});
}

$(function () {
    $('.datepicker').datepicker();
    
	id('telefone').onkeypress = function(e) {
		if(NumberUtil.retornaSomenteNumero(e)){ 
			mascararElementoInput(this, aplicarMascaraNaAcaoOnKeyPress);
		}else{
			return false;
		}
	}
	
	id('telefone').onblur = function() {
		calculaEMontagemMascaraTelefone(this);
	}
});


function ocultarDivGruposContrato() {
    $('#gruposContrato').hide();
}

function exibirDivGruposContrato() {
    $('#gruposContrato').show();
}

function excluir() {
    if (document.getElementById("idEmpregado").value != "") {
        if (confirm(i18n_message("citcorpore.comum.deleta"))) {
            document.form.fireEvent("delete");
        }
    }
}

function gravar() {
    var validaEmail = ValidacaoUtils.validaEmail(document.getElementById('email'), '');

    if (validaEmail == false) {
        return;
    }
    if (!validaDatas()) {
        return;
    }

    if ($('#tipo option:selected').val() != 'N') {
        if ($("#idUnidade").val() == null || $("#idUnidade").val() == "") {
            alert("Unidade: Campo Obrigat�rio");
            return;
        }
        if ($("#idCargo").val() == null || $("#idCargo").val() == "") {
            alert("Cargo: Campo Obrigat�rio");
            return;
        }
    }

    document.form.save();

}

function validaDatas() {
    if (!nullOrEmpty(gebi("dataNasc"))) {
        if (!nullOrEmpty(gebi("dataEmissaoRg"))) {
            if (!DateTimeUtil.comparaDatas(document.form.dataNasc, document.form.dataEmissaoRg, i18n_message("citcorpore.comum.validacao.datargmenordatanasc"))) {
                return false;
            }
        }
        if (!nullOrEmpty(gebi("dataEm"))) {
            if (!DateTimeUtil.comparaDatas(document.form.dataNasc, document.form.dataEm, i18n_message("citcorpore.comum.validacao.emissaoctpsmenornasci"))) {
                return false;
            }
        }
        if (!nullOrEmpty(gebi("dataAdmissao"))) {
            if (!DateTimeUtil.comparaDatas(document.form.dataNasc, document.form.dataAdmissao, i18n_message("citcorpore.comum.validacao.admissaomenornascimento"))) {
                return false;
            }
        }
    }
    return true;
}

/**
 * getElementById
 */
function gebi(id) {
    return document.getElementById(id);
}

function nullOrEmpty(comp) {
    return comp.value == null || comp.value == "" ? true : false;
}

//<!-- Thiago Fernandes - 23/10/2013 14:06 - Sol. 121468 - Cria��o de fun��o para n�o digitar numeros, para retirar bug de n�o poder usar setas do teclado . -->
function naoDigitarNumeros(e) {
    var tecla = event.keyCode || e.which;
    if ((tecla > 47 && tecla < 58))
        return false;
    else {
        if (tecla == 8 || tecla == 0)
            return true;
        else
            return true;
    }
}


function preparaVisualizacaoDeUnidades(pValidarComboUnidade, pUnidadeAutoComplete) {
    validarComboUnidade = pValidarComboUnidade;
    unidadeAutoComplete = pUnidadeAutoComplete;

    if (unidadeAutoComplete.toUpperCase() === "S") {
        $('#unidadeDes').attr('autocomplete', 'on');

        $("#unidadeDes").autocomplete({
            source: listaUnidades,
            noCache: true,
            select: function (event, ui) {
                if (ui.item && ui.item.value) {
                    var vr = $.trim(ui.item.value);
                    var ix = listaUnidades.indexOf(vr);
                    $('#idUnidade').val(listaIdUnidades[ix]);
                }
            }
        });
    }
    else
    {
        var dropdown = document.getElementById("idUnidade");
        if (dropdown) {
            for (var i = 0; i < listaUnidades.length; ++i) {
                addOption(dropdown, listaUnidades[i], listaIdUnidades[i]);
            }
        }
    }
}
;

function onChangeUnidade() {
    var selectBox = document.getElementById("idUnidade");
    var id = selectBox.options[selectBox.selectedIndex].value;
    $('#idUnidade').val(id);
}
;

addOption = function (selectbox, text, value) {
    var optn = document.createElement("OPTION");
    optn.text = text;
    optn.value = value;
    selectbox.options.add(optn);
};
