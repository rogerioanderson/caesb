<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>

<!doctype html public "">
<html>
<head>
	<%@include file="/include/security/security.jsp" %>
	<%@include file="/include/header.jsp"%>
	<title><fmt:message key="citcorpore.comum.title" /></title>
	<%@include file="/include/javaScriptsComuns/javaScriptsComuns.jsp" %>

<style type="text/css">
	.table {
		border-left:1px solid #ddd;
	}

	.table th {
		border:1px solid #ddd;
		padding:4px 10px;
		border-left:none;
		background:#eee;
	}

	.table td {
		border:1px solid #ddd;
		padding:4px 10px;
		border-top:none;
		border-left:none;
	}
</style>
<script>

	//addEvent(window, "load", load, false);
	//function load() {};

	function atualizarEmail(){
		document.form.fireEvent("carregarEmails");
	}

	function toggleDiv(id){
		var div = document.getElementById(id);
		div.style.display = div.style.display == "none" ? "block" : "none";
	}


</script>
</head>
<body>
	<div id="wrapper">
		<%@include file="/include/menu_vertical.jsp"%>
		<!-- Conteudo -->
		<div id="main_container" class="main_container container_16 clearfix">
			<%@include file="/include/menu_horizontal.jsp"%>

			<div class="flat_area grid_16">
				<h2>E-mails - Central de Servi�os</h2>
			</div>
			<form name='form' action='${ctx}/pages/ClienteEmailCentralServico/ClienteEmailCentralServico'>
				<div class="box grid_16 tabs">
					<table>
						<tr>
							<td>Trazer mensagen(s) do(s) �ltimo(s)</td>
							<td><input size="3" type="text" id="ultimosXDias" name="ultimosXDias" /></td>
							<td>dia(s)</td>
							<td> <a href="#" onclick="atualizarEmail()">atualizar</a> </td>
						</tr>
					</table>
				</div>
				<div class="box grid_16 tabs" id="emails">
					<!--<table id="emails" class='table'>
						<tr>
							<th>Data</th>
							<th>De</th>
							<th>Assunto</th>
						</tr>
					</table>
				--></div>
			</form>
		</div>
		<!-- Fim da Pagina de Conteudo -->
	</div>
	<%@include file="/include/footer.jsp"%>
</body>
</html>
