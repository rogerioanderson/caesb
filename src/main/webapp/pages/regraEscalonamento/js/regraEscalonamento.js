/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/


			var objTab = null;

			addEvent(window, "load", load, false);

			function load() {
				document.form.afterRestore = function() {
					$('.tabs').tabs('select', 0);
				}

				$("#POPUP_SERVICO").dialog({
					autoOpen : false,
					width : 600,
					height : 400,
					modal : true
				});

				$("#POPUP_GRUPO").dialog({
					autoOpen : false,
					width : 600,
					height : 400,
					modal : true
				});

				$("#POPUP_SOLICITANTE").dialog({
					autoOpen : false,
					width : 600,
					height : 400,
					modal : true
				});

			}

			function excluir() {
				if (confirm(i18n_message("citcorpore.comum.deleta"))) {
					document.form.fireEvent("excluir");
				}
			}

			function LOOKUP_REGRAESCALONAMENTO_select(idParam, desc) {
				document.form.restore({
					idRegraEscalonamento : idParam
				});
			}

			function LOOKUP_SERVICO_select(id, desc){
				document.getElementById("idServico").value = id;
				document.getElementById("servico").value = desc;
				$("#POPUP_SERVICO").dialog("close");
			}

			function LOOKUP_GRUPO_select(id, desc){
				document.getElementById("idGrupo").value = id;
				document.getElementById("grupo").value = desc;
				$("#POPUP_GRUPO").dialog("close");
			}

			function LOOKUP_SOLICITANTE_select(id, desc){
				document.getElementById("idSolicitante").value = id;
				document.getElementById("nomeSolicitante").value = desc;
				$("#POPUP_SOLICITANTE").dialog("close");
			}

			function abrePopupServico(){
				$("#POPUP_SERVICO").dialog("open");
			}

			function abrePopupGrupo(){
				$("#POPUP_GRUPO").dialog("open");
			}

			function abrePopupUsuario(){
				$("#POPUP_SOLICITANTE").dialog("open");
			}

			function addGrupoExecutor(){
				if(document.getElementById('idGrupoAtual').value == ""){
					alert(i18n_message("regraEscalonamento.alerta.informeGrupoExecutor"));
					return;
				}else if(document.getElementById('prazoExecucao').value == ""){
					alert(i18n_message("regraEscalonamento.alerta.informePrazoExecucao"));
					return;
				}
		        var obj = new CIT_EscalonamentoDTO();
		        obj.idGrupoExecutor = document.getElementById('idGrupoAtual').value;
		        obj.descricao = document.getElementById('idGrupoAtual').options[document.getElementById('idGrupoAtual').selectedIndex].text;
				obj.prazoExecucao = document.getElementById('prazoExecucao').value;
				obj.idPrioridade = document.getElementById('idPrioridade').value;
				obj.descrPrioridade = document.getElementById('idPrioridade').options[document.getElementById('idPrioridade').selectedIndex].text;

				HTMLUtils.addRow('tblGrupoExecutor', document.form, '', obj,
						['', 'idGrupoExecutor', 'descricao', 'prazoExecucao', 'idPrioridade', 'descrPrioridade'], ["idGrupoExecutor"], i18n_message("regraEscalonamento.alerta.grupoJaAdicionado"), [gerarImgDelGrupoExecutor], null, null, false);

			}

			function deleteLinha(table, index){
				if (index > 0 && confirm(i18n_message("regraEscalonamento.alerta.exclusaoEscalonamento"))) {
					HTMLUtils.deleteRow(table, index);
				}
			}

			function gerarImgDelGrupoExecutor(row, obj){
			        row.cells[0].innerHTML = '<img src="' + ctx + '/imagens/delete.png" style="cursor: pointer;" onclick="deleteLinha(\'tblGrupoExecutor\', this.parentNode.parentNode.rowIndex);"/>';
}

function serializaTabelaGrupos() {
	var itens = HTMLUtils.getObjectsByTableId('tblGrupoExecutor');
	document.form.grupos_serialize.value = ObjectUtils
			.serializeObjects(itens);
}

limpar = function() {
	document.form.clear();
	HTMLUtils.deleteAllRows("tblGrupoExecutor");
}

salvar = function() {
	serializaTabelaGrupos();

	if (document.getElementById("tempoExecucao").value == ""
			&& document.getElementById("idTipoGerenciamento").value == 1) {
		alert(i18n_message("regraEscalonamento.alerta.informePrazoRestanteExecucao"));
		document.getElementById("tempoExecucao").focus();
		return;
	}
	if (document.getElementById("enviarEmail").value == "S"
			&& document.getElementById("intervaloNotificacao").value == "") {
		alert(i18n_message("regraEscalonamento.alerta.informeIntervaloNotificacao"));
		document.getElementById("enviarEmail").focus();
		return;
	}
	document.form.save();
}

function LimparCampoServico() {
	document.getElementById("servico").value = "";
	document.getElementById("idServico").value = "";
}
function LimparCampoGrupo() {
	document.getElementById("grupo").value = "";
	document.getElementById("idGrupo").value = "";
}
function LimparCampoNomeSolicitante() {
	document.getElementById("nomeSolicitante").value = "";
	document.getElementById("idSolicitante").value = "";
}

