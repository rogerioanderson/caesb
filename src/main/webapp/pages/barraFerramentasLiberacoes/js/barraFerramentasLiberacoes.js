/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
var popup;
var menusLiberados;
var todosOsMenus;
var objTab = null;

function LOOKUP_OCORRENCIA_MUDANCA_select(id, desc) {
	document.formOcorrenciaLiberacao.restore({
		idOcorrencia : id
	});
}

function salvar() {
	document.formOcorrenciaLiberacao.descricao.value = document.formOcorrenciaLiberacao.descricao1.value;
	if (document.getElementById("idOcorrencia").value != null && document.getElementById("idOcorrencia").value != "") {
		alert(i18n_message("gerenciaservico.suspensaosolicitacao.validacao.alteraregistroocorrencia") );
	} else {
		document.formOcorrenciaLiberacao.save();
	}	
}

function fecharBaseConhecimentoView() {
	$('#popupCadastroRapido').dialog('close');
}

$(function() {
	popupBarraFerramenta = new PopupManager(1300, 600, ctx+"/pages/");
	popupCadastroCategoriaOcorrencia = new PopupManager(1200, 450, ctx+"/pages/");
	popupCadastroOrigemOcorrencia = new PopupManager(1200, 450, ctx+"/pages/");
	
	document.formOcorrenciaLiberacao.afterRestore = function() {
		$('.tabs').tabs('select', 0);
	};
	
	$(".POPUP_barraFerramentasLiberacoes").dialog({
		autoOpen : false,
		width : 1000,
		height : 580,
		modal : true
	});
	
	$('.POPUP_PESQUISA_CATEGORIA_OCORRENCIA').dialog({
		autoOpen: false,
		width: 900,
		height: 450,
		modal: true
	});
	
	$('.POPUP_PESQUISA_ORIGEM_OCORRENCIA').dialog({
		autoOpen: false,
		width: 900,
		height: 450,
		modal: true
	});
	
	menusLiberados = [];
	todosOsMenus = [];
	todosOsMenus.push("btAnexos");
	todosOsMenus.push("btOcorrencias");
		todosOsMenus.push("btBaseConhecimentoView");	
		menusLiberados.push("btBaseConhecimentoView");

	//adicionar um IF para verificar ID de Serviço quando já estier sendo feito o restore.
	liberaMenusDeConsulta();
	escondeMenusBloqueadosEMostraLiberados();

	/**
		* O nome do método já diz tudo.
		*/
	function escondeMenusBloqueadosEMostraLiberados() {
		for(var i = 0; i < todosOsMenus.length; i++) {
			if(!isLiberado(todosOsMenus[i]) ) {
				document.getElementById(todosOsMenus[i]).style.display = "none";
			} else {
				document.getElementById(todosOsMenus[i]).style.display = "block";
			}
		}
	}
		
	function ocultarAnexos(){
		$('#formularioDeAnexos').hide();
	}
	
	function exibirAnexos(){
		$('#formularioDeAnexos').show();
	}	 			

	/**
	 * Só deve ser chamada quando houver restore do serviço
	 */
	function liberaMenusDeConsulta() {
		menusLiberados.push("btAnexos");
		menusLiberados.push("btOcorrencias");
			menusLiberados.push("btBaseConhecimentoView");
	}

	/**
	 * Bloqueia os menus que não devem ser acessados caso seja 
	 * nova solicitação de serviço.
	 */
	function bloqueiaMenusDeConsulta() {
		deletaItemLista("btAnexos");
		deletaItemLista("btOcorrencias");
		alert(1)
	}

	/**
	 * Deleta um item da lista e a reordena.
	 */
	function deletaItemLista(nomeItem) {
		var novaLista = [];
		for(var i = 0; i < menusLiberados.length; i++) {
			if(menusLiberados[i] == nomeItem) {
				menusLiberados[i] = null;
			} else {
				novaLista.push(menusLiberados[i]);
			}
		}
		menusLiberados = [];
		menusLiberados = null;
		menusLiberados = novaLista;
	}

	/**
	 * Verifica se um item está na lista de liberação.
	 */
	function isLiberado(nomeMenu) {
		for(var i = 0; i < menusLiberados.length; i++) {
			if(menusLiberados[i] == nomeMenu) {
				return true;
			}
		}
		return false;
	}

	//ações dos botões	
	$("#btAnexos").click(function() {
		//a popup é aberta do lado do java.
		document.form.fireEvent("verificarParametroAnexos");
		$('#POPUP_menuAnexos').dialog('open');
		/**
        Analista desenvolvedor: rcs (Rafael César Soyer)
        data: 13/02/2015
        A linha abaixo, que executar o comando "uploadRequisicaoMudanca.refresh()", foi comentada
        */
		//uploadAnexos.refresh();
	});

	$("#btOcorrencias").click(function() {
		document.formOcorrenciaLiberacao.clear();
		document.formOcorrenciaLiberacao.idRequisicaoLiberacao.value = document.form.idRequisicaoLiberacao.value; 
		document.formOcorrenciaLiberacao.fireEvent('load');
		document.getElementById('divRelacaoOcorrencias').innerHTML = i18n_message("citcorpore.comum.aguardecarregando");
// 					document.formOcorrenciaLiberacao.fireEvent('listInfoRegExecucaoRequisicaoMudanca');
		$("#POPUP_menuOcorrencias").dialog("open");
		//posteriormente trocar pelo serviço carregado
//					document.getElementById("pesqLockupLOOKUP_OCORRENCIA_MUDANCA").value = 1;
// 					document.getElementById("pesqLockupLOOKUP_OCORRENCIA_MUDANCA_IDREQUISICAOMUDANCA").value = 1;
		document.getElementById("btnTodos").style.display = "none";
	});

/* 
	$("#btIncidentesRelacionados").click(function() {
		//popup aberta do lado java
		document.formIncidentesRelacionados.idSolicitacaoIncRel.value = document.form.idRequisicaoMudanca.value; 
		inicializarTemporizadorRel1();
		document.formIncidentesRelacionados.fireEvent("restore");
	}); */

	$("#btRelacionarSolicitacao").click(function() {
		inicializarTemporizadorRel2();
		document.formIncidentesRelacionados.fireEvent("listarSolicitacoesServicoEmAndamento");
		$("#divSolicitacoesFilhas").dialog("open");
	});

	$("#btRelacionarSolicitacaoFechar").click(function() {
		$('#POPUP_menuIncidentesRelacionados').dialog('close');
	});

		$("#btBaseConhecimentoView").click(function() {
			//dialog tratado pelo PopupManager.js
			/* $("#popupCadastroRapido").dialog({height :700,width :1400}); */
			var altura = parseInt($(window).height() * 0.8);
        var largura = parseInt($(window).width() * 0.8);
			$("#popupCadastroRapido").dialog({height :altura,width :largura});
			popupBarraFerramenta.titulo = i18n_message('baseConhecimento.consultaABaseConhecimento');
			popupBarraFerramenta.abrePopup('baseConhecimentoView', '');
		});

	$("#btnFecharTelaAnexos").click(function() {
		$('#POPUP_menuAnexos').dialog('close');
	});
	
	$('#nomeCategoriaOcorrencia').click(function() {
		$('#POPUP_PESQUISA_CATEGORIA_OCORRENCIA').dialog('open')
	});
	
	$('#nomeOrigemOcorrencia').click(function() {
		$('#POPUP_PESQUISA_ORIGEM_OCORRENCIA').dialog('open')
	});
});

function gravarAnexo() {
	document.form.idRequisicaoMudanca.disabled = false;
	document.form.fireEvent("gravarAnexo");
}

function abrirPopupCadastroCategoriaOcorrencia() {
	popupCadastroCategoriaOcorrencia.abrePopupParms('categoriaOcorrencia', '');
}			

function LOOKUP_CATEGORIA_OCORRENCIA_select(id, desc) {
	$('#idCategoriaOcorrencia').val(id);				
	$('#nomeCategoriaOcorrencia').val(desc);
	$('.POPUP_PESQUISA_CATEGORIA_OCORRENCIA').dialog('close');
}

function LOOKUP_ORIGEM_OCORRENCIA_select(id, desc) {
	$('#idOrigemOcorrencia').val(id);				
	$('#nomeOrigemOcorrencia').val(desc);
	$('.POPUP_PESQUISA_ORIGEM_OCORRENCIA').dialog('close');
}			

function abrirPopupCadastroOrigemOcorrencia() {
	popupCadastroOrigemOcorrencia.abrePopupParms('origemOcorrencia', '');
}

function excluir() {				
	if (document.getElementById("id").value != "") {
		if (confirm(i18n_message("citcorpore.comum.deleta") ) ) {
			document.formPesquisaCategoriaOcorrencia.fireEvent("delete");
		}
	}
}
