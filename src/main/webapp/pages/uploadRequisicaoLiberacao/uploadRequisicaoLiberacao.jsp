<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.tld.UploadControl"%>
<%@page import="br.com.centralit.citcorpore.bean.UploadDTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collection"%>
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>
<%
response.setHeader( "Cache-Control", "no-cache");
response.setHeader( "Pragma", "no-cache");
response.setDateHeader ( "Expires", -1);
pageContext.setAttribute("acaoRetorno", request.getParameter("acaoRetorno"));
%>
<html>
<head>
<%@include file="/include/cssComuns/cssComuns.jsp" %>
<%@include file="/include/internacionalizacao/internacionalizacao.jsp"%>

	<link rel="stylesheet" type="text/css" href="${ctx}/pages/uploadRequisicaoLiberacao/css/uploadRequisicaoLiberacao.css">
	
</head>
<body>
<script type="text/javascript">
	var acaoRetorno = "${acaoRetorno}";
	var ctx = "${ctx}";
</script>
<script type="text/javascript" src="${ctx}/pages/uploadRequisicaoLiberacao/js/uploadRequisicaoLiberacao.js"></script>
<table id="tabelaAnexosRequisicaoLiberacao" width="100%" class='tableLess'>
	<thead>
		<tr>
		  <th>&nbsp;</th>
          <th ><fmt:message key="citcorpore.comum.arquivo" /></th>
          <th><fmt:message key="citcorpore.comum.descricao" /></th>
          <th><fmt:message key="citcorpore.comum.situacao" /></th>
		</tr> 
	</thead>
        <%boolean branco = true;%>
      	<%String cor = "#ffffff";%>	
      	<%
		Collection colUploadsGED = (Collection)request.getSession(true).getAttribute("colUploadRequisicaoLiberacaoGED");
		if (colUploadsGED == null){
			colUploadsGED = new ArrayList();
		}      	
      	%>		
      	<%for(Iterator it = colUploadsGED.iterator(); it.hasNext();){ %>
      		<%
      		UploadDTO uploadDTO = (UploadDTO)it.next();
      		%>
		<TR>
			<%
			if(branco){ 
				cor ="#ffffff"; 
			}else{
				cor="#E5EBF6";
			}
			%>	
			<td>
			  <%
			  /**
              Analista desenvolvedor: rcs (Rafael C�sar Soyer)
              data : 09/02/2015
              A linha abaixo verifica se o par�metro editar est� como "RO" - read only.
              Caso positivo o bot�o de exclus�o de anexos n�o ser� montado na tela.
              */
			    String str_valorParametroEditar = request.getParameter("editar");
              	System.out.println("valor de str_valorParametroEditar : "+str_valorParametroEditar);
			   
			    if (str_valorParametroEditar == null || !str_valorParametroEditar.equals("RO")){
			    %>
			     <img src='<%=CitCorporeConstantes.CAMINHO_SERVIDOR%><%=request.getContextPath()%>/imagens/delete.png' style='cursor:pointer' onclick='excluirImagemUpload("<%=uploadDTO.getPath()%>")' title='<fmt:message key="citcorpore.ged.msg.exluiranexo" /> '/>
			    <% } %>
			</td>
			<td >
				<a href="#" onclick='obtemArquivoTemporario("<%=uploadDTO.getPath()%>")'><%=uploadDTO.getNameFile()%></a>
			</td>
			<td >
				<%=uploadDTO.getDescricao()%>
			</td>		
			<td  title='<fmt:message key="citcorpore.ged.situacao" />'>			
				<%=uploadDTO.getSituacao()%>
			</td>	
        </TR>
		<%}%>
</table>
</body>

</html>
