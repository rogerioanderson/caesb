<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>

<%
	response.setCharacterEncoding("ISO-8859-1");
%>
 <!DOCTYPE head PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<%@include file="/include/header.jsp"%>
	<%@include file="/include/titleComum/titleComum.jsp" %>

	<%@include file="/include/javaScriptsComuns/javaScriptsComuns.jsp"%>

	<script type="text/javascript" src="../../cit/objects/DemandaDTO.js"></script>
	<script type="text/javascript" src="./js/copiarSLA.js"></script>

</head>

<!-- Definicoes Comuns -->
<cit:janelaAguarde id="JANELA_AGUARDE_MENU" title="<fmt:message key='citcorpore.comum.aguardeProcessando'/>" style="display:none;top:100px;width:300px;left:200px;height:50px;position:absolute;">
</cit:janelaAguarde>

<body>

<div id="paginaTotal" >
	<div id="areautil">
		<div id="formularioIndex">
       		<div id=conteudo>
				<table width="100%">
					<tr>
						<td width="100%">
								<h2><b><fmt:message key="citcorpore.comum.copiarSLA"/></b></h2>
								<!-- ## AREA DA APLICACAO ## -->
										 	<form name='form' action='${ctx}/pages/copiarSLA/copiarSLA'>
										 		<input type='hidden' name='idServicoContrato'/>
										 		<input type='hidden' name='idContrato'/>
										 		<input type='hidden' name='idAcordoNivelServico'/>
											  	<table id="tabFormulario" cellpadding="0" cellspacing="0">
											  		<tr>
											  			<td>
											  				<b><fmt:message key="citcorpore.comum.copiarAcordo"/>:</b>
											  			</td>
											  			<td>
											  				<b><fmt:message key="citcorpore.comum.paraServicosDesteContrato"/>:</b>
											  			</td>
											  		</tr>
											  		<tr>
											  			<td>
											  				<div id='slaCopiar' style='width: 300px; height: 300px; overflow: auto; border: 1px solid black'>
											  				</div>
											  			</td>
											  			<td>
											  				<div id='copiarPara' style='width: 600px; height: 300px; overflow: auto; border: 1px solid black'>
											  				</div>
											  			</td>
											  		</tr>
										         <tr>
										         	<td>
										         		<button type='button' id="btnGravar" name='btnGravar' style="margin-top: 5px; margin-left: 3px" class="light img_icon has_text" onclick="gravarForm()">
															<img src="${ctx}/template_new/images/icons/small/grey/pencil.png">
															<span><fmt:message key="dinamicview.gravardados"/></span>
														</button>
														<button type='button' id="btnFechar" name='btnFechar' style="margin-top: 5px; margin-left: 3px" class="light img_icon has_text" onclick="parent.limparAreaInformacao();parent.fecharVisao()">
															<img src="${ctx}/template_new/images/icons/small/grey/trashcan.png">
															<span><fmt:message key="citcorpore.comum.fechar"/></span>
														</button>
									         		</td>
										         	<td>
										         		<div style="margin-top: 5px;">
											         		<input type='checkbox' name='selecionarTodas' id='selecionarTodas' onclick='marcaTodas();' style='float:left;' />
											         		<label style='float:left;'><fmt:message key="citcorpore.comum.selecionarTodas"/></label>
										         		</div>
									         		</td>
									         	</tr>
										</table>
									</form>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

</body>
</html>
