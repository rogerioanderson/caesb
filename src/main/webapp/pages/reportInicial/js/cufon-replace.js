/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
Cufon.replace('#faded ul.slides li a, header nav li, .link1, .link3, .link4', { fontFamily: 'Myriad Pro Regular', textShadow: '#000 1px 1px', hover:true });
Cufon.replace('.link2', { fontFamily: 'Myriad Pro Regular', textShadow: '#265800 1px 1px', hover:{textShadow: 'none'} });
Cufon.replace('h1', { fontFamily: 'Myriad Pro Regular', textShadow: '#000 1px 1px', color: '-linear-gradient(#82b704, #699303)' });
Cufon.replace('h1 span', { fontFamily: 'Myriad Pro Regular', color: '-linear-gradient(#fff, #cdcdcd)' });
Cufon.replace('#faded ul.pagination li a span, h2', { fontFamily: 'Myriad Pro Light' });
Cufon.replace('h3, h4, .banners li a', { fontFamily: 'Myriad Pro Light', textShadow: '#417c01 1px 1px' });
Cufon.replace('.banners li a b', { fontFamily: 'Myriad Pro Semibold', textShadow: '#417c01 1px 1px' });
Cufon.replace('.price', { fontFamily: 'Myriad Pro Regular', color: '-linear-gradient(#7fb504, #488801)' });
Cufon.replace('#slogan h2', { fontFamily: 'Myriad Pro Regular' });
