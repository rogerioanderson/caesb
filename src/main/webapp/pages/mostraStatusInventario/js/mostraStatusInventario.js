/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/

	var objTab = null;

	addEvent(window, "load", load, false);
	function load() {
		window.setTimeout('processsa()',1000);
	}
	function processsa() {
		document.form.fireEvent('mostraInfo');
		window.setTimeout('processsa()',5000);
	}

	function LOOKUP_COMANDO_select(id, desc) {
		document.form.restore({
			id :id});
	}
	function submeteIP(){
		document.form.fireEvent('submeteIP');
	}
	function forcarLacoInv(){
		document.form.fireEvent('forcarLacoInv');
	}
	function inventarioAgora(ip){
		document.form.ip.value = ip;
		document.form.fireEvent('inventarioAgora');
	}
	function refreshIPs(){
		document.getElementById('divInfo').innerHTML = 'Aguarde...';
		document.form.fireEvent('refreshIPs');
	}
	function gerarFaixaIPs(){
		$("#faixaIps").modal("show");
	}
	function submeteGerarFaixaIPs(){
		document.getElementById('divResultado').innerHTML = 'Aguarde...';
		document.formGerarFaixa.fireEvent('gerarFaixaIPs');
	}
	function adicionarIPSAtivosLista(){
		document.form.ip.value = document.formGerarFaixa.txtIPSAtivos.value;
		document.form.fireEvent('submeteIP');
	}
	function adicionarTodosIPSLista(){
		document.form.ip.value = document.formGerarFaixa.txtIPSTodos.value;
		document.form.fireEvent('submeteIP');
	}
	function fazPing(ip){
		document.form.ip.value = ip;
		document.form.fireEvent('fazPing');
	}
	function refresh(){
		window.location = ctx+'/pages/mostraStatusInventario/mostraStatusInventario.load';
	}
	function alterarThreadInv(){
		document.formAlteraValor.tipoDado.value = 'INV';
		$("#alteraValor").modal("show");
	}
	function alterarThreadDisc(){
		document.formAlteraValor.tipoDado.value = 'DIS';
		$("#alteraValor").modal("show");
	}
	function alterarPingTimeout(){
		document.formAlteraValor.tipoDado.value = 'PING';
		$("#alteraValor").modal("show");
	}
	function submeteAlteracaoValor(){
		document.formAlteraValor.fireEvent('alteraValor');
	}


