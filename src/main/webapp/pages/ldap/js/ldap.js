/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
var countAtributo = 0;

function addLinhaTabelaAtributosLdap(id, atributoLdap, valorAtributoLdap){
	var tbl = document.getElementById('tabelaAtributosLdap');

	$('#tabelaAtributosLdap').show();

	var lastRow = tbl.rows.length;

	countAtributo++;

	var row = tbl.insertRow(lastRow);

	var coluna = row.insertCell(0);

	coluna.innerHTML = '<input type="hidden" readonly="readonly" class="outputext" id="idAtributo' + countAtributo + '" name="idAtributoLdap" value="' + id + '" />' + atributoLdap;

	coluna = row.insertCell(1);
	if(countAtributo == 6){
		coluna.innerHTML = '<input style="width: 100%;  border: 0 none;" type="password" onblur="validarQuantidade(' + countAtributo + ');" id="valorAtributoLdap' + countAtributo + '" name="valorAtributoLdap" value="' + valorAtributoLdap + '"/>';
	}else{
	coluna.innerHTML = '<input style="width: 100%;  border: 0 none;" type="text" onblur="validarQuantidade(' + countAtributo + ');" id="valorAtributoLdap' + countAtributo + '" name="valorAtributoLdap" value="' + valorAtributoLdap + '"/>';
	}
	}

function deleteAllRowsTabelaAtributosLdap() {
	var tabela = document.getElementById('tabelaAtributosLdap');
	var count = tabela.rows.length;

	while (count > 1) {
		tabela.deleteRow(count - 1);
		count--;
	}
}

function serializaAtributosLdap() {
	var tabela = document.getElementById('tabelaAtributosLdap');
	var count = countAtributo + 1;
	var listAtributos = [];

	for ( var i = 1; i < count; i++) {
		if (document.getElementById('idAtributo' + i) != "" && document.getElementById('idAtributo' + i) != null) {
			var idAtributo = document.getElementById('idAtributo' + i).value;

			var valorAtributo = $('#valorAtributoLdap' + i).val()

			var ldapDto = new LdapDTO(idAtributo, valorAtributo);

			listAtributos.push(ldapDto);
		}
	}
	document.form.listAtributoLdapSerializado.value = ObjectUtils.serializeObjects(listAtributos);
}

function LdapDTO(idAtributo, valorAtributo){
	this.idAtributoLdap = idAtributo;
		this.valorAtributoLdap = valorAtributo;
	}

function gravar(){
	 var campo1 = document.getElementById("valorAtributoLdap1").value.split(";").length;
	 var campo2 = document.getElementById("valorAtributoLdap2").value.split(";").length;
	 var campo3 = document.getElementById("valorAtributoLdap3").value.split("&").length;
	 var campo4 = document.getElementById("valorAtributoLdap4").value.split(";").length;
	 var campo5 = document.getElementById("valorAtributoLdap5").value.split(";").length;
	 var campo6 = document.getElementById("valorAtributoLdap6").value.split(";").length;
	 var campo7 = document.getElementById("valorAtributoLdap7").value.split(";").length;
	 var campo8 = document.getElementById("valorAtributoLdap8").value.split(";").length;
	 var campo9 = document.getElementById("valorAtributoLdap9").value.split(";").length;
	 var campo10 = document.getElementById("valorAtributoLdap10").value.split(";").length;
	 var campo11 = document.getElementById("valorAtributoLdap11").value.split(";").length;
	 var campo12 = document.getElementById("valorAtributoLdap12").value.split(";").length;

		if(campo1 == campo2 && campo1 == campo3 && campo1 == campo4 && campo1 == campo5 && campo1 == campo6 && campo1 == campo7  && campo1 == campo8  && campo1 == campo10  && campo1 == campo11 && campo1 == campo12){
				 if((campo7 == 1 || campo1 == campo7) && (campo8 == 1 || campo1 == campo8) && (campo10 == 1 || campo1 == campo10) && (campo11 == 1 || campo1 == campo11) && (campo12 == 1 || campo1 == campo12)){
					 serializaAtributosLdap();
					 document.form.save();
				 }
				 else{
					 alert(i18n_message("ldap.quantidadeparametros"));
				 }
		}else{
			alert(i18n_message("ldap.quantidadeparametros"));
		}
}

function testarConexao() {
	JANELA_AGUARDE_MENU.show();
	document.form.fireEvent("testarConexao");
}

function atualizar(){
	document.form.fireEvent("load");
}
function sincronizaLDAP() {
	if(confirm(i18n_message("ldap.desejaSincronizarLDAP"))) {
		JANELA_AGUARDE_MENU.show();
		document.form.fireEvent("sincronizaLDAP");
	}
}

function validarQuantidade(nomeCampo) {
	switch (nomeCampo) {
	case 9:
	case 13:
	case 14:
		var valorCampo = document.getElementById("valorAtributoLdap" + nomeCampo).value.trim();
		
		if (valorCampo != "S" && valorCampo != "s" && valorCampo != "N" && valorCampo != "n" && valorCampo != "") {
			document.getElementById("valorAtributoLdap" + nomeCampo).style.color = "red";
			alert("Parametro incorreto!");
			document.getElementById("valorAtributoLdap" + nomeCampo).value = "";
		}
		
		document.getElementById("valorAtributoLdap" + nomeCampo).style.color = "black";
		
		break;
	default:
		var campoInicial = document.getElementById("valorAtributoLdap1").value.split(";");
		var quantidade = [];
		
		if (nomeCampo == 3) {
			quantidade = document.getElementById("valorAtributoLdap" + nomeCampo).value.split("&");
		} else {
			quantidade = document.getElementById("valorAtributoLdap" + nomeCampo).value.split(";");
		}

		if (campoInicial.length == quantidade.length) {
			document.getElementById("valorAtributoLdap" + nomeCampo).style.color = "black";
		} else {
			document.getElementById("valorAtributoLdap" + nomeCampo).style.color = "red";
		}
	}
}
