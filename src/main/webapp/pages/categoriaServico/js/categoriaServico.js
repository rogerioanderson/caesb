/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/

		var objTab = null;

		addEvent(window, "load", load, false);
		function load() {
			document.form.afterRestore = function() {
				$('.tabs').tabs('select', 0);
			}
		}

		function LOOKUP_CATEGORIASERVICO_select(id, desc) {
			document.form.restore({
				idcategoriaServico : id
			});
		}


		function LOOKUP_CATEGORIASERVICO_SUPERIOR_select(idTipo, desc) {
			document.form.idCategoriaServicoPai.value = idTipo;

		    var valor = desc.split('-');
			var nomeConcatenado = "";
			for(var i = 0 ; i < valor.length; i++){
				if(i == 0){
					document.form.nomeCategoriaServicoPai.value = valor[i];
				}
			}
			document.form.fireEvent("verificaHierarquia");
			fecharPopup();
			/* document.form.fireEvent("restoreTipoItemConfiguracao"); */
		}

		function atualizaData() {
			var idCategoriaServico = document.getElementById("idCategoriaServico");

			if (idCategoriaServico != null && idCategoriaServico.value == 0) {
				alert(i18n_message("citcorpore.comum.necessarioSelecionarRegistro"));
				return false;
			}
			if (confirm(i18n_message("citcorpore.comum.deleta")))
				document.form.fireEvent("atualizaData");
		}

		$(function() {
			$("#POPUP_CATEGORIASERVICO_SUPERIOR").dialog( {
				autoOpen : false,
				width : 705,
				height : 500,
				modal : true
			});
		});

		function consultarCategoriaServicoSuperior(){
			$("#POPUP_CATEGORIASERVICO_SUPERIOR").dialog("open");
		}

		function fecharPopup(){
			$("#POPUP_CATEGORIASERVICO_SUPERIOR").dialog("close");
		}

	
