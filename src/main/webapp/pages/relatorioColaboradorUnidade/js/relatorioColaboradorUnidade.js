/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/

		function inserirNaListaColaborador(id,nome){
			var aux = new Option(nome, id);
			var options = document.getElementById("idColaborador").options;
			for(var i = 0; i < options.length; i++)
				if(parseInt(options[i].value) === id){
					alert(i18n_message("relatorioColaboradorUnidade.colaboradorDuplicado"));
					limparColaborador();
					return;
				}

			$("#idColaborador").append(aux);
			limparColaborador();
		}

		function limparLista(lista){
			$("#"+lista+" > option").each(function(i){
				$(this).remove();
		    });
		}


		function limparColaborador(){
			$('#colaboradorBusca').val('');
		}

		/**Autocomplete **/
		var completeColaborador;
		$(document).ready(function() {
			$('#colaboradorBusca').on('click', function(){
				verificaContrato();
				montaParametrosAutocompleteColaborador();
			});

			completeColaborador = $('#colaboradorBusca').autocomplete({
				serviceUrl:'pages/autoCompleteSolicitante/autoCompleteSolicitante.load',
				noCache: true,
				onSelect: function(value, data){
					inserirNaListaColaborador(data, value);
					document.form.idUnidade.focus();
				}
			});

		});
		var contrato;

		/**Monta os parametros para a buscas do autocomplete**/
		function montaParametrosAutocompleteColaborador(){
		 	contrato =  $("#idContrato").val();
		 	unidade =  $("#idUnidade").val();
		 	completeColaborador.setOptions({params: {contrato: contrato, unidade: unidade} });
		}

		function RetirarDaLista(lista1, lista2){
			var texto = $("#"+lista2+" option:selected").text();
			var valor = $("#"+lista2+" option:selected").val();
			if(texto!="" & valor!=""){
				$('#'+lista1).append("<option value='"+valor+"' selected='selected'>"+texto+"</option>");
				$('#'+lista2+' option:selected').remove();
			}
		}

		function retirarTodosDaLista(lista1, lista2){
			$("#"+lista2+" > option").each(function(i){
				$('#'+lista1).append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
				$(this).remove();
		    });
		}

		function limpar() {
			limparLista("idColaborador");
			document.form.clear();
		}

		function reportEmpty(){
			alert(i18n_message("citcorpore.comum.relatorioVazio"));
		}

		function verificaContrato() {
			if (document.form.idContrato.value == '' ||  document.form.idContrato.value == '-- Todos --'){
				alert(i18n_message("solicitacaoservico.validacao.contrato"));
				document.form.idContrato.focus();
				return false;
			}
			return true;
		}

		function preencherComboUnidade() {
			document.form.fireEvent('carregaUnidade');
		}

		function imprimirRelatorio(formato) {
			var contrato = document.getElementById("idContrato");

			if (contrato.value == ""){
				alert(i18n_message("solicitacaoservico.validacao.contrato"));
				contrato.focus();
				return false;
			}

			document.form.formatoArquivoRelatorio.value = formato.value;
			$("#idColaborador > option").attr("selected", true);

			JANELA_AGUARDE_MENU.show();
			document.form.fireEvent("imprimirRelatorio");
		}
  	
