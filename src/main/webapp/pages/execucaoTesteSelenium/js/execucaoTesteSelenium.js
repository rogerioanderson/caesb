/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
addEvent(window, "load", load, false);

/**
 * Inicia a tela
 */
function load() {
	
	//Esconde as divs
	$("#divCadastrosGerais").hide('slow');
	$("#divGerenciaDeCatalogoDeServico").hide('slow');
	$("#divGerenciaDeConfiguracao").hide('slow');
	$("#divGerenciaDeConhecimento").hide('slow');
	$("#divGerenciaDeIncidente").hide('slow');
	$("#divGestaoContrato").hide('slow');
	
}

function mostrarDiv(){
	
	esconderDiv();
	
	switch($("#pacotesDeTeste option:selected").val()) {
	
		case "0":
			$("#divCadastrosGerais").show('slow');
			break;
		case "1":
			$("#divGerenciaDeCatalogoDeServico").show('slow');
			break;
		case "2":
			$("#divGerenciaDeConfiguracao").show('slow');
			break;
		case "3":
			$("#divGerenciaDeConhecimento").show('slow');
			break;
		case "4":
			$("#divGerenciaDeIncidente").show('slow');
			break;
		case "5":
			$("#divGestaoContrato").show('slow');
			break;
	}
	
}

function esconderDiv(){
	
	$("#divCadastrosGerais").hide('slow');
	$("#divGerenciaDeCatalogoDeServico").hide('slow');
	$("#divGerenciaDeConfiguracao").hide('slow');
	$("#divGerenciaDeConhecimento").hide('slow');
	$("#divGerenciaDeIncidente").hide('slow');
	$("#divGestaoContrato").hide('slow');
	
}

function limparFormulario() {
	
	document.form.clear();
	mostrarDiv();
	document.form.fireEvent("load");
	
}

function executarRotina() {
	JANELA_AGUARDE_MENU.show();
	document.form.fireEvent("executarRotina");
}

function atualizarDadosGrafico(total, sucesso, falha, totalPct, sucessoPct, falhaPct) {
	
	$(".totalQtd").text(total);
	$(".sucessoQtd").text(sucesso);
	$(".falhaQtd").text(falha);
	
	$(".totalPct").width(totalPct+"%");
	$(".sucessoPct").width(sucessoPct+"%");
	$(".falhaPct").width(falhaPct+"%");

	$('.tabsbar a[href="#tabs-2"]').tab('show');
	
}

function atualizaLabel(labelSucesso, labelFalha){

	$('#testesExecutadosComSucesso').text(labelSucesso);
	$('#testesExecutadosComFalha').text(labelFalha);
	
}
