<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>

<%
		request.setCharacterEncoding("UTF-8");
		response.setHeader("Content-Language", "lang");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	
		<%@include file="/include/header.jsp"%>

		<%@include file="/include/security/security.jsp"%>
		<title><fmt:message key="citcorpore.comum.title" /></title>
		<%@include file="/include/javaScriptsComuns/javaScriptsComuns.jsp"%>
	
	</head>
	
	<cit:janelaAguarde id="JANELA_AGUARDE_MENU"  title="" style="display:none;top:325px;width:300px;left:500px;height:50px;position:absolute;"></cit:janelaAguarde>
	
	<body>
	
		<div id="wrapper">
			<%@include file="/include/menu_vertical.jsp"%>
			<!-- Conteudo -->
			<div id="main_container" class="main_container container_16 clearfix">
				<%@include file="/include/menu_horizontal.jsp"%>
				<div class="flat_area grid_16">
					<h2>
						<fmt:message key="execucaoTesteSelenium" />
					</h2>
				</div>
							
				<div class="box grid_16 tabs">
					<ul class="tab_header clearfix">
						<li><a href="#tabs-1"><fmt:message key="execucaoTesteSelenium.executarTestes" /></a></li>
						<li><a href="#tabs-2" class="round_top"><fmt:message key="execucaoTesteSelenium.resultadoTeste" /></a></li>
					</ul>
					<a href="#" class="toggle">&nbsp;</a>
					<div class="toggle_container">
						<div id="tabs-1" class="block">
							<div class="">
								<form name='form' action='${ctx}/pages/execucaoTesteSelenium/execucaoTesteSelenium'>
									<div class="span3">
								
										<label class="strong campoObrigatorio " ><fmt:message key='usuario.usuario'/></label>
										<div>													
											<input type="text" id="usuarioDoTeste" name="usuarioDoTeste" />
										</div>
												
										<label class="strong campoObrigatorio " ><fmt:message key='usuario.senha'/></label>
										<div>													
											<input type="password" id="senhaDoTeste" name="senhaDoTeste" />
										</div>
												
										<label class="strong campoObrigatorio " ><fmt:message key='execucaoTesteSelenium.navegador'/></label>
										<div>													
											<select name="navegadorDeExecucao" id ="navegadorDeExecucao" ></select>
										</div>
																
									</div>
															
									<div class="span1" ></div>
										
									<div class="span6">
										
										<label class="strong campoObrigatorio " ><fmt:message key='execucaoTesteSelenium.pacoteDeTeste'/></label>
										<div>													
											<select name="pacotesDeTeste" id ="pacotesDeTeste" class="span12" onchange="mostrarDiv()"></select>
										</div>
													
										<div id="divCadastrosGerais" >
											<div>
												<table>
													<tr><td><input type='checkbox' checked="checked"  name='cargos' id='cargos'/></td><td><fmt:message key="cargo.cargo" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='clientes' id='clientes'/></td><td><fmt:message key="lookup.cliente" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='colaborador' id='colaborador'/></td><td><fmt:message key="menu.nome.colaborador" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='empresa' id='empresa'/></td><td><fmt:message key="menu.nome.empresa" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='fornecedor' id='fornecedor'/></td><td><fmt:message key="contrato.fornecedor" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='unidade' id='unidade'/></td><td><fmt:message key="menu.nome.unidade" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='usuario' id='usuario'/></td><td><fmt:message key="menu.nome.usuario" /></td></tr>
												</table>
											</div>
										</div>
																	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
										<div id="divGerenciaDeCatalogoDeServico">
											<div>
												<table>
													<tr><td><input type='checkbox' checked="checked"  name='tipoEventoServico' id='tipoEventoServico'/></td><td><fmt:message key="menu.nome.tipoEventoServico" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='categoriaServico' id='categoriaServico'/></td><td><fmt:message key="menu.nome.categoriaServico" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='categoriaSolucao' id='categoriaSolucao'/></td><td><fmt:message key="citcorpore.comum.categoriaSolucao" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='causaIncidente' id='causaIncidente'/></td><td><fmt:message key="visao.causaIncidentes" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='condicaoOperacao' id='condicaoOperacao'/></td><td><fmt:message key="menu.nome.condicaoOperacao" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='importanciaNegocio' id='importanciaNegocio'/></td><td><fmt:message key="importanciaNegocio.importanciaNegocio" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='justificativaSolicitacao' id='justificativaSolicitacao'/></td><td><fmt:message key="visao.justificativaSolicitacao" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='localExecucaoServico' id='localExecucaoServico'/></td><td><fmt:message key="visao.localExecucaoServico" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='prioridade' id='prioridade'/></td><td><fmt:message key="solicitacaoServico.prioridade" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='situacao' id='situacao'/></td><td><fmt:message key="servico.situacao" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='tipoServico' id='tipoServico'/></td><td><fmt:message key="menu.nome.tipoServico" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='tipoSolicitacaoServico' id='tipoSolicitacaoServico'/></td><td><fmt:message key="menu.nome.tipoSolicitacaoServico" /></td></tr>
												</table>
											</div>
										</div>
										
										<div id="divGerenciaDeConfiguracao">
											<div>
												<table>
													<tr><td><input type='checkbox' checked="checked"  name='caracteristica' id='caracteristica'/></td><td><fmt:message key="citcorpore.comum.caracteristica" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='grupoItem' id='grupoItem'/></td><td><fmt:message key="grupoItemConfiguracao.grupo" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='midiaSoftware' id='midiaSoftware'/></td><td><fmt:message key="menu.midiaSoftware" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='softwareListaNegra' id='softwareListaNegra'/></td><td><fmt:message key="menu.nome.softwareListaNegra" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='tipoItemConfiguracao' id='tipoItemConfiguracao'/></td><td><fmt:message key="menu.nome.tipoItemConfiguracao" /></td></tr>
												</table>
											</div>
										</div>		
										
										<div id="divGerenciaDeConhecimento">
											<div>
												<table>
													<tr><td><input type='checkbox' checked="checked"  name='categoriaGaleria' id='categoriaGaleria'/></td><td><fmt:message key="menu.nome.categoriaGaleriaImagens" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='palavraGemea' id='palavraGemea'/></td><td><fmt:message key="palavraGemea.palavraGemea" /></td></tr>
												</table>
											</div>
										</div>											
										
										<div id="divGerenciaDeIncidente">
											<div>
												<table>
													<tr><td><input type='checkbox' checked="checked"  name='origem' id='origem'/></td><td><fmt:message key="menu.nome.origemSolicitacoes" /></td></tr>
												</table>
											</div>
										</div>						
																
										<div id="divGestaoContrato">
											<div>
												<table>
													<tr><td><input type='checkbox' checked="checked"  name='formulaOS' id='formulaOS'/></td><td><fmt:message key="menu.nome.formulaOs" /></td></tr>
													<tr><td><input type='checkbox' checked="checked"  name='formula' id='formula'/></td><td><fmt:message key="menu.nome.formula" /></td></tr>
												</table>
											</div>
										</div>
																	
									</div>
									
									<div class="separator top"></div>
							
									<div class="row-fluid">
										<div class="span12">
											<button type='button' name='btnLimpar' class="light" onclick='document.form.fireEvent("load");'>
												<img src="${ctx}/template_new/images/icons/small/grey/clear.png">
												<span><fmt:message key="citcorpore.comum.limpar" /></span>
											</button>
											<button type='button' name='btnExecutarRotina' class="light" onclick='executarRotina();'>
												<img src="${ctx}/template_new/images/icons/small/grey/clear.png">
												<span><fmt:message key="execucaoTesteSelenium.executarTestes" /></span>
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
											
						<div id="tabs-2" class="block">
					
							<div id="resultados">
						
								<div class="span6">
									<table>
										<th class="shortRight" ><fmt:message key="pagamentoProjeto.total" /></th>
										<td>
											<div class="progress">
												<div class="bar totalPct" style="width: 0%;"></div>
											</div>
										</td>
										<td class="totalQtd"></td>
									</table>
								</div>
								
								<div class="span6">
									<table>
										<th class="shortRight" ><fmt:message key="execucaoTesteSelenium.sucesso" /></th>
										<td>
											<div class="progress progress-success">
												<div class="bar sucessoPct" style="width: 0%;"></div>
											</div>
										</td>
										<td class="sucessoQtd"></td>
									</table>
								</div>					
								
								<div class="span6">
									<table>
										<th class="shortRight" ><fmt:message key="execucaoTesteSelenium.falha" /></th>
										<td>
											<div class="progress progress-danger">
												<div class="bar falhaPct" style="width: 0%;"></div>
											</div>
										</td>
										<td class="falhaQtd"></td>
									</table>
								</div>									
								
								<div class="row-fluid">
							
									<div class="span6">
										<div class="box-generic">															
											<div id="testesExecutadosComSucesso" name="testesExecutadosComSucesso"></div>
										</div>
									</div>
									<div class="span6">
										<div class="box-generic">
											<div id="testesExecutadosComFalha" name="testesExecutadosComFalha"></div>
										</div>
									</div>
														
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	<!--  Fim conteudo-->
	<%@include file="/include/footer.jsp"%>
	<script type="text/javascript" src="js/execucaoTesteSelenium.js"></script>
	</body>
</html>
