<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>

<%
	String iframe = "";
	iframe = request.getParameter("iframe");
%>
<!doctype html public "">
<html>
	<head>
		<%@include file="/novoLayout/common/include/libCabecalho.jsp" %>
		<%@include file="/novoLayout/common/include/titulo.jsp" %>
		<link type="text/css" rel="stylesheet" href="../../novoLayout/common/include/css/template.css"/>
		<link type="text/css" rel="stylesheet" href="./css/relatorioQuantitativoServicoAnalitico.css" />
	</head>
	<cit:janelaAguarde id="JANELA_AGUARDE_MENU"  title="" style="display:none;top:325px;width:300px;left:500px;height:50px;position:absolute;"></cit:janelaAguarde>
	<body>
		<div class="<%=(iframe == null) ? "container-fluid fixed" : "" %>">

			<!-- Top navbar (note: add class "navbar-hidden" to close the navbar by default) -->
			<div class="navbar <%=(iframe == null) ? "main" : "nomain" %> hidden-print">
				<% if(iframe == null) { %>
					<%@include file="/novoLayout/common/include/cabecalho.jsp" %>
					<%@include file="/novoLayout/common/include/menuPadrao.jsp" %>
				<% } %>
			</div>

			<div id="wrapper" class="<%=(iframe == null) ? "" : "nowrapper" %>">

				<!-- Inicio conteudo -->
				<div id="content">
					<div class="separator top"></div>	
						<div class="row-fluid">
							<div class="innerLR">
								<div class="widget">
									<div class="widget-body collapse in">	
										<div class="tabsbar">
											<ul>
												<li class="active"><a href="#tab1-3" data-toggle="tab"><fmt:message key="relatorioQuantitativo.relatorioQuantitativoServicoConcluidosAnalitico"/></a></li>
											</ul>
										</div>
										<div class="tab-content">
											<div class="tab-pane active" id="tab1-3">
												<div id="parametros">
													<form name="form" action="${ctx}/pages/relatorioQuantitativoServicoAnalitico/relatorioQuantitativoServicoAnalitico">
														<div class='row-fluid'>
															<div class='span12'>
																<div class='span4'>
																	<label class="strong campoObrigatorio"><fmt:message key="pesquisasolicitacao.periodoEncerramento" /></label>
																	<input type="text" class=" span4 citdatepicker" id="dataInicio" name="dataInicio" maxlength="10" required="required"  style="cursor: pointer; background: #FFF;">
																		&nbsp;<fmt:message key='citcorpore.comum.a' />&nbsp;
																	<input type="text" class=" span4 citdatepicker" id="dataFim" name="dataFim" maxlength="10" required="required" style="cursor: pointer; background: #FFF;" >
																																		
																</div>
															</div>
														</div>
														<div class='row-fluid'>
															<div class='span12'>
																<div class="span3">
																	<label class="controle_label_this_bold"><fmt:message key="contrato.contrato" /></label>
																	<div>
																		<select name="idContrato" class="span12"></select>
																	</div>
																</div>
																
																<div class="span3">
																	<label class="controle_label_this_bold"><fmt:message key="citcorpore.comum.tipoDemandaServico" /></label>
																	<div>
																		<select name="idTipoDemandaServico" class="span12"></select>
																	</div>
																</div>
																
																<div class="span3">
																	<label class="controle_label_this_bold"><fmt:message key="citcorpore.comum.topList" /></label>
																	<div >
																		<select name="topList" class="span12"></select>
																	</div>
																</div>
															</div>
														</div>
														
														<div style="padding-bottom: 19px !important;"></div>
														
														<div class='row-fluid'>
															<div class='span12'>
																<button type='button' name='btnRelatorio' class="lFloat btn btn-icon btn-primary"
																		onclick="imprimirRelatorioQuantitativo();">
																	<img src="${ctx}/template_new/images/icons/small/util/file_pdf.png">
																	<i></i><fmt:message key="citcorpore.comum.gerarrelatorio"/>
																</button>
																
																<button type='button' name='btnRelatorio' class="lFloat btn btn-icon btn-primary"
																		onclick="imprimirRelatorioQuantitativoXls();">
																	<img src="${ctx}/template_new/images/icons/small/util/excel.png">
																	<i></i><fmt:message key="citcorpore.comum.gerarrelatorio"/>
																</button>
																
																<button type='button' name='btnLimpar' class="lFloat btn btn-icon btn-primary"
																		onclick="limpar()">
																	<img src="${ctx}/template_new/images/icons/small/grey/clear.png">
																	<i></i><fmt:message key="citcorpore.comum.limpar"/>
																</button>
															
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<!--  Fim conteudo-->
				<%@include file="/novoLayout/common/include/rodape.jsp" %>
				<script type="text/javascript" src="./js/relatorioQuantitativoServicoAnalitico.js"></script>
			</div>
		</div>
	</body>
</html>
