/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/

			var popup;
		    addEvent(window, "load", load, false);
		    function load(){
				popup = new PopupManager(850, 500, "${ctx}/pages/");
		    }

			exportar =  function(){
			document.form.fireEvent('exportarParametroCsv');
			JANELA_AGUARDE_MENU.show();
			}

			salvar =  function(){
				var pass = document.getElementById("valor");
				if(pass.type == 'password'){
					removeAllEventos()
					limpaElemntoValidacao();
					$('#valor').unmask();
					$('#valor').removeClass();
					removeAllEventos();
				}
				document.form.save();
			}

			function LOOKUP_PARAMETROCORPORE_select(id, desc) {
				document.form.restore({
					id : id
				});
			}

			function limpaCaracteristica(){
				removeAllEventos()
				limpaElemntoValidacao();
				$('#valor').unmask();
				$('#valor').removeClass();
				removeAllEventos();
				 $("#valor").attr('maxlength','200');
			}
			function MudarCampovalorParaTipoSenha(){
				removeAllEventos()
				limpaElemntoValidacao();
				$('#valor').unmask();
				$('#valor').removeClass();
				removeAllEventos();
				var pass = document.getElementById("valor");
				pass.type = 'password';
				//$('#valor').attr('type', 'password');
			}
			function MudarCampovalorParaTipoTexto(){
				removeAllEventos()
				limpaElemntoValidacao();
				$('#valor').unmask();
				$('#valor').removeClass();
				removeAllEventos();
				var pass = document.getElementById("valor");
				pass.type = 'text';
			}
			function limpaElemntoValidacao(){
				var element = document.form.valor;
				var aux = element.validacao;
				if (aux == null || aux == undefined){
					element.validacao = '';
				}else{
					element.validacao = '';
				}

				var aux = element.descricao;
				if (aux == null || aux == undefined){
					element.descricao = '';
				}else{
					element.descricao = '';
				}
			}

		    function setaLingua(){
		    	document.getElementById("pesqLockupLOOKUP_PARAMETROCORPORE_SIGLA_LINGUA").value = document.formPesquisa.siglaLingua.value;
		    }

			function mascara(tipo){

				if(tipo == "Date"){
				 	$("#valor").mask("99/99/9999");
				    $("#valor").attr('maxlength','10');

				} else if(tipo == "CPF"){
			 		$("#valor").mask("999.999.999-99");
					$("#valor").attr('maxlength','14');

				} else if(tipo == "Telefone"){
					$("#valor").mask("(99)9999-9999");
					$("#valor").attr('maxlength','13');

				} else if(tipo == "Hora"){
					 $("#valor").mask('99:99');
					$("#valor").attr('maxlength','5');

				} else if(tipo == "Numero"){
					$("#valor").mask("999999999999999999999999999999");
					$("#valor").attr('maxlength','30');

				} else if(tipo == "CNPJ"){
				 	$("#valor").mask("99.999.999/9999-99");
					$("#valor").attr('maxlength','17');
				} else if(tipo == "CEP"){
				 	$("#valor").mask("99.999-999");
					$("#valor").attr('maxlength','10');
				}
			}

			function removeAllEventos(){
				document.getElementById('valor').removeEventListener('keydown',DEFINEALLPAGES_formataMoedaSaidaCampo, false);
				document.getElementById('valor').removeEventListener('keydown',DEFINEALLPAGES_formataDecimalSaidaCampo, false);
				document.getElementById('valor').removeEventListener('keydown',DEFINEALLPAGES_formataNumero, false);
				document.getElementById('valor').removeEventListener('keydown',DEFINEALLPAGES_formataData, false);
				document.getElementById('valor').removeEventListener('keydown',DEFINEALLPAGES_formataHora, false);
				document.getElementById('valor').removeEventListener('keydown',DEFINEALLPAGES_formataCNPJ, false);
				document.getElementById('valor').removeEventListener('keydown',DEFINEALLPAGES_formataCPF, false);
				document.getElementById('valor').removeEventListener('keydown',DEFINEALLPAGES_formataMoeda, false);

				document.getElementById('valor').removeEventListener('blur',DEFINEALLPAGES_formataMoedaSaidaCampo, false);
				document.getElementById('valor').removeEventListener('blur',DEFINEALLPAGES_formataDecimalSaidaCampo, false);
				document.getElementById('valor').removeEventListener('blur',DEFINEALLPAGES_formataNumero, false);
				document.getElementById('valor').removeEventListener('blur',DEFINEALLPAGES_formataData, false);
				document.getElementById('valor').removeEventListener('blur',DEFINEALLPAGES_formataHora, false);
				document.getElementById('valor').removeEventListener('blur',DEFINEALLPAGES_formataCNPJ, false);
				document.getElementById('valor').removeEventListener('blur',DEFINEALLPAGES_formataCPF, false);
				document.getElementById('valor').removeEventListener('blur',DEFINEALLPAGES_formataMoeda, false);
			}

		
