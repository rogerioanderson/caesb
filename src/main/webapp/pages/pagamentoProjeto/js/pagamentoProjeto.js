/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/


var objTab = null;

addEvent(window, "load", load, false);

function load() {
	document.form.afterRestore = function() {
	}
}

function selecionaProjeto(obj){
	document.form.idProjeto.value = obj.value;
	document.form.fireEvent('getInformacoes');
}

function refreshInfo(){
	$("#POPUP_EDITAR").dialog("close");
	$("#POPUP_MUDA_SIT_PAGAMENTO").dialog("close");
	document.getElementById("divTarefasParaPagamento").innerHTML = "";
	document.getElementById("divTarefasParaPagamentoVis").innerHTML = "";
	document.getElementById("divPagamentosEfetuados").innerHTML = "";
	document.form.fireEvent('getInformacoes');
}

function limpar(){
	document.getElementById("divTarefasParaPagamento").innerHTML = "";
	document.getElementById("divTarefasParaPagamentoVis").innerHTML = "";
	document.getElementById("divPagamentosEfetuados").innerHTML = "";
	document.formConsulta.clear();
	document.form.clear();
}

function gerarPagamento(){
	if (document.form.idProjeto.value == ''){
		alert(i18n_message("pagamentoProjeto.informeProjeto"));
		return;
	}
	document.form.fireEvent('getMarcosFinanceiros');
	$("#POPUP_EDITAR").dialog("open");
	//document.form.save();
}

function selecionaMarcoFin(){
	HTMLUtils.setForm(document.form);
	document.form.fireEvent('setaTarefasMarcoFinanceiro');
}

function save(){
	document.form.save();
}
function saveAtu(){
	if (document.formAtuSit.validate()){
		document.formAtuSit.fireEvent('atualizaPagamento');
	}
}
function indicarPagamento(id){
	document.formAtuSit.idPagamentoProjeto.value = id;
	$("#POPUP_MUDA_SIT_PAGAMENTO").dialog("open");
}

$(function() {
	$("#POPUP_EDITAR").dialog({
		autoOpen : false,
		width : 1100,
		height : 700,
		modal : true,
		buttons: {
			"Fechar": function() {
				$( this ).dialog( "close" );
			}
		}
	});
	$("#POPUP_MUDA_SIT_PAGAMENTO").dialog({
		autoOpen : false,
		width : 500,
		height : 300,
		modal : true,
		buttons: {
			"Fechar": function() {
				$( this ).dialog( "close" );
			}
		}
	});
});


