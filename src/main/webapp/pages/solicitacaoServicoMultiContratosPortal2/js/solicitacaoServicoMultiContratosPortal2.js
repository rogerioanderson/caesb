/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
function cancelar() {
	//parent.fecharModalNovaSolicitacao();
	parent.$('#modal_novaSolicitacao').modal('hide');
	JANELA_AGUARDE_MENU.hide();
}
function abrir() {
	parent.abrirModalNovaSolicitacao();
}
function mostraMensagemInsercao(param) {
	bootbox.alert(param, function(result) {
		parent.fecharModalNovaSolicitacao();
	});
}
function validarCampoDescricao() {
	if (/^\s*$/g.test($('#descricao').val())) {
		return false;
	}

	ajustaTamanho();
	return true;
};
function salvar() {
	JANELA_AGUARDE_MENU.show();
	if (validarCampoDescricao()) {
		document.form.save();
	} else {
		JANELA_AGUARDE_MENU.hide();
		alert(i18n_message("solicitacaoServico.informedescricao"));
		var editor = new wysihtml5.Editor("descricao");
		editor.focus(true);
	}
}

function ajustaTamanho() {
	if ($('#descricao').val().length > 40000) {
		$('#descricao').val($('#descricao').val().substring(0, 40000));
		alert(i18n_message("solicitacaoServico.descricaoUltrapassouLimite"));
	}
}
