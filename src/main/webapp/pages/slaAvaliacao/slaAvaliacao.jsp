<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>

<!doctype html>
<html>
<head>
<%
	//identifica se a p�gina foi aberta a partir de um iframe (popup de cadastro r�pido)
	String iframe = "";
	iframe = request.getParameter("iframe");

%>
<%@include file="/include/header.jsp"%>

<%@include file="/include/security/security.jsp" %>
<title><fmt:message key="citcorpore.comum.title"/></title>

<%@include file="/include/javaScriptsComuns/javaScriptsComuns.jsp"%>

<script type="text/javascript" src="${ctx}/js/UploadUtils.js"></script>
<script type="text/javascript" src="${ctx}/js/PopupManager.js"></script>


<script type="text/javascript" src="./js/slaAvaliacao.js"></script>

<%
//se for chamado por iframe deixa apenas a parte de cadastro da p�gina
if(iframe != null){%>
<link rel="stylesheet" type="text/css" href="./css/slaAvaliacao.css" />
<%}%>

</head>

<body>
	<div id="wrapper">
	<%if(iframe == null){%>
		<%@include file="/include/menu_vertical.jsp"%>
	<%}%>
<!-- Conteudo -->
	<div id="main_container">
	<%if(iframe == null){%>
		<%@include file="/include/menu_horizontal.jsp"%>
	<%}%>

		<%@include file="/include/menu_vertical.jsp"%>
		<!-- Conteudo -->
		<div id="main_container" class="main_container container_16 clearfix" style='height: 0px!important;top: 5%!important;'>
			<div class="box grid_16 tabs">
				<ul class="tab_header clearfix">
					<li><a href="#tabs-1"><fmt:message key="menu.nome.avaliarSLA" /></a></li>
				</ul>
				<div class="toggle_container">
					<div class="block" >
						<div id="tabs-1" class="block">
						<div class="section">
							<form name='form' action='${ctx}/pages/slaAvaliacao/slaAvaliacao'>
								<fieldset>
									<legend><fmt:message key="citcorpore.comum.filtros" /></legend>
									<table>
										<tr>
											<td>
												<fmt:message key="avaliacaocontrato.periodo" /><font color="red">*</font>
											</td>
											<td>
												<input type='text' name='dataInicio' id ='dataInicio'  size="10" maxlength="10" class="Valid[Required,Date] Description[avaliacao.fornecedor.dataInicio] Format[Date] datepicker" />
											</td>
											<td>
												<input type='text' name='dataFim' id = 'dataFim' size="10" maxlength="10" class="Valid[Required,Date] Description[avaliacao.fornecedor.dataFim] Format[Date] datepicker" />
											</td>
											<td>
												&nbsp;
											</td>
											<td style='vertical-align: top;'>
												<button type="button" onclick='gerarInformacoes()'>
													<fmt:message key="citcorpore.comum.gerarInformacoes" />
												</button>
											</td>
										</tr>
									</table>
								</fieldset>
								<div id='divInfo' style="overflow: auto!important;">
								</div>
							</form>
							</div>
						</div>
					</div>
					<!-- ## FIM - AREA DA APLICACAO ## -->
				</div>
			</div>
		</div>
		<!-- Fim da Pagina de Conteudo -->
	</div>
		</div>
		<!-- Fim da Pagina de Conteudo -->

	<%@include file="/include/footer.jsp"%>
</body>
</html>

