/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
var temporizador;
addEvent(window, "load", load, false);
function load() {
	$("#POPUP_SOLICITANTE").dialog({
		autoOpen : false,
		width : 600,
		height : 400,
		modal : true
	});

	$("#POPUP_RESPONSAVEL").dialog({
		autoOpen : false,
		width : 600,
		height : 400,
		modal : true
	});

	$("#POPUP_ITEMCONFIG").dialog({
		autoOpen : false,
		width : 600,
		height : 400,
		modal : true
	});

	$("#POPUP_OCORRENCIAS").dialog({
		autoOpen : false,
		width : 800,
		height : 600,
		modal : true
	});

	$("#POPUP_UNIDADE").dialog({
		autoOpen : false,
		width : 600,
		height : 400,
		modal : true
	});
	$("#POPUP_menuAnexos").dialog({
		autoOpen : false,
		width : 600,
		height : 400,
		modal : true
	});
	$("#POPUP_EMPREGADO").dialog({
		autoOpen : false,
		width : 600,
		height : 400,
		modal : true
	});

	$('#contentFrameReqMudanca')
			.html(
					'<iframe src="about:blank" id="frameReqMudanca" width="100%" class="iframeSemBorda"></iframe>');

	ajustarModalReqMudanca();

}

function anexos(idRequisicao) {
	document.form.idRequisicaoMudanca.value = idRequisicao;
	document.form.fireEvent("restoreUpload");
}

function LOOKUP_UNIDADE_SOLICITACAO_select(id, desc) {
	document.form.idUnidade.value = id;
	document.form.nomeUnidade.value = desc.split(" - ")[0];
	$("#POPUP_UNIDADE").dialog("close");
}

function LOOKUP_PESQUISAITEMCONFIGURACAO_select(id, desc) {
	document.getElementById("idItemConfiguracao").value = id;
	document.getElementById("nomeItemConfiguracao").value = desc;
	$("#POPUP_ITEMCONFIG").dialog("close");
}

function abrePopupIC() {
	$("#POPUP_ITEMCONFIG").dialog("open");
}
function inicializarTemporizador() {
	if (temporizador == null) {
		temporizador = new Temporizador("imgAtivaTimer");
	} else {
		temporizador = null;
		try {
			temporizador.listaTimersAtivos = [];
		} catch (e) {
		}
		try {
			temporizador.listaTimersAtivos.length = 0;
		} catch (e) {
		}
		temporizador = new Temporizador("imgAtivaTimer");
	}
}

function pesquisaRequisicaoMudanca() {
	var dataInicio = document.getElementById("dataInicio").value;
	var dataFim = document.getElementById("dataFim").value;
	var numero = document.getElementById("idRequisicaoMudanca").value;
	if (numero != "") {
		if (dataInicio != "" && dataFim != "") {
			if (validaData()) {
				inicializarTemporizador();
				JANELA_AGUARDE_MENU.show();
				document.form.fireEvent("pesquisaRequisicaoMudanca");
			}
		} else {
			inicializarTemporizador();
			JANELA_AGUARDE_MENU.show();
			document.form.fireEvent("pesquisaRequisicaoMudanca");
		}
	} else if (validaData()) {
		inicializarTemporizador();
		JANELA_AGUARDE_MENU.show();
		document.form.fireEvent("pesquisaRequisicaoMudanca");
	}
}

function imprimirRelatorioRequisicaoMudanca() {
	var dataInicio = document.getElementById("dataInicio").value;
	var dataFim = document.getElementById("dataFim").value;
	var numero = document.getElementById("idRequisicaoMudanca").value;
	document.form.formatoArquivoRelatorio.value = 'pdf';
	if (numero != "") {
		if (dataInicio != "" && dataFim != "") {
			if (validaData()) {
				inicializarTemporizador();
				JANELA_AGUARDE_MENU.show();
				document.form.fireEvent("imprimirRelatorioRequisicaoMudanca");
			}
		} else {
			inicializarTemporizador();
			JANELA_AGUARDE_MENU.show();
			document.form.fireEvent("imprimirRelatorioRequisicaoMudanca");
		}
	} else if (validaData()) {
		inicializarTemporizador();
		JANELA_AGUARDE_MENU.show();
		document.form.fireEvent("imprimirRelatorioRequisicaoMudanca");
	}
}

function imprimirRelatorioRequisicaoMudancaXls() {
	var dataInicio = document.getElementById("dataInicio").value;
	var dataFim = document.getElementById("dataFim").value;
	var numero = document.getElementById("idRequisicaoMudanca").value;
	document.form.formatoArquivoRelatorio.value = 'xls';
	if (numero != "") {
		if (dataInicio != "" && dataFim != "") {
			if (validaData()) {
				inicializarTemporizador();
				JANELA_AGUARDE_MENU.show();
				document.form.fireEvent("imprimirRelatorioRequisicaoMudanca");
			}
		} else {
			inicializarTemporizador();
			JANELA_AGUARDE_MENU.show();
			document.form.fireEvent("imprimirRelatorioRequisicaoMudanca");
		}
	} else if (validaData()) {
		inicializarTemporizador();
		JANELA_AGUARDE_MENU.show();
		document.form.fireEvent("imprimirRelatorioRequisicaoMudanca");
	}
}

function limpar() {
	$('#divTblRequisicaoMudanca').hide();
	document.form.clear();

}

function pageLoad() {
	$(function() {
		$('input.datepicker').datepicker();
	});
}

// popup para pesquisar de unidade

$(function() {
	$("#addUnidade").click(function() {
		$("#POPUP_UNIDADE").dialog("open");
	});
});

/*
 * Reaproveitamento da lookup EMPREGADO
 */
function selecionarSolicitante() {
	LOOKUP_EMPREGADO_select = function(id, desc) {
		document.form.idSolicitante.value = id;
		document.form.nomeSolicitante.value = desc.split("-")[0];
		$("#POPUP_EMPREGADO").dialog("close");
	}

	$("#POPUP_EMPREGADO").dialog("open");
}

function selecionarProprietario() {
	limpar_LOOKUP_EMPREGADO();
	LOOKUP_EMPREGADO_select = function(id, desc) {
		document.form.idProprietario.value = id;
		document.form.nomeProprietario.value = desc.split("-")[0];
		$("#POPUP_EMPREGADO").dialog("close");
	}

	$("#POPUP_EMPREGADO").dialog("open");
}

function mostrarCategoria() {

	document.form.fireEvent('validacaoCategoriaMudanca');

}

visualizarSolicitacao = function(idRequisicao,idTarefa) {
	if (parent.$("#fraRequisicaoMudanca").length) {
		parent.visualizarSolicitacao(idRequisicao,idTarefa);
		parent.$("#popupCadastroRapido").dialog("close");
	} else {
		JANELA_AGUARDE_MENU.show();
		
		if(typeof idTarefa === "undefined")
			document.getElementById('frameReqMudanca').src = ctx + "/pages/requisicaoMudanca/requisicaoMudanca.load?idRequisicaoMudanca="+ idRequisicao + "&escalar=S&alterarSituacao=N&editar=RO";
		else
			document.getElementById('frameReqMudanca').src = ctx + "/pages/requisicaoMudanca/requisicaoMudanca.load?idRequisicaoMudanca="+ idRequisicao + "&idTarefa=" + idTarefa +"&escalar=S&alterarSituacao=N&editar=RO";
		$('#modal_reqMudanca').modal('show');
	}
}

function ajustarModalReqMudanca() {
	$('#modal_reqMudanca').css({
		'width' : '97%',
		'height' : 'auto',
		'margin-top' : '0% !important',
		'margin-left' : '-48.5%'
	});

	$('#modal_reqMudanca .modal-body').css('max-height', '92%');
	$('#frameReqMudanca').attr('style', 'min-height: 800px !important;');
}
