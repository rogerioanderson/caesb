/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**Autocomplete **/
var completeServico;
var completeSolicitante;
var tipoDemanda;
var contrato;

/**Monta os parametros para a buscas do autocomplete**/
function montaParametrosAutocompleteServico(){
	contrato =  $("#idContrato").val();
	tipoDemanda = $("#idTipoDemandaServico").val();
	completeServico.setOptions({params: {contrato: contrato, tipoDemanda: tipoDemanda} });
}

function montaParametrosAutocompleteSolicitante(){
	contrato =  $("#idContrato").val();
	completeSolicitante.setOptions({params: {contrato: contrato} });
}

$(document).ready(function() {
	$('#nomeServico').on('click', function(){
		montaParametrosAutocompleteServico();
	});
	completeServico = $('#nomeServico').autocomplete({ 
		serviceUrl:'pages/autoCompleteServico/autoCompleteServico.load',
		noCache: true,
		onSelect: function(value, data){
			$('#idServico').val(data);
			$('#nomeServico').val(value);
		}
	});
	
	$('#solicitante').on('click', function(){
		montaParametrosAutocompleteSolicitante();
	});
	completeSolicitante = $('#solicitante').autocomplete({ 
		serviceUrl:'pages/autoCompleteSolicitante/autoCompleteSolicitante.load',
		noCache: true,
		onSelect: function(value, data){
			$('#idSolicitante').val(data);
			$('#solicitante').val(value);
		}
	});
});

function limpar(){
	document.form.clear();
	document.getElementById("Resumida").checked=true;
}

function alimentaVisualizacao(){
	if (document.getElementById("Resumida").checked){
		document.getElementById("visualizacao").value = "R";
	} else {
		document.getElementById("visualizacao").value = "A";
	}
}

function valida(){
	if ((document.getElementById('idRelatorio').value=='')||(document.getElementById('idRelatorio').value=='0')){
		alert(i18n_message("relatorioTop10IncidentesRequisicoes.selecioneRelatorio"));
		return false;
	}
	if (document.getElementById('dataInicial').value==''){
		alert(i18n_message("citcorpore.comum.validacao.datainicio"));
		return false;
	}
	if (document.getElementById('dataFinal').value==''){
		alert(i18n_message("citcorpore.comum.validacao.datafim"));
		return false;
	}
	if (document.getElementById('solicitante').value==''){
		document.getElementById('idSolicitante').value='0';
	}
	if (document.getElementById('nomeServico').value==''){
		document.getElementById('idServico').value='0';
	}
/*
   N�o est� validando porque ainda precisamos criar uma estrutura para validar a data na internacionaliza��o
  	if (!DateTimeUtil.isValidDate(document.getElementById('dataInicial').value)){
		alert(i18n_message("citcorpore.comum.datainvalida"));
		return false;
	}

  	if (!DateTimeUtil.isValidDate(document.getElementById('dataFinal').value)){
		alert(i18n_message("citcorpore.comum.dataFinalInvalida"));
		return false;
	}
*/	
	return true;
}

function gerarRelatorio(formato){
	if (valida()){
		alimentaVisualizacao();
		document.getElementById('formato').value=formato;
		JANELA_AGUARDE_MENU.show();
		document.form.fireEvent("gerarRelatorio");
	}
}

function preencherComboUnidade(opcao){
	if(opcao.value!=""){
		document.form.fireEvent("preencherComboUnidade");
	}
}

function configurarObjetos(relatorio){
	if(relatorio.value!=""){
		document.form.fireEvent("configuraObjetos");
	}
}
