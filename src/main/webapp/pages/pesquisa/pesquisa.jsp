<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.citframework.util.Constantes"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>
<!doctype html public "">
<html>
<head>
	<%@include file="/include/header.jsp"%>
	<%@include file="/include/security/security.jsp" %>
	<%@include file="/include/javaScriptsComuns/javaScriptsComuns.jsp"%>
	<title><fmt:message key="citcorpore.comum.title"/></title>
	<%@include file="/include/menu_vertical.jsp"%>
	<%@include file="/include/footer.jsp"%>
	<%@include file="/include/menu_horizontal.jsp"%>
	<%@include file="/include/welcome.jsp"%>
	<style type="text/css">
		#result li ul {
			margin:10px;
		}
	</style>
</head>
<body>
	<div id="wrapper">
		<!-- Conteudo -->
		<div id="main_container" class="main_container container_16 clearfix">
			<div id="result" style="letter-spacing: 0px;"></div>
		</div>
		<!-- Fim da Pagina de Conteudo -->
	</div>
</body>
</html>
