<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.bean.UploadDTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collection"%>
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>

<%@ include file="/WEB-INF/templates/taglibs.jsp"%>

<html>
<head>
	<%@include file="/include/cssComuns/cssComuns.jsp" %>

	<style type="text/css">
	body {
	background-color: white;
	background-image: url("");
	}
	.table {
		border-left:1px solid #ddd;
	}

	.table th {
		border:1px solid #ddd;
		padding:4px 10px;
		border-left:none;
		background:#eee;
	}

	.table td {
		border:1px solid #ddd;
		padding:4px 10px;
		border-top:none;
		border-left:none;
	}

	.tableLess {
	  font-family: arial, helvetica !important;
	  font-size: 10pt !important;
	  cursor: default !important;
	  margin: 0 !important;
	  background: white !important;
	  border-spacing: 0  !important;
	  width: 100%  !important;
	}

	.tableLess tbody {
	  background: transparent  !important;
	}

	.tableLess * {
	  margin: 0 !important;
	  vertical-align: middle !important;
	  padding: 2px !important;
	}

	.tableLess thead th {
	  font-weight: bold  !important;
	  background: #fff url(../../imagens/title-bg.gif) repeat-x left bottom  !important;
	  text-align: center  !important;
	}

	.tableLess tbody tr:ACTIVE {
	  background-color: #fff  !important;
	}

	.tableLess tbody tr:HOVER {
	  background-color: #e7e9f9!important ;
	  cursor: pointer;
	}

	.tableLess th {
	  border: 1px solid #BBB  !important;
	  padding: 6px  !important;
	}

	.tableLess td{
	  border: 1px solid #BBB  !important;
	  padding: 6px 10px  !important;
	}
	a{
		font-size: 11px!important;

	}
	a:HOVER {
		font-size: 11px!important;
		font-weight: bold!important;
}
td{
font-size: 11px!important;

}


	</style>

</head>
<body>
<script>


	function excluirImagemUpload(path){
		if (confirm(i18n_message("citcorpore.comum.confirme.desejaexcluiranexo"))){
			window.location = '${ctx}/pages/uploadExcluirDocsGerais/uploadExcluirDocsGerais.load?path=' + path;
		}
	}
	function obtemArquivoTemporario(path){
		window.location = '${ctx}/pages/visualizarUploadTemp/visualizarUploadTemp.load?path=' + path;
	}
	function excluirAnexo(id, nomeArquivo){
		if(confirm(i18n_message("citcorpore.comum.confirme.exclusaoanexo"))){
			window.location = '${ctx}/pages/upload/excluirAnexo.do?nameFile=' + nomeArquivo + '&id='+id;
		}
	}
	<%
		if(request.getAttribute("acaoRetorno") != null){
	%>
		alert(i18n_message("citcorpore.comum.validacao.anexoexcluido"));
	<%
		}
	%>
</script>
<table width="100%" class='tableLess'>
	<thead>
		<tr>
		  <th>&nbsp;</th>
          <th ><fmt:message key="citcorpore.comum.arquivo" /></th>
          <th><fmt:message key="citcorpore.comum.descricao" /></th>
          <th><fmt:message key="citcorpore.comum.situacao" /></th>
		</tr>
	</thead>
        <%boolean branco = true;%>
      	<%String cor = "#ffffff";%>
      	<%
		Collection colUploadsGED = (Collection)request.getSession(true).getAttribute("colUploadGeraisGED");
		if (colUploadsGED == null){
			colUploadsGED = new ArrayList();
		}
      	%>
      	<%for(Iterator it = colUploadsGED.iterator(); it.hasNext();){ %>
      		<%
      		UploadDTO uploadDTO = (UploadDTO)it.next();
      		%>
		<TR>
			<%
			if(branco){
				cor ="#ffffff";
			}else{
				cor="#E5EBF6";
			}
			%>
			<td >
					<img src='${ctx}/imagens/delete.png' style='cursor:pointer' onclick='excluirImagemUpload("<%=uploadDTO.getPath()%>")' title='<fmt:message key="citcorpore.ged.msg.exluiranexo" /> '/>
			</td>
			<td >
				<a href="#" onclick='obtemArquivoTemporario("<%=uploadDTO.getPath()%>")'><%=uploadDTO.getNameFile()%></a>
			</td>
			<td >
				<%=uploadDTO.getDescricao()%>
			</td>
			<td  title='<fmt:message key="citcorpore.ged.situacao" />'>
				<%=uploadDTO.getSituacao()%>
			</td>
			<%branco=!branco;%>
        </TR>
		<%}%>
</table>
</body>

</html>
