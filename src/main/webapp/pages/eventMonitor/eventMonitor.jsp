<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.citframework.util.Constantes"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>

<html>
<head>
<%@include file="/include/security/security.jsp"%>
<%@include file="/include/javaScriptsComuns/javaScriptsComuns.jsp"%>
<title><fmt:message key="citcorpore.comum.title" /></title>

<script type="text/javascript" src="./js/eventMonitor.js"></script>
</head>

<body>
	<form name='form' id='form' action="${ctx}/pages/eventMonitor/eventMonitor" method="post">
		<input type='hidden' name='idGrupoRecurso'/>
		<input type='hidden' name='nomeGrupoRecurso'/>
		<table width='100%'>
			<tr>
				<td>
					<img src='${ctx}/imagens/logo/logo.png' border='0'/>
				</td>
				<td style='border:1px solid black; background-color: lightgray'>
					<div id='timeRefresh'>
					</div>
				</td>
				<td>
					<div id='divServicesCritical' style='width: 100%;border:1px solid black; height: 60px; overflow: auto'>
					</div>
				</td>
			</tr>
		</table>
		<table width='100%'>
			<tr>
				<td>
					<div id='divGrupos' style='width: 100%;'>

					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id='divDetalhamento' style='width: 100%;'>

					</div>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>

