/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/

	var objTab = null;

	addEvent(window, "load", load, false);
	function load(){
		document.form.afterRestore = function () {
			document.getElementById('tabTela').tabber.tabShow(0);
		}
	}

	function selecionaCategoriaGaleriaImagem(){
		JANELA_AGUARDE_MENU.show();
		document.form.fireEvent('listaImagens');
	}
	function voltar(){
		verificarAbandonoSistema = false;
		window.location = retorno;
	}
	function adicionarImagem(){
		if(document.form.idCategoriaGaleriaImagem.value != ""){
			document.getElementById("arquivo").value = "";
			$('#POPUP_ADD_IMAGEM').dialog('open');
		}else{
			alert(i18n_message("galeriaImagens.selecioneUmaCategoria"));
		}
	}

	function excluirImagem(id){
		document.form.idImagem.value = id;
		if(confirm(i18n_message("citcorpore.comum.deleta"))){
			JANELA_AGUARDE_MENU.show();
			document.form.fireEvent("excluirImagem");
		}
	}

	function fecharPopUpAdicionarImagem(){
		$('#POPUP_ADD_IMAGEM').dialog('close');
	}

	$(function() {
		$("#POPUP_ADD_IMAGEM").dialog({
			autoOpen : false,
			width : 650,
			height : 350,
			modal : true,
			show: "fade",
			hide: "fade"
		});
	});

	carregouIFrameAnexo = function() {
    	selecionaCategoriaGaleriaImagem();
    };
    
    /**
     * alterado por rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
     * data da alteração rcs: 03/07/2015
     * comentário alteração rcs: método alterado, de forma a chama outro método JS que verifica se o usuário selecionou um arquivo para fazer upload.
     */
	submitFormAnexo = function(){
		JANELA_AGUARDE_MENU.show();
		HTMLUtils.addEvent(document.getElementById("frameUpload"),"load", verificarSeFoiSelecionadoArquivoParaSubmit, true);
		document.formAddImagem.setAttribute("target","frameUpload");
		document.formAddImagem.setAttribute("method","post");
	    document.formAddImagem.setAttribute("enctype","multipart/form-data");
	    document.formAddImagem.setAttribute("encoding","multipart/form-data");
	    	
	    //submetendo
	    document.formAddImagem.idCategoriaGaleriaImagem.value = document.form.idCategoriaGaleriaImagem.value;
	    document.formAddImagem.submit();
	};
	
	/**
	 * @author rcs - Analista Desenvolvedor <a href="rafael.soyer@centralit.com.br">rafael.soyer@centralit.com.br</a>
	 * @since 03/07/2015
	 */
	verificarSeFoiSelecionadoArquivoParaSubmit = function(){
		document.formAddImagem.fireEvent("verificaSeFoiSelecionadoArquivoParaSubmit");
		HTMLUtils.removeEvent(document.getElementById("frameUpload"),"load", verificarSeFoiSelecionadoArquivoParaSubmit);
	};
	
