/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/

	var temporizador;
	
	
	$(window).load(function() {
		
		$("#POPUP_SOLICITANTE").dialog({
			autoOpen : false,
			width : 600,
			height : 400,
			modal : true
		});
		
	});	

	function LOOKUP_SOLICITANTE_select(id, desc){
		document.getElementById("idSolicitante").value = id;
		document.getElementById("nomeSolicitante").value = desc;
		$("#POPUP_SOLICITANTE").dialog("close");
	}

	function imprimirRelatorioQuantitativo(formatoRelatorio){
		var contrato = document.getElementById("idContrato").value;
		var tipo     = document.getElementById("tipoRelatorio").value;
		var str_nomeDoRelatorio = "";

		if(contrato == ""){
			alert(i18n_message("solicitacaoservico.validacao.contrato"));
			return false;
		}

		if(tipo == 1){
			str_nomeDoRelatorio = "imprimirCargaHorariaUsuario" + formatoRelatorio;
			imprimirRelatorioPeloNomeDoRelatorio(str_nomeDoRelatorio);
		}else{
			str_nomeDoRelatorio = "imprimirCargaHorariaGrupo" + formatoRelatorio;
			imprimirRelatorioPeloNomeDoRelatorio(str_nomeDoRelatorio);
		}
	}

	function abrePopupUsuario(){
		$("#POPUP_SOLICITANTE").dialog("open");
	}

	function preencherComboGrupo(){
		document.form.fireEvent("preencherComboGrupo");
	}

