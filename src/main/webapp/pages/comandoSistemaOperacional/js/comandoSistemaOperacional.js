/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/


	$(window).load( function() {
		
			document.form.afterRestore = function() {
				$('.tabs').tabs('select', 0);
			};		
		
	});
				
				
				

	function LOOKUP_COMANDOSISTEMAOPEARCIONAL_select(idSO, desc) {
		document.form.restore({id: idSO});
	}

	function gravar() {
		document.form.save();
	}

	//funcoes de tratamento da popup de cadastro rápido

	/*
	*configurações da popup
	*layout: <div id="popupCadastroRapido">
	*			<iframe id="frameCadastroRapido" name="frameCadastroRapido" width="100%" height="100%">
	*			</iframe>
	*		</div>
	*/
	$(document).ready(function() {
		$( "#popupCadastroRapido" ).dialog({
			title: 'Cadastro Rápido',
			width: 900,
			height: 500,
			modal: true,
			autoOpen: false,
			resizable: true,
			show: "fade",
			hide: "fade"
			});

		$("#popupCadastroRapido").dialog('close');

	});



	/*
	*funcao chamada no onclick para abrir a popup passando como parâmetro
	*a página que deseja abrir e a fireEvent que será executada
	*ao fechar a popup (que poder ser uma função do action para
	*recarregar a combo). Exemplo: abrePopup('unidade', 'preencheLista');
	*/
	function abrePopup(pagina, callbackBeforeClose) {
		document.getElementById('frameCadastroRapido').src =ctx+'/pages/' + pagina + '/' + pagina + '.load?iframe=true';

		$("#popupCadastroRapido").dialog('open');

		//quando fechar a popup ele executa um evento
		$("#popupCadastroRapido").dialog({
			beforeClose: function(event, ui) {
	   			//aqui o evento disparado ao fechar
	   			document.form.fireEvent(callbackBeforeClose);
	   		}
		});
	}
