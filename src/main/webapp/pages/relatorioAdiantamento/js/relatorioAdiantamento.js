/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
addEvent(window, "load", load, false);
	
function fctValidaData(obj){
	if(obj.value != null && obj.value != ""){
		var data = obj.value;
	    var dia = data.substring(0,2);
	    var mes = data.substring(3,5);
	    var ano = data.substring(6,10);
	 
	    //Criando um objeto Date usando os valores ano, mes e dia.
	    var novaData = new Date(ano,(mes-1),dia);
	 
	    var mesmoDia = parseInt(dia,10) == parseInt(novaData.getDate());
	    var mesmoMes = parseInt(mes,10) == parseInt(novaData.getMonth())+1;
	    var mesmoAno = parseInt(ano) == parseInt(novaData.getFullYear());
	 
	    if (!((mesmoDia) && (mesmoMes) && (mesmoAno))){
	        alert(i18n_message("requisicaoViagem.dataInformadaInvalida"));   
	        obj.value = "";
	        obj.focus(); 
	        return false;
	    }  
	    return true;
	}
	return true;
}

function pesquisarRequisicoes() {
	
	if(document.getElementById("idSolicitacaoServico").value == null || document.getElementById("idSolicitacaoServico").value == ""){
		
		if(document.getElementById("nomeEmpregado").value == null || document.getElementById("nomeEmpregado").value == ""){
		
			if(document.getElementById("dataInicio").value == null || document.getElementById("dataInicio").value == ""){
				
				if(document.getElementById("dataInicioAux").value == null || document.getElementById("dataInicioAux").value == ""){
			
					if(document.getElementById("dataFim").value == null || document.getElementById("dataFim").value == ""){
						
						if(document.getElementById("dataFimAux").value == null || document.getElementById("dataFimAux").value == ""){
						
							alert(i18n_message("MSG11"));	
							return;
						}
					} 
				} 
			}
		}
	}
	
	
	var dataI = document.getElementById("dataInicio").value;
	var dataIA = document.getElementById("dataInicioAux").value;
	var dataF = document.getElementById("dataFim").value;
	var dataFA = document.getElementById("dataFimAux").value;
	
	if((dataI != null && dataI != "") && (dataIA == null || dataIA == "")){
		alert(i18n_message("relatorio.intevaloInicioInvalido"));
		return false;
	}
	
	if((dataIA != null && dataIA != "") && (dataI == null || dataI == "")){
		alert(i18n_message("relatorio.intevaloInicioInvalido"));
		return false;
	}
	
	if((dataF != null && dataF != "") && (dataFA == null || dataFA == "")){
		alert(i18n_message("relatorio.intevaloFimInvalido"));
		return false;
	}
	
	if((dataFA != null && dataFA != "") && (dataF == null || dataF == "")){
		alert(i18n_message("relatorio.intevaloFimInvalido"));
		return false;
	}
	
	if((dataI != null && dataI != "") && (dataIA != null && dataIA != "")){
		if (!DateTimeUtil.comparaDatas(document.form.dataInicio, document.form.dataInicioAux, i18n_message("citcorpore.comum.validacao.periodoInicioRemarcacao"))){
			return false;
		} 
	}
	
	if((dataF != null && dataF != "") && (dataFA != null && dataFA != "")){
		if (!DateTimeUtil.comparaDatas(document.form.dataFim, document.form.dataFimAux, i18n_message("citcorpore.comum.validacao.periodoFimRemarcacao"))){
			return false;
		} 
	}
	
	if((dataI != null && dataI != "") && (dataFA != null && dataFA != "") && ($('#e').is(":checked"))){
		if (!DateTimeUtil.comparaDatas(document.form.dataInicio, document.form.dataFimAux, i18n_message("rh.dataInicioMaior"))){
			return false;
		} 
	}
	
	document.form.fireEvent("pesquisaRequisicoesViagem");
}

function limparFormulario(){
	HTMLUtils.clearForm(document.form);
	$('#e').attr("checked",true);
}

