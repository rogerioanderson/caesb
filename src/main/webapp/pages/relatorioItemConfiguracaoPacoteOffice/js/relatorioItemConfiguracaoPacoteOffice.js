/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/

		$(function() {
			$("#POPUP_MIDIASOFTWARE").dialog({
				autoOpen : false,
				width : 600,
				height : 400,
				modal : true
			});

			$('#duplicado').change(function() {
			  if (!$(this).is(':checked')) {
				  $('#contentChaves').hide();
				  $('#softwaresL').attr('disabled', false);
				  document.form.fireEvent("listaChavesMidiaSoftware");
			  }else{
				  $('#contentChaves').show();
				  $('#softwaresL').attr('disabled', true);
			  }
			});

		});

		function LOOKUP_MIDIASOFTWARE_select(id, desc) {
			document.form.idMidiaSoftware.value = id;
			document.getElementById("nomeMidia").value = desc;
			document.form.fireEvent("listaChavesMidiaSoftware");
			 $('#duplicados').show();
			 $('#softwares').show();
			$("#POPUP_MIDIASOFTWARE").dialog("close");
		}

		function abrePopupMidia(){
			$("#POPUP_MIDIASOFTWARE").dialog("open");
		}

		function imprimirRelatorioPacoteOffice(){
			serializa();
			if(document.form.midiaSoftwareChavesSerealizadas.value == '' && confirm(i18n_message("relatorio.office.desejaListarTodos"))){
				document.form.fireEvent("imprimirRelatorioPacoteOffice");
			}else {
				document.form.fireEvent("imprimirRelatorioPacoteOffice");
			}
		}

		function imprimirRelatorioPacoteOfficeXls(){
			serializa();
			if(document.form.midiaSoftwareChavesSerealizadas.value == '' && confirm(i18n_message("relatorio.office.desejaListarTodos"))){
				document.form.fireEvent("imprimirRelatorioPacoteOfficeXls");
			}else {
				document.form.fireEvent("imprimirRelatorioPacoteOfficeXls");
			}
		}

		serializa = function() {
			try {
				var tabela = document.getElementById('tblMidiaSoftwareChave');
				var count = tabela.rows.length;
				var contadorAux = 0;
				var baselines = new Array();
				for ( var i = 0; i < count; i++) {
					var trObj = document.getElementById('idMidiaSoftwareChave' + i);
					if (!trObj)
						continue;
					else if(trObj.checked){
						baselines[contadorAux] = get(i);
						contadorAux = contadorAux + 1;
						continue;
					}
				}
				var objs = ObjectUtils.serializeObjects(baselines);
				document.form.midiaSoftwareChavesSerealizadas.value = objs;
				return true;
			}catch(e){

			}
		}

		get = function(seq) {
			var midiaSoftwareChaveDTO = new CIT_MidiaSoftwareChaveDTO();
			midiaSoftwareChaveDTO.sequencia = seq;
			midiaSoftwareChaveDTO.idMidiaSoftwareChave = eval('document.form.idMidiaSoftwareChave' + seq + '.value');
			midiaSoftwareChaveDTO.chave = eval('document.form.chave' + seq + '.value');
			return midiaSoftwareChaveDTO;
		}

		function limpar() {
			$("#contentChaves").text("");
			$("#nomeMidia").val("");
			$("#midiaSoftwareChavesSerealizadas").val("");
			$('#duplicados').hide();
			$('#softwares').hide();
		}

	
