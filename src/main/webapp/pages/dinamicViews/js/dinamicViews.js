/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
$(document).ready(function() {
	$('li.dropdown').click(function() {
		$('.dropdown').removeClass('open');
		if ($(this).is('.open')) {
			$(this).removeClass('open');
		} else {
			$(this).addClass('open');
		}
	});
	$('html').off('click.dropdown.data-api');
	$('html').on('click.dropdown.data-api', function(e) {
		if (!$(e.target).parents('li').is('.open')) {
			$('.dropdown').removeClass('open');
		} else {
			e.stopPropagation();
		}
	});

	/**
	 * Luiz.borges 04/12/2013 #126174 - Adicionada linha Corrige a sobreposição
	 * da dinamicView sobre o Menu.*
	 */
	$('.layout-panel-south').css('z-index', '1');
});

jQuery(document).ready(function($) {
	"use strict";
	$('.panel-body, .layout-body').perfectScrollbar();
});

function prepareStringJSON(json_data_geral) {
	var ret = json_data_geral.replace(/[\\]/g, '\\\\').replace(/[\"]/g, '\\\"')
			.replace(/[\/]/g, '\\/').replace(/[\b]/g, '\\b').replace(/[\f]/g,
					'\\f').replace(/[\n]/g, '\\n').replace(/[\r]/g, '\\r')
			.replace(/[\t]/g, '\\t');
	return ret;
}

function abreFechaMaisMenos(obj, idObj) {
	var n = obj.src.indexOf(ctx + '/imagens/mais.jpg');
	if (n > -1) {
		document.getElementById(idObj).style.display = 'block';
		document.getElementById('img_' + idObj).src = ctx
				+ '/imagens/menos.jpg';
	} else {
		document.getElementById(idObj).style.display = 'none';
		document.getElementById('img_' + idObj).src = ctx + '/imagens/mais.jpg';
	}
}

function cancelar() {
	try {
		parent.fecharVisao();
	} catch (e) {
	}
}

function fecharSePOPUP() {
	try {
		parent.fecharVisao();
	} catch (e) {
	}
}

function resize_iframe() {
}

if (window
		.matchMedia("screen and (-ms-high-contrast: active), (-ms-high-contrast: none)").matches) {
	document.documentElement.className += " " + "ie10";
}

$.fn.datebox.defaults.formatter = function(date) {
	var y = '' + date.getFullYear();
	var m = '' + (date.getMonth() + 1);
	var d = '' + date.getDate();
	if (m.length < 2) {
		m = '0' + m;
	}
	if (d.length < 2) {
		d = '0' + d;
	}
	return d + '/' + m + '/' + y;
}

function excluir() {
	if (confirm(i18n_message("dinamicview.confirmaexclusao"))) {
		JANELA_AGUARDE_MENU.show();
		document.form.fireEvent('delete');
	}
}

var acaoPesquisar = 'N';
function TABLE_SEARCH_CLICK(idVisao, acao, obj, action) {
	if (acaoPesquisar == 'N') {
		alert(i18n_message("dinamicview.naoehpossivelpesquisar"));
		return;
	}
	document.form.dinamicViewsIdVisaoPesquisaSelecionada.value = idVisao;
	document.form.dinamicViewsAcaoPesquisaSelecionada.value = acao;
	var json_data = JSON.stringify(obj);
	document.form.dinamicViewsJson_data.value = json_data;
	document.form.fireEvent('tableSearchClick');
}

function executeTimeout(func, timeout) {
	window.setTimeout(func, timeout);
}

function setDataTemp(key, data) {
	document.form.dinamicViewsJson_tempData.value = data;
	document.form.keyControl.value = key;
	document.form.fireEvent('setDadosTemporarios');
}

function retiraApostrofe(str) {
	var myRegExp = new RegExp("'", "g");
	var myResult = str.replace(myRegExp, "-");
	return myResult;
}

function enviaDados(urlParm, divParm, theForm) {
	var dataToSend = $("#" + theForm.name).serialize();
	$
			.ajax({
				url : urlParm,
				type : 'post',
				data : dataToSend,
				dataType : 'html',
				beforeSend : function() {
					try {
						document.getElementById(divParm).innerHTML = i18n_message("citcorpore.comum.carregando")
								+ '...';
					} catch (e) {
					}
				},
				timeout : 3000,
				success : function(retorno) {
					$('#' + divParm).html(retorno);
				},
				error : function(erro) {
					$('#' + divParm).html(erro);
				}
			});
}

/*
 * Desenvolvedor: Fabio Amorim - Data: 28/05/2015 - Horário: 14:29 - ID
 * Citsmart: 155831 - Motivo/Comentário: CPF/CNPJ no cadastro de cliente
 * (DinamicView) não é validado. campo aceita qualquer caracter, não
 * existe mascara.
 */
function mascaraCpfOuCnpj(str) {
	// Caso passe de 14 caracteres será formatado como CNPJ
	if (str.value.length > 14)
		str.value = mascaraCnpj(str.value);
	// Caso contrário como CPF
	else
		str.value = mascaraCpf(str.value);
}

// Funcao de formatacao CPF
function mascaraCpf(valor) {
	// Remove qualquer caracter digitado que não seja numero
	valor = valor.replace(/\D/g, "");

	// Coloca ponto entre o terceiro e o quarto dígitos
	valor = valor.replace(/(\d{3})(\d)/, "$1.$2");

	// Coloca ponto entre o setimo e o oitava dígitos
	valor = valor.replace(/(\d{3})(\d)/, "$1.$2");

	// Coloca ponto entre o decimoprimeiro e o decimosegundo dígitos
	valor = valor.replace(/(\d{3})(\d)/, "$1-$2");

	return valor;
}

// Funcao de formatacao CNPJ
function mascaraCnpj(valor) {
	// Remove qualquer caracter digitado que não seja numero
	valor = valor.replace(/\D/g, "");

	// Adiciona um ponto entre o segundo e o terceiro dígitos
	valor = valor.replace(/^(\d{2})(\d)/, "$1.$2");

	// Adiciona um ponto entre o quinto e o sexto dígitos
	valor = valor.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");

	// Adiciona uma barra entre o oitavaloro e o nono dígitos
	valor = valor.replace(/\.(\d{3})(\d)/, ".$1/$2");

	// Adiciona um hífen depois do bloco de quatro dígitos
	valor = valor.replace(/(\d{4})(\d)/, "$1-$2");
	return valor;
}

/*
 * Desenvolvedor: Fabio Amorim - Data: 28/05/2015 - Horário: 14:29 - ID
 * Citsmart: 155831 - Motivo/Comentário: CPF/CNPJ no cadastro de cliente
 * (DinamicView) não é validado. campo aceita qualquer caracter, não
 * existe mascara.
 */
function validaCpfOuCnpj(str) {
	mascaraCpfOuCnpj(str);
	
	if (str.value.length > 0) {
		// Caso passe de 14 caracteres será validado como CNPJ
		if (str.value.length > 14) {
			if (!valida_cnpj(str.value)) {
				alert(i18n_message("citcorpore.validacao.numeroCNPJInvalido"));
			}
		} else {
			// Caso contrário como CPF
			if (!valida_cpf(str.value)) {
				alert(i18n_message("citcorpore.validacao.numeroCPFInvalido"));
			}
		}
	}
}

function valida_cnpj(cnpj) {
	cnpj = cnpj.replace(/[^\d]+/g,'');
	 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
         
    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
}

function valida_cpf(cpf) {
    cpf = cpf.replace(/[^\d]+/g,'');    
    if(cpf == '') return false; 
    // Elimina CPFs invalidos conhecidos    
    if (cpf.length != 11 || 
        cpf == "00000000000" || 
        cpf == "11111111111" || 
        cpf == "22222222222" || 
        cpf == "33333333333" || 
        cpf == "44444444444" || 
        cpf == "55555555555" || 
        cpf == "66666666666" || 
        cpf == "77777777777" || 
        cpf == "88888888888" || 
        cpf == "99999999999")
            return false;       
    // Valida 1o digito 
    add = 0;    
    for (i=0; i < 9; i ++)       
        add += parseInt(cpf.charAt(i)) * (10 - i);  
        rev = 11 - (add % 11);  
        if (rev == 10 || rev == 11)     
            rev = 0;    
        if (rev != parseInt(cpf.charAt(9)))     
            return false;       
    // Valida 2o digito 
    add = 0;    
    for (i = 0; i < 10; i ++)        
        add += parseInt(cpf.charAt(i)) * (11 - i);  
    rev = 11 - (add % 11);  
    if (rev == 10 || rev == 11) 
        rev = 0;    
    if (rev != parseInt(cpf.charAt(10)))
        return false;       
    return true;
}

