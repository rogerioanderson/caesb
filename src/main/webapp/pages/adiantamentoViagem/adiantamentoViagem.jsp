<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.centralit.citcorpore.bean.ProdutoDTO"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.citframework.util.Constantes"%>
<%@page import="java.util.Collection"%>

<!doctype html public "">
<html>
	<head>
		<%@include file="/novoLayout/common/include/libCabecalho.jsp"%>
		<%@include file="/novoLayout/common/include/titulo.jsp"%>
		
		<link type="text/css" rel="stylesheet" href="../../novoLayout/common/include/css/template.css"/>
		<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/jqueryTreeview/jquery.treeview.css"/>
		<link type="text/css" rel="stylesheet" href="../../novoLayout/common/include/css/jqueryautocomplete.css"/>
		<link type="text/css" rel="stylesheet" href="css/AdiantamentoViagem.css" />

		<%
			response.setHeader("Cache-Control", "no-cache"); 
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", -1);
		%>
		
		<script type="text/javascript" src="${ctx}/js/ValidacaoUtils.js"></script>
		<script type="text/javascript" src="${ctx}/js/PopupManager.js"></script>
		<script type="text/javascript" src="${ctx}/cit/objects/EmpregadoDTO.js"></script>
		<script type="text/javascript" src="${ctx}/cit/objects/RequisicaoViagemDTO.js"></script>
		<script type="text/javascript" src="${ctx}/cit/objects/DespesaViagemDTO.js"></script>
	</head>
	<body>
		<div class="nowrapper">
			<!-- Inicio conteudo -->
			<div id="content">
				<form name='form' action='${ctx}/pages/adiantamentoViagem/adiantamentoViagem'>
					<input type='hidden' name='idSolicitacaoServico' id='idSolicitacaoServico' />
					<input type='hidden' name='idIntegrante' id='idIntegrante' />
					<input type='hidden' name='estado'/>
					<input type='hidden' name='idCidadeOrigem'/>
					<input type='hidden' name='idCidadeDestino'/>
					<input type='hidden' name="confirma" id="confirma" />
					
					<div class="widget">
						<div class="widget-head">
	                    	<h2 class="heading"><fmt:message key="adiantamentoViagem.confirmarAdiantamentoDiarias" /></h2>
	                    </div><!-- .widget-head -->
	                    
	                    <div class="widget-body">
	                    	<div style="margin-bottom: 15px;">
                        		<a href="javascript:;" class="btn btn-default btn-primary" onclick="visualizarResponsaveisPopup();">
                        			<i class="icon-white icon-eye-open"></i> <fmt:message key="viagem.visualizarResponsaveis"/>
                        		</a>
                        	</div>
                        	
	                    	<div id="infoViagem" class="widget row-fluid" data-toggle="collapse-widget" data-collapse-closed="true">
	                    		<div class="widget-head">
	                    			<h4 class="heading"><fmt:message key="requisicaoViagem.dadosGerais"/></h4>
	                    		</div><!-- .widget-head -->
	                    		
	                    		<div class="widget-body">
	                    			<div class="row-fluid">
	                    				<div class="span4">
		                            		<label for="idCentroCusto" class="strong campoObrigatorio"><fmt:message key="centroResultado"/></label>
		                            	 	<select id='idCentroCusto' name='idCentroCusto' class="Valid[Required] Description[centroResultado.custo] span12"></select>
		                            	</div><!-- .span4 -->
		                            	<div class="span4">
		                            		<label for="idProjeto" class="strong campoObrigatorio"><fmt:message key="requisicaoProduto.projeto"/></label>
		                            	 	<select name='idProjeto' class="Valid[Required] Description[requisicaoProduto.projeto] span12"></select>
		                            	</div><!-- .span4 -->
		                            	<div class="span4">
		                            		<label for="idMotivoViagem" class="strong campoObrigatorio"><fmt:message key="requisicaoViagem.justificativa"/></label>
		                            	 	<select name='idMotivoViagem' class="Valid[Required] Description[requisicaoViagem.justificativa] span12"></select>
		                            	</div><!-- .span4 -->
	                    			</div><!-- .row-fluid -->
	                    			
	                    			<div class="row-fluid">
		                            	<div class="span12">
		                            		<label for="descricaoMotivo" class="strong campoObrigatorio"><fmt:message key="requisicaoViagem.motivo"/></label>
		                            		<textarea class="span12" name="descricaoMotivo" id="descricaoMotivo" cols='200' rows='5' maxlength = "2000"></textarea>
		                            	</div><!-- .span11 -->
		                            </div><!-- .row-fluid -->
	                    		</div><!-- .widget-body -->
	                    	</div><!-- #infoViagem -->
	                    	
	                    	<div id="widgetIntegrantes" class="widget row-fluid" data-toggle="collapse-widget">
	                    		<div class="widget-head">
	                    			<h4 class="heading"><fmt:message key="autorizacaoViagem.custo" /></h4>
	                    		</div><!-- .widget-head -->
	                    		
	                    		<div class="widget-body">
									<div class="row-fluid">
										<div id="divBotoesAcao" class="span6">
											<a href="javascript:;" onclick="expandirItemDespesa();">+ Expandir todos</a> | <a href="javascript:;" onclick="retrairItemDespesa();">- Retrair todos</a>
										</div>
										
										<div id="divCustoTotal" class="span6">
											<label id="valorTotalViagem" style="float: right; margin-right: 18px;"></label>
										</div>
									</div>	
									<div class="row-fluid">
										<div id="despesa-viagem-items-container">
										</div><!-- .widget-body -->
									</div><!-- .row-fluid -->
								</div><!-- .widget-body -->
	                    		
	                    	</div><!-- #widgetIntegrantes -->
	                       	
							<div class="widget row-fluid" data-toggle="collapse-widget">
								<div class="widget-head">
									<h4 class="heading"><fmt:message key="requisicaoViagem.confirmacaoAdiantamento"/></h4>
								</div><!-- .widget-head -->
								
								<div class="widget-body">
									<div class="row-fluid">
										<div class="span12 strong" style="display: none;">
			                        		<fmt:message key="requisicaoViagem.cancelarRequisicao"/>
			                        		<label id="cancelarRequisicaoLabel" class="checkbox inline strong">
			                        			<input type="checkbox" class="checkbox" name="cancelarRequisicao" value="S" id="cancelarRequisicao"/><fmt:message key="citcorpore.comum.sim" />
			                        		</label>
			                        	</div><!-- .span12 -->
			                        </div><!-- .row-fluid -->
			                        
			                        <div class="row-fluid">
			                        	<div class="span12 strong">
			                        	<fmt:message key="requisicaoViagem.confimarAdiantamento"/>?
			                        		<label id="confirmarRequisicaoLabel" class="checkbox inline strong">
			                        			<input type="checkbox" class="checkbox" name="confirmaExec" value="S" id="confirmaExec" onclick="confirmaExecucao();"/><fmt:message key="citcorpore.comum.sim" />
			                        		</label>
			                        	</div><!-- .span12 -->
			                        </div><!-- .row-fluid -->
			                    </div><!-- .widget-body -->
		                    </div><!-- .widget -->
	                    </div><!-- .widget-body -->
					</div><!-- .widget -->
				
					<div id="POPUP_DADOSINTEGRANTE" name="POPUP_DADOSINTEGRANTE" title="<fmt:message key="requisicaoViagem.dadosIntegrante"/>">
						<div class='row-fluid'>
							<div class="span12">
								<div id="dadosIntegrante"></div>
							</div>
						</div>		
					</div><!-- #POPUP_DADOSINTEGRANTE -->
					
					<div id="POPUP_VISUALIZARRESPONSAVEIS" name="POPUP_VISUALIZARRESPONSAVEIS" title="<fmt:message key="viagem.visualizarResponsaveis"/>"></div><!-- #POPUP_VISUALIZARRESPONSAVEIS -->
				
				</form>
			</div><!-- #content -->
		</div><!-- .nowrapper -->
		
		<%@include file="/novoLayout/common/include/libRodape.jsp" %>
		<script src="${ctx}/template_new/js/jqueryTreeview/jquery.treeview.js"></script>
		<script src="js/AdiantamentoViagem.js"></script>
	</body>
</html>
