/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/

	    var popup;
	    addEvent(window, "load", load, false);
	    function load(){
		popup = new PopupManager(1000, 900, ctx+"/pages/");

	    }
	    $(function() {
			$("#POPUP_ACAO").dialog({
				autoOpen : false,
				width : 300,
				height : 200,
				modal : true
			});
		});
		var tabela = "";
		acao = function(valor){
			$("#POPUP_ACAO").dialog("open");
			tabela = valor;
		}
		acaoTabela = function(tipo){
			document.form.tipoAcao.value = tipo;
			document.form.tabela.value = tabela;
			document.form.fireEvent("executaMontaSQL");
		}
		executaScript = function(){
			document.form.tipoAcao.value = "";
			document.form.fireEvent("executaSQL");
		}
		$(function() {
			$(".ui-widget-overlay").click(function() {
				$("#POPUP_ACAO").dialog("close");
			});
		});

		acaoDrop = function(){
			if(confirm(i18n_message("tooldatabase.alerta.ExclusaoTabela")))
				acaoTabela('drop');
			else
				$("#POPUP_ACAO").dialog("close");
		}

		createTable = function(){
			document.form.tipoAcao.value = "createTable";
			document.form.tabela.value = "";
			document.form.fireEvent("executaMontaSQL");
		}

    
