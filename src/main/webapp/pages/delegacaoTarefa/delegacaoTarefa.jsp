<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.citframework.util.UtilHTML"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>

<html>
	<head>
	<%
	String iframe = "";
	iframe = request.getParameter("iframe");

	String idSolicitacaoServico = UtilStrings.nullToVazio((String)request.getParameter("idSolicitacaoServico"));
	String nomeTarefa = UtilHTML.encodeHTMLComEspacos((String)request.getAttribute("nomeTarefa"));


	%>
		<%@include file="/novoLayout/common/include/libCabecalho.jsp" %>
		<link type="text/css" rel="stylesheet" href="css/delegacaoTarefa.css"/>
		<link type="text/css" rel="stylesheet" href="../../novoLayout/common/include/css/template.css"/>
        <link type="text/css" rel="stylesheet" href="../../novoLayout/common/include/css/jqueryautocomplete.css"/>

    <style>
        .autocomplete {
            min-width:529px;
        }
    </style>


	</head>

    <cit:janelaAguarde id="JANELA_AGUARDE_MENU"  title="" style="display:none;top:325px;width:300px;left:500px;height:50px;position:absolute;"></cit:janelaAguarde>

	<body>
		<div class="<%=(iframe == null) ? "container-fluid fixed" : "" %>">

			<!-- Top navbar (note: add class "navbar-hidden" to close the navbar by default) -->
			<div class="navbar <%=(iframe == null) ? "main" : "nomain" %> hidden-print">

				<% if(iframe == null) { %>
					<%@include file="/novoLayout/common/include/cabecalho.jsp" %>
					<%@include file="/novoLayout/common/include/menuPadrao.jsp" %>
				<% } %>

			</div>

			<div id="wrapper" class="<%=(iframe == null) ? "" : "nowrapper" %>">

				<!-- Inicio conteudo -->
				<div id="content">
					<h3><fmt:message key="solicitacaoServico.solicitacao"/>&nbsp;N�&nbsp;<%=idSolicitacaoServico%>&nbsp;-&nbsp;<fmt:message key="tarefa.tarefa"/>&nbsp;<%=nomeTarefa%></h3>
					<div class="box-generic">
						<form class="form-horizontal" name='form' id='form' action='${ctx}/pages/delegacaoTarefa/delegacaoTarefa' accept-charset="UTF-8" enctype="multipart/form-data">
							<input type='hidden' name='idSolicitacaoServico' id='idSolicitacaoServico'/>
							<input type='hidden' name='idTarefa'/>
							<input type='hidden' name='acaoFluxo'/>
                            <input type='hidden' name='idUsuarioDestino' id='idUsuarioDestino' />
                            <input type='hidden' name='txtFiltro' id='txtFiltro' />

                            <div class="row-fluid">
								<div class="span5">
									<label  class="strong"><fmt:message key="solicitacaoServico.atribuicaoGrupo"/></label>
									  	<select placeholder="" class="span10" id="idGrupoDestino" required="required"  type="text" name="idGrupoDestino"></select>
								</div>
							</div>
							<div class="row-fluid">
								<div class="span5">
									<label  class="strong" style="padding-top: 7px"><fmt:message key="citSmart.comum.ou"/></label>
								</div>
							</div>
							<div class="control-group row-fluid">
                                <div class="row-fluid">
                                    <div class="row-fluid input-append" id='divNomeDoUsuario'>
                                        <input class="span8" type="text" name="acUsuario" required="required" id="acUsuario" placeholder="<fmt:message key="citcorpore.comum.digiteNomeUsuario" />" >
                                        <span class="add-on"><i class="icon-search"></i></span>
                                        <button type="button" class="btn btn-small btn-primary" onclick='limparUsuario()' id='btnLimparUsuario' style="margin-top: 1px; padding-top: 4px"><fmt:message key="citcorpore.ui.botao.rotulo.Limpar" /></button>&nbsp;
                                        <button type="button" class="btn btn-small btn-primary" onclick="mostrarComboUsuario()" data-toggle="modal" id='modals-bootbox-confirm' style="margin-top: 1px; padding-top: 4px"><fmt:message key="solicitacaoServico.delegacao.listaDeUsuarios"/></button>
                                    </div>
                                </div>
							</div>
							<div style="margin: 0;" class="form-actions">
								<button class="btn btn-icon btn-primary glyphicons circle_ok" type="button" onclick='gravar();'><i></i><fmt:message key="citcorpore.comum.gravar" /></button>
								<button class="btn btn-icon btn-default glyphicons cleaning" type="button" onclick='document.form.clear();'><i></i><fmt:message key="citcorpore.comum.limpar" /></button>
							</div>
						</form>
					</div>

                    <div class="modal hide fade in" id="modal_infoUsuarios" aria-hidden="false">
                        <!-- Modal heading -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                            <h3><fmt:message key="solicitacaoServico.delegacao.listaDeUsuarios" /></h3>
                        </div>
                        <!-- // Modal heading END -->
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class='slimScrollDiv'>
                                <div class='slim-scroll'>
                                    <div id="divInfoUsuarios">
                                        <div class="filter-bar">
                                            <div class='row-fluid'>
                                                <input type='text' class='span12' id='filtroTableUsuarios' placeholder="<fmt:message key="portal.carrinho.filtroBusca" />" onkeyup="filtroTableJs(this, 'tblListaUsuarios')">
                                            </div>
                                        </div>
                                        <table id='tblListaUsuarios' class='dynamicTable table  table-bordered table-condensed dataTable'>
                                            <tr>
                                                <th></th>
                                                <th ><fmt:message key="citcorpore.comum.usuario" /></th>
                                            </tr>
                                        </table>

                                        <div class="row-fluid" id="divConsultaEstourouLimite">
                                            <label id="lbConsultaLimitada" class="strong" style="padding-top: 7px" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // Modal body END -->
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <div style="margin: 0;" class="form-actions">
                                <a href="#" class="btn btn-primary" data-dismiss="modal"><fmt:message key="citcorpore.comum.fechar" /></a>
                            </div>
                            <!-- // Modal footer END -->
                        </div>
				    </div>
				<!--  Fim conteudo-->
				<%@include file="/novoLayout/common/include/rodape.jsp" %>
				<script type="text/javascript" src="js/delegacaoTarefa.js"></script>
			</div>
		</div>
	</body>
</html>
