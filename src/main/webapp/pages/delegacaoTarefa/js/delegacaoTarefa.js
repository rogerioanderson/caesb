/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**Autocomplete **/
var completeServico;

$(document).ready(function() {
    completeServico = $('#acUsuario').autocomplete({
        serviceUrl:'pages/autoCompleteUsuariosDosGruposDoUsuarioLogado/autoCompleteUsuariosDosGruposDoUsuarioLogado.load',
        noCache: true,
        onSelect: function(value, data){
            document.form.idUsuarioDestino.value = data;
            $('#acUsuario').val(value);
        }
    });

    $('#lbConsultaLimitada').html(StringUtils.format(i18n_message("citcorpore.comum.consultaLimitada"), 100));
});

fechar = function(){
	parent.fecharModalDelegacaoTarefa();
}

gravar = function() {
	if (StringUtils.isBlank(document.form.idGrupoDestino.value) && StringUtils.isBlank(document.form.idUsuarioDestino.value)){
		alert(i18n_message("gerenciaservico.delegartarefa.validacao.informeprazo"));
		document.form.IdGrupoDestino.focus();
		return;
	}
	if (window.confirm(i18n_message("gerenciaservico.delegartarefa.confirm.delegaratividade"))) 
		document.form.save();
}

function limparUsuario(){
    $('#acUsuario').val('');
    document.form.idUsuarioDestino.value = '';
    $('#acUsuario').focus();
}

function mostrarComboUsuario(){
    JANELA_AGUARDE_MENU.show();
    $('#filtroTableUsuarios').val("");
    document.form.txtFiltro.value = '';
    $('#filtroTableUsuarios').focus();
    document.form.fireEvent('listarUsuariosDosGruposDoUsuarioLogado');
}

function selecionarUsuario(row, obj){
    JANELA_AGUARDE_MENU.show()
    document.form.idUsuarioDestino.value = obj.idUsuario;
    $('#acUsuario').val(obj.nomeUsuario);

    if (!StringUtils.isBlank(obj.login) && !StringUtils.isBlank(obj.nomeUsuario)) {
        $('#acUsuario').val(obj.login + ' - ' + obj.nomeUsuario);
    } else if (!StringUtils.isBlank(obj.login)) {
        $('#acUsuario').val(obj.login);
    } else if (!StringUtils.isBlank(obj.nomeUsuario)) {
        $('#acUsuario').val(obj.nomeUsuario);
    }

    JANELA_AGUARDE_MENU.hide()
    $('#modal_infoUsuarios').modal('hide');
}

function filtroTableJs(txtFiltro) {
    var filtro = '';

    if (txtFiltro && txtFiltro.value) {
        filtro = txtFiltro.value;
    }

    document.form.acceptCharset = "UTF-8";
    document.form.txtFiltro.value = filtro;
    document.form.fireEvent('listarUsuariosDosGruposDoUsuarioLogado');
}
