<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>
<%@page import="java.util.Collection"%>
<!doctype html public "">
<html>
<head>
<%
			//identifica se a p�gina foi aberta a partir de um iframe (popup de cadastro r�pido)
			String iframe = "";
			iframe = request.getParameter("iframe");
%>
<%@include file="/include/header.jsp"%>

<%@include file="/include/security/security.jsp"%>

<title><fmt:message key="citcorpore.comum.title" /></title>

<%@include file="/include/javaScriptsComuns/javaScriptsComuns.jsp"%>
    <script type="text/javascript" src="${ctx}/js/boxover.js"></script>

<script>
$(function() {
    $("#POPUP_RESULTADO").dialog({
        autoOpen : false,
        width : 600,
        height : 400,
        modal : true
    });
});

	var objTab = null;

	addEvent(window, "load", load, false);
	function load() {
		document.form.afterLoad = function() {
			$('.tabs').tabs('select', 0);
		}
	}

    function limpar() {
    	document.form.clear();
        $('.tabs').tabs('select', 0);
    }

    function login() {
        JANELA_AGUARDE_MENU.show();
    	document.form.fireEvent("autentica");
    }

    function exibeResultado(result) {
    	document.getElementById("divResultado").innerHTML = result;
        $("#POPUP_RESULTADO").dialog("open");
    }
</script>
<%//se for chamado por iframe deixa apenas a parte de cadastro da p�gina
			if (iframe != null) {%>
<style>
    div#main_container {
        margin: 10px 10px 10px 10px;
        width: 100%;
    }
</style>
<%}%>
</head>
<cit:janelaAguarde id="JANELA_AGUARDE_MENU"  title="" style="display:none;top:325px;width:300px;left:500px;height:50px;position:absolute;">
</cit:janelaAguarde>
<body>
	<div id="wrapper">
		<%if (iframe == null) {%>
		<%@include file="/include/menu_vertical.jsp"%>
		<%}%>
		<!-- Conteudo -->
		<div id="main_container" class="main_container container_16 clearfix" style="height: auto !important;">
			<%if (iframe == null) {%>
			<%@include file="/include/menu_horizontal.jsp"%>
			<%}%>

			<div class="flat_area grid_16">
				<h2>
					Execu��o de opera��o Rest
				</h2>
			</div>
			<div class="box grid_16 tabs">
				<ul class="tab_header clearfix">
					<li><a href="#tabs-1">Teste
					</a>
				</ul>
				<a href="#" class="toggle">&nbsp;</a>
				<div class="toggle_container">
					<div id="tabs-1" class="block">
						<div class="section">
							<form name='form'
								action='${ctx}/pages/testeOperacaoRest/testeOperacaoRest'>
								<div class="columns clearfix">

                                    <div class="col_100">
                                       <div class="col_60">
											<fieldset style='height:60px'>
												<label>URL
												</label>
		                                           <div>
		                                               <input id="url" type="text" name="url" />
		                                           </div>
											</fieldset>
										</div>
	                                    <div class="col_20">
                                            <fieldset style='height:60px'>
                                                <label >Formato de sa�da
                                                </label>
                                                   <div>
                                                       <select name='formatoSaida' class="Valid[Required] Description[Formato]"></select>
                                                   </div>
                                            </fieldset>
								         </div>
								    </div>

                                    <div class="col_100">
                                         <div class="col_20">
                                            <fieldset style='height:60px'>
                                                <label >Usu�rio
                                                </label>
                                                   <div>
                                                       <input id="loginUsuario" type='text' name="loginUsuario" />
                                                   </div>
                                            </fieldset>
                                         </div>
                                         <div class="col_20">
											<fieldset style='height:60px'>
												<label>Senha
												</label>
		                                           <div>
		                                               <input id="senha" type="password" name="senha"  />
		                                           </div>
											</fieldset>
								         </div>
	                                    <div class="col_10">
	                                        <fieldset style='height:60px'>
		                                        <div style="padding:20px">
					                                <button type='button' name='btnLogin' class="light"
					                                    onclick='login();'>
					                                    <img
					                                        src="${ctx}/template_new/images/icons/small/grey/pencil.png">
					                                    <span>Autenticar
					                                    </span>
					                                </button>
		                                        </div>
	                                        </fieldset>
	                                    </div>
                                         <div class="col_30">
											<fieldset style='height:60px'>
												<label>ID da Sess�o
												</label>
		                                           <div>
		                                               <input id="idSessao" type="text" name="idSessao" readonly="readonly" />
		                                           </div>
											</fieldset>
								         </div>
	                                </div>
	                                <div class="col_100">
							            <div class="tabs" style="width: 100%;">
							                <ul class="tab_header clearfix">
                                                <li><a href="#tabs-2" class="round_top" onclick=''>AddServiceRequest
                                                </a>
                                                <li><a href="#tabs-3" class="round_top" onclick=''>Mobile
                                                </a>
							                </ul>
                                            <a href="#" class="toggle">&nbsp;</a>
							                <div class="toggle_container">
                                                <div id="tabs-2" class="block">
                                                    <div class="columns clearfix">
                                                    	<div class="col_100">
					                                         <div class="col_30">
					                                            <fieldset style='height:60px'>
					                                                <label >Solicitante
					                                                </label>
					                                                   <div>
					                                                       <select name='loginSolicitante' class="Valid[Required] Description[Solicitante]"></select>
					                                                   </div>
					                                            </fieldset>
					                                         </div>
					                                         <div class="col_50">
					                                            <fieldset style='height:60px'>
					                                                <label >Servi�o
					                                                </label>
					                                                   <div>
					                                                       <select name='idServico' class="Valid[Required] Description[Servi�o]"></select>
					                                                   </div>
					                                            </fieldset>
					                                         </div>
					                                    </div>
                                                    	<div class="col_100">
                                                    		<div class="col_30">
					                                            <fieldset style='height:60px'>
					                                                <label >N�mero
					                                                </label>
					                                                   <div>
					                                                       <input id="numero" type="text" name="numero" class="Valid[Required] Description[N�mero]" />
					                                                   </div>
					                                            </fieldset>
					                                         </div>
					                                         <div class="col_25">
					                                            <fieldset style='height:60px'>
					                                                <label >Impacto
					                                                </label>
					                                                   <div>
					                                                       <select name='impacto' class="Valid[Required] Description[Impacto]"></select>
					                                                   </div>
					                                            </fieldset>
					                                         </div>
					                                         <div class="col_25">
					                                            <fieldset style='height:60px'>
					                                                <label >Urg�ncia
					                                                </label>
					                                                   <div>
					                                                       <select name='urgencia' class="Valid[Required] Description[Urg�ncia]"></select>
					                                                   </div>
					                                            </fieldset>
					                                         </div>
						                                    <div class="col_20">
						                                        <fieldset style='height:60px'>
							                                        <div style="padding:20px">
										                                <button type='button' name='btnSave' class="light"
										                                    onclick='JANELA_AGUARDE_MENU.show();document.form.save();'>
										                                    <img
										                                        src="${ctx}/template_new/images/icons/small/grey/pencil.png">
										                                    <span>Testar
										                                    </span>
										                                </button>
							                                        </div>
						                                        </fieldset>
						                                    </div>
					                                     </div>
                                                   		<div class="col_100">
					                                         <div class="col_30">
					                                            <fieldset style='height:100px'>
					                                                <label >Tipo
					                                                </label>
					                                                   <div>
					                                                       <select name='tipo' class="Valid[Required] Description[Tipo]"></select>
					                                                   </div>
					                                            </fieldset>
					                                         </div>
					                                         <div class="col_50">
					                                            <fieldset style='height:100px'>
					                                                <label >Descri��o
					                                                </label>
					                                                   <div>
					                                                       <textarea name="descricao" id="descricao" cols='200' rows='4' class="Valid[Required] Description[Descri��o]"></textarea>
					                                                   </div>
					                                            </fieldset>
					                                         </div>
						                                    <div class="col_20">
						                                        <fieldset style='height:100px'>
							                                        <div style="padding:20px">
										                                <button type='button' name='btnSave' class="light"
										                                    onclick='JANELA_AGUARDE_MENU.show();document.form.fireEvent("saveVersaoAnterior");'>
										                                    <img
										                                        src="${ctx}/template_new/images/icons/small/grey/pencil.png">
										                                    <span>Testar vers�o antiga
										                                    </span>
										                                </button>
							                                        </div>
						                                        </fieldset>
						                                    </div>
						                                 </div>
					                               </div>
                                                </div>
                                                <div id="tabs-3" class="block">
                                                    <div class="columns clearfix">
                                                    	<div class="col_100">
					                                         <div class="col_50">
																<fieldset style='height:60px'>
																	<label>Tipos de solicita��o</label>
																	<div style="padding:20px">
																		<input value='0' type='radio' name='tipoListagem' checked='checked'/>Todas
																		<input value='1' type='radio' name='tipoListagem' />Compras
																		<input value='2' type='radio' name='tipoListagem' />Viagens
																		<input value='3' type='radio' name='tipoListagem' />RH
																		<input value='4' type='radio' name='tipoListagem' />Incidentes
																		<input value='5' type='radio' name='tipoListagem' />Outras requisi��es
																	</div>
																</fieldset>
					                                         </div>
					                                         <div class="col_30">
																<fieldset style='height:60px'>
																	<label>Somente em aprova��o</label>
																	<div style="padding:20px">
																		<input value='1' type='radio' name='somenteEmAprovacao' checked='checked' />Sim
																		<input value='0' type='radio' name='somenteEmAprovacao' />N�o
																	</div>
																</fieldset>
					                                         </div>
					                                         <div class="col_20">
						                                        <fieldset style='height:60px'>
							                                        <div style="padding:20px">
										                                <button type='button' name='btnSave1' class="light"
										                                    onclick='JANELA_AGUARDE_MENU.show();document.form.fireEvent("notification_getByUser");'>
										                                    <img
										                                        src="${ctx}/template_new/images/icons/small/grey/pencil.png">
										                                    <span>getByUser
										                                    </span>
										                                </button>
							                                        </div>
						                                        </fieldset>
						                                    </div>
						                                 </div>
                                                    	<div class="col_100">
					                                         <div class="col_20">
					                                            <fieldset style='height:60px'>
					                                                <label >Id da tarefa
					                                                </label>
					                                                   <div>
					                                                       <input id="idTarefa" type="text" name="idTarefa" />
					                                                   </div>
					                                            </fieldset>
					                                         </div>
					                                         <div class="col_20">
					                                            <fieldset style='height:60px'>
					                                            	<label>Feedback</label>
																	<div style="padding:20px">
																		<input value='0' type='radio' name='feedback' />Negativo
																		<input value='1' type='radio' name='feedback' />Positivo
																	</div>
					                                            </fieldset>
					                                         </div>
					                                         <div class="col_30">
					                                            <fieldset style='height:60px'>
					                                                <label >Justificativa
					                                                </label>
					                                                   <div>
					                                                       <select name='idJustificativa' class="Valid[Required] Description[Justificativa]"></select>
					                                                   </div>
					                                            </fieldset>
					                                         </div>
						                                    <div class="col_10">
						                                        <fieldset style='height:60px'>
							                                        <div style="padding:20px">
										                                <button type='button' name='btnSave2' class="light"
										                                    onclick='JANELA_AGUARDE_MENU.show();document.form.fireEvent("notification_getById");'>
										                                    <img
										                                        src="${ctx}/template_new/images/icons/small/grey/pencil.png">
										                                    <span>getById
										                                    </span>
										                                </button>
							                                        </div>
						                                        </fieldset>
						                                    </div>
						                                    <div class="col_10">
						                                        <fieldset style='height:60px'>
							                                        <div style="padding:20px">
										                                <button type='button' name='btnSave3' class="light"
										                                    onclick='JANELA_AGUARDE_MENU.show();document.form.fireEvent("notification_getReasons");'>
										                                    <img
										                                        src="${ctx}/template_new/images/icons/small/grey/pencil.png">
										                                    <span>getReasons
										                                    </span>
										                                </button>
							                                        </div>
						                                        </fieldset>
						                                    </div>
						                                    <div class="col_10">
						                                        <fieldset style='height:60px'>
							                                        <div style="padding:20px">
										                                <button type='button' name='btnSave4' class="light"
										                                    onclick='JANELA_AGUARDE_MENU.show();document.form.fireEvent("notification_feedback");'>
										                                    <img
										                                        src="${ctx}/template_new/images/icons/small/grey/pencil.png">
										                                    <span>feedback
										                                    </span>
										                                </button>
							                                        </div>
						                                        </fieldset>
						                                    </div>
						                                  </div>
						                              </div>
                                                    	<div class="col_100">
					                                         <div class="col_80">
					                                            <fieldset style='height:100px'>
					                                                <label >Descri��o
					                                                </label>
					                                                   <div>
					                                                       <textarea name="descricaoPortal" id="descricaoPortal" cols='200' rows='4' class="Valid[Required] Description[Descri��o]"></textarea>
					                                                   </div>
					                                            </fieldset>
					                                         </div>
						                                    <div class="col_20">
						                                        <fieldset style='height:100px'>
							                                        <div style="padding:20px">
										                                <button type='button' name='btnSave3' class="light"
										                                    onclick='JANELA_AGUARDE_MENU.show();document.form.fireEvent("notification_new");'>
										                                    <img
										                                        src="${ctx}/template_new/images/icons/small/grey/pencil.png">
										                                    <span>new
										                                    </span>
										                                </button>
							                                        </div>
						                                        </fieldset>
						                                    </div>
						                                 </div>
						                              </div>                                                 </div>
                                            </div>
                                       		</div>
                                        </div>
                                    </div>
                                    <div style="clear: both"></div>
    							</div>
							</form>
						</div>
					</div>
					<!-- ## FIM - AREA DA APLICACAO ## -->
				</div>
			</div>
		</div>
		<!-- Fim da Pagina de Conteudo -->
	</div>

      <div id="POPUP_RESULTADO" title="Resultado"  style="overflow: hidden;">
            <div id='divResultado' class="columns clearfix">
           </div>
      </div>

	<%@include file="/include/footer.jsp"%>
</body>

</html>
