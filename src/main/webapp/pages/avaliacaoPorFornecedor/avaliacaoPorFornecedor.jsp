<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="java.util.Collection"%>
<%@page import="java.util.Iterator"%>
<%@page import="br.com.centralit.citcorpore.bean.ServicoContratoDTO"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.citframework.dto.Usuario"%>
<%@page import="br.com.citframework.util.UtilDatas"%>
<%@page import="br.com.citframework.util.UtilFormatacao"%>

<!doctype html>
<html>
<head>
	<%
		Collection listaServicos = (Collection) request.getAttribute("listaServicos");

		//identifica se a p�gina foi aberta a partir de um iframe (popup de cadastro r�pido)
		String iframe = "";
		iframe = request.getParameter("iframe");

	%>
	<%@include file="/include/header.jsp"%>

	<%@include file="/include/security/security.jsp" %>
	<title><fmt:message key="citcorpore.comum.title"/></title>
	<%@include file="/include/javaScriptsComuns/javaScriptsComuns.jsp"%>

	<link rel="stylesheet" type="text/css" href="./css/avaliacaoPorFornecedor.css" />

	<script type="text/javascript" src="./js/avaliacaoPorFornecedor.js"></script>
	<%
	//se for chamado por iframe deixa apenas a parte de cadastro da p�gina
	if(iframe != null){%>
		<link rel="stylesheet" type="text/css" href="./css/avaliacaoPorFornecedorIFrame.css">
	<%}%>

</head>
<cit:janelaAguarde id="JANELA_AGUARDE_MENU"  title="" style="display:none;top:325px;width:300px;left:500px;height:50px;position:absolute;"></cit:janelaAguarde>
<body>
	<div id="wrapper">
		<%if(iframe == null){%>
			<%@include file="/include/menu_vertical.jsp"%>
		<%}%>
		<!-- Conteudo -->
		<div id="main_container" class="main_container container_16 clearfix">
		<%if(iframe == null){%>
			<%@include file="/include/menu_horizontal.jsp"%>
		<%}%>
		<form name='form' action='${ctx}/pages/avaliacaoPorFornecedor/avaliacaoPorFornecedor'>
			<div class="flat_area grid_16">
				<h2>
					<fmt:message key="avaliacao.fornecedor.servico"/>
				</h2>
			</div>
			<div class="box grid_16 tabs">
				<fieldset>
					<div class="col_30" style="width: 30% !important; float: left;">
						<label><fmt:message key="avaliacao.fornecedor.pesquisa"/></label>
						<select name="comboFornecedor" id="comboFornecedor"></select>
					</div>
					<div class="col_30" style="width: 30%; float: left; margin-top: 20px;">
						<button type='button' name='btnpesquisa' class="light" onclick='pesquisa();'>
							<img src="${ctx}/template_new/images/icons/small/grey/magnifying_glass.png">
							<span><fmt:message key="citcorpore.comum.pesquisa" /></span>
						</button>
					</div>
				</fieldset>
				<div id="tableGrafico"></div>
				<div id="tableResult"></div>
			</div>
		</form>
		</div>
		<!-- Fim da Pagina de Conteudo -->
	</div>
	<%@include file="/include/footer.jsp"%>
</body>
</html>

