<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.bean.UploadDTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collection"%>
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>

<html class="backGroundBranco">
<head>
	<%@include file="/include/cssComuns/cssComuns.jsp" %>
	<%@include file="/include/internacionalizacao/internacionalizacao.jsp"%>
	
    <link type="text/css" rel="stylesheet" href="css/uploadPlanoDeReversao.css" />

</head>
<body>
<script>


	function excluirImagemUpload(path){
		if (confirm(i18n_message("citcorpore.comum.confirme.desejaexcluiranexo"))){
			window.location = '${ctx}/pages/uploadExcluirPlanoDeReversao/uploadExcluirPlanoDeReversao.load?path=' + path;
		}
	}
	function obtemArquivoTemporario(path){
		window.location = '${ctx}/pages/visualizarUploadTemp/visualizarUploadTemp.load?path=' + path;
	}
	function excluirAnexo(id, nomeArquivo){
		if(confirm(i18n_message("citcorpore.comum.confirme.exclusaoanexo"))){
			window.location = '${ctx}/pages/upload/excluirAnexo.do?nameFile=' + nomeArquivo + '&id='+id;
		}
	}
	<%
		if(request.getAttribute("acaoRetorno") != null){
	%>
		alert(i18n_message("citcorpore.comum.validacao.anexoexcluido"));
	<%
		}
	%>
</script>
<table width="100%" class='tableLess'>
	<thead>
		<tr>
		  <th>&nbsp;</th>
          <th ><fmt:message key="citcorpore.comum.arquivo" /></th>
          <th><fmt:message key="citcorpore.comum.descricao" /></th>
          <th><fmt:message key="citcorpore.comum.situacao" /></th>
          <th><fmt:message key="citcorpore.comum.versao" /></th>
		</tr>
	</thead>
        <%boolean branco = true;%>
      	<%String cor = "#ffffff";%>
      	<%
		Collection colUploadsGED = (Collection)request.getSession(true).getAttribute("colUploadPlanoDeReversaoGED");
		if (colUploadsGED == null){
			colUploadsGED = new ArrayList();
		}
      	%>
      	<%for(Iterator it = colUploadsGED.iterator(); it.hasNext();){ %>
      		<%
      		UploadDTO uploadDTO = (UploadDTO)it.next();
      		%>
		<TR>
			<%
			if(branco){
				cor ="#ffffff";
			}else{
				cor="#E5EBF6";
			}
			%>
			<%
			if(request.getSession().getAttribute("disable")== null || request.getSession().getAttribute("disable").equals("false")){ %>
				<td>
					<img src='${ctx}/imagens/delete.png' style='cursor:pointer' onclick='excluirImagemUpload("<%=uploadDTO.getPath()%>")' title='<fmt:message key="citcorpore.ged.msg.exluiranexo" /> '/>
				</td>
			<%}else{ %>
				<td>&nbsp;</td>
			<%}%>
			<td >
				<a href="#" onclick='obtemArquivoTemporario("<%=uploadDTO.getPath()%>")'><%=uploadDTO.getNameFile()%></a>
			</td>
			<td >
				<%=uploadDTO.getDescricao()%>
			</td>
			<td  title='<fmt:message key="citcorpore.ged.situacao" />'>
				<%=uploadDTO.getSituacao()%>
			</td>
			<td  title='<fmt:message key="citcorpore.ged.versao" />'>
				<%=uploadDTO.getVersao()%>
			</td>
			<%branco=!branco;%>
        </TR>
		<%}%>
</table>
</body>

</html>
