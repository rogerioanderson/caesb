<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<!-- Thiago Fernandes - 29/10/2013 - 18:49 - Sol. 121468 - Cria��o de Upload para requisi��o mudan�a para evitar conflitos com outras telas do sistema que us�o upload.  -->
<!-- Esta p�gina realiza apenas a listagens dos uploads -->

<%@page import="br.com.centralit.citged.bean.ControleGEDDTO"%>
<%@page import="br.com.centralit.citcorpore.bean.UploadDTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collection"%>
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>

<%@ include file="/WEB-INF/templates/taglibs.jsp"%>

<html>
<head>
	<%@include file="/include/cssComuns/cssComuns.jsp" %>

	<style type="text/css">
	body {
	background-color: white;
	background-image: url("");
	}
	table tr
	{
		height: 15px;
	}
	</style>

</head>
<body>
<script>

	function obtemArquivoTemporario(path){
		window.location = '${ctx}/pages/visualizarUploadTemp/visualizarUploadTemp.load?path=' + path;
	}
</script>
<table width="100%" class='ui-widget ui-state-default ui-corner-all' id="table">
		<tr>
          <th ><fmt:message key="citcorpore.comum.arquivo" /></th>
          <th><fmt:message key="citcorpore.comum.descricao" /></th>
          <th><fmt:message key="citcorpore.comum.situacao" /></th>
		</tr>
        <%boolean branco = true;%>
      	<%String cor = "#ffffff";%>
      	<%
		Collection<UploadDTO> colUploadsGED = (Collection<UploadDTO>)request.getSession(true).getAttribute("colUploadRequisicaoProblemaGED");

		if (colUploadsGED == null){
			colUploadsGED = new ArrayList();
		}
		%>
      	<%
      	if(!colUploadsGED.isEmpty())
      	{
	      	for(UploadDTO bean : colUploadsGED){ %>
	      		<%
	      		UploadDTO uploadDTO = (UploadDTO)bean;
	      		%>
			<tr>
				<%
				if(branco){
					cor ="#ffffff";
				}else{
					cor="#E5EBF6";
				}
				%>
				<td bgcolor="<%=cor%>" style="font-size: 11px;" width="40%">
					<a href="#" onclick='obtemArquivoTemporario("<%=uploadDTO.getPath()%>")'><%=uploadDTO.getNameFile()%></a>
				</td>
				<td bgcolor="<%=cor%>" style="font-size: 11px;" width="40%">
					<%=uploadDTO.getDescricao()%>
				</td>
				<td bgcolor="<%=cor%>" style="font-size: 11px;" width="15%" title='<fmt:message key="citcorpore.ged.situacao" />'>
					<%=uploadDTO.getSituacao()%>
				</td>
				<%branco=!branco;%>
	        </tr>
			<%}
			}else
			{
			%>
			<tr><td colspan="3"><fmt:message key="citcorpore.comum.resultado" /></td></tr>
			<%
			}
			%>
</table>
</body>

</html>
