/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**Monta os parametros para a buscas do autocomplete**/
function montaParametrosAutocompleteServico(){
	idConexaoBI =  $("#idConexaoBI").val() ;
 	completeServicoBI.setOptions({params: {idConexaoBI: idConexaoBI} });
}

/**Autocomplete **/
var completeServicoBI;
var completeServicoCorporeBI

$(document).ready(function() {
	
	$('#servicoDe').on('click', function(){
		montaParametrosAutocompleteServico();
	});
	
	completeServicoBI = $('#servicoDe').autocomplete({ 
		serviceUrl:'pages/autoCompleteServicoBI/autoCompleteServicoBI.load',
		noCache: true,
		onSelect: function(value, data){
			$('#idServicoDe').val(data);
			$('#servicoDe').val(value);
		}
	});

	completeServicoCorporeBI = $('#servicoPara').autocomplete({ 
		serviceUrl:'pages/autoCompleteServicoCorporeBI/autoCompleteServicoCorporeBI.load',
		noCache: true,
		onSelect: function(value, data){
			$('#idServicoPara').val(data);
			$('#servicoPara').val(value);
		}
	});
	
});

function relacionar() {
	JANELA_AGUARDE_MENU.show();
	document.form.fireEvent("relacionar");
}

function listar() {
	document.form.fireEvent("carregarListaDePara");
}

function excluirDePara(conexao,idde,idpara){
	if (confirm(i18n_message("deParaCatalogoServicos.removerDePara"))) {
		JANELA_AGUARDE_MENU.show();
		document.form.idConexaoBI.value = conexao;
        document.form.idServicoDe.value = idde;
        document.form.idServicoPara.value = idpara;
        document.form.fireEvent('excluirDePara');
    }
}

function onkeyDownIdDe(evt){
	if (!FormatUtils.bloqueiaNaoNumerico(evt)){
		document.getElementById("servicoDe").value="";
	};	
}

function onkeyDownIdPara(evt){
	if (!FormatUtils.bloqueiaNaoNumerico(evt)){
		document.getElementById("servicoPara").value="";
	};	
}

function enterOnIdDe(evt){
	var kc = evt.keyCode || evt.which;
	if (kc == 13) {
		document.getElementById("idServicoPara").focus();
	}
}

function enterOnIdPara(evt){
	var kc = evt.keyCode || evt.which;
	if (kc == 13) {
		relacionar();
	}
}

$("#idServicoDe").on("blur", function(e){
	document.form.fireEvent("onExitIdDe"); 
});

$("#idServicoPara").on("blur", function(e){
	document.form.fireEvent("onExitIdPara"); 
});
