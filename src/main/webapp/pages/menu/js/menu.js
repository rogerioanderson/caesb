/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
    addEvent(window, "load", load, false);
    function load() {
		document.form.afterRestore = function () {
			$('.tabs').tabs('select', 0);
		}
    }
    
    $(function() {
    	$("#ordem").focusout(function (){
    		validacaoOrdem();
    	});
    	$("#btnTodos").css('display','none');
		$("#btnLimpar").css('display','none');
		
		/* Ao clicar na imagem o item � checado
		 * Autor: flavio.santana
		 * Data/Hora 31/10/2013 08:53
		 * */
		$('#docs_icons a.glyphicons').click(
				function(){
					$(this).parent().each( 
					    function() { 
					    	$(this).find('.checked').removeClass('checked');
					});
				$(this).find('input[name="imagem"]').attr('checked',true).parent().addClass('checked');
		});
		
		desativaMenuRapido();
    });
    
    
    function validacaoOrdem(){
		if($("#ordem").val() == ""){
			alert(i18n_message("citcorpore.comum.campoOrdemMenusObrigatorio")); 
			return false;
		}else{
			if (!$("#ordem").val().match(/^\d{1,10}$/) ) {
    			alert(i18n_message("citcorpore.comum.campoOrdemMenus"));
    			return false;
    		}else{
    			return true;
    		}
		}
    }
    
    function setaLingua(idioma){
    	/*document.getElementById("idioma").value = idioma;*/
    }
	function LOOKUP_MENU_select(id,desc){
		$('.tabsbar a[href="#tab1-3"]').tab('show');
		document.form.restore({idMenu:id});
	}
	function excluir() {
		if (document.getElementById("idMenu").value != "") {
			if (confirm(i18n_message("citcorpore.comum.deleta"))) {
				document.form.fireEvent("update");
			}
		}
	}
	
	function gerar(){
		document.form.fireEvent("exportarMenuXml");
	}
	
	function atualizar(){
		document.form.fireEvent("atualizarMenuXml");
	}

    function desativarDescricao(desativado) {
        $('#descricao').prop('readonly', desativado);
    }
	
	/**
	* @author rodrigo.oliveira
	*/
	function desativaMenuPai(){
		var combobox = document.getElementById("idMenuPai")
		combobox.selectedIndex = 0;		
		combobox.disabled = true;
	}
	
	/**
	* @author rodrigo.oliveira
	*/
	function ativaMenuPai(){
		document.getElementById("idMenuPai").disabled = false;
	}

	function desativaMenuRapido(){
		var checkbox = document.getElementById("menuRapido")
		$('#menuRapido').parent().removeClass('checked');
		$('#uniform-menuRapido').addClass('disabled');
		checkbox.checked = false;	
		checkbox.disabled = true;
		$("#painelImagens").show();
	}

	function ativaMenuRapido(){
		document.getElementById("menuRapido").disabled = false;
		$('#uniform-menuRapido').removeClass('disabled');
		$("#painelImagens").hide();
	}
	
	/**
	* @author rodrigo.oliveira
	*/
	function limpar(){
		document.getElementById("idMenuPai").disabled = false;
		document.form.clear();
		desativaMenuRapido();
        desativarDescricao(false)
	}
	
	function gravar(){
		if(!validacaoOrdem()){
			return;
		}
		document.form.save();
	}
