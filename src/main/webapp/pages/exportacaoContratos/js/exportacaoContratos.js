/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
function exportar() {
	if (confirm(i18n_message("citcorpore.comum.confirmaexportacao"))) {
		$("#export").attr("value", "y");
		submitForm("form");
	}
}

function submitForm (f) {
	$("form[name=" + f + "]").submit();
	//document.f.submit();
}

//Atualiza informa��es da p�gina de acordo com o contrato selecionado.
$("#idContrato").on("change", function() {
	var idContrato = $(this).attr("value");
	
	if (idContrato != "" && typeof idContrato != "undefined") {
		var v = $("input[name='exportarAcordoNivelServico']:checked").val();
		if (v == "y") {
			$("input[name='exportarCatalogoServico'][value='y']").attr("checked", true);
			$("input[name='exportarCatalogoServico']").attr("disabled", true);
		} else {
			$("input[name='exportarCatalogoServico'][value='n']").attr("checked", true);
			$("input[name='exportarCatalogoServico']").attr("disabled", false);		
		}
	} else {
		$("input[name='exportarCatalogoServico'][value='n']").attr("checked", true);
		$("input[name='exportarCatalogoServico']").attr("disabled", false);
	}
	
	document.form.fireEvent("atualizaGrupos");
});

$("input[name='exportarAcordoNivelServico']").change(function() {
	var idContrato = $("#idContrato").attr("value");
	
	if (idContrato != "" && typeof idContrato != "undefined") {
		var v = $("input[name='exportarAcordoNivelServico']:checked").val();
		if (v == "y") {
			$("input[name='exportarCatalogoServico'][value='y']").attr("checked", true);
			$("input[name='exportarCatalogoServico']").attr("disabled", true);
		} else {
			$("input[name='exportarCatalogoServico'][value='n']").attr("checked", true);
			$("input[name='exportarCatalogoServico']").attr("disabled", false);		
		}
	} else {
		return false;
	}
});

$("#selectTodosGrupos").live("change", function() {
	if ($(this).is(':checked')) {
		$("input[name='idGrupos']").attr("checked", true);
	} else {
		$("input[name='idGrupos']").attr("checked", false);
	}
});



