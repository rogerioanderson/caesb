/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
$(function() {
	ativaClienteAsterisk();
});

function exibirNotificacaoAsterisk(lista){
	if(!$("#modal_Telefonia").is(':visible')) {
		document.formCtrlAsterisk.listaChamadas.value = lista;
		document.formCtrlAsterisk.fireEvent("abrePopUpAsterisk");
	}
}

function ativaClienteAsterisk() {
	if (asterisk_ativo === "S") {
		dwr.engine.setActiveReverseAjax(true);
		dwr.engine.setErrorHandler(errorHandler);
	}
}

function errorHandler(message, ex) {
	dwr.util.setValue("error", "Cannot connect to server. Initializing retry logic.", {escapeHtml:false});
}

function abreRamalTelefone(){
	document.formCtrlAsterisk.fireEvent("abreRamalTelefone");
	$('#modal_ramalTelefone').on('shown.bs.modal', function(){
		$("#ramalTelefone").focus();
	});
}

gravarRamalTelefone = function(){
	document.formCtrlAsterisk.fireEvent("gravarRamalTelefone");
}

function abrirSolicitacao(){
	var idEmpregado = $('#idEmpregado').val();
	var id =  parseInt(idEmpregado);
	/**
	* Motivo: Adicionando a url o parametro modalAsterisk=true para possivel callback
	* Autor: flavio.santana
	* Data/Hora: 11/12/2013 10:00
	**/
	$("#conteudoframeSolicitacaoServico").html('<iframe height="700" id="frameSolicitacaoServico" class="iframeSemBorda" width="100%" src ="'+ctx+'/pages/solicitacaoServicoMultiContratos/solicitacaoServicoMultiContratos.load?iframe=true&modalAsterisk=true&idEmpregado='+ id + '"></iframe>');
	$("#modal_INCIDENTE").modal('show');
}

function fecharModalSolicitacaoAsterisk() {
	if($("#modal_INCIDENTE").is(':visible')) {
		$("#modal_INCIDENTE").modal('hide');
	}
}

$(document).ready(function () {
	$('input').keypress(function (e) {
		var code = null;
		code = (e.keyCode ? e.keyCode : e.which);
		return (code == 13) ? false : true;
	});
});

$(document).on('hide.bs.modal','#modal_INCIDENTE', function () {
	if($("#modal_Telefonia").is(':visible')) {
		$("#modal_Telefonia").modal('hide');
	}
});
