<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>

<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<%@include file="/include/security/security.jsp" %>
<%@include file="/include/header.jsp"%>

<title>CIT Corpore</title>
<%@include file="/include/titleComum/titleComum.jsp" %>

<%@include file="/include/javaScriptsComuns/javaScriptsComuns.jsp" %>

<script>
	$(document).ready(function() {
		$( "#POPUP_OBJ" ).dialog({
			title: 'Item de Workflow',
			width: 800,
			height: 400,
			modal: true,
			autoOpen: false,
			resizable: false,
			show: "fade",
			hide: "fade"
			});

		$("#POPUP_OBJ").hide();

		$( "#sortable" ).sortable({
			cancel: ".ui-state-disabled"
		});
		$( "#sortable" ).disableSelection();
	});

	function geraSortable(id){
		$( "#" + id ).sortable();
	}

	function mostraAddObj(){
		$( "#POPUP_OBJ" ).dialog( 'open' );
	}

	function adicionaItem(){
		document.formItem.fireEvent('addItem');
	}

	function selecionaItemWorkflow(obj){
		if (obj.value == '1'){
			document.getElementById('divDecisao').style.display = 'none';
		}
		if (obj.value == '2'){
			document.getElementById('divDecisao').style.display = 'block';
		}
	}
	function clickSaveWorkFlow(){
		$.ajax({
			  url: 'http://localhost:8080/designer/uuidRepository',
			  success: function( data ) {
			      alert(data);
			  }
			});
	}
</script>

<style>
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 100%; }
	#sortable li span { position: absolute; margin-left: -1.3em; }
</style>

</head>
<body>
<div id="wrapper">
	<%@include file="/include/menu_vertical.jsp"%>
<!-- Conteudo -->
	<div id="main_container" class="main_container container_16 clearfix">

		<div class="flat_area grid_16">
				<h2>Workflow</h2>
		</div>

	<form name='form' method="POST" action='${ctx}/pages/designerWorkflow/designerWorkflow'>
		<input type='button' name='btnSourceWrkFlw' value='Gravar' onclick='clickSaveWorkFlow()'/>
		<iframe src='http://localhost:8080/designer/editorprofile=jbpm&uuid=123456' width="100%" height="100%"></iframe>
	</form>
	</div>

<div id="POPUP_OBJ" style='width: 600px; height: 400px' >
	<form name='formItem' method="POST" action='${ctx}/pages/designerWorkflow/designerWorkflow'>
	<table>
		<tr>
			<td>
				Item de Workflow:
			</td>
			<td>
				<select name='type' onchange='selecionaItemWorkflow(this)'>
					<option value='1'>Passo</option>
					<option value='2'>Decis�o</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				Descri��o:
			</td>
			<td>
				<input type='text' name='nome'/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div id='divDecisao'>

				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<button name="btnSalvar" type='button' onclick='adicionaItem()'>OK</button>
			</td>
		</tr>
	</table>
	</form>
</div>

<!-- Fim da Pagina de Conteudo -->
</div>

<%@include file="/include/footer.jsp"%>


</body>
</html>
