/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * 
 */


		jQuery(function($){
			   $("#horaInicio").mask("99:99");
			   $('.datepicker').datepicker();
			});

		gravar = function() {


			hrs = (document.form.horaInicio.value.substring(0,2));
			min = (document.form.horaInicio.value.substring(3,5));
			estado = "";
			if ((hrs < 00 ) || (hrs > 23) || ( min < 00) ||( min > 59)){
				estado = "errada";
			}

			if (document.form.horaInicio.value == "") {
				estado = "errada";
			}
			if (StringUtils.isBlank(document.form.idGrupoAtvPeriodica.value)){
				alert(i18n_message("gerenciaservico.agendaratividade.valida.grupo"));
				document.form.idGrupoAtvPeriodica.focus();
				return;
			}
			if (StringUtils.isBlank(document.form.dataInicio.value)){
				alert(i18n_message("gerenciaservico.agendaratividade.valida.dataagendamento"));
				document.form.dataInicio.focus();
				return;
			}
			if (!DateTimeUtil.isValidDate(document.form.dataInicio.value)) {
				alert(i18n_message("citcorpore.validacao.dataInvalida"));
				return;
			}
			if (estado == "errada") {
				alert(i18n_message("citcorpore.validacao.horaInvalida"));
				document.form.horaInicio.focus();
				return;
			}
			if (StringUtils.isBlank(document.form.horaInicio.value)){
				alert(i18n_message("gerenciaservico.agendaratividade.valida.hora"));
				document.form.horaInicio.focus();
				return;
			}
			if (StringUtils.isBlank(document.form.duracaoEstimada.value)){
				alert(i18n_message("gerenciaservico.agendaratividade.valida.duracao"));
				document.form.duracaoEstimada.focus();
				return;
			}
			if (document.form.duracaoEstimada.value == '0' || document.form.duracaoEstimada.value == '00' || document.form.duracaoEstimada.value == '000'){
				alert(i18n_message("gerenciaservico.agendaratividade.valida.duracao"));
				document.form.duracaoEstimada.focus();
				return;
			}
			if (confirm(i18n_message("gerenciaservico.agendaratividade.confirm.agendamento")))
				document.form.save();
			}


		function somenteNumero(e){
		    var tecla=(window.event)?event.keyCode:e.which;
		    if((tecla>47 && tecla<58)) return true;
		    else{
		    	if (tecla==8 || tecla==0) return true;
			else  return false;
		    }
		}

		function limpar(){
			document.form.idGrupoAtvPeriodica.value = '';
			document.form.dataInicio.value = '';
			document.form.horaInicio.value = '';
			document.form.duracaoEstimada.value = '';
			//Limpar TextArea HTML5
			$("#orientacaoTecnica").data("wysihtml5").editor.setValue('');
		}
