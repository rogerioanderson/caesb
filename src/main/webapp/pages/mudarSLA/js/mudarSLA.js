/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * 
 */

		gravar = function() {
			if (document.form.slaACombinar.value == 'N'){
				if (StringUtils.isBlank(document.form.prazoHH.value) && StringUtils.isBlank(document.form.prazoMM.value)){
					alert(i18n_message("gerenciaservico.mudarsla.validacao.informeprazo"));
					document.form.prazoHH.focus();
					return;
				}
				if (document.form.prazoHH.value == '0' && document.form.prazoMM.value == '0'){
					alert(i18n_message("gerenciaservico.mudarsla.validacao.informeprazo"));
					document.form.prazoHH.focus();
					return;
				}
				if (document.form.prazoHH.value == '0' && StringUtils.isBlank(document.form.prazoMM.value)){
					alert(i18n_message("gerenciaservico.mudarsla.validacao.informeprazo"));
					document.form.prazoHH.focus();
					return;
				}
				if (document.form.idJustificativa.value == '') {
					alert(i18n_message("citcorpore.comum.justificativa") + ': ' + i18n_message("citcorpore.comum.campo_obrigatorio"));
					document.form.idJustificativa.focus();
					return;
				}
				if (StringUtils.isBlank(document.form.prazoHH.value) && document.form.prazoMM.value == '0'){
					alert(i18n_message("gerenciaservico.mudarsla.validacao.informeprazo"));
					document.form.prazoHH.focus();
					return;
				}
				if (StringUtils.isBlank(document.form.idCalendario.value)){
					alert(i18n_message("gerenciaservico.mudarsla.validacao.informeprazo"));
					document.form.idCalendario.focus();
					return;
				}
			}
			if (!document.form.validate()){
				return;
			}
			if (document.form.idJustificativa.value == '') {
				alert(i18n_message("citcorpore.comum.justificativa") + ': ' + i18n_message("citcorpore.comum.campo_obrigatorio"));
				document.form.idJustificativa.focus();
				return;
			}
			if (confirm(i18n_message("gerenciaservico.mudarsla.confirm.mudanca")))
				document.form.save();
		}

		function mudarTipoSLA(obj){
			if (obj.value == 'S'){
			 	document.getElementById('tempo').style.display = 'none';
				document.getElementById('calendario').style.display = 'none';
				/*$('#tempo').switchClass( "inativo", "ativo", null );
				$('#calendario').switchClass( "inativo", "ativo", null );*/
			}else{
				document.getElementById('tempo').style.display = 'block';
				document.getElementById('calendario').style.display = 'block';
				/*$('#tempo').switchClass( "ativo", "inativo", null );
				$('#calendario').switchClass( "ativo", "inativo", null );*/
			}
		}
		function verificaMudarTipoSLA(){
			mudarTipoSLA(document.form.slaACombinar);
		}

		function somenteNumero(e){
		    var tecla=event.keyCode || e.which;
		    if((tecla>47 && tecla<58)) return true;
		    else{
		    	if (tecla==8 || tecla==0) return true;
			else  return false;
		    }
		}
