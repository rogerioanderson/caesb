/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
addEvent(window, "load", load, false);
function load() {
	document.form.afterLoad = function() {
		$('#divJustificativa').hide();
		$('#autorizado').val('');
		$('#complemJustificativaAutorizacao').val('');
		parent.escondeJanelaAguarde();
	}
}

$("#POPUP_VISUALIZARRESPONSAVEIS").dialog({
	autoOpen : false,
	width : "500px",
	modal : true
});

function visualizarResponsaveisPopup() {
	document.form.fireEvent("visualizarResponsaveisPopup");
}

function getObjetoSerializado() {
	var obj = new CIT_RequisicaoViagemDTO();
	HTMLUtils.setValuesObject(document.form, obj);
	return ObjectUtils.serializeObject(obj);
}

function configuraJustificativa(aprovado) {
	HTMLUtils.setOptionSelected('idJustificativaAutorizacao',0);
	$('#complemJustificativaAutorizacao').val('');
	if (aprovado == 'N') {
		$('#divJustificativa').show();
		$('#autorizado').val('N');
		document.form.fireEvent('preencherComboJustificativaAutorizacao');
	} else {
		$('#divJustificativa').hide();
		$('#autorizado').val('S');
	}
}

function expandirItemDespesa() {
	$('.browser .filetree-inner, .browser .filetree-inner ul').show(0);
	$('.expandable').addClass('collapsable').removeClass('expandable');
	$('.lastExpandable').addClass('lastCollapsable').removeClass('lastExpandable');
}
function retrairItemDespesa() {
	$('.browser .filetree-inner, .browser .filetree-inner ul').hide(0);
	$('.collapsable').addClass('expandable').removeClass('collapsable');
	$('.lastCollapsable').addClass('lastExpandable').removeClass('lastCollapsable');
}

$("#POPUP_DADOSINTEGRANTE").dialog({
	autoOpen : false,
	width : "500px",
	modal : true
});

function abrirModalIntegrante(idIntegrante){
	document.getElementById("idIntegranteAux").value = idIntegrante;

	document.form.fireEvent('carregaIntegrantePopup');

	$("#POPUP_DADOSINTEGRANTE").dialog("open");
}
