<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="br.com.citframework.util.Constantes"%>
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>
<%@page import="br.com.centralit.citcorpore.util.WebUtil"%>
<%@page import="br.com.centralit.citcorpore.bean.UsuarioDTO"%>
<%@page import="br.com.citframework.dto.Usuario"%>

<!DOCTYPE html public "">
<html>
	<head>
		<%@include file="/novoLayout/common/include/libCabecalho.jsp"%>
		<%@include file="/novoLayout/common/include/titulo.jsp"%>
		
		<link type="text/css" rel="stylesheet" href="../../novoLayout/common/include/css/template.css"/>
		<link rel="stylesheet" type="text/css" href="${ctx}/template_new/js/jqueryTreeview/jquery.treeview.css"/>
		<link type="text/css" rel="stylesheet" href="../../novoLayout/common/include/css/jqueryautocomplete.css"/>
		<link type="text/css" rel="stylesheet" href="css/AutorizacaoViagem.css" />
		
		<%
	        response.setHeader("Cache-Control", "no-cache"); 
	        response.setHeader("Pragma", "no-cache");
	        response.setDateHeader("Expires", -1);
    	%>
	       
	    <script type="text/javascript" src="${ctx}/js/ValidacaoUtils.js"></script>
	    <script type="text/javascript" src="${ctx}/js/PopupManager.js"></script>
	    <script type="text/javascript" src="${ctx}/cit/objects/RequisicaoViagemDTO.js"></script>
	    <script type="text/javascript" src="${ctx}/cit/objects/DespesaViagemDTO.js"></script>
	    <script type="text/javascript" src="${ctx}/cit/objects/IntegranteViagemDTO.js"></script>
	</head>
	
	<cit:janelaAguarde id="JANELA_AGUARDE_MENU"  title="" style="display:none;top:325px;width:300px;left:500px;height:50px;position:absolute;"></cit:janelaAguarde>
	
	<body>
		<div class="nowrapper">
			<!-- Inicio conteudo -->
			<div id="content">
				<div id="tabs-2" class="box-generic" style="overflow: hidden;">
   				<form name='form' action='${ctx}/pages/autorizacaoViagem/autorizacaoViagem'>
					<input type='hidden' name='idSolicitacaoServico' id='idSolicitacaoServico' /> 
                    <input type='hidden' name='editar' id='editar' /> 
                    <input type='hidden' name='acao' id='acao'/> 
                    <input type='hidden' name='detalhes' id='detalhes'/>
                    <input type='hidden' name='itemIndex' id='itemIndex'/>
                    <input type='hidden' name='integranteViagemSerialize' id='integranteViagemSerialize'/>
                    <input type='hidden' name='estado' id='estado'/>
                    <input type='hidden' name='idCidadeOrigem' id='idCidadeOrigem'/>
                    <input type='hidden' name='idCidadeDestino' id='idCidadeDestino'/>
                    <input type='hidden' name='idEmpregado' id='idEmpregado'/>
                    <input type='hidden' name='autorizado' id='autorizado'/>
                    <input type='hidden' name='idContrato' id='idContrato'/>
                    <input type='hidden' name='idIntegranteAux' id='idIntegranteAux'/>
                    
                    <div class="widget">
                    	<div class="widget-head">
                        	<h2 class="heading"><fmt:message key="autorizacaoViagem.aprovarSolicitacaoViagens" /></h2>
                        </div><!-- .widget-head -->
                        
                        <div class="widget-body">
                        	<div style="margin-bottom: 15px;">
                        		<a href="javascript:;" class="btn btn-default btn-primary" onclick="visualizarResponsaveisPopup();">
                        			<i class="icon-white icon-eye-open"></i> <fmt:message key="viagem.visualizarResponsaveis"/>
                        		</a>
                        	</div>
                        	
                        	<div id="infoViagem" class="widget row-fluid" data-toggle="collapse-widget" data-collapse-closed="true">
                        		<div class="widget-head">
			                    	<h4 class="heading"><fmt:message key="requisicaoViagem.dadosGerais" /></h4>
			                    </div><!-- .widget-head -->
			                    
			                    <div class="widget-body collapse">
									<div class="widget-body">                 
			                            <div class="row-fluid">
			                         		<div class="span3">
                                            	<label for='finalidade' class="strong campoObrigatorio"><fmt:message key="requisicaoProduto.finalidade" /></label>
                                          		<select name='finalidade' id='finalidade' class="Valid[Required] Description[requisicaoProduto.finalidade] span12"></select>
                                            </div>
			                            	<div class="span3">
			                            		<label for="idCentroCusto" class="strong campoObrigatorio"><fmt:message key="centroResultado"/></label>
			                            	 	<select id='idCentroCusto' name='idCentroCusto' class="Valid[Required] Description[centroResultado.custo] span12"></select>
			                            	</div><!-- .span3 -->
			                            	<div class="span3">
			                            		<label for="idProjeto" class="strong campoObrigatorio"><fmt:message key="requisicaoProduto.projeto"/></label>
			                            	 	<select name='idProjeto' class="Valid[Required] Description[requisicaoProduto.projeto] span12"></select>
			                            	</div><!-- .span3 -->
			                            	<div class="span3">
			                            		<label for="idMotivoViagem" class="strong campoObrigatorio"><fmt:message key="requisicaoViagem.justificativa"/></label>
			                            	 	<select name='idMotivoViagem' class="Valid[Required] Description[requisicaoViagem.justificativa] span12"></select>
			                            	</div><!-- .span3 -->
			                            </div><!-- .row-fluid -->
			                           
			                            <div class="row-fluid">
			                            	<div class="span12">
			                            		<label for="descricaoMotivo" class="strong campoObrigatorio"><fmt:message key="requisicaoViagem.motivo"/></label>
			                            		<textarea name="descricaoMotivo" id="descricaoMotivo" cols='200' rows='5' maxlength = "2000" class="span12"></textarea>
			                            	</div><!-- .span12 -->
			                            </div><!-- .row-fluid -->
				                    </div><!-- widget-body -->
								</div><!-- widget-body -->
                        	</div><!-- #infoViagem-->
                        	
                        	<div id="infoCtrlViagem" class="widget" data-toggle="collapse-widget">
                        		<div class="widget-head">
									<h4 class="heading"><fmt:message key="autorizacaoViagem.custo" /></h4>
								</div><!-- .widget-head -->
								
								<div class="widget-body">
									<div class="row-fluid">
										<div id="divBotoesAcao" class="span6">
											<a href="javascript:;" onclick="expandirItemDespesa();">+ Expandir todos</a> | <a href="javascript:;" onclick="retrairItemDespesa();">- Retrair todos</a>
										</div>
										
										<div id="divCustoTotal" class="span6">
											<label id="valorTotalViagem" style="float: right; margin-right: 18px;"></label>
										</div>
									</div>	
									<div class="row-fluid">
										<div id="despesa-viagem-items-container">
										</div><!-- .widget-body -->
									</div><!-- .row-fluid -->
								</div><!-- .widget-body -->
                        	</div><!-- #infoCtrlViagem-->
                        	
                        	<div id="confirmaAprovacao" class="widget" data-toggle="collapse-widget">
                        		<div class="widget-head">
									<h2 class="heading"><fmt:message key="autorizacaoViagem" /></h2>
								</div><!-- .widget-head -->
								
								<div class="widget-body">
									<div class="row-fluid">		
										<div class="span2" >
			                           		<label class="strong campoObrigatorio"><fmt:message key="itemRequisicaoProduto.parecer"/></label>
					                        <label class="radio" style='cursor:pointer'><input type='radio' name="autorizar" id="autorizarS" value="S" onclick='configuraJustificativa("S");' ><fmt:message key="itemRequisicaoProduto.autorizado"/></label>
					                        <label class="radio" style='cursor:pointer'><input type='radio' name="autorizar" id="autorizarN" value="N" onclick='configuraJustificativa("N");' ><fmt:message key="itemRequisicaoProduto.naoAutorizado"/></label>
			                            </div>	
			                            
			                            <div id="divJustificativa" class="span10">
			                           		<div class="span4">
			                           			<label for="idJustificativaAutorizacao" class="strong campoObrigatorio"><fmt:message key="itemRequisicaoProduto.justificativa" /></label>
									        	<select id='idJustificativaAutorizacao'  name='idJustificativaAutorizacao' class="span12"></select>
			                           		</div>
			                           		<div class="span8">
			                           			<label for="complemJustificativaAutorizacao" class="strong"><fmt:message key="itemRequisicaoProduto.complementoJustificativa" /></label>
									        	<textarea id="complemJustificativaAutorizacao" name="complemJustificativaAutorizacao"  class="span12"></textarea>
			                           		</div>
			                           	</div>
				 							<!--<div class="row-fluid">
					                            <div class="span12" >
					                        		<label for="cancelarRequisicao" style='cursor:pointer; margin-top: 1%; padding-left: 10px; font-weight: bold;'><fmt:message key="requisicaoViagem.cancelarRequisicao"/>&nbsp
					                              	<input type="checkbox" class="checkbox" name="cancelarRequisicao" value="S" id="cancelarRequisicao"/><fmt:message key="citcorpore.comum.sim" /></label>
					                        	</div>    
			                       			</div>-->
		                       		</div><!-- .row-fluid -->	
		                   		</div><!-- .widget-body -->
                        	</div><!-- #confirmaAprovacao-->
                        </div><!-- .widget-->
                    </div><!-- .widget-->
                    
                    <div id="POPUP_DADOSINTEGRANTE" name="POPUP_DADOSINTEGRANTE" title="<fmt:message key="requisicaoViagem.dadosIntegrante"/>">
						<div class='row-fluid'>
							<div class="span12">
								<div id="dadosIntegrante"></div>
							</div>
						</div>		
					</div><!-- #POPUP_ITEMCONTROLEFINANCEIRO -->
					
					<div id="POPUP_VISUALIZARRESPONSAVEIS" name="POPUP_VISUALIZARRESPONSAVEIS" title="<fmt:message key="viagem.visualizarResponsaveis"/>"></div><!-- #POPUP_VISUALIZARRESPONSAVEIS -->
                </form>
    		</div>
	    </div>
	    <%@include file="/novoLayout/common/include/libRodape.jsp" %>
		<script src="${ctx}/template_new/js/jqueryTreeview/jquery.treeview.js"></script>
		<script src="js/AutorizacaoViagem.js"></script>
	</body>
</html>
