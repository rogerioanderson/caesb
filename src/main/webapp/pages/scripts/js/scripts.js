/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
var descricaoOuQueryAlterada = false;

addEvent(window, "load", load, false);

function load() {
	document.form.afterRestore = function() {
		$('.tabs').tabs('select', 0);
	}

	$("#POPUP_RESULTADO_CONSULTA").dialog({
		closeOnEscape : false,
		title : i18n_message('scripts.resultadoConsulta'),
		autoOpen : false,
		width : 900,
		height : 600,
		modal : true
	});

	$("#POPUP_MENSAGEM_FALTA_PERMISSAO").dialog({
		title : i18n_message('mensagem'),
		autoOpen : false,
		width : 600,
		height : 400,
		modal : true
	});

}

function LOOKUP_SCRIPTS_select(id, desc) {
	document.form.restore({
		idScript : id
	});
}

function excluir() {
	if (document.getElementById("idScript").value != "") {
		if (confirm(i18n_message("citcorpore.comum.deleta"))) {
			document.form.fireEvent("delete");
		}
	}
}

function validaAtualizacao() {
	if (confirm(i18n_message("scripts.validaAtualizacao"))) {
		document.form.fireEvent("validaAtualizacao");
	}
}

function encaminhaParaIndex() {
	document.form.submit();
	window.location = ctx
			+ "/pages/index/index.load?mensagem=citcorpore.comum.validacaoSucesso";
}

function orientacaoTecnicaPopUp() {
	$('#POPUP_ORIENTACAO_TECNICA').dialog('open');
}

function executar() {
	if (document.getElementById("tipo").value != "consulta"
			&& (document.getElementById("idScript").value == "" || descricaoOuQueryAlterada)) {
		alert(i18n_message("scripts.favorGravarScript"));
	} else if (document.getElementById("tipo").value == "consulta"
			|| document.getElementById("historico").value == ""
			|| confirm(i18n_message("scripts.scriptJaFoiExecutado"))) {
		JANELA_AGUARDE_MENU.show();
		document.form.fireEvent("executar");
	}
}

function downloadDocumento(sel) {
	var value = sel.options[sel.selectedIndex].value;
	window.open(value, '_blank')
}
