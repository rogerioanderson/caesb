/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/

	var objTab = null;

	addEvent(window, "load", load, false);
	function load() {
	}

	function submeter(){
		document.getElementById('divInfo').innerHTML = i18n_message("citcorpore.comum.aguarde");
		//if (document.form.idGrupoItemConfiguracao.value == ''){
		//	document.getElementById('divInfo').innerHTML = '';
		//	alert('Selecione o Grupo!');
		//	return;
		//}
		//document.form.fireEvent('mostraInfo');
		document.form.action = ctx + '/pages/treeViewItemCfgAval/treeViewItemCfgAval.load';
		document.form.submit();
	}
	function visualizaSofts(id){
		document.form.idItemConfiguracao.value = id;
		$('#POPUP_SOFTS').dialog('open');
		document.getElementById('divInfoSoftware').innerHTML = i18n_message("citcorpore.comum.aguarde");
		document.form.fireEvent('viewSoftwares');
	}
	function visualizaNet(id){
		document.form.idItemConfiguracao.value = id;
		$('#POPUP_SOFTS').dialog('open');
		document.getElementById('divInfoSoftware').innerHTML = i18n_message("citcorpore.comum.aguarde");
		document.form.fireEvent('viewNetwork');
	}
	$(function() {
		$("#POPUP_SOFTS").dialog({
			autoOpen : false,
			width : 1000,
			height : 700,
			modal : true,
			show: "fade",
			hide: "fade"
		});
	});

	function imprimir(){
		$('#divInfo').printElement({
			printMode : 'popup',
			leaveOpen : true,
			 pageTitle : 'CITSMART Report',
			iframeElementOptions:{classNameToAdd : 'ui-corner-all resBusca'}
			});
	}

	function setaProcessar(v) {
		$('input[name="processar"]').attr('value', v);
	}

