/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: KK (Kazakh; қазақ тілі)
 */
jQuery.extend(jQuery.validator.messages, {
        required: "Бұл өрі�?ті міндетті түрде толтырыңыз.",
        remote: "Дұры�? мағына енгізуіңізді �?ұраймыз.",
        email: "�?ақты �?лектронды поштаңызды енгізуіңізді �?ұраймыз.",
        url: "�?ақты URL-ды енгізуіңізді �?ұраймыз.",
        date: "�?ақты URL-ды енгізуіңізді �?ұраймыз.",
        dateISO: "�?ақты ISO форматымен �?әйке�? дата�?ын енгізуіңізді �?ұраймыз.",
        number: "Күнді енгізуіңізді �?ұраймыз.",
        digits: "Тек қана �?андарды енгізуіңізді �?ұраймыз.",
        creditcard: "�?е�?ие карта�?ының нөмірін дұры�? енгізуіңізді �?ұраймыз.",
        equalTo: "О�?ы мәнді қайта енгізуіңізді �?ұраймыз.",
        accept: "Файлдың кеңейтуін дұры�? таңдаңыз.",
        maxlength: jQuery.format("Ұзындығы {0} �?имволдан  көр болма�?ын."),
        minlength: jQuery.format("Ұзындығы {0} �?имволдан аз болма�?ын."),
        rangelength: jQuery.format("Ұзындығы {0}-{1} дейін мән енгізуіңізді �?ұраймыз."),
        range: jQuery.format("Пожалуй�?та, введите чи�?ло от {0} до {1}. - {0} - {1} �?анын енгізуіңізді �?ұраймыз."),
        max: jQuery.format("{0} аз неме�?е тең �?анын енгізуіңіді �?ұраймыз."),
        min: jQuery.format("{0} көп неме�?е тең �?анын енгізуіңізді �?ұраймыз.")
});
