/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
(function($){
	var currentPage, currentHash;

	function resizeUI() {
		$('#doc3').css('height', (window.innerHeight || document.documentElement.clientHeight) - $('#hd').height() - 12);
	}

	function scrollToHash(hash) {
		if (hash) {
			$(hash).each(function() {
				$(this)[0].scrollIntoView();
			});
		}
	}

	function loadURL(url) {
		var parts, hash;

		// Trim away everything but the file name
		url = /([^\/]+)$/.exec(url)[0];

		// Parse out parts
		parts = /^([^#]+)(#.+)?$/.exec(url);
		hash = parts[2];

		// In page link, no need to load anything
		if (parts[1] == currentPage) {
			if (hash)
				scrollToHash(hash);
			else
				 $('#detailsView')[0].scrollTop = 0;

			return;
		}

		currentPage = parts[1];

		$("#classView a.selected").removeClass('selected');
		$("#classView a[href='" + currentPage.replace(/^.*\/([^\/]+)$/, '$1') + "']").addClass('selected').focus().parents("li.expandable").each(function() {
			var li = $(this).removeClass("expandable").addClass("collapsable");

			li.find("> div.expandable-hitarea").removeClass("expandable-hitarea").addClass("collapsable-hitarea");
			li.find("> ul").show();
		});

		$('#detailsView').find("div.page").hide();

		// Check if the page has been loaded before
		if ($("#detailsView div[url='" + currentPage + "']").show().length == 0) {
			$('#detailsView').addClass("loading");

			// Load page and cache it in a div
			$.get(currentPage, "", function(data) {
				data = /<body[^>]*>([\s\S]+)<\/body>/.exec(data);

				if (data) {
					$('#detailsView').removeClass("loading").append('<div url="' + currentPage + '" class="page">' + data[1] + '</div>')[0].scrollTop = 0;

					SyntaxHighlighter.config.clipboardSwf = 'js/clipboard.swf';
					SyntaxHighlighter.highlight({gutter : false});

					scrollToHash(hash);
				}
			});
		} else
			scrollToHash(hash);
	}

	$().ready(function(){
		$("#browser").treeview();
		$(window).resize(resizeUI).trigger('resize');

		window.setInterval(function() {
			var hash = document.location.hash;

			if (hash != currentHash && hash) {
				loadURL(hash.replace(/\-/g, '#').substring(1));
				currentHash = hash;
			}
		}, 100);

		$("a").live("click", function(e) {
			var url = e.target.href;

			if (e.button == 0) {
				if (url.indexOf('class_') != -1 || url.indexOf('alias_') != -1 || url.indexOf('member_') != -1) {
					document.location.hash = e.target.href.replace(/^.*\/([^\/]+)/, '$1').replace(/#/g, '-');

					loadURL(url);
				}

				e.preventDefault();
			}
		});
	});
})(jQuery);
