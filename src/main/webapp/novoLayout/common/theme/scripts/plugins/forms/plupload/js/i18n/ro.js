/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// Romanian
plupload.addI18n({
    'Select files' : 'Selectare fişiere',
    'Add files to the upload queue and click the start button.' : 'Adaugă fişiere în lista apoi apasă butonul \'Începe încărcare\'.',
    'Filename' : 'Nume fişier',
    'Status' : 'Stare',
    'Size' : 'Mărime',
    'Add files' : 'Adăugare fişiere',
    'Stop current upload' : 'Întrerupe încărcarea curentă',
    'Start uploading queue' : 'Începe incărcarea',
    'Uploaded %d/%d files': 'Fişiere încărcate %d/%d',
    'N/A' : 'N/A',
    'Drag files here.' : 'Trage aici fişierele',
    'File extension error.': 'Extensie fişier eronată',
    'File size error.': 'Eroare dimensiune fişier',
    'Init error.': 'Eroare iniţializare',
    'HTTP Error.': 'Eroare HTTP',
    'Security error.': 'Eroare securitate',
    'Generic error.': 'Eroare generică',
    'IO error.': 'Eroare Intrare/Ieşire',
    'Stop Upload': 'Oprire încărcare',
    'Start upload': 'Începe încărcare',
    '%d files queued': '%d fişiere listate'
});
