/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// Croatian
plupload.addI18n({
    'Select files': 'Izaberite datoteke:',
    'Add files to the upload queue and click the start button.': 'Dodajte datoteke u listu i kliknite Upload.',
    'Filename': 'Ime datoteke',
    'Status': 'Status',
    'Size': 'Veli�?ina',
    'Add files': 'Dodajte datoteke',
    'Stop current upload': 'Zaustavi trenutan upload',
    'Start uploading queue': 'Pokreni Upload',
    'Uploaded %d/%d files': 'Uploadano %d/%d datoteka',
    'N/A': 'N/A',
    'Drag files here.': 'Dovucite datoteke ovdje',
    'File extension error.': 'Greška ekstenzije datoteke.',
    'File size error.': 'Greška veli�?ine datoteke.',
    'Init error.': 'Greška inicijalizacije.',
    'HTTP Error.': 'HTTP greška.',
    'Security error.': 'Sigurnosna greška.',
    'Generic error.': 'Generi�?ka greška.',
    'IO error.': 'I/O greška.',
    'Stop Upload': 'Zaustavi upload.',
    'Add Files': 'Dodaj datoteke',
    'Start Upload': 'Pokreni upload.',
    '%d files queued': '%d datoteka na �?ekanju.'
});
