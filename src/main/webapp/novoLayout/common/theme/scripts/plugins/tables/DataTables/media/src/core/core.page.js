/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Note that most of the paging logic is done in 
 * DataTable.ext.oPagination
 */

/**
 * Generate the node required for default pagination
 *  @param {object} oSettings dataTables settings object
 *  @returns {node} Pagination feature node
 *  @memberof DataTable#oApi
 */
function _fnFeatureHtmlPaginate ( oSettings )
{
	if ( oSettings.oScroll.bInfinite )
	{
		return null;
	}
	
	var nPaginate = document.createElement( 'div' );
	nPaginate.className = oSettings.oClasses.sPaging+oSettings.sPaginationType;
	
	DataTable.ext.oPagination[ oSettings.sPaginationType ].fnInit( oSettings, nPaginate, 
		function( oSettings ) {
			_fnCalculateEnd( oSettings );
			_fnDraw( oSettings );
		}
	);
	
	/* Add a draw callback for the pagination on first instance, to update the paging display */
	if ( !oSettings.aanFeatures.p )
	{
		oSettings.aoDrawCallback.push( {
			"fn": function( oSettings ) {
				DataTable.ext.oPagination[ oSettings.sPaginationType ].fnUpdate( oSettings, function( oSettings ) {
					_fnCalculateEnd( oSettings );
					_fnDraw( oSettings );
				} );
			},
			"sName": "pagination"
		} );
	}
	return nPaginate;
}


/**
 * Alter the display settings to change the page
 *  @param {object} oSettings dataTables settings object
 *  @param {string|int} mAction Paging action to take: "first", "previous", "next" or "last"
 *    or page number to jump to (integer)
 *  @returns {bool} true page has changed, false - no change (no effect) eg 'first' on page 1
 *  @memberof DataTable#oApi
 */
function _fnPageChange ( oSettings, mAction )
{
	var iOldStart = oSettings._iDisplayStart;
	
	if ( typeof mAction === "number" )
	{
		oSettings._iDisplayStart = mAction * oSettings._iDisplayLength;
		if ( oSettings._iDisplayStart > oSettings.fnRecordsDisplay() )
		{
			oSettings._iDisplayStart = 0;
		}
	}
	else if ( mAction == "first" )
	{
		oSettings._iDisplayStart = 0;
	}
	else if ( mAction == "previous" )
	{
		oSettings._iDisplayStart = oSettings._iDisplayLength>=0 ?
			oSettings._iDisplayStart - oSettings._iDisplayLength :
			0;
		
		/* Correct for under-run */
		if ( oSettings._iDisplayStart < 0 )
		{
		  oSettings._iDisplayStart = 0;
		}
	}
	else if ( mAction == "next" )
	{
		if ( oSettings._iDisplayLength >= 0 )
		{
			/* Make sure we are not over running the display array */
			if ( oSettings._iDisplayStart + oSettings._iDisplayLength < oSettings.fnRecordsDisplay() )
			{
				oSettings._iDisplayStart += oSettings._iDisplayLength;
			}
		}
		else
		{
			oSettings._iDisplayStart = 0;
		}
	}
	else if ( mAction == "last" )
	{
		if ( oSettings._iDisplayLength >= 0 )
		{
			var iPages = parseInt( (oSettings.fnRecordsDisplay()-1) / oSettings._iDisplayLength, 10 ) + 1;
			oSettings._iDisplayStart = (iPages-1) * oSettings._iDisplayLength;
		}
		else
		{
			oSettings._iDisplayStart = 0;
		}
	}
	else
	{
		_fnLog( oSettings, 0, "Unknown paging action: "+mAction );
	}
	$(oSettings.oInstance).trigger('page', oSettings);
	
	return iOldStart != oSettings._iDisplayStart;
}

