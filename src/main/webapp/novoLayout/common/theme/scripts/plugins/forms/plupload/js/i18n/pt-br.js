/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// Brazilian Portuguese
plupload.addI18n({
   'Select files' : 'Escolha os arquivos',
   'Add files to the upload queue and click the start button.' : 'Adicione os arquivos abaixo e clique no botão "Iniciar o envio".',
   'Filename' : 'Nome do arquivo',
   'Status' : 'Status',
   'Size' : 'Tamanho',
   'Add Files' : 'Adicionar arquivo(s)',
   'Stop Upload' : 'Parar o envio',
   'Start Upload' : 'Iniciar o envio',
   'Add files' : 'Adicionar arquivo(s)',
   'Add files.' : 'Adicionar arquivo(s)',
   'Stop upload' : 'Parar o envio',
   'Start upload' : 'Iniciar o envio',
   'Uploaded %d/%d files': 'Enviado(s) %d/%d arquivo(s)',
   'N/A' : 'N/D',
   'Drag files here.' : 'Arraste os arquivos pra cá',
   'File extension error.': 'Tipo de arquivo não permitido.',
   'File size error.': 'Tamanho de arquivo não permitido.',
   'File count error.': 'Erro na contagem dos arquivos',
   'Init error.': 'Erro inicializando.',
   'HTTP Error.': 'Erro HTTP.',
   'Security error.': 'Erro de segurança.',
   'Generic error.': 'Erro genérico.',
   'IO error.': 'Erro de E/S.',
   'File: %s': 'Arquivo: %s',
   'Close': 'Fechar',
   '%d files queued': '%d arquivo(s)',
   'Using runtime: ': 'Usando: ',
   'File: %f, size: %s, max file size: %m': 'Arquivo: %f, tamanho: %s, máximo: %m',
   'Upload element accepts only %d file(s) at a time. Extra files were stripped.': 'Só são aceitos %d arquivos por vez. O que passou disso foi descartado.',
   'Upload URL might be wrong or doesn\'t exist': 'URL de envio está errada ou não existe',
   'Error: File too large: ': 'Erro: Arquivo muito grande: ',
   'Error: Invalid file extension: ': 'Erro: Tipo de arquivo não permitido: '
});
