/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: HR (Croatia; hrvatski jezik)
 */
jQuery.extend(jQuery.validator.messages, {
		required: "Ovo polje je obavezno.",
		remote: "Ovo polje treba popraviti.",
		email: "Unesite ispravnu e-mail adresu.",
		url: "Unesite ispravan URL.",
		date: "Unesite ispravan datum.",
		dateISO: "Unesite ispravan datum (ISO).",
		number: "Unesite ispravan broj.",
		digits: "Unesite samo brojeve.",
		creditcard: "Unesite ispravan broj kreditne kartice.",
		equalTo: "Unesite ponovo istu vrijednost.",
		accept: "Unesite vrijednost sa ispravnom ekstenzijom.",
		maxlength: $.validator.format("Maksimalni broj znakova je {0} ."),
		minlength: $.validator.format("Minimalni broj znakova je {0} ."),
		rangelength: $.validator.format("Unesite vrijednost između {0} i {1} znakova."),
		range: $.validator.format("Unesite vrijednost između {0} i {1}."),
		max: $.validator.format("Unesite vrijednost manju ili jednaku {0}."),
		min: $.validator.format("Unesite vrijednost veću ili jednaku {0}.")
});
