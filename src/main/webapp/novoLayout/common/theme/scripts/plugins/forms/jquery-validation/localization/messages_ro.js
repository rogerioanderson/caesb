/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: RO (Romanian, limba română)
 */
jQuery.extend(jQuery.validator.messages, {
  required: "Acest câmp este obligatoriu.",
  remote: "Te rugăm să completezi acest câmp.",
  email: "Te rugăm să introduci o adresă de email validă",
  url: "Te rugăm sa introduci o adresă URL validă.",
  date: "Te rugăm să introduci o dată corectă.",
  dateISO: "Te rugăm să introduci o dată (ISO) corectă.",
  number: "Te rugăm să introduci un număr întreg valid.",
  digits: "Te rugăm să introduci doar cifre.",
  creditcard: "Te rugăm să introduci un numar de carte de credit valid.",
  equalTo: "Te rugăm să reintroduci valoarea.",
  accept: "Te rugăm să introduci o valoare cu o extensie validă.",
  maxlength: jQuery.validator.format("Te rugăm să nu introduci mai mult de {0} caractere."),
  minlength: jQuery.validator.format("Te rugăm să introduci cel puțin {0} caractere."),
  rangelength: jQuery.validator.format("Te rugăm să introduci o valoare între {0} și {1} caractere."),
  range: jQuery.validator.format("Te rugăm să introduci o valoare între {0} și {1}."),
  max: jQuery.validator.format("Te rugăm să introduci o valoare egal sau mai mică decât {0}."),
  min: jQuery.validator.format("Te rugăm să introduci o valoare egal sau mai mare decât {0}.")
});
