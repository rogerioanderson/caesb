/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: SI (Slovenian)
 */
jQuery.extend(jQuery.validator.messages, {
	required: "To polje je obvezno.",
	remote: "Vpis v tem polju ni v pravi obliki.",
	email: "Prosimo, vnesite pravi email naslov.",
	url: "Prosimo, vnesite pravi URL.",
	date: "Prosimo, vnesite pravi datum.",
	dateISO: "Prosimo, vnesite pravi datum (ISO).",
	number: "Prosimo, vnesite pravo številko.",
	digits: "Prosimo, vnesite samo številke.",
	creditcard: "Prosimo, vnesite pravo številko kreditne kartice.",
	equalTo: "Prosimo, ponovno vnesite enako vsebino.",
	accept: "Prosimo, vnesite vsebino z pravo kon�?nico.",
	maxlength: $.validator.format("Prosimo, da ne vnašate ve�? kot {0} znakov."),
	minlength: $.validator.format("Prosimo, vnesite vsaj {0} znakov."),
	rangelength: $.validator.format("Prosimo, vnesite od {0} do {1} znakov."),
	range: $.validator.format("Prosimo, vnesite vrednost med {0} in {1}."),
	max: $.validator.format("Prosimo, vnesite vrednost manjšo ali enako {0}."),
	min: $.validator.format("Prosimo, vnesite vrednost ve�?jo ali enako {0}.")
});
