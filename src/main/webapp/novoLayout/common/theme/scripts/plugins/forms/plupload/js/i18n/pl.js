/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
plupload.addI18n({
'Select files' : 'Wybierz pliki:',
'Add files to the upload queue and click the start button.' : 'Dodaj pliki i kliknij \'Rozpocznij transfer\'.',
'Filename' : 'Nazwa pliku',
'Status' : 'Status',
'Size' : 'Rozmiar',
'Add files' : 'Dodaj pliki',
'Stop current upload' : 'Przerwij aktualny transfer',
'Start uploading queue' : 'Rozpocznij wysyłanie',
'Uploaded %d/%d files': 'Wysłano %d/%d plików',
'N/A' : 'Nie dostępne',
'Drag files here.' : 'Przeciągnij tu pliki',
'File extension error.': 'Nieobsługiwany format pliku.',
'File size error.': 'Plik jest zbyt duży.',
'Init error.': 'Błąd inicjalizacji.',
'HTTP Error.': 'Błąd HTTP.',
'Security error.': 'Błąd bezpieczeństwa.',
'Generic error.': 'Błąd ogólny.',
'IO error.': 'Błąd IO.',
'Stop Upload': 'Przerwij transfer.',
'Add Files': 'Dodaj pliki',
'Start upload': 'Rozpocznij transfer.',
'%d files queued': '%d plików w kolejce.'
});
