/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: LT (Lithuanian; lietuvių kalba)
 */
jQuery.extend(jQuery.validator.messages, {
       required: "Šis laukas yra privalomas.",
       remote: "Prašau pataisyti šį lauką.",
       email: "Prašau įvesti teisingą elektroninio pašto adresą.",
       url: "Prašau įvesti teisingą URL.",
       date: "Prašau įvesti teisingą datą.",
       dateISO: "Prašau įvesti teisingą datą (ISO).",
       number: "Prašau įvesti teisingą skai�?ių.",
       digits: "Prašau naudoti tik skaitmenis.",
       creditcard: "Prašau įvesti teisingą kreditinės kortelės numerį.",
       equalTo: "Prašau įvestį tą pa�?ią reikšmę dar kartą.",
       accept: "Prašau įvesti reikšmę su teisingu plėtiniu.",
       maxlength: $.format("Prašau įvesti ne daugiau kaip {0} simbolių."),
       minlength: $.format("Prašau įvesti bent {0} simbolius."),
       rangelength: $.format("Prašau įvesti reikšmes, kurių ilgis nuo {0} iki {1} simbolių."),
       range: $.format("Prašau įvesti reikšmę intervale nuo {0} iki {1}."),
       max: $.format("Prašau įvesti reikšmę mažesnę arba lygią {0}."),
       min: $.format("Prašau įvesti reikšmę didesnę arba lygią {0}.")
});
