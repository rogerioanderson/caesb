/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// German
plupload.addI18n({
    'Select files' : 'Dateien hochladen',
    'Add files to the upload queue and click the start button.' : 'Dateien hinzuf&uuml;gen und auf \'Hochladen\' klicken.',
    'Filename' : 'Dateiname',
    'Status' : 'Status',
    'Size' : 'Gr&ouml;&szlig;e',
    'Add files' : 'Dateien', // hinzuf&uuml;gen',
    'Stop current upload' : 'Aktuelles Hochladen stoppen',
    'Start uploading queue' : 'Hochladen starten',
    'Uploaded %d/%d files': '%d/%d Dateien sind hochgeladen',
    'N/A' : 'Nicht verf&uuml;gbar',
    'Drag files here.' : 'Ziehen Sie die Dateien hier hin',
    'File extension error.': 'Fehler bei Dateiendung',
    'File size error.': 'Fehler bei Dateigr&ouml;ße',
    'Init error.': 'Initialisierungsfehler',
    'HTTP Error.': 'HTTP-Fehler',
    'Security error.': 'Sicherheitsfehler',
    'Generic error.': 'Typischer Fehler',
    'IO error.': 'Ein/Ausgabe-Fehler',
    'Stop Upload': 'Hochladen stoppen',
    'Start upload': 'Hochladen',
    '%d files queued': '%d Dateien in der Warteschlange'
});
