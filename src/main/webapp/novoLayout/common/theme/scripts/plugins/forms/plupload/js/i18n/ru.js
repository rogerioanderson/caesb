/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// Russian
plupload.addI18n({
    'Select files' : 'Выберите файлы',
    'Add files to the upload queue and click the start button.' : 'Добавьте файлы в очередь и нажмите кнопку "Загрузить файлы".',
    'Filename' : 'Им�? файла',
    'Status' : 'Стату�?',
    'Size' : 'Размер',
    'Add files' : 'Добавить файлы',
    'Stop current upload' : 'О�?тановить загрузку',
    'Start uploading queue' : 'Загрузить файлы',
    'Uploaded %d/%d files': 'Загружено %d из %d файлов',
    'N/A' : 'N/D',
    'Drag files here.' : 'Перетащите файлы �?юда.',
    'File extension error.': '�?еправильное ра�?ширение файла.',
    'File size error.': '�?еправильный размер файла.',
    'Init error.': 'Ошибка инициализации.',
    'HTTP Error.': 'Ошибка HTTP.',
    'Security error.': 'Ошибка безопа�?но�?ти.',
    'Generic error.': 'Обща�? ошибка.',
    'IO error.': 'Ошибка ввода-вывода.'
});
