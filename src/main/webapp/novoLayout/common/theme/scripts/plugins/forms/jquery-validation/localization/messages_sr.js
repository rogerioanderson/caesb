/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
﻿/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: SR (Serbian; �?рп�?ки језик)
 */
jQuery.extend(jQuery.validator.messages, {
    required: "Поље је обавезно.",
    remote: "Средите ово поље.",
    email: "Уне�?ите и�?правну и-мејл адре�?у",
    url: "Уне�?ите и�?праван URL.",
    date: "Уне�?ите и�?праван датум.",
    dateISO: "Уне�?ите и�?праван датум (ISO).",
    number: "Уне�?ите и�?праван број.",
    digits: "Уне�?ите �?амо цифе.",
    creditcard: "Уне�?ите и�?праван број кредитне картице.",
    equalTo: "Уне�?ите и�?ту вредно�?т поново.",
    accept: "Уне�?ите вредно�?т �?а одговарајућом ек�?тензијом.",
    maxlength: $.validator.format("Уне�?ите мање од {0}карактера."),
    minlength: $.validator.format("Уне�?ите барем {0} карактера."),
    rangelength: $.validator.format("Уне�?ите вредно�?т дугачку између {0} и {1} карактера."),
    range: $.validator.format("Уне�?ите вредно�?т између {0} и {1}."),
    max: $.validator.format("Уне�?ите вредно�?т мању или једнаку {0}."),
    min: $.validator.format("Уне�?ите вредно�?т већу или једнаку {0}.")
});
