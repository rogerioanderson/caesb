/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ZH (Chinese; 中文 (Zh�?ngwén), 汉语, 漢語)
 * Region: TW (Taiwan)
 */
jQuery.extend(jQuery.validator.messages, {
        required: "必填",
		remote: "請修正此欄�?",
		email: "請輸入正確的電�?信箱",
		url: "請輸入�?�法的URL",
		date: "請輸入�?�法的日期",
		dateISO: "請輸入�?�法的日期 (ISO).",
		number: "請輸入數字",
		digits: "請輸入整數",
		creditcard: "請輸入�?�法的信用�?�號碼",
		equalTo: "請�?複輸入一次",
		accept: "請輸入有效的後缀字串",
		maxlength: jQuery.validator.format("請輸入長度�?大於{0} 的字串"),
		minlength: jQuery.validator.format("請輸入長度�?�?於 {0} 的字串"),
		rangelength: jQuery.validator.format("請輸入長度介於 {0} 和 {1} 之間的字串"),
		range: jQuery.validator.format("請輸入介於 {0} 和 {1} 之間的數值"),
		max: jQuery.validator.format("請輸入�?大於 {0} 的數值"),
		min: jQuery.validator.format("請輸入�?�?於 {0} 的數值")
});
