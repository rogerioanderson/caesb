/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: RU (Russian; ру�?�?кий �?зык)
 */
jQuery.extend(jQuery.validator.messages, {
        required: "Это поле необходимо заполнить.",
        remote: "Пожалуй�?та, введите правильное значение.",
        email: "Пожалуй�?та, введите корректный адре�? �?лектронной почты.",
        url: "Пожалуй�?та, введите корректный URL.",
        date: "Пожалуй�?та, введите корректную дату.",
        dateISO: "Пожалуй�?та, введите корректную дату в формате ISO.",
        number: "Пожалуй�?та, введите чи�?ло.",
        digits: "Пожалуй�?та, вводите только цифры.",
        creditcard: "Пожалуй�?та, введите правильный номер кредитной карты.",
        equalTo: "Пожалуй�?та, введите такое же значение ещё раз.",
        accept: "Пожалуй�?та, выберите файл �? правильным ра�?ширением.",
        maxlength: jQuery.validator.format("Пожалуй�?та, введите не больше {0} �?имволов."),
        minlength: jQuery.validator.format("Пожалуй�?та, введите не меньше {0} �?имволов."),
        rangelength: jQuery.validator.format("Пожалуй�?та, введите значение длиной от {0} до {1} �?имволов."),
        range: jQuery.validator.format("Пожалуй�?та, введите чи�?ло от {0} до {1}."),
        max: jQuery.validator.format("Пожалуй�?та, введите чи�?ло, меньшее или равное {0}."),
        min: jQuery.validator.format("Пожалуй�?та, введите чи�?ло, большее или равное {0}.")
});
