/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// Republic of Korea
plupload.addI18n({
   'Select files' : '파�?� 선�?',
   'Add files to the upload queue and click the start button.' : '파�?��?� 업로드 �??�? 추가하여 시작 버튼�?� �?�릭하십시오.',
   'Filename' : '파�?� �?�름',
   'Status' : '�?태',
   'Size' : '�?�기',
   'Add Files' : '파�?� 추가',
   'Stop Upload': '업로드 중지',
   'Start Upload': '업로드',
   'Add files': '파�?� 추가',
   'Stop current upload': '현재 업로드를 정지',
   'Start uploading queue': '업로드',
   'Stop upload': '업로드 중지',
   'Start upload': '업로드',
   'Uploaded % d / % d files': '업로드 중 % d / % d 파�?�',
   'N / A': 'N / A',
   'Drag files here': '여기�? 파�?��?� 드래그',
   'File extension error': '파�?� 확장�? 오류',
   'File size error': '파�?� �?�기 오류',
   'File count error': '�?�미지 : 오류',
   'Init error': '초기화 오류',
   'HTTP Error': 'HTTP 오류',
   'Security error': '보안 오류',
   'Generic error': '오류',
   'IO error': 'IO 오류',
   'File : % s': '파�?� % s',
   'Close': '닫기',
   '% d files queued': '% d 파�?��?� 추가�?�었습니다',
   'Using runtime :': '모드',
   'File : % f, size : % s, max file size : % m': '파�?� : % f, �?�기 : % s, 최대 파�?� �?�기 : % m',
   'Upload element accepts only % d file (s) at a time. Extra files were stripped': '업로드 가능한 파�?��?� 수는 % d입니다. 불필요한 파�?��?� 삭제�?�었습니다 ',
   'Upload URL might be wrong or doesn \'t exist ':'업로드할 URL�?� 존재하지 않습니다 ',
   'Error : File too large :': '오류 : �?�기가 너무 �?�니다',
   'Error : Invalid file extension :': '오류 : 확장�?가 허용�?�지 않습니다 :'
});
