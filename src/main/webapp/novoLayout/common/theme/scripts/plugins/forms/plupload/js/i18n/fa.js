/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// Persian
plupload.addI18n({
   'Select files' : 'انتخاب �?ایل',
   'Add files to the upload queue and click the start button.' : 'اضا�?ه کنید �?ایل ها را به ص�? آپلود و دکمه شروع را کلیک کنید.',
   'Filename' : 'نام �?ایل',
   'Status' : 'وضعیت',
   'Size' : 'سایز',
   'Add Files' : 'ا�?زودن �?ایل',
   'Stop Upload' : 'توق�? انتقال',
   'Start Upload' : 'شروع انتقال',
   'Add files' : 'ا�?زودن �?ایل',
   'Add files.' : 'ا�?زودن �?ایل',
   'Stop current upload' : 'توق�? انتقال جاری',
   'Start uploading queue' : 'شروع ص�? انتقال',
   'Stop upload' : 'توق�?  انتقال',
   'Start upload' : 'شروع انتقال',
   'Uploaded %d/%d files': 'منتقل شد %d/%d از �?ایلها',
   'N/A' : 'N/A',
   'Drag files here.' : 'بکشید �?ایل ها رو به اینجا',
   'File extension error.': 'خطا پیشوند �?ایل',
   'File size error.': 'خطای سایز �?ایل',
   'File count error.': 'خطای تعداد �?ایل',
   'Init error.': 'خطا در استارت اسکریپت',
   'HTTP Error.': 'HTTP خطای',
   'Security error.': 'خطای امنیتی',
   'Generic error.': 'خطای عمومی',
   'IO error.': 'IO خطای',
   'File: %s': ' �?ایل ها : %s',
   'Close': 'بستن',
   '%d files queued': '%d �?ایل در ص�?',
   'Using runtime: ': 'است�?اده میکنید از : ',
   'File: %f, size: %s, max file size: %m': �?ایل: %f, سایز: %s, بزرگترین سایز �?ایل: %m',
   'Upload element accepts only %d file(s) at a time. Extra files were stripped.': 'عنصر بارگذار �?قط %d �?ایل رو در یک زمان  می پذیرد. سایر �?ایل ها مجرد از این موضوع هستند.',
   'Upload URL might be wrong or doesn\'t exist': 'آدرس آپلود اشتباه می باشد یا وجود ندارد',
   'Error: File too large: ': 'خطا: �?ایل حجیم است :: ',
   'Error: Invalid file extension: ': 'خطا پسوند �?ایل معتبر نمی باشد : '
});
