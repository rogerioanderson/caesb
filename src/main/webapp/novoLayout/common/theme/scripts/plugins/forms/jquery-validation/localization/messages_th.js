/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: TH (Thai; ไทย)
 */
jQuery.extend(jQuery.validator.messages, {
	required: "โปรดระบุ",
	remote: "โปรด�?�?้ไขให้ถู�?ต้อง",
	email: "โปรดระบุที่อยู่อีเมล์ที่ถู�?ต้อง",
	url: "โปรดระบุ URL ที่ถู�?ต้อง",
	date: "โปรดระบุวันที่ ที่ถู�?ต้อง",
	dateISO: "โปรดระบุวันที่ ที่ถู�?ต้อง (ระบบ ISO).",
	number: "โปรดระบุทศนิยมที่ถู�?ต้อง",
	digits: "โปรดระบุจำนวนเต็มที่ถู�?ต้อง",
	creditcard: "โปรดระบุรหัสบัตรเครดิตที่ถู�?ต้อง",
	equalTo: "โปรดระบุค่าเดิมอี�?ครั้ง",
	accept: "โปรดระบุค่าที่มีส่วนขยายที่ถู�?ต้อง",
	maxlength: jQuery.validator.format("โปรดอย่าระบุค่าที่ยาว�?ว่า {0} อั�?ขระ"),
	minlength: jQuery.validator.format("โปรดอย่าระบุค่าที่สั้น�?ว่า {0} อั�?ขระ"),
	rangelength: jQuery.validator.format("โปรดอย่าระบุค่าความยาวระหว่าง {0} ถึง {1} อั�?ขระ"),
	range: jQuery.validator.format("โปรดระบุค่าระหว่าง {0} �?ละ {1}"),
	max: jQuery.validator.format("โปรดระบุค่าน้อย�?ว่าหรือเท่า�?ับ {0}"),
	min: jQuery.validator.format("โปรดระบุค่ามา�?�?ว่าหรือเท่า�?ับ {0}")
});
