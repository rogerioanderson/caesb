/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: LV (Latvian; latviešu valoda)
 */
jQuery.extend(jQuery.validator.messages, {
        required: "Šis lauks ir oblig�?ts.",
        remote: "Lūdzu, p�?rbaudiet šo lauku.",
        email: "Lūdzu, ievadiet derīgu e-pasta adresi.",
        url: "Lūdzu, ievadiet derīgu URL adresi.",
        date: "Lūdzu, ievadiet derīgu datumu.",
        dateISO: "Lūdzu, ievadiet derīgu datumu (ISO).",
        number: "Lūdzu, ievadiet derīgu numuru.",
        digits: "Lūdzu, ievadiet tikai ciparus.",
        creditcard: "Lūdzu, ievadiet derīgu kredītkartes numuru.",
        equalTo: "Lūdzu, ievadiet to pašu vēlreiz.",
        accept: "Lūdzu, ievadiet vērtību ar derīgu paplašin�?jumu.",
        maxlength: jQuery.validator.format("Lūdzu, ievadiet ne vair�?k k�? {0} rakstzīmes."),
        minlength: jQuery.validator.format("Lūdzu, ievadiet vismaz {0} rakstzīmes."),
        rangelength: jQuery.validator.format("Lūdzu ievadiet {0} līdz {1} rakstzīmes."),
        range: jQuery.validator.format("Lūdzu, ievadiet skaitli no {0} līdz {1}."),
        max: jQuery.validator.format("Lūdzu, ievadiet skaitli, kurš ir maz�?ks vai vien�?ds ar {0}."),
        min: jQuery.validator.format("Lūdzu, ievadiet skaitli, kurš ir liel�?ks vai vien�?ds ar {0}.")
});
