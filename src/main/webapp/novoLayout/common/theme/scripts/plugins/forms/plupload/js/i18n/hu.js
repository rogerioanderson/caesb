/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// Hungarian
plupload.addI18n({
    'Select files' : 'Fájlok kiválasztása',
    'Add files to the upload queue and click the start button.' : 'Válaszd ki a fájlokat, majd kattints az Indítás gombra.',
    'Filename' : 'Fájlnév',
    'Status' : '�?llapot',
    'Size' : 'Méret',
    'Add files' : 'Hozzáadás',
    'Stop current upload' : 'Jelenlegi feltöltés megszakítása',
    'Start uploading queue' : 'Várakozási sor feltöltésének indítása',
    'Uploaded %d/%d files': 'Feltöltött fájlok: %d/%d',
    'N/A': 'Nem elérhető',
    'Drag files here.' : 'Húzd ide a fájlokat.',
    'Stop upload': 'Feltöltés megszakítása',
    'Start upload': 'Indítás',
    '%d files queued': '%d fájl sorbaállítva',
    'File: %s': 'Fájl: %s',
    'Close': 'Bezárás',
    'Using runtime: ': 'Használt runtime: ',
    'File: %f, size: %s, max file size: %m': 'Fájl: %f, méret: %s, maximális fájlméret: %m',
    'Upload element accepts only %d file(s) at a time. Extra files were stripped.': 'A feltöltés egyszerre csak %d fájlt fogad el, a többi fájl nem lesz feltöltve.',
    'Upload URL might be wrong or doesn\'t exist': 'A megadott URL hibás vagy nem létezik',
    'Error: File too large: ': 'Hiba: A fájl túl nagy: ',
    'Error: Invalid file extension: ': 'Hiba: Érvénytelen fájlkiterjesztés: ',
    'File extension error.': 'Hibás fájlkiterjesztés.',
    'File size error.': 'Hibás fájlméret.',
    'File count error.': 'A fájlok számával kapcsolatos hiba.',
    'Init error.': 'Init hiba.',
    'HTTP Error.': 'HTTP hiba.',
    'Security error.': 'Biztonsági hiba.',
    'Generic error.': '�?ltalános hiba.',
    'IO error.': 'I/O hiba.'
});
