/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: BG (Bulgarian; българ�?ки език)
 */
jQuery.extend(jQuery.validator.messages, {
		 required: "Полето е задължително.",
		 remote: "Мол�?, въведете правилната �?тойно�?т.",
		 email: "Мол�?, въведете валиден email.",
		 url: "Мол�?, въведете валидно URL.",
		 date: "Мол�?, въведете валидна дата.",
		 dateISO: "Мол�?, въведете валидна дата (ISO).",
		 number: "Мол�?, въведете валиден номер.",
		 digits: "Мол�?, въведете �?амо цифри",
		 creditcard: "Мол�?, въведете валиден номер на кредитна карта.",
		 equalTo: "Мол�?, въведете �?ъщата �?тойно�?т отново.",
		 accept: "Мол�?, въведете �?тойно�?т �? валидно разширение.",
		 maxlength: $.validator.format("Мол�?, въведете повече от {0} �?имвола."),
		 minlength: $.validator.format("Мол�?, въведете поне {0} �?имвола."),
		 rangelength: $.validator.format("Мол�?, въведете �?тойно�?т �? дължина между {0} и {1} �?имвола."),
		 range: $.validator.format("Мол�?, въведете �?тойно�?т между {0} и {1}."),
		 max: $.validator.format("Мол�?, въведете �?тойно�?т по-малка или равна на {0}."),
		 min: $.validator.format("Мол�?, въведете �?тойно�?т по-гол�?ма или равна на {0}.")
});
