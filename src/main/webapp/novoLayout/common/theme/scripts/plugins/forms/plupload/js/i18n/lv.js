/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// .lv file like language pack
plupload.addI18n({
    'Select files' : 'Izvēlieties failus',
    'Add files to the upload queue and click the start button.' : 'Pieveinojiet failus rindai un klikšķiniet uz "S�?kt augšupiel�?di" pogas.',
    'Filename' : 'Faila nosaukums',
    'Status' : 'Statuss',
    'Size' : 'Izmērs',
    'Add files' : 'Pievienot failus',
    'Stop current upload' : 'Apturēt pašreizējo augšupiel�?di',
    'Start uploading queue' : 'S�?kt augšupiel�?di',
    'Drag files here.' : 'Ievelciet failus šeit',
    'Start upload' : 'S�?kt augšupiel�?di',
    'Uploaded %d/%d files': 'Augšupiel�?dēti %d/%d faili',
    'Stop upload': 'P�?rtraukt augšupiel�?di',
    'Start upload': 'S�?kt augšupiel�?di',
    '%d files queued': '%d faili pievienoti rindai',
    'File: %s': 'Fails: %s',
    'Close': 'Aizvērt',
    'Using runtime: ': 'Lieto saskarni: ',
    'File: %f, size: %s, max file size: %m': 'Fails: %f, izmērs: %s, maksim�?lais faila izmērs: %m',
    'Upload element accepts only %d file(s) at a time. Extra files were stripped.': 'Iespējams iel�?dēt tikai %d failus vien�? reizē. Atlikušie faili netika pievienoti',
    'Upload URL might be wrong or doesn\'t exist': 'Augšupiel�?des URL varētu būt nepareizs vai neeksistē',
    'Error: File too large: ': 'Kļūda: Fails p�?r�?k liels: ',
    'Error: Invalid file extension: ': 'Kļūda: Nekorekts faila paplašin�?jums:',
    'File extension error.': 'Faila paplašin�?juma kļūda.',
    'File size error.': 'Faila izmēra kļūda.',
    'File count error.': 'Failu skaita kļūda',
    'Init error.': 'Inicializ�?cijas kļūda.',
    'HTTP Error.': 'HTTP kļūda.',
    'Security error.': 'Drošības kļūda.',
    'Generic error.': 'Visp�?rēja rakstura kļūda.',
    'IO error.': 'Ievades/Izvades kļūda.'
});
