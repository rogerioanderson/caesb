/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// Italian
plupload.addI18n({
    'Select files' : 'Seleziona i files',
    'Add files to the upload queue and click the start button.' : 'Aggiungi i file alla coda di caricamento e clicca il pulsante di avvio.',
    'Filename' : 'Nome file',
    'Status' : 'Stato',
    'Size' : 'Dimensione',
    'Add Files' : 'Aggiungi file',
    'Stop current upload' : 'Interrompi il caricamento',
    'Start uploading queue' : 'Avvia il caricamento',
    'Uploaded %d/%d files': 'Caricati %d/%d file',
    'N/A' : 'N/D',
    'Drag files here.' : 'Trascina i file qui.',
    'File extension error.': 'Errore estensione file.',
    'File size error.': 'Errore dimensione file.',
    'Init error.': 'Errore inizializzazione.',
    'HTTP Error.': 'Errore HTTP.',
    'Security error.': 'Errore sicurezza.',
    'Generic error.': 'Errore generico.',
    'IO error.': 'Errore IO.',
	'Stop Upload': 'Ferma Upload',
	'Start Upload': 'Inizia Upload',
	'%d files queued': '%d file in lista'
});
