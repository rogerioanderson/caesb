/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: UK (Ukrainian; україн�?ька мова)
 */
jQuery.extend(jQuery.validator.messages, {
    required: "Це поле необхідно заповнити.",
    remote: "Будь ла�?ка, введіть правильне значенн�?.",
    email: "Будь ла�?ка, введіть коректну адре�?у електронної пошти.",
    url: "Будь ла�?ка, введіть коректний URL.",
    date: "Будь ла�?ка, введіть коректну дату.",
    dateISO: "Будь ла�?ка, введіть коректну дату у форматі ISO.",
    number: "Будь ла�?ка, введіть чи�?ло.",
    digits: "Вводите потрібно лише цифри.",
    creditcard: "Будь ла�?ка, введіть правильний номер кредитної карти.",
    equalTo: "Будь ла�?ка, введіть таке ж значенн�? ще раз.",
    accept: "Будь ла�?ка, виберіть файл з правильним розширенн�?м.",
    maxlength: jQuery.validator.format("Будь ла�?ка, введіть не більше {0} �?имволів."),
    minlength: jQuery.validator.format("Будь ла�?ка, введіть не менше {0} �?имволів."),
    rangelength: jQuery.validator.format("Будь ла�?ка, введіть значенн�? довжиною від {0} до {1} �?имволів."),
    range: jQuery.validator.format("Будь ла�?ка, введіть чи�?ло від {0} до {1}."),
    max: jQuery.validator.format("Будь ла�?ка, введіть чи�?ло, менше або рівно {0}."),
    min: jQuery.validator.format("Будь ла�?ка, введіть чи�?ло, більше або рівно {0}.")
});
