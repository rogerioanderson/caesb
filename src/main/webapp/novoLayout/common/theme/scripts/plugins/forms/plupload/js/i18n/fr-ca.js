/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// French-Canadian
plupload.addI18n({
    'Select files' : 'Sélectionnez les fichiers',
    'Add files to the upload queue and click the start button.' : 'Ajoutez des fichiers à la file d\'attente et appuyez sur le bouton démarrer.',
    'Filename' : 'Nom du fichier',
    'Status' : 'Statut',
    'Size' : 'Taille',
    'Add files' : 'Ajouter Fichiers',
    'Stop current upload' : 'Arrêter le téléversement actuel',
    'Start uploading queue' : 'Démarrer le téléversement',
    'Uploaded %d/%d files': '%d/%d fichiers envoyés',
    'N/A' : 'Non applicable',
    'Drag files here.' : 'Glisser-déposer les fichiers ici',
    'File extension error.': 'Erreur d\'extension de fichier',
    'File size error.': 'Erreur de taille de fichier',
    'Init error.': 'Erreur d\'initialisation',
    'HTTP Error.': 'Erreur HTTP',
    'Security error.': 'Erreur de sécurité',
    'Generic error.': 'Erreur commune',
    'IO error.': 'Erreur E/S',
    'Stop Upload': 'Arrêter le téléversement',
    'Add Files': 'Ajouter des fichiers',
    'Start upload': 'Démarrer le téléversement',
    '%d files queued': '%d fichiers en attente',
    'File: %s':'Fichier: %s',
    'Close':'Fermer',
    'Using runtime:':'Moteur logiciel:',
    'File: %f, size: %s, max file size: %m':'Fichier: %f, poids: %s, poids maximal: %m',
    'Upload element accepts only %d file(s) at a time. Extra files were stripped.':'La file accepte %d fichier(s) à la fois. Les fichiers en trop sont ignorés',
    'Upload URL might be wrong or doesn\'t exist':'L\'URL de téléversement est erroné ou inexistant',
    'Error: File to large: ':'Fichier trop volumineux: ',
    'Error: Invalid file extension: ':'Extension de fichier invalide: ',
    'File size error.':'Erreur de taile de fichier',
    'File count error.':'Erreur de décompte des fichiers'    
});
