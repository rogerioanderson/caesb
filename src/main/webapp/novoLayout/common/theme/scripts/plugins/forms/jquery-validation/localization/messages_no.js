/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: NO (Norwegian; Norsk)
 */
jQuery.extend(jQuery.validator.messages, {
       required: "Dette feltet er obligatorisk.",
       maxlength: jQuery.validator.format("Maksimalt {0} tegn."),
       minlength: jQuery.validator.format("Minimum {0} tegn."),
       rangelength: jQuery.validator.format("Angi minimum {0} og maksimum {1} tegn."),
       email: "Oppgi en gyldig epostadresse.",
       url: "Angi en gyldig URL.",
       date: "Angi en gyldig dato.",
       dateISO: "Angi en gyldig dato (&ARING;&ARING;&ARING;&ARING;-MM-DD).",
       dateSE: "Angi en gyldig dato.",
       number: "Angi et gyldig nummer.",
       numberSE: "Angi et gyldig nummer.",
       digits: "Skriv kun tall.",
       equalTo: "Skriv samme verdi igjen.",
       range: jQuery.validator.format("Angi en verdi mellom {0} og {1}."),
       max: jQuery.validator.format("Angi en verdi som er mindre eller lik {0}."),
       min: jQuery.validator.format("Angi en verdi som er st&oslash;rre eller lik {0}."),
       creditcard: "Angi et gyldig kredittkortnummer."
});
