/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: VI (Vietnamese; Tiếng Việt)
 */
jQuery.extend(jQuery.validator.messages, {
 required: "Hãy nhập.",
 remote: "Hãy sửa cho đúng.",
 email: "Hãy nhập email.",
 url: "Hãy nhập URL.",
 date: "Hãy nhập ngày.",
 dateISO: "Hãy nhập ngày (ISO).",
 number: "Hãy nhập số.",
 digits: "Hãy nhập chữ số.",
 creditcard: "Hãy nhập số thẻ tín dụng.",
 equalTo: "Hãy nhập thêm lần nữa.",
 accept: "Phần mở rộng không đúng.",
 maxlength: jQuery.format("Hãy nhập từ {0} kí tự trở xuống."),
 minlength: jQuery.format("Hãy nhập từ {0} kí tự trở lên."),
 rangelength: jQuery.format("Hãy nhập từ {0} đến {1} kí tự."),
 range: jQuery.format("Hãy nhập từ {0} đến {1}."),
 max: jQuery.format("Hãy nhập từ {0} trở xuống."),
 min: jQuery.format("Hãy nhập từ {1} trở lên.")
});
