/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: CA (Catalan; català)
 */
jQuery.extend(jQuery.validator.messages, {
  required: "Aquest camp és obligatori.",
  remote: "Si us plau, omple aquest camp.",
  email: "Si us plau, escriu una adreça de correu-e vàlida",
  url: "Si us plau, escriu una URL vàlida.",
  date: "Si us plau, escriu una data vàlida.",
  dateISO: "Si us plau, escriu una data (ISO) vàlida.",
  number: "Si us plau, escriu un número enter vàlid.",
  digits: "Si us plau, escriu només dígits.",
  creditcard: "Si us plau, escriu un número de tarjeta vàlid.",
  equalTo: "Si us plau, escriu el maateix valor de nou.",
  accept: "Si us plau, escriu un valor amb una extensió acceptada.",
  maxlength: jQuery.validator.format("Si us plau, no escriguis més de {0} caracters."),
  minlength: jQuery.validator.format("Si us plau, no escriguis menys de {0} caracters."),
  rangelength: jQuery.validator.format("Si us plau, escriu un valor entre {0} i {1} caracters."),
  range: jQuery.validator.format("Si us plau, escriu un valor entre {0} i {1}."),
  max: jQuery.validator.format("Si us plau, escriu un valor menor o igual a {0}."),
  min: jQuery.validator.format("Si us plau, escriu un valor major o igual a {0}.")
});
