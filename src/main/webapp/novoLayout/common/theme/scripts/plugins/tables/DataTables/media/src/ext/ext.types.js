/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/


$.extend( DataTable.ext.aTypes, [
	/*
	 * Function: -
	 * Purpose:  Check to see if a string is numeric
	 * Returns:  string:'numeric' or null
	 * Inputs:   mixed:sText - string to check
	 */
	function ( sData )
	{
		/* Allow zero length strings as a number */
		if ( typeof sData === 'number' )
		{
			return 'numeric';
		}
		else if ( typeof sData !== 'string' )
		{
			return null;
		}
		
		var sValidFirstChars = "0123456789-";
		var sValidChars = "0123456789.";
		var Char;
		var bDecimal = false;
		
		/* Check for a valid first char (no period and allow negatives) */
		Char = sData.charAt(0); 
		if (sValidFirstChars.indexOf(Char) == -1) 
		{
			return null;
		}
		
		/* Check all the other characters are valid */
		for ( var i=1 ; i<sData.length ; i++ ) 
		{
			Char = sData.charAt(i); 
			if (sValidChars.indexOf(Char) == -1) 
			{
				return null;
			}
			
			/* Only allowed one decimal place... */
			if ( Char == "." )
			{
				if ( bDecimal )
				{
					return null;
				}
				bDecimal = true;
			}
		}
		
		return 'numeric';
	},
	
	/*
	 * Function: -
	 * Purpose:  Check to see if a string is actually a formatted date
	 * Returns:  string:'date' or null
	 * Inputs:   string:sText - string to check
	 */
	function ( sData )
	{
		var iParse = Date.parse(sData);
		if ( (iParse !== null && !isNaN(iParse)) || (typeof sData === 'string' && sData.length === 0) )
		{
			return 'date';
		}
		return null;
	},
	
	/*
	 * Function: -
	 * Purpose:  Check to see if a string should be treated as an HTML string
	 * Returns:  string:'html' or null
	 * Inputs:   string:sText - string to check
	 */
	function ( sData )
	{
		if ( typeof sData === 'string' && sData.indexOf('<') != -1 && sData.indexOf('>') != -1 )
		{
			return 'html';
		}
		return null;
	}
] );

