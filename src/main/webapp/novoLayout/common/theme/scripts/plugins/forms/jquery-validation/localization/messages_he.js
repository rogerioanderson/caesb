/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: HE (Hebrew; עברית)
 */
jQuery.extend(jQuery.validator.messages, {
	required: ".השדה הזה הינו שדה חובה",
	remote: "נ�? לתקן שדה זה.",
	email: "נ�? למל�? כתובת דו�?\"ל חוקית",
	url: "נ�? למל�? כתובת �?ינטרנט חוקית.",
	date: "נ�? למל�? ת�?ריך חוקי",
	dateISO: "נ�? למל�? ת�?ריך חוקי (ISO).",
	number: "נ�? למל�? מספר.",
	digits: ".נ�? למל�? רק מספרי�?",
	creditcard: "נ�? למל�? מספר כרטיס �?שר�?י חוקי.",
	equalTo: "נ�? למל�? �?ת �?ותו ערך שוב.",
	accept: "נ�? למל�? ערך ע�? סיומת חוקית.",
	maxlength: jQuery.validator.format(".נ�? ל�? למל�? יותר מ- {0} תווי�?"),
	minlength: jQuery.validator.format("נ�? למל�? לפחות {0} תווי�?."),
	rangelength: jQuery.validator.format("נ�? למל�? ערך בין {0} ל- {1} תווי�?."),
	range: jQuery.validator.format("נ�? למל�? ערך בין {0} ל- {1}."),
	max: jQuery.validator.format("נ�? למל�? ערך קטן �?ו שווה ל- {0}."),
	min: jQuery.validator.format("נ�? למל�? ערך גדול �?ו שווה ל- {0}.")
});
