/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// .fi file like language pack
plupload.addI18n({
	'Select files' : 'Valitse tiedostoja',
	'Add files to the upload queue and click the start button.' : 'Lisää tiedostoja latausjonoon ja klikkaa aloita-nappia.',
	'Filename' : 'Tiedostonimi',
	'Status' : 'Tila',
	'Size' : 'Koko',
	'Add files' : 'Lisää tiedostoja',
	'Stop current upload' : 'Pysäytä nykyinen lataus',
	'Start uploading queue' : 'Aloita jonon lataus',
	'Drag files here.' : 'Raahaa tiedostot tänne.',
	'Start upload' : 'Aloita lataus',
	'Uploaded %d/%d files': 'Ladattu %d/%d tiedostoa',
	'Stop upload': 'Pysäytä lataus',
	'Start upload': 'Aloita lataus',
	'%d files queued': '%d tiedostoa jonossa',
	'File: %s': 'Tiedosto: %s',
	'Close': 'Sulje',
	'Using runtime: ': 'Käytetään ajonaikaista: ',
	'File: %f, size: %s, max file size: %m': 'Tiedosto: %f, koko: %s, maksimi tiedostokoko: %m',
	'Upload element accepts only %d file(s) at a time. Extra files were stripped.': 'Latauselementti sallii ladata vain %d tiedosto(a) kerrallaan. Ylimääräiset tiedostot ohitettiin.',
	'Upload URL might be wrong or doesn\'t exist': 'Lataus URL saattaa olla väärin tai ei ole olemassa',
	'Error: File too large: ': 'Virhe: Tiedosto liian suuri: ',
	'Error: Invalid file extension: ': 'Virhe: Kelpaamaton tiedostopääte: ',
	'File extension error.': 'Tiedostopäätevirhe.',
	'File size error.': 'Tiedostokokovirhe.',
	'File count error.': 'Tiedostolaskentavirhe.',
	'Init error.': 'Init virhe.',
	'HTTP Error.': 'HTTP virhe.',
	'Security error.': 'Tietoturvavirhe.',
	'Generic error.': 'Yleinen virhe.',
	'IO error.': 'I/O virhe.'
});
