/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: EU (Basque; euskara, euskera)
 */
jQuery.extend(jQuery.validator.messages, {
  required: "Eremu hau beharrezkoa da.",
  remote: "Mesedez, bete eremu hau.",
  email: "Mesedez, idatzi baliozko posta helbide bat.",
  url: "Mesedez, idatzi baliozko URL bat.",
  date: "Mesedez, idatzi baliozko data bat.",
  dateISO: "Mesedez, idatzi baliozko (ISO) data bat.",
  number: "Mesedez, idatzi baliozko zenbaki oso bat.",
  digits: "Mesedez, idatzi digituak soilik.",
  creditcard: "Mesedez, idatzi baliozko txartel zenbaki bat.",
  equalTo: "Mesedez, idatzi berdina berriro ere.",
  accept: "Mesedez, idatzi onartutako luzapena duen balio bat.",
  maxlength: jQuery.validator.format("Mesedez, ez idatzi {0} karaktere baino gehiago."),
  minlength: jQuery.validator.format("Mesedez, ez idatzi {0} karaktere baino gutxiago."),
  rangelength: jQuery.validator.format("Mesedez, idatzi {0} eta {1} karaktere arteko balio bat."),
  range: jQuery.validator.format("Mesedez, idatzi {0} eta {1} arteko balio bat."),
  max: jQuery.validator.format("Mesedez, idatzi {0} edo txikiagoa den balio bat."),
  min: jQuery.validator.format("Mesedez, idatzi {0} edo handiagoa den balio bat.")
});
