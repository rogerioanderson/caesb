/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/

$.extend( DataTable.ext.oStdClasses, {
	"sTable": "dataTable",

	/* Two buttons buttons */
	"sPagePrevEnabled": "paginate_enabled_previous",
	"sPagePrevDisabled": "paginate_disabled_previous",
	"sPageNextEnabled": "paginate_enabled_next",
	"sPageNextDisabled": "paginate_disabled_next",
	"sPageJUINext": "",
	"sPageJUIPrev": "",
	
	/* Full numbers paging buttons */
	"sPageButton": "paginate_button",
	"sPageButtonActive": "paginate_active",
	"sPageButtonStaticDisabled": "paginate_button paginate_button_disabled",
	"sPageFirst": "first",
	"sPagePrevious": "previous",
	"sPageNext": "next",
	"sPageLast": "last",
	
	/* Striping classes */
	"sStripeOdd": "odd",
	"sStripeEven": "even",
	
	/* Empty row */
	"sRowEmpty": "dataTables_empty",
	
	/* Features */
	"sWrapper": "dataTables_wrapper",
	"sFilter": "dataTables_filter",
	"sInfo": "dataTables_info",
	"sPaging": "dataTables_paginate paging_", /* Note that the type is postfixed */
	"sLength": "dataTables_length",
	"sProcessing": "dataTables_processing",
	
	/* Sorting */
	"sSortAsc": "sorting_asc",
	"sSortDesc": "sorting_desc",
	"sSortable": "sorting", /* Sortable in both directions */
	"sSortableAsc": "sorting_asc_disabled",
	"sSortableDesc": "sorting_desc_disabled",
	"sSortableNone": "sorting_disabled",
	"sSortColumn": "sorting_", /* Note that an int is postfixed for the sorting order */
	"sSortJUIAsc": "",
	"sSortJUIDesc": "",
	"sSortJUI": "",
	"sSortJUIAscAllowed": "",
	"sSortJUIDescAllowed": "",
	"sSortJUIWrapper": "",
	"sSortIcon": "",
	
	/* Scrolling */
	"sScrollWrapper": "dataTables_scroll",
	"sScrollHead": "dataTables_scrollHead",
	"sScrollHeadInner": "dataTables_scrollHeadInner",
	"sScrollBody": "dataTables_scrollBody",
	"sScrollFoot": "dataTables_scrollFoot",
	"sScrollFootInner": "dataTables_scrollFootInner",
	
	/* Misc */
	"sFooterTH": "",
	"sJUIHeader": "",
	"sJUIFooter": ""
} );


$.extend( DataTable.ext.oJUIClasses, DataTable.ext.oStdClasses, {
	/* Two buttons buttons */
	"sPagePrevEnabled": "fg-button ui-button ui-state-default ui-corner-left",
	"sPagePrevDisabled": "fg-button ui-button ui-state-default ui-corner-left ui-state-disabled",
	"sPageNextEnabled": "fg-button ui-button ui-state-default ui-corner-right",
	"sPageNextDisabled": "fg-button ui-button ui-state-default ui-corner-right ui-state-disabled",
	"sPageJUINext": "ui-icon ui-icon-circle-arrow-e",
	"sPageJUIPrev": "ui-icon ui-icon-circle-arrow-w",
	
	/* Full numbers paging buttons */
	"sPageButton": "fg-button ui-button ui-state-default",
	"sPageButtonActive": "fg-button ui-button ui-state-default ui-state-disabled",
	"sPageButtonStaticDisabled": "fg-button ui-button ui-state-default ui-state-disabled",
	"sPageFirst": "first ui-corner-tl ui-corner-bl",
	"sPageLast": "last ui-corner-tr ui-corner-br",
	
	/* Features */
	"sPaging": "dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi "+
		"ui-buttonset-multi paging_", /* Note that the type is postfixed */
	
	/* Sorting */
	"sSortAsc": "ui-state-default",
	"sSortDesc": "ui-state-default",
	"sSortable": "ui-state-default",
	"sSortableAsc": "ui-state-default",
	"sSortableDesc": "ui-state-default",
	"sSortableNone": "ui-state-default",
	"sSortJUIAsc": "css_right ui-icon ui-icon-triangle-1-n",
	"sSortJUIDesc": "css_right ui-icon ui-icon-triangle-1-s",
	"sSortJUI": "css_right ui-icon ui-icon-carat-2-n-s",
	"sSortJUIAscAllowed": "css_right ui-icon ui-icon-carat-1-n",
	"sSortJUIDescAllowed": "css_right ui-icon ui-icon-carat-1-s",
	"sSortJUIWrapper": "DataTables_sort_wrapper",
	"sSortIcon": "DataTables_sort_icon",
	
	/* Scrolling */
	"sScrollHead": "dataTables_scrollHead ui-state-default",
	"sScrollFoot": "dataTables_scrollFoot ui-state-default",
	
	/* Misc */
	"sFooterTH": "ui-state-default",
	"sJUIHeader": "fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix",
	"sJUIFooter": "fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"
} );

