/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// French
plupload.addI18n({
    'Select files' : 'Sélectionnez les fichiers',
    'Add files to the upload queue and click the start button.' : 'Ajoutez des fichiers à la file et appuyez sur le bouton démarrer.',
    'Filename' : 'Nom de fichier',
    'Status' : 'Status',
    'Size' : 'Taille',
    'Add files' : 'Ajouter Fichiers',
    'Stop current upload' : 'Arrêter l\'envoi en cours',
    'Start uploading queue' : 'Démarrer l\'envoi',
    'Uploaded %d/%d files': '%d/%d fichiers envoyés',
    'N/A' : 'Non applicable',
    'Drag files here.' : 'Déposer les fichiers ici.',
    'File extension error.': 'Erreur extension fichier',
    'File size error.': 'Erreur taille fichier.',
    'Init error.': 'Erreur d\'initialisation.',
    'HTTP Error.': 'Erreur HTTP.',
    'Security error.': 'Erreur de sécurité.',
    'Generic error.': 'Erreur générique.',
    'IO error.': 'Erreur E/S.',
    'Stop Upload': 'Arrêter les envois.',
    'Add Files': 'Ajouter des fichiers',
    'Start Upload': 'Démarrer les envois.',
    '%d files queued': '%d fichiers en attente.'
});
