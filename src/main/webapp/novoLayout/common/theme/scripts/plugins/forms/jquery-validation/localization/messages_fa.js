/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: FA (Persian; �?ارسی)
 */
jQuery.extend(jQuery.validator.messages, {
       required: "تکمیل این �?یلد اجباری است.",
       remote: "لط�?ا این �?یلد را تصحیح کنید.",
       email: ".لط�?ا یک ایمیل صحیح وارد کنید",
       url: "لط�?ا آدرس صحیح وارد کنید.",
       date: "لط�?ا یک تاریخ صحیح وارد کنید",
       dateISO: "لط�?ا تاریخ صحیح وارد کنید (ISO).",
       number: "لط�?ا عدد صحیح وارد کنید.",
       digits: "لط�?ا تنها رقم وارد کنید",
       creditcard: "لط�?ا کریدیت کارت صحیح وارد کنید.",
       equalTo: "لط�?ا مقدار برابری وارد کنید",
       accept: "لط�?ا مقداری وارد کنید که ",
       maxlength: jQuery.validator.format("لط�?ا بیشتر از {0} حر�? وارد نکنید."),
       minlength: jQuery.validator.format("لط�?ا کمتر از {0} حر�? وارد نکنید."),
       rangelength: jQuery.validator.format("لط�?ا مقداری بین {0} تا {1} حر�? وارد کنید."),
       range: jQuery.validator.format("لط�?ا مقداری بین {0} تا {1} حر�? وارد کنید."),
       max: jQuery.validator.format("لط�?ا مقداری کمتر از {0} حر�? وارد کنید."),
       min: jQuery.validator.format("لط�?ا مقداری بیشتر از {0} حر�? وارد کنید.")
}); 
