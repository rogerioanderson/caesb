/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: EL (Greek; ελληνικά)
 */
jQuery.extend(jQuery.validator.messages, {
	required: "Αυτό το πεδίο είναι υποχ�?εωτικό.",
	remote: "Πα�?ακαλώ διο�?θώστε αυτό το πεδίο.",
	email: "Πα�?ακαλώ εισάγετε μια έγκυ�?η διε�?θυνση email.",
	url: "Πα�?ακαλώ εισάγετε ένα έγκυ�?ο URL.",
	date: "Πα�?ακαλώ εισάγετε μια έγκυ�?η ημε�?ομηνία.",
	dateISO: "Πα�?ακαλώ εισάγετε μια έγκυ�?η ημε�?ομηνία (ISO).",
	number: "Πα�?ακαλώ εισάγετε έναν έγκυ�?ο α�?ιθμό.",
	digits: "Πα�?ακαλώ εισάγετε μόνο α�?ιθμητικά ψηφία.",
	creditcard: "Πα�?ακαλώ εισάγετε έναν έγκυ�?ο α�?ιθμό πιστωτικής κά�?τας.",
	equalTo: "Πα�?ακαλώ εισάγετε την ίδια τιμή ξανά.",
	accept: "Πα�?ακαλώ εισάγετε μια τιμή με έγκυ�?η επέκταση α�?χείου.",
	maxlength: $.validator.format("Πα�?ακαλώ εισάγετε μέχ�?ι και {0} χα�?ακτή�?ες."),
	minlength: $.validator.format("Πα�?ακαλώ εισάγετε τουλάχιστον {0} χα�?ακτή�?ες."),
	rangelength: $.validator.format("Πα�?ακαλώ εισάγετε μια τιμή με μήκος μεταξ�? {0} και {1} χα�?ακτή�?ων."),
	range: $.validator.format("Πα�?ακαλώ εισάγετε μια τιμή μεταξ�? {0} και {1}."),
	max: $.validator.format("Πα�?ακαλώ εισάγετε μια τιμή μικ�?ότε�?η ή ίση του {0}."),
	min: $.validator.format("Πα�?ακαλώ εισάγετε μια τιμή μεγαλ�?τε�?η ή ίση του {0}.")
});

