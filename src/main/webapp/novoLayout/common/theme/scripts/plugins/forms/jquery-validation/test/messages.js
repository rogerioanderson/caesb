/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
module("messages");

test("predefined message not overwritten by addMethod(a, b, undefined)", function() {
	var message = "my custom message";
	$.validator.messages.custom = message;
	$.validator.addMethod("custom", function() {});
	deepEqual(message, $.validator.messages.custom);
	delete $.validator.messages.custom;
	delete $.validator.methods.custom;
});

test("group error messages", function() {
	$.validator.addClassRules({
		requiredDateRange: {required:true, date:true, dateRange:true}
	});
	$.validator.addMethod("dateRange", function() {
		return new Date($("#fromDate").val()) < new Date($("#toDate").val());
	}, "Please specify a correct date range.");
	var form = $("#dateRangeForm");
	form.validate({
		groups: {
			dateRange: "fromDate toDate"
		},
		errorPlacement: function(error) {
			form.find(".errorContainer").append(error);
		}
	});
	ok( !form.valid() );
	equal( 1, form.find(".errorContainer *").length );
	equal( "Please enter a valid date.", form.find(".errorContainer label.error").text() );

	$("#fromDate").val("12/03/2006");
	$("#toDate").val("12/01/2006");
	ok( !form.valid() );
	equal( "Please specify a correct date range.", form.find(".errorContainer label.error").text() );

	$("#toDate").val("12/04/2006");
	ok( form.valid() );
	ok( form.find(".errorContainer label.error").is(":hidden") );
});

test("read messages from metadata", function() {
	var form = $("#testForm9")
	form.validate();
	var e = $("#testEmail9")
	e.valid();
	equal( form.find("label").text(), "required" );
	e.val("bla").valid();
	equal( form.find("label").text(), "email" );
});


test("read messages from metadata, with meta option specified, but no metadata in there", function() {
	var form = $("#testForm1clean")
	form.validate({
		meta: "validate",
		rules: {
			firstname: "required"
		}
	});
	ok(!form.valid(), "not valid");
});
