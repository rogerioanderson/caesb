/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa็ใo Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo ้ parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart ้ um software livre; voc๊ pode redistribui-lo e/ou modificแ-lo dentro dos termos da Licen็a           *
* P๚blica Geral GNU como publicada pela Funda็ใo do Software Livre (FSF); na versใo 2 da Licen็a.                  *
*                                                                                                                  *
* Este programa/software ้ distribuํdo na esperan็a que possa ser ๚til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia implํcita de ADEQUAวรO a qualquer MERCADO ou APLICAวรO EM PARTICULAR. Veja a Licen็a P๚blica Geral      *
* GNU/GPL em portugu๊s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc๊ deve ter recebido uma c๓pia da Licen็a P๚blica Geral GNU, sob o tํtulo 'LICENCA.txt', junto com este        *
* programa/software, se nใo, acesse o Portal do Software P๚blico Brasileiro no endere็o www.softwarepublico.gov.br *
* ou escreva para a Funda็ใo do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: KA (Georgian; แฅแ?แ แแฃแแ)
 */
jQuery.extend(jQuery.validator.messages, {
        required: "แ?แ แแแแแก แจแแแกแแแ? แ?แฃแชแแแแแแแแ?.",
        remote: "แแแฎแ?แแ แแแฃแแแแ?แ แกแฌแ?แ แ แแแแจแแแแแ?แแ?.",
        email: "แแแฎแ?แแ แแแฃแแแแ?แ แแ-แคแ?แกแขแแก แแ?แ แแฅแขแฃแแ แแแกแ?แแ?แ แแ.",
        url: "แแแฎแ?แแ แแแฃแแแแ?แ แแ?แ แแฅแขแฃแแ URL.",
        date: "แแแฎแ?แแ แแแฃแแแแ?แ แแ?แ แแฅแขแฃแแ แแ?แ แแฆแ.",
        dateISO: "แแแฎแ?แแ แแแฃแแแแ?แ แแ?แ แแฅแขแฃแแ แแ?แ แแฆแ ISO แคแ?แ แแ?แขแจแ.",
        number: "แแแฎแ?แแ แแแฃแแแแ?แ แชแแคแ แ.",
        digits: "แแแฎแ?แแ แแแฃแแแแ?แ แแฎแ?แแ?แ แชแแคแ แแแ.",
        creditcard: "แแแฎแ?แแ แแแฃแแแแ?แ แกแ?แแ แแแแขแ? แแ?แ แ?แแแก แแ?แ แแฅแขแฃแแ แแ?แแแ แ.",
        equalTo: "แแแฎแ?แแ แแแฃแแแแ?แ แ?แกแแแแแ แแแแจแแแแแ?แแ? แแแแแ แแ แแฎแแ.",
        accept: "แแแฎแ?แแ แ?แแ แฉแแ?แ แคแ?แแแ แแ?แ แแฅแขแฃแแ แแ?แคแ?แ แแ?แแแแ.",
        maxlength: jQuery.validator.format("แแ?แกแ?แจแแแแแ? แ?แ แ?แฃแแแขแแก {0} แกแแแแ?แแ?."),
        minlength: jQuery.validator.format("แ?แฃแชแแแแแแแแ? แจแแแงแแ?แแ?แ แแแแแแฃแ {0} แกแแแแ?แแ?."),
        rangelength: jQuery.validator.format("แขแแฅแกแขแจแ แกแแแแ?แแ?แแแแก แ แ?แ?แแแแ?แแ? แฃแแแ? แแงแ?แก {0}-แแ?แ {1}-แแแ."),
        range: jQuery.validator.format("แแแฎแ?แแ แจแแแงแแ?แแ?แ แชแแคแ แ {0}-แแ?แ {1}-แแแ."),
        max: jQuery.validator.format("แแแฎแ?แแ แจแแแงแแ?แแ?แ แชแแคแ แ แ แ?แแแแแช แแ?แแแแแแ? แ?แ แฃแแ แแกย {0}-แก."),
        min: jQuery.validator.format("แแแฎแ?แแ แจแแแงแแ?แแ?แ แชแแคแ แ แ แ?แแแแแช แแแขแแ? แ?แ แฃแแ แแกย {0}-แก.")
});
