/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: JA (Japanese; 日本語)
 */
jQuery.extend(jQuery.validator.messages, {
  required: "�?��?�フィールド�?�必須�?��?�。",
  remote: "�?��?�フィールドを修正�?��?��??�?��?��?�。",
  email: "有効�?�Eメールアドレスを入力�?��?��??�?��?��?�。",
  url: "有効�?�URLを入力�?��?��??�?��?��?�。",
  date: "有効�?�日付を入力�?��?��??�?��?��?�。",
  dateISO: "有効�?�日付（ISO）を入力�?��?��??�?��?��?�。",
  number: "有効�?�数字を入力�?��?��??�?��?��?�。",
  digits: "数字�?��?�を入力�?��?��??�?��?��?�。",
  creditcard: "有効�?�クレジットカード番�?�を入力�?��?��??�?��?��?�。",
  equalTo: "�?��?�値をも�?�一度入力�?��?��??�?��?��?�。",
  accept: "有効�?�拡張�?を�?�む値を入力�?��?��??�?��?��?�。",
  maxlength: jQuery.format("{0} 文字以内�?�入力�?��?��??�?��?��?�。"),
  minlength: jQuery.format("{0} 文字以上�?�入力�?��?��??�?��?��?�。"),
  rangelength: jQuery.format("{0} 文字�?�ら {1} 文字�?��?��?�値を入力�?��?��??�?��?��?�。"),
  range: jQuery.format("{0} �?�ら {1} �?��?��?�値を入力�?��?��??�?��?��?�。"),
  max: jQuery.format("{0} 以下�?�値を入力�?��?��??�?��?��?�。"),
  min: jQuery.format("{0} 以上�?�値を入力�?��?��??�?��?��?�。")
});
