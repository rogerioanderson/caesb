<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<div class="span8">
	<div class="row-fluid">
		<div class="span8">
			<label class="campoObrigatorio strong"><fmt:message key="candidato.nomeCompleto" /></label>
			<input id="nome" name="nome" value="${nome}" style="width: 100% !important;" type='text' maxlength="150" class="Valid[Required] Description[citcorpore.comum.nome]" />
		</div>
	
		<div class="span4">
			<label class="campoObrigatorio strong"><fmt:message key="colaborador.cpf" /></label>
			<input id="cpf" type="text" name="cpf" value="${cpf}" style="width: 100% !important;" maxlength="14" class="Valid[Required] Description[colaborador.cpf]"  onclick="cpf()" onblur="validaCpf()"/>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span8">
			<label class="campoObrigatorio strong"><fmt:message key="avaliacaoFonecedor.email" /></label>
			<input id="email" name="email" value="${email}" style="width: 100% !important;" type='text' maxlength="150" class="Valid[Required] Description[avaliacaoFonecedor.email]" onblur="validaEmail()"/>
		</div>
	</div>
	<div id="recebeDiv">
		<div id="divSenha">
			<div class="row-fluid">
				<div class="span3">
					<label class="campoObrigatorio strong"><fmt:message key="eventoItemConfiguracao.senha" /></label>
					<input id="senha" name="senha" type="password" maxlength="20" onblur="testaSenha()" />
				</div>
		
				<div class="span3">
					<label class="campoObrigatorio strong"><fmt:message key="trabalheConosco.confirmacaoSenha" /></label>
					<input id="senha2" name="senha2" type="password" maxlength="20" onblur="testaSenha()"/>
				</div>
			</div>
		</div>
	</div>
	<div id="divAlterarSenha" style="display: none;">
		<fieldset>
			<label onclick="alterarSenha()" style="cursor: pointer; margin-top: 5px; margin-bottom: 5px;"><img alt="" src="${ctx}/template_new/images/icons/small/util/alterarsenha.png"><fmt:message key="usuario.alterarSenha"/></label>
		</fieldset>
	</div>
</div>
