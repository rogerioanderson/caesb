<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<script type="text/javascript" src="${ctx}/cit/objects/TreinamentoCurriculoDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/TelefoneCurriculoDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/EmailCurriculoDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/EnderecoCurriculoDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/FormacaoCurriculoDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/ExperienciaProfissionalCurriculoDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/FuncaoExperienciaProfissionalCurriculoDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/CompetenciaCurriculoDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/CertificacaoCurriculoDTO.js"></script>
<script type="text/javascript" src="${ctx}/cit/objects/IdiomaCurriculoDTO.js"></script>
<script type="text/javascript" src='${ctx}/js/UploadUtils.js'></script>
<script src="${ctx}/novoLayout/common/theme/scripts/plugins/forms/jquery-validation/demo/marketo/jquery.maskedinput.js" type="text/javascript"></script>
<script src="${ctx}/novoLayout/common/include/js/templateCurriculo.js"></script>
<script type="text/javascript">
/*bruno.aquino: foi retirado o trecho de c�digo abaixo de templateCurriculo.js pois n�o era possivel capturar o locale, este � incarregado de retirar a mascara do cpf quando a linguagem for ingl�s */
	addEvent(window, "load", load, false);
	function load(){
		$("#cep").mask("99.999-999");

		$("#cpf").mask("999.999.999-99");

		$("#telefone").mask("9999-99999");
		$("#dataNascimento").mask("99/99/9999");
		jQuery(document).ready(function($) {
			$('#telefone').keypress(function(e) {
				var tecla = e.keyCode ? e.keyCode : e.which;
				if((tecla > 47 && tecla < 58) || (tecla > 34 && tecla < 41) || tecla == 8 || tecla == 46) {
					return true;
				}
				return false;
			});
		});

	$(document).ready(function(){
		$('#inserir').click(function(event){
		 event.preventDefault();
		$("#remover_foto").show();
		$("#adicionar_foto").hide();
		});
		$('#remover_foto').click(function(event){
			event.preventDefault();
		$("#remover_foto").hide();
		$("#adicionar_foto").show();
		});
	});

	/* Desenvolvedor: Gilberto Nery - Data: 25/11/2013 - Hor�rio: 15:00 - ID Citsmart: 0
	 * Alterado por luiz.borges - 09/12/2013 - 09:30 hrs - Internacionaliza��o do datePicker
	 * Motivo/Coment�rio: Formato o campo Data de Nascimento para o padr�o brasileiro (dd/mm/yyyy)
	 * */
		$('#dataNascimento').datepicker({
			dateFormat: 'dd/mm/yy',
			language: 'pt-BR',
			dayNamesMin: [i18n_message("citcorpore.texto.abreviado.diaSemana.domingo"),
			              i18n_message("citcorpore.texto.abreviado.diaSemana.segundaFeira"),
			              i18n_message("citcorpore.texto.abreviado.diaSemana.tercaFeira"),
			              i18n_message("citcorpore.texto.abreviado.diaSemana.quartaFeira"),
			              i18n_message("citcorpore.texto.abreviado.diaSemana.quintaFeira"),
			              i18n_message("citcorpore.texto.abreviado.diaSemana.sextaFeira"),
			              i18n_message("citcorpore.texto.abreviado.diaSemana.sabado")],
			monthNames: [ i18n_message("citcorpore.texto.mes.janeiro"),
			              i18n_message("citcorpore.texto.mes.fevereiro"),
			              i18n_message("citcorpore.texto.mes.marco"),
			              i18n_message("citcorpore.texto.mes.abril"),
			              i18n_message("citcorpore.texto.mes.maio"),
			              i18n_message("citcorpore.texto.mes.junho"),
			              i18n_message("citcorpore.texto.mes.julho"),
			              i18n_message("citcorpore.texto.mes.agosto"),
			              i18n_message("citcorpore.texto.mes.setembro"),
			              i18n_message("citcorpore.texto.mes.outubro"),
			              i18n_message("citcorpore.texto.mes.novembro"),
			              i18n_message("citcorpore.texto.mes.dezembro") ]
		});
  	}
</script>
