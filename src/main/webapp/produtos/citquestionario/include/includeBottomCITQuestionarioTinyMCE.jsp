<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.centralit.citcorpore.util.CitCorporeConstantes"%>

<form name='formArquivosMultimidia' method="post" action='${ctx}/pages/uploadArquivoMultimidia/uploadArquivoMultimidia'>
	<input type='hidden' name='idDIV'/>
</form>

<cit:janelaPopup style="display:none;top:1350px;width:600px;left:50px;height:200px;position:absolute;" title="Upload Anexo" id="POPUP_UPLOAD_MULTIMIDIA">
	<iframe name='frameUploadAnexo' id='frameUploadAnexo' src='about:blank' height="0" width="0"/></iframe>
	<form name='formAnexo' method="post" ENCTYPE="multipart/form-data" action='${ctx}/pages/uploadArquivoMultimidia/uploadArquivoMultimidia'>
		<input type='hidden' name='idPessoa'/>
		<table>
			<tr>
				<td>
					Arquivo Mult�midia (Docs, Planilhas, Videos, PDFs, etc.):
				</td>
				<td>
					<input type='file' name='arquivo' size="60"/>
				</td>
			</tr>
			<tr>
				<td>
					Descri��o/Observa��o do Arquivo:
				</td>
				<td>
					<textarea rows="5" cols="60" name="observacao"></textarea>
				</td>
			</tr>			
			<tr>
				<td>
					<input type='button' name='btnEnviarImagem' value='Enviar' onclick='submitFormAnexo()'/>
				</td>
			</tr>
		</table>
	</form>
</cit:janelaPopup>

	<script type="text/javascript">
		<%
		Collection lstFields = (Collection) request.getAttribute("LST_CAMPOS_EDITOR");
		if (lstFields == null)
			lstFields = new ArrayList();
		
		for(Iterator it = lstFields.iterator(); it.hasNext();){
			String fieldName = (String)it.next();
		%>
			tinyMCE.execCommand('mceAddControl', false, '<%=fieldName%>');
		<%
		}
		%>
	</script>
