<!--
**********************************LICENCA*GPLv2*********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informacao Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="br.com.citframework.util.Constantes"%>

<%
	response.setCharacterEncoding("ISO-8859-1");

	String subForm = request.getParameter("subForm");
	if (subForm == null || subForm.equalsIgnoreCase("")){
		subForm = "N";
	}

	String idPessoaVisualizacaoHistCampos = (String)request.getAttribute("idPessoaVisualizacaoHistCampos");
	if (idPessoaVisualizacaoHistCampos == null){
		idPessoaVisualizacaoHistCampos = "";
	}
%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Iterator"%>
<%@page import="br.com.centralit.citquestionario.bean.LinhaSpoolQuestionario"%>
<%@page import="java.util.ArrayList"%>
		<table name="tblQuestoesSubQuest" border="0" id="tblQuestoesSubQuest" width="100%">
			<%
			Collection colLinhas = (Collection)request.getAttribute("linhasQuestionario");
			if (colLinhas != null){
				for(Iterator it = colLinhas.iterator(); it.hasNext();){
					LinhaSpoolQuestionario linhaQuestionario = (LinhaSpoolQuestionario)it.next();
					if (linhaQuestionario.isGenerateTR()){
						//out.println("<tr><td>");
					}

					out.println(linhaQuestionario.getLinha());

					if (linhaQuestionario.isGenerateTR()){
						//out.println("</td></tr>");
					}
				}
			}
			%>
		</table>
