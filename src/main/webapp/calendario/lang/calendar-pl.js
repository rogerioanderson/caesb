/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// Polish| dariush pietrzak, eyck@ghost.anime.pl
// ** I18N
Calendar._DN = new Array
("Niedziela",
 "Poniedzia�ek",
 "Wtorek",
 "�roda",
 "Czwartek",
 "Pi�tek",
 "Sobota",
 "Niedziela");
Calendar._MN = new Array
("Stycze�",
 "Luty",
 "Marzec",
 "Kwiecie�",
 "Maj",
 "Czerwiec",
 "Lipiec",
 "Sierpie�",
 "Wrzesie�",
 "Pa�dziernik",
 "Listopad",
 "Grudzie�");

// tooltips
Calendar._TT = {};
Calendar._TT["TOGGLE"] = "Zmie� pierwszy dzie� tygodnia";
Calendar._TT["PREV_YEAR"] = "Poprzedni rok (przytrzymaj dla menu)";
Calendar._TT["PREV_MONTH"] = "Poprzedni miesi�c (przytrzymaj dla menu)";
Calendar._TT["GO_TODAY"] = "Id� do dzisiaj";
Calendar._TT["NEXT_MONTH"] = "Nast�pny miesi�c (przytrzymaj dla menu)";
Calendar._TT["NEXT_YEAR"] = "Nast�pny rok (przytrzymaj dla menu)";
Calendar._TT["SEL_DATE"] = "Wybierz dat�";
Calendar._TT["DRAG_TO_MOVE"] = "Przeci�gnij by przesun��";
Calendar._TT["PART_TODAY"] = " (dzisiaj)";
Calendar._TT["MON_FIRST"] = "Wy�wietl poniedzia�ek jako pierwszy";
Calendar._TT["SUN_FIRST"] = "Wy�wietl niedziel� jako pierwsz�";
Calendar._TT["CLOSE"] = "Zamknij";
Calendar._TT["TODAY"] = "Dzisiaj";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "y-mm-dd";
Calendar._TT["TT_DATE_FORMAT"] = "D, M d";

Calendar._TT["WK"] = "wk";
