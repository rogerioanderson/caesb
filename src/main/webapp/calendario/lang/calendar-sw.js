/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// ** I18N
Calendar._DN = new Array
("S�ndag",
 "M�ndag",
 "Tisdag",
 "Onsdag",
 "Torsdag",
 "Fredag",
 "L�rdag",
 "S�ndag");
Calendar._MN = new Array
("Januari",
 "Februari",
 "Mars",
 "April",
 "Maj",
 "Juni",
 "Juli",
 "Augusti",
 "September",
 "Oktober",
 "November",
 "December");

// tooltips
Calendar._TT = {};
Calendar._TT["TOGGLE"] = "Skifta f�rsta veckodag";
Calendar._TT["PREV_YEAR"] = "F�rra �ret (tryck f�r meny)";
Calendar._TT["PREV_MONTH"] = "F�rra m�naden (tryck f�r meny)";
Calendar._TT["GO_TODAY"] = "G� till dagens datum";
Calendar._TT["NEXT_MONTH"] = "N�sta m�nad (tryck f�r meny)";
Calendar._TT["NEXT_YEAR"] = "N�sta �r (tryck f�r meny)";
Calendar._TT["SEL_DATE"] = "V�lj dag";
Calendar._TT["DRAG_TO_MOVE"] = "Flytta f�nstret";
Calendar._TT["PART_TODAY"] = " (idag)";
Calendar._TT["MON_FIRST"] = "Visa M�ndag f�rst";
Calendar._TT["SUN_FIRST"] = "Visa S�ndag f�rst";
Calendar._TT["CLOSE"] = "St�ng f�nstret";
Calendar._TT["TODAY"] = "Idag";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "y-mm-dd";
Calendar._TT["TT_DATE_FORMAT"] = "DD, d MM y";

Calendar._TT["WK"] = "wk";
