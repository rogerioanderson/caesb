/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
// ** Translated by ATang ** I18N
Calendar._DN = new Array
("������",
 "����һ",
 "���ڶ�",
 "������",
 "������",
 "������",
 "������",
 "������");
Calendar._MN = new Array
("һ��",
 "����",
 "����",
 "����",
 "����",
 "����",
 "����",
 "����",
 "����",
 "ʮ��",
 "ʮһ��",
 "ʮ����");

// tooltips
Calendar._TT = {};
Calendar._TT["TOGGLE"] = "�л��ܿ�ʼ��һ��";
Calendar._TT["PREV_YEAR"] = "��һ�� (��ס���˵�)";
Calendar._TT["PREV_MONTH"] = "��һ�� (��ס���˵�)";
Calendar._TT["GO_TODAY"] = "������";
Calendar._TT["NEXT_MONTH"] = "��һ�� (��ס���˵�)";
Calendar._TT["NEXT_YEAR"] = "��һ�� (��ס���˵�)";
Calendar._TT["SEL_DATE"] = "ѡ������";
Calendar._TT["DRAG_TO_MOVE"] = "�϶�";
Calendar._TT["PART_TODAY"] = " (����)";
Calendar._TT["MON_FIRST"] = "������ʾ����һ";
Calendar._TT["SUN_FIRST"] = "������ʾ������";
Calendar._TT["CLOSE"] = "�ر�";
Calendar._TT["TODAY"] = "����";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "y-mm-dd";
Calendar._TT["TT_DATE_FORMAT"] = "D, M d";

Calendar._TT["WK"] = "��";
