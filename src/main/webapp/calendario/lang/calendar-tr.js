/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
//////////////////////////////////////////////////////////////////////////////////////////////
//	Turkish Translation by Nuri AKMAN
//	Location: Ankara/TURKEY
//	e-mail	: nuriakman@hotmail.com
//	Date	: April, 9 2003
//
//	Note: if Turkish Characters does not shown on you screen
//		  please include falowing line your html code:
//
//		  <meta http-equiv="Content-Type" content="text/html; charset=windows-1254">
//
//////////////////////////////////////////////////////////////////////////////////////////////

// ** I18N
Calendar._DN = new Array
("Pazar",
 "Pazartesi",
 "Sal�",
 "�ar�amba",
 "Per�embe",
 "Cuma",
 "Cumartesi",
 "Pazar");
Calendar._MN = new Array
("Ocak",
 "�ubat",
 "Mart",
 "Nisan",
 "May�s",
 "Haziran",
 "Temmuz",
 "A�ustos",
 "Eyl�l",
 "Ekim",
 "Kas�m",
 "Aral�k");

// tooltips
Calendar._TT = {};
Calendar._TT["TOGGLE"] = "Haftan�n ilk g�n�n� kayd�r";
Calendar._TT["PREV_YEAR"] = "�nceki Y�l (Men� i�in bas�l� tutunuz)";
Calendar._TT["PREV_MONTH"] = "�nceki Ay (Men� i�in bas�l� tutunuz)";
Calendar._TT["GO_TODAY"] = "Bug�n'e git";
Calendar._TT["NEXT_MONTH"] = "Sonraki Ay (Men� i�in bas�l� tutunuz)";
Calendar._TT["NEXT_YEAR"] = "Sonraki Y�l (Men� i�in bas�l� tutunuz)";
Calendar._TT["SEL_DATE"] = "Tarih se�iniz";
Calendar._TT["DRAG_TO_MOVE"] = "Ta��mak i�in s�r�kleyiniz";
Calendar._TT["PART_TODAY"] = " (bug�n)";
Calendar._TT["MON_FIRST"] = "Takvim Pazartesi g�n�nden ba�las�n";
Calendar._TT["SUN_FIRST"] = "Takvim Pazar g�n�nden ba�las�n";
Calendar._TT["CLOSE"] = "Kapat";
Calendar._TT["TODAY"] = "Bug�n";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "dd-mm-y";
Calendar._TT["TT_DATE_FORMAT"] = "d MM y, DD";

Calendar._TT["WK"] = "Hafta";
