/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
��/ *   C r o a t i a n   l a n g u a g e   f i l e   f o r   t h e   D H T M L   C a l e n d a r   v e r s i o n   0 . 9 . 2   
 
 *   A u t h o r   K r u n o s l a v   Z u b r i n i c   < k r u n o s l a v . z u b r i n i c @ v i p . h r > ,   J u n e   2 0 0 3 . 
 
 *   F e e l   f r e e   t o   u s e   t h i s   s c r i p t   u n d e r   t h e   t e r m s   o f   t h e   G N U   L e s s e r   G e n e r a l 
 
 *   P u b l i c   L i c e n s e ,   a s   l o n g   a s   y o u   d o   n o t   r e m o v e   o r   a l t e r   t h i s   n o t i c e . 
 
 * / 
 
 C a l e n d a r . _ D N   =   n e w   A r r a y 
 
 ( " N e d j e l j a " , 
 
   " P o n e d j e l j a k " , 
 
   " U t o r a k " , 
 
   " S r i j e d a " , 
 
   " e t v r t a k " , 
 
   " P e t a k " , 
 
   " S u b o t a " , 
 
   " N e d j e l j a " ) ; 
 
 C a l e n d a r . _ M N   =   n e w   A r r a y 
 
 ( " S i j e 
a n j " , 
 
   " V e l j a 
a " , 
 
   " O ~u j a k " , 
 
   " T r a v a n j " , 
 
   " S v i b a n j " , 
 
   " L i p a n j " , 
 
   " S r p a n j " , 
 
   " K o l o v o z " , 
 
   " R u j a n " , 
 
   " L i s t o p a d " , 
 
   " S t u d e n i " , 
 
   " P r o s i n a c " ) ; 
 
 
 
 / /   t o o l t i p s 
 
 C a l e n d a r . _ T T   =   { } ; 
 
 C a l e n d a r . _ T T [ " T O G G L E " ]   =   " P r o m j e n i   d a n   s   k o j i m   p o 
i n j e   t j e d a n " ; 
 
 C a l e n d a r . _ T T [ " P R E V _ Y E A R " ]   =   " P r e t h o d n a   g o d i n a   ( d u g i   p r i t i s a k   z a   m e n i ) " ; 
 
 C a l e n d a r . _ T T [ " P R E V _ M O N T H " ]   =   " P r e t h o d n i   m j e s e c   ( d u g i   p r i t i s a k   z a   m e n i ) " ; 
 
 C a l e n d a r . _ T T [ " G O _ T O D A Y " ]   =   " I d i   n a   t e k u i   d a n " ; 
 
 C a l e n d a r . _ T T [ " N E X T _ M O N T H " ]   =   " S l i j e d e i   m j e s e c   ( d u g i   p r i t i s a k   z a   m e n i ) " ; 
 
 C a l e n d a r . _ T T [ " N E X T _ Y E A R " ]   =   " S l i j e d e a   g o d i n a   ( d u g i   p r i t i s a k   z a   m e n i ) " ; 
 
 C a l e n d a r . _ T T [ " S E L _ D A T E " ]   =   " I z a b e r i t e   d a t u m " ; 
 
 C a l e n d a r . _ T T [ " D R A G _ T O _ M O V E " ]   =   " P r i t i s n i   i   p o v u c i   z a   p r o m j e n u   p o z i c i j e " ; 
 
 C a l e n d a r . _ T T [ " P A R T _ T O D A Y " ]   =   "   ( t o d a y ) " ; 
 
 C a l e n d a r . _ T T [ " M O N _ F I R S T " ]   =   " P r i k a ~i   p o n e d j e l j a k   k a o   p r v i   d a n " ; 
 
 C a l e n d a r . _ T T [ " S U N _ F I R S T " ]   =   " P r i k a ~i   n e d j e l j u   k a o   p r v i   d a n " ; 
 
 C a l e n d a r . _ T T [ " C L O S E " ]   =   " Z a t v o r i " ; 
 
 C a l e n d a r . _ T T [ " T O D A Y " ]   =   " D a n a s " ; 
 
 
 
 / /   d a t e   f o r m a t s 
 
 C a l e n d a r . _ T T [ " D E F _ D A T E _ F O R M A T " ]   =   " d d - m m - y " ; 
 
 C a l e n d a r . _ T T [ " T T _ D A T E _ F O R M A T " ]   =   " D D ,   d d . m m . y " ; 
 
 
 
 C a l e n d a r . _ T T [ " W K " ]   =   " T j e " ; 
