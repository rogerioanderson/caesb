--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- PostgreSQL

-- In�cio Murilo Gabriel 27/06/13

insert into modelosemails (idmodeloemail, titulo, texto, situacao, identificador) values (54, 'Pesquisa de Satisfa��o - Solicita��o ${IDSOLICITACAOSERVICO}', 'Uma nota &quot;${NOTA}&quot; foi dada pelo usu&aacute;rio ${USUARIO} na pesquisa de satisfa&ccedil;&atilde;o da solicita&ccedil;&atilde;o&nbsp;${IDSOLICITACAOSERVICO}.<br />Segue o coment&aacute;rio/sujest&atilde;o de melhoria informados pelo usu&aacute;rio:<br /><br />${COMENTARIO}<br /><br />Atenciosamente,<br /><br />Central IT Tecnologia da Informa&ccedil;&atilde;o Ltda.<br type="_moz" />', 'A', 'pesqSatisfNegativo');

-- Fim Murilo Gabriel

-- In�cio Fl�vio 28/06/13

ALTER TABLE acordoservicocontrato ADD COLUMN habilitado character varying;

UPDATE acordoservicocontrato SET habilitado = 'S';

-- Fim Fl�vio

-- In�cio Cledson 04/07/13

CREATE TABLE historicosolicitacaoservico (
	idhistoricosolicitacao integer NOT NULL,
	idsolicitacaoservico integer NOT NULL,
	idresponsavelatual integer,
	idgrupo integer,
	idocorrencia integer NOT NULL,
	idservicocontrato integer NOT NULL,
	idcalendario integer NOT NULL,
	datacriacao timestamp,
	datafinal timestamp,
	horastrabalhadas float,
	status varchar
);

-- Fim Cledson

-- In�cio Fl�vio J�nior 19/07/13

ALTER TABLE moedas ADD COLUMN datainicio date;

ALTER TABLE moedas ADD COLUMN datafim date;

ALTER TABLE moedas ADD COLUMN usarcotacao character varying(1);

-- Fim Fl�vio J�nior

-- In�cio Rodrigo Engelberg 22/07/13

update menu set datafim = now() where descricao like 'Download Agente';

-- Fim Rodrigo Engelberg
