--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- INICIO [douglas.japiassu] Inser��o do modelo de email para Solicita��o Conclu�da, em atendimento � iniciativa 652. 

INSERT INTO modelosemails (
	idmodeloemail, 
	titulo, 
	texto, 
	situacao, 
	identificador
) VALUES (
	(select max(idmodeloemail) + 1 from modelosemails where	rownum = 1),
	'CITSMART - Solicita��o n�mero ${IDSOLICITACAOSERVICO} conclu�da.', 
	'<div><table style="width:692px">     <tbody>         <tr>             <td colspan="2"><a href="https://www.centralit.com.br"> 						<img src="http://i.imgur.com/ni5d7Wb.jpg" alt="Central IT, Excel&ecirc;ncia no atendimento &eacute; a nossa meta. Ajude-nos a conquist&aacute;-la" width="100%" /> 					</a></td>         </tr>         <tr>             <td width="335px">Ol&aacute; ${NOMECONTATO}, <br />             <br />             Nossa equipe de suporte concluiu a sua solicita&ccedil;&atilde;o de<br />             n&uacute;mero: <strong>${IDSOLICITACAOSERVICO}</strong>.<br />             <br />             <strong>Resposta do grupo solucionador:</strong> <br />             <em>${RESPOSTA}</em><br />             <br />             <strong>No intuito de buscar a excel&ecirc;ncia, a Central IT deseja<br />             saber a sua opini&atilde;o sobre os servi&ccedil;os prestados neste<br />             atendimento. Desta forma, contamos com sua<br />             avalia&ccedil;&atilde;o. <br />             <br />             Atenciosamente, <br />             Equipe de suporte Central IT. </strong><br />             <strong>${LINKPESQUISASATISFACAO}</strong></td>             <td width="357px"><a href="https://www.centralit.com.br"> 					<img src="http://i.imgur.com/zw5G1H7.jpg" alt="Responda nossa Pesquisa de Satisfa&ccedil;&atilde;o" width="100%" /> 				</a></td>         </tr>     </tbody> </table></div>',
	'A', 
	'fnd'
);

-- FIM [douglas.japiassu]

-- INICIO [euler.ramos] Modelo de email para comunicar ao grupo executor o encerramento da mudan�a
insert into modelosemails (idmodeloemail, titulo, texto,situacao,identificador) values (
(select max(idmodeloemail)+1 from modelosemails),
'Mudan�a Conclu�da',
'Informamos ao grupo executor que&nbsp; a requisi&ccedil;&atilde;o de mudan&ccedil;a de <strong>n&uacute;mero ${IDREQUISICAOMUDANCA}&nbsp;</strong> foi conclu&iacute;da conforme os dados abaixo:<br /><br />Tipo: <strong>${TIPO}</strong><br />Titulo: <strong>${TITULO}</strong><br /><br />Descri&ccedil;&atilde;o: <br /><strong>${DESCRICAO}</strong><br /><br />Atenciosamente,<br /><br />Central IT Tecnologia da Informa&ccedil;&atilde;o Ltda.<br /><br /><strong>&quot;Essa conta de e-mail &eacute; usada apenas para notifica&ccedil;&atilde;o, favor n&atilde;o responder. D&uacute;vidas, entrar em contato com o canal de atendimento.&quot;</strong><br />',
'A',
'MudancaConcluida');

-- FIM [euler.ramos]

-- INICIO [cristian.guedes] Removi a constraint "not null" do campo [idServicoContrato] da tabela [infocatalogoservico] para resolver a solicitacao 174530 do Kleuber - O campo estava causando problema por ser obrigatorio
alter table infocatalogoservico modify (idServicoContrato null);
-- FIM [cristian.guedes]
