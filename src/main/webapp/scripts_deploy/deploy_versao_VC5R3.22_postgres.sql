--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- INICIO - FABIO TEODORO AMORIM - 23/07/2015

INSERT INTO parametrocorpore (
	idparametrocorpore, 
	nomeparametrocorpore, 
	valor, 
	idempresa, 
	datainicio, 
	datafim, 
	tipodado) 
VALUES (
	269, 
	'parametro.269', 
	(select parametro.valor from parametrocorpore parametro where parametro.idparametrocorpore = 27), 
	1, 
	current_date, 
	null, 
	'Texto');

-- FIM - FABIO TEODORO AMORIM - 23/07/2015
	
-- INICIO - Euler Ramos - 16/09/2015

alter table contratos add centroderesultado integer default 0;

-- FIM - Euler Ramos - 16/09/2015

-- INICIO - euler.ramos - 13/10/2015

CREATE OR REPLACE FUNCTION plpgsql_call_handler() RETURNS language_handler AS 
    '$libdir/plpgsql' LANGUAGE C;
 
CREATE OR REPLACE TRUSTED PROCEDURAL LANGUAGE 'plpgsql' 
  HANDLER plpgsql_call_handler;
 
CREATE OR REPLACE FUNCTION f_busca_itemtrabalho(bigint) RETURNS SETOF bigint AS 
$BODY$ 
DECLARE 
	registros bigint; 
BEGIN 
	FOR registros IN (select distinct it.iditemtrabalho 
					  from execucaosolicitacao ex join bpm_itemtrabalhofluxo it on ex.idsolicitacaoservico = $1 and ex.idinstanciafluxo = it.idinstancia and it.situacao in ('Disponivel', 'EmAndamento') 
					                              join bpm_elementofluxo e on e.idelemento = it.idelemento and e.tipoelemento = 'Tarefa' 
												  join bpm_atribuicaofluxo a on it.iditemtrabalho = a.iditemtrabalho and a.tipo in ('Automatica','Delegacao') 
					 ) 
	LOOP 
	    RETURN NEXT registros; 
	END LOOP; 
	IF NOT FOUND 
		THEN 
		    FOR registros IN (select it.iditemtrabalho 
							  from execucaosolicitacao ex join bpm_itemtrabalhofluxo it on ex.idsolicitacaoservico = $1 and ex.idinstanciafluxo = it.idinstancia 
														  join bpm_elementofluxo e on e.idelemento = it.idelemento and e.tipoelemento = 'Tarefa' 
														  join bpm_atribuicaofluxo a on it.iditemtrabalho = a.iditemtrabalho and a.tipo in ('Automatica','Delegacao') 
							  ORDER BY datahorafinalizacao desc limit 1 
							 ) 
		    LOOP 
			RETURN NEXT registros; 
		    END LOOP; 
	END IF; 
END; 
$BODY$ 
LANGUAGE 'plpgsql' VOLATILE;

CREATE TYPE t_execucao_solicitacao AS (idsolicitacaoservico bigint, iditemtrabalho bigint);

CREATE OR REPLACE FUNCTION f_execucao_solicitacao(char(1),timestamp,timestamp,bigint) RETURNS SETOF t_execucao_solicitacao AS 
$BODY$ 
DECLARE 
	registros t_execucao_solicitacao; 
	iditemtrabalho bigint; 
 
	campoData char(1); 
	dataInicio timestamp; 
	dataFim timestamp; 
	p_idsolicitacaoservico bigint; 
 
	sql text; 
BEGIN 
	sql =  'SELECT idsolicitacaoservico FROM solicitacaoservico'; 
 
	campoData = $1; 
	dataInicio = $2; 
	dataFim = $3; 
	p_idsolicitacaoservico = $4; 
 
	IF campoData IS NOT NULL 
	   THEN 
	       sql = sql ||  ' WHERE ('; 
	       IF campoData =  'A' 
			THEN 
		      sql = sql ||  'datahorasolicitacao'; 
			ELSE 
		      sql = sql ||  'datahorafim'; 
	       END IF; 
	       sql = sql ||  ' BETWEEN '''||dataInicio|| ''' AND '''||dataFim|| ''')'; 
		   IF p_idsolicitacaoservico IS NOT NULL 
			THEN 
			  sql = sql ||  ' AND idsolicitacaoservico = '||p_idsolicitacaoservico; 
		   END IF; 
	   ELSE 
		   IF p_idsolicitacaoservico IS NOT NULL 
			THEN 
			  sql = sql ||  ' WHERE idsolicitacaoservico = '||p_idsolicitacaoservico; 
		   END IF; 
	END IF; 
 
	FOR registros IN EXECUTE sql 
	LOOP 
		FOR iditemtrabalho IN (SELECT f_busca_itemtrabalho(registros.idsolicitacaoservico)) 
		LOOP 
		    registros.iditemtrabalho = iditemtrabalho; 
		END LOOP; 
		RETURN NEXT registros; 
	END LOOP; 
 
END; 
$BODY$ 
LANGUAGE  'plpgsql' VOLATILE;

-- FIM - euler.ramos - 13/10/2015
