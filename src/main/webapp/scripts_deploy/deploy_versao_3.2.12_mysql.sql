--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
set sql_safe_updates = 0;

-- INICIO - MURILO GABRIEL RODRIGUES - 18/03/2014

ALTER TABLE matrizcomunicacaotiporegistro ENGINE = INNODB;
ALTER TABLE matrizcomunicacaofrequencia ENGINE = INNODB;
ALTER TABLE matrizcomunicacaoformacontato ENGINE = INNODB;

CREATE TABLE matrizcomunicacao (
  idmatrizcomunicacao int(11) NOT NULL,
  idcontrato int(11) NOT NULL,
  grupoenvolvido int(11) NOT NULL,
  responsabilidades text NOT NULL,
  idtiporegistro int(11),
  idfrequencia int(11),
  idformacontato int(11),
  deleted char(1) default null,
  PRIMARY KEY (idmatrizcomunicacao),
  constraint foreign key (idcontrato) references contratos (idcontrato),
  constraint foreign key (idtiporegistro) references matrizcomunicacaotiporegistro (idtiporegistro),
  constraint foreign key (idfrequencia) references matrizcomunicacaofrequencia (idfrequencia),
  constraint foreign key (idformacontato) references matrizcomunicacaoformacontato (idformacontato)
) ENGINE=InnoDB;

-- FIM - MURILO GABRIEL RODRIGUES - 18/03/2014

-- INICIO - MARIO HAYASAKI JUNIOR - 21/03/2014

ALTER TABLE pedidoportal DROP FOREIGN KEY rel_pedidosolicitacao_usuario;
DROP index rel_pedidosolicitacao_usuario on pedidoportal;
ALTER TABLE pedidoportal change column idusuario idempregado int null;
ALTER TABLE pedidoportal ADD CONSTRAINT rel_pedidosolicitacao_empregado FOREIGN KEY ( idempregado ) REFERENCES empregados ( idempregado )  ON DELETE NO ACTION ON UPDATE NO ACTION;

-- FIM - MARIO HAYASAKI JUNIOR - 21/03/2014

-- INICIO - BRUNO CARVALHO DE AQUINO - 28/03/2014

ALTER TABLE dicionario ADD personalizado CHAR(1) DEFAULT 'N';

-- FIM - BRUNO CARVALHO DE AQUINO - 28/03/2014

-- INICIO - RODRIGO PECCI ACORSE - 09/11/2013

ALTER TABLE atividadeperiodica CHANGE COLUMN criadopor criadopor VARCHAR(256);
ALTER TABLE atividadeperiodica CHANGE COLUMN alteradopor alteradopor VARCHAR(256);

-- FIM - RODRIGO PECCI ACORSE - 09/11/2013
-- Inicio - M�RIO HAYASAKI J�NIOR - 14/07/2014
alter table empregados modify telefone varchar(100);
-- FIM

set sql_safe_updates = 1;
