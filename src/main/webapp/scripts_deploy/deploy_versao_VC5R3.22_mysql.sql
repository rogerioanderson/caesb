--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- INICIO - FABIO TEODORO AMORIM - 23/07/2015

INSERT INTO parametrocorpore (
	idparametrocorpore, 
	nomeparametrocorpore, 
	valor, 
	idempresa, 
	datainicio, 
	datafim, 
	tipodado) 
VALUES (
	269, 
	'parametro.269', 
	(select parametro.valor from parametrocorpore parametro where parametro.idparametrocorpore = 27), 
	1, 
	current_date, 
	null, 
	'Texto');

-- FIM - FABIO TEODORO AMORIM - 23/07/2015
	
-- INICIO - Euler Ramos - 16/09/2015
alter table contratos add centroderesultado int(11) default 0;
-- FIM - Euler Ramos - 16/09/2015


	
-- INICIO - euler.ramos - 16/10/2015

DROP PROCEDURE IF EXISTS pr_relaciona_solicitacao_itemtrabalho;

CREATE PROCEDURE pr_relaciona_solicitacao_itemtrabalho (p_campoData char(1),p_dataInicio timestamp,p_dataFim timestamp,p_idsolicitacaoservico bigint) 
body:BEGIN 
	DROP TEMPORARY TABLE IF EXISTS tab_solicitacao; 
    CREATE TEMPORARY TABLE tab_solicitacao( 
        idsolicitacaoservico bigint, 
        PRIMARY KEY  (idsolicitacaoservico) 
    ) ENGINE=Memory; 
	SET @sql_text = 'INSERT INTO tab_solicitacao (idsolicitacaoservico) SELECT idsolicitacaoservico FROM solicitacaoservico'; 
	IF p_campoData IS NOT NULL 
	   THEN 
	       SET @sql_text = concat(@sql_text,' WHERE ('); 
	       IF p_campoData =  'A' 
				THEN 
					SET @sql_text = concat(@sql_text,'datahorasolicitacao'); 
				ELSE 
					SET @sql_text = concat(@sql_text,'datahorafim'); 
	       END IF; 
	       SET @sql_text = concat(@sql_text,' BETWEEN ''',p_dataInicio,''' AND ''',p_dataFim,''')'); 
		   IF p_idsolicitacaoservico IS NOT NULL 
			THEN 
			  SET @sql_text = concat(@sql_text,' AND idsolicitacaoservico = ',p_idsolicitacaoservico); 
		   END IF; 
	   ELSE 
		   IF p_idsolicitacaoservico IS NOT NULL 
			THEN 
			  SET @sql_text = concat(@sql_text,' WHERE idsolicitacaoservico = ',p_idsolicitacaoservico); 
		   END IF; 
	END IF; 
	SET @sql_text = concat(@sql_text,' ORDER BY idsolicitacaoservico'); 
    PREPARE stm FROM @sql_text; 
    EXECUTE stm; 
    DEALLOCATE PREPARE stm; 
 	DROP TEMPORARY TABLE IF EXISTS tab_solicitacao_itemtrabalho; 
    CREATE TEMPORARY TABLE tab_solicitacao_itemtrabalho( 
        idsolicitacaoservico bigint, 
		iditemtrabalho bigint, 
        PRIMARY KEY  (idsolicitacaoservico,iditemtrabalho) 
    ) ENGINE=Memory; 
 	BLOCK1: begin 
		DECLARE v_idsolicitacao bigint; 
		DECLARE pararLoop BOOLEAN DEFAULT FALSE; 
		DECLARE cursorSolicitacoes CURSOR FOR  SELECT idsolicitacaoservico FROM tab_solicitacao; 
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET pararLoop = TRUE; 
		OPEN cursorSolicitacoes; 
 		the_loop1: LOOP 
			FETCH cursorSolicitacoes INTO v_idsolicitacao; 
			IF pararLoop 
				THEN 
					CLOSE cursorSolicitacoes; 
					LEAVE the_loop1; 
			END IF; 
 			BLOCK2: begin 
 				DECLARE v_inseriu BOOLEAN DEFAULT FALSE; 
				DECLARE v_iditem bigint; 
				DECLARE pararLoop2 BOOLEAN DEFAULT FALSE; 
				DECLARE cursorItens CURSOR FOR SELECT DISTINCT it.iditemtrabalho 
											   FROM execucaosolicitacao ex JOIN bpm_itemtrabalhofluxo it ON ex.idsolicitacaoservico = v_idsolicitacao AND ex.idinstanciafluxo = it.idinstancia AND it.situacao IN ('Disponivel', 'EmAndamento') 
																		   JOIN bpm_elementofluxo e ON e.idelemento = it.idelemento AND e.tipoelemento = 'Tarefa' 
																		   JOIN bpm_atribuicaofluxo a ON it.iditemtrabalho = a.iditemtrabalho AND a.tipo IN ('Automatica','Delegacao'); 
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET pararLoop2 = TRUE; 
				OPEN cursorItens; 
 				the_loop2: LOOP 
 					FETCH cursorItens INTO v_iditem; 
					IF pararLoop2 
						THEN 
							CLOSE cursorItens; 
							LEAVE the_loop2; 
					END IF; 
					INSERT INTO tab_solicitacao_itemtrabalho VALUES (v_idsolicitacao, v_iditem); 
					IF NOT v_inseriu 
						THEN 
							SET v_inseriu := TRUE; 
					END IF; 
 				END LOOP the_loop2; 
				IF NOT v_inseriu 
					THEN 
						BLOCK3: begin 
							DECLARE pararLoop3 BOOLEAN DEFAULT FALSE; 
							DECLARE cursorItens3 CURSOR FOR SELECT it.iditemtrabalho 
															FROM execucaosolicitacao ex JOIN bpm_itemtrabalhofluxo it ON ex.idsolicitacaoservico = v_idsolicitacao AND ex.idinstanciafluxo = it.idinstancia 
																					    JOIN bpm_elementofluxo e ON e.idelemento = it.idelemento AND e.tipoelemento = 'Tarefa' 
																						JOIN bpm_atribuicaofluxo a ON it.iditemtrabalho = a.iditemtrabalho AND a.tipo IN ('Automatica','Delegacao') 
															ORDER BY datahorafinalizacao DESC LIMIT 1; 
							DECLARE CONTINUE HANDLER FOR NOT FOUND SET pararLoop3 = TRUE; 
							OPEN cursorItens3; 
 							the_loop3: LOOP 
 								FETCH cursorItens3 INTO v_iditem; 
								IF pararLoop3 
									THEN 
										CLOSE cursorItens3; 
										LEAVE the_loop3; 
								END IF; 
								INSERT INTO tab_solicitacao_itemtrabalho VALUES (v_idsolicitacao, v_iditem); 
							END LOOP the_loop3; 
						end BLOCK3; 
				END IF; 
			end BLOCK2; 
		END LOOP the_loop1; 
	end BLOCK1; 
	select 1; 
END

-- FIM - euler.ramos - 16/10/2015
