--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- IN�CIO - MARIO HAYASAKI JUNIOR 30/12/2013

ALTER TABLE PLANOMELHORIA MODIFY (CRIADOPOR VARCHAR2(255 CHAR) );

ALTER TABLE PLANOMELHORIA MODIFY (MODIFICADOPOR VARCHAR2(255 CHAR) );

-- FIM - MARIO HAYASAKI JUNIOR

-- IN�CIO - GILBERTO TAVARES DE FRANCO NERY 14/01/2014

alter table rh_curriculo add naturalidade varchar2(45);

-- FIM - GILBERTO TAVARES DE FRANCO NERY

-- IN�CIO - THIAGO BORGES DA SILVA 16/01/2014

ALTER TABLE rh_requisicaopessoal ADD observacoes varchar(255);

-- FIM - THIAGO BORGES DA SILVA

-- IN�CIO - DAVID RODRIGUES 17/01/2014

ALTER TABLE rh_requisicaopessoal MODIFY observacoes varchar2(500);

-- FIM - DAVID RODRIGUES
