--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- INICIO - thyen.chang - 27.11.2014

ALTER TABLE COLETAPRECO MODIFY (PRAZOMEDIOPAGTO DECIMAL(10,2));
ALTER TABLE COLETAPRECO MODIFY (PRAZOMEDIOPAGTO NOT NULL);

-- FIM - thyen.chang - 27.11.2014

-- INICIO - thyen.chang - 02/12/2014

alter table itemconfiguracao add nome varchar2(400);
update itemconfiguracao set nome = identificacao;
alter table itemconfiguracao modify nome varchar2(400) not null;
ALTER TABLE ITEMCONFIGURACAO ADD INFORMACOESADICIONAIS VARCHAR2(4000);
ALTER TABLE ITEMCONFIGURACAO MODIFY INFORMACOESADICIONAIS DEFAULT '';
ALTER TABLE ITEMCONFIGURACAO MODIFY INFORMACOESADICIONAIS NOT NULL;

-- FIM - thyen.chang - 02/12/2014

-- INICIO - thyen.chang - 04/12/2014

alter table itemconfiguracao add idgruporesponsavel integer;

-- FIM - thyen.chang	- 04/12/2014

-- INICIO - euler.ramos - 03.12.2014

alter table ocorrenciasolicitacao add notificarsolicitante char(1);
alter table ocorrenciasolicitacao modify notificarsolicitante default 'N';
update ocorrenciasolicitacao set notificarsolicitante = 'N';
insert into modelosemails (idmodeloemail,titulo,texto,situacao,identificador) values ($id_modeloemail_90,'Ocorrência da Solicitação - ${IDSOLICITACAOSERVICO}','Senhor(a) <strong>${NOMECONTATO}</strong>,<br /><br />O t&eacute;cnico solucionador <strong>${REGISTRADOPOR}</strong> registrou a seguinte ocorr&ecirc;ncia:<br />Descri&ccedil;&atilde;o:<br /><strong>${DESCRICAO}</strong><br /><br />Ocorr&ecirc;ncia:<br /><strong>${OCORRENCIA}</strong><br /><br />N&uacute;mero da solicita&ccedil;&atilde;o: <strong>${IDSOLICITACAOSERVICO}</strong><br />Tipo: <strong>${DEMANDA}</strong><br />Servi&ccedil;o:<strong> ${SERVICO}</strong><br />Informa&ccedil;&otilde;es de Contato:<strong> ${INFORMACOESCONTATO}</strong><br />Categoria:<strong> ${CATEGORIA}</strong><br />Origem: <strong>${ORIGEM}</strong><br />Tempo gasto:<strong> ${TEMPOGASTO}</strong><br /><br />Atenciosamente,<br />Central de Servi&ccedil;os de TI','A','NOTIFOCORRENCIA');
update parametrocorpore set valor = '$id_modeloemail_90' where nomeparametrocorpore =  'parametro.251';

-- FIM - euler.ramos - 03.12.2014

-- INICIO - thyen.chang - 08/12/2014

UPDATE menu SET datafim = CURRENT_DATE WHERE nome LIKE '%$menu.nome.relatorioRetorno%';

-- FIM - thyen.chang - 08/12/2014

-- INICIO - ezequiel.nunes - 11.12.2014

alter table ocorrenciasolicitacao add notificarresponsavel char(1);
ALTER TABLE OCORRENCIASOLICITACAO MODIFY NOTIFICARRESPONSAVEL DEFAULT 'N';
insert into modelosemails (idmodeloemail,titulo,texto,situacao,identificador) values($id_modelo_email_notificar_responsavel,'Registro ocorrência pelo portal para solicitação - ${IDSOLICITACAOSERVICO}','&nbsp;Informamos ao grupo executor que foi registrada uma ocorr&ecirc;ncia para a solicita&ccedil;&atilde;o de n&uacute;mero ${IDSOLICITACAOSERVICO} conforme os dados abaixo:<div>&nbsp;</div><div>&nbsp;</div><div>Data/hora: ${dataHora}</div><div>Registrado por: ${registradoPor},</div><div>Categoria: ${categoria}</div><div>Origem: ${origem}</div><div>Ocorr&ecirc;ncia: ${ocorrencias}</div><div>Informa&ccedil;&otilde;es do Contato: ${informacoesContato}</div><div>&nbsp;</div><div>Descri&ccedil;&atilde;o:${descricao}</div><div>&nbsp;</div><div>Atenciosamente,</div><div>&nbsp;</div><div>Central IT Tecnologia da Informa&ccedil;&atilde;o Ltda.</div><div>&nbsp;</div><div>&quot;Essa conta de e-mail &eacute; usada apenas para notifica&ccedil;&atilde;o, favor n&atilde;o responder. D&uacute;vidas, entrar em contato com o canal de atendimento.&quot;</div>','A','regOcorrenciaPortal');
update parametrocorpore set valor = '$id_modelo_email_notificar_responsavel' where nomeparametrocorpore =  'parametro.253';

-- FIM - ezequiel.nunes - 11.12.2014

-- Inicio Script autor Ezequiel --

ALTER TABLE perfilacesso  ADD acessosistemacitsmart char(1) DEFAULT 'S';

-- FIM Script --

-- INICIO - thyen.chang - 18/12/2014

ALTER TABLE itemconfiguracao MODIFY nome VARCHAR2(400) NULL;
ALTER TABLE ITEMCONFIGURACAO MODIFY INFORMACOESADICIONAIS VARCHAR2(4000) NULL;

-- FIM -- thyen.chang - 18/12/2014
