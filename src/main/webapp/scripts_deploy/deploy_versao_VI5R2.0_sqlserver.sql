--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- INICIO - THIAGO BORGES DA SILVA - 30/03/2015

ALTER TABLE requisicaoviagem DROP CONSTRAINT fk_requisicaoviagem_reference_cidadedestino;
ALTER TABLE requisicaoviagem DROP CONSTRAINT fk_requisicaoviagem_reference_cidadeorigem;

ALTER TABLE requisicaoviagem ALTER COLUMN idcidadeorigem INT NULL;
ALTER TABLE requisicaoviagem ALTER COLUMN idcidadedestino INT NULL;
ALTER TABLE requisicaoviagem ALTER COLUMN datainicio DATE NULL;
ALTER TABLE requisicaoviagem ALTER COLUMN datafim DATE NULL;
ALTER TABLE requisicaoviagem ALTER COLUMN qtdedias INT NULL;

ALTER TABLE requisicaoviagem ADD finalidade VARCHAR(45) NOT NULL;

ALTER TABLE roteiroviagem ADD hoteispreferenciais VARCHAR(150) NULL;
ALTER TABLE roteiroviagem ADD horainicio VARCHAR(5) NULL;
ALTER TABLE roteiroviagem ADD horafim VARCHAR(5) NULL;
ALTER TABLE roteiroviagem ADD aeroportoorigem VARCHAR(45) NULL;
ALTER TABLE roteiroviagem ADD aeroportodestino VARCHAR(100) NULL;

CREATE TABLE dadosbancariosintegrante(
iddadosbancarios INT NOT NULL,
idintegrante INT NULL,
banco VARCHAR(3) NULL,
agencia VARCHAR(5) NULL,
conta VARCHAR(20) NULL,
operacao VARCHAR(20) NULL,
cpf VARCHAR(14) NULL,
PRIMARY KEY (iddadosbancarios));

ALTER TABLE rh_funcionario ADD banco VARCHAR(3) NULL;
ALTER TABLE rh_funcionario ADD agencia VARCHAR(5) NULL;
ALTER TABLE rh_funcionario ADD conta VARCHAR(20) NULL;

-- FIM - THIAGO BORGES DA SILVA - 30/03/2015

-- INICIO - RENATO ARAUJO JESUS - 24/04/2015

delete from menu where nome = '$menu.prestacoesPendentes';

-- FIM - RENATO ARAUJO JESUS - 24/04/2015
