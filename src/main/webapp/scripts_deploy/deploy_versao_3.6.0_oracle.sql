--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- INICIO - Bruno.Aquino 29/05/2014
	INSERT INTO modelosemails (idmodeloemail,	titulo,	texto,	situacao,	identificador)
		VALUES(86,'Altera��o da Solicita��o de Servi�o - ${IDSOLICITACAOSERVICO}',
			'&nbsp;Informamos que a Solicita&ccedil;&atilde;o de Servi&ccedil;o de N&uacute;mero ${IDSOLICITACAOSERVICO} sofreu altera&ccedil;&atilde;o.<div>&nbsp;</div><div>Atenciosamente,</div><div>&nbsp;</div><div>&nbsp;</div><div>Central IT Tecnologia da Informa&ccedil;&atilde;o Ltda.</div><div>&nbsp;</div>',
			'A','alterSolServico');
	
	INSERT INTO modelosemails (	idmodeloemail,	titulo,	texto,	situacao,	identificador)
		VALUES	(87,'Altera��o da Descri��o da Solicita��o de Servi�o - ${IDSOLICITACAOSERVICO}',
			'&nbsp;Informamos que a Solicita&ccedil;&atilde;o de Servi&ccedil;o de N&uacute;mero ${IDSOLICITACAOSERVICO} sofreu altera&ccedil;&atilde;o.<div>&nbsp;</div><div><strong>Descri&ccedil;&atilde;o</strong>:&nbsp;</div><div>${DESCRICAO}</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>Atenciosamente,</div><div>&nbsp;</div><div>&nbsp;</div><div>Central IT Tecnologia da Informa&ccedil;&atilde;o Ltda.</div><div>&nbsp;</div>',
			'A','alterSolServDesc');
      
-- FIM - Bruno.Aquino 29/05/2014

-- Inicio - maycon.fernandes 02/06/2014
create table problemarelacionado(
 idproblemarelacionado number(19,0) not null,
 idproblema number(19,0) not null,
  primary key (idproblema, idproblemarelacionado)
);
-- Fim - maycon.fernandes 02/06/2014

-- INICIO - rodrigo.acorse - 02/06/2014
CREATE TABLE EMAILSOLICITACAO (
	IDEMAIL NUMBER(11) NOT NULL,
	IDMESSAGE VARCHAR2(500) NOT NULL,
	IDSOLICITACAO NUMBER(11) NOT NULL,
	ORIGEM VARCHAR2(255) NOT NULL,
	PRIMARY KEY (IDEMAIL)
);

CREATE INDEX INDEX_IDMESSAGE ON EMAILSOLICITACAO (IDMESSAGE ASC);
CREATE INDEX INDEX_IDSOLICITACAO ON EMAILSOLICITACAO (IDSOLICITACAO ASC);
CREATE UNIQUE INDEX INDEX_IDEMAIL ON EMAILSOLICITACAO (IDEMAIL ASC);
-- FIM - rodrigo.acorse - 02/06/2014
