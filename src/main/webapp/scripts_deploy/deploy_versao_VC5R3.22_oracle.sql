--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- INICIO - FABIO TEODORO AMORIM - 23/07/2015

INSERT INTO parametrocorpore (
	idparametrocorpore, 
	nomeparametrocorpore, 
	valor, 
	idempresa, 
	datainicio, 
	datafim, 
	tipodado) 
VALUES (
	269, 
	'parametro.269', 
	(select parametro.valor from parametrocorpore parametro where parametro.idparametrocorpore = 27), 
	1, 
	current_date, 
	null, 
	'Texto');

-- FIM - FABIO TEODORO AMORIM - 23/07/2015

	
-- INICIO - Euler Ramos - 16/09/2015

alter table contratos add centroderesultado NUMBER(10,0) default 0;

-- FIM - Euler Ramos - 16/09/2015

	
-- INICIO - euler.ramos - 20/10/2015

CREATE OR REPLACE TYPE o_itemtrabalho AS OBJECT (iditemtrabalho NUMBER);;

CREATE OR REPLACE TYPE t_itemtrabalho AS TABLE OF o_itemtrabalho;;

CREATE OR REPLACE FUNCTION f_busca_itemtrabalho(p_idsolicitacao NUMBER) RETURN t_itemtrabalho PIPELINED IS record_obj o_itemtrabalho; 
encontrou BOOLEAN; 
BEGIN 
  record_obj := o_itemtrabalho(NULL); 
  encontrou := false; 
  FOR consulta IN (SELECT DISTINCT it.iditemtrabalho 
				   FROM execucaosolicitacao ex JOIN bpm_itemtrabalhofluxo it ON ex.idsolicitacaoservico = p_idsolicitacao AND ex.idinstanciafluxo = it.idinstancia AND it.situacao IN ('Disponivel', 'EmAndamento') 
					                           JOIN bpm_elementofluxo e ON e.idelemento = it.idelemento AND e.tipoelemento = 'Tarefa' 
											   JOIN bpm_atribuicaofluxo a ON it.iditemtrabalho = a.iditemtrabalho AND a.tipo IN ('Automatica','Delegacao') 
				  ) 
  LOOP 
      record_obj.iditemtrabalho := consulta.iditemtrabalho; 
      encontrou := true; 
      PIPE ROW (record_obj); 
  END LOOP; 
  IF NOT encontrou 
	THEN 
      FOR novaConsulta IN (SELECT iditemtrabalho 
						   FROM (SELECT it.iditemtrabalho 
							     FROM execucaosolicitacao ex JOIN bpm_itemtrabalhofluxo it ON ex.idsolicitacaoservico = p_idsolicitacao AND ex.idinstanciafluxo = it.idinstancia 
														     JOIN bpm_elementofluxo e ON e.idelemento = it.idelemento AND e.tipoelemento = 'Tarefa' 
															 JOIN bpm_atribuicaofluxo a ON it.iditemtrabalho = a.iditemtrabalho AND a.tipo IN ('Automatica','Delegacao') 
								 ORDER BY datahorafinalizacao DESC) 
						   WHERE ROWNUM =1) 
	  LOOP 
        record_obj.iditemtrabalho := novaConsulta.iditemtrabalho; 
        PIPE ROW (record_obj); 
	  END LOOP; 
  END IF;  
  RETURN;  
END;;

CREATE OR REPLACE TYPE o_execucao_solicitacao AS OBJECT (idsolicitacaoservico NUMBER, iditemtrabalho NUMBER);;

CREATE OR REPLACE TYPE t_execucao_solicitacao AS TABLE OF o_execucao_solicitacao;;

CREATE OR REPLACE FUNCTION f_execucao_solicitacao(campoData CHAR, dataInicio DATE, dataFim DATE, p_idsolicitacaoservico NUMBER) RETURN t_execucao_solicitacao PIPELINED IS record_obj o_execucao_solicitacao; 
  TYPE ref_cursor_solicitacao IS REF CURSOR; 
  cursor_solicitacao ref_cursor_solicitacao; 
  TYPE t_registro is record (idsolicitacaoservico NUMBER); 
  registro_solicitacao t_registro; 
  sqlTexto VARCHAR2(300); 
BEGIN 
	record_obj := o_execucao_solicitacao(NULL,NULL); 
	sqlTexto :=  'SELECT idsolicitacaoservico FROM solicitacaoservico'; 
	IF campoData IS NULL 
	   THEN 
		   IF p_idsolicitacaoservico IS NULL 
			THEN 
			  OPEN cursor_solicitacao FOR sqlTexto; 
			ELSE 
			  sqlTexto := sqlTexto ||  ' WHERE idsolicitacaoservico = :p_idsolicitacaoservico'; 
			  OPEN cursor_solicitacao FOR sqlTexto USING p_idsolicitacaoservico; 
		   END IF; 
       ELSE 
	       sqlTexto := sqlTexto ||  ' WHERE ('; 
	       IF campoData =  'A' 
            THEN 
                sqlTexto := sqlTexto ||  'datahorasolicitacao'; 
            ELSE 
                sqlTexto := sqlTexto ||  'datahorafim'; 
	       END IF; 
	       sqlTexto := sqlTexto ||  ' BETWEEN :dataInicio AND :dataFim )'; 
		   IF p_idsolicitacaoservico IS NULL 
			THEN 
			  OPEN cursor_solicitacao FOR sqlTexto USING dataInicio, dataFim; 
			ELSE 
			  sqlTexto := sqlTexto ||  ' AND idsolicitacaoservico = :p_idsolicitacaoservico'; 
			  OPEN cursor_solicitacao FOR sqlTexto USING dataInicio, dataFim, p_idsolicitacaoservico; 
		   END IF; 
	END IF; 
	LOOP 
      FETCH cursor_solicitacao INTO registro_solicitacao; 
      EXIT WHEN cursor_solicitacao%NOTFOUND; 
      FOR iditemtrabalho IN (SELECT * FROM table(f_busca_itemtrabalho(registro_solicitacao.idsolicitacaoservico))) 
      LOOP 
          record_obj.idsolicitacaoservico := registro_solicitacao.idsolicitacaoservico; 
          record_obj.iditemtrabalho := iditemtrabalho.iditemtrabalho; 
          PIPE ROW (record_obj); 
      END LOOP; 
  END LOOP; 
  CLOSE cursor_solicitacao; 
  RETURN; 
END;;

-- FIM - euler.ramos - 20/10/2015
