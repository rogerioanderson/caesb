--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
alter table solicitacaoservico 
add idtarefaencerramento BIGINT null default null;
alter table solicitacaoservico 	add constraint fk_tarefa_encerramento foreign key (idtarefaencerramento) references bpm_itemtrabalhofluxo (iditemtrabalho);
create index fk_tarefa_encerramento_idx ON solicitacaoservico(idtarefaencerramento asc);

update solicitacaoservico set idtarefaencerramento = 
		(select max(ocorrenciasolicitacao.iditemtrabalho) from ocorrenciasolicitacao  inner join bpm_itemtrabalhofluxo  
		on bpm_itemtrabalhofluxo.iditemtrabalho = ocorrenciasolicitacao.iditemtrabalho
		where upper(bpm_itemtrabalhofluxo.situacao) = 'EXECUTADO' 
			and bpm_itemtrabalhofluxo.idresponsavelatual is not null 
			and upper(ocorrenciasolicitacao.categoria) = 'EXECUCAO' 
			and (upper(dadossolicitacao) like '%RESOLVIDA%' 
			or upper(dadossolicitacao) like '%CANCELADA%')
			and ocorrenciasolicitacao.idsolicitacaoservico = solicitacaoservico.idsolicitacaoservico)
where idtarefaencerramento is null;
