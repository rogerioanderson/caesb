--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- INICIO - GILBERTO NERY - 04/12/2015

-- Cadastrar email de remarcacao de viagem
SET @maxId = (select idmodeloemail + 1 from modelosemails order by idmodeloemail desc limit 1);
insert into  modelosemails (idmodeloemail, titulo, texto, situacao, identificador)
values (@maxid, 'Requisi�ao Viagem - Remarca��o - ${IDSOLICITACAOSERVICO}',
'Senhor(a),<div>&nbsp;</div><div>Informamos que a requisi&ccedil;&atilde;o de viagem N&uacute;mero: ${IDSOLICITACAOSERVICO} foi remarcada e precisa de nova autoriza&ccedil;&atilde;o.</div><div>&nbsp;</div><div>Servi&ccedil;o: ${SERVICO}</div><div>&nbsp;</div><div>${INFORMACOESCOMPLEMENTARESHTML}</div><div>&nbsp;</div><div>&nbsp;</div><div>Atenciosamente,</div><div>&nbsp;</div><div>Central IT Tecnologia da Informa&ccedil;&atilde;o Ltda.</div>',
'A', 'AutorizaRemarcViagem');

-- FIM - GILBERTO NERY - 04/12/2015
