--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- INICIO - GILBERTO NERY - 04/11/2015

BEGIN TRANSACTION
GO
ALTER TABLE dbo.midiasoftwarechave ALTER COLUMN idmidiasoftwarechave INTEGER NOT NULL;
ALTER TABLE dbo.midiasoftwarechave ALTER COLUMN idmidiasoftware INTEGER NOT NULL;
COMMIT TRANSACTION

BEGIN TRANSACTION
GO
ALTER TABLE dbo.midiasoftwarechave ADD CONSTRAINT
	PK_midiasoftwarechave PRIMARY KEY CLUSTERED 
	(
	idmidiasoftwarechave
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX idx_chave ON dbo.midiasoftwarechave
	(
		chave
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
ALTER TABLE dbo.midiasoftwarechave ADD CONSTRAINT
	FK_midsof_midsofchave_idx FOREIGN KEY
	(
		idmidiasoftware
	) REFERENCES dbo.midiasoftware
	(
		idmidiasoftware
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION
COMMIT TRANSACTION

-- FIM - GILBERTO NERY - 04/11/2015
