--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- Inicio Mario Hayasaki - 31/03/2014

ALTER TABLE categoriaocorrencia ALTER COLUMN nome TYPE character varying(255);

-- Fim Mario Hayasaki - 31/03/2014

-- INICIO - M�RIO HAYASAKI J�NIOR - 08/04/2014

ALTER TABLE bpm_tipofluxo ALTER nomefluxo TYPE character varying(255);

-- FIM - M�RIO HAYASAKI J�NIOR - 08/04/2014

-- In�cio Thiago Matias 07/04/14

alter table aprovacaomudanca alter column nomeempregado type character varying(256);

-- Fim Thiago Matias

-- INICIO - FL�VIO J�NIOR - 1/04/2014

ALTER TABLE ocorrenciasolicitacao ALTER COLUMN registradopor TYPE character varying(256);

-- FIM - FL�VIO J�NIOR - 1/04/2014

-- INICIO - RODRIGO PECCI ACORSE - 10/04/2014

ALTER TABLE contatosolicitacaoservico ALTER COLUMN nomecontato TYPE character varying(256);

-- FIM - RODRIGO PECCI ACORSE - 10/04/2014
