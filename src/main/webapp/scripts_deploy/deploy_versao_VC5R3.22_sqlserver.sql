--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- INICIO - FABIO TEODORO AMORIM - 23/07/2015

INSERT INTO parametrocorpore (
	idparametrocorpore, 
	nomeparametrocorpore, 
	valor, 
	idempresa, 
	datainicio, 
	datafim, 
	tipodado) 
VALUES (
	269, 
	'parametro.269', 
	(select parametro.valor from parametrocorpore parametro where parametro.idparametrocorpore = 27), 
	1, 
	GETDATE(), 
	null, 
	'Texto');

-- FIM - FABIO TEODORO AMORIM - 23/07/2015
	


-- INICIO - Euler Ramos - 16/09/2015

alter table contratos add centroderesultado int default(0);

-- FIM - Euler Ramos - 16/09/2015

	
-- INICIO - euler.ramos - 15/10/2015

CREATE FUNCTION f_busca_itemtrabalho(@p_idsolicitacao bigint) RETURNS @table_item TABLE (idsolicitacaoservico bigint, iditemtrabalho bigint) AS 
BEGIN 
	INSERT INTO @table_item (idsolicitacaoservico,iditemtrabalho) 
	SELECT DISTINCT ex.idsolicitacaoservico, it.iditemtrabalho FROM execucaosolicitacao ex JOIN bpm_itemtrabalhofluxo it ON ex.idsolicitacaoservico = @p_idsolicitacao AND ex.idinstanciafluxo = it.idinstancia AND it.situacao IN ('Disponivel', 'EmAndamento') 
																				  JOIN bpm_elementofluxo e ON e.idelemento = it.idelemento AND e.tipoelemento = 'Tarefa' 
																				  JOIN bpm_atribuicaofluxo a ON it.iditemtrabalho = a.iditemtrabalho AND a.tipo IN ('Automatica','Delegacao') 
	IF (@@rowcount <= 0) 
	BEGIN 
		INSERT INTO @table_item (idsolicitacaoservico,iditemtrabalho) 
		SELECT TOP 1 ex.idsolicitacaoservico, it.iditemtrabalho 
		FROM execucaosolicitacao ex JOIN bpm_itemtrabalhofluxo it ON ex.idsolicitacaoservico = @p_idsolicitacao AND ex.idinstanciafluxo = it.idinstancia 
									JOIN bpm_elementofluxo e ON e.idelemento = it.idelemento AND e.tipoelemento = 'Tarefa' 
									JOIN bpm_atribuicaofluxo a ON it.iditemtrabalho = a.iditemtrabalho AND a.tipo IN ('Automatica','Delegacao') 
		ORDER BY datahorafinalizacao DESC 
	END 
    RETURN 
END;


CREATE FUNCTION f_execucao_solicitacao(@p_campoData char(1),@p_dataInicio datetime,@p_dataFim datetime,@p_idsolicitacaoservico bigint) RETURNS @table_execucao_solicitacao TABLE 
(idsolicitacaoservico bigint, iditemtrabalho bigint) 
AS 
BEGIN 
	DECLARE @table_ss TABLE (rowNumber bigint, idsolicitacaoservico bigint) 
	DECLARE @currentrow bigint 
	DECLARE @count bigint 
	DECLARE @idSolicitacao bigint 
 
	IF @p_campoData IS NULL 
		IF @p_idsolicitacaoservico IS NULL 
			INSERT INTO @table_ss (rowNumber, idsolicitacaoservico) 
			SELECT row_number() over(order by idsolicitacaoservico), idsolicitacaoservico FROM solicitacaoservico ORDER BY idsolicitacaoservico 
		ELSE 
			INSERT INTO @table_ss (rowNumber, idsolicitacaoservico) 
			SELECT row_number() over(order by idsolicitacaoservico), idsolicitacaoservico FROM solicitacaoservico WHERE idsolicitacaoservico = @p_idsolicitacaoservico ORDER BY idsolicitacaoservico 
	ELSE 
		BEGIN 
			IF @p_campoData = 'A' 
				IF @p_idsolicitacaoservico IS NULL 
					INSERT INTO @table_ss (rowNumber, idsolicitacaoservico) 
					SELECT row_number() over(order by idsolicitacaoservico), idsolicitacaoservico FROM solicitacaoservico WHERE datahorasolicitacao BETWEEN @p_dataInicio AND @p_dataFim ORDER BY idsolicitacaoservico 
				ELSE 
					INSERT INTO @table_ss (rowNumber, idsolicitacaoservico) 
					SELECT row_number() over(order by idsolicitacaoservico), idsolicitacaoservico FROM solicitacaoservico WHERE (datahorasolicitacao BETWEEN @p_dataInicio AND @p_dataFim) AND idsolicitacaoservico = @p_idsolicitacaoservico ORDER BY idsolicitacaoservico 
			ELSE 
				IF @p_idsolicitacaoservico IS NULL 
					INSERT INTO @table_ss (rowNumber, idsolicitacaoservico) 
					SELECT row_number() over(order by idsolicitacaoservico), idsolicitacaoservico FROM solicitacaoservico WHERE datahorafim BETWEEN @p_dataInicio AND @p_dataFim ORDER BY idsolicitacaoservico 
				ELSE 
					INSERT INTO @table_ss (rowNumber, idsolicitacaoservico) 
					SELECT row_number() over(order by idsolicitacaoservico), idsolicitacaoservico FROM solicitacaoservico WHERE (datahorafim BETWEEN @p_dataInicio AND @p_dataFim) AND idsolicitacaoservico = @p_idsolicitacaoservico ORDER BY idsolicitacaoservico 
		END; 
 
	SET @currentrow = 1 
	SET @count = (SELECT count(*) FROM @table_ss) 
 
	WHILE @currentrow <= @count 
	BEGIN 
		set @idSolicitacao = (select idsolicitacaoservico from @table_ss where rowNumber = @currentrow) 
 
		INSERT INTO @table_execucao_solicitacao (idsolicitacaoservico, iditemtrabalho) 
	    SELECT idsolicitacaoservico, iditemtrabalho FROM f_busca_itemtrabalho(@idSolicitacao) 
 
		set @currentrow = @currentrow +1 
	END 
	RETURN 
END;

-- FIM - euler.ramos - 15/10/2015
