--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
set sql_safe_updates = 0;

-- INICIO - THIAGO BORGES DA SILVA - 30/03/2015

ALTER TABLE requisicaoviagem
DROP FOREIGN KEY fk_requisicaoviagem_reference_cidadedestino,
DROP FOREIGN KEY fk_requisicaoviagem_reference_cidadeorigem;

ALTER TABLE requisicaoviagem
CHANGE COLUMN idcidadeorigem idcidadeorigem INT(11) NULL,
CHANGE COLUMN idcidadedestino idcidadedestino INT(11) NULL,
CHANGE COLUMN datainicio datainicio DATE NULL,
CHANGE COLUMN datafim datafim DATE NULL,
CHANGE COLUMN qtdedias qtdedias INT(11) NULL;

ALTER TABLE requisicaoviagem
ADD COLUMN finalidade VARCHAR(45) NOT NULL AFTER cancelarrequisicao;

ALTER TABLE roteiroviagem
ADD COLUMN hoteispreferenciais VARCHAR(150) NULL AFTER volta,
ADD COLUMN horainicio VARCHAR(5) NULL AFTER hoteispreferenciais,
ADD COLUMN horafim VARCHAR(5) NULL AFTER horainicio,
ADD COLUMN aeroportoorigem VARCHAR(45) NULL AFTER horafim,
ADD COLUMN aeroportodestino VARCHAR(100) NULL AFTER aeroportoorigem;

CREATE TABLE dadosbancariosintegrante(
iddadosbancarios INT NOT NULL,
idintegrante INT NULL,
banco VARCHAR(3) NULL,
agencia VARCHAR(5) NULL,
conta VARCHAR(20) NULL,
operacao VARCHAR(20) NULL,
cpf VARCHAR(14) NULL,
PRIMARY KEY (`iddadosbancarios`));

ALTER TABLE rh_funcionario
ADD COLUMN banco VARCHAR(3) NULL AFTER cpf,
ADD COLUMN agencia VARCHAR(5) NULL AFTER banco,
ADD COLUMN conta VARCHAR(20) NULL AFTER agencia;

-- FIM

-- INICIO - RENATO ARAUJO JESUS - 24/04/2015

delete from menu where nome = '$menu.prestacoesPendentes';

-- FIM - RENATO ARAUJO JESUS - 24/04/2015

set sql_safe_updates = 1;
