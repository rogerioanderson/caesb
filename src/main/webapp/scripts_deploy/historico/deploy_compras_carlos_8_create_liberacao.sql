--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     08/01/2013 23:03:48                          */
/*==============================================================*/


drop table if exists liberacao;

drop table if exists liberacaomudanca;

/*==============================================================*/
/* Table: liberacao                                             */
/*==============================================================*/
create table liberacao
(
   idliberacao          int not null,
   idsolicitante        int not null,
   idresponsavel        int,
   titulo               varchar(100) not null,
   descricao            text not null,
   datainicial          date not null,
   datafinal            date not null,
   dataliberacao        date,
   situacao             char(1) not null comment 'A - Aceita
            E - Em execu��o
            F - Finalizada
            X - Cancelada',
   risco                char(1) not null comment 'B - Baixo
            M - M�dio
            A - Alto',
   versao               varchar(25)
);

alter table liberacao
   add primary key (idliberacao);

/*==============================================================*/
/* Table: liberacaomudanca                                      */
/*==============================================================*/
create table liberacaomudanca
(
   idliberacao          int not null,
   idrequisicaomudanca  int not null
);

alter table liberacaomudanca
   add primary key (idliberacao, idrequisicaomudanca);

alter table liberacao add constraint fk_reference_720 foreign key (idsolicitante)
      references empregados (idempregado) on delete restrict on update restrict;

alter table liberacao add constraint fk_reference_721 foreign key (idresponsavel)
      references empregados (idempregado) on delete restrict on update restrict;

alter table liberacaomudanca add constraint fk_reference_709 foreign key (idliberacao)
      references liberacao (idliberacao) on delete restrict on update restrict;

alter table liberacaomudanca add constraint fk_reference_710 foreign key (idrequisicaomudanca)
      references requisicaomudanca (idrequisicaomudanca) on delete restrict on update restrict;

