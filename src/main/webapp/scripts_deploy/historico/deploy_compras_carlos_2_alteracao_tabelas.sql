--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
-- bpm
alter table bpm_tipofluxo add nomeclassefluxo varchar(255);
alter table bpm_elementofluxo add template varchar(40);
alter table bpm_elementofluxo add intervalo int;
alter table bpm_elementofluxo add condicaodisparo text;
alter table bpm_elementofluxo add multiplasinstancias char(1);
alter table bpm_itemtrabalhofluxo add datahoraexecucao timestamp

-- servico
alter table servico add idtemplatesolicitacao int;
alter table servico add idtemplateacompanhamento int;

alter table servico add constraint fk_reference_641 foreign key (idtemplatesolicitacao)
      references citsmart.templatesolicitacaoservico (idtemplate) on delete restrict on update restrict;

alter table servico add constraint fk_reference_642 foreign key (idtemplateacompanhamento)
      references citsmart.templatesolicitacaoservico (idtemplate) on delete restrict on update restrict;

-- fornecedor
alter table fornecedor add telefone varchar(20);
alter table fornecedor add fax varchar(20);
alter table fornecedor add nomeContato varchar(100);
alter table fornecedor add inscricaoEstadual varchar(25);
alter table fornecedor add inscricaoMunicipal varchar(25);
alter table fornecedor add idendereco int;
alter table fornecedor add idendereco int;
alter table fornecedor add tipopessoa char(1);
alter table fornecedor add constraint fk_forn_end foreign key (idendereco)
      references endereco (idendereco) on delete restrict on update restrict;

-- unidade
alter table unidade add idendereco int;
alter table unidade add aceitaentregaproduto char(1);
alter table unidade add constraint fk_unid_end foreign key (idendereco)
      references endereco (idendereco) on delete restrict on update restrict;

-- ufs
alter table ufs add idpais int;
alter table ufs add constraint fk_uf_pais foreign key (idpais)
      references pais (idpais) on delete restrict on update restrict;

INSERT INTO `pais` (`idpais`,`nomepais`) VALUES (1,'Brasil');

update ufs set idpais = 1;

update fornecedor set tipopessoa = 'J';
