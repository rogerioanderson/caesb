--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
ALTER TABLE PROCESSAMENTOBATCH ALTER column DESCRICAO type varchar(256);

--In�cio rodrigo.oliveira
DROP TABLE impacto CASCADE;

CREATE TABLE impacto (
  idimpacto INTEGER NOT NULL, 
  nivelimpacto VARCHAR(100) NOT NULL, 
  siglaimpacto CHAR(2) DEFAULT NULL, 
  CONSTRAINT impacto_pkey PRIMARY KEY(idimpacto)
);

INSERT INTO impacto (idImpacto, nivelImpacto, siglaImpacto) VALUES ('1', 'Alt�ssimo', 'AL');
INSERT INTO impacto (idImpacto, nivelImpacto, siglaImpacto) VALUES ('2', 'Alto', 'A');
INSERT INTO impacto (idImpacto, nivelImpacto, siglaImpacto) VALUES ('3', 'Elevado', 'E');
INSERT INTO impacto (idImpacto, nivelImpacto, siglaImpacto) VALUES ('4', 'M�dio', 'M');
INSERT INTO impacto (idImpacto, nivelImpacto, siglaImpacto) VALUES ('5', 'Baixo', 'B');


DROP TABLE urgencia CASCADE;

CREATE TABLE urgencia (
  idurgencia INTEGER NOT NULL, 
  nivelurgencia VARCHAR(100) NOT NULL, 
  siglaurgencia CHAR(2) DEFAULT NULL, 
  CONSTRAINT urgencia_pkey PRIMARY KEY(idurgencia)
);

INSERT INTO urgencia (idUrgencia, nivelUrgencia, siglaUrgencia) VALUES ('1', 'Cr�tica', 'C');
INSERT INTO urgencia (idUrgencia, nivelUrgencia, siglaUrgencia) VALUES ('2', 'Alta', 'A');
INSERT INTO urgencia (idUrgencia, nivelUrgencia, siglaUrgencia) VALUES ('3', 'M�dia', 'M');
INSERT INTO urgencia (idUrgencia, nivelUrgencia, siglaUrgencia) VALUES ('4', 'Baixa', 'B');


DROP TABLE matrizprioridade;

CREATE TABLE matrizprioridade (
  idmatrizprioridade INTEGER NOT NULL, 
  siglaimpacto CHAR(2) NOT NULL, 
  siglaurgencia CHAR(2) NOT NULL, 
  valorprioridade INTEGER NOT NULL, 
  idcontrato INTEGER, 
  deleted CHAR(1) DEFAULT NULL, 
  CONSTRAINT matrizprioridade_pkey PRIMARY KEY(idmatrizprioridade)
  );
  
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (1,'AL','B',2,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (2,'AL','M',2,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (3,'AL','A',1,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (4,'AL','C',1,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (5,'A','B',3,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (6,'A','M',2,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (7,'A','A',2,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (8,'A','C',1,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (9,'E','B',3,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (10,'E','M',3,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (11,'E','A',2,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (12,'E','C',2,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (13,'M','B',4,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (14,'M','M',3,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (15,'M','A',3,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (16,'M','C',2,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (17,'B','B',5,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (18,'B','M',4,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (19,'B','A',3,NULL,'');
INSERT INTO matrizprioridade (idMatrizPrioridade,siglaImpacto,siglaUrgencia,valorPrioridade,idcontrato,deleted) 
VALUES (20,'B','C',3,NULL,'');

CREATE TABLE tabfederacaodados (
   nometabela VARCHAR(255) NOT NULL,
   chavefinal VARCHAR(255) NOT NULL,
   chaveoriginal VARCHAR(255),
   origem VARCHAR(255),
   criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   ultatualiz TIMESTAMP DEFAULT to_timestamp('01-JAN-70 00:00:00', 'dd-MON-yy hh24:mi:ss'),
   CONSTRAINT tabfederacaodados_pkey PRIMARY KEY (nometabela, chavefinal)
);

--Fim rodrigo.oliveira
