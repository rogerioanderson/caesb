--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
ALTER TABLE limitealcada DROP COLUMN `qualificacaofornecedor` ;

alter table categoriaproduto add pesocotacaopreco int;

alter table categoriaproduto add pesocotacaoprazoentrega int;

alter table categoriaproduto add pesocotacaoprazopagto int;

alter table categoriaproduto add pesocotacaotaxajuros int;

alter table categoriaproduto add pesocotacaoprazogarantia int;

alter table itemcotacao add pesopreco int;

alter table itemcotacao add pesoprazoentrega int;

alter table itemcotacao add pesoprazopagto int;

alter table itemcotacao add pesotaxajuros int;

alter table itemcotacao add pesoprazogarantia int;

alter table criterioavaliacao add tipoavaliacao char(1);

drop table if exists criteriocotacaocategoria;

alter table itemrequisicaoproduto add valoraprovado decimal(8,2);

alter table itemcotacao add exigefornecedorqualificado char(1);

alter table cotacaoitemrequisicao add iditemtrabalho int;
alter table cotacaoitemrequisicao add constraint fk_reference_699 foreign key (iditemtrabalhoaprovacao)
      references bpm_itemtrabalhofluxo (iditemtrabalho) on delete restrict on update restrict;

ALTER TABLE `inspecaoentregaitem` CHANGE COLUMN `avaliacao` `avaliacao` VARCHAR(25) NULL DEFAULT NULL  ;

ALTER TABLE `inspecaopedidocompra` CHANGE COLUMN `avaliacao` `avaliacao` VARCHAR(25) NULL DEFAULT NULL  ;


/*==============================================================*/
/* Table: criteriocotacaocategoria                              */
/*==============================================================*/
create table criteriocotacaocategoria
(
   idcategoria          int not null,
   idcriterio           int not null,
   pesocotacao          int not null
);

alter table criteriocotacaocategoria
   add primary key (idcategoria, idcriterio);

alter table criteriocotacaocategoria add constraint fk_reference_722 foreign key (idcategoria)
      references categoriaproduto (idcategoria) on delete restrict on update restrict;

alter table criteriocotacaocategoria add constraint fk_reference_723 foreign key (idcriterio)
      references criterioavaliacao (idcriterio) on delete restrict on update restrict;

drop table if exists historicosituacaocotacao;

/*==============================================================*/
/* Table: historicosituacaocotacao                              */
/*==============================================================*/
create table historicosituacaocotacao
(
   idhistorico          int not null,
   idcotacao            int not null,
   idresponsavel        int not null,
   datahora             timestamp not null,
   situacao             varchar(25) not null
);

alter table historicosituacaocotacao
   add primary key (idhistorico);

alter table historicosituacaocotacao add constraint fk_reference_696 foreign key (idcotacao)
      references cotacao (idcotacao) on delete restrict on update restrict;

alter table historicosituacaocotacao add constraint fk_reference_697 foreign key (idresponsavel)
      references empregados (idempregado) on delete restrict on update restrict;




