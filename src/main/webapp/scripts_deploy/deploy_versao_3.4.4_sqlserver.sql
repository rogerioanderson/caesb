--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************

-- Inicio - thiago.borges 16/05/2014
ALTER TABLE rh_formacaocurriculo ALTER COLUMN descricao varchar(1000);
-- FIM - thiago.borges 16/05/2014

-- INICIO - david.silva 16/05/2014
ALTER TABLE rh_atitudeindividual ALTER COLUMN descricao varchar(100);
ALTER TABLE rh_atitudeindividual ALTER COLUMN detalhe varchar(1000);
-- FIM - david.silva 16/05/2014

-- INICIO - david.silva 16/05/2014
ALTER TABLE rh_curso ALTER COLUMN descricao varchar(100);
ALTER TABLE rh_curso ALTER COLUMN detalhe varchar(1000);
ALTER TABLE rh_certificacao ALTER COLUMN descricao varchar(100);
ALTER TABLE rh_certificacao ALTER COLUMN detalhe varchar(1000);
ALTER TABLE rh_experienciainformatica ALTER COLUMN descricao varchar(100);
ALTER TABLE rh_experienciainformatica ALTER COLUMN detalhe varchar(1000);
-- FIM - david.silva 16/05/2014
