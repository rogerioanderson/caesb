/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * Utilit�rios para consumo da API de Geocoding do Google
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 12/09/2014
 */
(function( $ ) {
	"use strict";

	var vars = {
		geocoder : null
	};

	var strings = {
		geocoder_error : i18n_message("geographic.geocoding.failed"),
		geocode_zero_results : i18n_message("geographic.notfound.coordinates.warning")
	};

	$.fn.populateProperties = function() {
		if (!vars.geocoder) {
			vars.geocoder = new google.maps.Geocoder();
		}
	}
	
	/**
	 * Realiza a busca por coordenadas de acordo com um endere�o informado
	 * 
	 * @param {address} endere�o a ser consultado
	 * @param {function} callback chamado ao t�rmico da chamada e que recebe um objeto google.maps.LatLng
	 */
	$.fn.performSearch = function(address, callback) {
		$().populateProperties();

		vars.geocoder.geocode({"address": address}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK && results.length > 0) {
				callback(results[0].geometry.location);
			} else if (results.length === 0) {
				alert(StringUtils.format(strings.geocode_zero_results, address));
			} else {
				alert(strings.geocoder_error);
			}
		});
	};

	/**
	 * Geocoding reverso: recupera o endere�o a partir de uma posi��o
	 *
	 * @param {Object} google.maps.LatLng
	 * @param {function} callback chamado ao t�rmico da chamada e que recebe uma string
	 */
	$.fn.locationName = function(position, callback) {
		var latlng = new google.maps.LatLng(position.lat(), position.lng());

		$().populateProperties();

		vars.geocoder.geocode({"latLng": latlng}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK && results[1]) {
				callback(results[1].formatted_address);
			}
		});
	};

}( jQuery ));
