/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * Utilitários para consumo da API de Geolocation do Google
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 28/10/2014
 */
(function( $ ) {

	var browserSupportGeolocation = new Boolean();

	var strings = {
		geolocation_service_failed: "Geolocation service failed.",
		geolocation_browser_doesnt_support: "Your browser doesn't support geolocation. We've placed you in Brasilia."
	};

	/**
	 * Caso o browser dê suporte à geolocalização, recupera a localização e seta como centro do mapa
	 *
	 * @param {params} objeto de parâmetros a serem usados. Deve possuir ao menos os atributos 'map' e  'latLng'
	 */
	$.fn.navigatorGeolocation = function(params) {
		if (navigator.geolocation) {
			browserSupportGeolocation = true;
			navigator.geolocation.getCurrentPosition(function(position) {
			var location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			params.map.setCenter(location);
			params.latLng = params.map.getCenter();
			}, function() {
				$().handleNoGeolocation(browserSupportGeolocation, params);
			});
		} else { // browser não dá suporte a geolocalização
			browserSupportGeolocation = false;
			handleNoGeolocation(browserSupportGeolocation);
		}
	};

	$.fn.handleNoGeolocation = function(errorFlag, params) {
		if (errorFlag == true) {
			console.log(strings.geolocation_service_failed);
		} else {
			console.log(strings.geolocation_browser_doesnt_support);
		}
		params.map.setCenter(params.latLng);
	};

}( jQuery ));
