/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
var UploadUtils_id_iframe = '';
var UploadUtils_id_elemento = '';
var UploadUtils_Action_Anterior = '';

function $m(quem){ 
	return document.getElementById(quem)
}
function remove(quem){ 
	quem.parentNode.removeChild(quem);
}
function addEvent(obj, evType, fn){ 
	if (obj.addEventListener)        
		obj.addEventListener(evType, fn, true)    
	if (obj.attachEvent)        
		obj.attachEvent("on"+evType, fn)
}
function removeEvent( obj, type, fn ) {
	if ( obj.detachEvent ) {
	    obj.detachEvent( 'on'+type, fn );  
	} else {    
		obj.removeEventListener( type, fn, false ); 
	}
} 
/* Funcao que faz o upload */
function uploadFile(form, url_action, id_elemento_retorno, html_exibe_carregando, html_erro_http, id_iframe, id_campo_file, tamanhoMaximoUpload){
	
	/* Obtendo o nome do arquivo para tratamento no servlet. */
	var hiddenArquivo = $m('upFileNameHidden');
	var campoArquivo = $m(id_campo_file);
	var nomeArquivo = campoArquivo.value;
	hiddenArquivo.value = nomeArquivo;
	
	/* Thiago Fernandes - 29/10/2013 - 18:49 - Sol. 121468 - Mensagem pedindo para selecionar um arquivo caso não seja selecionado.  */
	if(nomeArquivo == ''){ 
		alert(i18n_message("uploadAgente.nenhum_arquivo_selecionado"));
		return;
	}
	
	if($m(id_elemento_retorno)==null){
		alert(i18n_message("uploadAgente.elementoPassadoComoParametroNaoExisteNaPagina"));
		return;
	} 
	
	if (id_iframe == null || id_iframe == ''){
		alert(i18n_message("uploadAgente.informeIdDoIframe"));
		return; 	
	}
	
	var file = $m(id_campo_file).files[0];
	if (file != null && file != '') {
		var tamanho = file.size;
		if (tamanho > tamanhoMaximoUpload) {
			alert(i18n_message("uploadAgente.tamanhoMaximoArquivoPermitido")+" "+tamanhoMaximoUpload+" bytes!");
			return;
		}
	}
	
	$m(id_elemento_retorno).style.display = 'block';
    //adicionando o evento ao carregar 
    var carregou = function() {   
    	form.action = UploadUtils_Action_Anterior; 
    	removeEvent($m(id_iframe),"load", carregou);
    	$m(id_elemento_retorno).style.display = 'none';
    } 
    addEvent($m(id_iframe),"load", carregou);
    //setando propriedades do form 
    
    UploadUtils_Action_Anterior = form.action;
    
    form.setAttribute("target",id_iframe);
    form.setAttribute("action",url_action);
    form.setAttribute("method","post"); 
    form.setAttribute("enctype","multipart/form-data"); 
    form.setAttribute("encoding","multipart/form-data"); 
    
    //submetendo 
    form.submit();  
    
    form.action = UploadUtils_Action_Anterior;
    
    //se for pra exibir alguma imagem ou texto enquanto carrega 
    if(html_exibe_carregando.length > 0){
    	$m(id_elemento_retorno ).innerHTML = html_exibe_carregando;
    } 
    
    /* Thiago Fernandes - 29/10/2013 - 18:49 - Sol. 121468 - Limpar campos ao clicar em adicionar um arquivo .  */
    if (id_iframe == 'fraUpload_uploadRequisicaoMudanca') {
    	document.getElementById('descUploadFile_uploadRequisicaoMudanca').value = '';
    }
    if (id_iframe == 'fraUpload_uploadAnexos') {
    	document.getElementById('descUploadFile_uploadAnexos').value = '';
    }
    if (id_iframe == 'fraUpload_uploadPlanoDeReversao') {
    	document.getElementById('descUploadFile_uploadPlanoDeReversao').value = '';
    	document.getElementById('versao_uploadPlanoDeReversao').value = '';
    }
    if (id_iframe == 'fraUpload_uploadAnexosdocsLegais') {
    	document.getElementById('descUploadFile_uploadAnexosdocsLegais').value = '';
    }
    if (id_iframe == 'fraUpload_uploadAnexosDocsGerais') {
    	document.getElementById('descUploadFile_uploadAnexosDocsGerais').value = '';
    }
    if (id_iframe == 'fraUpload_uploadRequisicaoProblema') {
    	document.getElementById('descUploadFile_uploadRequisicaoProblema').value = '';
    }
    
    campoArquivo.value = '';
}
