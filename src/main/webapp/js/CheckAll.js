/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/*
 * Fun��o respons�vel por selecionar todos checkbox ou desmarcar todos
 * @author: Marco Nascimento
 * @date: 23/05/2008
 * marcotulio.nascimento@gmail.com / knox872003@yahoo.com.br
 */

/*
 * Para utilizar esta fun��o;
 * Utilize o modelo:
 * CheckBox de checkAll
 * <input type="checkbox" id="chAll" name="chAll" onclick="checkAll('formControleLotes');" >
 * O Atributo name deve ser identico ao deste modelo e o formul�rio deve ser relativo  
 */
 
/* Check ALL true/false */
function checkAll(objForm) {
 	var qtdEl = document.forms(objForm).elements.length;
	var el = document.forms(objForm).elements;
	var go = false;
	for(i=0; qtdEl > i; i++){
		if(el[i].type == 'checkbox' && !el[i].disabled){
			if(el[i].name == 'chAll' && !el[i].checked){
				go = true;
			}
			if(go==false){
				el[i].checked = true;
			} else {
				el[i].checked = false;
			}
		}
	}
}
