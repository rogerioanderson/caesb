/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/* Este arquivo cont�m fun��es para efeitos gr�ficos jqueryui */

// Fun��o respons�vel por dar efeito tab no elemento tab do jqueryui
// Para funcionar as tabs devem estar dentro de um container com
// id="effect"
// Exemplo em http://jqueryui.com/demos/tabs/

$(function() {
	$( "#tab_container" ).tabs();
});


// Fun��o respons�vel por estilizar bot�es e links segundo o tema jqueryui
// Para funcionar os elements devem estar dentro de um container com
// class = "demo"
// Exemplo em http://jqueryui.com/demos/button/
$(function() {
	$( "input:button, input:submit, input:reset, button", ".demo" ).button();
	// alguns estilos css s�o definidos aqui pois a fun��o button
	// sobrescreve alguns estilos como color, font-size, ...
	$( "input:button, input:submit, input:reset, button", ".demo" ).css({
		'padding' : '3px 10px',
		'vertical-align' : 'middle',
		'height' : '35px',
		'color' : 'black',
		'font-size' : '12px'
	});
});

// Fun��o que aumenta o wrap para o tamanho da tela caso ele seja menor que a mesma
$(function(){
	var window_height = $(window).height();
	var wrap_height = $("#wrap").height();
	var height = window_height > wrap_height ? window_height : wrap_height;
	
	$("#wrap").css({
		'height' : height
	});
});

// Fun��o que permite o efeito accordion
// Exemplo em http://jqueryui.com/demos/accordion/

$(function() {
	$("#accordion").accordion();
});
