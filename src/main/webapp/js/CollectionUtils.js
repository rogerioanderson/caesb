/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
function HashMap(){
	//Variaveis
	this._length = 0;
	this._arrayControle = new Array();
	
	//prototipos de funcoes
	this.set = _set;
	this.get = _get;
	this.setArray = _setArray;
	this.remove = _remove;
	this.toArray = _toArray;	
	this.length = _flength;
	
	

	//Funcoes privadas
	function _set(id, object){
		this[id] = object;
		object.idHashMapInternalControl = id;
		this._arrayControle[this._length] = object;
		this._length++;		
	}
	function _get(id){
		return this[id];
	}
	function _toArray(){
		return this._arrayControle;
	}
	function _remove(id){
		var array = this.toArray();
		for(var i = 0; i < array.length; i++){
			if (array[i].idHashMapInternalControl == id){
				this._arrayControle.splice(i,1); //Remove o elemento do array.
				this._length--;
			}
		}
		delete this[id]; //Remove o elemento do Hash.
	}
	function _flength(){
		return this._length;
	}
	/**
	 * Seta o hash a partir de um array de objetos (deve se usar a funcao que esta exposta setArray e nao _setArray).
	 *   Parametros:
	 *       name: prefixo de cada objeto que sera adicionado no hash
	 *       propIdent: propriedade do objeto que sera concatenada junto com o name para ser o id do objeto no hash.
	 *       arr: array de objetos a ser acrescentado no hash.
	 *            Exemplo: 
	 *                  var hashEpcs = new HashMap();
	 *                  hashEpcs.setArray('EPC','idepc',objsEpc);
	 *                             onde objsEpc � um array de objetos recuperado via Ajax.
	 *                             cada elemento no hash ser� identificado por: 'EPC1'
	 *                                                                          'EPC2' e assim por diante.
	 */
	function _setArray(name,propIdent,arr){
		if (arr == null || arr == undefined){
			return;
		}
		for(var i = 0; i < arr.length; i++){
			var obj = arr[i];
			this.set(name + obj[propIdent], obj);
		}
	}
}
