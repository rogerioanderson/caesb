/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/* In�cio do c�digo de Requisito SLA */
		
	var count4 = 0;
		
	function insereRowRequisitoSLA(idRequisitoSLA, assuntoRequisito, dataVinculacao){
		
		var tabela = document.getElementById('tabelaRequisitoSLA');
		var lastRow = tabela.rows.length;
		
		var row = tabela.insertRow(lastRow);
		count4++;
		
		var coluna = row.insertCell(0);
		coluna.innerHTML = '<input type="hidden" id="idRequisitoSLA' + count4 + '" name="idRequisitoSLA" value="' + idRequisitoSLA + '"/><img id="imgExcluiUnidadePrioridade' + count4 + '" style="cursor: pointer;" title="i18n_message("citcorpore.comum.excluir")" src="/citsmart/imagens/delete.png" onclick="removeLinhaTabela(\'tabelaRequisitoSLA\', this.parentNode.parentNode.rowIndex);">';
		
		coluna = row.insertCell(1);
		coluna.innerHTML = '<input type="text" class="text" id="dataVinculacao' + count4 + '" name="dataVinculacao" value="' + dataVinculacao + '" style="width: 100%; border: 0 none;" readonly="readonly" />';
		
		coluna = row.insertCell(2);
		coluna.innerHTML = '<input type="text" class="text" id="assuntoRequisito' + count4 + '" name="assuntoRequisito" value="' + assuntoRequisito + '" style="width: 100%; border: 0 none;" readonly="readonly" />';
		
		limpaRequisitoSLA();
		
	}
	
	function limpaRequisitoSLA(){
		document.form.idRequisitoSLAVinc.value = '';
		document.form.addRequisitoSLA.value = '';
		document.form.dataVinculacaoSLA.value = '';
	}
	
	function removeLinhaTabela(idTabela, rowIndex) {
		if (confirm(i18n_message("citcorpore.comum.deleta"))) {
			HTMLUtils.deleteRow(idTabela, rowIndex);
		}
	}
	
	function serializaRequisitoSLA(){
		
		var tabela = document.getElementById('tabelaRequisitoSLA');
		var count = tabela.rows.length;
		var contadorAux = 0;
		var requisitoSLA = new Array();
		for ( var i = 1; i <= count; i++) {
			var trObj = document.getElementById('idRequisitoSLA' + i);
			if (!trObj) {
				continue;
			}
			requisitoSLA[contadorAux] = getRequisitoSLA(i);
			contadorAux = contadorAux + 1;
		}
		
		var requisitoSlaSerializados = ObjectUtils.serializeObjects(requisitoSLA);
		document.form.requisitoSlaSerializados.value = requisitoSlaSerializados;
		
		return true;
		
	}
	
	function getRequisitoSLA(seq) {
		var SlaRequisitoSlaDTO = new CIT_SlaRequisitoSlaDTO();
		SlaRequisitoSlaDTO.sequencia = seq;
		SlaRequisitoSlaDTO.idRequisitoSLA = document.getElementById('idRequisitoSLA' + seq).value;
		SlaRequisitoSlaDTO.dataVinculacao = document.getElementById('dataVinculacao' + seq).value;
		return SlaRequisitoSlaDTO;
	}
	
	function deleteAllRowsRequisitoSLA() {
		var tabela = document.getElementById('tabelaRequisitoSLA');
		var count = tabela.rows.length;

		while (count > 1) {
			tabela.deleteRow(count - 1);
			count--;
		}
		
		count4 = 0;
	}
	
	
