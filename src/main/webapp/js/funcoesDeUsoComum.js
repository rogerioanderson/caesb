/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
var locale_format = locale === 'en' ? 'MM/dd/yyyy' : 'dd/MM/yyyy';

function defineLocale() {
	if (typeof(locale) === "undefined") locale = '';
}

/* Desenvolvedor: Fabio Amorim  Data: 02/06/2015 - Horário: 09:15  ID Citsmart: 168912  Motivo/Comentário: Alterado para possibilitar exibir texto sempre em uft-8. */
function i18n_message(lbl) {
	var l = bundle['key'][lbl];

	if (l === undefined) {
		l = lbl;
	}
	return l;
}

function internacionalizar(parametro){
	document.getElementById('locale').value = parametro;
	document.formInternacionaliza.fireEvent('internacionaliza');
}

/* Desenvolvedor: Euler.Ramos  Data: 23/10/2013 - Horário: 16h04min  ID Citsmart: 120393  Motivo/Comentário: Para evitar o erro: "...has no method fecharTelaAguarde" na tela de pesquisa da base de conhecimento. */
function fecharJanelaAguarde(){
	JANELA_AGUARDE_MENU.hide();
}

//imprime no console qualquer erro de javascript no sistema
onerror = handleErr;
function handleErr(msg, url, l) {
	var txt;
	txt += "Erro: " + msg + " - ";
	txt += "URL: " + url + " - ";
	txt += "Linha: " + l;
	console.log(txt);
	return true;
}

/* desenvolvedor: rcs (Rafael César Soyer)
 o método abaixo limpa todos os campos de uma tela
 data: 26/12/2014
*/
function limpar() {
	document.form.clear();
}

function validaData(){
	var dataInicio = document.getElementById("dataInicio").value;
	var dataFim	= document.getElementById("dataFim").value;

	if (typeof(locale) === "undefined") locale = '';

	var dtInicio = new Date();
	var dtFim = new Date();

	var dtInicioConvert = '';
	var dtFimConvert = '';
	var dtInicioSplit = dataInicio.split("/");
	var dtFimSplit = dataFim.split("/");

	if (locale == 'en') {
		dtInicioConvert = dtInicioSplit[2] + "/" + dtInicioSplit[0] + "/" + dtInicioSplit[1];
		dtFimConvert = dtFimSplit[2] + "/" + dtFimSplit[0] + "/" + dtFimSplit[1];
	} else {
		dtInicioConvert = dtInicioSplit[2] + "/" + dtInicioSplit[1] + "/" + dtInicioSplit[0];
		dtFimConvert = dtFimSplit[2] + "/" + dtFimSplit[1] + "/" + dtFimSplit[0];
	}

	dtInicio.setTime(Date.parse(dtInicioConvert)).setFullYear;
	dtFim.setTime(Date.parse(dtFimConvert)).setFullYear;

	if (dataInicio.trim() == ""){
		alert(i18n_message("citcorpore.comum.validacao.datainicio"));
		return false;
	} else if(DateTimeUtil.isValidDate(dataInicio) == false){
		alert(i18n_message("citcorpore.comum.datainvalida"));
		document.getElementById("dataInicio").value = '';
		return false;
	}

	if (dataFim.trim() == ""){
		alert(i18n_message("citcorpore.comum.validacao.datafim"));
		return false;
	} else if(DateTimeUtil.isValidDate(dataFim) == false){
		alert(i18n_message("citcorpore.comum.dataFinalInvalida"));
		document.getElementById("dataFim").value = '';
		return false;
	}

	if (dtInicio > dtFim){
		alert(i18n_message("citcorpore.comum.dataInicioMenorFinal"));
		return false;
	}
	return true;
}

/**
 * @author cristian.guedes
 * @param dataInicio
 * @param dataFim
 * @returns {Boolean}
 */
function validaDataParametrizado(dataInicio, dataFim){
	if (typeof(locale) === "undefined") locale = '';

	var dtInicio = new Date();
	var dtFim = new Date();

	var dtInicioConvert = '';
	var dtFimConvert = '';
	var dtInicioSplit = dataInicio.split("/");
	var dtFimSplit = dataFim.split("/");

	if (locale == 'en') {
		dtInicioConvert = dtInicioSplit[2] + "/" + dtInicioSplit[0] + "/" + dtInicioSplit[1];
		dtFimConvert = dtFimSplit[2] + "/" + dtFimSplit[0] + "/" + dtFimSplit[1];
	} else {
		dtInicioConvert = dtInicioSplit[2] + "/" + dtInicioSplit[1] + "/" + dtInicioSplit[0];
		dtFimConvert = dtFimSplit[2] + "/" + dtFimSplit[1] + "/" + dtFimSplit[0];
	}

	dtInicio.setTime(Date.parse(dtInicioConvert)).setFullYear;
	dtFim.setTime(Date.parse(dtFimConvert)).setFullYear;

	if (dataInicio.trim() == ""){
		alert(i18n_message("citcorpore.comum.validacao.datainicio"));
		return false;
	} else if(DateTimeUtil.isValidDate(dataInicio) == false){
		alert(i18n_message("citcorpore.comum.datainvalida"));
		document.getElementById("dataInicio").value = '';
		return false;
	}

	if (dataFim.trim() == ""){
		alert(i18n_message("citcorpore.comum.validacao.datafim"));
		return false;
	} else if(DateTimeUtil.isValidDate(dataFim) == false){
		alert(i18n_message("citcorpore.comum.dataFinalInvalida"));
		document.getElementById("dataFim").value = '';
		return false;
	}

	if (dtInicio > dtFim){
		alert(i18n_message("citcorpore.comum.dataInicioMenorFinal"));
		return false;
	}
	return true;
}

function imprimirRelatorioPeloNomeDoRelatorio(str_nomeDoRelatorio){
	if (validaData()) {
		JANELA_AGUARDE_MENU.show();
		document.form.fireEvent(str_nomeDoRelatorio);
	}
}

function imprimirRelatorio(str_tipoDoRelatorio){
	JANELA_AGUARDE_MENU.show();
	document.form.formatoArquivoRelatorio.value = str_tipoDoRelatorio;
	document.form.fireEvent("imprimirRelatorio");
}

function imprimirRelatorioPdf() {
	var tipoDoRelatorio = "pdf"

	if (validaData()) {
		imprimirRelatorio(tipoDoRelatorio);
	}
}

function imprimirRelatorioXls() {
	var tipoDoRelatorio = "xls"

	if (validaData()) {
		imprimirRelatorio(tipoDoRelatorio);
	}
}

function reportEmpty(){
	alert(i18n_message("citcorpore.comum.relatorioVazio"));
	 }

     function isDateValid(date){
        return DateTimeUtil.isValidDate(date);
     }

     function isHourValid(time){
        time = time.split(":");

        if(parseInt(time[0]) > 23){
            return false;
        }

        return true;
     }

     function isMinuteValid(time){
        time = time.split(":");

        if(parseInt(time[1]) > 59){
            return false;
        }

        return true;
     }
     
/**
 * Redimensiona a popup em tamanho compativel com o tamanho da tela
 */
function redimensionarTamhanho(identificador, tipo_variacao) {
	var h;
	var w;
	switch (tipo_variacao) {
	case "PEQUENO":
		w = parseInt($(window).width() * 0.25);
		h = parseInt($(window).height() * 0.35);
		break;
	case "MEDIO":
		w = parseInt($(window).width() * 0.5);
		h = parseInt($(window).height() * 0.6);
		break;
	case "GRANDE":
		w =  '75%';
		h =  '85%';
		break;
	case "XG":
		w = parseInt($(window).width() * 0.95);
		h = parseInt($(window).height() * 0.95);
		break;
	default:
		w = parseInt($(window).width() * 0.5);
		h = parseInt($(window).height() * 0.6);
	}

	$(identificador).dialog("option", "top", 0);
	$(identificador).dialog("option", "width", w);
	$(identificador).dialog("option", "height", h)
}


//---------Tratamento Mascara Telefone 8 e 9 digitos

/**
 * Desenvolvedor: ibimon.morais - Data: 10/09/2015 - Horário: 17:42 - ID Citsmart: 177545 -
 * Motivo/Comentário: Alteração para que o campo de controle de telefone, aceite informacoes com 8 e 9 digitos,
 * formatando com o DDD (AA) e (AAA) e adaptando tambem o "-". Foi necessario formataca manual porque o layout 
 * novo, usa um jquery especifico, e ainda existe o jquery antigo, para nao correr risco, optei por usar uma
 * forma generica que nao impacte na usabilidade do campo, caso a pessoa digite 10, 11 ou mais digitos, vai 
 * formatar para no maximo 12 caracteres, caso seja informado um telefone invalido, nao aplica a mascara
 * mantendo o que o usuario informou. No controle do "onblur", ele formata o telefone de acordo com a quantidade
 * de caracteres informados. 		 
 */

/**
 * Tratamento de mascarmascararElementoInputrpreta e determina o timeout de execução.
 * @param o - objeto
 * @param f - função que será executada apos o tempo
 */
function mascararElementoInput(o,f){
    v_obj=o;
    v_fun=f;
    setTimeout("executarMascara()",5);
}

/**
 * Execução da função indicada na hora de mascarar o valor digitado no input.
 */
function executarMascara(){
    v_obj.value = v_fun(v_obj.value)
}

/**
 * Retira o que nao e digito, caso venha nulo ou vazio, devolve um valor em branco.
 * @param valor
 * @returns valor - sem caracteres, somente com os digitos
 */
function retiraCrtFone(valor){
	return (valor != null && valor.length >0) ? valor.replace(/\D/g,"") : ''; 
}

/**
 * Devolve o elemente de acordo com o id.
 * @param el 
 * @returns element encontrato de acordo com o parametro
 */
function id( elemento ){
	return document.getElementById( elemento );
}

function aplicarMascaraNaAcaoOnKeyPress(telefone){
	var aux = retiraCrtFone(telefone);
	if(aux == null || aux == ''){
		telefone = '';
	}else{
		switch (aux.length) {
		case 1:
			telefone = '(' + telefone;
			break;
		case 3:	
			telefone = telefone + ') ';
			break;
		case 8:
			telefone = telefone + '-';
			break;
		default:
			break;
		}
	}
	return telefone;
}

function calculaEMontagemMascaraTelefone(f1){
	var ff1 = retiraCrtFone(f1.value);
	
	if(ff1 == null || ff1 == ''){
		f1.value = '';
		return;
	}
	
	if( ff1.substring(0, 1) == '0'){
		if(ff1.length == 11 ){
			f1.value =  "(" +ff1.substring(0, 3) +") "+ ff1.substring(3, 7) + "-" + ff1.substring(7, 11);
		}else if(ff1.length >= 12 ){
			f1.value =  "(" +ff1.substring(0, 3) +") "+ ff1.substring(3, 8) + "-" + ff1.substring(8, 12);
		}
	}else{
		if(ff1.length == 10 ){
			f1.value =  "(" +ff1.substring(0, 2) +") "+ ff1.substring(2, 6) + "-" + ff1.substring(6, 10);
		}else if(ff1.length == 11 ){
			f1.value = "(" +ff1.substring(0, 2) +") "+ ff1.substring(2, 7) + "-" + ff1.substring(7, 11);
		}else if(ff1.length > 11 ){
			f1.value = "(" +ff1.substring(0, 3) +") "+ ff1.substring(3, 8) + "-" + ff1.substring(8, 12);
		}
	}
}

