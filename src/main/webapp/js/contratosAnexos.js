/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
    carregouIFrameAnexo = function() {    
    	//POPUP_ANEXO.hide();
		refreshFuncionalidade();
		HTMLUtils.removeEvent(document.getElementById("frameUploadAnexo"),"load", carregouIFrameAnexo);
    };
    
	submitFormAnexo = function(){
		HTMLUtils.addEvent(document.getElementById("frameUploadAnexo"),"load", carregouIFrameAnexo, true);
		
	    document.formAnexo.setAttribute("target","frameUploadAnexo");
	    document.formAnexo.setAttribute("method","post"); 
	    document.formAnexo.setAttribute("enctype","multipart/form-data"); 
	    document.formAnexo.setAttribute("encoding","multipart/form-data"); 
	    
	    //submetendo 
	    document.formAnexo.aba.value = abaSelecionada;
	    document.formAnexo.submit();
	};
	
	setaInfoImagens = function(){
		document.formAnexo.idContrato.value = document.formProntuario.idContrato.value;
		document.formProntuario.funcaoUpload.value = 'anexos';
		document.getElementById('divEnviarArquivo').style.display='block';
		document.getElementById('frameUploadCertificadoApplet').style.display='none';
		document.getElementById('divMsgCertDigApplet').style.display='none';
		document.getElementById('frameUploadCertificadoApplet').src = 'about:blank';		
		POPUP_ANEXO.showInYPosition({top:30});
	};
