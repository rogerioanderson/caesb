/**********************************LICENCA*GPLv2********************************************************************
* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
*                                                                                                                  *
* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
*                                                                                                                  *
* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
*                                                                                                                  *
* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
*                                                                                                                  *
* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
********************************************************************************************************************/
/**
 * Sobrescreve o comportamento do alert exibindo um alert do BootStrap
 *
 * @author bruno.ribeiro - <a href="mailto:bruno.ribeiro@centrait.com.br">bruno.ribeiro@centrait.com.br</a>
 * @since 24/11/2014
 */
(function( $ ) {

	var proxied = window.alert;

	window.alert = function(message) {
		if (message.indexOf("Google has disabled use of the Maps API") != -1) {
			$().handleWrongMapsInitialize();
			var warning = '<div class="alert no-key">{0}<a href="{1}" target="_blank"><strong>{2}</strong></a></div>';

			var ixHttps = message.indexOf("http");
			var url = message.substring(ixHttps, message.length);
			var newMessage = message.substring(0, ixHttps);

			$("#map-box-div").html(StringUtils.format(warning, newMessage, url, url));
		} else {
			var alertLen = $("#alerts").length;
			if (alertLen > 0) {
				var warning = '<div id="alert" class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>{0}</div>';
				$("#alerts").html(StringUtils.format(warning, message));
				window.setTimeout(function() {
					$("#alert").alert("close");
				}, 5000);
			} else {
				return proxied.apply(this, arguments);
			}
		}
	};

}( jQuery ));
