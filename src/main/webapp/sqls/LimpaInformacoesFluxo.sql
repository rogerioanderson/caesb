--**********************************LICENCA*GPLv2*********************************************************************
--* Copyright [2011,2012,2013,2014,2015,2016] da CentralIT Tecnologia da Informa��o Ltda (www.centralit.com.br)      *
--*                                                                                                                  *
--* Este arquivo � parte do programa/software: Citsmart (www.citsmart.com.br)                                        *
--*                                                                                                                  *
--* O Citsmart � um software livre; voc� pode redistribui-lo e/ou modific�-lo dentro dos termos da Licen�a           *
--* P�blica Geral GNU como publicada pela Funda��o do Software Livre (FSF); na vers�o 2 da Licen�a.                  *
--*                                                                                                                  *
--* Este programa/software � distribu�do na esperan�a que possa ser �til, mas SEM NENHUMA GARANTIA; sem uma          *
--* garantia impl�cita de ADEQUA��O a qualquer MERCADO ou APLICA��O EM PARTICULAR. Veja a Licen�a P�blica Geral      *
--* GNU/GPL em portugu�s para maiores detalhes.                                                                      *
--*                                                                                                                  *
--* Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU, sob o t�tulo 'LICENCA.txt', junto com este        *
--* programa/software, se n�o, acesse o Portal do Software P�blico Brasileiro no endere�o www.softwarepublico.gov.br *
--* ou escreva para a Funda��o do Software Livre (FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,USA  *
--********************************************************************************************************************
--SQLs de exclus�o de dados do fluxo:
delete from bpm_objetoinstanciafluxo;
delete FROM bpm_atribuicaofluxo;
delete from bpm_historicoitemtrabalho;
delete from bpm_itemtrabalhofluxo;
delete from bpm_instanciafluxo;
delete from solicitacaoservico;
delete from solicitacaoservicomudanca;
delete from solicitacaoservicoevtmon;
delete from solicitacaoservicoproblema;
delete from contatosolicitacaoservico;
delete from conhecimentosolicitacaoservico;
delete from execucaosolicitacao;
delete from reaberturasolicitacao;
delete from pesquisasatisfacao;
delete from ocorrenciasolicitacao;

--Limpeza do log do sistema
delete from logdados;

--Se quiser apagar os elementos e fluxo use estes:
delete from citsmart.bpm_sequenciafluxo;
delete from citsmart.bpm_elementofluxo;
delete from citsmart.bpm_fluxo;
delete from citsmart.bpm_tipofluxo;

